package com.plugin.yongyou.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DispatchLists {
	//来源订单号
	String cInvCode;//材料编码
	String cInvName;//材料名称
	String cInvStd;//Inventory.cInvStd规格型号
	String cComUnitName;//主计量ComputationUnit.cComUnitName
	float iQuantity;//数量
	String cMemo;//备注
	//体积
	//电机编码
}
