package com.plugin.yongyou.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DispatchList {

	String cDLCode;//出库单号
	
	String DLID;
	
	Date dDate;		//出库日期
	//仓库
	String cDepName;//生产部门
	//订单号
	//产品编号
	String cRdName;	//出库类别
	//生产班组
	String cMemo;	//备注
	
	String cMaker;//制单人
	String cVerifier;//审核人
	String cCusName;//客户
	
}
