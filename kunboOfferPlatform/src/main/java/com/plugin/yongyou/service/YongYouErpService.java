package com.plugin.yongyou.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.plugin.yongyou.controller.YongYouController;
import com.plugin.yongyou.entity.DispatchList;
import com.plugin.yongyou.entity.DispatchListCDLCode;
import com.plugin.yongyou.entity.DispatchLists;
import com.plugin.yongyou.util.ErpSqlUtil;
import com.shuohe.util.returnBean.ReturnBean3;

import cn.hutool.core.util.StrUtil;

/**
 * @author qin
 *
 */
@Service
public class YongYouErpService {
	
	
	private Logger logger = LoggerFactory.getLogger(YongYouErpService.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	/** 
	* @Description: 从ERP获得发货单的列表 
	* @Title: getDispatchListCDLCodeList 
	* @return List<String>
	* @author qin
	* @date 2018年6月27日上午11:00:17
	*/ 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getDispatchListCDLCodeList(String cDLCode)
	{
		if(StrUtil.isBlank(cDLCode)) return null;
		
		QueryRunner qRunner = new QueryRunner();   
		String sql = "";
		sql += " SELECT";
		sql += " 	top 50 cDLCode";
		sql += " FROM";
		sql += " 	DispatchList";
		sql += " ";
		sql += " WHERE";
		sql += " 	cDLCode LIKE '%"+cDLCode+"%'";
		
		List<String> str_list = new ArrayList<String>();
		
		try {	
			List<DispatchListCDLCode> list = new ArrayList<DispatchListCDLCode>();
			list = (List<DispatchListCDLCode>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(DispatchListCDLCode.class));
			for(DispatchListCDLCode l:list)
			{
				str_list.add(l.getCDLCode());
			}
			return str_list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DispatchList> getDispatchByCDLCode(String cDLCode)
	{
		if(StrUtil.isBlank(cDLCode)) return null;
		
		QueryRunner qRunner = new QueryRunner();   
		String sql = "";
		sql += " SELECT";
		sql += " 	top 50";
		sql += " 	DispatchList.*, Rd_Style.cRdName,";
		sql += " 	Department.cDepName";
		sql += " FROM";
		sql += " 	DispatchList";
		sql += " LEFT JOIN Rd_Style ON Rd_Style.cRdCode = DispatchList.cRdCode";
		sql += " LEFT JOIN Department ON Department.cDepCode = DispatchList.cDepCode";
		sql += " WHERE";
		sql += " 	DispatchList.cDLCode LIKE '%"+cDLCode+"%'";

		try {	
//			List<DispatchList> list = new ArrayList<DispatchList>();
//			list = (List<DispatchList>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(DispatchList.class));
//			if(list.size() == 1) return list.get(0);
//			else return null;
			return (List<DispatchList>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(DispatchList.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<DispatchLists> getDispatchListsByDLID(String dLID)
	{
		if(StrUtil.isBlank(dLID)) return null;
		
		QueryRunner qRunner = new QueryRunner();   
		String sql = "";
		sql += " SELECT";
		sql += " 	DispatchLists.*, Inventory.cInvStd,";
		sql += " 	ComputationUnit.cComUnitName";
		sql += " FROM";
		sql += " 	DispatchLists";
		sql += " LEFT JOIN Inventory ON Inventory.cInvCode = DispatchLists.cInvCode";
		sql += " LEFT JOIN ComputationUnit ON ComputationUnit.cComunitCode = Inventory.cComUnitCode";
		sql += " WHERE";
		sql += " 	DispatchLists.DLID = '"+dLID+"'";

		try {	
			return (List<DispatchLists>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(DispatchLists.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
//	@Autowired
//    private JdbcTemplate jdbcTemplate;
//	
//	@Autowired
//	private YongYouErpUserDao userDao;
//	
//	@Autowired
//	private YongYouErpDepartmentDao departmentDao;
//
//	
//	private Logger logger = LoggerFactory.getLogger(YongYouErpService.class);
//	
//	/** 
//	* @Description: 获取ERP用户对应表 
//	* @Title: getLocalUsers 
//	* @return List<YongYouErpUser>
//	* @author qin
//	* @date 2018年6月3日上午10:56:56
//	*/ 
//	public List<YongYouErpUser> getSyncUsers()
//	{
//		return userDao.findAll();
//	}
//	
//	/** 
//	* @Description: 获得ERP的所有用户 
//	* @Title: getErpUsers 
//	* @return List<YongYouErpUser>
//	* @author qin
//	* @date 2018年6月2日下午9:15:12
//	*/ 
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	public List<YongYouErpUser> getErpUsers()
//	{
//		QueryRunner qRunner = new QueryRunner();   
//		String sql = "select * from Person";
//		try {
//			return (List<YongYouErpUser>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(YongYouErpUser.class));
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//	}
//	/** 
//	* @Description: 同步ERP和本地的同步表，将ERP新增的用户添加到本地的不同表中 
//	* @Title: syncLocalErp 
//	* @return int
//	* @author qin
//	* @date 2018年6月2日下午9:33:51
//	*/ 
//	public ReturnBean3 syncErpAndLocalUsers()
//	{
//		//获得用友的所有ERP用户
//		List<YongYouErpUser> erp_user_list = this.getErpUsers();
//		if(erp_user_list == null)
//		{
//			return new ReturnBean3(false,"连接用友ERP错误");
//		}
//		//获得本地ERP用户对应表
//		List<YongYouErpUser> local_user_list = userDao.findAll();
//		//比较本地和ERP，本地不存在的用户则插入，本地存在的用户不操作
//		int operate_number = 0;
//		for(int i=0;i<erp_user_list.size();i++)
//		{
//			boolean flag = false;
//			for(int a=0;a<local_user_list.size();a++)
//			{
//				if(local_user_list.get(a).getCPersonCode().equals(erp_user_list.get(i).getCPersonCode()))
//				{
//					flag = true;
//					break;
//				}
//			}
//			if(!flag)
//			{
//				userDao.save(erp_user_list.get(i));
//				operate_number++;
//			}
//		}
//		return new ReturnBean3(true,new Integer(operate_number));
//	}
//	/** 
//	* @Description: 解除用友ERP和本地用户关系表之间的联系 
//	* @Title: unlink 
//	* @param cUser_Id
//	* @return ReturnBean3
//	* @author qin
//	* @date 2018年6月2日下午11:01:54
//	*/ 
//	public ReturnBean3 userUnlink(String cpersonCode)
//	{
//		YongYouErpUser yongYouErpUser = userDao.findByCPersonCode(cpersonCode);
//		if(yongYouErpUser == null)
//		{
//			return new ReturnBean3(false,"传递了不存在的ERP用户编号");
//		}
//		else
//		{
//			yongYouErpUser.setUser_id(0);
//			yongYouErpUser.setUser_name(null);
//			userDao.save(yongYouErpUser);
//		}
//		return new ReturnBean3(true,null);
//	}
//
//	/** 
//	* @Description: 保存用户 
//	* @Title: userSave 
//	* @param yongYouErpUser void
//	* @author qin
//	* @date 2018年6月3日上午10:32:51
//	*/ 
//	public void userSave(YongYouErpUser yongYouErpUser)
//	{
//		userDao.save(yongYouErpUser);
//	}
//	
//	
//	/** 
//	* @Description: 获取ERP部门对应表 
//	* @Title: getSyncDepartments 
//	* @return List<YongYouErpUser>
//	* @author qin
//	* @date 2018年6月3日上午10:57:46
//	*/ 
//	public List<YongYouErpDepartment> getSyncDepartments()
//	{
//		return departmentDao.findAll();
//	}
//	/** 
//	* @Description: 获得ERP部门 
//	* @Title: getErpDepartments 
//	* @return List<YongYouErpDepartment>
//	* @author qin
//	* @date 2018年6月3日上午10:55:02
//	*/ 
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	public List<YongYouErpDepartment> getErpDepartments()
//	{		
//		QueryRunner qRunner = new QueryRunner();   
//		String sql = "select * from department";
//		try {
//			return (List<YongYouErpDepartment>)qRunner.query(ErpSqlUtil.getConnection(), sql, new BeanListHandler(YongYouErpDepartment.class));
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	/** 
//	* @Description: 同步ERP和本地的同步表，将ERP新增的部门添加到本地的不同表中  
//	* @Title: syncLocalDepartment 
//	* @return ReturnBean3
//	* @author qin
//	* @date 2018年6月3日上午10:42:01
//	*/ 
//	public ReturnBean3 syncErpAndLocalDepartments()
//	{
//		//获得用友的所有ERP用户
//		List<YongYouErpDepartment> erp_department_list = this.getErpDepartments();
//		if(erp_department_list == null)
//		{
//			return new ReturnBean3(false,"连接用友ERP错误");
//		}
//		//获得本地ERP用户对应表
//		List<YongYouErpDepartment> local_erp_list = departmentDao.findAll();
//		//比较本地和ERP，本地不存在的用户则插入，本地存在的用户不操作
//		int operate_number = 0;
//		for(int i=0;i<erp_department_list.size();i++)
//		{
//			boolean flag = false;
//			for(int a=0;a<local_erp_list.size();a++)
//			{
//				if(local_erp_list.get(a).getCDepCode().equals(erp_department_list.get(i).getCDepCode()))
//				{
//					flag = true;
//					break;
//				}
//			}
//			if(!flag)
//			{
//				departmentDao.save(erp_department_list.get(i));
//				operate_number++;
//			}
//		}
//		return new ReturnBean3(true,new Integer(operate_number));
//	} 
//	public void departmentSave(YongYouErpDepartment yongYouErpDepartment)
//	{
//		departmentDao.save(yongYouErpDepartment);
//	}
//	
//	public ReturnBean3 departmentUnlink(String cDepCode)
//	{
//		YongYouErpDepartment yongYouErpDepartment = departmentDao.findByCDepCode(cDepCode);
//		if(yongYouErpDepartment == null)
//		{
//			return new ReturnBean3(false,"传递了不存在的ERP部门编号");
//		}
//		else
//		{
//			yongYouErpDepartment.setDepartment_id(0);
//			yongYouErpDepartment.setDepartment_name(null);
//			departmentDao.save(yongYouErpDepartment);
//		}
//		return new ReturnBean3(true,null);
//	}
//	
//
//
//	/** 
//	* @Description: 将用户中间表数据拷贝到最终表  
//	* @Title: copyCustomerMiddletableToEnd 
//	* @param middle_table
//	* @param end_table
//	* @return ReturnBean3
//	* @author qin
//	* @date 2018年6月7日上午10:36:47
//	*/ 
//	public ReturnBean3 copyCustomerMiddletableToEnd(String uuid)
//	{
//		//获取中间表数据
//		String middle_sql = "select * from business_cloud_customer_information_middle where uuid='"+uuid+"'";
//		Map<String, Object> middle_data = jdbcTemplate.queryForMap(middle_sql);
//		if(middle_data == null) return new ReturnBean3(false,"没有找到中间表数据");
//		
//		//根据用户的customer_code（用户编码）检索当前用户表是否有重复用户
//		String middle_customer_code = (String) middle_data.get("middle_data");
//		
//		//将中间表数据插入至最后的结果表		
//		String keys = "";
//		String questionmarks = "";
//		ArrayList<Object> args = new ArrayList<Object>();
//		for(String key : middle_data.keySet()){
//			Object value = middle_data.get(key);
// 			System.out.println(key+"  "+value);
//			args.add(value);
//			keys += key+",";
//			questionmarks += "?,";
//		}		
//		keys = keys.substring(0,keys.length()-1);
//		questionmarks = questionmarks.substring(0,questionmarks.length()-1);
//		String sql = "insert into business_cloud_customer_information("+keys+")values("+questionmarks+")";  
//		logger.info(sql);
//		try 
//		{
//			jdbcTemplate.update(sql,args.toArray());
//			return new ReturnBean3(true,"保存成功");
//		}
//		catch(DataAccessException e)
//		{
//			e.printStackTrace();
//			return new ReturnBean3(false,"保存中间表数据是错误");
//		}
//	}
	
}
