package com.plugin.yongyou.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.plugin.yongyou.entity.DispatchList;
import com.plugin.yongyou.entity.DispatchListCDLCode;
import com.plugin.yongyou.entity.DispatchLists;
import com.plugin.yongyou.service.YongYouErpService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.returnBean.ReturnBean3;

@Controller  
@RequestMapping("/plugin/yongyou/*")  
public class YongYouController {
	
	@Autowired
	private YongYouErpService yongYouErpService;

	private Logger logger = LoggerFactory.getLogger(YongYouController.class);
    

    /** 
    * @Description: 获得发货单好的列表
    * @Title: getDispatchListCDLCodeList 
    * @param request
    * @param response
    * @param session
    * @return List<String>
    * @author qin
    * @date 2018年6月27日上午10:55:04
    */ 
    @RequestMapping(value="getDispatchListCDLCodeList.do")  
    public @ResponseBody List<String> getDispatchListCDLCodeList(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    	String cDLCode = request.getParameter("cDLCode");
    	logger.info("cDLCode = "+cDLCode);
    	return yongYouErpService.getDispatchListCDLCodeList(cDLCode);
    }
    
    
    /** 
    * @Description: 根据单号获得发货单的详情 
    * @Title: getDispatchByCDLCode 
    * @param request
    * @param response
    * @param session
    * @return DispatchList
    * @author qin
    * @date 2018年6月27日下午2:55:18
    */ 
    @RequestMapping(value="getDispatchByCDLCode.do")  
    public @ResponseBody List<DispatchList> getDispatchByCDLCode(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    	String cDLCode = request.getParameter("cDLCode");
    	logger.info("cDLCode = "+cDLCode);
    	return yongYouErpService.getDispatchByCDLCode(cDLCode);
    }
    
    
    /** 
    * @Description: 根据单号获得发货单中的物料的清单 
    * @Title: getDispatchListsByDLID 
    * @param request
    * @param response
    * @param session
    * @return List<DispatchLists>
    * @author qin
    * @date 2018年6月27日下午4:08:14
    */ 
    @RequestMapping(value="getDispatchListsByDLID.do")  
    public @ResponseBody List<DispatchLists> getDispatchListsByDLID(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    	String dLID = request.getParameter("dLID");
    	logger.info("dLID = "+dLID);
    	return yongYouErpService.getDispatchListsByDLID(dLID);
    }
    
   
    
}
