package com.shuohe.dao.system.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.system.user.Position;

public interface PositionDao extends JpaRepository<Position, Long> {
	Position findById(int id);

}
