package com.shuohe.dao.system.user;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.system.user.UserStatus;

public interface UserStatusDao extends JpaRepository<UserStatus, Long> {

}
