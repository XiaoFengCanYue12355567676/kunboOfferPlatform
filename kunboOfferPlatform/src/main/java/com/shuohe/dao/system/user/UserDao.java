package com.shuohe.dao.system.user;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.system.user.User;

public interface UserDao extends JpaRepository<User, Long>{
	User findByNameAndPassword(String name, String password);
	User findById(int id);
	
	@Query("from User where position_id=?1")
	List<User> findByPositionId(int position_id);
	
	@Query("from User where name= ?1 and password= ?2")
	User check(String name,String password);
	
	@Query("from User where activiti_uuid= ?1 ")
	User findByActivitiUUID(String activiti_uuid);
	
	User findByName(String name);
	
	@Query( value = "from User", countQuery = "select count(*) from User" )  
	Page<User> page(Pageable pageable);
	
	/** 
	* @Description: 将某以部门下的所有用户变更为根目录下 ,根目录默认为0
	* @Title: upUserDepartmentIdTo0 
	* @param parent_id
	* @return int
	* @author qin
	* @date 2018年7月31日上午11:03:47
	*/ 
	@Modifying
	@Transactional
	@Query("update User user set user.department_id=1 where department_id= ?1")
	int upUserDepartmentIdTo0(int department_id);
	
	
	/** 
	* @Description: 查找某一用户名的用户 
	* @Title: findByName 
	* @param name
	* @return List<User>
	* @author qin
	* @date 2018年7月31日下午12:06:27
	*/ 
	@Query("from User where name= ?1 ")
	List<User> findByNameList(String name);
}
