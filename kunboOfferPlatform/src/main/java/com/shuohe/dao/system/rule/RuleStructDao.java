package com.shuohe.dao.system.rule;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.system.rule.RuleStruct;

public interface RuleStructDao extends JpaRepository<RuleStruct, Long> {

}
