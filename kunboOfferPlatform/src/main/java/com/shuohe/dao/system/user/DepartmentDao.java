package com.shuohe.dao.system.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.system.user.Department;

public interface DepartmentDao  extends JpaRepository<Department, Long>{
	Department findById(int id);
	
	/** 
	* @Description: 根据id删除部门 
	* @Title: deleteById 
	* @param id
	* @return int
	* @author qin
	* @date 2018年7月31日上午10:56:56
	*/ 
	@Modifying
	@Transactional
	@Query("delete Department where id= ?1")
	int deleteById(int id);
	
	
	/** 
	* @Description: 将原父节点的部门全部改为根目录下 
	* @Title: upDateDepartmentParentId 
	* @param parent_id
	* @return int
	* @author qin
	* @date 2018年7月31日上午11:01:05
	*/ 
	@Modifying
	@Transactional
	@Query("update Department department set department.parent_id=1 where parent_id= ?1")
	int upDateDepartmentParentIdTo0(int parent_id);
}