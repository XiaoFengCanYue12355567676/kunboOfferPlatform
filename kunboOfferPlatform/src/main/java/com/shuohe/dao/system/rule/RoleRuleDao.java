package com.shuohe.dao.system.rule;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.system.rule.RoleRule;

public interface RoleRuleDao extends JpaRepository<RoleRule, Long> {
	@Modifying
	@Transactional
	@Query("delete RoleRule where rule_struct_id= ?1")
	int deleteBy_rule_struct_id(int rule_struct_id);
	
	
	@Modifying
	@Transactional
	@Query("delete RoleRule where postation_id= ?1")
	int deleteBy_postation_id(int postation_id);
}
