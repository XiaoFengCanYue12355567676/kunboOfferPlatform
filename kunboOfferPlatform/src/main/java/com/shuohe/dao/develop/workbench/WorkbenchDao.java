package com.shuohe.dao.develop.workbench;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.workbench.Workbench;

public interface WorkbenchDao extends JpaRepository<Workbench, Long> {

	@Modifying
	@Transactional
	@Query("delete Workbench where postation_id= ?1")
	int deleteByPid(int id);
		
	
	@Query("from Workbench where postation_id= ?1")
	Workbench findByPostation(int postation_id);
}
