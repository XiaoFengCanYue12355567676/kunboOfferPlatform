package com.shuohe.dao.develop.url;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.url.Url;

public interface UrlDao extends JpaRepository<Url, Long> {
	List<Url> findByPid(int pid);
	Url findById(int id);
	Url findByName(String name);
	
	@Modifying
	@Transactional
	@Query("delete Url where id= ?1")
	int deleteById(int id);
}
