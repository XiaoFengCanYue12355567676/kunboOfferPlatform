package com.shuohe.dao.develop.poi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.poi.UrlPoi;
public interface UrlPoiDao extends JpaRepository<UrlPoi, Long> {
	List<UrlPoi> findByPid(int pid);
	List<UrlPoi> findByName(String name);
	UrlPoi findById(int id);

	@Modifying
	@Transactional
	@Query("delete UrlPoi where id= ?1")
	int deleteById(int id);
}

