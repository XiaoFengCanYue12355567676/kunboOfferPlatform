package com.shuohe.dao.develop.poi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.poi.UrlPoiHeader;

public interface UrlPoiHeaderDao extends JpaRepository<UrlPoiHeader, Long> {

	@Modifying
	@Transactional
	@Query("delete UrlPoiHeader where pid= ?1")
	int deleteByPid(int pid);
	
	List<UrlPoiHeader> findByPid(int pid);
}

