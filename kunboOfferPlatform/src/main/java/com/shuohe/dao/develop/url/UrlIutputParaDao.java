package com.shuohe.dao.develop.url;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.url.UrlIutputPara;

public interface UrlIutputParaDao extends JpaRepository<UrlIutputPara, Long> {
	List<UrlIutputPara> findByPid(int pid);
	
	
	@Modifying
	@Transactional
	@Query("delete UrlIutputPara where pid= ?1")
	int deleteByPid(int pid);
}
