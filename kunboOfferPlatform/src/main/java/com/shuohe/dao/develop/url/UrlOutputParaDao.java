package com.shuohe.dao.develop.url;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.url.UrlOutputPara;

public interface UrlOutputParaDao extends JpaRepository<UrlOutputPara, Long> {
	List<UrlOutputPara> findByPid(int pid);
	
	@Modifying
	@Transactional
	@Query("delete UrlOutputPara where pid= ?1")
	int deleteByPid(int pid);
}
