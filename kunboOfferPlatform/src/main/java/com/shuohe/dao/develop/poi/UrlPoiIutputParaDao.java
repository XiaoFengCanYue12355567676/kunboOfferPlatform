package com.shuohe.dao.develop.poi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.poi.UrlPoiIutputPara;

public interface UrlPoiIutputParaDao extends JpaRepository<UrlPoiIutputPara, Long> {

	@Modifying
	@Transactional
	@Query("delete UrlPoiIutputPara where pid= ?1")
	int deleteByPid(int pid);
	
	List<UrlPoiIutputPara> findByPid(int pid);
}

