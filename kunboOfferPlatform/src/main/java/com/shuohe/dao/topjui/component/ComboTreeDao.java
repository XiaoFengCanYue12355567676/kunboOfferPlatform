package com.shuohe.dao.topjui.component;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.component.ComboTree;

public interface ComboTreeDao extends JpaRepository<ComboTree, Long>{
	
	@Modifying
	@Transactional
	@Query("delete ComboTree where template_id= ?1")
	public int deleteByPageTepmlateId(String template_id);
	
	@Query("from ComboTree where template_id= ?1")
	public List<ComboTree> findByPageTepmlateId(String template_id);
}
