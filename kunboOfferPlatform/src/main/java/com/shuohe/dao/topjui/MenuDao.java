package com.shuohe.dao.topjui;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.topjui.Menu;

public interface MenuDao extends JpaRepository<Menu, Long>{
	Menu findById(int id);
}
