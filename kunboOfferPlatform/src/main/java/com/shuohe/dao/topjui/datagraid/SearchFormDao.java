package com.shuohe.dao.topjui.datagraid;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.datagraid.SearchForm;

public interface SearchFormDao extends JpaRepository<SearchForm, Long> {

	@Modifying
	@Transactional
	@Query("delete SearchForm where template_id= ?1")
	public int deleteByPageTepmlateId(String template_id);
	
	@Query("from SearchForm where template_id= ?1")
	public List<SearchForm> findByPageTepmlateId(String template_id);
}
