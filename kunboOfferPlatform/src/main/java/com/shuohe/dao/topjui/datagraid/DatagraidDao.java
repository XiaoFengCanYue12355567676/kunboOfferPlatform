package com.shuohe.dao.topjui.datagraid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.datagraid.Datagraid;

public interface DatagraidDao extends JpaRepository<Datagraid, Long> {
	
	/** 
	* @Description: 根据id查找表格信息 
	* @Title: findById 
	* @param template_id
	* @return Datagraid
	* @author qin
	* @date 2018年8月1日上午10:53:26
	*/ 
	@Query("from Datagraid where uuid= ?1")
	public Datagraid findByUuid(String uuid);
	
	
	@Modifying
	@Transactional
	@Query("delete Datagraid where uuid= ?1")
	public int deleteByUuid(String uuid);
}
