package com.shuohe.dao.topjui.datagraid;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.datagraid.DatagraidColumn;

public interface DatagraidColumnDao  extends JpaRepository<DatagraidColumn, Long>{
	
	
	@Query("from DatagraidColumn where template_id= ?1")
	public List<DatagraidColumn> findByPageTepmlateId(String template_id);
	
	
	@Modifying
	@Transactional
	@Query("delete DatagraidColumn where template_id= ?1")
	public int deleteByPageTepmlateId(String template_id);
}
