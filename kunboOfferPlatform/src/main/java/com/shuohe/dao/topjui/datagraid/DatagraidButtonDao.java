package com.shuohe.dao.topjui.datagraid;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.datagraid.DatagraidButton;

public interface DatagraidButtonDao extends JpaRepository<DatagraidButton, Long> {
	
	@Modifying
	@Transactional
	@Query("delete DatagraidButton where template_id= ?1")
	public int deleteByPageTepmlateId(String template_id);
	
	@Query("from DatagraidButton where template_id= ?1")
	public List<DatagraidButton> findByPageTepmlateId(String template_id);
}
