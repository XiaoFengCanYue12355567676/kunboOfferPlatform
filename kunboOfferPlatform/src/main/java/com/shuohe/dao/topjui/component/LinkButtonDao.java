package com.shuohe.dao.topjui.component;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.topjui.component.LinkButton;

public interface LinkButtonDao extends JpaRepository<LinkButton, Long> {
	
	@Modifying
	@Transactional
	@Query("delete LinkButton where template_id= ?1")
	public int deleteByPageTepmlateId(String template_id);
	
	@Query("from LinkButton where template_id= ?1")
	public List<LinkButton> findByPageTepmlateId(String template_id);
}
