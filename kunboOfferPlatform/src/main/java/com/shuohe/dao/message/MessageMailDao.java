package com.shuohe.dao.message;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.message.MessageMail;

public interface MessageMailDao extends JpaRepository<MessageMail, Long> {

}
