package com.shuohe.dao.message;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.message.MessageSms;

public interface MessageSmsDao extends JpaRepository<MessageSms, Long> {

}
