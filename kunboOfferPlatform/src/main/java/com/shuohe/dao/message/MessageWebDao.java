package com.shuohe.dao.message;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shuohe.entity.message.MessageWeb;

public interface MessageWebDao extends JpaRepository<MessageWeb, Long> {
	MessageWeb findById(int id);
}
