package com.shuohe.service.message;

import com.shuohe.entity.message.Message;
import com.shuohe.util.returnBean.ReturnBean;

/**
 * 消息中心
 * @author qin
 *
 */
public interface MessageServer {

	/**
	 * 创建消息
	 * @param template_id  消息模板ID
	 * @param user_list	用户ID列表
	 * @param msg	要传递消息的Bean
	 * @return
	 */
	public ReturnBean createMessage(Message msg);
	
	/**
	 * 用户读取网页版消息
	 * @param msg_id  	消息id
	 * @param recver_id	接收人id
	 * @return
	 */
	public ReturnBean readWebMessage(int msg_id,int recver_id);
	
	
	
	
}
