package com.shuohe.service.topjui.component;

import java.util.List;

import com.shuohe.entity.topjui.component.LinkButton;

public interface LinkButtonService {


	/** 
	* @Description: 获得某个模板下的所有按钮  
	* @Title: getByTempateId 
	* @param template_id
	* @return List<DatagraidButton>
	* @author qin
	* @date 2018年8月3日上午10:24:35
	*/ 
	public List<LinkButton> getByTempateId(String template_id);
}
