package com.shuohe.service.topjui.datagraid;

import java.util.List;

import com.shuohe.entity.topjui.datagraid.DatagraidButton;

public interface DatagraidButtonService {


	/** 
	* @Description: 获得某个模板下的所有按钮  
	* @Title: getByTempateId 
	* @param template_id
	* @return List<DatagraidButton>
	* @author qin
	* @date 2018年8月3日上午10:24:35
	*/ 
	public List<DatagraidButton> getByTempateId(String template_id);
}
