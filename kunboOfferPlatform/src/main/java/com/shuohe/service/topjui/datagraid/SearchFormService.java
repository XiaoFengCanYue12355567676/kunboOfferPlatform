package com.shuohe.service.topjui.datagraid;

import java.util.List;

import com.shuohe.entity.topjui.datagraid.SearchForm;

public interface SearchFormService {
	
	public List<SearchForm> getByTempateId(String template_id);
}
