package com.shuohe.service.topjui.datagraid;

import java.util.List;

import com.shuohe.entity.topjui.component.ComboTree;
import com.shuohe.entity.topjui.component.LinkButton;
import com.shuohe.entity.topjui.datagraid.Datagraid;
import com.shuohe.entity.topjui.datagraid.DatagraidColumn;
import com.shuohe.entity.topjui.datagraid.SearchForm;
import com.shuohe.util.returnBean.ReturnBean;

public interface DatagraidService {
	/** 
	* @Description: 保存表格及其表头 
	* @Title: saveDatagraidAndColumns 
	* @param datagraid
	* @param datagraidColumns
	* @return ReturnBean
	* @author qin
	* @date 2018年8月1日上午10:46:12
	*/ 
	public ReturnBean saveDatagraidAndColumns(Datagraid datagraid,List<DatagraidColumn> datagraidColumns
			,List<LinkButton> datagraidButtons,List<SearchForm> searchForm);
	
	
	/** 
	* @Description: 更新表格及其表头  
	* @Title: updateDatagraidAndColumns 
	* @param datagraid
	* @param datagraidColumns
	* @return ReturnBean
	* @author qin
	* @date 2018年8月2日上午9:55:34
	*/ 
	public ReturnBean updateDatagraidAndColumns(Datagraid datagraid,List<DatagraidColumn> datagraidColumns
			,List<LinkButton> datagraidButtons,List<SearchForm> searchForm);
	
	
	/** 
	* @Description: 删除表格及其表头  
	* @Title: deleteDatagraidAndColumns 
	* @param datagraid
	* @param datagraidColumns
	* @return ReturnBean
	* @author qin
	* @date 2018年8月2日下午3:15:23
	*/ 
	public ReturnBean deleteDatagraidAndColumns(String uuid);
	
	/** 
	* @Description: 根据id获取表格信息实体类 
	* @Title: getDatagraidById 
	* @param id
	* @return Datagraid
	* @author qin
	* @date 2018年8月1日上午10:54:51
	*/ 
	public Datagraid getDatagraidByUuid(String uuid);
}
