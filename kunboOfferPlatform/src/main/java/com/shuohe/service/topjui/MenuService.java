package com.shuohe.service.topjui;

import java.util.List;
import java.util.Map;

import com.shuohe.entity.topjui.Menu;

public interface MenuService {
	public boolean save(Menu menu);
	public boolean update(Menu menu);
	public boolean delete(Menu menu);
	public List<Map<String,Object>> getMenuByPid(String pid,String postation_id);
	public List<Menu> getMenuList(int parent_id);
	Menu findById(int id);
	public List<Menu> getMenuListForDe(int parent_id,int department_id);
}
