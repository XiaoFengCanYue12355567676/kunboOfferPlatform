package com.shuohe.service.topjui.datagraid;

import java.util.List;

import com.shuohe.entity.topjui.datagraid.DatagraidColumn;
import com.shuohe.util.returnBean.ReturnBeanJpa;

public interface DatagraidColumnService {
	/** 
	* @Description: 保存单条表格表头数据 
	* @Title: saveDatagraidColumn 
	* @param datagraidColumn
	* @return ReturnBeanJpa DatagraidColumn实体类
	* @author qin
	* @date 2018年8月1日上午10:15:20
	*/ 
	public ReturnBeanJpa saveDatagraidColumn(DatagraidColumn datagraidColumn);
	
	/** 
	* @Description: 根据模板名称获取所有表格表头信息 
	* @Title: findByPageTepmlateId 
	* @param template_id
	* @return List<DatagraidColumn>
	* @author qin
	* @date 2018年8月1日上午10:55:13
	*/ 
	public List<DatagraidColumn> findByPageTepmlateId(String template_id);
	
	
}
