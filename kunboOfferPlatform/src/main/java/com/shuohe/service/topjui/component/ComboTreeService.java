package com.shuohe.service.topjui.component;

import java.util.List;

import com.shuohe.entity.topjui.component.ComboTree;

public interface ComboTreeService {

	public List<ComboTree> getByTempateId(String template_id);
}
