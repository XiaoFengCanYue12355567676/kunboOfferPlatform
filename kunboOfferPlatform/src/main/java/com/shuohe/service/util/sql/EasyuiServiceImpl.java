package com.shuohe.service.util.sql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.shuohe.util.db.DbQuerySql;
import com.shuohe.util.easyuiBean.EasyUiCombobox;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;


@Service
public class EasyuiServiceImpl {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;


	private Logger loger = LoggerFactory.getLogger(EasyuiServiceImpl.class);
	
	/*********************************EasyUi前台组件接口***********************************/
	///////////////////////////////////////////////////// DataGrid ///////////////////////////////////////////////////
	public EasyUiDataGrid getEasyUiDataGrid(String table,String page,String row) throws SQLException
	{
		EasyUiDataGrid ret=null;
		
		String sql = "";						
		sql+=" SELECT * FROM "+table; 
		sql+=" ORDER BY "+table+".id DESC";	
		sql+=DbQuerySql.limit(page, row);
		
		String count_sql = "";	
		count_sql+=" SELECT * FROM "+table;
		
		ret = getEasyUiDataGrid(sql, count_sql);			
		return ret;
	}
	
	public EasyUiDataGrid getEasyUiDataGrid(String sql,String count_sql) 
	{
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		List<Map<String, Object>> list_len = jdbcTemplate.queryForList(count_sql);
		loger.info(sql);
		loger.info(count_sql);
		return new EasyUiDataGrid(list_len.size(),list);		
	}
	
	public EasyUiDataGrid getEasyUiTreeGrid(String sql,String count_sql) 
	{
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		System.out.println();
		for (Map<String, Object> map : list) {
			if("".equals(map.get("_parentId")) || null==map.get("_parentId")) {
				map.remove("_parentId");
			}
		}
		List<Map<String, Object>> list_len = jdbcTemplate.queryForList(count_sql);
		loger.info(sql);
		loger.info(count_sql);
		return new EasyUiDataGrid(list_len.size(),list);		
	}

	public  EasyUiCombobox getEasyUiCombobox(String sql) throws SQLException
	{
		loger.info("sql = "+sql);
		EasyUiCombobox e = new EasyUiCombobox();		
		ArrayList<Map<String, Object>> ret= (ArrayList<Map<String, Object>>) getEasyUiDataGridNoPage(sql);    
		for(Map<String, Object> m:ret)
		{
			e.add(m);
		}		
		return e; 
	}
	
	
	
	
	
	public List<Map<String,Object>> getEasyUiDataGridNoPage(String sql)
	{
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		return list;
	}
	
	////////////////////////////////////////////////// DataList ///////////////////////////////////////////////////////
	
	
	
	
}
