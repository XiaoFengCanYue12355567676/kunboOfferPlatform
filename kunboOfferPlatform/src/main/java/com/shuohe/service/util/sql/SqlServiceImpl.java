package com.shuohe.service.util.sql;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SqlServiceImpl {

	@Autowired
    private JdbcTemplate jdbcTemplate;

	/**
	 * 检索一条数据
	 * @author    秦晓宇
	 * @date      2017年10月31日 下午3:20:19 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public Map<String,Object> getDataForOneItem(String sql) throws SQLException
	{ 
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		Map<String, Object> r = new HashMap<String, Object>();
		for(Map<String, Object> m:list)
		{
			r=m;
		}
		return r;
	}
	

	/**
	 * 检索多条数组数据
	 * @author    秦晓宇
	 * @date      2017年10月31日 下午3:21:00 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getDataForList(String sql) throws SQLException
	{
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		return list;
	}
	
	
	/**
	 * 修改数据
	 * @author    秦晓宇
	 * @date      2017年10月31日 下午3:21:45 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public int executeUpdate(String sql) throws SQLException
	{
		return jdbcTemplate.update(sql);
	}
	
}
