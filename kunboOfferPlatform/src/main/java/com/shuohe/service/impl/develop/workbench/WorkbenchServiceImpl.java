package com.shuohe.service.impl.develop.workbench;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.develop.workbench.WorkbenchDao;
import com.shuohe.entity.develop.workbench.Workbench;
import com.shuohe.service.develop.workbench.WorkbenchService;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public class WorkbenchServiceImpl implements WorkbenchService{

	@Autowired
	private WorkbenchDao workbenchDao; 
	
	@Override
	public ReturnBean save(Workbench workbench) {
		// TODO Auto-generated method stub
		try
		{
			workbenchDao.save(workbench);
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
		return new ReturnBean(true,"");
	}

	@Override
	public ReturnBean update(Workbench workbench) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnBean deleteByPostatisonId(int id) {
		// TODO Auto-generated method stub
		try
		{
			workbenchDao.deleteByPid(id);
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
		return new ReturnBean(true,"");
	}

	@Override
	public Workbench findByPostation(int postation) {
		return workbenchDao.findByPostation(postation);
	}
	
}
