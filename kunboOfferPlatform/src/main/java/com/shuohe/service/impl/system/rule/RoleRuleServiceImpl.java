package com.shuohe.service.impl.system.rule;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.system.rule.RoleRuleDao;
import com.shuohe.entity.system.rule.RoleRule;
import com.shuohe.service.system.rule.RoleRuleService;
import com.shuohe.service.util.sql.EasyuiServiceImpl;
import com.shuohe.service.util.sql.SqlServiceImpl;
import com.shuohe.util.db.DbQuerySql;

@Service
public class RoleRuleServiceImpl implements RoleRuleService
{

	 @Resource
	 private EasyuiServiceImpl easyUiDto;
	
	@Autowired
	private RoleRuleDao roleRuleDao; 
	
	@Resource
	private SqlServiceImpl sqlServiceImpl;
	
	private Logger loger = LoggerFactory.getLogger(RoleRuleServiceImpl.class);
	 
	@Override
	public List<Map<String, Object>> getRulesStructsByPageId(String page_id) {
	    	String sql="";
	    	sql += " SELECT";
	    	sql += " 	*";
	    	sql += " FROM";
	    	sql += " 	db_system_topjui_rule_struct";
	    	sql += " WHERE";
	    	sql += DbQuerySql.AEqualToB("db_system_topjui_rule_struct.page_id ", page_id);
	    	return easyUiDto.getEasyUiDataGridNoPage(sql);
	}

	@Override
	public List<Map<String, Object>> getRoleRuleByPostAndPageid(String page_id, String position_id) {
	  	String sql="";
	    	sql += " SELECT";
	    	sql += " 	db_system_topjui_rule.id,";
	    	sql += " 	db_system_topjui_rule.is_use,";
	    	sql += " 	db_system_topjui_rule.postation_id,";
	    	sql += " 	db_system_topjui_rule.rule_struct_id,";
	    	sql += " 	db_system_topjui_rule_struct.component_id,";
	    	sql += " 	db_system_topjui_rule_struct.text,";
	    	sql += " 	db_system_topjui_rule_struct.component_style";
	    	sql += " FROM";
	    	sql += " 	db_system_topjui_rule";
	    	sql += " left join db_system_topjui_rule_struct on db_system_topjui_rule_struct.id = db_system_topjui_rule.rule_struct_id";
	    	sql += " WHERE";
	    	sql += DbQuerySql.AEqualToB("db_system_topjui_rule.postation_id", position_id);
	    	sql += DbQuerySql.andAEqualToB("db_system_topjui_rule_struct.page_id", page_id);
	    	return easyUiDto.getEasyUiDataGridNoPage(sql);
	}

	@Override
	public boolean save(RoleRule roleRule) {
		// TODO Auto-generated method stub
		roleRuleDao.save(roleRule);
		return true;
	}

	@Override
	public boolean delete(RoleRule roleRule) {
		// TODO Auto-generated method stub
		roleRuleDao.delete(roleRule);
		return true;
	}
	
	/**
	 * 获得职位页面权限
	 * @author    秦晓宇
	 * @date      2017年12月14日 下午4:24:41 
	 * @param uri
	 * @param position_id
	 * @return
	 * @throws SQLException 
	 */
	public List<Map<String,Object>> getPostationPageRule(String uri,int position_id) throws SQLException
	{		
		String sql = "";
		sql += " SELECT";
		sql += " 	db_system_topjui_rule.id,";
		sql += " 	db_system_topjui_rule.is_use,";
		sql += " 	db_system_topjui_rule_struct.component_id,";
		sql += " 	db_system_topjui_rule_struct.component_style";
		sql += " FROM";
		sql += " 	db_system_topjui_menu";
		sql += " left join db_system_topjui_rule_struct on db_system_topjui_rule_struct.page_id = db_system_topjui_menu.id";
		sql += " left join db_system_topjui_rule on db_system_topjui_rule.rule_struct_id = db_system_topjui_rule_struct.id";
		sql += " WHERE";
		sql += DbQuerySql.AEqualToB("db_system_topjui_rule.postation_id", position_id+"");
		sql += DbQuerySql.andALikeB("db_system_topjui_menu.url", uri);
//		loger.info("sql = "+sql);
		System.out.println("sql = "+sql);
		return sqlServiceImpl.getDataForList(sql);
	}
	
	
}