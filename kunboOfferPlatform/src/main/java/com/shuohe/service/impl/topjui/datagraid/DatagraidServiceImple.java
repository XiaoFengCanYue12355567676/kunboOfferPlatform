package com.shuohe.service.impl.topjui.datagraid;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.dao.topjui.component.ComboTreeDao;
import com.shuohe.dao.topjui.component.LinkButtonDao;
import com.shuohe.dao.topjui.datagraid.DatagraidColumnDao;
import com.shuohe.dao.topjui.datagraid.DatagraidDao;
import com.shuohe.dao.topjui.datagraid.SearchFormDao;
import com.shuohe.entity.topjui.component.ComboTree;
import com.shuohe.entity.topjui.component.LinkButton;
import com.shuohe.entity.topjui.datagraid.Datagraid;
import com.shuohe.entity.topjui.datagraid.DatagraidColumn;
import com.shuohe.entity.topjui.datagraid.SearchForm;
import com.shuohe.service.topjui.datagraid.DatagraidService;
import com.shuohe.util.returnBean.ReturnBean;

/**
 * 表格服务实体
 * @author qin
 *
 */
@Service
public class DatagraidServiceImple implements DatagraidService{

	@Autowired
	DatagraidColumnDao datagraidColumnDao;
	
	@Autowired
	DatagraidDao datagraidDao;
	
	@Autowired
	LinkButtonDao datagraidButtonDao;

//	@Autowired
//	ComboTreeDao comboTreeDao;
	
	@Autowired
	SearchFormDao searchFormDao;
	
	
	@Override
	@Modifying
	@Transactional
	public ReturnBean saveDatagraidAndColumns(Datagraid datagraid, List<DatagraidColumn> datagraidColumns
			,List<LinkButton> datagraidButtons,List<SearchForm> searchForm) {
		Datagraid datagraid_temp;
		try {
			datagraid_temp = datagraidDao.save(datagraid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格信息失败");
		}
		
		try {
			List<DatagraidColumn> datagraidColumns_temp = new ArrayList<DatagraidColumn>();
			for(DatagraidColumn d:datagraidColumns)
			{
				DatagraidColumn c = d;
				c.setTemplate_id(datagraid_temp.getUuid());
				datagraidColumns_temp.add(c);
			}
			datagraidColumnDao.save(datagraidColumns_temp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格表头信息失败");
		}
		
		try {
			List<LinkButton> datagraidButton_temp = new ArrayList<LinkButton>();
			for(LinkButton d:datagraidButtons)
			{
				LinkButton c = d;
				c.setTemplate_id(datagraid_temp.getUuid());
				datagraidButton_temp.add(c);
			}
			datagraidButtonDao.save(datagraidButton_temp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格按钮信息失败");
		}
				
		try {
			List<SearchForm> comboTree_temp = new ArrayList<SearchForm>();
			for(SearchForm d:searchForm)
			{
				SearchForm c = d;
				c.setTemplate_id(datagraid_temp.getUuid());
				comboTree_temp.add(c);
			}
			searchFormDao.save(comboTree_temp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存检索条件失败");
		}		
			
		return new ReturnBean(true,"保存信息陈宫");
	}

	@Override
	public Datagraid getDatagraidByUuid(String uuid) {		
		return datagraidDao.findByUuid(uuid);
	}

	@Override
	@Modifying
	@Transactional
	public ReturnBean updateDatagraidAndColumns(Datagraid datagraid, List<DatagraidColumn> datagraidColumns
			,List<LinkButton> datagraidButtons,List<SearchForm> searchForm) {
		Datagraid datagraid_temp;
		try {
			datagraid_temp = datagraidDao.save(datagraid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格信息失败");
		}
		
		try {
			datagraidColumnDao.deleteByPageTepmlateId(datagraid_temp.getUuid());
			
			List<DatagraidColumn> datagraidColumns_temp = new ArrayList<DatagraidColumn>();
			for(DatagraidColumn d:datagraidColumns)
			{
				DatagraidColumn c = d;
				c.setTemplate_id(datagraid_temp.getUuid());
				datagraidColumns_temp.add(c);
			}
			datagraidColumnDao.save(datagraidColumns_temp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格表头信息失败");
		}
		
		try {
			datagraidButtonDao.deleteByPageTepmlateId(datagraid_temp.getUuid());
			
			List<LinkButton> datagraidButton_temp = new ArrayList<LinkButton>();
			for(LinkButton d:datagraidButtons)
			{
				LinkButton c = d;
				c.setTemplate_id(datagraid_temp.getUuid());
				datagraidButton_temp.add(c);
			}
			datagraidButtonDao.save(datagraidButton_temp);
			
			
			try {
				searchFormDao.deleteByPageTepmlateId(datagraid_temp.getUuid());
				
				List<SearchForm> comboTree_temp = new ArrayList<SearchForm>();
				for(SearchForm d:searchForm)
				{
					SearchForm c = d;
					c.setTemplate_id(datagraid_temp.getUuid());
					comboTree_temp.add(c);
				}
				searchFormDao.save(comboTree_temp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ReturnBean(false,"保存检索条件失败");
			}		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"保存表格操作按钮信息失败");
		}
		
		
		return new ReturnBean(true,"保存信息成功");
	}

	@Override
	@Modifying
	@Transactional
	public ReturnBean deleteDatagraidAndColumns(String uuid) {
		try {
			datagraidDao.deleteByUuid(uuid);
			datagraidColumnDao.deleteByPageTepmlateId(uuid);
			datagraidButtonDao.deleteByPageTepmlateId(uuid);
			searchFormDao.deleteByPageTepmlateId(uuid);
			return new ReturnBean(true,"删除成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"删除失败");
			
		}
	}

}
