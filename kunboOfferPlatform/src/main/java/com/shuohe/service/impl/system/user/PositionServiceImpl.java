package com.shuohe.service.impl.system.user;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.shuohe.dao.system.rule.RoleRuleDao;
import com.shuohe.dao.system.rule.RuleStructDao;
import com.shuohe.dao.system.user.PositionDao;
import com.shuohe.entity.system.rule.RoleRule;
import com.shuohe.entity.system.rule.RuleStruct;
import com.shuohe.entity.system.user.Position;
import com.shuohe.service.system.user.PositionService;
import com.shuohe.util.json.Json;
import com.tyteam.SpacApplication;

@Service
public class PositionServiceImpl implements PositionService{

	@Autowired
	private PositionDao positionDao;
	
	@Autowired
	private RuleStructDao ruleStructDao;
	
	@Autowired
	private RoleRuleDao roleRuleDao; 
	
	private Logger logger = LoggerFactory.getLogger(PositionServiceImpl.class);
	
	@Override
	public List<Position> list() {
		// TODO Auto-generated method stub
		return positionDao.findAll();
	}

	@Override
	public Position save(Position position) {
		// TODO Auto-generated method stub
		
		logger.info("save");
		
		Position p = positionDao.save(position);
		
		//存储activiti的group
		String uuid = UUID.randomUUID().toString();
		
		positionDao.save(p);
		
		//向角色插入权限列表
		List<RuleStruct>  list = ruleStructDao.findAll();
		for(RuleStruct rs:list)
		{
			RoleRule r = new RoleRule();
			r.setId(0);
			r.setIs_use(false);
			r.setPostation_id(p.getId());
			r.setRule_struct_id(rs.getId());
			roleRuleDao.save(r);
		}
		return p;		
	}
	
	@Modifying
	@Transactional
	@Override
	public void delete(Position position) {
		// TODO Auto-generated method stub
		positionDao.delete(position);
		roleRuleDao.deleteBy_postation_id(position.getId());
	}

	@Override
	public Position findById(int id) {
		// TODO Auto-generated method stub
		return positionDao.findById(id);
	}

	@Override
	public Position update(Position position) {
		// TODO Auto-generated method stub		
		logger.debug("update");
		return positionDao.save(position);		
	}

	@Override
	public void page(PageQuery<Entity> pageQuery) {
		// TODO Auto-generated method stub
		
	}

}
