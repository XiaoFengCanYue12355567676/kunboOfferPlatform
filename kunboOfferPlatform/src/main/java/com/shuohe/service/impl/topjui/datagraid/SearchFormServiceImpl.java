package com.shuohe.service.impl.topjui.datagraid;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.datagraid.SearchFormDao;
import com.shuohe.entity.topjui.datagraid.SearchForm;
import com.shuohe.service.topjui.datagraid.SearchFormService;


@Service
public class SearchFormServiceImpl implements SearchFormService{

	
	@Autowired
	SearchFormDao searchFormDao;
	
	@Override
	public List<SearchForm> getByTempateId(String template_id) {
		// TODO Auto-generated method stub
		return searchFormDao.findByPageTepmlateId(template_id);
	}
	
	

}
