package com.shuohe.service.impl.topjui.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.component.ComboTreeDao;
import com.shuohe.entity.topjui.component.ComboTree;
import com.shuohe.service.topjui.component.ComboTreeService;

@Service
public class ComboTreeServiceImpl implements ComboTreeService{

	@Autowired
	ComboTreeDao comboTreeDao;
	
	@Override
	public List<ComboTree> getByTempateId(String template_id) {
		// TODO Auto-generated method stub
		return comboTreeDao.findByPageTepmlateId(template_id);		
	}

}
