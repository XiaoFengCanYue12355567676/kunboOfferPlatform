package com.shuohe.service.impl.topjui.datagraid;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.datagraid.DatagraidColumnDao;
import com.shuohe.entity.topjui.datagraid.DatagraidColumn;
import com.shuohe.service.topjui.datagraid.DatagraidColumnService;
import com.shuohe.util.returnBean.ReturnBeanJpa;


/**
 * 表头表头操作服务执行类
 * @author qin
 *
 */
@Service
public class DatagraidColumnServiceImple implements DatagraidColumnService{

	@Autowired
	DatagraidColumnDao datagraidColumnDao;
	
	
	@Override
	public ReturnBeanJpa saveDatagraidColumn(DatagraidColumn datagraidColumn) {
		if(datagraidColumn == null) return new ReturnBeanJpa(false,"存储的表头数据为空",null);
		return new ReturnBeanJpa(true,"存储的表头数据为空",datagraidColumnDao.save(datagraidColumn));
	}


	@Override
	public List<DatagraidColumn> findByPageTepmlateId(String template_id) {
		return datagraidColumnDao.findByPageTepmlateId(template_id);
	}

	
}
