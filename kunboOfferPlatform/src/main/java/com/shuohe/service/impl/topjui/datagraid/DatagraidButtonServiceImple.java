package com.shuohe.service.impl.topjui.datagraid;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.datagraid.DatagraidButtonDao;
import com.shuohe.dao.topjui.datagraid.DatagraidColumnDao;
import com.shuohe.entity.topjui.datagraid.Datagraid;
import com.shuohe.entity.topjui.datagraid.DatagraidButton;
import com.shuohe.service.topjui.datagraid.DatagraidButtonService;

@Service
public class DatagraidButtonServiceImple implements DatagraidButtonService{

	@Autowired
	DatagraidButtonDao datagraidButtonDao;
	
	@Override
	public List<DatagraidButton> getByTempateId(String template_id) {
		return datagraidButtonDao.findByPageTepmlateId(template_id);
	}

}
