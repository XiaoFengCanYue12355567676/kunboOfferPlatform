package com.shuohe.service.impl.topjui.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.component.LinkButtonDao;
import com.shuohe.dao.topjui.datagraid.DatagraidColumnDao;
import com.shuohe.entity.topjui.component.LinkButton;
import com.shuohe.entity.topjui.datagraid.Datagraid;
import com.shuohe.service.topjui.component.LinkButtonService;

@Service
public class LinkButtonServiceImple implements LinkButtonService{

	@Autowired
	LinkButtonDao datagraidButtonDao;
	
	@Override
	public List<LinkButton> getByTempateId(String template_id) {
		return datagraidButtonDao.findByPageTepmlateId(template_id);
	}

}
