package com.shuohe.service.impl.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.dao.message.MessageWebDao;
import com.shuohe.entity.message.Message;
import com.shuohe.entity.message.MessageWeb;
import com.shuohe.service.message.MessageServer;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public class MessageServerImpl implements MessageServer{
	
	
	@Autowired
	private MessageWebDao messageWebDao; 
	
	@Transactional
	@Override
	public ReturnBean createMessage(Message msg) {
		
		try 
		{	
			//添加邮箱的消息提醒
			if(msg.getMsg_mail()!=null)
			{
				
			}
			//添加短信的消息提醒
			if(msg.getMsg_sms()!=null)
			{
				
			}
			//添加web的消息提醒
			if(msg.getMsg_web()!=null)
			{				
				for(MessageWeb web_msg:msg.getMsg_web())
				{
					messageWebDao.save(web_msg);
				}
			}		
		}catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
		return new ReturnBean(true,null);
	}


	@Override
	public ReturnBean readWebMessage(int msg_id, int recver_id) {
		MessageWeb messageWeb = messageWebDao.findById(msg_id);
		if(messageWeb == null)
			return new ReturnBean(false,"没有对应的消息ID");
		if(recver_id != messageWeb.getReciver_id())
			return new ReturnBean(false,"接收人ID错误，这条通知不是该接收人所有");
		
		//以上条件都通过了，则设置这条信息已读
		messageWeb.setRead(true);
		messageWebDao.save(messageWeb);
		return new ReturnBean(true,"更新通知的状态为已读");
	}
	
}
