package com.shuohe.service.impl.topjui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.shuohe.dao.topjui.MenuDao;
import com.shuohe.entity.topjui.Menu;
import com.shuohe.service.topjui.MenuService;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;

  
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean save(Menu menu) { 
		// TODO Auto-generated method stub
		menuDao.save(menu);
		return false;
	}

	@Override
	public boolean update(Menu menu) {
		// TODO Auto-generated method stub
		menuDao.save(menu);
		return false;
	}

	@Override
	public boolean delete(Menu menu) {
		// TODO Auto-generated method stub
		menuDao.delete(menu);
		return false;
	}

	@Override
	public List<Map<String, Object>> getMenuByPid(String pid, String postation_id) {
		String sql = "";
		sql += " SELECT";
		sql += " 	db_system_topjui_menu.icon_cls as iconCls,";
		sql += " 	db_system_topjui_menu.id,";
		sql += " 	db_system_topjui_menu.`name`,";
		sql += " 	db_system_topjui_menu.pid,";
		sql += " 	db_system_topjui_menu.state,";
		sql += " 	db_system_topjui_menu.text,";
		sql += " 	db_system_topjui_menu.url";
		sql += " FROM";
		sql += " 	db_system_topjui_menu";
		sql += " left join db_system_topjui_rule_struct on db_system_topjui_rule_struct.page_id = db_system_topjui_menu.id";
		sql += " left join db_system_topjui_rule on db_system_topjui_rule.rule_struct_id = db_system_topjui_rule_struct.id";
		sql += " WHERE";
		sql += " 	db_system_topjui_menu.pid = "+pid;
		sql += " 	AND db_system_topjui_rule_struct.component_style = 'page' ";
		sql += " 	AND db_system_topjui_rule.is_use = '1'";
		sql += " 	AND db_system_topjui_rule.postation_id = "+postation_id;
		sql += " 	group by db_system_topjui_menu.id";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		return list;
	}

	@Override
	public List<Menu> getMenuList(int parent_id) {
		// TODO Auto-generated method stub
		 List<Menu> list = menuDao.findAll();
		 return getChildsManyGroup(list,parent_id);
	}
	public List<Menu> getChildsManyGroup(List<Menu> list,int pid)
	{  
		List<Menu> arr = new ArrayList<Menu>();  
		for(Menu location : list){  
		    if(pid == location.getPid()){  
		        location.setChildren(getChildsManyGroup(list, location.getId()));    
		        arr.add(location);   
		    }  
		}  
		return arr;  
	}

	@Override
	public Menu findById(int id) {
		// TODO Auto-generated method stub
		return menuDao.findById(id);
	}

	@Override
	public List<Menu> getMenuListForDe(int parent_id, int department_id) {
		 		 // TODO Auto-generated method stub
				 List<Menu> list = menuDao.findAll();
				 return getChildsManyGroups(list,parent_id,department_id);
	} 
	public List<Menu> getChildsManyGroups(List<Menu> list,int pid,int department_id)
	{  
		List<Menu> arr = new ArrayList<Menu>();  
		for(Menu location : list){  
		    if(pid == location.getId() && department_id == location.getPid()){  
		        location.setChildren(getChildsManyGroups(list, location.getId(),location.getPid()));    
		        arr.add(location);   
		    }  
		}  
		return arr;  
	}
}
