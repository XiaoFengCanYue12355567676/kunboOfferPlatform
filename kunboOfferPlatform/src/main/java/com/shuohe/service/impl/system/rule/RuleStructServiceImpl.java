package com.shuohe.service.impl.system.rule;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.dao.system.rule.RoleRuleDao;
import com.shuohe.dao.system.rule.RuleStructDao;
import com.shuohe.dao.system.user.PositionDao;
import com.shuohe.dao.system.user.UserDao;
import com.shuohe.entity.system.rule.RoleRule;
import com.shuohe.entity.system.rule.RuleStruct;
import com.shuohe.entity.system.user.Position;
import com.shuohe.service.system.rule.RuleStructService;

@Service
public class RuleStructServiceImpl implements RuleStructService{

	@Autowired
	private RuleStructDao ruleStructDao;
	
	@Autowired
	private RoleRuleDao roleRuleDao; 
	
	@Autowired
	private PositionDao positionDao;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addRuleStructForAllRole(RuleStruct ruleStruct) throws Exception {
		// TODO Auto-generated method stub
		ruleStructDao.save(ruleStruct);
		List<Position> list = positionDao.findAll();
		for(Position p:list)
		{
			RoleRule r = new RoleRule();
			r.setId(0);
			r.setIs_use(false);
			r.setPostation_id(p.getId());
			r.setRule_struct_id(ruleStruct.getId());
			roleRuleDao.save(r);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRuleStructForAllRole(RuleStruct ruleStruct) throws Exception {
		// TODO Auto-generated method stub
		ruleStructDao.save(ruleStruct);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteRuleStructForAllRole(RuleStruct ruleStruct) throws Exception {
		// TODO Auto-generated method stub
		ruleStructDao.delete(ruleStruct);
		roleRuleDao.deleteBy_rule_struct_id(ruleStruct.getId());		
	}

	
	@Override
	public boolean save(RuleStruct ruleStruct) {
		// TODO Auto-generated method stub
		ruleStructDao.save(ruleStruct);
		return true;
	}

	@Override
	public boolean delete(RuleStruct ruleStruct) {
		// TODO Auto-generated method stub
		ruleStructDao.delete(ruleStruct);
		return true;
	}
	
	
}
