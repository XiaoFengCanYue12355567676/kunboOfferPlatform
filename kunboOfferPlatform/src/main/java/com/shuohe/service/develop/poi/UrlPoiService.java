package com.shuohe.service.develop.poi;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.develop.poi.UrlPoi;
import com.shuohe.entity.develop.poi.UrlPoiHeader;
import com.shuohe.entity.develop.poi.UrlPoiIutputPara;

public interface UrlPoiService {
	
	UrlPoi findUrlById(int id);
	List<UrlPoiHeader> findUrlPoiHeaderByPid(int pid);
	List<UrlPoiIutputPara> findUrlPoiIutputParaByPid(int pid);
	List<UrlPoi> getUrlPoiByPid(int pid);
	
	/**
	 * 新增POI条目、及输入参数、输出参数
	 * @param urlPoi
	 * @param list
	 * @param urlPoiIutputPara_list
	 * @return
	 */
	@Modifying
	@Transactional
	public void save(UrlPoi urlPoi,ArrayList<UrlPoiHeader> list,ArrayList<UrlPoiIutputPara> urlPoiIutputPara_list) throws Exception;
	/**
	 * 更新POI条目、及输入参数、输出参数
	 * @param urlPoi
	 * @param list
	 * @param urlPoiIutputPara_list
	 * @return
	 */
	@Modifying
	@Transactional
	public void update(UrlPoi urlPoi,ArrayList<UrlPoiHeader> list,ArrayList<UrlPoiIutputPara> urlPoiIutputPara_list) throws Exception;
	
	/**
	 * 删除POI及相关的输入参数与输出参数
	 * @param urlPoi
	 * @return
	 */
	@Modifying
	@Transactional
	public void delete(UrlPoi urlPoi) throws Exception ;
	/**
	 * 根据所属页面的id获得与页面功能相关联的所有的导出功能
	 * @param pid
	 * @return
	 */
	public List<UrlPoi> findByPid(int pid);
	/**
	 * 根据POI的英文名称，获得实体类
	 * @param name
	 * @return
	 */
	public UrlPoi findByName(String name);
	
	/**
	 * 由需要获取的Excel报表的英文简称，获取该报表的所有数据
	 * @param request
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getExcelDataByName(HttpServletRequest request,String name) throws SQLException;
}
