package com.shuohe.service.develop.workbench;

import org.springframework.stereotype.Service;

import com.shuohe.entity.develop.workbench.Workbench;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public interface WorkbenchService {
	public ReturnBean save(Workbench workbench);
	public ReturnBean update(Workbench workbench);
	public ReturnBean deleteByPostatisonId(int id);
	
	public Workbench findByPostation(int postation);
}
