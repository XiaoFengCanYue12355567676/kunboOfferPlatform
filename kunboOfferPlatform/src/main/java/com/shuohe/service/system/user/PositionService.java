package com.shuohe.service.system.user;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.shuohe.entity.system.user.Position;

public interface PositionService {
	List<Position> list();
	Position save(Position p);
	
	@Modifying
	@Transactional
	void delete(Position p);
	Position findById(int id);
	Position update(Position p);
	void page(PageQuery<Entity> pageQuery);
}
