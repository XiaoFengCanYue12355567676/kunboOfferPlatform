package com.shuohe.service.system.user;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.shuohe.entity.system.user.Department;
import com.shuohe.util.returnBean.ReturnBean;

public interface DepartmentService {
	public void save(Department department);
	public void delete(Department department);
	public List<Department> getAll(int pid);
	public Department getOne(int id);
	
	
	/** 
	* @Description: 根据部门Id删除部门，并且将人员放置于根目录下 
	* @Title: deleteDepartmentById 
	* @param id
	* @return ReturnBean
	* @author qin
	* @throws Exception 
	* @date 2018年7月31日上午10:52:49
	*/ 
	@Modifying
	@Transactional
	public ReturnBean deleteDepartmentById(int id);
}
