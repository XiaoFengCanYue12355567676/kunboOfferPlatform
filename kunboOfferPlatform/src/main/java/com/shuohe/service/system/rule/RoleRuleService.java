package com.shuohe.service.system.rule;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.shuohe.entity.system.rule.RoleRule;

public interface RoleRuleService
{
	/**
	 * 获取某个页面下的控件权限状况
	 * @param page_id
	 * @return
	 */
	List<Map<String,Object>> getRulesStructsByPageId(String page_id);
	
	/**
	 * 根据用户职位和打开页面，获取该用户可以操作的组件权限
	 * @param page_id
	 * @param position_id
	 * @return
	 */
	List<Map<String,Object>> getRoleRuleByPostAndPageid(String page_id,String position_id);
	
	List<Map<String,Object>> getPostationPageRule(String uri,int position_id) throws SQLException;
	
	boolean save(RoleRule roleRule);
	boolean delete(RoleRule roleRule);
}