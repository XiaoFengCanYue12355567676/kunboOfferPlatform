package com.shuohe.service.system.rule;

import com.shuohe.entity.system.rule.RoleRule;
import com.shuohe.entity.system.rule.RuleStruct;

public interface RuleStructService {
	/**
	 * 向所有角色添加权限
	 * @param ruleStruct
	 * @return
	 * @throws Exception 
	 */
	void addRuleStructForAllRole(RuleStruct ruleStruct) throws Exception;
	/**
	 * 修改权限结构
	 * @param ruleStruct
	 * @return
	 */
	void updateRuleStructForAllRole(RuleStruct ruleStruct) throws Exception;
	
	/**
	 * 删除权限结构
	 * @param ruleStruct
	 * @return
	 */
	void deleteRuleStructForAllRole(RuleStruct ruleStruct) throws Exception;
	
	
	public boolean delete(RuleStruct ruleStruct);
	public boolean save(RuleStruct ruleStruct);
}
