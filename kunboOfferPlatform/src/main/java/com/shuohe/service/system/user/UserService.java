package com.shuohe.service.system.user;

import java.util.List;

import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.shuohe.entity.system.user.User;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;

public interface UserService {
	public boolean save(User user);
	public boolean update(User user);	
	public boolean delete(User user);
	public User check(String name,String password);
	public EasyUiDataGrid getUserByDepartment(String department,String rows,String page);
	public User findById(int id);
	User findByActivitiUUID(String activiti_uuid);
	List<User> findByPositionId(int position_id);
	List<User> findAll();

	/**
	 * 检测用户是否存在
	 * @param user_id
	 * @return ReturnBean(result  true:存在；false:不存在)；
	 */
	public ReturnBean isUserExist(int id);
	
	
	/** 
	* @Description: 根据用户的注册名判断用户是否重复 
	* @Title: isUserExist 
	* @param name
	* @return ReturnBean (result  true:存在；false:不存在)；
	* @author qin
	* @date 2018年7月31日下午12:04:08
	*/ 
	public ReturnBean isUserExist(String name);
	
	
	public class DepTmp
	{
		int id;	
		int parent_id;				/**父id*/
		String text;				/**组织名称*/	
		boolean isDmp;
		List<DepTmp> children;
		public DepTmp(int id, int parent_id, String text, boolean isDmp, List<DepTmp> children) {
			super();
			this.id = id;
			this.parent_id = parent_id;
			this.text = text;
			this.isDmp = isDmp;
			this.children = children;
		}		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getParent_id() {
			return parent_id;
		}
		public void setParent_id(int parent_id) {
			this.parent_id = parent_id;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public boolean isDmp() {
			return isDmp;
		}
		public void setDmp(boolean isDmp) {
			this.isDmp = isDmp;
		}
		public List<DepTmp> getChildren() {
			return children;
		}
		public void setChildren(List<DepTmp> children) {
			this.children = children;
		}		
	}
	
	public List<DepTmp> getEasyUiTreeDepAndUser();
	
	public List<DepTmp> getEasyUiTreeDepAndUser(int department_id);
	
	public User findByName(String name);
	
	public void page(PageQuery<Entity> query);
	
	public void page1(PageQuery<Entity> query);
}
