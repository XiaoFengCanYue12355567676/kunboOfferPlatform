package com.shuohe.controller.develop.pageTemplate;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shuohe.entity.topjui.component.ComboTree;
import com.shuohe.entity.topjui.component.LinkButton;
import com.shuohe.entity.topjui.datagraid.Datagraid;
import com.shuohe.entity.topjui.datagraid.DatagraidColumn;
import com.shuohe.entity.topjui.datagraid.SearchForm;
import com.shuohe.service.impl.topjui.component.ComboTreeServiceImpl;
import com.shuohe.service.impl.topjui.component.LinkButtonServiceImple;
import com.shuohe.service.impl.topjui.datagraid.DatagraidColumnServiceImple;
import com.shuohe.service.impl.topjui.datagraid.DatagraidServiceImple;
import com.shuohe.service.impl.topjui.datagraid.SearchFormServiceImpl;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.returnBean.ReturnBeanJpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 表格模板接口类
 * @author qin
 *
 */
@Controller
@RequestMapping("/develop/pagetemplate/*")  
public class PageTemplateController {
	
	
	// @Fields datagraidColumnService : 表格表头操作服务类
	@Resource
	private DatagraidColumnServiceImple datagraidColumnService;
	
	@Resource
	private DatagraidServiceImple datagraidService;
	
	@Resource
	private LinkButtonServiceImple datagraidButtonService;
	
	@Resource
	private SearchFormServiceImpl searchFormService;
	
	
	private Logger logger = LoggerFactory.getLogger(PageTemplateController.class);
	
	/** 
	* @Description: 保存一条数据表表头信息 
	* @Title: saveDatagraidColumn 
	* @param datagraidColumn
	* @return ReturnBean
	* @author qin
	* @date 2018年8月1日上午10:05:36
	*/ 
	@RequestMapping(value="saveDatagraidColumn.do")  
    public @ResponseBody ReturnBeanJpa saveDatagraidColumn(@RequestParam("data") DatagraidColumn data){
		return datagraidColumnService.saveDatagraidColumn(data);
	}
	
	/** 
	* @Description: 根据模板编号获取模板表头信息 
	* @Title: getColumnsByPageTemplateId 
	* @param data
	* @return ReturnBeanJpa
	* @author qin
	* @date 2018年8月1日上午10:18:51
	*/ 
	@RequestMapping(value="getColumnsByPageTemplateId.do")  
    public @ResponseBody List<DatagraidColumn> getColumnsByPageTemplateId(@RequestParam("uuid") String uuid){
		return datagraidColumnService.findByPageTepmlateId(uuid);
	}
	
	/** 
	* @Description: 一次性保存表格及其表头  
	* @Title: saveDatagraidAndColumns 
	* @param datagraid	
	* @param datagraidColumns
	* @return ReturnBeanJpa
	* @author qin
	* @date 2018年8月1日上午10:35:25
	*/ 
	@ResponseBody
	@RequestMapping(value="saveDatagraidAndColumns.do")  
    public ReturnBean saveDatagraidAndColumns(
    		@RequestParam("datagraid") String datagraid_str,
    		@RequestParam("datagraidColumns") String datagraidColumns_str,
    		@RequestParam("datagraidButtons") String datagraidButton_str,
    		@RequestParam("searchForms") String searchForm_str
    ){		
		Gson reGson = new Gson();
		Datagraid datagraid = reGson.fromJson(datagraid_str, new TypeToken<Datagraid>(){}.getType());
		ArrayList<DatagraidColumn> datagraidColumns = reGson.fromJson(datagraidColumns_str, new TypeToken<ArrayList<DatagraidColumn>>(){}.getType());	
		ArrayList<LinkButton> datagraidButton = reGson.fromJson(datagraidButton_str, new TypeToken<ArrayList<LinkButton>>(){}.getType());
		ArrayList<SearchForm> searchForm = reGson.fromJson(searchForm_str, new TypeToken<ArrayList<SearchForm>>(){}.getType());
		logger.info("isAutoRowHeight = "+ datagraid.isAutoRowHeight());
		return datagraidService.saveDatagraidAndColumns(datagraid, datagraidColumns,datagraidButton, searchForm);
	}
	/** 
	* @Description: 一次性更新表格及其表头   
	* @Title: updateDatagraidAndColumns 
	* @param datagraid_str
	* @param datagraidColumns_str
	* @return ReturnBean
	* @author qin
	* @date 2018年8月2日下午3:12:11
	*/ 
	@ResponseBody
	@RequestMapping(value="updateDatagraidAndColumns.do")  
    public ReturnBean updateDatagraidAndColumns(
    		@RequestParam("datagraid") String datagraid_str,
    		@RequestParam("datagraidColumns") String datagraidColumns_str,
    		@RequestParam("datagraidButtons") String datagraidButton_str,
    		@RequestParam("searchForms") String searchForm_str
    ){		
		Gson reGson = new Gson();
		Datagraid datagraid = reGson.fromJson(datagraid_str, new TypeToken<Datagraid>(){}.getType());
		ArrayList<DatagraidColumn> datagraidColumns = reGson.fromJson(datagraidColumns_str, new TypeToken<ArrayList<DatagraidColumn>>(){}.getType());		
		ArrayList<LinkButton> datagraidButton = reGson.fromJson(datagraidButton_str, new TypeToken<ArrayList<LinkButton>>(){}.getType());	
		ArrayList<SearchForm> searchForms = reGson.fromJson(searchForm_str, new TypeToken<ArrayList<SearchForm>>(){}.getType());
		logger.info("isAutoRowHeight = "+ datagraid.isAutoRowHeight());
		return datagraidService.updateDatagraidAndColumns(datagraid, datagraidColumns,datagraidButton, searchForms);
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	private class DatagraidAndColumns
	{
		List<List<DatagraidColumn>> datagraidColumns = new ArrayList<List<DatagraidColumn>>();
		List<LinkButton> datagraidButtons = new ArrayList<LinkButton>();
		List<SearchForm> searchForm = new ArrayList<SearchForm>();
		Datagraid datagraid = new Datagraid();
	}
	
	/** 
	* @Description: 删除数据表格 
	* @Title: deleteDatagraidAndColumns 
	* @param uuid
	* @return ReturnBean
	* @author qin
	* @date 2018年8月2日下午3:19:52
	*/ 
	@ResponseBody
	@RequestMapping(value="deleteDatagraidAndColumns.do")  
    public ReturnBean deleteDatagraidAndColumns(
    		@RequestParam("uuid") String uuid){			
		return datagraidService.deleteDatagraidAndColumns(uuid);
	}
	
	
	/** 
	* @Description: 同时获得表格和表头信息 
	* @Title: getDatagraidAndColumns 
	* @param uuid
	* @return DatagraidAndColumns
	* @author qin
	* @date 2018年8月2日下午3:42:39
	*/ 
	@ResponseBody
	@RequestMapping(value="getDatagraidAndColumns.do")  
    public DatagraidAndColumns getDatagraidAndColumns(
    		@RequestParam("uuid") String uuid){		
		DatagraidAndColumns DatagraidAndColumns = new DatagraidAndColumns();
		DatagraidAndColumns.setDatagraid(datagraidService.getDatagraidByUuid(uuid));
		DatagraidAndColumns.datagraidColumns.add(datagraidColumnService.findByPageTepmlateId(uuid));
		DatagraidAndColumns.setDatagraidButtons(datagraidButtonService.getByTempateId(uuid));
		DatagraidAndColumns.setSearchForm(searchFormService.getByTempateId(uuid));
		return DatagraidAndColumns;
	}
	
	/** 
	* @Description: 根据id获取表格信息 
	* @Title: getDatagraidById 
	* @param uuid
	* @return Datagraid
	* @author qin
	* @date 2018年8月1日上午10:54:39
	*/ 
	@RequestMapping(value="getDatagraidById.do")  
    public @ResponseBody Datagraid getDatagraidById(@RequestParam("uuid") String uuid){
		return datagraidService.getDatagraidByUuid(uuid);
	}
	
	
	
}
