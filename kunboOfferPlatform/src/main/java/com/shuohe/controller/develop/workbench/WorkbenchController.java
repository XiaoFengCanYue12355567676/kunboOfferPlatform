package com.shuohe.controller.develop.workbench;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.entity.develop.workbench.Workbench;
import com.shuohe.service.impl.develop.workbench.WorkbenchServiceImpl;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/develop/workbench/*")  
public class WorkbenchController {
	
	@Resource
	private WorkbenchServiceImpl workbenchService;
	
	
	@Transactional
	@RequestMapping(value="save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response){		
		String data_ = request.getParameter("data").replaceAll("\"\"", "null");					
		Workbench workbench = (Workbench) Json.toObject(Workbench.class, data_);							
		return workbenchService.save(workbench);			
	}
	
	@RequestMapping(value="findByPostationId.do")  
    public @ResponseBody Workbench findByPostationId(HttpServletRequest request,HttpServletResponse response){		
		String _postation_id = request.getParameter("postation_id").replaceAll("\"\"", "null");			
		int postation_id;
		
		try {
			postation_id = Integer.parseInt(_postation_id);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
		Workbench workbench = workbenchService.findByPostation(postation_id);
		if(workbench == null)
		{
			workbench = new Workbench();
			workbench.setPostation_id(postation_id);
			workbench.setConfig_str("");
			workbenchService.save(workbench);
			return workbench;
		}		
		return workbenchService.findByPostation(postation_id);
	}
}
