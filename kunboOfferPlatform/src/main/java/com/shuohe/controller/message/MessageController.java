package com.shuohe.controller.message;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.entity.system.user.User;
import com.shuohe.service.message.MessageServer;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/msg/*")  
public class MessageController {

	@Resource
	private MessageServer messageServer;
	
	@RequestMapping(value="readWebMsg.do")  
    public @ResponseBody ReturnBean readWebMsg(HttpServletRequest request,HttpServletResponse response){
		String _msg_id=request.getParameter("msg_id");
		int msg_id = -1;
		User user = (User)request.getSession().getAttribute("user");
		if(user == null)
		{
			return new ReturnBean(false,"您还未登录，请先登录");
		}
		try
		{
			msg_id = Integer.parseInt(_msg_id);
		}
		catch(NumberFormatException e)
		{
			e.printStackTrace();
			return new ReturnBean(false,"通讯错误!(消息编号传递错误)");
		}
		return messageServer.readWebMessage(msg_id, user.getId());		
	}
	
}
