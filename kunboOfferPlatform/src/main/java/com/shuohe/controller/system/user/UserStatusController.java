package com.shuohe.controller.system.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.dao.system.user.UserStatusDao;
import com.shuohe.entity.system.user.UserStatus;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/system/user/userStatus/*")  
public class UserStatusController {
	
	@Autowired
	private UserStatusDao userStatusDao;
	
	
    @RequestMapping(value="getAll.do")  
    public @ResponseBody List<UserStatus> getAll(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		return userStatusDao.findAll();
    }
    
    @RequestMapping(value="save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		UserStatus userStatus = (UserStatus) Json.toObject(UserStatus.class, data);
    		userStatusDao.save(userStatus);
    		return new ReturnBean(true,"");
    }
    
    @RequestMapping(value="delete.do")  
    public @ResponseBody ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		UserStatus userStatus = (UserStatus) Json.toObject(UserStatus.class, data);
    		userStatusDao.delete(userStatus);
    		return new ReturnBean(true,"");
    }
    
}
