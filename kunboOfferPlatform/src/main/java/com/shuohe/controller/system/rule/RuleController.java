package com.shuohe.controller.system.rule;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.entity.system.rule.RoleRule;
import com.shuohe.entity.system.rule.RuleStruct;
import com.shuohe.entity.system.user.User;
import com.shuohe.entity.topjui.Menu;
import com.shuohe.service.system.rule.RoleRuleService;
import com.shuohe.service.system.rule.RuleStructService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/system/rule/*")  
public class RuleController {
	
	@Resource
	private RuleStructService ruleStructService;
	
	@Resource
	private RoleRuleService roleRuleService;
	
	@RequestMapping(value="getRulesStructsByPageId.do")  
    public @ResponseBody List<Map<String,Object>> getRulesStructsByPageId(HttpServletRequest request,HttpServletResponse response){
		String page_id=request.getParameter("page_id");
		return roleRuleService.getRulesStructsByPageId(page_id);
	}
	
	@RequestMapping(value="addRuleStructForAllRole.do")  
    public @ResponseBody ReturnBean addRuleStructForAllRole(HttpServletRequest request,HttpServletResponse response){
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RuleStruct ruleStruct = (RuleStruct) Json.toObject(RuleStruct.class, json);
		try {
			ruleStructService.addRuleStructForAllRole(ruleStruct);
			return new ReturnBean(true,"为所有角色添加职位权限结构成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"！为所有角色添加职位权限结构失败");
		}
		
	}
	@RequestMapping(value="updateRuleStructForAllRole.do")  
    public @ResponseBody ReturnBean updateRuleStructForAllRole(HttpServletRequest request,HttpServletResponse response){  
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RuleStruct ruleStruct = (RuleStruct) Json.toObject(RuleStruct.class, json);
		try {
			ruleStructService.updateRuleStructForAllRole(ruleStruct);
			return new ReturnBean(true,"为所有角色修改职位权限结构成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ReturnBean(false,"！为所有角色修改职位权限结构失败");
		}
		
	}
	
	@RequestMapping(value="deleteRuleStructForAllRole.do")  
    public @ResponseBody ReturnBean deleteRuleStructForAllRole(HttpServletRequest request,HttpServletResponse response){  	   
		String classJson = request.getParameter("data").replaceAll("\"\"", "null");
	    	RuleStruct ruleStruct = (RuleStruct) Json.toObject(RuleStruct.class, classJson);
	    	try {
				ruleStructService.deleteRuleStructForAllRole(ruleStruct);
				return new ReturnBean(false,"！为所有角色删除职位权限结构失败");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return new ReturnBean(false,"！为所有角色删除职位权限结构失败");
			}
		
	}
	@RequestMapping(value="getRoleRuleByPostAndPageid.do")  
    public @ResponseBody List<Map<String,Object>> getRoleRuleByPostAndPageid(HttpServletRequest request,HttpServletResponse response){  
	    	String page_id = request.getParameter("page_id");    	
	    	String position_id = request.getParameter("position_id");  
	    	return roleRuleService.getRoleRuleByPostAndPageid(page_id, position_id);
	}
	@RequestMapping(value="roleRule/save.do")  
    public @ResponseBody ReturnBean saveRoleRule(HttpServletRequest request,HttpServletResponse response){  
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RoleRule roleRule = (RoleRule) Json.toObject(RoleRule.class, json);
		boolean ret = roleRuleService.save(roleRule);
		return new ReturnBean(ret,"");
	}
	@RequestMapping(value="roleRule/delete.do")  
    public @ResponseBody ReturnBean deleteRoleRule(HttpServletRequest request,HttpServletResponse response){  
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RoleRule roleRule = (RoleRule) Json.toObject(RoleRule.class, json);
		boolean ret = roleRuleService.delete(roleRule);
		return new ReturnBean(ret,"");
	}
	@RequestMapping(value="ruleStruct/save.do")  
    public @ResponseBody ReturnBean saveRuleStruct(HttpServletRequest request,HttpServletResponse response){  
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RuleStruct roleRule = (RuleStruct) Json.toObject(RuleStruct.class, json);
		boolean ret = ruleStructService.save(roleRule);
		return new ReturnBean(ret,"");
	}
	@RequestMapping(value="ruleStruct/delete.do")  
    public @ResponseBody ReturnBean deleteRuleStruct(HttpServletRequest request,HttpServletResponse response){  
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		RuleStruct roleRule = (RuleStruct) Json.toObject(RuleStruct.class, json);
		boolean ret = ruleStructService.delete(roleRule);
		return new ReturnBean(ret,"");
	}
	
	@RequestMapping(value="roleRule/getRoleRuleByPostation.do")  
    public @ResponseBody List<Map<String,Object>> getRoleRuleByPostation(HttpServletRequest request,HttpServletResponse response,HttpSession session)
	{  
		String uri = request.getParameter("uri").replaceAll("\"\"", "null");
		try {
			User user = (User) session.getAttribute("user");
			return roleRuleService.getPostationPageRule(uri, user.getPosition_id());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
}
