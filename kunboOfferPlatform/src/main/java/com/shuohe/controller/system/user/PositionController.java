package com.shuohe.controller.system.user;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.entity.system.user.Position;
import com.shuohe.service.system.user.PositionService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/system/user/position/*")  
public class PositionController {
	
	@Resource
	private PositionService positionService;
    
	@RequestMapping(value="getAll.do")  
    public @ResponseBody List<Position> getAll(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		return positionService.list();
    }
    
    @RequestMapping(value="save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		Position position = (Position) Json.toObject(Position.class, data);
    		positionService.save(position);
    		return new ReturnBean(true,"");
    }
    
    @RequestMapping(value="update.do")  
    public @ResponseBody ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		Position position = (Position) Json.toObject(Position.class, data);
    		positionService.update(position);
    		return new ReturnBean(true,"");
    }    
    
    @RequestMapping(value="delete.do")  
    public @ResponseBody ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		Position position = (Position) Json.toObject(Position.class, data);
    		positionService.delete(position);
    		return new ReturnBean(true,"");
    }
    
    @RequestMapping(value="findById.do")  
    public @ResponseBody Position findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String id = request.getParameter("id");
    		return positionService.findById(Integer.parseInt(id));
    }
	    
}
