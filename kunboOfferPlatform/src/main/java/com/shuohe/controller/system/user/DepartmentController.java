package com.shuohe.controller.system.user;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.dao.system.user.DepartmentDao;
import com.shuohe.entity.system.user.Department;
import com.shuohe.service.system.user.DepartmentService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@Controller  
@RequestMapping("/system/user/department/*")  
public class DepartmentController {
	

    @Resource 
    private DepartmentService departmentService;
    
	@Autowired
	private DepartmentDao departmentDao;
	
    @RequestMapping(value="getAll.do")  
    public @ResponseBody List<Department> login(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		return departmentService.getAll(0);
    }
    
    
    @RequestMapping(value="save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		Department userStatus = (Department) Json.toObject(Department.class, data);
    		departmentDao.save(userStatus);
    		return new ReturnBean(true,"");
    }
    
    @RequestMapping(value="delete.do")  
    public @ResponseBody ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String data = request.getParameter("data").replaceAll("\"\"", "null");
    		Department userStatus = (Department) Json.toObject(Department.class, data);
    		departmentDao.delete(userStatus);
    		return new ReturnBean(true,"");
    }
    
    
    @RequestMapping(value="getOne.do")  
    public @ResponseBody Department getOne(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
    		String id = request.getParameter("id");
    		return departmentDao.findById(Integer.parseInt(id));
    }
    
	/** 
	* @Description: 删除部门，并且将部门人员全部放置于根部门下 
	* @Title: getDayPlanList 
	* @param project_id
	* @return List<Map<String,Object>>
	* @author qin
	* @date 2018年7月31日上午10:31:34
	*/ 
	@RequestMapping(value="deleteDepartmentById.do")  
    public @ResponseBody ReturnBean deleteDepartmentById(@RequestParam("id") int id) {
		return departmentService.deleteDepartmentById(id);
	}
    
    
}
