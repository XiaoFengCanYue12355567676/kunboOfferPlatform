package com.shuohe.controller.system.user;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.shuohe.entity.system.user.User;
import com.shuohe.service.system.user.UserService;
import com.shuohe.service.system.user.UserService.DepTmp;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.encryption.MD5Utils;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;



@Controller  
@RequestMapping("/system/user/*")  
public class UserController {
	
    @Resource
    private UserService userService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
//    @Autowired(required = false)
//	private IdentityService identityService;

    @RequestMapping(value="login.do")  
    public @ResponseBody ReturnBean login(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String name=request.getParameter("userName");
		String password=request.getParameter("password");
		User user = userService.check(name, password);
		if(user == null)
		{
			return new ReturnBean(false,"");
		}
		else
		{
			session.setAttribute("user",user);
//			identityService.setAuthenticatedUserId(user.getActiviti_uuid());//设置activiti 用户登陆
//			Group group = identityService.createGroupQuery().groupMember(user.getActiviti_uuid()).singleResult();//查询用户组信息
//			session.setAttribute("group",group);
			return new ReturnBean(true,"");
		}
    }
    @RequestMapping(value="logout.do")  
    public @ResponseBody ReturnBean logout(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
			User user = (User) session.getAttribute("user");
			if(user == null)
	        {
				return new ReturnBean(true,"");
	        }
			else
			{
				session.removeAttribute("user"); //注销session中的username对象
				//session.invalidate();
				return new ReturnBean(true,"");
			}
//			identityService.setAuthenticatedUserId(user.getActiviti_uuid());//设置activiti 用户登陆
//			Group group = identityService.createGroupQuery().groupMember(user.getActiviti_uuid()).singleResult();//查询用户组信息
//			session.setAttribute("group",group);
    }
    
    @RequestMapping(value="getUserByDepartment.do")  
    public @ResponseBody EasyUiDataGrid getUserByDepartment(HttpServletRequest request,HttpServletResponse response) {
		String department=request.getParameter("department");
		String page=request.getParameter("page");
		String rows=request.getParameter("rows");
		EasyUiDataGrid ret = userService.getUserByDepartment(department, rows, page);	
//		System.out.println(Json.toJsonByPretty(ret));
		return ret;	
    }
    @RequestMapping(value="registe.do")  
    public @ResponseBody ReturnBean registe(HttpServletRequest request,HttpServletResponse response) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		User user = (User) Json.toObject(User.class, data);		
		if(userService.isUserExist(user.getName()).isResult() == true) return new ReturnBean(false,"不能创建同名的用户");
		String pd = user.getPassword();
		user.setPassword(MD5Utils.md5(pd));//密码加密
		userService.save(user);
		User usr = userService.check(user.getName(), pd);
		String u="";
		u=usr.getId()+","+usr.getName()+","+usr.getPosition_id();
		return new ReturnBean(true,u);
    }
        
    @RequestMapping(value="update.do")  
    public @ResponseBody ReturnBean update(HttpServletRequest request,HttpServletResponse response) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		User user = (User) Json.toObject(User.class, data);					
		userService.update(user);
		return new ReturnBean(true,"");
    }
    
    @RequestMapping(value="getById.do")  
    public @ResponseBody User getById(HttpServletRequest request,HttpServletResponse response) {
		String id=request.getParameter("id");
		return userService.findById(Integer.parseInt(id));
    }
    
    @RequestMapping(value="findByPositionId.do")  
    public @ResponseBody List<User> getByPositionId(HttpServletRequest request,HttpServletResponse response) {
    	String position_id=request.getParameter("position_id");
    	if(!"".equals(position_id)) {
    		return userService.findByPositionId(Integer.parseInt(position_id));
    	}else {
    		return userService.findAll();
    	}
    }
    
    @RequestMapping(value="getEasyUiTreeDepAndUser.do")  
    public @ResponseBody List<DepTmp> getEasyUiTreeDepAndUser(HttpServletRequest request,HttpServletResponse response) {
		return userService.getEasyUiTreeDepAndUser();
    }
    
	@RequestMapping(value="getDepUserByDepartmentId.do")  
    public @ResponseBody List<DepTmp> getDepUserByDepartmentId(@RequestParam("department_id") int department_id) {
		return userService.getEasyUiTreeDepAndUser(department_id);
	}
    
    
    
    @RequestMapping(value="getUser.do")  
    public @ResponseBody User getUser(HttpServletRequest request,HttpServletResponse response) {
    		HttpSession session = request.getSession();
    		User user = (User)session.getAttribute("user");
    		if(user == null)	return null;
    		user.setPassword("");
    		user.setActiviti_uuid("");
    		user.setEmail("");
    		user.setName("");
    		return user;
    }
    
    
    //退出系统的方法
    @RequestMapping(value="logoutSys.do") 
    public @ResponseBody ReturnBean logoutSys(HttpServletRequest request,HttpServletResponse response,HttpSession session){
    	ReturnBean rb = null;
    	try {
			session.removeAttribute("user"); //注销session中的username对象
			session.invalidate();
			rb = new ReturnBean(true, "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rb = new ReturnBean(false, "");
		}
		return rb;
		
    }
    @RequestMapping(value="getAllSelectUser.do") 
    public @ResponseBody List<Map<String,Object>> getAllSelectUser(){
    	List<Map<String,Object>> list = jdbcTemplate.queryForList(" select id,actual_name as name from db_system_user_user ");
    	return list;
    }
    
    //密码修改的方法
    @RequestMapping(value="editPassword.do") 
    public @ResponseBody ReturnBean editPassword(HttpServletRequest request,HttpServletResponse response,HttpSession session){
    	String password = request.getParameter("password");
    	ReturnBean rb = null;
    	try {
			User user = (User) session.getAttribute("user");
			user.setPassword(MD5Utils.md5(password));//密码加密
			userService.save(user);
			rb = new ReturnBean(true, "密码修改成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rb = new ReturnBean(false, "密码修改失败");
		}
		return rb;
		
    }
    
  
}
