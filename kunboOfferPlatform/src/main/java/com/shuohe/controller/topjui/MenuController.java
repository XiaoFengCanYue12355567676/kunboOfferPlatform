package com.shuohe.controller.topjui;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuohe.entity.system.rule.RuleStruct;
import com.shuohe.entity.topjui.Menu;
import com.shuohe.service.topjui.MenuService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.string.QString;

//import io.swagger.annotations.ApiOperation;

@Controller  
@RequestMapping("/topJUI/index/*")  
public class MenuController {
	
    @Resource
    private MenuService menuService;
	
//    @ApiOperation(value="获取MENU项目", notes="")
	@RequestMapping(value="getMenu.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String getMenu(HttpServletRequest request,HttpServletResponse response){  
	    	String name = request.getParameter("name");    
	    	String position_id = request.getParameter("position_id");    	    
		String r="";
		r = Json.toJsonByPretty(menuService.getMenuByPid(name,position_id ));
		return r;
	}
	
	@RequestMapping(value="getMenuList.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String  getMenuList(HttpServletRequest request,HttpServletResponse response){      
		String id_str = request.getParameter("id");
		int id = 0;
		if(!QString.isNull(id_str))
			id = Integer.parseInt(id_str);  
		return Json.toJsonByPretty(menuService.getMenuList(id));
	}
	@RequestMapping(value="findById.do")  
    public @ResponseBody Menu findById(HttpServletRequest request,HttpServletResponse response){      
		String id = request.getParameter("id");
		if(StringUtils.isBlank(id))
			return null;
		return menuService.findById(Integer.parseInt(id));
	}
	
	@RequestMapping(value="save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response){      
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		Menu menu = (Menu) Json.toObject(Menu.class, json);
		System.out.println(menu);
		boolean ret = menuService.save(menu);
		return new ReturnBean(ret,"");
	}
	
	@RequestMapping(value="delete.do")  
    public @ResponseBody ReturnBean delete(HttpServletRequest request,HttpServletResponse response){      
		String json = request.getParameter("data").replaceAll("\"\"", "null");
		Menu menu = (Menu) Json.toObject(Menu.class, json);
		boolean ret = menuService.delete(menu);
		return new ReturnBean(ret,"");
	}
	
	/*
	 * 
	 * 添加条件为部门id
	 * @time 2018/6/12
	 * 
	 * */
	@RequestMapping(value="getMenuListForDe.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String  getMenuListForDe(HttpServletRequest request,HttpServletResponse response){      
		String id_str = request.getParameter("id");
		String position_id = request.getParameter("position_id");
		int id = 0;
		int position = 0;
		if(!QString.isNull(id_str))
			id = Integer.parseInt(id_str);  
		if(!QString.isNull(position_id))
			position = Integer.parseInt(position_id); 
		return Json.toJsonByPretty(menuService.getMenuListForDe(id,position));
	}
}
