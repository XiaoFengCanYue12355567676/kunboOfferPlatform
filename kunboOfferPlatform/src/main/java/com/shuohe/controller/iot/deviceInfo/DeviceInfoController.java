package com.shuohe.controller.iot.deviceInfo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller  
@RequestMapping("/iot/deviceInfo/*")  
public class DeviceInfoController {
	
	@RequestMapping(value="getAll.do")  
    public @ResponseBody List<Map<String,String>> getAll(HttpServletRequest request,HttpServletResponse response){
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		
		for(int i=0;i<3;i++)
		{
			Map<String,String> t1 = new LinkedHashMap<String,String>();
			t1.put("t1", "WHKLT-S-DN-002");
			t1.put("t2", "抛丸机");
			t1.put("t3", "Q37");
			t1.put("t4", "A");
			t1.put("t5", "动能类");
			t1.put("t6", "青岛青铸工业机械有限公司");
			t1.put("t7", "南厂");
			t1.put("t8", "机车喷漆班");
			t1.put("t9", "2012.01");
			list.add(t1);
			
			
			Map<String,String> t2 = new LinkedHashMap<String,String>();
			t2.put("t1", "WHKLT-S-DN-012");
			t2.put("t2", "空压机");
			t2.put("t3", "HD-75VF");
			t2.put("t4", "A");
			t2.put("t5", "动能类");
			t2.put("t6", "福建泉州华达机械有限公司");
			t2.put("t7", "南厂");
			t2.put("t8", "机车空压房");
			t2.put("t9", "2010.08");
			list.add(t2);
			
			
			Map<String,String> t3 = new LinkedHashMap<String,String>();
			t3.put("t1", "WHKLT-S-DN-013");
			t3.put("t2", "空压机");
			t3.put("t3", "HD-30");
			t3.put("t4", "A");
			t3.put("t5", "动能类");
			t3.put("t6", "福建泉州华达机械有限公司");
			t3.put("t7", "南厂");
			t3.put("t8", "机车空压房");
			t3.put("t9", "2010.08");
			list.add(t3);
			
			Map<String,String> t4 = new LinkedHashMap<String,String>();
			t4.put("t1", "WHKLT-S-DN-001");
			t4.put("t2", "喷涂流水线");
			t4.put("t3", "XT160");
			t4.put("t4", "B");
			t4.put("t5", "动能类");
			t4.put("t6", "无锡南方物流设备有限公司");
			t4.put("t7", "南厂");
			t4.put("t8", "机车喷漆班");
			t4.put("t9", "2010.03");
			list.add(t4);
			
			Map<String,String> t5 = new LinkedHashMap<String,String>();
			t5.put("t1", "WHKLT-S-DN-007");
			t5.put("t2", "喷涂流水线");
			t5.put("t3", "");
			t5.put("t4", "B");
			t5.put("t5", "动能类");
			t5.put("t6", "潍坊特尔涂装设备有限公司");
			t5.put("t7", "南厂");
			t5.put("t8", "机车喷漆班");
			t5.put("t9", "2014.01");
			list.add(t5);
			
			
			Map<String,String> t6 = new LinkedHashMap<String,String>();
			t6.put("t1", "WHKLT-S-DN-010");
			t6.put("t2", "打沙设备");
			t6.put("t3", "自制");
			t6.put("t4", "B");
			t6.put("t5", "动能类");
			t6.put("t6", "威海克莱特");
			t6.put("t7", "南厂");
			t6.put("t8", "离心车间");
			t6.put("t9", "2013.12");
			list.add(t6);
			
			Map<String,String> t7 = new LinkedHashMap<String,String>();
			t7.put("t1", "WHKLT-S-DN-014");
			t7.put("t2", "喷涂流水线");
			t7.put("t3", "自制");
			t7.put("t4", "B");
			t7.put("t5", "动能类");
			t7.put("t6", "威海克莱特");
			t7.put("t7", "南厂");
			t7.put("t8", "机电三楼");
			t7.put("t9", "2009.08");
			list.add(t7);
			
			Map<String,String> t8 = new LinkedHashMap<String,String>();
			t8.put("t1", "WHKLT-N-DY-003");
			t8.put("t2", "数控旋压机");
			t8.put("t3", "2800");
			t8.put("t4", "A");
			t8.put("t5", "锻压类");
			t8.put("t6", "文登市东方机械配件厂");
			t8.put("t7", "北厂");
			t8.put("t8", "一车间成型班");
			t8.put("t9", "2009.07.31");
			list.add(t8);
		}
		
		return list;
	}
}
