package com.shuohe.entity.develop.url;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author qin
 * url管理bean类
 */
@Entity
@Table(name="db_dev_url_url")
public class Url {

	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 归属id，用于url分类
	 */
	int pid;
	
	/**
	 * url的名称
	 */
	String name;
	
	/**
	 * 描述
	 */
	String descr;
	
	/**
	 * url 类型
	 */
	String type;
	
	/**
	 * 创建人
	 */
	String creater;
	
	/**
	 * 创建时间
	 */
	Date create_date;

	
	/**
	 * sql语句
	 */
	@Column(name="sql_str", columnDefinition="LONGTEXT", nullable=true)
	String sql_str;
	
	
	/**
	 * sql排序语句
	 */
	@Column(name="arrange_str", columnDefinition="LONGTEXT", nullable=true)
	String arrange_str;
	
	
	
	
	
	public String getArrange_str() {
		return arrange_str;
	}

	public void setArrange_str(String arrange_str) {
		this.arrange_str = arrange_str;
	}

	public String getSql_str() {
		return sql_str;
	}

	public void setSql_str(String sql_str) {
		this.sql_str = sql_str;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	
	
}
