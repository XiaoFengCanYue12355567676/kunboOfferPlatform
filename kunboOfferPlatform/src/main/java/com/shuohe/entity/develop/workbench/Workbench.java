package com.shuohe.entity.develop.workbench;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="db_dev_workbench")
@DynamicUpdate
@Data
public class Workbench {

	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	int postation_id;
	
	/**
	 * 配置文件
	 */
	@Column(name="config_str", columnDefinition="LONGTEXT", nullable=true)
	String config_str;
	
	
	
}
