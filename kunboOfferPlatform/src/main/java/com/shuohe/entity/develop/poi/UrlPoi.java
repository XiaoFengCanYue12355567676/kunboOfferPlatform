package com.shuohe.entity.develop.poi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="db_dev_url_poi")
public class UrlPoi
{
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 归属id，用于url分类
	 */
	int pid;
	
	/**
	 * url的名称
	 */
	String name;
	
	/**
	 * 描述
	 */
	String descr;
	
	/**
	 * 导出文件的文件名
	 */
	String fileName;
	
	/**
	 * 创建人
	 */
	String creater;
	
	/**
	 * 创建时间
	 */
	String create_date;

	
	/**
	 * sql语句
	 */
	@Column(name="sql_str", columnDefinition="LONGTEXT", nullable=true)
	String sql_str;


	public int getId()
	{
		return id;
	}


	public void setId(int id)
	{
		this.id = id;
	}


	public int getPid()
	{
		return pid;
	}


	public void setPid(int pid)
	{
		this.pid = pid;
	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public String getDescr()
	{
		return descr;
	}


	public void setDescr(String descr)
	{
		this.descr = descr;
	}




	public String getFileName()
	{
		return fileName;
	}


	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}


	public String getCreater()
	{
		return creater;
	}


	public void setCreater(String creater)
	{
		this.creater = creater;
	}




	public String getCreate_date()
	{
		return create_date;
	}


	public void setCreate_date(String create_date)
	{
		this.create_date = create_date;
	}


	public String getSql_str()
	{
		return sql_str;
	}


	public void setSql_str(String sql_str)
	{
		this.sql_str = sql_str;
	}
	
	
}
