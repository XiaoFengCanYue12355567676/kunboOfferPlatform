package com.shuohe.entity.develop.url;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author qin
 * url管理 url 返回数据
 */
@Entity
@Table(name="db_dev_url_iutput_para")
public class UrlOutputPara {

	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 归属id，属于哪个url
	 */
	int pid;
	
	/**
	 * 返回数据的名称
	 */
	String name;
	/**
	 * 返回数据的类型
	 */
	String type;
	
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
