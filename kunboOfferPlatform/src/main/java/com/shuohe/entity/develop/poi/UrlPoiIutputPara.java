package com.shuohe.entity.develop.poi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author qin
 * url poi 管理 输入参数
 */
@Entity
@Table(name="db_dev_url_poi_iutput_para")
public class UrlPoiIutputPara
{
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 归属id，属于哪个url
	 */
	int pid;
	
	/**
	 * 返回数据的名称
	 */
	String name;
	
	/**sql 字段检索的名称*/
	String sql_para_name;
	
	
	/**
	 * 是否必须输入
	 */
	boolean is_necessary = false;
	
	
	
	
	public boolean isIs_necessary() {
		return is_necessary;
	}


	public void setIs_necessary(boolean is_necessary) {
		this.is_necessary = is_necessary;
	}


	/**
	 * 返回数据的类型
	 */
	String type;


	public int getId()
	{
		return id;
	}


	public void setId(int id)
	{
		this.id = id;
	}


	public int getPid()
	{
		return pid;
	}


	public void setPid(int pid)
	{
		this.pid = pid;
	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public String getSql_para_name()
	{
		return sql_para_name;
	}


	public void setSql_para_name(String sql_para_name)
	{
		this.sql_para_name = sql_para_name;
	}


	public String getType()
	{
		return type;
	}


	public void setType(String type)
	{
		this.type = type;
	}
	
	
}
