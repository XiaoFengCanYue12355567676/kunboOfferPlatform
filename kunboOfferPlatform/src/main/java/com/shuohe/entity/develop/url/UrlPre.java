package com.shuohe.entity.develop.url;

/**
 * 前台展示url
 * @author zz
 *
 */
public class UrlPre {
	 
	int id;
	String name;
	String descr;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
 
	
	
	 
}
