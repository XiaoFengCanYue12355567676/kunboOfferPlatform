package com.shuohe.entity.develop.poi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * poi 导出后的excel表格表头
 * @author    秦晓宇
 * @date      2018年1月11日 下午9:09:29 
 */
@Entity
@Table(name="db_dev_url_poi_header")
public class UrlPoiHeader
{
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 隶属于那个UrlPoi
	 * */
	int pid;
	
	/**
	 *数据库对应字段
	 * */
	String key_str;
	/**
	 * 表头汉字字段
	 * */
	String header_str;
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPid()
	{
		return pid;
	}
	public void setPid(int pid)
	{
		this.pid = pid;
	}
	public String getKey_str()
	{
		return key_str;
	}
	public void setKey_str(String key_str)
	{
		this.key_str = key_str;
	}
	public String getHeader_str()
	{
		return header_str;
	}
	public void setHeader_str(String header_str)
	{
		this.header_str = header_str;
	}
	
	
	
	
}
