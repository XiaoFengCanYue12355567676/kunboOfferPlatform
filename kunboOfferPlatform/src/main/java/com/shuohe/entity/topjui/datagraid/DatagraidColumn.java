package com.shuohe.entity.topjui.datagraid;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 表头信息
 * @author qin
 *
 */
@Entity
@Table(name="db_dev_easyui_datagraid_columns")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatagraidColumn {
	
	@Id
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "uuid")
	String uuid;
	
	// @Fields template_id : 模板名称 对应于 Datagraid 的 uuid
	String template_id;
	
	String field;
	String title;
	String align;
	String width;
	boolean hidden;
}
