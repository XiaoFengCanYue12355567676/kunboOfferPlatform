package com.shuohe.entity.topjui.component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="db_dev_easyui_combotrees")
//@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ComboTree {

	@Id	
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	String uuid;
	
	String id;
	
	// @Fields template_id : 上级模板的编号
	String template_id;
	
	String text;
	
	String value;
	
	String url;
	
	String iconCls;
	
	String prompt;
	
	boolean hidden = false;
	
}
