package com.shuohe.entity.topjui.datagraid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 表头信息
 * @author qin
 *
 */
@Entity
@Table(name="db_dev_easyui_datagraids")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Datagraid { 

	@Id	
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	String uuid;
	
	// @Fields name : 模板名称，不能重复
	String name;
	
	// @Fields url : 数据加载链接地址
	String url;
	
	boolean rowNumbers = true;
	
	boolean singleSelect = true;
	
	boolean autoRowHeight = true;
	
	boolean pagination = true;
	
	boolean fitColumns = true;
	
	boolean striped = true;
	
	boolean checkOnSelect = true;
	
	boolean selectOnCheck = true;
	
	boolean collapsible = true;
	
	String toolbar;
	
	int pageSize;
	
	@Column(name="jsstrengthen", columnDefinition="LONGTEXT", nullable=true)
	String jsstrengthen;
	
}
