package com.shuohe.entity.topjui.datagraid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="db_dev_easyui_datagraid_searchform")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchForm {
	@Id	
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	String uuid;
	
	String id;
	
	// @Fields template_id : 上级模板的编号
	String template_id;
	
    @Column(name = "clazz",columnDefinition="varchar(30) not null")
	String clazz;
	
	boolean editable;
	
	String prompt;
	
	boolean hidden;
	
}
