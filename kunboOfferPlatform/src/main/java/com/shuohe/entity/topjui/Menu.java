package com.shuohe.entity.topjui;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * topjui menu侧边栏显示
 * @author    秦晓宇
 * @date      2017年12月13日 上午9:10:16 
 */
@Entity
@Table(name="db_system_topjui_menu")
public class Menu
{
	
	@Override
	public String toString() {
		return "Menu [id=" + id + ", pid=" + pid + ", name=" + name + ", state=" + state + ", iconCls=" + iconCls
				+ ", text=" + text + ", url=" + url + ", children=" + children + "]";
	}

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;						//子id
	int pid;					//父id
	String name;			//名称
	String state;			//开合状态
	String iconCls;		//图标
	String text;				//名称
	String url;				//连接地址
	
	@Transient
	List<Menu> children;

	
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getPid()
	{
		return pid;
	}

	public void setPid(int pid)
	{
		this.pid = pid;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getIconCls()
	{
		return iconCls;
	}

	public void setIconCls(String iconCls)
	{
		this.iconCls = iconCls;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public List<Menu> getChildren()
	{
		return children;
	}

	public void setChildren(List<Menu> children)
	{
		this.children = children;
	}
	
	
}
