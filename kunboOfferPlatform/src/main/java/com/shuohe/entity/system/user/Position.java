package com.shuohe.entity.system.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="db_system_user_position")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Position {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;	
	
	String text;
	String activiti_uuid;

}
