package com.shuohe.entity.system.dto;

import java.util.ArrayList;
import java.util.List;

public class DeptDto {
	private Integer deptId;
	private String deptName;
	private Integer parentId;
	List<UserDto> userlist = new ArrayList<UserDto>();
	List<DeptDto> DeptDtoList;
	public List<DeptDto> getDeptDtoList() {
		return DeptDtoList;
	}
	public void setDeptDtoList(List<DeptDto> deptDtoList) {
		DeptDtoList = deptDtoList;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public List<UserDto> getUserlist() {
		return userlist;
	}
	public void setUserlist(List<UserDto> userlist) {
		this.userlist = userlist;
	}
	

}
