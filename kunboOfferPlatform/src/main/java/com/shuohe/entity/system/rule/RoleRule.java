package com.shuohe.entity.system.rule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="db_system_topjui_rule")
public class RoleRule
{
	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;								//权限id
	int rule_struct_id;			//权限结构名称
	int postation_id;				//用户角色
	boolean is_use;				//是否可用
	
	
	public int getRule_struct_id()
	{
		return rule_struct_id;
	}
	public void setRule_struct_id(int rule_struct_id)
	{
		this.rule_struct_id = rule_struct_id;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPostation_id()
	{
		return postation_id;
	}
	public void setPostation_id(int postation_id)
	{
		this.postation_id = postation_id;
	}
	public boolean isIs_use()
	{
		return is_use;
	}
	public void setIs_use(boolean is_use)
	{
		this.is_use = is_use;
	}
	
	
	
	
	
}