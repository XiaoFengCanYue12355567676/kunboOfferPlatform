package com.shuohe.entity.system.rule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="db_system_topjui_rule_struct")
public class RuleStruct
{
	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;								//权限结构id
	int page_id;					//对应菜单页面id
	
	String component_id;		//对应组件id
	String text;						//权限名称
	String component_style;	//组件类型
	
	
	public String getComponent_style()
	{
		return component_style;
	}
	public void setComponent_style(String component_style)
	{
		this.component_style = component_style;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPage_id()
	{
		return page_id;
	}
	public void setPage_id(int page_id)
	{
		this.page_id = page_id;
	}
	public String getComponent_id()
	{
		return component_id;
	}
	public void setComponent_id(String component_id)
	{
		this.component_id = component_id;
	}
	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		this.text = text;
	}
	
	
	
	
	
	
	
	
}
