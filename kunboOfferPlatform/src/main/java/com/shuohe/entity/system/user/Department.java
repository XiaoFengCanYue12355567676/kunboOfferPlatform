package com.shuohe.entity.system.user;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 部门类，对应于数据库中部门的id和名称
 * @author    秦晓宇
 * @date      2017年2月28日 下午6:25:20 
 */
@Entity
@Table(name="db_system_user_department")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {

	public enum Status
	{
		ServiceStation,
		GuoNeng
	}
	
	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;	
	int parent_id;				/**父id*/
	String text;				/**组织名称*/
	Status type;					//类型
	
	// @Fields header_id : 部门负责人id
	int header_id;
	
	// @Fields header_name : 负责人名称
	String header_name;
	
	@Transient
	List<Department> children;

}
