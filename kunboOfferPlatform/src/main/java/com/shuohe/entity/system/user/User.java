package com.shuohe.entity.system.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="db_system_user_user")
public class User {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;				//用户id
	String name;		//用户名
	String actual_name;	//用户真实姓名
	String password;	//用户密码
	int department_id;	//用户部门id
	Integer position_id;	//用户职位id
	String email;		//用户邮件
	String phone_call;	//用户电话
	@Temporal(TemporalType.TIMESTAMP)
	Date registration_date;	//用户注册时间
	int status;			//用户状态
	String activiti_uuid;	//activiti uuid 表征activiti的用户
	
	
	
	public String getActiviti_uuid() {
		return activiti_uuid;
	}
	public void setActiviti_uuid(String activiti_uuid) {
		this.activiti_uuid = activiti_uuid;
	}
	public User(int id, String name, String actual_name, String password, int department_id, Integer position_id,
			String email, String phone_call, Date registration_date, int status) {
		super();
		this.id = id;
		this.name = name;
		this.actual_name = actual_name;
		this.password = password;
		this.department_id = department_id;
		this.position_id = position_id;
		this.email = email;
		this.phone_call = phone_call;
		this.registration_date = registration_date;
		this.status = status;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActual_name() {
		return actual_name;
	}
	public void setActual_name(String actual_name) {
		this.actual_name = actual_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public Integer getPosition_id() {
		return position_id;
	}
	public void setPosition_id(Integer position_id) {
		this.position_id = position_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone_call() {
		return phone_call;
	}
	public void setPhone_call(String phone_call) {
		this.phone_call = phone_call;
	}
	public Date getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}