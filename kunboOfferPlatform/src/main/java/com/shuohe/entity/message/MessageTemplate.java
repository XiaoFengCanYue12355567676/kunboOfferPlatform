//package com.shuohe.entity.message;
//
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EntityListeners;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.DynamicUpdate;
//import org.hibernate.annotations.GenericGenerator;
//import org.springframework.data.jpa.domain.support.AuditingEntityListener;
//
//import com.kunbo.shop.entity.template.AttriType;
//import com.kunbo.shop.entity.template.OptionType;
//import com.kunbo.shop.entity.template.TemplateInfo;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
///**
// * 消息模板；用于保存某种消息的提醒方式、提醒所包含的内容、提醒所包含的连接等信息
// * @author qin
// *
// */
//
//@Entity
//@Table(name="db_msg_template")
//@EntityListeners(AuditingEntityListener.class)
//@Data
//public class MessageTemplate {
//	
//	@Id @Column(name="id")
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	int id;	
//	
//	/**
//	 * 别名
//	 */
//	String alias_name;	
//	
//	/**
//	 * 是否进行短信推送
//	 */
//	boolean sms;
//	
//	/**
//	 * 是否进行微信公众号推送
//	 */
//	boolean weChartOfficeAccountPush;
//	
//	/**
//	 * 是否进行企业微信号推送
//	 */
//	boolean enterpriseWeChat;
//	
//	/**
//	 * 是否进行邮箱推送
//	 */
//	boolean mail;
//	
//	
//	
//}
