package com.shuohe.entity.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qin
 * 邮件消息箱
 */

@Entity
@Table(name="db_msg_mail1")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageMail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 接收人的邮箱
	 */
	String reciver_mail;
	
	/**
	 * 接收人的ID
	 */
	int reciver_id;
	
	/**
	 * 接收人的姓名
	 */
	String reciver_name;
	
	boolean isSuccess;

	
	
}
