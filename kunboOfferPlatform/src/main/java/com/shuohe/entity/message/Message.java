package com.shuohe.entity.message;

import java.util.List;

public class Message {
		
	/**
	 * 邮件消息列表
	 */
	List<MessageMail> msg_mail;
	
	/**
	 * 短信消息列表
	 */
	List<MessageSms> msg_sms;
	
	/**
	 * 网页消息列表
	 */
	List<MessageWeb> msg_web;

	
	public List<MessageMail> getMsg_mail() {
		return msg_mail;
	}

	public void setMsg_mail(List<MessageMail> msg_mail) {
		this.msg_mail = msg_mail;
	}

	public List<MessageSms> getMsg_sms() {
		return msg_sms;
	}

	public void setMsg_sms(List<MessageSms> msg_sms) {
		this.msg_sms = msg_sms;
	}

	public List<MessageWeb> getMsg_web() {
		return msg_web;
	}

	public void setMsg_web(List<MessageWeb> msg_web) {
		this.msg_web = msg_web;
	}
	
	
	
}
