package com.shuohe.entity.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qin
 * 网页消息箱
 */
@Entity
@Table(name="db_msg_web")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageWeb {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 接收人ID
	 */
	int reciver_id;
	
	/**
	 * 接收人的姓名
	 */
	int reciver_name;
	
	/**
	 * 是否已读
	 */
	boolean isRead;
	
	/**
	 * 通知内容
	 */
	String content;
	
	/**
	 * 通知携带的连接
	 */
	String url;
}
