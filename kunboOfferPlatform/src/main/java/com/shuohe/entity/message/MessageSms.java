package com.shuohe.entity.message;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qin
 * 短信推送记录表
 */
@Entity
@Table(name="db_msg_sms")
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageSms {
	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;	
	
	/**
	 * 接收人电话号码
	 */
	String reciver_phone_number;
	
	/**
	 * 接收人的ID
	 */
	int reciver_id;
	
	/**
	 * 接收人的姓名
	 */
	String reciver_name;
	
	/**
	 * 短消息内容
	 */
	String content;
	
	/**
	 * 发出时间
	 */
	Date send_date;
	
	/**
	 * 是否发送成功
	 */
	boolean isSuccess;

	



	
	
	
	
}
