package com.shuohe.util.datastyle;

import java.util.List;

public class Dg<T> {
	
	int total;
	private List<T> rows;
	public Dg() {
		// TODO Auto-generated constructor stub
	}
	public Dg(int total, List<T> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	
	
}
