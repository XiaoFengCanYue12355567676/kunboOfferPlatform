package com.shuohe.util.returnBean;

/**
 * 返回bean<br/>
 * 当前台向后台请求数据时，返回这个bean的json字符串
 * @author    秦晓宇
 * @date      2017年3月1日 下午3:24:04 
 */
public class ReturnBean3 {

	boolean result;
	Object object;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Object getObject() {
		return object;
	}
	public void setDescribe(Object object) {
		this.object = object;
	}
	public ReturnBean3(boolean result, Object object) {
		super();
		this.result = result;
		this.object = object;
	}
	

	
	
}
