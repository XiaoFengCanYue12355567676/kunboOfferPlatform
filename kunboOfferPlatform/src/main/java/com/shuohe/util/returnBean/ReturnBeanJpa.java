package com.shuohe.util.returnBean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnBeanJpa {
	
	// @Fields result : jpa 执行结果
	boolean result;
	// @Fields describe : jpa 执行结果描述
	String describe;
	// @Fields object : jpa执行返回
	Object object;
}
