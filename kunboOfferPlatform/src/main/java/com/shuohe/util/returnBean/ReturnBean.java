package com.shuohe.util.returnBean;

import com.shuohe.util.string.QString;

/**
 * 返回bean<br/>
 * 当前台向后台请求数据时，返回这个bean的json字符串
 * @author    秦晓宇
 * @date      2017年3月1日 下午3:24:04 
 */
public class ReturnBean {

	public ReturnBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	boolean result;
	String describe;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public ReturnBean(boolean result, String describe) {
		super();
		this.result = result;
		this.describe = describe;
	}
	
	public static ReturnBean genReturnbean(String res) {
		return res == null ? new ReturnBean(true, null) : new ReturnBean(false, res);
	}

	public static ReturnBean genReturnbean(boolean result, String res) {
		return new ReturnBean(result, res);
	}

	public static ReturnBean genSuccessReturnbean(Object res) {
		return new ReturnBean(true, QString.GSON.toJson(res));
	}

	public static final ReturnBean SIMPLE_SUCCESS = ReturnBean.genReturnbean(null);

}
