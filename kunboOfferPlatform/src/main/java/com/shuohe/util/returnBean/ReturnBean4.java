package com.shuohe.util.returnBean;

import java.util.ArrayList;

/**
 * 返回bean<br/>
 * 当前台向后台请求数据时，返回这个bean的json字符串
 * @author    秦晓宇
 * @date      2017年3月1日 下午3:24:04 
 */
public class ReturnBean4 {

	boolean result;
	String describe;
	ArrayList<String> list = new ArrayList<String>();
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public ReturnBean4(boolean result, String describe,ArrayList<String> list) {
		super();
		this.result = result;
		this.describe = describe;
		this.list = list;
	}
	
			
	public ReturnBean4() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the list
	 * @date      2017年5月23日 下午5:32:07 
	 */
	public ArrayList<String> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 * @date      2017年5月23日 下午5:32:07 
	 */
	public void setList(ArrayList<String> list) {
		this.list = list;
	}
	public void addList(String str)
	{
		this.list.add(str);
	}
	

	
	
}
