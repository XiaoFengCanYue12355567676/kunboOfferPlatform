package com.shuohe.util.resultSet;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Util {
	/*********************************SQL缁撴灉杞琹ist宸ュ叿***********************************/
	public static List<Map<String,Object>> ResultSetToList(ResultSet rs) throws SQLException
	{
		List<Map<String,Object>> results=new ArrayList<Map<String,Object>>();
		ResultSetMetaData rsmd = rs.getMetaData();  
		int colCount=rsmd.getColumnCount();
		List<String> colNameList=new ArrayList<String>();
		for(int i=0;i<colCount;i++)
		{
			colNameList.add(rsmd.getColumnLabel(i+1));
//			debug(" colNameList = "+rsmd.getColumnLabel(i+1));
		} 
		
		
		while(rs.next())
		{
			Map map=new HashMap<String, Object>();
			for(int i=0;i<colCount;i++)
			{
				
				String key=colNameList.get(i);
				Object value=rs.getString(colNameList.get(i));
//				System.out.println("key = "+key+" value ="+value );
				map.put(key, value);	
			}
			results.add(map);
		}
		rs.beforeFirst();//灏嗙粨鏋滈泦鎸囬拡鎸囧洖鍒板紑濮嬩綅缃紝杩欐牱鎵嶈兘閫氳繃while鑾峰彇rs涓殑鏁版嵁
		return results;
	}
	
	
	public static Map<String,Object> ResultSetToMap(ResultSet rs) throws SQLException
	{
		Map<String,Object> results=new HashMap<String,Object>();
		ResultSetMetaData rsmd = rs.getMetaData();  
		int colCount=rsmd.getColumnCount();
		List<String> colNameList=new ArrayList<String>();
		for(int i=0;i<colCount;i++)
		{
			colNameList.add(rsmd.getColumnLabel(i+1));
//			debug(" colNameList = "+rsmd.getColumnLabel(i+1));
		} 
		
		
		while(rs.next())
		{
			Map map=new HashMap<String, Object>();
			for(int i=0;i<colCount;i++)
			{				
				String key=colNameList.get(i);
				Object value=rs.getString(colNameList.get(i));
//				System.out.println("key = "+key+" value ="+value );
				results.put(key, value);	
			}
			break;
		}
		rs.beforeFirst();//灏嗙粨鏋滈泦鎸囬拡鎸囧洖鍒板紑濮嬩綅缃紝杩欐牱鎵嶈兘閫氳繃while鑾峰彇rs涓殑鏁版嵁
		return results;
	}
	
	
}
