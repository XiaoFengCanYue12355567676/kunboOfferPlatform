package com.shuohe.util.excel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.util.CellRangeAddress;

/** * @author xj * @Describe Excel 工具类--将 Excel 文件变为二维 String 数组，需要 poi.jar 包 */ 
public class ExcelUtils {
	
	/**
	 * 从 Excel 文件得到二维数组
	 * @param file
	 * @param startRows  开始的行数，通常为每个 sheet 的标题行数
	 * @param endRows  结束的行数   0表示无行数限制
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String[][] getData(File file, int startRows, int endRows) throws Exception {
		return getData(file, startRows, endRows, 0);
	}
	
	public static void main(String[] args){
		File excelFile = new File("C:\\11.xls");
		String[][] strs;
		try {
			strs = ExcelUtils.getData(excelFile,0,0);
			System.out.println(strs[0].length);
			for (int i = 0; i < strs.length; i++) {
				System.err.println(strs[i][0]+" | "+strs[i][1]+" | "+strs[i][2]+" | "+strs[i][3]+" | "+strs[i][4]);
			}
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
	
	/**
	 * 从 Excel 文件得到二维数组
	 * @param file
	 * @param startRows  开始的行数，通常为每个 sheet 的标题行数
	 * @param endRows  结束的行数   0表示无行数限制
	 * @param startSheet  从第几张sheet 开始执行
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String[][] getData(File file, int startRows, int endRows,int startSheet) throws Exception {
		ArrayList<String[]> result = new ArrayList<String[]>();
		int rowSize = 0; 
		BufferedInputStream in = new BufferedInputStream(new FileInputStream( file)); 
		// 打开 HSSFWorkbook 
		POIFSFileSystem fs = new POIFSFileSystem(in); 
		HSSFWorkbook wb = new HSSFWorkbook(fs); 
		for (int sheetIndex = startSheet; sheetIndex < startSheet+1; sheetIndex++) {
			HSSFSheet st = wb.getSheetAt(sheetIndex);
			int rowNums = st.getLastRowNum();
			if(endRows != 0 && endRows<rowNums){
				rowNums = endRows;
			}
			System.err.println(st.getSheetName()+"行数：" + rowNums); 
			// 第一行为标题，不取 
			for (int rowIndex = startRows; rowIndex <= rowNums; rowIndex++) {
				HSSFRow row = st.getRow(rowIndex); 
				if (row == null) { continue; } 
				int tempRowSize = row.getLastCellNum() + 1; 
				if (tempRowSize > rowSize) {
					rowSize = tempRowSize; 
				} 
				String[] values = new String[rowSize]; 
				Arrays.fill(values, ""); 
				boolean hasValue = false; 
				for (int columnIndex = 0; columnIndex <= row.getLastCellNum(); columnIndex++) { 
					String value = ""; 
					HSSFCell cell = row.getCell(columnIndex); 
					if (cell != null) { 
						try{
						/*	HSSFDataFormatter f=new HSSFDataFormatter();
							value = f.formatCellValue(cell).trim();*/
							value = getFormula(cell);
						}catch (Exception e) {
							System.err.println(st.getSheetName()+" " + rowIndex +"行 "+columnIndex+"列出错了"); 
						}
					} 
					values[columnIndex] = value;
					hasValue = true;
				}
				if (hasValue) {
					result.add(values);
				}
			}
		}
		in.close();
		String[][] returnArray = new String[result.size()][rowSize]; 
		for (int i = 0; i < returnArray.length; i++) { 
			returnArray[i] = (String[]) result.get(i); 
		}
		return returnArray;
	}
	
	public static String getFormula(HSSFCell cell){
		if (cell.getCellTypeEnum() == CellType.FORMULA) {
			cell.setCellType(CellType.NUMERIC);
			return   String.valueOf(cell.getNumericCellValue());
	    }
		HSSFDataFormatter f=new HSSFDataFormatter();
		return f.formatCellValue(cell).trim();
	}

	/**
	 * 创建单元格样式
	 * 
	 * @param wb
	 *            HSSFWorkbook
	 * @param halin
	 *            显示方式 【HSSFCellStyle.ALIGN_CENTER】
	 * @param fontStyle
	 *            字体样式
	 * @return
	 */
	public static HSSFCellStyle createCellStyle(HSSFWorkbook wb, short halin,
			HSSFFont fontStyle) {
		HSSFCellStyle cellStyle = wb.createCellStyle(); // 在工作薄的基础上建立一个样式
		cellStyle.setFont(fontStyle); // 设置字体样式
		//cellStyle.setAlignment(halin); // 显示方式【居中，靠左，靠右】
		cellStyle.setWrapText(true); // 自动换行
		//cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		//改变单元格格式为文本
		cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("@"));
		return cellStyle;
	}

	/**
	 * 创建字体
	 * 
	 * @param wb
	 * @param boldweight
	 *            【HSSFFont.BOLDWEIGHT_NORMAL】
	 * @param size
	 *            【(short)14】
	 * @return
	 */
	public static HSSFFont createFont(HSSFWorkbook wb, String fontName,
			short boldweight, short size) {
		HSSFFont font = wb.createFont();
		if (StringUtils.isNotBlank(fontName)) {
			font.setFontName(fontName);
		} else {
			font.setFontName("宋体");
		}
		//font.setBoldweight(boldweight);
		font.setFontHeightInPoints(size);
		return font;
	}

	/**
	 * 创建行
	 * 
	 * @param sheet
	 *            【HSSFSheet】
	 * @param rowNum
	 *            【0】第几行
	 * @param rowHeight
	 *            行高【200】
	 * @return
	 */
	public static HSSFRow createRow(HSSFSheet sheet, int rowNum, float rowHeight) {
		HSSFRow row = sheet.createRow(rowNum);
		if (rowHeight > 0) {
			row.setHeightInPoints(rowHeight); // 设置行高
		}
		return row;
	}

	/**
	 * 合并单元格
	 * 
	 * @param sheet
	 * @param startRow
	 *            开始行
	 * @param endRow
	 *            结束行
	 * @param startColumn
	 *            开始列
	 * @param lendColumn
	 *            结束列
	 * @return
	 */
	public static CellRangeAddress mergeCell(HSSFSheet sheet, int startRow,
			int endRow, int startColumn, int lendColumn) {
		CellRangeAddress craddrs = new CellRangeAddress(startRow, endRow,
				startColumn, lendColumn);// 坐标是（开始行，结束行，开始列，结束列）
		sheet.addMergedRegion(craddrs);
		return craddrs;
	}
}
