package com.shuohe.util.poi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class POI {
	String file_name;
	String table;
	ArrayList<String> headers = new ArrayList<String>();
	ArrayList<String> keys = new ArrayList<String>();
	List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	
	
	public String getFile_name() {
		return file_name;
	}



	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}



	public String getTable() {
		return table;
	}



	public void setTable(String table) {
		this.table = table;
	}



	public ArrayList<String> getHeaders() {
		return headers;
	}



	public void setHeaders(ArrayList<String> headers) {
		this.headers = headers;
	}




	
	public List<Map<String, Object>> getData() {
		return data;
	}



	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}





	public ArrayList<String> getKeys() {
		return keys;
	}



	public void setKeys(ArrayList<String> keys) {
		this.keys = keys;
	}





	public class Rb
	{
		HSSFWorkbook excel;
		boolean result;
		String discribe;
		public HSSFWorkbook getExcel() {
			return excel;
		}
		public void setExcel(HSSFWorkbook excel) {
			this.excel = excel;
		}
		public boolean isResult() {
			return result;
		}
		public void setResult(boolean result) {
			this.result = result;
		}
		public String getDiscribe() {
			return discribe;
		}
		public void setDiscribe(String discribe) {
			this.discribe = discribe;
		}				
	}

	public Rb createExcel()
	{
		Rb r = new Rb();
		if("".equals(this.file_name)||this.file_name==null)
		{
			r.setResult(false);
			r.setDiscribe("未命名文件名");
			r.setExcel(null);
			return r;
		}
		if("".equals(this.table)||this.table==null)
		{
			r.setResult(false);
			r.setDiscribe("未命名表名");
			r.setExcel(null);
			return r;
		}
		if(headers == null)
		{
			r.setResult(false);
			r.setDiscribe("未设置表头");
			r.setExcel(null);
			return r;
		}		
		else
		{
			if(headers.size() <= 0)
			{
				r.setResult(false);
				r.setDiscribe("表头数不能为0");
				r.setExcel(null);
				return r;
			}
		}
		
		// 创建Excel的工作书册 Workbook,对应到一个excel文档  
        HSSFWorkbook wb = new HSSFWorkbook();  
        
        // 创建Excel的工作sheet,对应到一个excel文档的tab  
        HSSFSheet sheet = wb.createSheet(this.table);  
        
        HSSFCellStyle style = wb.createCellStyle();
        //创建第一行（也可以称为表头）
        HSSFRow row = sheet.createRow(0);
                
        final short cellNumber = (short)headers.size(); 
        for(int i = 0;i < cellNumber;i++)     
        {     
            HSSFCell headerCell = row.createCell(i);    
            headerCell.setCellType(HSSFCell.CELL_TYPE_STRING);  
            headerCell.setCellValue(headers.get(i));                 
            sheet.autoSizeColumn(i,true); 
        }   
		
        
        //向单元格里填充数据
        for (int i = 0; i < this.data.size(); i++) 
        {
        	row = sheet.createRow(i + 1);
        	for(int x=0;x<this.headers.size();x++)
        	{
        		//若在这里发生转换错误，可再继续添加转换错误的format方法
        		if(this.data.get(i).get(this.keys.get(x)) instanceof java.sql.Date)
        		{
        			String date = ((java.sql.Date)this.data.get(i).get(this.keys.get(x))).toString();
        			row.createCell(x).setCellValue(date);
        		}
        		else
        		{   
//        			System.out.println(null==this.data);
//        			System.out.println(null==this.data.get(i));
        			if(null==this.data.get(i).get(this.keys.get(x))){
        				row.createCell(x).setCellValue("");
        			}else{
            			row.createCell(x).setCellValue((String)this.data.get(i).get(this.keys.get(x)).toString());
        			}
        			
        		}        		                	
        	}
        }
        
        for(int i = 0;i < cellNumber;i++)     
        {     
            sheet.autoSizeColumn(i,true); 
        }   
        
		r.setResult(true);
		r.setDiscribe("创建excel文件成功");
		r.setExcel(wb);
		return r;
		
	}
	
	
	
}
