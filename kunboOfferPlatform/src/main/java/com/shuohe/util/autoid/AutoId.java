//package com.shuohe.util.autoid;
//
//import java.util.Iterator;
//import java.util.List;
//
////import org.hibernate.Query;
////import org.hibernate.Session;
//
//import com.shuohe.util.date.SDate;
////import com.shuohe.util.hibernate.HibernateUtil;
//
//public class AutoId {
//	
//	
//	private static void debug(String s)
//	{
//		System.out.println(s);
//	}
//	
//	public static void main(String[] args)
//	{
////		debug(getOrderId(new SupplierClaimOrder(),"auto_id","GN-GYSFSP-"));
////		SupplierClaimOrder s = (SupplierClaimOrder) getAutoObjectByAutoId(SupplierClaimOrder.class,"GN-GYSFSP-20170429-001");
////		debug(Json.toJsonByPretty(s));	
//	}
//	
//	public static Object getAutoObjectByAutoId(Class clazz, String auto_id)
//	{
//		Session session = null;  	
//		Object ret=new Object();
//        try 
//        {          	
//            session = HibernateUtil.currentSession();  
//            session.beginTransaction();    
//            
//            String hql="from "+clazz.getName()+" where auto_id="+"\'"+auto_id+"\'";   
//            Query query=session.createQuery(hql);
//            Iterator its=query.list().iterator();
//            while(its.hasNext())
//            {  
//            	ret =(Object)its.next(); 
//            	break;
//            }               
//            
//        }catch(Exception e) 
//        {  
//            e.printStackTrace();  
//        }
//        finally
//        {        	
//            HibernateUtil.closeSession();  
//        }
//        return ret;
//	}
//	
//	public static Object getAutoObjectByAutoId(Session session,Class clazz, String auto_id)
//	{
//
//		Object ret=new Object();
//		
//		
//	    String hql="from "+clazz.getName()+" where auto_id="+"\'"+auto_id+"\'";   
//	    Query query=session.createQuery(hql);
//	    Iterator its=query.list().iterator();
//	    while(its.hasNext())
//	    {  
//	    	ret =(Object)its.next(); 
//	    	break;
//	    }               
//		    
//		return ret;
//	}
//	
//	
//
//	public static String getOrderId(AutoIdBean clazz,String base,String header_name)
//	{
//		String day = SDate.getSystemDateYMDToString();
//		String ret = null;
//		long id = getOrderIdForOneDay(clazz.getClass().getName(),base,day);
//		id++;
//		String xs=String.valueOf(id);
//		String [] ss = {"0000","000","00","0",""};
//		xs = ss[xs.length()+1] + xs;
//		ret = header_name+"-"+SDate.getSystemDateYMD2ToString()+"-"+xs;
//		clazz.setAuto_id(ret);
//		clazz.setDate(SDate.getSystemDate());
//		insert(clazz);
//		return ret;
//	}
//	public static String getOrderId(Session session,AutoIdBean clazz,String base,String header_name)
//	{
//		String day = SDate.getSystemDateYMDToString();
//		String ret = null;
//		long id = getOrderIdForOneDay(session,clazz.getClass().getName(),base,day);
//		id++;
//		String xs=String.valueOf(id);
//		String [] ss = {"0000","000","00","0",""};
//		xs = ss[xs.length()+1] + xs;
//		ret = header_name+"-"+SDate.getSystemDateYMD2ToString()+"-"+xs;
//		clazz.setAuto_id(ret);
//		clazz.setDate(SDate.getSystemDate());
//		insert(session,clazz);
//		return ret;
//	}
//	
//	private static long getOrderIdForOneDay(String clazz,String base,String day)
//	{
//		long ret=0;
//		String start = day+" 00:00:00";  
//	    String end = day+" 23:59:59";  
//	    Session session = null;  	
//		try 
//        {          	
//            session = HibernateUtil.currentSession();  
//            session.beginTransaction();  
//                         
//            String hql="select p."+base
//            		+ " from "+clazz+" as"
//            		+ " p where p.date >='" +start+"' and p.date <='"+end+"'"
//            		+ " order by p."+base+" desc";   
//            
//            Query query=session.createQuery(hql);          
//            
//        	List<String> its=query.list();                
//
//        	debug("len="+its.size());
//        	
//        	String temp = null;
//        	for(String s:its)
//        	{
//        		debug("s = "+s);
//        		temp = s;
//        		break;
//        	}
//        	if(temp!=null)
//        	{        		
//        		ret = Long.parseLong(temp.substring(temp.length()-3,temp.length()));
//        		debug("ret = "+ret);
//        	}
//        	else if(temp==null)
//        	{
//        		ret = 0;
//        	}
//
//        }catch(Exception e) 
//        {  
//            e.printStackTrace();  
//        }
//        finally
//        {        	
//            HibernateUtil.closeSession();  
//        }
//		return ret; 
//	}
//	private static long getOrderIdForOneDay(Session session,String clazz,String base,String day)
//	{
//		long ret=0;
//		String start = day+" 00:00:00";  
//	    String end = day+" 23:59:59";  
//                        
//        String hql="select p."+base
//        		+ " from "+clazz+" as"
//        		+ " p where p.date >='" +start+"' and p.date <='"+end+"'"
//        		+ " order by p."+base+" desc";   
//        
//        Query query=session.createQuery(hql);          
//        
//    	List<String> its=query.list();                
//
//    	debug("len="+its.size());
//    	
//    	String temp = null;
//    	for(String s:its)
//    	{
//    		debug("s = "+s);
//    		temp = s;
//    		break;
//    	}
//    	if(temp!=null)
//    	{        		
//    		ret = Long.parseLong(temp.substring(temp.length()-3,temp.length()));
//    		debug("ret = "+ret);
//    	}
//    	else if(temp==null)
//    	{
//    		ret = 0;
//    	}
//
//		return ret; 
//	}
//	
//	private static void insert(Object object)
//	{
//	    Session session = null;  	
//        try 
//        {          	
//            session = HibernateUtil.currentSession();  
//            session.beginTransaction();  
//                         
//            session.save(object);
//            
//            session.flush();  
//            session.clear();  
//            
//            session.getTransaction().commit();
//        }catch(Exception e) 
//        {  
//            e.printStackTrace();  
//        }
//        finally
//        {        	
//            HibernateUtil.closeSession();  
//        }
//	}
//	private static void insert(Session session,Object object)
//	{                         
//        session.save(object);            
//        session.flush();  
//        session.clear();             
//	}
//}
