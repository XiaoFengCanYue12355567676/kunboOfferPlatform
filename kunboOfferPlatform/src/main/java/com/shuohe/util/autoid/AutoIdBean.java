package com.shuohe.util.autoid;

import java.util.Date;

public class AutoIdBean {

	transient String auto_id;
	transient Date date;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAuto_id() {
		return auto_id;
	}

	public void setAuto_id(String auto_id) {
		this.auto_id = auto_id;
	}

	public AutoIdBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
