package com.shuohe.util.string;

import com.google.gson.Gson;
import org.springframework.beans.BeanUtils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Optional;

public final class QString {

	/**
	 * 检查字符串是否为null
	 * @author    秦晓宇
	 * @date      2017年5月23日 下午5:56:55 
	 * @param b
	 * @return 是null返回true，不是null返回false
	 */
	public static boolean isNull(String b)
	{
		if (b != null && !b.isEmpty())
		{
			return false;
		}
		return true;
	}

	public static boolean isAllEmpty(String... args) {
		if (args == null) {
			return true;
		}
		for (String one :
				args) {
			if (!isNull(one)) {
				return false;
			}
		}
		return true;
	}
	public static boolean hasOneEmpty(Object... args) {
		if (args == null || args.length == 0) {
			return true;
		}
		for (Object one :
				args) {
			if (one == null) {
				return true;
			}
		}
		return false;
	}

	public static String changeNull(String b)
	{
		if(isNull(b))
			return "";
		else
			return b;
	}
	
	public static boolean isInteger(String b)
	{
		if(isNull(b))
		{
			return false;
		}
		else
		{
	        try 
	        {  
	        		Integer.parseInt(b);
	        		return true;	
	        }
	        catch(Exception e) {  
	        		return false;
	        }
		}
	}
	
	public static String filterOffUtf8Mb4_2(String text) throws UnsupportedEncodingException {
		byte[] bytes = text.getBytes("utf-8");
		ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
		int i = 0;
		while (i < bytes.length) {
		short b = bytes[i];
		if (b > 0) {
		buffer.put(bytes[i++]);
		continue;
		}

		b += 256; //去掉符号位

		if (((b >> 5) ^ 0x06) == 0) {
		buffer.put(bytes, i, 2);
		i += 2;
		System.out.println("2");
		} else if (((b >> 4) ^ 0x0E) == 0) {
		System.out.println("3");
		buffer.put(bytes, i, 3);
		i += 3;
		} else if (((b >> 3) ^ 0x1E) == 0) {
		i += 4;
		System.out.println("4");
		} else if (((b >> 2) ^ 0xBE) == 0) {
		i += 5;
		System.out.println("5");
		} else {
		i += 6;
		System.out.println("6");
		}
		}
		buffer.flip();
		return new String(buffer.array(), "utf-8");
		}
	
	
	/**
	 * 过滤掉字符串中含有的非UTF8的字符
	 * @author    秦晓宇
	 * @date      2017年12月24日 下午10:46:42 
	 * @param text
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	static public String filterOffUtf8Mb4(String text) throws UnsupportedEncodingException {  
	    byte[] bytes = text.getBytes("UTF-8");  
	    ByteBuffer buffer = ByteBuffer.allocate(bytes.length);  
	    int i = 0;  
	    while (i < bytes.length) {  
	        short b = bytes[i];  
	        if (b > 0) {  
	            buffer.put(bytes[i++]);  
	            continue;  
	        }  
	        b += 256;  
	        if ((b ^ 0xC0) >> 4 == 0) {  
	            buffer.put(bytes, i, 2);  
	            i += 2;  
	        }  
	        else if ((b ^ 0xE0) >> 4 == 0) {  
	            buffer.put(bytes, i, 3);  
	            i += 3;  
	        }  
	        else if ((b ^ 0xF0) >> 4 == 0) {  
	            i += 4;  
	        }  
	    }  
	    buffer.flip();  
	    return new String(buffer.array(), "utf-8");  
	}


	public static final Gson GSON = new Gson();
	
}
