package com.shuohe.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class SDate {

	private static void debug(String s)
	{
		System.out.println(s);
	}
	
//	public static void main(String[] args)
//	{
////		debug("getSystemDate = "+getSystemDate());
////		debug("getSystemDate = "+praseStringToDate("2017-01-03 14:30:22"));
//		//debug(""+getHour("2017-01-03"));
//		try {
//			System.out.println(dateAddMonth(new Date(),3));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
	
	public static String getDateYMDToString(String date)
	{
		Date d = praseStringToDate(date);
		if(d == null)
		{
			return "";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String x = simpleDateFormat.format(d);
		return x;
	}
	
	
	
	/**
	 * 获得系统时间
	 * @author    秦晓宇
	 * @date      2016年9月6日 下午4:43:23 
	 * @return
	 */
	public static String getSystemDateToString()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = simpleDateFormat.format(new java.util.Date());
		return date;
	}
	
	public static String getSystemDateYMDToString()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = simpleDateFormat.format(new java.util.Date());
		return date;
	}
	
	public static String getSystemDateYMD2ToString()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		String date = simpleDateFormat.format(new java.util.Date());
		return date;
	}
	
	/**
	 * 获得年
	 * @author    秦晓宇
	 * @date      2017年1月16日 下午2:40:35 
	 * @return
	 */
	public static int getYear()
	{
		Date currTime = new Date();
		return currTime.getYear()+1900;//年
	}
	/**
	 * 获得月
	 * @author    秦晓宇
	 * @date      2017年1月16日 下午2:40:51 
	 * @return
	 */
	public static int getMonth()
	{
		Date currTime = new Date();
		return currTime.getMonth()+1;//月
	}
	/**
	 * 获得日
	 * @author    秦晓宇
	 * @date      2017年1月16日 下午2:40:56 
	 * @return
	 */
	public static int getDate()
	{
		Date currTime = new Date();
		return currTime.getDate();//年
	}
	
	/**
	 * 用于数据库Date数据插入，请勿其他地方使用
	 * @author    秦晓宇
	 * @date      2017年1月3日 下午3:19:54 
	 * @param date
	 * @return
	 */
	public static String praseDate(Date date)
	{
		String ret = null;
		try  
		{  
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ret = "\'"+simpleDateFormat.format(date)+"\'";
		}  
		catch (Exception e)  
		{  
			e.printStackTrace();
		}		
		return ret;
	}
	public static String praseYMDHMSDate(Date date)
	{
		String ret = null;
		try  
		{  
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ret = simpleDateFormat.format(date);
		}  
		catch (Exception e)  
		{  
			e.printStackTrace();
		}		
		return ret;
	}
	
	
	public static Date getSystemDate()
	{
		return new java.util.Date();
	}

	public static Date praseStringToDate(String dateString)
	{
		Date date=null;
		try  
		{  
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		    date = simpleDateFormat.parse(dateString);  
		}  
		catch (ParseException e)  
		{  
		    System.out.println(e.getMessage());  
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
		    try {
				date = simpleDateFormat.parse(dateString);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}  
		}  
		return date;
	}
	public static String get30DayBeforeDate()
	{
		//获取当前日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		String endDate = sdf.format(today);//当前日期

		//获取三十天前日期
		Calendar theCa = Calendar.getInstance();
		theCa.setTime(today);
		theCa.add(theCa.DATE, -30);//最后一个数字30可改，30天的意思
		Date start = theCa.getTime();
		String startDate = sdf.format(start);//三十天之前日期
		return startDate;
	}
	
	/**
	 * 获得String的时
	 * @author    秦晓宇
	 * @date      2017年4月30日 下午11:09:29 
	 * @param date
	 * @return
	 */
	public static int getHour(String date)
	{
		Date d = praseStringToDate(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour;
	}
	/**
	 * 获得String的分
	 * @author    秦晓宇
	 * @date      2017年4月30日 下午11:10:54 
	 * @param date
	 * @return
	 */
	public static int getMinute(String date)
	{
		Date d = praseStringToDate(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int hour = calendar.get(Calendar.MINUTE);
		return hour;
	}
	/**
	 * 获得String的秒
	 * @author    秦晓宇
	 * @date      2017年4月30日 下午11:10:56 
	 * @param date
	 * @return
	 */
	public static int getSecond(String date)
	{
		Date d = praseStringToDate(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int hour = calendar.get(Calendar.SECOND);
		return hour;
	}
	/**
	 * 获得String的毫秒
	 * @author    秦晓宇
	 * @date      2017年4月30日 下午11:10:59 
	 * @param date
	 * @return
	 */
	public static int getMm(String date)
	{
		Date d = praseStringToDate(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		int hour = calendar.get(Calendar.MILLISECOND);
		return hour;
	}
	
	
	 public static Date dateAddMonth(Date date,int month) throws Exception {

		  //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		  Calendar rightNow = Calendar.getInstance();
		  rightNow.setTime(date);
		  rightNow.add(Calendar.MONTH, month);// 日期加3个月
		  Date dt1 = rightNow.getTime();
		  return dt1;
	} 
	 
	 /**
	  * 计算连个日期相差的天数
	  * @param smdate较小的日期
	  * @param bdate较大的日期
	  * @return
	  * @throws ParseException
	  */
	 public static int daysBetween(String smdate,String bdate) throws ParseException 
	 { 
		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
		 Calendar cal = Calendar.getInstance(); 
		 cal.setTime(sdf.parse(smdate)); 
		 long time1 = cal.getTimeInMillis(); 
		 cal.setTime(sdf.parse(bdate)); 
		 long time2 = cal.getTimeInMillis(); 
		 long between_days=(time2-time1)/(1000*3600*24); 
		 return Integer.parseInt(String.valueOf(between_days))+1; 
	 }
	 
	 
	 public static Map<String,String> showResult(List<String> dateList) {
		 Map<String, String> map = new TreeMap<String, String>();
	        Map<String, Integer> dateMap = new TreeMap<String, Integer>();
	        int i, arrayLen;
	        arrayLen = dateList.size();
	        for(i = 0; i < arrayLen; i++){
	            String dateKey = dateList.get(i);
	            if(dateMap.containsKey(dateKey)){
	                int value = dateMap.get(dateKey) + 1;
	                dateMap.put(dateKey, value);
	            }else{
	                dateMap.put(dateKey, 1);
	            }
	        }
	        Set<String> keySet = dateMap.keySet();
	        String []sorttedArray = new String[keySet.size()];
	        Iterator<String> iter = keySet.iterator();
	        int index = 0;
	        while (iter.hasNext()) {
	            String key = iter.next();
	        //    System.out.println(key + ":" + dateMap.get(key));
	            sorttedArray[index++] = key;
	        }
	        int sorttedArrayLen = sorttedArray.length;
	        System.out.println("最小日期是：" + sorttedArray[0] + "," +
	                " 天数为" + dateMap.get(sorttedArray[0]));
	        System.out.println("最大日期是：" + sorttedArray[sorttedArrayLen - 1] + "," +
	                " 天数为" + dateMap.get(sorttedArray[sorttedArrayLen - 1]));
	        map.put("minDate", sorttedArray[0]);
	        map.put("maxDate", sorttedArray[sorttedArrayLen - 1]);
	        return map;
	    }
	 public static void main(String[] args) throws Exception {
		 List<String> l = new ArrayList<String>();
		 l.add("2018-07-31");
		 l.add("2018-08-06");
		 l.add("2018-08-08");
		 l.add("2018-08-10");
		 l.add("2018-08-17");
		 l.add("2018-09-02");
		 showResult(l);
		 
	}
	

	
}
