package com.shuohe.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class QFile {
	/**
     * 删除文件，可以是文件或文件夹
     *
     * @param fileName
     *            要删除的文件名
     * @return 删除成功返回true，否则返回false
     */
    public static boolean delete(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            System.out.println("删除文件失败:" + fileName + "不存在！");
            return false;
        } else {
            if (file.isFile())
                return deleteFile(fileName);
            else
                return deleteDirectory(fileName);
        }
    }
    /**
     * 删除单个文件
     *
     * @param fileName
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

    /**
     * 删除目录及目录下的文件
     *
     * @param dir
     *            要删除的目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String dir) {
        // 如果dir不以文件分隔符结尾，自动添加文件分隔符
        if (!dir.endsWith(File.separator))
            dir = dir + File.separator;
        File dirFile = new File(dir);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if ((!dirFile.exists()) || (!dirFile.isDirectory())) {
            System.out.println("删除目录失败：" + dir + "不存在！");
            return false;
        }
        boolean flag = true;
        // 删除文件夹中的所有文件包括子目录
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            // 删除子文件
            if (files[i].isFile()) {
                flag = QFile.deleteFile(files[i].getAbsolutePath());
                if (!flag)
                    break;
            }
            // 删除子目录
            else if (files[i].isDirectory()) {
                flag = QFile.deleteDirectory(files[i]
                        .getAbsolutePath());
                if (!flag)
                    break;
            }
        }
        if (!flag) {
            System.out.println("删除目录失败！");
            return false;
        }
        // 删除当前目录
        if (dirFile.delete()) {
            System.out.println("删除目录" + dir + "成功！");
            return true;
        } else {
            return false;
        }
    }
    
    /**
	 * 读文件
	 * @author    秦晓宇
	 * @date      2016年4月14日 下午3:00:32 
	 * @param filePath
	 * 			-文件路径
	 * @return
	 * 			- 文件内容。若未读到文件则返回null
	 */
	public static String read(String filePath)
	{
		if(filePath == null) return null;
		String ret = "";
		
		File file = new File(filePath);
    	if(!file.exists())
		{ 
			try 
			{
				file.createNewFile();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
    	
		FileReader fr = null;
		BufferedReader br = null;
		try 
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String s = null;
			try 
			{
				while((s = br.readLine()) != null)
				{
					s = s+"\n";
					ret += s;
				}
		        br.close();  
		        fr.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
//			debug("读文件"+ret);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * 获取文件的拓展名
	 * @author    秦晓宇
	 * @date      2016年4月14日 下午3:07:07 
	 * @param file
	 * @return
	 */
	public static String getFileSuffix(File file) 
	{
		String type = "*/*";
		String fileName = file.getName();
		int dotIndex = fileName.indexOf('.');
		if((dotIndex >-1) && (dotIndex < (fileName.length() - 1))) 
		{    
            return fileName.substring(dotIndex + 1);    
        }    
		return type;
	}
	public static String getFileSuffix(String fileName) 
	{
		String type = "*/*";
		int dotIndex = fileName.indexOf('.');
		if((dotIndex >-1) && (dotIndex < (fileName.length() - 1))) 
		{    
            return fileName.substring(dotIndex + 1);    
        }    
		return type;
	}
    
	public static String read(String filePath,String encode)
	{
		if(filePath == null) return null;
		String ret = "";
		
		File file = new File(filePath);
    	if(!file.exists())
		{ 
			try 
			{
				file.createNewFile();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
    	
//		FileReader fr = null;
		BufferedReader br = null;
//		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("e:/read.txt"),"GBK"));
		try 
		{
//			fr = new FileReader(filePath);
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath),encode));
//			br = new BufferedReader(fr);
			String s = null;
			try 
			{
				while((s = br.readLine()) != null)
				{
					s = s+"\n";
					ret += s;
				}
		        br.close();  
//		        fr.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
//			debug("读文件"+ret);
		} 
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return ret;
	}
}
