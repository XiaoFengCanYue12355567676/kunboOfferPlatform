package com.shuohe.util.easyuiBean;

import java.util.List;
import java.util.Map;

public class EasyUiDataGrid {
	private long total; 
	private List<Map<String,Object>> rows;
	public EasyUiDataGrid(long total, List rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<Map<String, Object>> getRows() {
		return rows;
	}
	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}
	
	
}
