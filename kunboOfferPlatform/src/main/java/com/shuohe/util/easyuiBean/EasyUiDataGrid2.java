package com.shuohe.util.easyuiBean;

import java.util.List;
import java.util.Map;

public class EasyUiDataGrid2 {
	private long total; 
	private List<Object> rows;
	public EasyUiDataGrid2(long total, List rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<Object> getRows() {
		return rows;
	}
	public void setRows(List<Object> rows) {
		this.rows = rows;
	}	
}
