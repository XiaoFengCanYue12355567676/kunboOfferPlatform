package com.kunbo.shop.service.component;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kunbo.shop.entity.component.StandardComponent;
import com.kunbo.shop.entity.component.StandardType;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public interface StandardService {
	//标准品类型
	public ReturnBean insertStandardType(StandardType standardType);
	public ReturnBean deleteStandardTypeById(String id);
	public List<StandardType> getStandardTypeForList();
	public StandardType findStandardTypeById(String id);
	

	//标准品组件
	public ReturnBean insertStandardComponent(StandardComponent standardComponent);
	public ReturnBean deleteStandardComponentById(String id);
	public List<StandardComponent> getStandardComponentForListByType(String typeId);
	public ReturnBean deleteStandardComponentByTypeId(String id);
	public StandardComponent findStandardComponentById(String id);
}
