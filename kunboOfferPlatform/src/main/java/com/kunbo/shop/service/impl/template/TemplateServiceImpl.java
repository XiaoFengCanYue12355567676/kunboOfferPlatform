package com.kunbo.shop.service.impl.template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.dao.product.ProductAssortPortDao;
import com.kunbo.shop.dao.product.ProductHalfProductDao;
import com.kunbo.shop.dao.product.ProductInfoDao;
import com.kunbo.shop.dao.product.ProductManHourDao;
import com.kunbo.shop.dao.product.ProductMaterialDao;
import com.kunbo.shop.dao.product.ProductParameterDao;
import com.kunbo.shop.dao.product.ProductStandardDao;
import com.kunbo.shop.dao.sort.ProductSortDao;
import com.kunbo.shop.dao.template.TemplateAssortPortDao;
import com.kunbo.shop.dao.template.TemplateHalfProductDao;
import com.kunbo.shop.dao.template.TemplateInfoDao;
import com.kunbo.shop.dao.template.TemplateManHourDao;
import com.kunbo.shop.dao.template.TemplateMaterialDao;
import com.kunbo.shop.dao.template.TemplateParameterDao;
import com.kunbo.shop.dao.template.TemplateStandardDao;
import com.kunbo.shop.entity.Product.ProductAssortPort;
import com.kunbo.shop.entity.Product.ProductHalfProduct;
import com.kunbo.shop.entity.Product.ProductInfo;
import com.kunbo.shop.entity.Product.ProductManHour;
import com.kunbo.shop.entity.Product.ProductMaterial;
import com.kunbo.shop.entity.Product.ProductParameter;
import com.kunbo.shop.entity.Product.ProductStandard;
import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.TemplateAssortPort;
import com.kunbo.shop.entity.template.TemplateHalfProduct;
import com.kunbo.shop.entity.template.TemplateInfo;
import com.kunbo.shop.entity.template.TemplateManHour;
import com.kunbo.shop.entity.template.TemplateMaterial;
import com.kunbo.shop.entity.template.TemplateParameter;
import com.kunbo.shop.entity.template.TemplateStandard;
import com.kunbo.shop.service.impl.product.ProductServiceImpl;
import com.kunbo.shop.service.product.ProductService;
import com.kunbo.shop.service.template.TemplateService;
import com.shuohe.util.returnBean.ReturnBean;


@Service
public class TemplateServiceImpl implements TemplateService{

	@Autowired  TemplateAssortPortDao 		templateAssortPortDao;
	@Autowired  TemplateHalfProductDao 		templateHalfProductDao;
	@Autowired  TemplateInfoDao 			templateInfoDao;
	@Autowired  TemplateManHourDao 			templateManHourDao;
	@Autowired  TemplateMaterialDao 		templateMaterialDao;
	@Autowired  TemplateParameterDao 		templateParameterDao;
	@Autowired  TemplateStandardDao 		templateStandardDao;
	
	@Autowired ProductSortDao productSortDao;
	@Autowired ProductInfoDao productInfoDao;
	@Autowired ProductParameterDao productParameterDao;
	@Autowired ProductAssortPortDao productAssortPortDao;
	
	@Autowired ProductHalfProductDao productHalfProductDao;
	@Autowired ProductManHourDao productManHourDao;
	@Autowired ProductStandardDao productStandardDao;
	@Autowired ProductMaterialDao productMaterialDao;
	
	@Autowired ProductServiceImpl productService;
	
	
	private Logger logger = LoggerFactory.getLogger(TemplateServiceImpl.class);
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public ReturnBean insertAssortPort(int level,String pid,TemplateAssortPort templateAssortPort) {
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateAssortPort.getSortId());
		templateInfo.setText(templateAssortPort.getText());
		templateInfo.setAttriType(AttriType.ASSORT_PORT);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateAssortPort.setTemplateInfoId(templateInfo.getId());		
		templateAssortPortDao.save(templateAssortPort);		
		
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateAssortPort.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductAssortPort productAssortPort = productService.getProductAssortPortByTemplateInfo(p,productInfo,templateInfo);  //.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productAssortPortDao.save(productAssortPort);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}

		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteAssortPortById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean deleteAssortPort(TemplateAssortPort templateAssortPort) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateAssortPort findAssortPortById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean insertHalfProduct(int level,String pid,TemplateHalfProduct templateHalfProduct) {
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateHalfProduct.getSortId());
		templateInfo.setText(templateHalfProduct.getText());
		templateInfo.setUnit(templateHalfProduct.getUnit());
		templateInfo.setAttriType(AttriType.HALF_PRODUCT);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}	
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateHalfProduct.setTemplateInfoId(templateInfo.getId());		
		templateHalfProductDao.save(templateHalfProduct);	
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateHalfProduct.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductHalfProduct productHalfProduct = productService.getProductHalfProductByTemplateInfo(p,productInfo,templateInfo);  //.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productHalfProductDao.save(productHalfProduct);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}

		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteHalfProductById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean deleteHalfProduct(TemplateHalfProduct templateHalfProduct) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateHalfProduct findHalfProductById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean insertManHour(int level,String pid,TemplateManHour templateManHour) {
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateManHour.getSortId());
		templateInfo.setText(templateManHour.getText());
		templateInfo.setUnit(templateManHour.getUnit());
		templateInfo.setAttriType(AttriType.MANHOUR);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}	
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateManHour.setTemplateInfoId(templateInfo.getId());		
		templateManHourDao.save(templateManHour);		
		
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateManHour.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductManHour productManHour = productService.getProductManHourByTemplateInfo(p,productInfo,templateInfo);  //.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productManHourDao.save(productManHour);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteManHourById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean deleteManHour(TemplateManHour templateManHour) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateManHour findManHourById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean insertMaterial(int level,String pid,TemplateMaterial templateMaterial) {
		logger.info("insertMaterial");
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateMaterial.getSortId());
		templateInfo.setText(templateMaterial.getText());
		templateInfo.setUnit(templateMaterial.getUnit());
		templateInfo.setAttriType(AttriType.MATERIAL);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateMaterial.setTemplateInfoId(templateInfo.getId());		
		templateMaterialDao.save(templateMaterial);		
		
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateMaterial.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductMaterial productMaterial = productService.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productMaterialDao.save(productMaterial);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}

		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteMaterialById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean deleteMaterial(TemplateMaterial templateMaterial) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateMaterial findMaterialById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean insertParameter(int level,String pid,TemplateParameter templateParameter) {
		// TODO Auto-generated method stub
		
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateParameter.getSortId());
		templateInfo.setText(templateParameter.getText());
		templateInfo.setUnit(templateParameter.getUnit());
		templateInfo.setAttriType(AttriType.PARAMETER);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}		
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateParameter.setTemplateInfoId(templateInfo.getId());		
		templateParameterDao.save(templateParameter);	
		
		//获得新增参数的模型编号
		//获得该模型编号下的所有产品
		//为所有产品插入新增参数
		
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateParameter.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductParameter productParameter = productService.getProductParameterByTemplateInfo(p,productInfo,templateInfo);  //.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productParameterDao.save(productParameter);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteParameterById(String id) {
		// TODO Auto-generated method stub
//		templateParameterDao.deleteByTemplateId(id);
		templateParameterDao.deleteById(id);
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteParameter(TemplateParameter templateParameter) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateParameter findParameterById(String id) {
		// TODO Auto-generated method stub		
		return templateParameterDao.findById(id);
	}
	@Override
	public ReturnBean insertStandard(int level,String pid,TemplateStandard templateStandard) {
		TemplateInfo templateInfo = new TemplateInfo();
		templateInfo.setSortId(templateStandard.getSortId());
		templateInfo.setText(templateStandard.getText());
		templateInfo.setAttriType(AttriType.STANDARD_PORT);
		if(pid == null||pid.equals(""))
		{
			pid = "-";
//			templateInfo.setLevel(0);
		}
//		else
//		{
			templateInfo.setLevel(level+1);
//		}	
		templateInfo.setPid(pid);
		templateInfoDao.save(templateInfo);
		templateStandard.setTemplateInfoId(templateInfo.getId());		
		templateStandardDao.save(templateStandard);		
		
		//log:2019-05-15 增加修改模型修改产品实体的功能
		//根据sortid找到所有的产品,为所有产品添加info，为所有产品添加材料实体

		List<ProductSort> productList = productSortDao.findByPid(templateStandard.getSortId());
		for(ProductSort p:productList)
		{
			logger.info("自动增加一个材料："+p.getText());
			ProductInfo productInfo = productService.geProductInfoByTemplateInfo(p,templateInfo);
			productInfoDao.save(productInfo);
			ProductStandard productStandard = productService.getProductStandardByTemplateInfo(p,productInfo,templateInfo);  //.getProductMaterialByTemplateInfo(p,productInfo,templateInfo);
			productStandardDao.save(productStandard);
			//重新计算这个产品的所有价格
			productService.calculatorPorductPrice(p.getId());
		}
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public ReturnBean deleteStandardById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ReturnBean deleteStandard(TemplateStandard templateStandard) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TemplateStandard findStandardById(String id) {
		return templateStandardDao.findById(id);
	}
	@Override
	public List<TemplateInfo> getInfoBySortIdForTree(String sortId) {
		List<TemplateInfo> list = templateInfoDao.findBySortId(sortId);
//		System.out.println(Json.toJson(list));
		return getChildsManyGroup(list,"-");
	}
	
	/**
	 * 根据id返回所有子集分类,(多维List集合,List中含有子集List) 
	 * @author    秦晓宇
	 * @date      2017年1月10日 下午10:40:12 
	 * @param list
	 * @param pid
	 * @return
	 */
	public List<TemplateInfo> getChildsManyGroup(List<TemplateInfo> list,String pid)
	{  
		List<TemplateInfo> arr = new ArrayList<TemplateInfo>();  
		for(TemplateInfo location : list){
			if(location.getAttriType() == AttriType.MATERIAL)
			{
				TemplateMaterial tm = templateMaterialDao.findBytemplateInfoId(location.getId());
				location.setFormula(tm.getFormula());
			}
			if(location.getAttriType() == AttriType.HALF_PRODUCT)
			{
				TemplateHalfProduct thp = templateHalfProductDao.findBytemplateInfoId(location.getId());
				location.setFormula(thp.getFormula());
			}
		    if(pid.equals(location.getPid())){  
		        location.setChildren(getChildsManyGroup(list, location.getId()));    
		        arr.add(location);   
		    }  
		}  
//		System.out.println("arr = "+Json.toJson(arr));
		return arr;  
	}
	@Override
	public ReturnBean updateParameter(TemplateParameter templateParameter) {
		// TODO Auto-generated method stub
		//获取概览信息
		TemplateInfo templateInfo = templateInfoDao.findById(templateParameter.getTemplateInfoId());

		templateInfo.setText(templateParameter.getText());
		templateInfo.setUnit(templateParameter.getUnit());

		templateInfoDao.save(templateInfo);
		templateParameterDao.save(templateParameter);	
		
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateParameter.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductParameter> productParameterList = productParameterDao.findByTemplateId(templateParameter.getId());
		List<ProductParameter> productMaterialList_t = new ArrayList<ProductParameter>();
		for(ProductParameter p:productParameterList)
		{
			logger.info("updateParameter = "+templateParameter.getText());
			p.setText(templateParameter.getText());
			p.setUnit(templateParameter.getUnit());
			productMaterialList_t.add(p);
		}
		productParameterDao.save(productMaterialList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateParameter findParameterByTemplateId(String id) {
		return templateParameterDao.findBytemplateInfoId(id);
	}
	@Override
	public ReturnBean updateAssortPort(TemplateAssortPort templateAssortPort) {
		// TODO Auto-generated method stub
		//获取概览信息
		TemplateInfo templateInfo = templateInfoDao.findById(templateAssortPort.getTemplateInfoId());

		templateInfo.setText(templateAssortPort.getText());

		templateInfoDao.save(templateInfo);
		templateAssortPortDao.save(templateAssortPort);	
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateInfo.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductAssortPort> productAssortPortList = productAssortPortDao.findByTemplateId(templateAssortPort.getId());
		List<ProductAssortPort>  productAssortPortList_t = new ArrayList<ProductAssortPort>();
		for(ProductAssortPort p:productAssortPortList)
		{
			logger.info("updateAssortPort = "+templateAssortPort.getText());
			p.setText(templateAssortPort.getText());
			p.setNum(templateAssortPort.getNum());
			productAssortPortList_t.add(p);
		}
		productAssortPortDao.save(productAssortPortList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		
		
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateAssortPort findAssortPortByTemplateId(String id) {
		return templateAssortPortDao.findBytemplateInfoId(id);
	}
	@Override
	public ReturnBean updateHalfProduct(TemplateHalfProduct templateHalfProduct) {
		// TODO Auto-generated method stub
		TemplateInfo templateInfo = templateInfoDao.findById(templateHalfProduct.getTemplateInfoId());

		templateInfo.setText(templateHalfProduct.getText());
		templateInfo.setUnit(templateHalfProduct.getUnit());

		templateInfoDao.save(templateInfo);
		templateHalfProductDao.save(templateHalfProduct);	
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateInfo.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductHalfProduct> productAssortPortList = productHalfProductDao.findByTemplateId(templateHalfProduct.getId());
		List<ProductHalfProduct>  productAssortPortList_t = new ArrayList<ProductHalfProduct>();
		for(ProductHalfProduct p:productAssortPortList)
		{
			logger.info("updateHalfProduct = "+templateHalfProduct.getText());
			p.setText(templateHalfProduct.getText());
			p.setOptionType(templateHalfProduct.getOptionType());
			p.setFormula(templateHalfProduct.getFormula());
			productAssortPortList_t.add(p);
		}
		productHalfProductDao.save(productAssortPortList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateHalfProduct findHalfProductByTemplateId(String id) {
		return templateHalfProductDao.findBytemplateInfoId(id);
	}
	@Override
	public ReturnBean updateManHour(TemplateManHour templateManHour) {
		// TODO Auto-generated method stub
		TemplateInfo templateInfo = templateInfoDao.findById(templateManHour.getTemplateInfoId());

		templateInfo.setText(templateManHour.getText());
		templateInfo.setUnit(templateManHour.getUnit());

		templateInfoDao.save(templateInfo);
		templateManHourDao.save(templateManHour);	
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateInfo.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductManHour> productAssortPortList = productManHourDao.findByTemplateId(templateManHour.getId());
		List<ProductManHour>  productAssortPortList_t = new ArrayList<ProductManHour>();
		for(ProductManHour p:productAssortPortList)
		{
			logger.info("updateManHour = "+templateManHour.getText());
			p.setText(templateManHour.getText());
			p.setManhourName(templateManHour.getManhourName());
			p.setManhourId(templateManHour.getManhourId());
			p.setUnitPrice(templateManHour.getUnitPrice());
			productAssortPortList_t.add(p);
		}
		productManHourDao.save(productAssortPortList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateManHour findManHourByTemplateId(String id) {
		return templateManHourDao.findBytemplateInfoId(id);
	}
	@Override
	public ReturnBean updateMaterial(TemplateMaterial templateMaterial) {
		// TODO Auto-generated method stub
		TemplateInfo templateInfo = templateInfoDao.findById(templateMaterial.getTemplateInfoId());

		templateInfo.setText(templateMaterial.getText());
		templateInfo.setUnit(templateMaterial.getUnit());

		templateInfoDao.save(templateInfo);
		templateMaterialDao.save(templateMaterial);	
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateInfo.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductMaterial> productMaterialList = productMaterialDao.findByTemplateId(templateMaterial.getId());
		List<ProductMaterial> productMaterialList_t = new ArrayList<ProductMaterial>();
		for(ProductMaterial p:productMaterialList)
		{
			logger.info("updateMaterial = "+templateMaterial.getText());
			p.setText(templateMaterial.getText());
			p.setFormula(templateMaterial.getFormula());
			productMaterialList_t.add(p);
		}
		productMaterialDao.save(productMaterialList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateMaterial findMaterialByTemplateId(String id) {
		return templateMaterialDao.findBytemplateInfoId(id);
	}
	@Override
	public ReturnBean updateStandard(TemplateStandard templateStandard) {
		TemplateInfo templateInfo = templateInfoDao.findById(templateStandard.getTemplateInfoId());

		templateInfo.setText(templateStandard.getText());

		templateInfoDao.save(templateInfo);
		templateStandardDao.save(templateStandard);	
		
		//更新所有该模型下的产品信息，和模型保持一致
		//log:2019-05-15 增加修改模型修改产品实体的功能
		
		List<ProductInfo> productInfoList = productInfoDao.findByTemplateInfoId(templateInfo.getId());
		List<ProductInfo> productInfoList_t = new ArrayList<ProductInfo>();
		for(ProductInfo p:productInfoList)
		{
			p.setText(templateInfo.getText());
			p.setLevel(templateInfo.getLevel());
			p.setFilter(templateInfo.isFilter());
			p.setQuote(templateInfo.isQuote());
			p.setCalculateSequ(templateInfo.getCalculateSequ());
			productInfoList_t.add(p);
		}
		productInfoDao.save(productInfoList_t);
		
		List<ProductStandard> productMaterialList = productStandardDao.findByTemplateId(templateStandard.getId());
		List<ProductStandard> productMaterialList_t = new ArrayList<ProductStandard>();
		for(ProductStandard p:productMaterialList)
		{
			logger.info("updateStandard = "+templateStandard.getText());
			p.setText(templateStandard.getText());
			p.setStandardName(templateStandard.getStandardName());
			productMaterialList_t.add(p);
		}
		productStandardDao.save(productMaterialList_t);
		//log:2019-05-15 增加修改模型修改产品实体的功能 -end
		productService.updateAllProductByTemplateChange(templateInfo.getSortId());
		return new ReturnBean(true,"save success");
	}
	@Override
	public TemplateStandard findStandardByTemplateId(String id) {
		return templateStandardDao.findBytemplateInfoId(id);
	}
	@Transactional
	@Override
	public ReturnBean deleteAssortPortByTemplateId(String id) {
		try
		{
			templateAssortPortDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Transactional
	@Override
	public ReturnBean deleteHalfProductByTemplateId(String id) {
		try
		{		
			templateHalfProductDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Transactional
	@Override
	public ReturnBean deleteManHourByTemplateId(String id) {
		try
		{		
			templateManHourDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Transactional
	@Override
	public ReturnBean deleteMaterialByTemplateId(String id) {
		try
		{		
			templateMaterialDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Transactional
	@Override
	public ReturnBean deleteParameterByTemplateId(String id) {
		try
		{		
			templateParameterDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Transactional
	@Override
	public ReturnBean deleteStandardByTemplateId(String id) {
		try
		{		
			templateStandardDao.deleteByTemplateId(id);
			templateInfoDao.deleteById(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	

	@Override
	public ReturnBean deleteInfoById(String id) {
		// TODO Auto-generated method stub
		TemplateInfo templateInfo = templateInfoDao.findById(id);
		try {
			switch(templateInfo.getAttriType())
			{
				case HALF_PRODUCT:	//半成品
					templateHalfProductDao.deleteByTemplateId(id);
					productHalfProductDao.deleteByTemplateId(id);
				break;
				case PARAMETER:		//参数
					templateParameterDao.deleteByTemplateId(id);
					productParameterDao.deleteByTemplateId(id);
				break;				
				case STANDARD_PORT:	//标准品
					templateStandardDao.deleteByTemplateId(id);
					productStandardDao.deleteByTemplateId(id);
				break;				
				case MANHOUR:		//工时
					templateManHourDao.deleteByTemplateId(id);
					productManHourDao.deleteByTemplateId(id);
				break;				
				case MATERIAL:		//材料
					templateMaterialDao.deleteByTemplateId(id);
					productMaterialDao.deleteByTemplateId(id);
				break;				
				case ASSORT_PORT:		//配套件
					templateAssortPortDao.deleteByTemplateId(id);
					productAssortPortDao.deleteByTemplateId(id);
				break;				
			}
			templateInfoDao.deleteById(id);
			productInfoDao.deleteByTemplateInfoId(id);
			return new ReturnBean(true,"delete success");
		}
		catch(Exception e)
		{
			return new ReturnBean(false,e.getMessage());
		}
	}
	@Override
	public ReturnBean updateFilter(String id, boolean filter) {
		int ret = templateInfoDao.updateFilter(id, filter);		
		if(ret == 0)
			return new ReturnBean(false,"更新数据错误");
		else
			return new ReturnBean(true,"更新"+ret+"条数据");
	}
	@Override
	public ReturnBean updateQuote(String id, boolean quote) {
		int ret = templateInfoDao.updateQuote(id, quote);		
		if(ret == 0)
			return new ReturnBean(false,"更新数据错误");
		else
			return new ReturnBean(true,"更新"+ret+"条数据");
	}
	@Override
	public List<TemplateInfo> getInfoForFilter(String sortId) {
		List<TemplateInfo> list = templateInfoDao.getInfoForFilter(sortId);
		return list;
	}
	@Override
	public boolean insertProductInfoDuplicateChecking(String sortId,String text) {
		//直接比较同一产品线下的输入text和原有的是否重复
		String sql = "";
		sql += " SELECT";
		sql += " 	*";
		sql += " FROM";
		sql += " 	db_offer_template_info";
		sql += " WHERE";
		sql += " 	sortId = '"+sortId+"'";
		sql += " AND text = '"+text+"'";

		List<Map<String, Object>> l = jdbcTemplate.queryForList(sql);
		if(l==null) return true;
		else
		{
			if(l.size()==0) return true;
			else
				return false;
		}
	}
	@Override
	public boolean updateProductInfoDuplicateChecking(AttriType attriType,String id, String sortId, String text) {
		//直接比较同一产品线下的输入text和原有的是否重复
		String sql = "";
		
		sql += " SELECT";
		sql += " 	*";
		sql += " FROM";
		sql += " 	db_offer_template_info";
//		if(attriType == AttriType.ASSORT_PORT)
//			sql += " LEFT JOIN db_offer_template_assortport on db_offer_template_assortport.templateInfoId = db_offer_template_info.id";
//		else if(attriType == AttriType.HALF_PRODUCT)
//			sql += " LEFT JOIN db_offer_template_halfproduct on db_offer_template_halfproduct.templateInfoId = db_offer_template_info.id";
//		else if(attriType == AttriType.MANHOUR)
//			sql += " LEFT JOIN db_offer_template_manhour on db_offer_template_manhour.templateInfoId = db_offer_template_info.id";
//		else if(attriType == AttriType.MATERIAL)
//			sql += " LEFT JOIN db_offer_template_material on db_offer_template_material.templateInfoId = db_offer_template_info.id";
//		else if(attriType == AttriType.PARAMETER)
//			sql += " LEFT JOIN db_offer_template_parameter on db_offer_template_parameter.templateInfoId = db_offer_template_info.id";
//		else if(attriType == AttriType.STANDARD_PORT)
//			sql += " LEFT JOIN db_offer_template_standard on db_offer_template_standard.templateInfoId = db_offer_template_info.id";

		sql += " WHERE";
		sql += " 	db_offer_template_info.sortId = '"+sortId+"'";
		sql += " AND db_offer_template_info.text = '"+text+"'";
		sql += " AND db_offer_template_info.id != '"+id+"'";
//		if(attriType == AttriType.ASSORT_PORT)
//			sql += " AND db_offer_template_assortport.id != '"+id+"'";
//		else if(attriType == AttriType.HALF_PRODUCT)
//			sql += " AND db_offer_template_halfproduct.id != '"+id+"'";
//		else if(attriType == AttriType.MANHOUR)
//			sql += " AND db_offer_template_manhour.id != '"+id+"'";
//		else if(attriType == AttriType.MATERIAL)
//			sql += " AND db_offer_template_material.id != '"+id+"'";
//		else if(attriType == AttriType.PARAMETER)
//			sql += " AND db_offer_template_parameter.id != '"+id+"'";
//		else if(attriType == AttriType.STANDARD_PORT)	
//			sql += " AND db_offer_template_standard.id != '"+id+"'";

		System.out.println("sql = "+sql);
		
		List<Map<String, Object>> l = jdbcTemplate.queryForList(sql);
		if(l==null) return true;
		else
		{
			if(l.size() == 0) return true;
			else
				return false;
		}
	}
	@Override
	public ReturnBean updateCalculateSequ(String id, int calculateSequ) {
		int ret = templateInfoDao.updateCalculateSequ(id, calculateSequ);		
		if(ret == 0)
			return new ReturnBean(false,"更新数据错误");
		else
			return new ReturnBean(true,"更新"+ret+"条数据");
	}
	@Override
	public List<TemplateInfo> findAllAssertPortBySortId(String sortId) {		
		List<TemplateInfo> l = templateInfoDao.getInfoForAssortPort(sortId);
		return l;
	}
}
