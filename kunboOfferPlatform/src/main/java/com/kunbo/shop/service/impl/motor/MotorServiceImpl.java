package com.kunbo.shop.service.impl.motor;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.shop.dao.motor.DiffeVoltageDao;
import com.kunbo.shop.dao.motor.MoistTemperatureDao;
import com.kunbo.shop.dao.motor.MotorAntiExplosionDao;
import com.kunbo.shop.dao.motor.MotorBearingDao;
import com.kunbo.shop.dao.motor.MotorBrandDao;
import com.kunbo.shop.dao.motor.MotorDao;
import com.kunbo.shop.dao.motor.MotorDefendDao;
import com.kunbo.shop.dao.motor.MotorHeaterDao;
import com.kunbo.shop.dao.motor.MotorInstallationDao;
import com.kunbo.shop.dao.motor.MotorIpLevelDao;
import com.kunbo.shop.dao.motor.MotorPaddingDao;
import com.kunbo.shop.dao.motor.MotorSeriesDao;
import com.kunbo.shop.dao.motor.MotorShaftDao;
import com.kunbo.shop.dao.motor.MotorStandardNumDao;
import com.kunbo.shop.dao.motor.SeriesStandardRelDao;
import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MoistTemperature;
import com.kunbo.shop.entity.motor.Motor;
import com.kunbo.shop.entity.motor.MotorAntiExplosion;
import com.kunbo.shop.entity.motor.MotorBearing;
import com.kunbo.shop.entity.motor.MotorBrand;
import com.kunbo.shop.entity.motor.MotorDefend;
import com.kunbo.shop.entity.motor.MotorHeater;
import com.kunbo.shop.entity.motor.MotorInstallation;
import com.kunbo.shop.entity.motor.MotorIpLevel;
import com.kunbo.shop.entity.motor.MotorPadding;
import com.kunbo.shop.entity.motor.MotorSeries;
import com.kunbo.shop.entity.motor.MotorShaft;
import com.kunbo.shop.entity.motor.MotorStandardNum;
import com.kunbo.shop.entity.motor.SeriesStandardRel;
import com.kunbo.shop.service.motor.MotorService;
import com.shuohe.service.util.sql.EasyuiServiceImpl;
import com.shuohe.util.db.DbQuerySql;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public class MotorServiceImpl implements MotorService{

	
	@Autowired MotorDao motorDao;
	@Autowired MotorBrandDao motorBrandDao;
	@Autowired MotorSeriesDao motorSeriesDao;
	@Autowired MotorStandardNumDao motorStandardNumDao;
	@Autowired SeriesStandardRelDao seriesStandardRelDao;
	@Autowired DiffeVoltageDao diffeVoltageDao;
	@Autowired MotorIpLevelDao motorIpLevelDao;
	@Autowired MotorInstallationDao motorInstallationDao;
	
	@Autowired MoistTemperatureDao moistTemperatureDao;
	@Autowired MotorBearingDao motorBearingDao;
	@Autowired MotorPaddingDao motorPaddingDao;
	@Autowired MotorShaftDao motorShaftDao;
	@Autowired MotorDefendDao motorDefendDao;
	@Autowired MotorAntiExplosionDao motorAntiExplosionDao;
	@Autowired MotorHeaterDao motorHeaterDao;
	
	
	
	
	@Resource
	private EasyuiServiceImpl easyUiDto;
	
	@Override
	public ReturnBean insertMotorBrand(MotorBrand motorBrand) {
		// TODO Auto-generated method stub
		motorBrandDao.save(motorBrand);
		return new ReturnBean(true,"");
	}

	@Override
	public ReturnBean deleteMotorBrandById(String id) {
		// TODO Auto-generated method stub
		int ret = motorBrandDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public List<MotorBrand> getMotorBrandForList() {
		// TODO Auto-generated method stub
		return motorBrandDao.findAll();
	}

	@Override
	public ReturnBean insertMotor(Motor motor) {
		// TODO Auto-generated method stub
		motorDao.save(motor);
		return new ReturnBean(true,"");
	}

	@Override
	public ReturnBean deleteMotorById(String id) {
		int ret = motorDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorBrand findMotorBrandById(String id) {
		return motorBrandDao.findById(id);
	}

	@Override
	public Motor findMotorById(String id) {
		// TODO Auto-generated method stub
		return motorDao.findById(id);
	}

	@Override
	public EasyUiDataGrid findDatagridForPageByBrandId(String brandId, String page, String rows) {
		String sql = "select * from db_offer_motor_motor where brandId = '"+brandId+"'";
		String sql_count="";
		sql_count = sql;
		sql += DbQuerySql.limit(page, rows);
						
		try { 
			EasyUiDataGrid dg = easyUiDto.getEasyUiDataGrid(sql,sql_count);
			return dg;
		} catch (Exception e) {					
			e.printStackTrace();
			return new EasyUiDataGrid(0,null);
		}
	}

	@Override
	public ReturnBean insertMotorSeries(MotorSeries motorSeries) {
		motorSeriesDao.save(motorSeries);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorSeriesById(String id) {
		int ret = motorSeriesDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorSeries findMotorSeriesById(String id) {
		return motorSeriesDao.findById(id);
	}

	@Override
	public List<MotorSeries> listMotorSeries(String brandId) {
		return motorSeriesDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorStandardNum(MotorStandardNum motorStandardNum) {
		motorStandardNumDao.save(motorStandardNum);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorStandardNumById(String id) {
		int ret = motorStandardNumDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorStandardNum findMotorStandardNumById(String id) {
		return motorStandardNumDao.findById(id);
	}

	@Override
	public List<MotorStandardNum> listMotorStandardNum(String brandId) {
		return motorStandardNumDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertSeriesStandardRel(SeriesStandardRel seriesStandardRel) {
		seriesStandardRelDao.save(seriesStandardRel);
		return new ReturnBean(true,"");
	}

	@Override
	public ReturnBean deleteSeriesStandardRelById(String id) {
		int ret = seriesStandardRelDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public SeriesStandardRel findSeriesStandardRelById(String id) {
		return seriesStandardRelDao.findById(id);
	}

	@Override
	public List<SeriesStandardRel> listSeriesStandardRelBySeriesText(String seriesText,String brandId) {
		return seriesStandardRelDao.findBySeriesTextAndBrandId(seriesText, brandId);
	}

	@Override
	public ReturnBean insertDiffeVoltage(DiffeVoltage diffeVoltage) {
		diffeVoltageDao.save(diffeVoltage);
		return new ReturnBean(true,"");
	}

	@Override
	public ReturnBean deleteDiffeVoltageById(String id) {
		int ret = diffeVoltageDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public DiffeVoltage findDiffeVoltageById(String id) {
		return diffeVoltageDao.findById(id);
	}

	@Override
	public List<DiffeVoltage> listDiffeVoltageBySeriesText(String seriesText,String brandId) {
		return diffeVoltageDao.findBySeriesTextAndBrandId(seriesText,brandId);
	}

	@Override
	public ReturnBean insertMotorIpLevel(MotorIpLevel motorIpLevel) {
		motorIpLevelDao.save(motorIpLevel);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorIpLevelById(String id) {
		int ret = motorIpLevelDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorIpLevel findMotorIpLevelById(String id) {
		return motorIpLevelDao.findById(id);
	}

	@Override
	public List<MotorIpLevel> listMotorIpLevel(String brandId) {
		return motorIpLevelDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorInstallation(MotorInstallation motorInstallation) {
		motorInstallationDao.save(motorInstallation);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorInstallationById(String id) {
		int ret = motorInstallationDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorInstallation findMotorInstallationById(String id) {
		return motorInstallationDao.findById(id);
	}

	@Override
	public List<MotorInstallation> listMotorInstallation(String brandId) {
		return motorInstallationDao.findByBrandId(brandId);
	}

	@Override
	public List<Motor> findMotorForListByBrandId(String brandId) {
		return motorDao.findByBrandId(brandId);
	}

	@Override
	public List<SeriesStandardRel> findseriesStandardRelByStandardText(String standardText,String brandId) {
		return seriesStandardRelDao.findByStandardTextAndBrandId(standardText,brandId);
	}

	@Override
	public ReturnBean insertMoistTemperature(MoistTemperature moistTemperature) {
		moistTemperatureDao.save(moistTemperature);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMoistTemperatureById(String id) {
		int ret = moistTemperatureDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MoistTemperature findMoistTemperatureById(String id) {
		return moistTemperatureDao.findById(id);
	}

	@Override
	public List<MoistTemperature> listMoistTemperature() {
		return moistTemperatureDao.findAll();
	}

	@Override
	public List<MoistTemperature> listMoistTemperatureBySeriesText(String seriesText,String brandId) {
		return moistTemperatureDao.findBySeriesTextAndBrandId(seriesText,brandId);
	}

	@Override
	public ReturnBean insertMotorBearing(MotorBearing motorBearing) {
		motorBearingDao.save(motorBearing);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorBearingById(String id) {
		int ret = motorBearingDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorBearing findMotorBearingById(String id) {
		return motorBearingDao.findById(id);
	}

	@Override
	public List<MotorBearing> listMotorBearing(String brandId) {
		return motorBearingDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorPadding(MotorPadding motorPadding) {
		motorPaddingDao.save(motorPadding);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorPaddingById(String id) {
		int ret = motorPaddingDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorPadding findMotorPaddingById(String id) {
		return motorPaddingDao.findById(id);
	}

	@Override
	public List<MotorPadding> listMotorPadding(String brandId) {
		return motorPaddingDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorShaft(MotorShaft motorShaft) {
		motorShaftDao.save(motorShaft);
		return new ReturnBean(true,"");		
	}

	@Override
	public ReturnBean deleteMotorShaftById(String id) {
		int ret = motorShaftDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorShaft findMotorShaftById(String id) {
		return motorShaftDao.findById(id);
	}

	@Override
	public List<MotorShaft> listMotorShaft(String brandId) {
		return motorShaftDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorDefend(MotorDefend motorDefend) {
		motorDefendDao.save(motorDefend);
		return new ReturnBean(true,"");	
	}

	@Override
	public ReturnBean deleteMotorDefendById(String id) {
		int ret = motorDefendDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorDefend findMotorDefendById(String id) {
		return motorDefendDao.findById(id);
	}

	@Override
	public List<MotorDefend> listMotorDefend(String brandId) {
		return motorDefendDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorAntiExplosion(MotorAntiExplosion motorAntiExplosion) {
		motorAntiExplosionDao.save(motorAntiExplosion);
		return new ReturnBean(true,"");	
	}

	@Override
	public ReturnBean deleteMotorAntiExplosionById(String id) {
		int ret = motorAntiExplosionDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorAntiExplosion findMotorAntiExplosionById(String id) {
		return motorAntiExplosionDao.findById(id);
	}

	@Override
	public List<MotorAntiExplosion> listMotorAntiExplosion(String brandId) {
		return motorAntiExplosionDao.findByBrandId(brandId);
	}

	@Override
	public ReturnBean insertMotorHeater(MotorHeater motorHeater) {
		motorHeaterDao.save(motorHeater);
		return new ReturnBean(true,"");	
	}

	@Override
	public ReturnBean deleteMotorHeaterById(String id) {
		int ret = motorHeaterDao.deleteById(id);
		return new ReturnBean(true,ret+"");
	}

	@Override
	public MotorHeater findMotorHeaterById(String id) {
		return motorHeaterDao.findById(id);
	}

	@Override
	public List<MotorHeater> listMotorHeater() {
		return motorHeaterDao.findAll();
	}

	@Override
	public List<MotorHeater> listMotorHeaterBySeriesText(String seriesText,String brandId) {
		return motorHeaterDao.findBySeriesTextAndBrandId(seriesText,brandId);
	}

	@Override
	public List<MotorHeater> listMotorHeaterAll() {
		return motorHeaterDao.findAll();
	}

}
