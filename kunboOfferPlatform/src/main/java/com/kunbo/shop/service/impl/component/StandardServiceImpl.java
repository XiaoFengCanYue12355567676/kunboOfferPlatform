package com.kunbo.shop.service.impl.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.shop.dao.component.StandardComponentDao;
import com.kunbo.shop.dao.component.StandardTypeDao;
import com.kunbo.shop.entity.component.StandardComponent;
import com.kunbo.shop.entity.component.StandardType;
import com.kunbo.shop.service.component.StandardService;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public class StandardServiceImpl implements StandardService{

	
	@Autowired StandardTypeDao standardTypeDao;
	@Autowired StandardComponentDao standardComponentDao;
	
	@Override
	public ReturnBean insertStandardType(StandardType standardType) {
		standardTypeDao.save(standardType);
		return new ReturnBean(true,"save success");
	}

	@Override
	public ReturnBean deleteStandardTypeById(String id) {
		// TODO Auto-generated method stub
		standardTypeDao.deleteById(id);
		return new ReturnBean(true,"save success");
	}

	@Override
	public List<StandardType> getStandardTypeForList() {
		// TODO Auto-generated method stub
		return standardTypeDao.findAll();
	}

	@Override
	public ReturnBean insertStandardComponent(StandardComponent standardComponent) {
		// TODO Auto-generated method stub
		standardComponentDao.save(standardComponent);
		return new ReturnBean(true,"delete success");
	}

	@Override
	public ReturnBean deleteStandardComponentById(String id) {
		// TODO Auto-generated method stub
		standardComponentDao.deleteById(id);
		return new ReturnBean(true,"delete success");
	}

	@Override
	public List<StandardComponent> getStandardComponentForListByType(String typeId) {
		// TODO Auto-generated method stub
		return standardComponentDao.findByTypeId(typeId);
	}

	@Override
	public ReturnBean deleteStandardComponentByTypeId(String id) {
		int r = standardComponentDao.deleteByTypeId(id);
		return new ReturnBean(true,"delete success");
	}

	@Override
	public StandardType findStandardTypeById(String id) {
		// TODO Auto-generated method stub
		return standardTypeDao.findById(id);
	}

	@Override
	public StandardComponent findStandardComponentById(String id) {
		return standardComponentDao.findById(id);
	}

}
