package com.kunbo.shop.service.sort;

import java.util.List;
import java.util.Map;

import javax.script.ScriptException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kunbo.offer.entity.CapabilityParam;
import com.kunbo.shop.entity.sort.ProductSort;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public interface ProductSortService {
	public ReturnBean insert(ProductSort productSort);
	@Transactional
	public ReturnBean deleteById(String id);
	public ReturnBean delete(ProductSort productSort);
	/** 
	* @Description: 获取产品与类别 
	* @Title: getForTree 
	* @return List<ProductSort>
	* @author qin
	* @date 2018年11月30日下午5:32:49
	*/ 
	public List<ProductSort> getForTree();
	/** 
	* @Description: 只获取类别，不获取产品 
	* @Title: getSortTree 
	* @return List<ProductSort>
	* @author qin
	* @date 2018年11月30日下午5:32:24
	*/ 
	public List<ProductSort> getSortTree();
	public ProductSort findById(String id);
	public List<Map<String,Object>> getFanSelectList(Map<String,Object> map,Map<String,Object> objMap) throws Exception, ScriptException;
	public List<Map<String,Object>> getFanSelectListEn(Map<String,Object> map) throws Exception, ScriptException;
	//相似设计
	public List<Map<String,Object>> getFanSelectList2(Map<String,Object> map,Map<String,Object> objMap) throws Exception, ScriptException;
	/** 
	* @Description: 获取当前产品上级的实体 
	* @Title: getParentById 
	* @param id
	* @return ProductSort
	* @author qin
	* @date 2019年1月16日下午11:30:04
	*/ 
	public ProductSort getParentById(String id);
	
//	public findProductByPid();
	/**
	 * 获取变形后数据==XUE
	 * @param map
	 * @return
	 */
	public List<CapabilityParam> getPerforParam(Map<String, Object> map);
}
