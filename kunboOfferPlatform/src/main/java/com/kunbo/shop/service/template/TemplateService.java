package com.kunbo.shop.service.template;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.TemplateAssortPort;
import com.kunbo.shop.entity.template.TemplateHalfProduct;
import com.kunbo.shop.entity.template.TemplateInfo;
import com.kunbo.shop.entity.template.TemplateManHour;
import com.kunbo.shop.entity.template.TemplateMaterial;
import com.kunbo.shop.entity.template.TemplateParameter;
import com.kunbo.shop.entity.template.TemplateStandard;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public interface TemplateService {	
	//选配件
	public ReturnBean insertAssortPort(int level,String pid,TemplateAssortPort templateAssortPort);
	public ReturnBean updateAssortPort(TemplateAssortPort templateAssortPort);
	public ReturnBean deleteAssortPortById(String id);
	public ReturnBean deleteAssortPort(TemplateAssortPort templateAssortPort);
	public ReturnBean deleteAssortPortByTemplateId(String id);
	public TemplateAssortPort findAssortPortById(String id);
	public TemplateAssortPort findAssortPortByTemplateId(String id);
	public List<TemplateInfo> findAllAssertPortBySortId(String sortId);
	
	
	//半成品
	public ReturnBean insertHalfProduct(int level,String pid,TemplateHalfProduct templateHalfProduct);
	public ReturnBean updateHalfProduct(TemplateHalfProduct templateHalfProduct);
	public ReturnBean deleteHalfProductById(String id);
	public ReturnBean deleteHalfProduct(TemplateHalfProduct templateHalfProduct);
	public ReturnBean deleteHalfProductByTemplateId(String id);
	public TemplateHalfProduct findHalfProductById(String id);
	public TemplateHalfProduct findHalfProductByTemplateId(String id);
	
	//工时
	public ReturnBean insertManHour(int level,String pid,TemplateManHour templateManHour);
	public ReturnBean updateManHour(TemplateManHour templateManHour);
	public ReturnBean deleteManHourById(String id);
	public ReturnBean deleteManHour(TemplateManHour templateManHour);
	public ReturnBean deleteManHourByTemplateId(String id);
	public TemplateManHour findManHourById(String id);
	public TemplateManHour findManHourByTemplateId(String id);
	
	//原料
	public ReturnBean insertMaterial(int level,String pid,TemplateMaterial templateMaterial);
	public ReturnBean updateMaterial(TemplateMaterial templateMaterial);
	public ReturnBean deleteMaterialById(String id);
	public ReturnBean deleteMaterial(TemplateMaterial templateMaterial);
	public ReturnBean deleteMaterialByTemplateId(String id);
	public TemplateMaterial findMaterialById(String id);
	public TemplateMaterial findMaterialByTemplateId(String id);
	
	//参数
	/** 
	* @Description: 保存参数的同时保存概述信息 
	* @Title: insertParameter 
	* @param templateParameter
	* @return ReturnBean
	* @author qin
	* @date 2018年11月24日下午3:42:10
	*/ 
	public ReturnBean insertParameter(int level,String pid,TemplateParameter templateParameter);
	public ReturnBean updateParameter(TemplateParameter templateParameter);
	public ReturnBean deleteParameterById(String id);
	public ReturnBean deleteParameter(TemplateParameter templateParameter);
	public ReturnBean deleteParameterByTemplateId(String id);
	public TemplateParameter findParameterById(String id);
	public TemplateParameter findParameterByTemplateId(String id);
	
	
	//标准件
	public ReturnBean insertStandard(int level,String pid,TemplateStandard templateStandard);
	public ReturnBean updateStandard(TemplateStandard templateStandard);
	public ReturnBean deleteStandardById(String id);
	public ReturnBean deleteStandard(TemplateStandard templateStandard);
	public ReturnBean deleteStandardByTemplateId(String id);
	public TemplateStandard findStandardById(String id);
	public TemplateStandard findStandardByTemplateId(String id);
	
	
	//模板信息概览
	public List<TemplateInfo> getInfoBySortIdForTree(String sortId);
	/** 
	* @Description: 删除模板概览及其挂接的详细信息 
	* @Title: deleteInfoById 
	* @param id
	* @return ReturnBean
	* @author qin
	* @date 2018年11月24日下午11:26:55
	*/ 
	
	/** 
	* @Description: 获取模板信息，中的筛选项目 
	* @Title: getInfoForFilter 
	* @param sortId
	* @return List<TemplateInfo>
	* @author qin
	* @date 2018年12月21日下午4:56:21
	*/ 
	public List<TemplateInfo> getInfoForFilter(String sortId);
	
	
	
	public ReturnBean deleteInfoById(String id);
	
	/** 
	* @Description: 更新过滤 
	* @Title: updateFilter 
	* @param id
	* @param filter
	* @return ReturnBean
	* @author qin
	* @date 2018年12月15日上午11:38:49
	*/ 
	public ReturnBean updateFilter(String id,boolean filter);
	/** 
	* @Description: 更新报价 
	* @Title: updateQuote 
	* @param id
	* @param quote
	* @return ReturnBean
	* @author qin
	* @date 2018年12月15日上午11:38:42
	*/ 
	public ReturnBean updateQuote(String id,boolean quote);
	
	/** 
	* @Description: 更新计算顺序 
	* @Title: updateCalculateSequ 
	* @param id
	* @param calculateSequ
	* @return ReturnBean
	* @author qin
	* @date 2019年1月6日下午4:15:02
	*/ 
	public ReturnBean updateCalculateSequ(String id,int calculateSequ);
	
	/** 
	* @Description: 插入数据检查产品详情的别称是否有相同的，这将决定计算公式是否会出现相同的输入参数 
	* @Title: productInfoDuplicateChecking 
	* @param text
	* @return boolean
	* @author qin
	* @date 2019年1月5日上午10:25:49
	*/ 
	public boolean insertProductInfoDuplicateChecking(String sortId,String text);
	
	/** 
	* @Description: TODO 
	* @Title: productInfoDuplicateChecking 
	* @param id
	* @param sortId
	* @param text
	* @return boolean
	* @author qin
	* @date 2019年1月5日上午11:00:30
	*/ 
	public boolean updateProductInfoDuplicateChecking(AttriType attriType,String id,String sortId,String text);
}
