package com.kunbo.shop.service.motor;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MoistTemperature;
import com.kunbo.shop.entity.motor.Motor;
import com.kunbo.shop.entity.motor.MotorAntiExplosion;
import com.kunbo.shop.entity.motor.MotorBearing;
import com.kunbo.shop.entity.motor.MotorBrand;
import com.kunbo.shop.entity.motor.MotorDefend;
import com.kunbo.shop.entity.motor.MotorHeater;
import com.kunbo.shop.entity.motor.MotorInstallation;
import com.kunbo.shop.entity.motor.MotorIpLevel;
import com.kunbo.shop.entity.motor.MotorPadding;
import com.kunbo.shop.entity.motor.MotorSeries;
import com.kunbo.shop.entity.motor.MotorShaft;
import com.kunbo.shop.entity.motor.MotorStandardNum;
import com.kunbo.shop.entity.motor.SeriesStandardRel;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;

@Service
public interface MotorService {
	
	public ReturnBean insertMotorBrand(MotorBrand motorBrand);
	public ReturnBean deleteMotorBrandById(String id);
	public List<MotorBrand> getMotorBrandForList();
	public MotorBrand findMotorBrandById(String id);
	
	
	
	public ReturnBean insertMotor(Motor motor);
	public ReturnBean deleteMotorById(String id);
	public Motor findMotorById(String id);
	public EasyUiDataGrid findDatagridForPageByBrandId(String brandId,String page,String rows);
	public List<Motor> findMotorForListByBrandId(String brandId);
	
	
	public ReturnBean insertMotorSeries(MotorSeries motorSeries);
	public ReturnBean deleteMotorSeriesById(String id);
	public MotorSeries findMotorSeriesById(String id);
	public List<MotorSeries> listMotorSeries(String brandId);
	
	
	public ReturnBean insertMotorStandardNum(MotorStandardNum motorStandardNum);
	public ReturnBean deleteMotorStandardNumById(String id);
	public MotorStandardNum findMotorStandardNumById(String id);
	public List<MotorStandardNum> listMotorStandardNum(String brandId);
	
	
	public ReturnBean insertSeriesStandardRel(SeriesStandardRel seriesStandardRel);
	public ReturnBean deleteSeriesStandardRelById(String id);
	public SeriesStandardRel findSeriesStandardRelById(String id);
	public List<SeriesStandardRel> listSeriesStandardRelBySeriesText(String seriesText,String brandId);
	public List<SeriesStandardRel> findseriesStandardRelByStandardText(String standardText,String brandId);
	
	
	public ReturnBean insertDiffeVoltage(DiffeVoltage diffeVoltage);
	public ReturnBean deleteDiffeVoltageById(String id);
	public DiffeVoltage findDiffeVoltageById(String id);
	public List<DiffeVoltage> listDiffeVoltageBySeriesText(String seriesText,String brandId);
	
	
	
	public ReturnBean insertMotorIpLevel(MotorIpLevel motorIpLevel);
	public ReturnBean deleteMotorIpLevelById(String id);
	public MotorIpLevel findMotorIpLevelById(String id);
	public List<MotorIpLevel> listMotorIpLevel(String brandId);
	
	public ReturnBean insertMotorInstallation(MotorInstallation motorInstallation);
	public ReturnBean deleteMotorInstallationById(String id);
	public MotorInstallation findMotorInstallationById(String id);
	public List<MotorInstallation> listMotorInstallation(String brandId);
	
	
	/**********************************潮温****************************************/
	public ReturnBean insertMoistTemperature(MoistTemperature moistTemperature);
	public ReturnBean deleteMoistTemperatureById(String id);
	public MoistTemperature findMoistTemperatureById(String id);
	public List<MoistTemperature> listMoistTemperature();
	public List<MoistTemperature> listMoistTemperatureBySeriesText(String seriesText,String brandId);
	
	/**********************************轴承****************************************/
	public ReturnBean insertMotorBearing(MotorBearing motorBearing);
	public ReturnBean deleteMotorBearingById(String id);
	public MotorBearing findMotorBearingById(String id);
	public List<MotorBearing> listMotorBearing(String brandId);
	
	/**********************************填料****************************************/
	public ReturnBean insertMotorPadding(MotorPadding motorPadding);
	public ReturnBean deleteMotorPaddingById(String id);
	public MotorPadding findMotorPaddingById(String id);
	public List<MotorPadding> listMotorPadding(String brandId);
	
	/**********************************电机轴****************************************/
	public ReturnBean insertMotorShaft(MotorShaft motorShaft);
	public ReturnBean deleteMotorShaftById(String id);
	public MotorShaft findMotorShaftById(String id);
	public List<MotorShaft> listMotorShaft(String brandId);

	/**********************************防护****************************************/
	public ReturnBean insertMotorDefend(MotorDefend motorDefend);
	public ReturnBean deleteMotorDefendById(String id);
	public MotorDefend findMotorDefendById(String id);
	public List<MotorDefend> listMotorDefend(String brandId);
	
	/**********************************防爆****************************************/
	public ReturnBean insertMotorAntiExplosion(MotorAntiExplosion motorAntiExplosion);
	public ReturnBean deleteMotorAntiExplosionById(String id);
	public MotorAntiExplosion findMotorAntiExplosionById(String id);
	public List<MotorAntiExplosion> listMotorAntiExplosion(String brandId);
	
	
	/**********************************加热****************************************/
	public ReturnBean insertMotorHeater(MotorHeater motorHeater);
	public ReturnBean deleteMotorHeaterById(String id);
	public MotorHeater findMotorHeaterById(String id);
	public List<MotorHeater> listMotorHeater();
	public List<MotorHeater> listMotorHeaterBySeriesText(String seriesText,String brandId);
	public List<MotorHeater> listMotorHeaterAll();
	
}
