package com.kunbo.shop.service.product;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.kunbo.offer.entity.ManHour;
import com.kunbo.shop.controller.product.ProductController.ParamqQuerys;
import com.kunbo.shop.entity.Product.ProductAssortPort;
import com.kunbo.shop.entity.Product.ProductHalfProduct;
import com.kunbo.shop.entity.Product.ProductInfo;
import com.kunbo.shop.entity.Product.ProductManHour;
import com.kunbo.shop.entity.Product.ProductMaterial;
import com.kunbo.shop.entity.Product.ProductParameter;
import com.kunbo.shop.entity.Product.ProductStandard;
import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.entity.template.TemplateInfo;
import com.kunbo.shop.service.impl.product.ProductServiceImpl.ProductInfoView;
import com.shuohe.util.returnBean.ReturnBean;

public interface ProductService {
	
	/** 
	* @Description: 创建产品：创建产品的同时，将产品响应的配件、参数等全部copy至产品相关的表中 
	* @Title: insert 
	* @param productSort
	* @return ReturnBean
	* @author qin
	* @date 2018年11月26日下午11:29:32
	*/ 
	public ReturnBean insert(ProductSort productSort);
	public List<ProductInfoView> getProductInfoForTreeByProductId(String productId);
	public ReturnBean deleteById(String id);
	/** 
	* @Description: 删除产品详情：包含概览及其余6种详细参数 
	* @Title: deleteBySortId 
	* @param id
	* @return ReturnBean
	* @author qin
	* @date 2019年1月6日下午5:33:54
	*/ 
	@Transactional
	public ReturnBean deleteBySortId(String sortId);
	public String findProductIdByProductInfoId(String productInfoId);
	public ReturnBean checkRepeatProduct(ProductSort productSort);
	
	
	/******************************************参数接口******************************************/
	public ProductParameter findParameterByProductInfoId(String productInfoId);
	public ProductParameter saveProductParameter(ProductParameter productParameter);
	
	/******************************************配件接口******************************************/
	public ProductAssortPort findAssortPortByProductInfoId(String productInfoId);
	public ProductAssortPort saveProductAssortPort(ProductAssortPort productAssortPort);
	public List<ProductAssortPort> getProductAssortPortByTemplateInfoId(String templateInfoId);
	
	/******************************************半成品接口******************************************/
	public ProductHalfProduct findHalfProductByProductInfoId(String productInfoId);
	public ProductHalfProduct saveProductHalfProduct(ProductHalfProduct productHalfProduct);
	
	/******************************************工时接口******************************************/
	public ProductManHour findManHourByProductInfoId(String productInfoId);
	public ProductManHour saveProductManHour(ProductManHour productManHour);
	/** 
	* @Description: 重新计算并且刷新所有产品工时相关的信息 
	* @Title: reCalculatorProductManHour 
	* @param manHour
	* @return boolean
	* @author qin
	* @date 2019年1月7日上午9:33:02
	*/ 
	public boolean reCalculatorProductManHour(ManHour manHour);
	
	/******************************************原料接口******************************************/
	public ProductMaterial findMaterialByProductInfoId(String productInfoId);
	public ProductMaterial saveProductMaterial(ProductMaterial productMaterial);
	public String findMaterialTemplateFormulaByProductInfoId(String productInfoId);

	/** 
	* @Description: 计算单独某个材料的用量 
	* @Title: calculatorMaterialUseage 
	* @param productInfoId
	* @return double
	* @author qin
	* @date 2018年12月1日上午11:02:27
	*/ 
	public double calculatorMaterialUseage(String productInfoId);
	
	public double calculatorHalfProductPrice(String productInfoId);
	
	
	/** 
	* @Description: 计算某个产品所有的材料的用量 
	* @Title: calculatorPorductMaterialUseage 
	* @param productId
	* @return double
	* @author qin
	* @date 2018年12月1日上午11:03:25
	*/ 
	public ReturnBean calculatorPorductPrice(String productId);
	
	/** 
	* @Description: 在内存中计算商品的报价 
	* @Title: calculatorPorductPriceByProductId 
	* @param productId
	* @return ReturnBean
	* @author qin
	* @date 2018年12月17日下午5:21:30
	*/ 
	public ReturnBean calculatorPorductPriceByProductId(String productId);
	
	/** 
	* @Description: TODO 
	* @Title: getPorductTreeListByNewMaterial 
	* @param productId
	* @param materialId void
	* @author qin
	* @date 2018年12月19日下午8:34:35
	*/ 
	public List<ProductInfoView> getPorductTreeListByNewMaterial(String productId,String materialId);
	
	
	/******************************************标准件接口******************************************/
	public ProductStandard findStandardByProductInfoId(String productInfoId);
	public ProductStandard saveProductStandard(ProductStandard productStandard);
	
	/** 
	* @Description: 根据产品模型编号获得产品参数
	* @Title: getProductParameters 
	* @return List<String>
	* @author qin
	* @date 2018年12月29日下午5:55:00
	*/ 
	public List<String> getProductParametersByTemplateInfoId(String templateInfoId);
	
	
	List<Map<String,String>> getProductByLineAndParams(String productLine,List<ParamqQuerys> paramqQuerys);
	
	/******************************************统一的由产品模板生成产品的接口******************************************/
	public ProductStandard getProductStandardByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);
	public ProductMaterial getProductMaterialByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);
	public ProductManHour getProductManHourByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);
	public ProductAssortPort getProductAssortPortByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);
	public ProductHalfProduct getProductHalfProductByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);
	public ProductInfo geProductInfoByTemplateInfo(ProductSort productSort,TemplateInfo templateInfo);
	public ProductParameter getProductParameterByTemplateInfo(ProductSort productSort,ProductInfo p,TemplateInfo s);


	/** 
	* @Description: 完全备份一个产品 
	* @Title: copyProduct 
	* @param productId
	* @param productNumber
	* @param text
	* @return ReturnBean
	* @author qin
	* @date 2019年5月27日下午2:49:52
	*/ 
	public ReturnBean copyProduct(String productId,String productNumber,String text);
	
	/** 
	* @Description: 更新所有由模型变化导致的产品报价 
	* @Title: updateAllProductByTemplateChange 
	* @return ReturnBean
	* @author qin
	* @date 2019年5月27日下午3:44:05
	*/ 
	public ReturnBean updateAllProductByTemplateChange(String sortId);

	
}
