package com.kunbo.shop.service.history;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kunbo.shop.entity.history.HistoryOfferOrder;
import com.kunbo.shop.entity.history.HistoryPorductInfo;
import com.kunbo.shop.entity.history.HistoryProduct;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.returnBean.ReturnBean3;

@Service
public interface HistoryProductService {

	List<HistoryPorductInfo> converJsonToHistoryPorductInfo(String json);

	ReturnBean saveHistoryPorductOfferOrder(String data, String orderId,
			String motor, String params, String productLineId,
			String productJson, String productId, String materialId,
			double totalPrice, double unitPrice, int num, double fanProfitRate,
			double motorProfitRate, double offerUnitPrice,
			double offerTotalPrice);

	HistoryProduct getHistoryProductById(String id);

	List<HistoryPorductInfo> getHistoryProductInfoByProductOfferId(
			String offerProuctId);

	/**
	 * @Description: 作废报价单中的某个产品
	 * @Title: discardProductById
	 * @param id
	 * @return ReturnBean
	 * @author qin
	 * @date 2019年1月5日下午4:22:15
	 */
	ReturnBean discardProductById(String id, boolean isDiscard);

	/********************************* 报价单操作 ***********************************/
	/**
	 * @Description: 根据用户编号获取报价单
	 * @Title: getOrderByUser
	 * @param userId
	 * @return List<HistoryOfferOrder>
	 * @author qin
	 * @date 2018年12月21日下午1:23:55
	 */
	List<HistoryOfferOrder> getOrderByUser(int userId);

	ReturnBean3 saveOrder(HistoryOfferOrder historyOfferOrder);

	ReturnBean deleteOrderById(String id);

	HistoryOfferOrder getOrderById(String id);

	EasyUiDataGrid getOrderByUserPage(String userId, String customerName,
			String text, String page, String row);

	List<HistoryProduct> getHistoryProductByOrderId(String orderId);
	
	public ReturnBean exportXML(String id);
}
