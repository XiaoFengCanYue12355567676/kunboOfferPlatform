package com.kunbo.shop.controller.history;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kunbo.shop.entity.component.StandardType;
import com.kunbo.shop.entity.history.HistoryOfferOrder;
import com.kunbo.shop.entity.history.HistoryPorductInfo;
import com.kunbo.shop.entity.history.HistoryProduct;
import com.kunbo.shop.service.history.HistoryProductService;
import com.shuohe.util.date.SDate;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.returnBean.ReturnBean3;

@RestController
@RequestMapping("/shop/history")
public class HistoryController {

	@Autowired HistoryProductService historyProductService;
	
	@RequestMapping(value="/saveHistoryPorductOfferOrder.do")  
    public @ResponseBody ReturnBean saveHistoryPorductOfferOrder(
    		@RequestParam("productInfoList") String productInfoList
    		,@RequestParam("orderId") String orderId
    		,@RequestParam("motor") String motor
    		,@RequestParam("params") String params
    		,@RequestParam("productLineId") String productLineId
    		,@RequestParam("productJson") String productJson
    		,@RequestParam("productId") String productId
    		,@RequestParam("materialId") String materialId
    		,@RequestParam("totalPrice") double totalPrice
    		,@RequestParam("unitPrice") double unitPrice
    		,@RequestParam("num") int num
    		,@RequestParam("fanProfitRate") double fanProfitRate
    		,@RequestParam("motorProfitRate") double motorProfitRate
    		,@RequestParam("offerUnitPrice") double offerUnitPrice
    		,@RequestParam("offerTotalPrice") double offerTotalPrice
    		){
		return historyProductService.saveHistoryPorductOfferOrder( 
				 productInfoList, 
				 orderId,
				 motor,
				 params,
				 productLineId,
				 productJson,
				 productId,
				 materialId,
				 totalPrice,
				 unitPrice,
				 num,
				 fanProfitRate,
				 motorProfitRate,
				 offerUnitPrice,
				 offerTotalPrice
				);
	}
	
	/** 
	* @Description: 根据报价产品的编码获取报价的详细内容 
	* @Title: getHistoryProductById 
	* @param id
	* @return HistoryProduct
	* @author qin
	* @date 2019年1月1日下午10:40:23
	*/ 
	@RequestMapping(value="/getHistoryProductById.do")  
    public @ResponseBody HistoryProduct getHistoryProductById(
    		@RequestParam("id") String id
    		){
		return historyProductService.getHistoryProductById(id);
	}
	
	
	
	/** 
	* @Description: 根据报价单具体产品的编号获取该产品的结构
	* @Title: saveHistoryPorductOfferOrder 
	* @param data
	* @param motor
	* @return ReturnBean
	* @author qin
	* @date 2018年12月21日上午11:16:35
	*/ 
	@RequestMapping(value="/getHistoryProductInfoByProductOfferId.do")  
    public @ResponseBody List<HistoryPorductInfo> getHistoryProductInfoByProductOfferId(
    		@RequestParam("id") String id
    		){
		return historyProductService.getHistoryProductInfoByProductOfferId(id);
	}
	
	@RequestMapping(value="/getHistoryProductByOrderId.do")  
    public @ResponseBody List<HistoryProduct> getHistoryProductByOrderId(
    		@RequestParam("orderId") String orderId
    		){
		return historyProductService.getHistoryProductByOrderId(orderId);
	}
	
	/** 
	* @Description: 作废报价单中的某个产品
	* @Title: discardProductById 
	* @param orderId
	* @return ReturnBean
	* @author qin
	* @date 2019年1月5日下午4:20:58
	* @author xuej 改
	* @date 2019年3月22日下午
	*/ 
	@RequestMapping(value="/discardProductById.do")  
    public @ResponseBody ReturnBean discardProductById(
    		@RequestParam("id") String id
    		){
		return historyProductService.discardProductById(id,true);
	}
	
	
	
	/*********************************报价单操作***********************************/
	/** 
	* @Description: 获取用户所建立的报价单 
	* @Title: getOrderByUser 
	* @param userId
	* @return List<HistoryOfferOrder>
	* @author qin:xue
	* @date 2019年3月21日下午1:19:22
	*/ 
	@RequestMapping(value="/order/getOrderByUserForPage.do")  
    public @ResponseBody EasyUiDataGrid getOrderByUserForPage(
    		@RequestParam("userId") String userId
    		,@RequestParam("customerName") String customerName
    		,@RequestParam("text") String text
    		,@RequestParam("page") String page
    		,@RequestParam("rows") String row
    		){
		System.out.println("getOrderByUserForPage.do userId = "+userId);
		return historyProductService.getOrderByUserPage(userId,customerName,text,page,row);
	}
	@RequestMapping(value="/order/getOrderByUser.do")  
    public @ResponseBody List<HistoryOfferOrder> getOrderByUser(
    		@RequestParam("userId") int userId
    		){
		return historyProductService.getOrderByUser(userId);
	}
	
	
	/** 
	* @Description: 保存报价单 
	* @Title: saveOrder 
	* @param data
	* @return ReturnBean
	* @author qin
	* @date 2018年12月21日下午1:28:30
	*/ 
	@RequestMapping(value="/order/saveOrder.do")  
    public @ResponseBody ReturnBean3 saveOrder(
    		@RequestParam("data") String data
    		){
		Gson reGson = new Gson();
		String _data = data.replaceAll("\"\"", "null");
		HistoryOfferOrder historyOfferOrder = reGson.fromJson(_data, new TypeToken<HistoryOfferOrder>(){}.getType());
		historyOfferOrder.setQuoteDate(SDate.getSystemDate());
		historyOfferOrder.setQuoteLastModifyDate(SDate.getSystemDate());
		return historyProductService.saveOrder(historyOfferOrder);	
	}
	
	/** 
	* @Description: 根据编号删除订单 
	* @Title: saveOrder 
	* @param data
	* @return ReturnBean
	* @author qin
	* @date 2018年12月21日下午1:29:34
	*/ 
	@RequestMapping(value="/order/deleteOrderById.do")  
    public @ResponseBody ReturnBean deleteOrderById(
    		@RequestParam("orderId") String orderId
    		){
		return historyProductService.deleteOrderById(orderId);
	}
	
	
	/** 
	* @Description: 根据编号删除订单 
	* @Title: saveOrder 
	* @param data
	* @return ReturnBean
	* @author qin
	* @date 2018年12月21日下午1:29:34
	*/ 
	@RequestMapping(value="/order/getOrderById.do")  
    public @ResponseBody HistoryOfferOrder getOrderById(
    		@RequestParam("orderId") String orderId
    		){
		return historyProductService.getOrderById(orderId);
	}
	
	/**
	 * 将报价单导出成pdf
	 * <p>Title: chartExport</p>
	 * <p>Description: </p>
	 * @param response
	 * @param quoteId
	 * @throws Exception
	 */
	@RequestMapping(value="/exportPdf", method=RequestMethod.POST)
    public void chartExport(HttpServletResponse response,
    		@RequestParam("quoteId") String quoteId) throws Exception {
		Document doc = new Document(PageSize.A4, 20, 20, 20, 20);
		BaseFont bf;
		Font font = null;
		float lineHeight1 = (float)25.0;
	    float lineHeight2 = (float)25.0; 
		BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
    	Font fontChinese =  new  Font(baseFontChinese , 8 , Font.NORMAL);
		//String exportFilePath = "C:\\Users\\qin\\Desktop\\克莱特菲尔报价系统\\报价单.pdf";
    	String exportFilePath = "D:\\内部报价单.pdf";
		PdfWriter.getInstance(doc, new FileOutputStream(exportFilePath));
		doc.open();
		PdfPTable quoteTitle = new PdfPTable(4);//报价标题
//		float[] columnWidths = {1f, 1f,4f, 1f, 3f, 3f,3f,3f};//设置列宽
//		quoteTitle.setWidths(columnWidths);
		PdfPTable quoteDetail = new PdfPTable(9);//报价明细
		float[] columnWidths = {3f, 1f,1f, 2f, 2f, 1f,2f,2f,3f};//设置列宽
		quoteDetail.setWidths(columnWidths);
		quoteTitle.setWidthPercentage(130);
		//报价单表头
		//PdfPCell h1 = new PdfPCell(new Paragraph("报价单信息",fontChinese));
		PdfPCell h2 = new PdfPCell(new Paragraph("报价明细信息",fontChinese));
        //h1.setColspan(8);
        h2.setColspan(9);
        //h1.setHorizontalAlignment(Element.ALIGN_CENTER);
        //h1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        h2.setHorizontalAlignment(Element.ALIGN_CENTER);
        h2.setVerticalAlignment(Element.ALIGN_MIDDLE);
        //quoteTitle.addCell(h1);
        quoteDetail.addCell(h2);
		//报价明细表头
		PdfPCell dhc1 = new PdfPCell(new Paragraph("风机型号",fontChinese));
		dhc1.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc2 = new PdfPCell(new Paragraph("风机利润率",fontChinese));
		dhc2.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc3 = new PdfPCell(new Paragraph("电机利润率",fontChinese));
		dhc3.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc4 = new PdfPCell(new Paragraph("成本单价(元)",fontChinese));
		dhc4.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc5 = new PdfPCell(new Paragraph("报价单价(元)",fontChinese));
		dhc5.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc6 = new PdfPCell(new Paragraph("数量",fontChinese));
		dhc6.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc7 = new PdfPCell(new Paragraph("成本总价(元)",fontChinese));
		dhc7.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc8 = new PdfPCell(new Paragraph("报价总价(元)",fontChinese));
		dhc8.setBackgroundColor(new BaseColor(0xdd7e6b));
		PdfPCell dhc9 = new PdfPCell(new Paragraph("报价时间",fontChinese));
		dhc9.setBackgroundColor(new BaseColor(0xdd7e6b));
		
		quoteTitle.setWidthPercentage(100);
		quoteDetail.addCell(dhc1);
		quoteDetail.addCell(dhc2);
		quoteDetail.addCell(dhc3);
		quoteDetail.addCell(dhc4);
		quoteDetail.addCell(dhc5);
		quoteDetail.addCell(dhc6);
		quoteDetail.addCell(dhc7);
		quoteDetail.addCell(dhc8);
		quoteDetail.addCell(dhc9);
		quoteDetail.setWidthPercentage(100);
		
		//根据id获取报价单信息
		System.out.println("报价单id为："+quoteId);
		HistoryOfferOrder hoo = historyProductService.getOrderById(quoteId);
		System.out.println(hoo);
		//根据报价单id过去报价产品明细
		List<HistoryProduct> list = historyProductService.getHistoryProductByOrderId(quoteId);
		System.out.println(list);
		
		DecimalFormat    df   = new DecimalFormat("######0.00");   
		
		PdfPCell thc1 = new PdfPCell(new Paragraph("报价单号",fontChinese));
		PdfPCell thc2 = new PdfPCell(new Paragraph("填报人",fontChinese));
		PdfPCell thc3 = new PdfPCell(new Paragraph("客户名称",fontChinese));
		PdfPCell thc4 = new PdfPCell(new Paragraph("数量",fontChinese));
		PdfPCell thc5 = new PdfPCell(new Paragraph("成本总价(元)",fontChinese));
		PdfPCell thc6 = new PdfPCell(new Paragraph("报价总价(元)",fontChinese));
		PdfPCell thc7 = new PdfPCell(new Paragraph("创建时间",fontChinese));
		PdfPCell thc8 = new PdfPCell(new Paragraph("修改时间",fontChinese));
		
		PdfPCell c1 = new PdfPCell(new Paragraph(hoo.getText(),fontChinese));
		PdfPCell c2 = new PdfPCell(new Paragraph(hoo.getQuotePeopleName(),fontChinese));
		PdfPCell c3 = new PdfPCell(new Paragraph(hoo.getCustomerName(),fontChinese));
		PdfPCell c4 = new PdfPCell(new Paragraph(hoo.getProductCount()+"",fontChinese));
		PdfPCell c5 = new PdfPCell(new Paragraph(df.format(hoo.getTotalPrice())+"",fontChinese));
		PdfPCell c6 = new PdfPCell(new Paragraph(df.format(hoo.getOfferTotalPrice())+"",fontChinese));
		PdfPCell c7 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(hoo.getQuoteDate()),fontChinese));
		PdfPCell c8 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(hoo.getQuoteLastModifyDate()),fontChinese));
		//报价单内容填充
		quoteTitle.addCell(thc1);
		quoteTitle.addCell(c1);
		quoteTitle.addCell(thc2);
		quoteTitle.addCell(c2);
		quoteTitle.addCell(thc3);
		quoteTitle.addCell(c3);
		quoteTitle.addCell(thc4);
		quoteTitle.addCell(c4);
		quoteTitle.addCell(thc5);
		quoteTitle.addCell(c5);
		quoteTitle.addCell(thc6);
		quoteTitle.addCell(c6);
		quoteTitle.addCell(thc7);
		quoteTitle.addCell(c7);
		quoteTitle.addCell(thc8);
		quoteTitle.addCell(c8);
		//加入table表格
		Image image = Image.getInstance("D:\\kelaitelogo\\logo.gif");
		image.scaleAbsolute(181f, 37f);// 直接设定显示尺寸
        doc.add(image);
        Paragraph gongSiMingChengParagraph = new Paragraph("");
        Font gongSiMingChengFont = new  Font(baseFontChinese , 20 , Font.NORMAL);
        gongSiMingChengFont.setColor(69, 69, 69);
        gongSiMingChengParagraph.setFont(gongSiMingChengFont);
        gongSiMingChengParagraph.add(new Chunk("克莱特菲尔风机股份有限公司"));
        gongSiMingChengParagraph.setAlignment(Element.ALIGN_CENTER);
        doc.add(gongSiMingChengParagraph);
        Paragraph baoJiaDanParagraph = new Paragraph("");
        baoJiaDanParagraph.setFont(new  Font(baseFontChinese , 20 , Font.NORMAL));        
        baoJiaDanParagraph.setAlignment(Element.ALIGN_CENTER);        
        baoJiaDanParagraph.add(new Chunk("报价单"));
        doc.add(baoJiaDanParagraph);
        quoteTitle.setSpacingBefore(10);
		quoteTitle.setSpacingAfter(30);
		doc.add(quoteTitle);
		PdfPCell t1 = new PdfPCell();
		PdfPCell t2 = new PdfPCell();
		PdfPCell t3 = new PdfPCell();
		PdfPCell t4 = new PdfPCell();
		PdfPCell t5 = new PdfPCell();
		PdfPCell t6 = new PdfPCell();
		PdfPCell t7 = new PdfPCell();
		PdfPCell t8 = new PdfPCell();
		PdfPCell t9 = new PdfPCell();
		for (HistoryProduct h : list) {
			 t1 = new PdfPCell(new Paragraph(h.getProductNumber(),fontChinese));
			 t2 = new PdfPCell(new Paragraph(h.getFanProfitRate()+"",fontChinese));
			 t3 = new PdfPCell(new Paragraph(h.getMotorProfitRate()+"",fontChinese));
			 t4 = new PdfPCell(new Paragraph(df.format(h.getUnitPrice())+"",fontChinese));
			 t5 = new PdfPCell(new Paragraph(df.format(h.getOfferUnitPrice())+"",fontChinese));
			 t6 = new PdfPCell(new Paragraph(h.getNum()+"",fontChinese));
			 t7 = new PdfPCell(new Paragraph(df.format(h.getTotalPrice())+"",fontChinese));
			 t8 = new PdfPCell(new Paragraph(df.format(h.getOfferTotalPrice())+"",fontChinese));
			 t9 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(h.getQuoteDate()),fontChinese));
			 quoteDetail.addCell(t1);
			 quoteDetail.addCell(t2);
			 quoteDetail.addCell(t3);
			 quoteDetail.addCell(t4);
			 quoteDetail.addCell(t5);
			 quoteDetail.addCell(t6);
			 quoteDetail.addCell(t7);
			 quoteDetail.addCell(t8);
			 quoteDetail.addCell(t9);
		}
		doc.add(quoteDetail);
		doc.close();
		//导出pdf文件
        File file = new File(exportFilePath);
        if (file.exists()) {
            response.setContentType("application/force-download");// 设置强制下载不打开
            //response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
            response.setHeader("Content-Disposition", "attachment; fileName="+  "内部报价单.pdf" +";"
            		+ "filename*=utf-8''"+URLEncoder.encode("内部报价单.pdf","UTF-8")); //设置中文名称并且解决中文乱码问题 
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                System.out.println("success");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
	}
        
        /**
    	 * 将报价单导出成pdf
    	 * <p>Title: chartExport</p>
    	 * <p>Description: </p>
    	 * @param response
    	 * @param quoteId
    	 * @throws Exception
    	 */
    	@RequestMapping(value="/exportPdf2", method=RequestMethod.POST)
        public void chartExport2(HttpServletResponse response,
        		@RequestParam("quoteId") String quoteId) throws Exception {
    		Document doc = new Document(PageSize.A4, 20, 20, 20, 20);
    		BaseFont bf;
    		Font font = null;
    		float lineHeight1 = (float)25.0;
    	    float lineHeight2 = (float)25.0; 
    		BaseFont baseFontChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        	Font fontChinese =  new  Font(baseFontChinese , 8 , Font.NORMAL);
    		//String exportFilePath = "C:\\Users\\qin\\Desktop\\克莱特菲尔报价系统\\报价单.pdf";
        	String exportFilePath = "D:\\客户报价单.pdf";
    		PdfWriter.getInstance(doc, new FileOutputStream(exportFilePath));
    		doc.open();
    		PdfPTable quoteTitle = new PdfPTable(4);//报价标题
//    		float[] columnWidths = {1f, 1f,4f, 1f, 3f, 3f,3f,3f};//设置列宽
//    		quoteTitle.setWidths(columnWidths);
    		PdfPTable quoteDetail = new PdfPTable(5);//报价明细
    		float[] columnWidths = {3f, 3f,3f,3f,3f};//设置列宽
    		//float[] columnWidths2 = {3f, 3f,3f,3f};//设置列宽
    		quoteDetail.setWidths(columnWidths);
    		quoteTitle.setWidthPercentage(100l);
    		//报价单表头
    		//PdfPCell h1 = new PdfPCell(new Paragraph("报价单信息",fontChinese));
    		PdfPCell h2 = new PdfPCell(new Paragraph("报价明细信息",fontChinese));
            //h1.setColspan(8);
            h2.setColspan(9);
            //h1.setHorizontalAlignment(Element.ALIGN_CENTER);
            //h1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            h2.setHorizontalAlignment(Element.ALIGN_CENTER);
            h2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            //quoteTitle.addCell(h1);
            quoteDetail.addCell(h2);
    		//报价明细表头
            PdfPCell dhc1 = new PdfPCell(new Paragraph("风机型号",fontChinese));
    		dhc1.setBackgroundColor(new BaseColor(0xdd7e6b));
    		PdfPCell dhc5 = new PdfPCell(new Paragraph("报价单价(元)",fontChinese));
    		dhc5.setBackgroundColor(new BaseColor(0xdd7e6b));
    		PdfPCell dhc6 = new PdfPCell(new Paragraph("数量",fontChinese));
    		dhc6.setBackgroundColor(new BaseColor(0xdd7e6b));
    		PdfPCell dhc8 = new PdfPCell(new Paragraph("报价总价(元)",fontChinese));
    		dhc8.setBackgroundColor(new BaseColor(0xdd7e6b));
    		PdfPCell dhc9 = new PdfPCell(new Paragraph("报价时间",fontChinese));
    		dhc9.setBackgroundColor(new BaseColor(0xdd7e6b));
    		quoteDetail.addCell(dhc1);
    		quoteDetail.addCell(dhc5);
    		quoteDetail.addCell(dhc6);
    		quoteDetail.addCell(dhc8);
    		quoteDetail.addCell(dhc9);
    		quoteDetail.setWidthPercentage(100);
    		
    		//根据id获取报价单信息
    		System.out.println("报价单id为："+quoteId);
    		HistoryOfferOrder hoo = historyProductService.getOrderById(quoteId);
    		System.out.println(hoo);
    		//根据报价单id过去报价产品明细
    		List<HistoryProduct> list = historyProductService.getHistoryProductByOrderId(quoteId);
    		System.out.println(list);
    		
    		DecimalFormat    df   = new DecimalFormat("######0.00");   
    		
    		PdfPCell thc1 = new PdfPCell(new Paragraph("报价单号",fontChinese));
    		//PdfPCell thc2 = new PdfPCell(new Paragraph("填报人",fontChinese));
    		PdfPCell thc3 = new PdfPCell(new Paragraph("客户名称",fontChinese));
    		PdfPCell thc4 = new PdfPCell(new Paragraph("数量",fontChinese));
    		//PdfPCell thc5 = new PdfPCell(new Paragraph("成本总价(元)",fontChinese));
    		PdfPCell thc6 = new PdfPCell(new Paragraph("报价总价(元)",fontChinese));
    		PdfPCell thc7 = new PdfPCell(new Paragraph("创建时间",fontChinese));
    		PdfPCell thc8 = new PdfPCell(new Paragraph("修改时间",fontChinese));
    		
    		PdfPCell c1 = new PdfPCell(new Paragraph(hoo.getText(),fontChinese));
    		//PdfPCell c2 = new PdfPCell(new Paragraph(hoo.getQuotePeopleName(),fontChinese));
    		PdfPCell c3 = new PdfPCell(new Paragraph(hoo.getCustomerName(),fontChinese));
    		PdfPCell c4 = new PdfPCell(new Paragraph(hoo.getProductCount()+"",fontChinese));
    		//PdfPCell c5 = new PdfPCell(new Paragraph(df.format(hoo.getTotalPrice())+"",fontChinese));
    		PdfPCell c6 = new PdfPCell(new Paragraph(df.format(hoo.getOfferTotalPrice())+"",fontChinese));
    		PdfPCell c7 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(hoo.getQuoteDate()),fontChinese));
    		PdfPCell c8 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(hoo.getQuoteLastModifyDate()),fontChinese));
    		//报价单内容填充
    		quoteTitle.addCell(thc1);
    		quoteTitle.addCell(c1);
    		quoteTitle.addCell(thc3);
    		quoteTitle.addCell(c3);
    		quoteTitle.addCell(thc4);
    		quoteTitle.addCell(c4);
    		quoteTitle.addCell(thc6);
    		quoteTitle.addCell(c6);
    		quoteTitle.addCell(thc7);
    		quoteTitle.addCell(c7);
    		quoteTitle.addCell(thc8);
    		quoteTitle.addCell(c8);
    		//加入table表格
    		Image image = Image.getInstance("D:\\kelaitelogo\\logo.gif");
    		image.scaleAbsolute(181f, 37f);// 直接设定显示尺寸
            doc.add(image);
            Paragraph gongSiMingChengParagraph = new Paragraph("");
            Font gongSiMingChengFont = new  Font(baseFontChinese , 20 , Font.NORMAL);
            gongSiMingChengFont.setColor(69, 69, 69);
            gongSiMingChengParagraph.setFont(gongSiMingChengFont);
            gongSiMingChengParagraph.add(new Chunk("克莱特菲尔风机股份有限公司"));
            gongSiMingChengParagraph.setAlignment(Element.ALIGN_CENTER);
            doc.add(gongSiMingChengParagraph);
            Paragraph baoJiaDanParagraph = new Paragraph("");
            baoJiaDanParagraph.setFont(new  Font(baseFontChinese , 20 , Font.NORMAL));        
            baoJiaDanParagraph.setAlignment(Element.ALIGN_CENTER);        
            baoJiaDanParagraph.add(new Chunk("报价单"));
            doc.add(baoJiaDanParagraph);
            quoteTitle.setSpacingBefore(10);
    		quoteTitle.setSpacingAfter(30);
    		doc.add(quoteTitle);
    		PdfPCell t1 = new PdfPCell();
    		PdfPCell t5 = new PdfPCell();
    		PdfPCell t6 = new PdfPCell();
    		PdfPCell t8 = new PdfPCell();
    		PdfPCell t9 = new PdfPCell();
    		for (HistoryProduct h : list) {
    			 t1 = new PdfPCell(new Paragraph(h.getProductNumber(),fontChinese));
    			 t5 = new PdfPCell(new Paragraph(df.format(h.getOfferUnitPrice())+"",fontChinese));
    			 t6 = new PdfPCell(new Paragraph(h.getNum()+"",fontChinese));
    			 t8 = new PdfPCell(new Paragraph(df.format(h.getOfferTotalPrice())+"",fontChinese));
    			 t9 = new PdfPCell(new Paragraph(SDate.praseYMDHMSDate(h.getQuoteDate()),fontChinese));
    			 quoteDetail.addCell(t1);
    			 quoteDetail.addCell(t5);
    			 quoteDetail.addCell(t6);
    			 quoteDetail.addCell(t8);
    			 quoteDetail.addCell(t9);
    		}
    		doc.add(quoteDetail);
    		doc.close();
    		//导出pdf文件
            File file = new File(exportFilePath);
            if (file.exists()) {
                response.setContentType("application/force-download");// 设置强制下载不打开
                //response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
                response.setHeader("Content-Disposition", "attachment; fileName="+  "客户报价单.pdf" +";"
                		+ "filename*=utf-8''"+URLEncoder.encode("客户报价单.pdf","UTF-8")); //设置中文名称并且解决中文乱码问题 
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    System.out.println("success");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
	}
	
	
    	/** 
    	* @Description: 开启报价流程 
    	* 生成XML文件，保存到固定位置去
    	*/ 
    	@RequestMapping(value="/order/exportXML.do")  
        public @ResponseBody ReturnBean exportXML(@RequestParam("orderId") String orderId){
    		return historyProductService.exportXML(orderId);
    	}
	
}
