package com.kunbo.shop.controller.sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.offer.entity.CapabilityParam;
import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.service.sort.ProductSortService;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;
import com.shuohe.util.returnBean.ReturnBean3;

@RestController
@RequestMapping("/shop/sort")
public class ProductSortController {

	@Autowired ProductSortService productSortService;
	
	
	/******************************************产品接口******************************************/
	@RequestMapping(value="/save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ProductSort productSort = reGson.fromJson(data, new TypeToken<ProductSort>(){}.getType());
		
		try
		{
			if(productSort.getPid()==null)
			productSort.setPid("-");
			productSortService.insert(productSort);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/findById.do")  
    public @ResponseBody ProductSort findById(@RequestParam("id") String id){
		return productSortService.findById(id);
	}
	
	
	
	@RequestMapping(value="/delete.do")  
    public @ResponseBody ReturnBean deleteById(@RequestParam("id") String id){
		return productSortService.deleteById(id);
	}
	
	@RequestMapping(value="/getAllForTree.do")  
    public @ResponseBody List<ProductSort> getAllForTree(){
		return productSortService.getForTree();
	}
	
	/**
	 * 
	 * <p>Title: getFanSelectList</p>
	 * <p>Description:获取风机选型列表 </p>
	 * @return
	 * @throws Exception 
	 * @throws ScriptException 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getFanSelectList.do")  
    public @ResponseBody ReturnBean3 getFanSelectList(HttpServletRequest request) throws ScriptException, Exception{
		Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
		String paramJson = request.getParameter("paramJson");
		map = gson.fromJson(paramJson, map.getClass());//关键
		System.out.println("paramJson:"+paramJson);
		System.out.println("map:"+map);
		
		
		Map<String, Object> objMap = new HashMap<String, Object>();
		String obj = request.getParameter("obj");
		objMap = gson.fromJson(obj, objMap.getClass());//关键
		
		System.out.println("obj:"+obj);
		System.out.println("objMap:"+objMap);
		List<Map<String,Object>> list = productSortService.getFanSelectList(map,objMap);
		EasyUiDataGrid edg = new EasyUiDataGrid(list.size(),list);
		return new ReturnBean3(true,edg);
	}
	/**
	 * 
	 * <p>Title: getFanSelectListEn</p>
	 * <p>Description:获取风机选型列表___英文  </p>
	 * @return
	 * @throws Exception 
	 * @throws ScriptException 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getFanSelectListEn.do")  
    public @ResponseBody ReturnBean3 getFanSelectListEn(HttpServletRequest request) throws ScriptException, Exception{
		Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
		String paramJson = request.getParameter("paramJson");
		map = gson.fromJson(paramJson, map.getClass());//关键
		System.out.println("paramJson:"+paramJson);
		System.out.println("map:"+map);
		List<Map<String,Object>> list = productSortService.getFanSelectListEn(map);
		EasyUiDataGrid edg = new EasyUiDataGrid(list.size(),list);
		return new ReturnBean3(true,edg);
	}
	/**
	 * 
	 * <p>Title: getFanSelectList</p>
	 * <p>Description:获取风机选型列表-------相似设计 </p>
	 * @return
	 * @throws Exception 
	 * @throws ScriptException 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getFanSelectList2.do")  
    public @ResponseBody ReturnBean3 getFanSelectList2(HttpServletRequest request) throws ScriptException, Exception{
		Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
		String paramJson = request.getParameter("paramJson");
		map = gson.fromJson(paramJson, map.getClass());//关键
		System.out.println("paramJson:"+paramJson);
		System.out.println("map:"+map);
		
		Map<String, Object> objMap = new HashMap<String, Object>();
		String obj = request.getParameter("obj");
		objMap = gson.fromJson(obj, objMap.getClass());//关键
		
		System.out.println("obj:"+obj);
		System.out.println("objMap:"+objMap);
		List<Map<String,Object>> list = productSortService.getFanSelectList2(map,objMap);
		EasyUiDataGrid edg = new EasyUiDataGrid(list.size(),list);
		return new ReturnBean3(true,edg);
	}
	@RequestMapping(value="/getSortForTree.do")  
    public @ResponseBody List<ProductSort> getSortForTree(){
		return productSortService.getSortTree();
	}
	
	/******************************************产品接口******************************************/
	/**
	 * 变形后的数据
	 * @param fanNumber
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getPerforParam.do")  
    public @ResponseBody List<CapabilityParam> getPerforParam(HttpServletRequest request){
		Gson gson = new Gson();
        Map<String, Object> map = new HashMap<String, Object>();
		String paramJson = request.getParameter("paramJson");
		map = gson.fromJson(paramJson, map.getClass());//关键
		return productSortService.getPerforParam(map);
	}
	
}
