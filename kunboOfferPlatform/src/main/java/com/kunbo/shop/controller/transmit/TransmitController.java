package com.kunbo.shop.controller.transmit;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.shuohe.util.http.QHttp;

@RestController
@RequestMapping("/transmit")
public class TransmitController {

	
	@RequestMapping(value="/getCustomer.do", produces = "application/json; charset=utf-8")  
    public @ResponseBody String getCustomer(
    		){
		return QHttp.sendPost(
			"http://27.221.114.67:9003/develop/url/getUrl.do?name=getCusUpdate"
			, "");
	}
}
