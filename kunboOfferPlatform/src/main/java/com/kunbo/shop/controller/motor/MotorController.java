package com.kunbo.shop.controller.motor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MoistTemperature;
import com.kunbo.shop.entity.motor.Motor;
import com.kunbo.shop.entity.motor.MotorAntiExplosion;
import com.kunbo.shop.entity.motor.MotorBearing;
import com.kunbo.shop.entity.motor.MotorBrand;
import com.kunbo.shop.entity.motor.MotorDefend;
import com.kunbo.shop.entity.motor.MotorHeater;
import com.kunbo.shop.entity.motor.MotorInstallation;
import com.kunbo.shop.entity.motor.MotorIpLevel;
import com.kunbo.shop.entity.motor.MotorPadding;
import com.kunbo.shop.entity.motor.MotorSeries;
import com.kunbo.shop.entity.motor.MotorShaft;
import com.kunbo.shop.entity.motor.MotorStandardNum;
import com.kunbo.shop.entity.motor.SeriesStandardRel;
import com.kunbo.shop.service.motor.MotorService;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/shop/motor")
public class MotorController {

	@Autowired MotorService motorService;
	
	@RequestMapping(value="/brand/save.do")  
    public @ResponseBody ReturnBean saveBrand(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorBrand motorBrand = reGson.fromJson(data, new TypeToken<MotorBrand>(){}.getType());
		
		try
		{
			motorService.insertMotorBrand(motorBrand);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/brand/list.do")  
    public @ResponseBody List<MotorBrand> deleteMotorBrandById(){
		return motorService.getMotorBrandForList();
	}
	@RequestMapping(value="/brand/findById.do")  
    public @ResponseBody MotorBrand findMotorBrandById(@RequestParam("id") String id){
		return motorService.findMotorBrandById(id);
	}
	
	@RequestMapping(value="/brand/delete.do")  
    public @ResponseBody ReturnBean deleteMotorBrandById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorBrandById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	
	@RequestMapping(value="/motor/save.do")  
    public @ResponseBody ReturnBean saveMotor(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		Motor motor = reGson.fromJson(data, new TypeToken<Motor>(){}.getType());
		
		try
		{
			motorService.insertMotor(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motor/delete.do")  
    public @ResponseBody ReturnBean deleteMotorById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motor/findById.do")  
    public @ResponseBody Motor findMotorById(@RequestParam("id") String id){
		return motorService.findMotorById(id);
	}
	
	@RequestMapping(value="/motor/findForPageByBrandId.do")  
	public EasyUiDataGrid findDatagridForPageByBrandId(
			@RequestParam("brandId") String brandId,
			@RequestParam("page") String page,
			@RequestParam("rows") String rows)
	{
		return motorService.findDatagridForPageByBrandId(brandId,page,rows);
	}
	
	@RequestMapping(value="/motor/findByBrandId.do")  
	public List<Motor> findMotorForListByBrandId(@RequestParam("brandId") String brandId)
	{
		return motorService.findMotorForListByBrandId(brandId);
	}
	
	
	@RequestMapping(value="/motorSeries/save.do")  
    public @ResponseBody ReturnBean saveMotorSeries(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorSeries motor = reGson.fromJson(data, new TypeToken<MotorSeries>(){}.getType());
		
		try
		{
			motorService.insertMotorSeries(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorSeries/delete.do")  
    public @ResponseBody ReturnBean deleteMotorSeriesById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorSeriesById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorSeries/findById.do")  
    public @ResponseBody MotorSeries findMotorSeriesById(@RequestParam("id") String id){
		return motorService.findMotorSeriesById(id);
	} 
	/**
	 * 系列列表
	 * @param brandId
	 * @return
	 */
	@RequestMapping(value="/motorSeries/list.do")  
    public @ResponseBody List<MotorSeries> listMotorSeries(@RequestParam("brandId") String brandId){
		return motorService.listMotorSeries(brandId);
		
	} 
	
	@RequestMapping(value="/motorStandardNum/save.do")  
    public @ResponseBody ReturnBean saveMotorStandardNum(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorStandardNum motor = reGson.fromJson(data, new TypeToken<MotorStandardNum>(){}.getType());
		
		try
		{
			motorService.insertMotorStandardNum(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorStandardNum/delete.do")  
    public @ResponseBody ReturnBean deleteMotorStandardNumById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorStandardNumById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorStandardNum/findById.do")  
    public @ResponseBody MotorStandardNum findMotorStandardNumById(@RequestParam("id") String id){
		return motorService.findMotorStandardNumById(id);
	} 
	/**
	 * 基座列表
	 * @param id 品牌ID
	 * @return
	 */
	@RequestMapping(value="/motorStandardNum/list.do")  
    public @ResponseBody List<MotorStandardNum> listMotorStandardNum(@RequestParam("brandId") String brandId){
		return motorService.listMotorStandardNum(brandId);
	} 
	
	@RequestMapping(value="/seriesStandardRel/save.do")  
    public @ResponseBody ReturnBean saveSeriesStandardRel(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		SeriesStandardRel motor = reGson.fromJson(data, new TypeToken<SeriesStandardRel>(){}.getType());
		
		try
		{
			motorService.insertSeriesStandardRel(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/seriesStandardRel/delete.do")  
    public @ResponseBody ReturnBean deleteSeriesStandardRelNumById(@RequestParam("id") String id){
		try
		{
			motorService.deleteSeriesStandardRelById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/seriesStandardRel/findById.do")  
    public @ResponseBody SeriesStandardRel findSeriesStandardRelById(@RequestParam("id") String id){
		return motorService.findSeriesStandardRelById(id);
	} 
	
	
	@RequestMapping(value="/seriesStandardRel/list.do")  
    public @ResponseBody List<SeriesStandardRel> listSeriesStandardRelBySeriesText(@RequestParam("seriesText") String seriesText,@RequestParam("brandId") String brandId){
		return motorService.listSeriesStandardRelBySeriesText(seriesText,brandId);
	} 
	
	@RequestMapping(value="/seriesStandardRel/findByStandardText.do")  
    public @ResponseBody List<SeriesStandardRel> findseriesStandardRelByStandardText(@RequestParam("standardText") String standardText,@RequestParam("brandId") String brandId){
		return motorService.findseriesStandardRelByStandardText(standardText,brandId);
	} 
	
	
	@RequestMapping(value="/diffeVoltage/save.do")  
    public @ResponseBody ReturnBean saveDiffeVoltage(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		DiffeVoltage motor = reGson.fromJson(data, new TypeToken<DiffeVoltage>(){}.getType());
		
		try
		{
			motorService.insertDiffeVoltage(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/diffeVoltage/delete.do")  
    public @ResponseBody ReturnBean deleteDiffeVoltageById(@RequestParam("id") String id){
		try
		{
			motorService.deleteDiffeVoltageById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/diffeVoltage/findById.do")  
    public @ResponseBody DiffeVoltage findDiffeVoltageById(@RequestParam("id") String id){
		return motorService.findDiffeVoltageById(id);
	} 
	
	
	@RequestMapping(value="/diffeVoltage/list.do")  
    public @ResponseBody List<DiffeVoltage> listDiffeVoltageBySeriesText(@RequestParam("seriesText") String seriesText,@RequestParam("brandId") String brandId){
		return motorService.listDiffeVoltageBySeriesText(seriesText,brandId);
	} 
	
	@RequestMapping(value="/motorIpLevel/save.do")  
    public @ResponseBody ReturnBean saveMotorIpLevel(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorIpLevel motor = reGson.fromJson(data, new TypeToken<MotorIpLevel>(){}.getType());
		
		try
		{
			motorService.insertMotorIpLevel(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorIpLevel/delete.do")  
    public @ResponseBody ReturnBean deleteMotorIpLevelById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorIpLevelById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorIpLevel/findById.do")  
    public @ResponseBody MotorIpLevel findMotorIpLevelById(@RequestParam("id") String id){
		return motorService.findMotorIpLevelById(id);
	} 
	/**
	 * IP等级列表
	 * @param brandId
	 * @return
	 */
	@RequestMapping(value="/motorIpLevel/list.do")  
    public @ResponseBody List<MotorIpLevel> listMotorIpLevel(@RequestParam("brandId") String brandId){
		return motorService.listMotorIpLevel(brandId);
	} 
	
	
	
	@RequestMapping(value="/motorInstallation/save.do")  
    public @ResponseBody ReturnBean saveMotorInstallation(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorInstallation motor = reGson.fromJson(data, new TypeToken<MotorInstallation>(){}.getType());
		
		try
		{
			motorService.insertMotorInstallation(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorInstallation/delete.do")  
    public @ResponseBody ReturnBean deleteMotorInstallationById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorInstallationById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorInstallation/findById.do")  
    public @ResponseBody MotorInstallation findMotorInstallationById(@RequestParam("id") String id){
		return motorService.findMotorInstallationById(id);
	} 
	/**
	 * 安装方式列表
	 * @return
	 */
	@RequestMapping(value="/motorInstallation/list.do")  
    public @ResponseBody List<MotorInstallation> listMotorInstallation(@RequestParam("brandId") String brandId){
		return motorService.listMotorInstallation(brandId);
	} 
	
	/**********************************潮温****************************************/
	@RequestMapping(value="/moistTemperature/save.do")  
    public @ResponseBody ReturnBean saveMoistTemperature(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MoistTemperature moistTemperature = reGson.fromJson(data, new TypeToken<MoistTemperature>(){}.getType());
		
		try
		{
			motorService.insertMoistTemperature(moistTemperature);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/moistTemperature/delete.do")  
    public @ResponseBody ReturnBean deleteMoistTemperatureById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMoistTemperatureById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/moistTemperature/findById.do")  
    public @ResponseBody MoistTemperature findMoistTemperatureById(@RequestParam("id") String id){
		return motorService.findMoistTemperatureById(id);
	} 
	
	@RequestMapping(value="/moistTemperature/list.do")  
    public @ResponseBody List<MoistTemperature> listMoistTemperatureBySeriesText(
    		@RequestParam("seriesText") String seriesText,@RequestParam("brandId") String brandId){
		return motorService.listMoistTemperatureBySeriesText(seriesText,brandId);
	} 

	/**********************************轴承****************************************/
	@RequestMapping(value="/motorBearing/save.do")  
    public @ResponseBody ReturnBean saveMotorBearing(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorBearing motor = reGson.fromJson(data, new TypeToken<MotorBearing>(){}.getType());
		
		try
		{
			motorService.insertMotorBearing(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorBearing/delete.do")  
    public @ResponseBody ReturnBean deleteMotorBearingById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorBearingById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorBearing/findById.do")  
    public @ResponseBody MotorBearing findMotorBearingById(@RequestParam("id") String id){
		return motorService.findMotorBearingById(id);
	} 
	/**
	 * 轴承列表
	 * @return
	 */
	@RequestMapping(value="/motorBearing/list.do")  
    public @ResponseBody List<MotorBearing> listMotorBearing(@RequestParam("brandId") String brandId){
		return motorService.listMotorBearing(brandId);
	} 
	
	/**********************************填料****************************************/
	@RequestMapping(value="/motorPadding/save.do")  
    public @ResponseBody ReturnBean saveMotorPadding(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorPadding motor = reGson.fromJson(data, new TypeToken<MotorPadding>(){}.getType());
		
		try
		{
			motorService.insertMotorPadding(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorPadding/delete.do")  
    public @ResponseBody ReturnBean deleteMotorPaddingById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorPaddingById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorPadding/findById.do")  
    public @ResponseBody MotorPadding findMotorPaddingById(@RequestParam("id") String id){
		return motorService.findMotorPaddingById(id);
	} 
	/**
	 * 填料列表
	 * @return
	 */
	@RequestMapping(value="/motorPadding/list.do")  
    public @ResponseBody List<MotorPadding> listMotorPadding(@RequestParam("brandId") String brandId){
		return motorService.listMotorPadding(brandId);
	} 
	
	/**********************************电机轴****************************************/
	@RequestMapping(value="/motorShaft/save.do")  
    public @ResponseBody ReturnBean saveMotorShaft(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorShaft motor = reGson.fromJson(data, new TypeToken<MotorShaft>(){}.getType());
		
		try
		{
			motorService.insertMotorShaft(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorShaft/delete.do")  
    public @ResponseBody ReturnBean deleteMotorShaftById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorShaftById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorShaft/findById.do")  
    public @ResponseBody MotorShaft findMotorShaftById(@RequestParam("id") String id){
		return motorService.findMotorShaftById(id);
	} 
	
	@RequestMapping(value="/motorShaft/list.do")  
    public @ResponseBody List<MotorShaft> listMotorShaft(@RequestParam("brandId") String brandId){
		return motorService.listMotorShaft(brandId);
	} 
	
	/**********************************防护****************************************/
	@RequestMapping(value="/motorDefend/save.do")  
    public @ResponseBody ReturnBean saveMotorDefend(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorDefend motor = reGson.fromJson(data, new TypeToken<MotorDefend>(){}.getType());
		
		try
		{
			motorService.insertMotorDefend(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorDefend/delete.do")  
    public @ResponseBody ReturnBean deleteMotorDefendById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorDefendById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorDefend/findById.do")  
    public @ResponseBody MotorDefend findMotorDefendById(@RequestParam("id") String id){
		return motorService.findMotorDefendById(id);
	} 
	/**
	 * 防护列表
	 * @return
	 */
	@RequestMapping(value="/motorDefend/list.do")  
    public @ResponseBody List<MotorDefend> listMotorDefend(@RequestParam("brandId") String brandId){
		return motorService.listMotorDefend(brandId);
	} 
	/**********************************防爆****************************************/
	@RequestMapping(value="/motorAntiExplosion/save.do")  
    public @ResponseBody ReturnBean saveMotorAntiExplosion(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorAntiExplosion motor = reGson.fromJson(data, new TypeToken<MotorAntiExplosion>(){}.getType());
		
		try
		{
			motorService.insertMotorAntiExplosion(motor);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorAntiExplosion/delete.do")  
    public @ResponseBody ReturnBean deleteMotorAntiExplosionById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorAntiExplosionById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorAntiExplosion/findById.do")  
    public @ResponseBody MotorAntiExplosion findMotorAntiExplosionById(@RequestParam("id") String id){
		return motorService.findMotorAntiExplosionById(id);
	} 
	/**
	 * 防爆列表
	 * @param brandId
	 * @return
	 */
	@RequestMapping(value="/motorAntiExplosion/list.do")  
    public @ResponseBody List<MotorAntiExplosion> listMotorAntiExplosion(@RequestParam("brandId") String brandId){
		return motorService.listMotorAntiExplosion(brandId);
	}
	
	/**********************************加热****************************************/
	@RequestMapping(value="/motorHeater/save.do")  
    public @ResponseBody ReturnBean saveMotorHeater(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MotorHeater motorHeater = reGson.fromJson(data, new TypeToken<MotorHeater>(){}.getType());
		
		try
		{
			motorService.insertMotorHeater(motorHeater);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorHeater/delete.do")  
    public @ResponseBody ReturnBean deleteMotorHeaterById(@RequestParam("id") String id){
		try
		{
			motorService.deleteMotorHeaterById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/motorHeater/findById.do")  
    public @ResponseBody MotorHeater findMotorHeaterById(@RequestParam("id") String id){
		return motorService.findMotorHeaterById(id);
	} 
	
	@RequestMapping(value="/motorHeater/findAll.do")  
    public @ResponseBody List<MotorHeater> listMotorHeaterBySeriesText(){
		return motorService.listMotorHeaterAll();
	} 
	
	@RequestMapping(value="/motorHeater/list.do")  
    public @ResponseBody List<MotorHeater> listMotorHeaterBySeriesText(
    		@RequestParam("seriesText") String seriesText,@RequestParam("brandId") String brandId){
		return motorService.listMotorHeaterBySeriesText(seriesText,brandId);
	} 


}
