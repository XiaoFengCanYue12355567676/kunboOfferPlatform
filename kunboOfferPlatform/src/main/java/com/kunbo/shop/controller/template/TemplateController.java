package com.kunbo.shop.controller.template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.TemplateAssortPort;
import com.kunbo.shop.entity.template.TemplateHalfProduct;
import com.kunbo.shop.entity.template.TemplateInfo;
import com.kunbo.shop.entity.template.TemplateManHour;
import com.kunbo.shop.entity.template.TemplateMaterial;
import com.kunbo.shop.entity.template.TemplateParameter;
import com.kunbo.shop.entity.template.TemplateStandard;
import com.kunbo.shop.service.template.TemplateService;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/shop/template")
public class TemplateController {

	@Autowired TemplateService templateService;
	
	/******************************************参数接口******************************************/
	@RequestMapping(value="/parameter/save.do")  
	public @ResponseBody ReturnBean  insertParameter(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateParameter templateParameter = reGson.fromJson(data, new TypeToken<TemplateParameter>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateParameter.getSortId(),
					templateParameter.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertParameter(level,pid,templateParameter);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/parameter/update.do")  
	public @ResponseBody ReturnBean  updateParameter(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateParameter templateParameter = reGson.fromJson(data, new TypeToken<TemplateParameter>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.PARAMETER,
					templateInfoId,
					templateParameter.getSortId(),
					templateParameter.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateParameter(templateParameter);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/parameter/findById.do")  
	public @ResponseBody TemplateParameter findParameterById(@RequestParam("id") String id)
	{
		return templateService.findParameterById(id);
	}
	@RequestMapping(value="/parameter/findByTemplateId.do")  
	public @ResponseBody TemplateParameter findByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findParameterByTemplateId(id);
	}
	
	@RequestMapping(value="/parameter/deleteById.do")  
	public @ResponseBody ReturnBean  deleteParameterById(String id)
	{
		return null;
	}
	/******************************************原材料接口******************************************/
	@RequestMapping(value="/material/save.do")  
	public @ResponseBody ReturnBean  insertMaterial(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateMaterial templateMaterial = reGson.fromJson(data, new TypeToken<TemplateMaterial>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateMaterial.getSortId(),
					templateMaterial.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertMaterial(level,pid,templateMaterial);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/material/update.do")  
	public @ResponseBody ReturnBean  updateMaterial(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateMaterial templateMaterial = reGson.fromJson(data, new TypeToken<TemplateMaterial>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.MATERIAL,
					templateInfoId,
					templateMaterial.getSortId(),
					templateMaterial.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateMaterial(templateMaterial);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/material/findById.do")  
	public @ResponseBody TemplateMaterial findMaterialById(@RequestParam("id") String id)
	{
		return templateService.findMaterialById(id);
	}
	@RequestMapping(value="/material/findByTemplateId.do")  
	public @ResponseBody TemplateMaterial findMaterialByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findMaterialByTemplateId(id);
	}
	/******************************************选配件接口******************************************/
	@RequestMapping(value="/assortPort/save.do")  
	public @ResponseBody ReturnBean insertAssortPort(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateAssortPort templateAssortPort = reGson.fromJson(data, new TypeToken<TemplateAssortPort>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateAssortPort.getSortId(),
					templateAssortPort.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertAssortPort(level,pid,templateAssortPort);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/assortPort/update.do")  
	public @ResponseBody ReturnBean  updateAssortPort(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateAssortPort templateAssortPort = reGson.fromJson(data, new TypeToken<TemplateAssortPort>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.ASSORT_PORT,
					templateInfoId,
					templateAssortPort.getSortId(),
					templateAssortPort.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateAssortPort(templateAssortPort);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/assortPort/findByTemplateId.do")  
	public @ResponseBody TemplateAssortPort findAssortPortByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findAssortPortByTemplateId(id);
	}
	@RequestMapping(value="/assortPort/findAllBySortId.do")  
	public @ResponseBody List<TemplateInfo> findAllTemplateInfoAssertPortBySortId(@RequestParam("sortId") String sortId)
	{
		return templateService.findAllAssertPortBySortId(sortId);
	}
	/******************************************半成品接口******************************************/
	@RequestMapping(value="/halfProduct/save.do")  
	public @ResponseBody ReturnBean insertHalfProduct(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateHalfProduct templateHalfProduct = reGson.fromJson(data, new TypeToken<TemplateHalfProduct>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateHalfProduct.getSortId(),
					templateHalfProduct.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertHalfProduct(level,pid,templateHalfProduct);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/halfProduct/update.do")  
	public @ResponseBody ReturnBean  updateHalfProduct(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateHalfProduct templateHalfProduct = reGson.fromJson(data, new TypeToken<TemplateHalfProduct>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.HALF_PRODUCT,
					templateInfoId,
					templateHalfProduct.getSortId(),
					templateHalfProduct.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateHalfProduct(templateHalfProduct);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/halfProduct/findByTemplateId.do")  
	public @ResponseBody TemplateHalfProduct findHalfProductByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findHalfProductByTemplateId(id);
	}
	
	/******************************************工时接口******************************************/
	@RequestMapping(value="/manHour/save.do")  
	public @ResponseBody ReturnBean insertManHour(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateManHour templateManHour = reGson.fromJson(data, new TypeToken<TemplateManHour>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateManHour.getSortId(),
					templateManHour.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertManHour(level,pid,templateManHour);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/manHour/update.do")  
	public @ResponseBody ReturnBean  updateManHour(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateManHour templateManHour = reGson.fromJson(data, new TypeToken<TemplateManHour>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.STANDARD_PORT,
					templateInfoId,
					templateManHour.getSortId(),
					templateManHour.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateManHour(templateManHour);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/manHour/findByTemplateId.do")  
	public @ResponseBody TemplateManHour findManHourByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findManHourByTemplateId(id);
	}
	
	/******************************************标准件接口******************************************/
	@RequestMapping(value="/standard/save.do")  
	public @ResponseBody ReturnBean insertStandard(
			@RequestParam("data") String data,
			@RequestParam("pid") String pid,
			@RequestParam("level") int level)
	{
		Gson reGson = new Gson();
		TemplateStandard templateStandard = reGson.fromJson(data, new TypeToken<TemplateStandard>(){}.getType());
		
		try
		{
			boolean r = templateService.insertProductInfoDuplicateChecking(
					templateStandard.getSortId(),
					templateStandard.getText());
			if(r == false) return new ReturnBean(false,"别称不能与其他部件重复"); 
			
			templateService.insertStandard(level,pid,templateStandard);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/standard/update.do")  
	public @ResponseBody ReturnBean  updateStandard(HttpServletRequest request,HttpServletResponse response)
	{
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String templateInfoId = request.getParameter("templateInfoId").replaceAll("\"\"", "null");
		TemplateStandard templateStandard = reGson.fromJson(data, new TypeToken<TemplateStandard>(){}.getType());
		
		try
		{
			boolean r = templateService.updateProductInfoDuplicateChecking(
					AttriType.STANDARD_PORT,
					templateInfoId,
					templateStandard.getSortId(),
					templateStandard.getText());
			if(r == false) return new ReturnBean(false,"修改别称不能与其他部件重复"); 
			
			templateService.updateStandard(templateStandard);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}	
	@RequestMapping(value="/standard/findByTemplateId.do")  
	public @ResponseBody TemplateStandard findStandardByTemplateId(@RequestParam("id") String id)
	{
		return templateService.findStandardByTemplateId(id);
	}
	@RequestMapping(value="/standard/findById.do")  
	public @ResponseBody TemplateStandard findStandardById(@RequestParam("id") String id)
	{
		return templateService.findStandardById(id);
	}
	
	
	
	/******************************************概览接口******************************************/
	@RequestMapping(value="/info/getInfoBySortIdForTree.do")  
    public @ResponseBody List<TemplateInfo> getInfoBySortIdForTree(@RequestParam("sortId") String sortId){
		return templateService.getInfoBySortIdForTree(sortId);
	}
	@RequestMapping(value="/info/getInfoForFilter.do")  
    public @ResponseBody List<TemplateInfo> getInfoForFilter(@RequestParam("sortId") String sortId){
		return templateService.getInfoForFilter(sortId);
	}
	
	
	@RequestMapping(value="/info/deleteById.do")  
    public @ResponseBody ReturnBean deleteInfoById(@RequestParam("id") String id){
		return templateService.deleteInfoById(id);
	}
	
	/** 
	* @Description: 更新是否参与过滤 
	* @Title: updateFilter 
	* @param id
	* @param filter
	* @return ReturnBean
	* @author qin
	* @date 2018年12月15日上午11:35:29
	*/ 
	@RequestMapping(value="/info/updateFilter.do")  
    public @ResponseBody ReturnBean updateFilter(
    		@RequestParam("id") String id,
    		@RequestParam("filter") boolean filter){
		return templateService.updateFilter(id,filter);
	}
	/** 
	* @Description: 跟新是否参与报价计算 
	* @Title: updateQuote 
	* @param id
	* @param quote
	* @return ReturnBean
	* @author qin
	* @date 2018年12月15日上午11:36:33
	*/ 
	@RequestMapping(value="/info/updateQuote.do")  
    public @ResponseBody ReturnBean updateQuote(
    		@RequestParam("id") String id,
    		@RequestParam("quote") boolean quote){
		return templateService.updateQuote(id,quote);
	}	
	/** 
	* @Description: 更新计算顺序  
	* @Title: updateCalculateSequ 
	* @param id
	* @param calculateSequ
	* @return ReturnBean
	* @author qin
	* @date 2019年1月6日下午4:15:22
	*/ 
	@RequestMapping(value="/info/updateCalculateSequ.do")  
    public @ResponseBody ReturnBean updateCalculateSequ(
    		@RequestParam("id") String id,
    		@RequestParam("calculateSequ") int calculateSequ){
		return templateService.updateCalculateSequ(id,calculateSequ);
	}	

	
	
	//测试
	@RequestMapping(value="/info/productInfoDuplicateChecking.do")  
	public @ResponseBody ReturnBean  productInfoDuplicateChecking(
    		@RequestParam("sortId") String sortId,
    		@RequestParam("text") String text)
	{
		boolean r = templateService.insertProductInfoDuplicateChecking(sortId,text);
		return new ReturnBean(r,"");		
	}
	//测试
	@RequestMapping(value="/info/updateProductInfoDuplicateChecking.do")  
	public @ResponseBody ReturnBean  updateProductInfoDuplicateChecking(
			@RequestParam("attriType") String attriType,
			@RequestParam("id") String id,
    		@RequestParam("sortId") String sortId,
    		@RequestParam("text") String text)
	{
		boolean r = templateService.updateProductInfoDuplicateChecking(AttriType.valueOf(attriType),id,sortId,text);
		return new ReturnBean(r,"");		
	}	
	
//	@RequestMapping(value="/parameter/delete.do")  
//	public @ResponseBody ReturnBean  deleteParameter(HttpServletRequest request,HttpServletResponse response)
//	{
//		
//	}
//	@RequestMapping(value="/parameter/findById.do")  
//	public @ResponseBody ReturnBean  findParameterById(String id)
//	{
//		
//	}
	
}
