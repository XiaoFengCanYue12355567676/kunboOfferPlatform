package com.kunbo.shop.controller.component;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.shop.entity.component.StandardComponent;
import com.kunbo.shop.entity.component.StandardType;
import com.kunbo.shop.service.component.StandardService;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/shop/standard")
public class StandardController {
	@Autowired StandardService standardService;
	
	
//	public ReturnBean insertStandardType(StandardType standardType);
//	public ReturnBean deleteStandardTypeById(String id);
//	public List<StandardType> getStandardTypeForList();
	
	@RequestMapping(value="/standardType/save.do")  
    public @ResponseBody ReturnBean insertStandardType(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		StandardType standardType = reGson.fromJson(data, new TypeToken<StandardType>(){}.getType());
		
		try
		{
			standardService.insertStandardType(standardType);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/standardType/findById.do")  
    public @ResponseBody StandardType findStandardTypeById(@RequestParam("id") String id){
		return standardService.findStandardTypeById(id);
	}
	@RequestMapping(value="/standardType/delete.do")  
    public @ResponseBody ReturnBean deleteStandardTypeById(@RequestParam("id") String id){
		try
		{
			standardService.deleteStandardTypeById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/standardType/list.do")  
    public @ResponseBody List<StandardType> getStandardTypeForList(){
		return standardService.getStandardTypeForList();
	}
	
	
	
//	public ReturnBean insertStandardComponent(StandardComponent standardComponent);
//	public ReturnBean deleteStandardComponentById(String id);
//	public List<StandardComponent> getStandardComponentForListByType(String typeId);
//	public ReturnBean deleteStandardComponentByTypeId(String id);
//	public StandardComponent findStandardComponentById(String id);
	
	
	@RequestMapping(value="/standardComponent/save.do")  
    public @ResponseBody ReturnBean insertStandardComponent(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		StandardComponent standardComponent = reGson.fromJson(data, new TypeToken<StandardComponent>(){}.getType());
		
		try
		{
			standardService.insertStandardComponent(standardComponent);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/standardComponent/findById.do")  
    public @ResponseBody StandardComponent findStandardComponentById(@RequestParam("id") String id){
		return standardService.findStandardComponentById(id);
	}
	@RequestMapping(value="/standardComponent/delete.do")  
    public @ResponseBody ReturnBean deleteStandardComponentById(@RequestParam("id") String id){
		try
		{
			standardService.deleteStandardComponentById(id);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/standardComponent/listByTypeId.do")  
    public @ResponseBody List<StandardComponent> getStandardComponentForListByType(@RequestParam("pid") String pid){
		return standardService.getStandardComponentForListByType(pid);
	}
	
}
