package com.kunbo.shop.controller.product;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Data;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.shop.dao.product.ProductInfoDao;
import com.kunbo.shop.dao.sort.ProductSortDao;
import com.kunbo.shop.entity.Product.ProductAssortPort;
import com.kunbo.shop.entity.Product.ProductHalfProduct;
import com.kunbo.shop.entity.Product.ProductInfo;
import com.kunbo.shop.entity.Product.ProductManHour;
import com.kunbo.shop.entity.Product.ProductMaterial;
import com.kunbo.shop.entity.Product.ProductParameter;
import com.kunbo.shop.entity.Product.ProductStandard;
import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.entity.sort.ProductSort.SORT_TYPE;
import com.kunbo.shop.entity.template.TemplateAssortPort;
import com.kunbo.shop.service.impl.product.ProductServiceImpl.ProductInfoView;
import com.kunbo.shop.service.product.ProductService;
import com.kunbo.shop.service.template.TemplateService;
import com.shuohe.util.returnBean.ReturnBean;

import ch.qos.logback.classic.Logger;

@RestController
@RequestMapping("/shop/product")
public class ProductController {

	@Autowired
	ProductService productService;

	@Autowired
	TemplateService templateService;

	@Autowired
	ProductInfoDao productInfoDao;

	@Autowired
	ProductSortDao productSortDao;

	/****************************************** 产品类型接口 ******************************************/
	@RequestMapping(value = "/save.do")
	public @ResponseBody ReturnBean save(HttpServletRequest request,
			HttpServletResponse response) {
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ProductSort productSort = reGson.fromJson(data,
				new TypeToken<ProductSort>() {
				}.getType());

		// 如果创建的是产品，则进行编码的查重确认
		if (productSort.getSort_type() == SORT_TYPE.PRODUCT) {
			ReturnBean r = productService.checkRepeatProduct(productSort);
			if (r.isResult() == false)
				return r;
		}

		try {
			productService.insert(productSort);
			return new ReturnBean(true, "");
		} catch (Exception e) {
			e.printStackTrace();
			return new ReturnBean(false, e.getMessage());
		}
	}

	@RequestMapping(value = "/getProductInfoForTreeByProductId.do")
	public @ResponseBody List<ProductInfoView> getProductInfoForTreeByProductId(
			@RequestParam("productId") String productId) {
		return productService.getProductInfoForTreeByProductId(productId);
	}

	/****************************************** 参数接口 ******************************************/
	@RequestMapping(value = "/parameter/findByProductInfoId.do")
	public @ResponseBody ProductParameter findParameterById(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findParameterByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/parameter/save.do")
	public @ResponseBody ReturnBean saveProductParameter(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductParameter productParameter = reGson.fromJson(data,
				new TypeToken<ProductParameter>() {
				}.getType());
		productService.saveProductParameter(productParameter);
		// 根据具体的参数编码找到所属的产品
		String productId = productService
				.findProductIdByProductInfoId(productParameter
						.getProductInfoId());
		productService.calculatorPorductPrice(productId);
		return new ReturnBean(true, "");
	}

	/****************************************** 材料接口 ******************************************/
	@RequestMapping(value = "/material/findByProductInfoId.do")
	public @ResponseBody ProductMaterial findMaterialByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findMaterialByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/material/save.do")
	public @ResponseBody ReturnBean saveProductMaterial(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductMaterial productMaterial = reGson.fromJson(data,
				new TypeToken<ProductMaterial>() {
				}.getType());
		productService.saveProductMaterial(productMaterial);

		// 根据具体的参数编码找到所属的产品
		String productId = productService
				.findProductIdByProductInfoId(productMaterial
						.getProductInfoId());
		productService.calculatorPorductPrice(productId);

		return new ReturnBean(true, "");
	}

	/**
	 * @Description: 找到材料数据模型的计算公式
	 * @Title: findMaterialTemplateFormula
	 * @param data
	 * @return ReturnBean
	 * @author qin
	 * @date 2018年11月30日上午10:49:05
	 */
	@RequestMapping(value = "/material/findTemplateFormula.do")
	public @ResponseBody ReturnBean findMaterialTemplateFormulaByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return new ReturnBean(
				true,
				productService
						.findMaterialTemplateFormulaByProductInfoId(productInfoId));
	}

	/**
	 * @Description: 计算材料用量
	 * @Title: calculatorMaterialUseage
	 * @param productInfoId
	 * @return String
	 * @author qin
	 * @date 2018年11月30日上午11:46:41
	 */
	@RequestMapping(value = "/material/calculator.do")
	public @ResponseBody ReturnBean calculatorMaterialUseage(
			@RequestParam("productInfoId") String productInfoId) {
		double result = productService.calculatorMaterialUseage(productInfoId);
		return new ReturnBean(true, result + "");
	}

	/****************************************** 配件接口 ******************************************/
	@RequestMapping(value = "/assortPort/findByProductInfoId.do")
	public @ResponseBody ProductAssortPort findAssortPortByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findAssortPortByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/assortPort/save.do")
	public @ResponseBody ReturnBean saveProductAssortPort(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductAssortPort productAssortPort = reGson.fromJson(data,
				new TypeToken<ProductAssortPort>() {
				}.getType());
		// 找到info的参数
		ProductInfo productInfo = productInfoDao.findById(productAssortPort
				.getProductInfoId());
		TemplateAssortPort templateAssortPort = templateService
				.findAssortPortByTemplateId(productInfo.getTemplateInfoId());
		// 找到外购件的模型数据，然后将数量赋予外购件产品
		productAssortPort.setNum(templateAssortPort.getNum());
		productAssortPort.setAssort_price(templateAssortPort.getNum()
				* productAssortPort.getUnit_price());
		productService.saveProductAssortPort(productAssortPort);

		// 保存半成品是把所有的半成品和材料都计算一次
		String productId = productService
				.findProductIdByProductInfoId(productAssortPort
						.getProductInfoId());
		productService.calculatorPorductPrice(productId);

		return new ReturnBean(true, "");
	}

	@RequestMapping(value = "/assortPort/priceAdjust.do")
	public @ResponseBody ReturnBean priceAdjust(
			@RequestParam("sortId") String sortId,
			@RequestParam("assortPortId") String assortPortId,
			@RequestParam("precent") float precent) {

		System.out.println("sortId =" + sortId);
		System.out.println("assortPortId =" + assortPortId);
		System.out.println("precent =" + precent);

		List<ProductAssortPort> l = productService
				.getProductAssortPortByTemplateInfoId(assortPortId);
		for (ProductAssortPort pap : l) {
			if (precent > 0) {
				pap.setUnit_price((1 + precent) * pap.getUnit_price());
				pap.setAssort_price(pap.getNum() * pap.getUnit_price());
			} else {
				pap.setUnit_price((1 + precent) * pap.getUnit_price());
				pap.setAssort_price(pap.getNum() * pap.getUnit_price());
			}
			productService.saveProductAssortPort(pap);
			// 保存半成品是把所有的半成品和材料都计算一次
			String productId = productService.findProductIdByProductInfoId(pap
					.getProductInfoId());
			productService.calculatorPorductPrice(productId);
		}
		return new ReturnBean(true, "更新数据" + l.size() + "条");
	}

	/****************************************** 工时接口 ******************************************/
	@RequestMapping(value = "/manHour/findByProductInfoId.do")
	public @ResponseBody ProductManHour findManHourByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findManHourByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/manHour/save.do")
	public @ResponseBody ReturnBean saveProductManHour(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductManHour productManHour = reGson.fromJson(data,
				new TypeToken<ProductManHour>() {
				}.getType());
		productService.saveProductManHour(productManHour);

		// 保存半成品是把所有的半成品和材料都计算一次
		String productId = productService
				.findProductIdByProductInfoId(productManHour.getProductInfoId());
		productService.calculatorPorductPrice(productId);

		return new ReturnBean(true, "");
	}

	/****************************************** 标准件接口 ******************************************/
	@RequestMapping(value = "/standard/findByProductInfoId.do")
	public @ResponseBody ProductStandard findStandardByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findStandardByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/standard/save.do")
	public @ResponseBody ReturnBean saveProductStandard(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductStandard productStandard = reGson.fromJson(data,
				new TypeToken<ProductStandard>() {
				}.getType());
		productService.saveProductStandard(productStandard);

		// 根据具体的参数编码找到所属的产品
		String productId = productService
				.findProductIdByProductInfoId(productStandard
						.getProductInfoId());
		productService.calculatorPorductPrice(productId);
		return new ReturnBean(true, "");
	}

	/****************************************** 半成品接口 ******************************************/
	@RequestMapping(value = "/halfProduct/findByProductInfoId.do")
	public @ResponseBody ProductHalfProduct findHalfproductByProductInfoId(
			@RequestParam("productInfoId") String productInfoId) {
		return productService.findHalfProductByProductInfoId(productInfoId);
	}

	@RequestMapping(value = "/halfProduct/save.do")
	public @ResponseBody ReturnBean saveHalfProduct(
			@RequestParam("data") String data) {
		Gson reGson = new Gson();
		ProductHalfProduct productHalfProduct = reGson.fromJson(data,
				new TypeToken<ProductHalfProduct>() {
				}.getType());
		productService.saveProductHalfProduct(productHalfProduct);

		// 保存半成品是把所有的半成品和材料都计算一次
		String productId = productService
				.findProductIdByProductInfoId(productHalfProduct
						.getProductInfoId());
		productService.calculatorPorductPrice(productId);
		return new ReturnBean(true, "");
	}

	@RequestMapping(value = "/calculatorPorductPriceByProductId.do")
	public @ResponseBody List<ProductInfoView> calculatorPorductPriceByProductId(
			@RequestParam("productId") String productId) {
		return productService.getProductInfoForTreeByProductId(productId);
	}

	@RequestMapping(value = "/getPorductTreeListByNewMaterial.do")
	public @ResponseBody List<ProductInfoView> getPorductTreeListByNewMaterial(
			@RequestParam("productId") String productId,
			@RequestParam("materialId") String materialId) {
		return productService.getPorductTreeListByNewMaterial(productId,
				materialId);
	}

	@RequestMapping(value = "/getProductParametersByTemplateInfoId.do")
	public @ResponseBody List<String> getProductParametersByTemplateInfoId(
			@RequestParam("templateInfoId") String templateInfoId) {
		return productService
				.getProductParametersByTemplateInfoId(templateInfoId);
	}

	@Data
	public class ParamqQuerys {
		String key;
		String value;
	}

	@RequestMapping(value = "/getProductByLineAndParams.do")
	public @ResponseBody List<Map<String, String>> getProductByLineAndParams(
			@RequestParam("productLine") String productLine,
			@RequestParam("data") String data) {

		Gson reGson = new Gson();
		String _data = data.replaceAll("\"\"", "null");
		List<ParamqQuerys> paramQuerys = reGson.fromJson(_data,
				new TypeToken<List<ParamqQuerys>>() {
				}.getType());

		return productService.getProductByLineAndParams(productLine,
				paramQuerys);
	}

	@RequestMapping(value = "/saveNoise.do")
	public @ResponseBody ReturnBean saveNoise(
			@RequestParam("productId") String productId,
			@RequestParam("data") String data) throws JSONException {
		String _data = data.replaceAll("\"\"", "null");
		ProductSort entity = productSortDao.findOne(productId);
		JSONObject obj = new JSONObject(_data);
		entity.setNoise125(Double.parseDouble(obj.getString("noise125")));
		entity.setNoise125s(Double.parseDouble(obj.getString("noise125s")));
		entity.setNoise63(Double.parseDouble(obj.getString("noise63")));
		entity.setNoise63s(Double.parseDouble(obj.getString("noise63s")));
		entity.setNoise250(Double.parseDouble(obj.getString("noise250")));
		entity.setNoise250s(Double.parseDouble(obj.getString("noise250s")));
		entity.setNoise500(Double.parseDouble(obj.getString("noise500")));
		entity.setNoise500s(Double.parseDouble(obj.getString("noise500s")));
		entity.setNoise1KHz(Double.parseDouble(obj.getString("noise1KHz")));
		entity.setNoise1KHzs(Double.parseDouble(obj.getString("noise1KHzs")));
		entity.setNoise2KHz(Double.parseDouble(obj.getString("noise2KHz")));
		entity.setNoise2KHzs(Double.parseDouble(obj.getString("noise2KHzs")));
		entity.setNoise4KHz(Double.parseDouble(obj.getString("noise4KHz")));
		entity.setNoise4KHzs(Double.parseDouble(obj.getString("noise4KHzs")));
		entity.setNoise8KHz(Double.parseDouble(obj.getString("noise8KHz")));
		entity.setNoise8KHzs(Double.parseDouble(obj.getString("noise8KHzs")));
		productSortDao.save(entity);
		return new ReturnBean(true, "");
	}
	@RequestMapping(value = "/getProductSortEntity.do")
	public @ResponseBody ProductSort getProductSortEntity(
			@RequestParam("productId") String productId) throws JSONException {
		ProductSort entity = productSortDao.findOne(productId);
		if(entity == null){
			return null;
		}
		return entity;
	}
	
	/** 
	* @Description:  复制一个产品
	* @Title: copyProduct 
	* @param productId
	* @return
	* @throws JSONException ReturnBean
	* @author qin
	* @date 2019年5月27日下午1:50:52
	*/ 
	@RequestMapping(value = "/copyProduct.do")
	public @ResponseBody ReturnBean copyProduct(
			@RequestParam("productId") String productId,
			@RequestParam("productNumber") String productNumber, 
			@RequestParam("text") String text) {
		return productService.copyProduct(productId,productNumber,text);
	}
	
	
}
