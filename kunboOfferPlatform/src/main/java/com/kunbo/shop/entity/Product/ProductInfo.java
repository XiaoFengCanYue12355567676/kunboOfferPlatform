package com.kunbo.shop.entity.Product;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.OptionType;
import com.kunbo.shop.entity.template.TemplateInfo;

import lombok.Data;

/**
 * 产品模型的组成组件
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_info")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductInfo {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String productId;//产品本身的编码
	
	String pid;
	
	String text;
	
	String sortId;//产品模型的编号
	
	String templateInfoId; //产品模型信息的父类编码
	
	String unit;
	
	AttriType attriType;
	
	OptionType optionType;
		
	@Transient
	List<ProductInfo> children;
	
	int level;		//在计算结构树中存在的层级
	
	boolean isFilter;	//是否作为筛选项
	
	boolean isQuote;	//是否作为报价项
	
	int calculateSequ; //运算顺序
}
