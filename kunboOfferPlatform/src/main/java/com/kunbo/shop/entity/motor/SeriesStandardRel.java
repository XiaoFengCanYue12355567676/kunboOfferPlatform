package com.kunbo.shop.entity.motor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 电机品牌
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_motor_seriesStandard")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class SeriesStandardRel {

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String brand;//厂家
		
	String seriesText;//系列名称
	
	String standardText;//基座号
	
	String standardFactor;//中准系数
	
	
}
