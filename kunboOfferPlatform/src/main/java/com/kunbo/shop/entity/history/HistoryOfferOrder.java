package com.kunbo.shop.entity.history;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="db_offer_history_offer")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class HistoryOfferOrder {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;//报价单别称
	
	String quotePeopleId;//报价人编号
	
	String quotePeopleName;//报价人名称
	
	String quotePeopleAccount;//报价人账号
	
	Date  quoteDate;	//报价单发起时间
	
	Date quoteLastModifyDate;//报价单最后修改时间
	
	String customerName;//客户名称
	
	String customerId;//客户编码
	
	int productCount;		//产品数量
	
	int productTypeCount;	//产品种类
	
	double offerTotalPrice;//报价总价
	
	double totalPrice;//成本总价

}
