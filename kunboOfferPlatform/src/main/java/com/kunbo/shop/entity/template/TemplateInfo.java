package com.kunbo.shop.entity.template;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 产品模型的组成组件
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_template_info")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class TemplateInfo {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String pid;
	
	String text;
	
	String sortId;//产品模型的编号
	
	String unit;
	
	AttriType attriType;
	
	OptionType optionType;
		
	@Transient
	List<TemplateInfo> children;

	int level;		//在计算结构树中存在的层级
	
	boolean isFilter;	//是否作为筛选项
	
	boolean isQuote;	//是否作为报价项
	
	int calculateSequ; //运算顺序
	
	@Transient
	String formula;//计算公式
}
