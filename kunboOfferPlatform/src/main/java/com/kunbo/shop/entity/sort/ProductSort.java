package com.kunbo.shop.entity.sort;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 产品的大类分类
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_sort")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductSort {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	
	private String pid = "";//上级id
	
	String text;//类型名称
	
	SORT_TYPE sort_type;//分类
	
	String productNumber; //产品编码
	
	private String pressureCurveFormula;//压力曲线公式（全压）	
	private String efficiencyCurveFormula;//效率曲线公式	
	private String internalPowerCurveFormula;//内功率曲线公式	
	private Double pressureRealityRatio;//流量压力实际系数
	private Double efficiencyRealityRatio;//流量效率实际系数
	private Double internalPowerRealityRatio;//流量内功率实际系数
	
	private Double minFlux;//最小流量
	private Double maxFlux;//最大流量
	
	private String staticCurveFormula;//压力曲线公式（静压）
	private Double noise63;//63Hz噪声  PWL
    private Double noise125;//125Hz噪声
    private Double noise250;//250Hz噪声
    private Double noise500;//500Hz噪声
    private Double noise1KHz;//1KHz噪声
    private Double noise2KHz;//2KHz噪声
    private Double noise4KHz;//4KHz噪声
    private Double noise8KHz;//8KHz噪声
    private Double noise63s;//63Hz噪声  SPL
    private Double noise125s;//125Hz噪声
    private Double noise250s;//250Hz噪声
    private Double noise500s;//500Hz噪声
    private Double noise1KHzs;//1KHz噪声
    private Double noise2KHzs;//2KHz噪声
    private Double noise4KHzs;//4KHz噪声
    private Double noise8KHzs;//8KHz噪声
	@Transient
	List<ProductSort> children;
	/**
	 * 产品分类类别
	 * @author qin
	 *
	 */
	public enum SORT_TYPE
	{
		GENERA,
		PRODUCT_LINE,
		PRODUCT
	}
	
	
	int responsible_id;	//责任人id
	String responsible_name;	//责任人名称
}
