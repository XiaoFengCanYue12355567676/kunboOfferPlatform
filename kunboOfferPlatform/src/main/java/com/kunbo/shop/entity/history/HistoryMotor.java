package com.kunbo.shop.entity.history;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;


@Entity
@Table(name="db_offer_history_motor")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class HistoryMotor {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String orderId;
	
	String historypProductId;
	
    String motorBrand;
    String motor;
    String motorSeries;
    String diffeVoltage;
    String motorIpLevel;
    String motorInstallation;
    
    String moistTemperature;//：潮温系数
    String motorAntiExplosion;//：防爆系数
    String motorBearing;//：轴承
    String motorDefend;//：防护
    
    boolean priceOfCoeff;//：填料函方式
    String motorPadding;//：填料函
    
    String motorShaft;//：电机轴
    
    String motorHeater;
    
}
