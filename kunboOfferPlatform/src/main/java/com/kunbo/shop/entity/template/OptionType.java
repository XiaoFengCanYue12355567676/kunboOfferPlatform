package com.kunbo.shop.entity.template;

/**
 * 模型的装配方式，决定半成品下级的半成品是否能够单选、多选、必选
 * @author qin
 *
 */
public enum OptionType {
	ALL_SELECT,		//全选
	MULTI_SELECT,	//多选
	SINGLE_SELECT	//单选
}
