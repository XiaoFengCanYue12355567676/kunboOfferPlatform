package com.kunbo.shop.entity.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 配套件模型
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_template_assortport")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class TemplateAssortPort{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String templateInfoId;//模型概述编号
	
	int num;//外购件数量
	
//	String unit;
	
//	String assortId;//配套件编码
//	
//	float assortPrice;	//配套件单价

}
