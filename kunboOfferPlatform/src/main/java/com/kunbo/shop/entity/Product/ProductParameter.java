package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 参数模型：参数只用来作为计算的一部分
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_parameter")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductParameter{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sort_id;//产品模型的编号
	
	String productId;	//产品编号
	
	String productInfoId;
	
	String templateId;//具体对引导模型内的编号
	
	double value;	//参数的实际值
	
	String unit;
		
	boolean isfilter;	//是否能作为过滤条件
}
