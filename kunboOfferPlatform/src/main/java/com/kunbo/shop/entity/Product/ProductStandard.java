package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 原材料模型
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_standard")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductStandard{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String productId;	//产品编号
	
	String productInfoId;//模型概述编号

	String standardSortId;	//标准品大类编码
	
	String templateId;//具体对引导模型内的编号
	
	String standardId;	//标准品编码

	String standardName;//标准品名称
	
	double unitPrice;//单价

}
