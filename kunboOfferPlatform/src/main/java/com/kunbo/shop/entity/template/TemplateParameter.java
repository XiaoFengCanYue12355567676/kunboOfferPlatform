package com.kunbo.shop.entity.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 参数模型
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_template_parameter")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class TemplateParameter{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String templateInfoId;//模型概述编号
	
	String unit;
		
	boolean isfilter;	//是否能作为过滤条件
}
