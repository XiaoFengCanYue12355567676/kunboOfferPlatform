package com.kunbo.shop.entity.history;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.OptionType;
import com.kunbo.shop.service.impl.product.ProductServiceImpl.ProductInfoView;

import lombok.Data;

/**
 * 记录单个产品报价的信息
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_history_porduct")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class HistoryProduct {
	
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
		
	String orderId;	//HistoryOfferOrder.id
	
	String productNumber;//产品编号	ProductSort.productNumber   以产品编号为准
	
	String MotorId;	//电机编号Motor.id
	
	boolean isChanged;	//是否有内部配件发生改过变更
	
	int num;		//数量
	
	double costPrice;//成本
	
	double unitPrice;	//成本单价
	
	double totalPrice;	//成本总价
	
	double offerUnitPrice;	//报价单价
	
	double offerTotalPrice;	//报价总价
		
	Date quoteDate;//报价时间
	
	String productLineId;
	
	String materialId;//选择材料的编码
	
	@Column(name="productJson", columnDefinition="LONGTEXT", nullable=true)
	String productJson;//产品的结构json

	@Transient
	HistoryMotor motor;
	@Transient
	List<HistoryPorductInfo> infos;	
	@Transient
	List<HistoryProductParams> params;
	
	// @Fields fanProfitRate : 风机利润率
	double fanProfitRate;
	
	// @Fields motorProfitRate : 电机利润率
	double motorProfitRate;
	
	boolean isDiscard = false;//是否废弃
	
}
