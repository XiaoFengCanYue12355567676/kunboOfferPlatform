package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 材料模型
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_material")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductMaterial{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String productId;	//产品编号
	
	String materialId;//材料编码
	
	String productInfoId;//模型概述编号
	
	String templateId;//具体对引导模型内的编号
	
	double unitPrice;	//单价

	String unit;		//单位
	
	double usage_amount;	//使用量

	String defaultMateId;//默认材料编码
	
	String defaultMateName; //默认材料名称
	
	double defaultMatePrice;	//默认材料单价
	
	String formula;//计算公式
	
	double defaultPrice;	//默认材料小计价格
}
