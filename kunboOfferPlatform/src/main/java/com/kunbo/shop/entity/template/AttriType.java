package com.kunbo.shop.entity.template;

/**
 * 属性类型：描述模型的组成部分的属性
 * @author qin
 *
 */
public enum AttriType {
	HALF_PRODUCT,	//半成品
	PARAMETER,		//参数
	STANDARD_PORT,	//标准品
	MANHOUR,		//工时
	MATERIAL,		//材料	
	ASSORT_PORT		//配套件
}
