package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 配套件模型：配套件只有价格，是一个客制化的价格
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_assortport")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductAssortPort{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String productId;	//产品编号
	
	String productInfoId;//产品概述编号
	
	String templateId;//具体对引导模型内的编号
	
	double assort_price;	//配套件单价
	
	int num;//外购件数量
	
	double unit_price;//配套件总价

}
