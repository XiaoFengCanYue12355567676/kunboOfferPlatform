package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.kunbo.shop.entity.template.OptionType;

import lombok.Data;

/**
 * 半成品模型：半成品模型体现的是通过计算公式算出的价格
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_halfproduct")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductHalfProduct{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String productInfoId;//模型概述编号
	
	String productId;	//产品编号
	
	OptionType optionType;	//装配方式
	
	String templateId;//具体对引导模型内的编号
	
	String unit;//（废弃）

	String formula;//计算公式
	
	double price;//半成品价格
}
