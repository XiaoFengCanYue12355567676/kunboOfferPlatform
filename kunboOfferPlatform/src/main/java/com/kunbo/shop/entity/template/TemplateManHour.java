package com.kunbo.shop.entity.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 工时模型
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_template_manhour")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class TemplateManHour{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;	//自定义工时名称
	
	String sortId;//产品模型的编号
	
	String manhourId;//工时种类对应的编码
	
	String manhourName;//工时类型名称
	
	String templateInfoId;//模型概述编号
	
	float unitPrice;	//工时单价
	
	String unit;

}
