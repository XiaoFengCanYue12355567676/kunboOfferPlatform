package com.kunbo.shop.entity.Product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 工时模型：工时的计算为：工时单价* 工时数量
 * @author qin
 *
 */
@Entity
@Table(name="db_offer_product_manhour")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductManHour{

	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String text;
	
	String sortId;//产品模型的编号
	
	String productId;	//产品编号
	
	String manhourId;//工时种类对应的编码
	
	String manhourName;//工时类型名称
	
	String productInfoId;//模型概述编号
	
	String templateId;//具体对引导模型内的编号
	
	double unitPrice;	//工时单价
	
	double number;		//工时数量
	
	double price;		//工时价格
	
	String unit;

}
