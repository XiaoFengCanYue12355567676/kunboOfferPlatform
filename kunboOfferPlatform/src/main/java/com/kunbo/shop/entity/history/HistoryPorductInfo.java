package com.kunbo.shop.entity.history;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.kunbo.shop.entity.template.AttriType;
import com.kunbo.shop.entity.template.OptionType;
import com.kunbo.shop.service.impl.product.ProductServiceImpl.ProductInfoView;

import lombok.Data;

@Entity
@Table(name="db_offer_history_porduct_info")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class HistoryPorductInfo {
	
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String newId;//主键uuid
	
	String orderId;
	
	String historypProductId;
	
	String id;	//原始数据Id，也就是默认参数的那个编码
	
	String productId;//产品本身的编码
	
	String pid;	//原始数据父Id，也就是默认参数的那个父编码
	
	String text;
	
	String sortId;//产品模型的编号
	
	String templateInfoId; //产品模型信息的父类编码
	
	String offerProductId;	//某个产品目前的报价Id，与HistoryProduct.id 关联
	
	//产品上具体组件的编码，关系到后期组件本身调整带来的价格变更
	//对应于ProductAssortPort.id、ProductHalfProduct.id  
	//ProductHalfProduct.id  ProductManHour.id  ProductMaterial.id  ProductParameter.id
	//ProductStandard.id
	String productCompId;
	
	String unit;
	
	AttriType attriType;
	
	OptionType optionType;
		
	@Transient
	List<HistoryPorductInfo> children;
	
	//参数
	double value;	//参数的实际值
	
	//配套件
	double assort_price;	//配件的价格
	
	//工时
	double unitPrice;	//工时单价&标准品单价
	
	double number;		//工时数量
	
	double price;		//工时价格
	
	//材料
	double usage_amount;	//使用量

	String formula;//计算公式
	
	int level;
	
	boolean isFilter;	//是否作为筛选项
	
	boolean isQuote;	//是否作为报价项
	
	boolean isCheck;	//是否选中
}
