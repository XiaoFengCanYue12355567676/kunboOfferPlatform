package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductAssortPort;
import com.kunbo.shop.entity.Product.ProductHalfProduct;

public interface ProductHalfProductDao extends JpaRepository<ProductHalfProduct, Long> {
	public ProductHalfProduct findById(String id);
	public ProductHalfProduct findByProductInfoId(String productInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductHalfProduct where productId= ?1")
	public int deleteByProductId(String id);
	
	@Modifying
	@Transactional
	@Query("delete ProductHalfProduct where templateId= ?1")
	public int deleteByTemplateId(String templateId);
	
	public List<ProductHalfProduct> findByTemplateId(String templateId);
}
