package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorSeries;
import com.kunbo.shop.entity.motor.MotorStandardNum;

public interface MotorSeriesDao extends JpaRepository<MotorSeries, Long> {
	
	@Modifying
	@Transactional
	@Query("delete MotorSeries where id= ?1")
	public int deleteById(String id);
	
	
	public MotorSeries findById(String id);
	/**
	 * 系列集合
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorSeries> findByBrandId(String brandId);
}
