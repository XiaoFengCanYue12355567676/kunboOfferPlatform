package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.Motor;

public interface MotorDao extends JpaRepository<Motor, Long> {

	@Modifying
	@Transactional
	@Query("delete Motor where id= ?1")
	public int deleteById(String id);
	
	
	public Motor findById(String id);
	
	public List<Motor> findByBrandId(String brandId);
	
}
