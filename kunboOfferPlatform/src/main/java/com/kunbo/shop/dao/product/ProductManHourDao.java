package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductHalfProduct;
import com.kunbo.shop.entity.Product.ProductManHour;

public interface ProductManHourDao extends JpaRepository<ProductManHour, Long> {
	public ProductManHour findById(String id);
	public ProductManHour findByProductInfoId(String productInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductManHour where productId= ?1")
	public int deleteByProductId(String id);
	
	
	public List<ProductManHour> findByManhourId(String manhourId);
	
	@Modifying
	@Transactional
	@Query("delete ProductManHour where templateId= ?1")
	public int deleteByTemplateId(String templateId);
	
	public List<ProductManHour> findByTemplateId(String templateId);
}
