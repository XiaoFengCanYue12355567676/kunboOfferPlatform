package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateAssortPort;

public interface TemplateAssortPortDao extends JpaRepository<TemplateAssortPort, Long> {
	public TemplateAssortPort findById(String id);
	public TemplateAssortPort findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateAssortPort where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateAssortPort where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
}
