package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductInfo;

public interface ProductInfoDao extends JpaRepository<ProductInfo, Long> {
	
	
	public List<ProductInfo> findByProductId(String productId);
	
	
	
	public ProductInfo findById(String id);
	
//	@Query("select "
//	+ " ProductParameter"
//	+ " from "
//	+ " ProductInfo productInfo "
//	+ " join ProductParameter productParameter with productParameter.productInfoId = productInfo.id"
//	+ " where productInfo.productId= ?1"
//	+ " and productInfo.attriType = 'PARAMETER'")
	@Query("from ProductInfo where productId = ?1 order by level desc")
	public List<ProductInfo> findByProductIdOrderByLevel(String productId);
	
	
	@Query("from ProductInfo where productId = ?1 order by calculateSequ asc")
	public List<ProductInfo> findByProductIdOrderByCalculateSequ(String productId);
	
	@Modifying
	@Transactional
	@Query("delete ProductInfo where productId= ?1")
	public int deleteByProductId(String id);
	
	public List<ProductInfo> findByTemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductInfo where templateInfoId= ?1")
	public int deleteByTemplateInfoId(String templateInfoId);
	
}
