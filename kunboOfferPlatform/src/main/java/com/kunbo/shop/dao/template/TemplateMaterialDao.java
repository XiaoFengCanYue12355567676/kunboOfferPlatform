package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateMaterial;

public interface TemplateMaterialDao extends JpaRepository<TemplateMaterial, Long> {
	public TemplateMaterial findById(String id);
	public TemplateMaterial findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateMaterial where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateMaterial where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
}
