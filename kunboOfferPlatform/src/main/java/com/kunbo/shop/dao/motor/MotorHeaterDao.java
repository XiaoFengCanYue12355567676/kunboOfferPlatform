package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MotorHeater;

public interface MotorHeaterDao extends JpaRepository<MotorHeater, Long> {

	
	@Modifying
	@Transactional
	@Query("delete MotorHeater where id= ?1")
	public int deleteById(String id);
	
	
	public MotorHeater findById(String id);
	@Query("from MotorHeater where seriesText = ?1 and brandId= ?2")
	List<MotorHeater> findBySeriesTextAndBrandId(String seriesText,String brandId);
}
