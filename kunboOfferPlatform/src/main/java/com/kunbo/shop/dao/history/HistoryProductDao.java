package com.kunbo.shop.dao.history;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.history.HistoryProduct;

public interface HistoryProductDao extends JpaRepository<HistoryProduct, Long> {
	
	HistoryProduct findById(String id);
	
	List<HistoryProduct> findByOrderId(String orderId);
	@Modifying
	@Transactional
	@Query("delete HistoryProduct where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("update HistoryProduct set isDiscard = ?2 where id= ?1")
	public int updateToDiscard(String id,boolean isDiscard);
	
}
