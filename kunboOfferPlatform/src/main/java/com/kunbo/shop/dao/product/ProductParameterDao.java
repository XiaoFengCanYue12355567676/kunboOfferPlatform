package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductParameter;

public interface ProductParameterDao extends JpaRepository<ProductParameter, Long> {
	public ProductParameter findById(String id);
	public ProductParameter findByProductInfoId(String productInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductParameter where productId= ?1")
	public int deleteByProductId(String id);
	
	/** 
	* @Description: 根据产品ID获得所有的参数 
	* @Title: findByProductId 
	* @param productInfoId
	* @return String
	* @author qin
	* @date 2018年11月30日下午12:02:24
	*/ 
//	@Query("select "
//		+ " ProductParameter"
//		+ " from "
//		+ " ProductInfo productInfo "
//		+ " join ProductParameter productParameter with productParameter.productInfoId = productInfo.id"
//		+ " where productInfo.productId= ?1"
//		+ " and productInfo.attriType = 'PARAMETER'")
//	List<ProductParameter> findByProductId(String productId);
	
	@Query("from ProductParameter where productId in(?1) ")
	List<ProductParameter> findByProductIds(List<String> productIds);
	
	
	@Modifying
	@Transactional
	@Query("delete ProductParameter where templateId= ?1")
	public int deleteByTemplateId(String templateId);
	
	public List<ProductParameter> findByTemplateId(String templateId);
}
