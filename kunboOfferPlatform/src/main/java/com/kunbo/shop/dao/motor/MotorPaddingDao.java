package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorDefend;
import com.kunbo.shop.entity.motor.MotorPadding;

public interface MotorPaddingDao extends JpaRepository<MotorPadding, Long> {

	@Modifying
	@Transactional
	@Query("delete MotorPadding where id= ?1")
	public int deleteById(String id);
	
	
	public MotorPadding findById(String id);
	/**
	 * 填料
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorPadding> findByBrandId(String brandId);
}
