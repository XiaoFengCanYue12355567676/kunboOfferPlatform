package com.kunbo.shop.dao.component;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.component.StandardComponent;
import com.kunbo.shop.entity.component.StandardType;

public interface StandardComponentDao extends JpaRepository<StandardComponent, Long> {

	
	@Modifying
	@Transactional
	@Query("delete StandardComponent where id= ?1")
	public int deleteById(String id);
	
	
	@Modifying
	@Transactional
	@Query("delete StandardComponent where pid= ?1")
	public int deleteByTypeId(String pid);
	
	
	@Modifying
	@Transactional
	@Query("from StandardComponent where pid= ?1")
	public List<StandardComponent> findByTypeId(String pid);
	
	public StandardComponent findById(String id);
}
