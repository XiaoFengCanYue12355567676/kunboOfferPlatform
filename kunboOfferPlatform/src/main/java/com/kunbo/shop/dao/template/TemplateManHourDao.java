package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateManHour;

public interface TemplateManHourDao extends JpaRepository<TemplateManHour, Long> {
	public TemplateManHour findById(String id);
	public TemplateManHour findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateManHour where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateManHour where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
}
