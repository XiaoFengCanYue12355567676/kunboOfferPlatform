package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductStandard;

public interface ProductStandardDao extends JpaRepository<ProductStandard, Long> {
	public ProductStandard findById(String id);
	public ProductStandard findByProductInfoId(String productInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductStandard where productId= ?1")
	public int deleteByProductId(String id);
	
	
	@Modifying
	@Transactional
	@Query("delete ProductStandard where templateId= ?1")
	public int deleteByTemplateId(String templateId);
	
	public List<ProductStandard> findByTemplateId(String templateId);

}
