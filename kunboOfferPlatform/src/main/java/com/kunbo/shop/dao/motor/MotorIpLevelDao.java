package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorIpLevel;
import com.kunbo.shop.entity.motor.MotorStandardNum;

public interface MotorIpLevelDao extends JpaRepository<MotorIpLevel, Long> {
	
	@Modifying
	@Transactional
	@Query("delete MotorIpLevel where id= ?1")
	public int deleteById(String id);
	
	
	public MotorIpLevel findById(String id);
	/**
	 * IP等级
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorIpLevel> findByBrandId(String brandId);
}
