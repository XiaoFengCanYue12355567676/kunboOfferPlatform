package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductMaterial;

public interface ProductMaterialDao extends JpaRepository<ProductMaterial, Long> {
	public ProductMaterial findById(String id);
	public ProductMaterial findByProductInfoId(String productInfoId);
	
	@Modifying
	@Transactional
	@Query("delete ProductMaterial where productId= ?1")
	public int deleteByProductId(String id);
	 
	
	public List<ProductMaterial> findByTemplateId(String templateId);
	
	
	@Modifying
	@Transactional
	@Query("delete ProductMaterial where templateId= ?1")
	public int deleteByTemplateId(String templateId);
	
//	@Query("select "
//			+ " * "
//			+ " from "
//			+ " ProductMaterial as productMaterial "
//			+ " left join ProductInfo as productInfo on productInfo.id = productMaterial.productInfoId"
//			+ " left join "
//			+ "where templateInfoId= ?1")
//	String findMaterialTemplateFormulaByProductInfoId(String productInfoId);
}
