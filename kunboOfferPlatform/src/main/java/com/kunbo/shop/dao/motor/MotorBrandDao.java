package com.kunbo.shop.dao.motor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorBrand;

public interface MotorBrandDao extends JpaRepository<MotorBrand, Long> {

	
	@Modifying
	@Transactional
	@Query("delete MotorBrand where id= ?1")
	public int deleteById(String id);
	
	
	public MotorBrand findById(String id);
	
}
