package com.kunbo.shop.dao.history;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kunbo.shop.entity.history.HistoryPorductInfo;

public interface HistoryPorductInfoDao extends JpaRepository<HistoryPorductInfo, Long> {
	List<HistoryPorductInfo> findByHistorypProductId(String historypProductId);
}
