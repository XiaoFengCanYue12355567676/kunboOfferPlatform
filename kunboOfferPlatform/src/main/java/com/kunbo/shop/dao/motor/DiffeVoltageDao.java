package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.DiffeVoltage;

public interface DiffeVoltageDao extends JpaRepository<DiffeVoltage, Long> {
	@Modifying
	@Transactional
	@Query("delete DiffeVoltage where id= ?1")
	public int deleteById(String id);
	
	
	public DiffeVoltage findById(String id);
	@Query("from DiffeVoltage where seriesText = ?1 and brand= ?2")
	List<DiffeVoltage> findBySeriesTextAndBrandId(String seriesText,String brandId);
}
