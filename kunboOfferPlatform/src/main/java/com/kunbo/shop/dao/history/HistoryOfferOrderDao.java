package com.kunbo.shop.dao.history;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.history.HistoryOfferOrder;

public interface HistoryOfferOrderDao extends JpaRepository<HistoryOfferOrder, Long> {
	List<HistoryOfferOrder> findByCustomerId(String customerId);
	
	List<HistoryOfferOrder> findByQuotePeopleId(String quotePeopleId);
	
	@Modifying
	@Transactional
	@Query("delete HistoryOfferOrder where id= ?1")
	public int deleteById(String id);
	
	HistoryOfferOrder findById(String id);

}
