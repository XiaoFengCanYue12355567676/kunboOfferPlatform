package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MoistTemperature;
import com.kunbo.shop.entity.motor.MotorHeater;

public interface MoistTemperatureDao extends JpaRepository<MoistTemperature, Long> {
	
	@Modifying
	@Transactional
	@Query("delete MoistTemperature where id= ?1")
	public int deleteById(String id);
	
	
	public MoistTemperature findById(String id);
	@Query("from MoistTemperature where seriesText = ?1 and brandId= ?2")
	List<MoistTemperature> findBySeriesTextAndBrandId(String seriesText,String brandId);
}
