package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorAntiExplosion;
import com.kunbo.shop.entity.motor.MotorInstallation;

public interface MotorAntiExplosionDao extends JpaRepository<MotorAntiExplosion, Long> {
	@Modifying
	@Transactional
	@Query("delete MotorAntiExplosion where id= ?1")
	public int deleteById(String id);
	
	
	public MotorAntiExplosion findById(String id);/**
	 * 防爆
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorAntiExplosion> findByBrandId(String brandId);
}
