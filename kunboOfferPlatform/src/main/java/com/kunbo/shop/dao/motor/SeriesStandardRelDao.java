package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.SeriesStandardRel;

public interface SeriesStandardRelDao extends JpaRepository<SeriesStandardRel, Long> {

	
	@Modifying
	@Transactional
	@Query("delete SeriesStandardRel where id= ?1")
	public int deleteById(String id);
	
	
	public SeriesStandardRel findById(String id);
	
	//List<SeriesStandardRel> findBySeriesText(String seriesText);
	@Query("from SeriesStandardRel where standardText = ?1 and brand= ?2")
	List<SeriesStandardRel> findByStandardTextAndBrandId(String standardText,String brandId);
	@Query("from SeriesStandardRel where seriesText = ?1 and brand= ?2")
	List<SeriesStandardRel> findBySeriesTextAndBrandId(String seriesText,String brandId);
	
}
