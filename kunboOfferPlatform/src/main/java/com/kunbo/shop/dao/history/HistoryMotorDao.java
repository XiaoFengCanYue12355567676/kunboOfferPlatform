package com.kunbo.shop.dao.history;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kunbo.shop.entity.history.HistoryMotor;

public interface HistoryMotorDao extends JpaRepository<HistoryMotor, Long> {
	HistoryMotor findByHistorypProductId(String historypProductId);
}
