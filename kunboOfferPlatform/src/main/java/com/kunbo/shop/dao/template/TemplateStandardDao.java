package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateStandard;

public interface TemplateStandardDao extends JpaRepository<TemplateStandard, Long> {
	public TemplateStandard findById(String id);
	public TemplateStandard findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateStandard where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateStandard where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
}
