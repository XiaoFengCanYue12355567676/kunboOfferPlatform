package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorAntiExplosion;
import com.kunbo.shop.entity.motor.MotorBearing;

public interface MotorBearingDao extends JpaRepository<MotorBearing, Long> {

	@Modifying
	@Transactional
	@Query("delete MotorBearing where id= ?1")
	public int deleteById(String id);
	
	
	public MotorBearing findById(String id);
	/**
	 * 轴承
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorBearing> findByBrandId(String brandId);
}
