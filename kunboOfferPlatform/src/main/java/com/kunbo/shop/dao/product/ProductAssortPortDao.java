package com.kunbo.shop.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.Product.ProductAssortPort;
import com.kunbo.shop.entity.Product.ProductMaterial;

public interface ProductAssortPortDao extends JpaRepository<ProductAssortPort, Long> {
	public ProductAssortPort findById(String id);
	public ProductAssortPort findByProductInfoId(String productInfoId);
	

	
	@Modifying
	@Transactional
	@Query("delete ProductAssortPort where productId= ?1")
	public int deleteByProductId(String id);
	
	public List<ProductAssortPort> findByTemplateId(String templateId);
	
	@Modifying
	@Transactional
	@Query("delete ProductAssortPort where templateId= ?1")
	public int deleteByTemplateId(String templateId);
}
