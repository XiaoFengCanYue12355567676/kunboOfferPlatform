package com.kunbo.shop.dao.history;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kunbo.shop.entity.history.HistoryProductParams;

public interface HistoryProductParamsDao extends JpaRepository<HistoryProductParams, Long> {
	List<HistoryProductParams> findByHistorypProductId(String historypProductId);
}
