package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateHalfProduct;

public interface TemplateHalfProductDao extends JpaRepository<TemplateHalfProduct, Long> {
	public TemplateHalfProduct findById(String id);
	public TemplateHalfProduct findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateHalfProduct where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateHalfProduct where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
}
