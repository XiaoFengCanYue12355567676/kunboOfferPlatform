package com.kunbo.shop.dao.template;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateParameter;

public interface TemplateParameterDao extends JpaRepository<TemplateParameter, Long> {
	public TemplateParameter findById(String id);
	public TemplateParameter findBytemplateInfoId(String templateInfoId);
	
	@Modifying
	@Transactional
	@Query("delete TemplateParameter where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateParameter where templateInfoId= ?1")
	public int deleteByTemplateId(String templateInfoId);
	
}
