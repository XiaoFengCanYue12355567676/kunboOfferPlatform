package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorPadding;
import com.kunbo.shop.entity.motor.MotorShaft;

public interface MotorShaftDao extends JpaRepository<MotorShaft, Long> {

	@Modifying
	@Transactional
	@Query("delete MotorShaft where id= ?1")
	public int deleteById(String id);
	
	
	public MotorShaft findById(String id);
	/**
	 * 电机轴
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorShaft> findByBrandId(String brandId);
}
