package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorInstallation;

public interface MotorInstallationDao extends JpaRepository<MotorInstallation, Long> {
	
	@Modifying
	@Transactional
	@Query("delete MotorInstallation where id= ?1")
	public int deleteById(String id);
	
	
	public MotorInstallation findById(String id);
	
	/**
	 * 安装方式
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorInstallation> findByBrandId(String brandId);
}
