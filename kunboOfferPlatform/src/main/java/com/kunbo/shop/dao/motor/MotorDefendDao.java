package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.MotorBearing;
import com.kunbo.shop.entity.motor.MotorDefend;

public interface MotorDefendDao extends JpaRepository<MotorDefend, Long> {

	@Modifying
	@Transactional
	@Query("delete MotorDefend where id= ?1")
	public int deleteById(String id);
	
	
	public MotorDefend findById(String id);
	/**
	 * 防护
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorDefend> findByBrandId(String brandId);
}
