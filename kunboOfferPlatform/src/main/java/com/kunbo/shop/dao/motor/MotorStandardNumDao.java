package com.kunbo.shop.dao.motor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.motor.DiffeVoltage;
import com.kunbo.shop.entity.motor.MotorSeries;
import com.kunbo.shop.entity.motor.MotorStandardNum;

public interface MotorStandardNumDao extends JpaRepository<MotorStandardNum, Long> {
	
	@Modifying
	@Transactional
	@Query("delete MotorStandardNum where id= ?1")
	public int deleteById(String id);
	
	
	public MotorStandardNum findById(String id);
	/**
	 * 基座号集合
	 * @param brandId 品牌ID
	 * @return
	 */
	List<MotorStandardNum> findByBrandId(String brandId);
}
