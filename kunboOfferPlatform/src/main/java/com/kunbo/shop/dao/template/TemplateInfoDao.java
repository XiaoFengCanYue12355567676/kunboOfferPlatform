package com.kunbo.shop.dao.template;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.template.TemplateInfo;

public interface TemplateInfoDao extends JpaRepository<TemplateInfo, Long> {
	
	public List<TemplateInfo> findBySortId(String sortId);
	
	public TemplateInfo findById(String id);
	
	@Modifying
	@Transactional
	@Query("delete TemplateInfo where id= ?1")
	public int deleteById(String id);
	
	@Modifying
	@Transactional
	@Query("update TemplateInfo set isFilter = ?2 where id= ?1")
	public int updateFilter(String id,boolean filter);
	
	@Modifying
	@Transactional
	@Query("update TemplateInfo set isQuote = ?2 where id= ?1")
	public int updateQuote(String id,boolean quote);
	
	@Modifying
	@Transactional
	@Query("update TemplateInfo set calculateSequ = ?2 where id= ?1")
	public int updateCalculateSequ(String id,int calculateSequ);
	
	@Query(
	 " from "
	+ " TemplateInfo templateInfo "
	+ " where templateInfo.sortId= ?1"
	+ " and templateInfo.isFilter = true")
	List<TemplateInfo> getInfoForFilter(String productId);
	
	
	@Query(
			 " from "
			+ " TemplateInfo templateInfo "
			+ " where templateInfo.sortId= ?1"
			+ " and templateInfo.attriType = 5")
	List<TemplateInfo> getInfoForAssortPort(String productId);
}
