package com.kunbo.shop.dao.sort;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.sort.ProductSort;

public interface ProductSortDao extends JpaRepository<ProductSort, String> {
	
	@Modifying
	@Transactional
	@Query("delete ProductSort where id= ?1")
	public int deleteById(String id);
	
	public ProductSort findById(String id);
	
	
	@Query("from ProductSort where sort_type != 2")
	public List<ProductSort> findNoProduct();
	
	/** 
	* @Description: 根据产品名称查找产品 
	* @Title: findProductByText 
	* @param text
	* @return List<ProductSort>
	* @author qin
	* @date 2018年12月13日下午5:55:45
	*/ 
	@Query("from ProductSort where sort_type = 2 and productNumber = ?1")
	public List<ProductSort> findProductByProductNumber(String text);
	
	
	public List<ProductSort> findByPid(String pid);
}
