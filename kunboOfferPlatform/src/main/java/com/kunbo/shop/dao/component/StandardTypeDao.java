package com.kunbo.shop.dao.component;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.shop.entity.component.StandardType;

public interface StandardTypeDao extends JpaRepository<StandardType, Long> {

	
	@Modifying
	@Transactional
	@Query("delete StandardType where id= ?1")
	public int deleteById(String id);
	
	
	public StandardType findById(String id);
	
}
