package com.kunbo.encode.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="db_offer_encode_encodeTemplate")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class EncodeTemplate {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String templateName;//授码名称
	
	String noLevel1Conn;//编号1连接符
	
	String noLevel1Name;//编号1连接符
	
	String noLevel2Conn;//编号2连接符
	
	String noLevel2Name;//编号2连接符
	
	Date date;//创建日期
}
