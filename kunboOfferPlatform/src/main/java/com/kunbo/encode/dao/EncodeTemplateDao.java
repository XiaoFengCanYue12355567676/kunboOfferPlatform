package com.kunbo.encode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kunbo.encode.entity.EncodeTemplate;

public interface EncodeTemplateDao extends JpaRepository<EncodeTemplate, Long> {
	EncodeTemplate findByTemplateName(String templateName);
	
	
}
