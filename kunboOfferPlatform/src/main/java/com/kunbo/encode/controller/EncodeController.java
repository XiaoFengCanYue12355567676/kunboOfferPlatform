package com.kunbo.encode.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.balance.dao.BalanceBladeDao;
import com.kunbo.balance.entity.BalanceBlade;
import com.kunbo.encode.dao.EncodeDao;
import com.kunbo.encode.dao.EncodeTemplateDao;
import com.kunbo.encode.entity.Encode;
import com.kunbo.encode.entity.EncodeTemplate;
import com.kunbo.encode.service.EncodeService;
import com.kunbo.shop.entity.motor.MotorPadding;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/encode/no")
public class EncodeController {
	
	@Autowired EncodeService encodeService;
	@Autowired EncodeTemplateDao encodeTemplateDao;
	
	/** 
	* @Description: 根据授码模板名称获取新的编码 
	* @Title: getNo 
	* @param templateName
	* @return ReturnBean
	* @author qin
	* @date 2019年1月8日下午1:55:40
	*/ 
	@RequestMapping(value="/getNo.do")  
    public @ResponseBody Encode getNo(@RequestParam("templateName") String templateName){

		return encodeService.getCode(templateName);
	}
	
	@RequestMapping(value="/template/insert.do")  
    public @ResponseBody ReturnBean templateInsert(@RequestParam("data") String _date){
		Gson reGson = new Gson();
		String data = _date.replaceAll("\"\"", "null");
		EncodeTemplate et = reGson.fromJson(data, new TypeToken<EncodeTemplate>(){}.getType());
		encodeTemplateDao.save(et);		
		return new ReturnBean(true,"");
	}

}
