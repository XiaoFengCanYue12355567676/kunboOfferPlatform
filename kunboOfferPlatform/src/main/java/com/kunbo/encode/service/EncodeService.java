package com.kunbo.encode.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kunbo.encode.entity.Encode;

@Service
public interface EncodeService {
	/** 
	* @Description: 获取今日类型编码 
	* @Title: getEncodeByTemplateIdAndDate 
	* @param templateId
	* @param Date
	* @return List<Encode>
	* @author qin
	* @date 2019年1月8日下午2:09:58
	*/ 
	List<Encode> getLastEncodeByTemplateIdAndDate(String templateId);
	
	Encode getCode(String templateName);
}
