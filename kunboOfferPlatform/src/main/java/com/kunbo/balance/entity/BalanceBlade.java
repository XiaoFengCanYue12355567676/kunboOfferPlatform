package com.kunbo.balance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="db_offer_balance_blade")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class BalanceBlade {
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String orderId;
	
	String bladeCode;//叶片编号
	
	double apexWight;//叶尖重量
	double rootWight;//页根重量
	double bladeTorque;//叶片力矩
	double torqueDeviation;//力矩差
	double calcApexMaxBallast;//计算叶尖最大配重/g
	double calcApexMinBallast;//最小配重重量/g
	double predictMaxBladeTorque;//预计最大配重后叶片力矩M/g.m
	double predictMinBladeTorque;//预计最大配重后叶片力矩M/g.m
	
	double ballastedApexWight;//配重后叶尖重量 G1/kg
	double ballastedRootWight;//配重后叶根重量 G2/kg
	double ballastedBladeTorque;//配重后叶片力矩 M/g.m
	double actualBallastWight;//叶尖实际配重量/g
	double ballastedtorqueDeviation;//配重后力矩差M/g.m
	double ballastedBladeWight;//配重后叶片重量/kg
	double ballastedCoreDistance;//配重后叶片重心与回转中心距离/m
	
	boolean qualified;//合格
	double recommendedWeight;//如果不合格计算继续增加配重量/g
	
}
