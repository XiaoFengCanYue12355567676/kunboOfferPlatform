package com.kunbo.balance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="db_offer_balance_order")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class BalanceOrder {
	
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	String id;//主键uuid
	
	String salesOrder;//销售订单号
	String productionOrder;//生产订单号
	String bladeCode;//叶片部编码
	double apexDistance;//叶尖支撑点到旋转中心距离L1
	double rootDistance;//叶根支撑点到旋转中心距离L2
	double bobDistance;//配重点与叶尖距离
	double multiDeviation;//标准互差
	double maxDeviation;//最大误差
	int bladeNum;//叶片数量	
	int proupNum;//组数
	int qualifyNum;//合格数量

}
