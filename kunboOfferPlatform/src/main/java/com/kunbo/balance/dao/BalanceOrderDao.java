package com.kunbo.balance.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.balance.entity.BalanceOrder;

public interface BalanceOrderDao extends JpaRepository<BalanceOrder, Long> {
	
	@Modifying
	@Transactional
	@Query("delete BalanceOrder where id= ?1")
	public int deleteById(String id);
	
	public BalanceOrder findById(String id);
}
