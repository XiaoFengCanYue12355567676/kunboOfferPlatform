package com.kunbo.balance.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.balance.entity.BalanceBlade;

public interface BalanceBladeDao extends JpaRepository<BalanceBlade, Long> {

	
	@Modifying
	@Transactional
	@Query("delete BalanceBlade where id= ?1")
	public int deleteById(String id);
	
	public BalanceBlade findById(String id);
	
	List<BalanceBlade> findByOrderId(String orderId);
}
