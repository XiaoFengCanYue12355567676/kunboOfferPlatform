package com.kunbo.balance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.balance.dao.BalanceBladeDao;
import com.kunbo.balance.entity.BalanceBlade;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/balance/blade")
public class BalanceBladeController {
	
	@Autowired BalanceBladeDao balanceBladeDao;
	
	@RequestMapping(value="/save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		BalanceBlade balanceBlade = reGson.fromJson(data, new TypeToken<BalanceBlade>(){}.getType());
	
		try
		{
			balanceBladeDao.save(balanceBlade);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/list.do")  
    public @ResponseBody List<BalanceBlade> list(){
		return balanceBladeDao.findAll();
	}
	
	@RequestMapping(value="/findByOrderId.do")  
    public @ResponseBody List<BalanceBlade> findByOrderId(@RequestParam("orderId") String orderId){
		return balanceBladeDao.findByOrderId(orderId);
	}
	
	@RequestMapping(value="/deleteById.do")  
    public @ResponseBody ReturnBean deleteById(@RequestParam("id") String id){
		balanceBladeDao.deleteById(id);
		return new ReturnBean(true,"");
	}
	
	@RequestMapping(value="/findById.do")  
    public @ResponseBody BalanceBlade findById(@RequestParam("id") String id){
		return balanceBladeDao.findById(id);
	}
}
