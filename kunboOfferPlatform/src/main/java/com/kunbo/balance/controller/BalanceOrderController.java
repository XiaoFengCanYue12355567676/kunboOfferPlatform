package com.kunbo.balance.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kunbo.balance.dao.BalanceOrderDao;
import com.kunbo.balance.entity.BalanceOrder;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/balance/order")
public class BalanceOrderController {

	
	@Autowired BalanceOrderDao balanceOrderDao;
	
	@RequestMapping(value="/save.do")  
    public @ResponseBody ReturnBean save(HttpServletRequest request,HttpServletResponse response){
		Gson reGson = new Gson();
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		BalanceOrder balanceOrder = reGson.fromJson(data, new TypeToken<BalanceOrder>(){}.getType());
	
		try
		{
			balanceOrderDao.save(balanceOrder);
			return new ReturnBean(true,"");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ReturnBean(false,e.getMessage());
		}
	}
	@RequestMapping(value="/list.do")  
    public @ResponseBody List<BalanceOrder> list(){
		return balanceOrderDao.findAll();
	}
	
	@RequestMapping(value="/deleteById.do")  
    public @ResponseBody ReturnBean deleteById(@RequestParam("id") String id){
		balanceOrderDao.deleteById(id);
		return new ReturnBean(true,"");
	}
	
	@RequestMapping(value="/findById.do")  
    public @ResponseBody BalanceOrder findById(@RequestParam("id") String id){
		return balanceOrderDao.findById(id);
	}
	
	
	
}
