package com.kunbo.offer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.FanOfferCollect;
import com.kunbo.offer.service.FanOfferCollectService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

/**
 * 选型收藏功能
 * 
 * @author xuejun
 *
 */
@RestController
@RequestMapping("/fanOfferCollect/")
public class FanOfferCollectController {
	@Autowired
	private FanOfferCollectService fanOfferCollectService;

	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		FanOfferCollect fanOfferCollect = (FanOfferCollect) Json.toObject(
				FanOfferCollect.class, data);
		fanOfferCollectService.save(fanOfferCollect);
		return new ReturnBean(true, "操作成功");

	}

	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String id = request.getParameter("id");
		fanOfferCollectService.delete(id);
		return new ReturnBean(true, "操作成功");

	}

	@RequestMapping("findByUserId")
	public List<FanOfferCollect> findByUserId(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String userId = request.getParameter("userId");
		if (StringUtils.isBlank(userId)) {
			return null;
		}
		return fanOfferCollectService.findByUserId(Integer.parseInt(userId));
	}

}
