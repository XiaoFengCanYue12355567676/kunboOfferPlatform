package com.kunbo.offer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.MachiningItem;
import com.kunbo.offer.entity.ManHour;
import com.kunbo.offer.service.MachiningItemService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/machiningItem/")
public class MachiningItemController {
	
	@Autowired
	private MachiningItemService machiningItemService;
	
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MachiningItem machiningItem = (MachiningItem) Json.toObject(MachiningItem.class, data);
		machiningItemService.save(machiningItem);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("update")
	public ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		MachiningItem machiningItem = (MachiningItem) Json.toObject(MachiningItem.class, data);
		machiningItemService.save(machiningItem);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		machiningItemService.delete(id);
		return new ReturnBean(true,"操作成功");
	}
	
	@RequestMapping("findById")
	public MachiningItem findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		return machiningItemService.findById(id);
	}
	
	@RequestMapping("list")
	public List<MachiningItem> getList() {
		return machiningItemService.findAll();
	}
	
	@RequestMapping("getTypeList")
	public List<MachiningItem> getTypeList() {
		return machiningItemService.getTypeList();
	}
	
	@RequestMapping("findListByType")
	public List<MachiningItem> findListByType(@RequestParam("type") String type) {
		return machiningItemService.findListByType(type);
	}
	
	

}
