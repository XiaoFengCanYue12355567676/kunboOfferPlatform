package com.kunbo.offer.controller;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kunbo.offer.entity.CapabilityParam;
import com.kunbo.offer.service.CapabilityParamService;
import com.shuohe.service.util.sql.EasyuiServiceImpl;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/capabilityParam/")
public class CapabilityParamController {
	@Autowired
	private CapabilityParamService capabilityParamService;
	
	@Resource
    private EasyuiServiceImpl easyUiDto;
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,
			HttpSession session,String fanId,String fanNumber) throws Exception {
		//参数传递，传风机基础数据和分机配件价格列表数据
		Gson reGson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String dataStr =  request.getParameter("paramRows").replaceAll("\"\"", "null");
		List<CapabilityParam> list = reGson.fromJson(dataStr, new TypeToken<List<CapabilityParam>>(){}.getType());
		capabilityParamService.save(list,fanId,fanNumber);
		return new ReturnBean(true,"操作成功");
	}
	/**
	 * 
	 * <p>Title: getDataByUuid</p>
	 * <p>Description:根据风机编号获取风机其他参数信息 </p>
	 * @param request
	 * @param response
	 * @return
	 * @throws SQLException
	 */
	@RequestMapping(value="getDataByFanId")  
    public @ResponseBody EasyUiDataGrid getDataByFanId(HttpServletRequest request,HttpServletResponse response) throws SQLException{  
		
		 
		String fanId = request.getParameter("fanId"); 
		String tableName = request.getParameter("tableName");
		  
		String sql = "select  *  from   " + tableName+"  where fanId='"+fanId+"'";
		
		EasyUiDataGrid l = easyUiDto.getEasyUiDataGrid(sql,sql);
    	return l;
	}

}
