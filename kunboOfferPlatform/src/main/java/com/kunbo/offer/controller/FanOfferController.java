package com.kunbo.offer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.entity.FanOffer;
import com.kunbo.offer.service.FanOfferService;

@RestController
@RequestMapping("/fanOffer/")
public class FanOfferController {
	@Autowired
	private FanOfferService fanOfferService;
	@RequestMapping("quotedPrice")
	public Map<String,Object> quotedPrice(HttpServletRequest request,
			HttpServletResponse response,HttpSession session){
		Gson reGson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		//1.获取风机编号
		String id = request.getParameter("id");
		String offerList = request.getParameter("offerList");
		List<FanOffer> list = reGson.fromJson(offerList, new TypeToken<List<FanOffer>>(){}.getType());
		return fanOfferService.offer(id,list);
		
	}

}
