package com.kunbo.offer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.ExtensionModel;
import com.kunbo.offer.entity.FanModelParts;
import com.kunbo.offer.service.ExtensionModelService;
import com.kunbo.offer.service.FanModelPartsService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
/**
 * 
 * <p>Title: ExtensionModelController</p>
 * <p>Description:风机模型controller </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午9:55:44
 * @version 1.0
 */
@RestController
@RequestMapping("/extensionModel/")
public class ExtensionModelController {
	@Autowired
	private ExtensionModelService extensionModelService;
	
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ExtensionModel extensionModel = (ExtensionModel) Json.toObject(ExtensionModel.class, data);
		extensionModelService.save(extensionModel);
		return new ReturnBean(true,"操作成功");
		
	}
	
	
	@RequestMapping("update")
	public ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ExtensionModel extensionModel = (ExtensionModel) Json.toObject(ExtensionModel.class, data);
		extensionModelService.save(extensionModel);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		extensionModelService.delete(id);
		return new ReturnBean(true,"操作成功");
		
	}
	@RequestMapping("findById")
	public ExtensionModel findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		return extensionModelService.findById(id);
		
	}

}