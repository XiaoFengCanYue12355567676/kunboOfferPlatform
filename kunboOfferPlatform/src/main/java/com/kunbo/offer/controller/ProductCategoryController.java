package com.kunbo.offer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.FanBaseParameter;
import com.kunbo.offer.entity.ProductCategory;
import com.kunbo.offer.service.ProductCategoryService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
/**
 * 
 * <p>Title: ProductCategoryController</p>
 * <p>Description:产品大类controller </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午4:22:54
 * @version 1.0
 */
@RestController
@RequestMapping("/productCategory/*")
public class ProductCategoryController {
	@Autowired
	private ProductCategoryService productCategoryService;
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		String fanParameter = request.getParameter("fanParameter").replaceAll("\"\"", "null");
		ProductCategory productCategory = (ProductCategory) Json.toObject(ProductCategory.class, data);
		FanBaseParameter fanBaseParameter = (FanBaseParameter) Json.toObject(FanBaseParameter.class, fanParameter);
		productCategoryService.save(productCategory,fanBaseParameter);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("update")
	public ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ProductCategory productCategory = (ProductCategory) Json.toObject(ProductCategory.class, data);
		productCategoryService.update(productCategory);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		productCategoryService.delete(id);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("findById")
	public ProductCategory findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		return productCategoryService.findById(id);
		
	}
	/**
	 * 获取风机大类的tree
	 * <p>Title: getProductCategoryTree</p>
	 * <p>Description: </p>
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping("getProductCategoryTree")
	public List<ProductCategory> getProductCategoryTree(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		return productCategoryService.getTreeProductCategoryList();
		
	}
	/**
	 * 
	 * <p>Title: getFanParameterByPcPId</p>
	 * <p>Description:获取风机参数实体拼接称为map返回 </p>
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping("getFanParameterByPcPId")
	public Map<String,Object> getFanParameterByPcPId(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String pid = request.getParameter("pcPId");
		return productCategoryService.findByPcPid(pid);
		
	}
	/**
	 * <p>Title: getFanParameterJsonStrByPcPId</p>
	 * <p>Description: 直接获取风机参数json串</p>
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping("getFanParameterJsonStrByPcPId")
	public String getFanParameterJsonStrByPcPId(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String pid = request.getParameter("pcPId");
		return productCategoryService.getFanParameterStrByPcPid(pid);
		
	}
	
	

}
