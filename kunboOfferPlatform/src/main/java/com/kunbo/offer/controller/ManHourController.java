package com.kunbo.offer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.ManHour;
import com.kunbo.offer.service.ManHourService;
import com.kunbo.shop.service.product.ProductService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
/**
 * 
 * <p>Title: ManHourController</p>
 * <p>Description:工时controlle </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午4:23:51
 * @version 1.0
 */
@RestController
@RequestMapping("/ManHour/")
public class ManHourController {
	@Autowired
	private ManHourService manHourService;
	
	@Autowired ProductService productService;
	
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ManHour ManHour = (ManHour) Json.toObject(ManHour.class, data);
		manHourService.save(ManHour);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("update")
	@Transactional
	public ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		ManHour manHourNew = (ManHour) Json.toObject(ManHour.class, data);
		ManHour manHourOld = manHourService.findById(manHourNew.getId());
		//价格发生了变更，修改所有产品工时相关的参数
		if(manHourNew.getPrice() != manHourOld.getPrice())
		{
			productService.reCalculatorProductManHour(manHourNew);
		}
		//修改所有历史报价产品的状态为价格变更
		
		manHourService.save(manHourNew);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		manHourService.delete(id);
		return new ReturnBean(true,"操作成功");
	}
	
	@RequestMapping("findById")
	public ManHour findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		return manHourService.findById(id);
	}
	
	@RequestMapping("list")
	public List<ManHour> getList() {
		return manHourService.findAll();
	}

}
