package com.kunbo.offer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kunbo.offer.entity.DraughtFan;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.service.DraughtFanService;
import com.shuohe.util.returnBean.ReturnBean;

@RestController
@RequestMapping("/draughtFan/")
public class DraughtFanController {
	@Autowired
	private DraughtFanService draughtFanService;
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		//参数传递，传风机基础数据和分机配件价格列表数据
		Gson reGson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String fanBaseData =  request.getParameter("data").replaceAll("\"\"", "null");
		String fanPartsList =  request.getParameter("fanPartsList").replaceAll("\"\"", "null");
		DraughtFan draughtFan = reGson.fromJson(fanBaseData, new TypeToken<DraughtFan>(){}.getType());
		List<DraughtFanBasePrice> list = reGson.fromJson(fanPartsList, new TypeToken<List<DraughtFanBasePrice>>(){}.getType());
		draughtFanService.save(draughtFan, list);
		return new ReturnBean(true,"操作成功");
		
	}

}


