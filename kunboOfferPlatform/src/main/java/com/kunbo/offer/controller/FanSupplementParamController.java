package com.kunbo.offer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.kunbo.offer.entity.FanSupplementParam;
import com.kunbo.offer.service.FanSupplementParamService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;

/**
 * 
 * <p>
 * Title: FanSupplementParam
 * </p>
 * <p>
 * Description:选型补充参数
 * </p>
 * <p>
 * Company: www.kunbo.cn
 * </p>
 * 
 * @author xuej
 * @date 2019-06-25
 * @version 1.0
 */
@RestController
@RequestMapping("/FanSupplementParam/")
public class FanSupplementParamController {
	@Autowired
	private FanSupplementParamService fanSupplementParamService;

	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		FanSupplementParam fanSupplementParam = (FanSupplementParam) Json
				.toObject(FanSupplementParam.class, data);
		fanSupplementParamService.save(fanSupplementParam);
		return new ReturnBean(true, "操作成功");

	}

	@RequestMapping("update")
	@Transactional
	public ReturnBean update(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		JSONObject json = JSONObject.parseObject(data);
		String id = json.getString("id");
		if (StringUtils.isBlank(id)) {
			return new ReturnBean(false, "操作失败，没有找到主键");
		}
		FanSupplementParam fanSupplementParam = (FanSupplementParam) Json
				.toObject(FanSupplementParam.class, data);
		fanSupplementParamService.save(fanSupplementParam);
		return new ReturnBean(true, "操作成功");

	}

	@RequestMapping("findByFanId")
	public FanSupplementParam findByFanId(HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String fanId = request.getParameter("fanId");
		List<FanSupplementParam> list = fanSupplementParamService
				.findByFanId(fanId);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

}
