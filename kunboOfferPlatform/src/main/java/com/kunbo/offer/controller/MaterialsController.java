package com.kunbo.offer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kunbo.offer.entity.Materials;
import com.kunbo.offer.service.MaterialsService;
import com.shuohe.util.json.Json;
import com.shuohe.util.returnBean.ReturnBean;
/**
 * <p>Title: MaterialsController</p>
 * <p>Description:物料controller </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午4:23:27
 * @version 1.0
 */
@RestController
@RequestMapping("/materials/")
public class MaterialsController {
	@Autowired
	private MaterialsService materialsService;
	@RequestMapping("save")
	public ReturnBean save(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		Materials materials = (Materials) Json.toObject(Materials.class, data);
		materialsService.save(materials);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("update")
	public ReturnBean update(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String data = request.getParameter("data").replaceAll("\"\"", "null");
		Materials materials = (Materials) Json.toObject(Materials.class, data);
		materialsService.save(materials);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("delete")
	public ReturnBean delete(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		materialsService.delete(id);
		return new ReturnBean(true,"操作成功");
		
	}
	
	@RequestMapping("findById")
	public Materials findById(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String id = request.getParameter("id");
		return materialsService.findById(id);
		
	}

}
