package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.DraughtFanBasePrice;

public interface DraughtFanBasePriceDao extends CrudRepository<DraughtFanBasePrice, String> {
	/**
	 * 
	 * <p>Title: findBydf_pid</p>
	 * <p>Description:根据风机编号获取风机基本配件价格数据</p>
	 * @param df_pid
	 * @return
	 */
	@Query(value = "select * from shop_draught_fan_base_price where df_pid=?1", nativeQuery = true)
	public List<DraughtFanBasePrice> findByDf_pid(String df_pid);

}
