package com.kunbo.offer.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanBaseParameter;

public interface FanBaseParameterDao extends CrudRepository<FanBaseParameter, String> {
	@Query(value = "select * from shop_fan_base_parameter where pc_pid =?1", nativeQuery = true)
	FanBaseParameter findByPc_pid(String pc_pid);

}
