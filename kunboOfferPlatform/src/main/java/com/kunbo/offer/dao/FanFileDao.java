package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanFile;

public interface FanFileDao extends CrudRepository<FanFile, String> {
	@Query("from FanFile where fan_number=?1 and fileType=?2")
	List<FanFile> findListByFanNumberAndFileType(String fanNumber,
			String fileType);
}
