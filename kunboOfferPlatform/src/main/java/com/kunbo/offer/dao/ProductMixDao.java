package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.ProductMix;

public interface ProductMixDao extends CrudRepository<ProductMix, String> {
	/**
	 * 
	 * <p>Title: findByPid</p>
	 * <p>Description:根据父id获取配件列表 </p>
	 * @param pid
	 * @return
	 */
	List<ProductMix> findByPid(String pid);
	/**
	 * 
	 * <p>Title: findByModelNumber</p>
	 * <p>Description:根据模型编号获取模型配件 </p>
	 * @param modelNumber
	 * @return
	 */
	List<ProductMix> findByModelNumber(String modelNumber);

}
