package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.entity.MachiningItem;

public interface MachiningItemDao extends CrudRepository<MachiningItem, String> {

	@Query("from MachiningItem machiningItem group by machiningItem.type")
	public List<MachiningItem> getTypeList();
	
	@Query("from MachiningItem machiningItem where machiningItem.type=?1")
	public List<MachiningItem> findListByType(String type);
	
	
}
