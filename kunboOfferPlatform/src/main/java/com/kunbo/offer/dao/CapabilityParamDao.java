package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.CapabilityParam;

public interface CapabilityParamDao extends CrudRepository<CapabilityParam, String> {
	/**
	 * 
	 * <p>Title: findByFanNumber</p>
	 * <p>Description: 根据风机编号获取风机性能参数</p>
	 * @param fanNumber
	 * @return
	 */
	public List<CapabilityParam> findByFanNumber(String fanNumber);

}
