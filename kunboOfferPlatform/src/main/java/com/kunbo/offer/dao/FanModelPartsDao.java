package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanModelParts;

public interface FanModelPartsDao extends CrudRepository<FanModelParts, String> {

}
