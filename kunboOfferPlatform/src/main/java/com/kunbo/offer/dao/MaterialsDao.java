package com.kunbo.offer.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kunbo.offer.entity.Materials;

public interface MaterialsDao extends JpaRepository<Materials, Long> {
	Materials findById(String id);
}
