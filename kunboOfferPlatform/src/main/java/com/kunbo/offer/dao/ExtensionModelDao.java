package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.ExtensionModel;

public interface ExtensionModelDao extends CrudRepository<ExtensionModel, String> {

}
