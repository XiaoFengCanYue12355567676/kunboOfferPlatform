package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanOfferCollect;

public interface FanOfferCollectDao extends
		CrudRepository<FanOfferCollect, String> {
	/**
	 * 
	 * @param userId
	 * @return
	 */
	@Query("from FanOfferCollect where userId =?1")
	public List<FanOfferCollect> findByUserId(Integer userId);
}
