package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.DraughtFan;

public interface DraughtFanDao extends CrudRepository<DraughtFan, String> {
	
//	@Query("from User u")
//	Page<User> findAllPage(Pageable pageable);
//	
//	//展示带有条件的分页查询
//	@Query(value = "from User u where u.email like %:emailLike%")
//	Page<User> findByEmailLike(Pageable pageable, @Param("emailLike")String emailLike);

}
