package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanOffer;

public interface FanOfferDao extends CrudRepository<FanOffer, String> {

}
