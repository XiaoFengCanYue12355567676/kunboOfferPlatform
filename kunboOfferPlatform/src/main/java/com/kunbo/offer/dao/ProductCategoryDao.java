package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.ProductCategory;

public interface ProductCategoryDao extends CrudRepository<ProductCategory, String> {

}
