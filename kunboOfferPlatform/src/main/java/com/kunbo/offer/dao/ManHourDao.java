package com.kunbo.offer.dao;

import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.ManHour;

public interface ManHourDao extends CrudRepository<ManHour, String> {

}
