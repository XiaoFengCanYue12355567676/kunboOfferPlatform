package com.kunbo.offer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.kunbo.offer.entity.FanSupplementParam;

public interface FanSupplementParamDao extends
		CrudRepository<FanSupplementParam, String> {
	/**
	 * 
	 * @param fanId
	 * @return
	 */
	@Query("from FanSupplementParam where fanId =?1")
	public List<FanSupplementParam> findByFanId(String fanId);
}
