package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name="shop_fan_file")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class FanFile implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String fan_number;//风机编码
	private String fileName;//文件名称
	private String fileType;//文件类型（空气略图、安装简图、产品介绍、其他信息）
	private String fileSuffix;//文件后缀
	private String filePath;//文件路径
	private String fileRealPath;//文件真实路径
	@CreatedDate
    private Date uploadTime;//创建时间

	

}
