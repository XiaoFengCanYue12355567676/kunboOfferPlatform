package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 
 * <p>Title: MachiningItem</p>
 * <p>Description:风机加工件实体类 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月12日下午3:11:25
 * @version 1.0
 */
@Entity
@Table(name="shop_machining_item")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class MachiningItem implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	/**
	 * 加工件类型，下拉选（软连接、橡胶垫、
	 * 填料函、法兰、接线盒等）
	 */
	private String type;
	private String supplier;//供应商
	private String stockNumber;//存货编码
	private String stockName;//存货名称
	private String specificationsAndModels;//规格型号
	private String mainMeasurement;//主计量(默认个)
	private Double unitPrice;//单价
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
