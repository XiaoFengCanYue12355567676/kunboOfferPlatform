package com.kunbo.offer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 
 * <p>Title: OfferGeneralView</p>
 * <p>Description:报价概览实体类 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日上午10:17:53
 * @version 1.0
 */
@Entity
@Table(name="shop_offer_general_view")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class OfferGeneralView implements Serializable{
	//周期，数量，询价人，询价单位，询价时间，报价人，报价时间
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String inquiryPerson;//询价人
	private String contactNumber;//联系电话
	private String inquiryUnit;//询价单位
	private String fanType;//询价方向（分机的类型）
	private Integer quantity;//数量
	private String period;//要货周期
	private String offerePerson;//报价人
	private String fanNumber;//分机编号
	private BigDecimal grossPrice;//风机总报价
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间
	
	

}
