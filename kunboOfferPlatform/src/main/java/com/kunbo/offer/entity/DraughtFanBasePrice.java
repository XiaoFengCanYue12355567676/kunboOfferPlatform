package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 
 * <p>Title: DraughtFanBasePrice</p>
 * <p>Description:风机基础价格实体类,该表相当于风机的字表，
 * 主要用于记录风机配件的详细价格信息，用于后期的计算 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月4日下午6:19:41
 * @version 1.0
 */
@Entity
@Table(name="shop_draught_fan_base_price")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class DraughtFanBasePrice implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String em_pid;//分机模型id
	private String df_pid;//风机id
	private String partsName;//配件名称
	private String partsUnit;//配件单位
	private String partsType;//配件类型
	private double partsValue;//配件值
	private double standardPrice;//配件价格
//	private String m_pid;//材料id
//	private String h_pid;//工时id
	/**
	 * nsp为nonstandard part的缩写，为非标准件id，
	 * 当非标准件为材料是，此处关联的是原材料的id，
	 * 当标准件是工时时，此处关联的是工时的id
	 */
	private String nsp_pd;//非标准件id（nonstandard part 缩写为 nsp ）
	private String fmp_pid;//模型配件父id
	private String nsp_name;//非标准件名称
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
