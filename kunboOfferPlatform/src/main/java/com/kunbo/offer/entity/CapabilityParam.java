package com.kunbo.offer.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 风机性能参数实体
 * <p>Title: CapabilityParam</p>
 * <p>Description: </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月28日下午10:19:39
 * @version 1.0
 */
@Entity
@Table(name="shop_capability_param")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class CapabilityParam implements Comparable<CapabilityParam>{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String fanNumber;//风机编号
	private Double flux;//流量
	private Double totalHead;//全压
	private Double staticPressure;//静压
	private Integer speed;//转速
	private Double internalPower;//输出功率(轴功率没有用过，先当轴功率使用)
	private Double tProductiveness;//全压效率
	private Double sProductiveness;//静压效率
	private Double noise;//噪声
	private Double soundLevel;//比A声级
	private Double fluxRatio;//流量系数
	private Double totalRatio;//全压系数
	private Double staticRatio;//静压系数
	private Double powerRatio;//功率系数
	@CreatedDate
	private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间
    //添加一些其他的备用参数
    private Double lsaCountNoise;//计算噪声，进出口声压级
    private Integer lasLessThan24;//小于24
    private Double noiseLwPower;//噪声功率
    private Double rateCountNoise;//频率计算噪声
    private Double noise63;//63Hz噪声  PWL
    private Double noise125;//125Hz噪声
    private Double noise250;//250Hz噪声
    private Double noise500;//500Hz噪声
    private Double noise1KHz;//1KHz噪声
    private Double noise2KHz;//2KHz噪声
    private Double noise4KHz;//4KHz噪声
    private Double noise8KHz;//8KHz噪声
    private Double shaftPower;//轴功率
    private Double ductDiameter;//风筒直径
    private Double powerOfMotor;//电机功率
    private Double fullSpeed;//全压比转速
    private Double staticSpeed;//静夺比转速
    @Column(length = 32)
    private String motorType;//电机型号
    private Integer weight;
    @Column(length = 32)
    private String F1;
    @Column(length = 32)
    private String F2;
    @Column(length = 32)
    private String F3;
    @Column(length = 32)
    private String F4;
    @Column(length = 32)
    private String F5;
    @Column(length = 32)
    private String F6;
    @Column(length = 32)
    private String F7;
    @Column(length = 32)
    private String F8;
    @Column(length = 32)
    private String F9;
    @Column(length = 32)
    private String F10;
    @Column(length = 32)
    private String F11;
    @Column(length = 32)
    private String F12;
    @Column(length = 32)
    private String F13;
    @Column(length = 32)
    private String F14;
    @Column(length = 32)
    private String F15;
    @Column(length = 32)
    private String F16;
    @Column(length = 32)
    private String F17;
    @Column(length = 32)
    private String F18;
    @Column(length = 32)
    private String F19;
    @Column(length = 32)
    private String F20;
    @Column(length = 32)
    private String F21;
    @Column(length = 32)
    private String F22;
    @Column(length = 32)
    private String F23;
    @Column(length = 32)
    private String F24;
    @Column(length = 32)
    private String F25;
    @Column(length = 32)
    private String F26;
    @Column(length = 32)
    private String F27;
    @Column(length = 32)
    private String F28;
    @Column(length = 32)
    private String F29;
    @Column(length = 32)
    private String F30;
    @Column(length = 32)
    private String F31;
    @Column(length = 32)
    private String F32;
    @Column(length = 32)
    private String F33;
    @Column(length = 32)
    private String F34;
    @Column(length = 32)
    private String F35;
    @Column(length = 32)
    private String F36;
    @Column(length = 32)
    private String F37;
    @Column(length = 32)
    private String F38;
    @Column(length = 32)
    private String F39;
    @Column(length = 32)
    private String F40;
	@Override
	public int compareTo(CapabilityParam t) {
		// TODO Auto-generated method stub
		return (int) (this.flux-t.getFlux());
	}

}
