package com.kunbo.offer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import lombok.Data;

/**
 * 
 * <p>Title: DraughtFan</p>
 * <p>Description:风机实体类 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月4日下午5:16:45
 * @version 1.0
 */
@Entity
@Table(name="shop_draught_fan")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class DraughtFan implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String pc_pid;//风机大类id
	private String em_pid;//风机模型id
	private String number;//风机编号
	private String name;//风机名称
	private String specificationsAndModels;//规格型号
	private BigDecimal fanReferenceTotalPrice;//风机参考总价
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
