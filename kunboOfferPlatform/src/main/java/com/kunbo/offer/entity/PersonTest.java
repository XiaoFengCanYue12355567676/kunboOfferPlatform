package com.kunbo.offer.entity;

import lombok.Data;

@Data
public class PersonTest {
	private String  name;
	private int age;
	private String address;

}
