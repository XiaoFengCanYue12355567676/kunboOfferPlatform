package com.kunbo.offer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 
 * <p>Title: FanBaseParameter</p>
 * <p>Description:风机基础参数表 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月15日上午11:12:27
 * @version 1.0
 */
@Entity
@Table(name="shop_fan_base_parameter")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class FanBaseParameter implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String pc_pid;//风机分类id
	private String number;//风机编号
	private Integer model;//风机型号
	private double boreSize;//内径
	private double externalDiameter;//外径
	private double airDuctThickness;//风筒板厚
	private double airDuctHeight;//风筒高度
	private Integer bladeQuantity;//叶片数量
	private double bladeUnitPrice;//叶片单价
	private Integer hub;//轮毂
	private Integer axleSleeve;//轴套
	private double normalPrice;//叶片标价
	private String fanParameterJsonStr;//风机参数接送串，用来描述风机参数信息
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
