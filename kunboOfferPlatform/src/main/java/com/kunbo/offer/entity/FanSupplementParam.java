package com.kunbo.offer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * 风机造型补充参数 Title: FanSupplementParam Description: Company: www.kunbo.cn
 * 
 * @author xuej
 * @date 2019年06月25日
 * @version 1.0
 */
@Entity
@Table(name = "shop_fan_supplement_param")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class FanSupplementParam implements Comparable<FanSupplementParam>{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
	@Column(length = 32)
	private String id;
	@Column(length = 32)
	private String fanId; // 风机主键
	@Column(length = 50)
	private String j_fjdl; // 风机大类
	@Column(length = 50)
	private String j_cpxh; // 产品类型
	@Column(length = 50)
	private String j_cdfs; // 传动方式
	@Column(length = 50)
	private String j_yps; // 叶片数
	@Column(length = 50)
	private String j_jd; // 角度
	@Column(length = 50)
	private String j_csbg; // 测试报告
	@Column(length = 50)
	private String j_zjzl; // 整机重量
	@Column(length = 50)
	private String j_wxth; // 外形图号
	@Column(length = 50)
	private String d_djxh; // 电机型号
	@Column(length = 50)
	private String d_djcj; // 电机厂家
	@Column(length = 50)
	private String d_djazxs; // 电机安装型式
	@Column(length = 50)
	private String d_sfsfbdj; // 是否是防爆电机
	@Column(length = 50)
	private String d_fbdj; // 防爆等级
	@Column(length = 50)
	private String d_edgl; // 额定功率
	@Column(length = 50)
	private String d_edzs; // 额定转速
	@Column(length = 50)
	private String d_eddy; // 额定电压
	@Column(length = 50)
	private String d_eddl; // 额定电流
	@Column(length = 50)
	private String d_dypl; // 电压频率
	@Column(length = 50)
	private String d_glys; // 功率因数
	@Column(length = 50)
	private String d_fhdj; // 防护等级
	@Column(length = 50)
	private String d_jydj; // 绝缘等级
	@Column(length = 50)
	private String d_jrd; // 加热带
	@Column(length = 50)
	private String d_dlcd; // 电缆长度
	@Column(length = 50)
	private String d_zdwd; // 最低使用温度
	@Column(length = 50)
	private String d_zgwd; // 最高使用温度
	@Column(length = 50)
	private String d_yxzdsysd; // 允许最大使用湿度
	@Column(length = 50)
	private String d_yxzdsyhb; // 允许最大使用海拔
	@Column(length = 50)
	private String d_qzcxh; // 前轴承型号
	@Column(length = 50)
	private String d_hzcxh; // 后轴承型号
	@Column(length = 50)
	private String d_djnxdj; // 电机能效等级
	@Column(length = 50)
	private String d_djkdcz; // 电机壳体材质
	@Column(length = 50)
	private String d_djzl; // 电机重量
	@Column(length = 50)
	private String c_jkcz; // 机壳材质
	@Column(length = 50)
	private String c_ylcz; // 叶轮材质
	@Column(length = 50)
	private String c_jfkcz; // 进风口材质
	@Column(length = 50)
	private String c_fhwcz; // 防护网材质
	@Column(length = 50)
	private String c_bzj; // 标准件
	@Column(length = 50)
	private String z_qlfx; // 气流方向
	@Column(length = 50)
	private String z_tthd; // 筒体厚度
	@Column(length = 50)
	private String z_flhd; // 法兰厚度
	@Column(length = 50)
	private String z_ftlx; // 风筒类型
	@Column(length = 50)
	private String z_sffbfj; // 是否防爆风机
	@Column(length = 50)
	private String z_fbwjjxhcj;// 防爆外接接线盒厂家
	@Column(length = 50)
	private String z_qfhw; // 前防护网
	@Column(length = 50)
	private String z_qfhwjj; // 前防护网间距
	@Column(length = 50)
	private String z_hfhw; // 后防护网
	@Column(length = 50)
	private String z_hfhwjj; // 后防护网间距
	@Column(length = 50)
	private String z_ftnj; // 风筒内径
	@Column(length = 50)
	private String z_fl1wj; // 法兰1外径
	@Column(length = 50)
	private String z_fl1lskszyzj; // 法兰1螺栓孔所在圆直径
	@Column(length = 50)
	private String z_fl1lskzj; // 法兰1螺栓孔直径
	@Column(length = 50)
	private String z_fl1lskgs; // 法兰1螺栓孔个数
	@Column(length = 50)
	private String z_fl2wj; // 法兰2外径
	@Column(length = 50)
	private String z_fl2lskszyzj; // 法兰2螺栓孔所在圆直径
	@Column(length = 50)
	private String z_fl2lskzj; // 法兰2螺栓孔直径
	@Column(length = 50)
	private String z_fl2lskgs; // 法兰2螺栓孔个数
	@Column(length = 50)
	private String z_ftcd; // 风筒长度
	@Column(length = 50)
	private String z_zjgd; // 整机高度
	@Column(length = 50)
	private String z_jxglxh; // 接线格兰型号
	@Column(length = 50)
	private String z_jxglcz; // 接线格兰材质
	@Column(length = 50)
	private String l_ckfx; // 出口方向
	@Column(length = 50)
	private String l_cdfs; // 传动方式(不用)
	@Column(length = 50)
	private String l_k; // 宽
	@Column(length = 50)
	private String l_c; // 长
	@Column(length = 50)
	private String l_g; // 高
	@Column(length = 50)
	private String l_zxg; // 中心高
	@Column(length = 50)
	private String l_ckc; // 出口长
	@Column(length = 50)
	private String l_ckk; // 出口宽
	@Column(length = 50)
	private String l_ckazkzj; // 出口安装孔直径
	@Column(length = 50)
	private String l_ckzaksl; // 出口安装孔数量
	@Column(length = 50)
	private String l_jkzj; // 进口直径
	@Column(length = 50)
	private String l_jkflwj; // 进口法兰外径
	@Column(length = 50)
	private String l_jkflazkszyzj; // 进口法兰安装孔所在圆直径
	@Column(length = 50)
	private String l_jkflazkzj;// 进口法兰安装孔直径
	@Column(length = 50)
	private String l_jkflazksl;// 进口法兰安装孔数量
	@Column(length = 50)
	private String l_azkzj; // 安装孔直径
	@Column(length = 50)
	private String l_azksl; // 安装孔数量
	@Column(length = 50)
	private String l_jxglxh; // 接线格兰型号
	@Column(length = 50)
	private String l_jxglcz; // 接线格兰材质
	@Override
	public int compareTo(FanSupplementParam o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
