package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
/**
 * 
 * <p>Title: ExtensionModel</p>
 * <p>Description:风机模型配件表 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月4日下午4:47:46
 * @version 1.0
 */
@Entity
@Table(name="shop_fan_model_parts")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class FanModelParts implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	private String pc_pid;//风机大类id
	private String partsName;//配件名称
	private String partsUnit;//配件单位
	private String partsType;//配件类型（非标准件（价格等通过一定的计算才能得出（原材料重量，原材料加工工时）），标准件）
//	private boolean ifMatch;//是否选配
//	private boolean ifChoice;//是否可选
	private boolean ifMustChoose;
    @CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间
    private String pid;//父id

}
