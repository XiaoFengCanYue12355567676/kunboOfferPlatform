package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
/**
 * 
 * <p>Title: ProductCategory</p>
 * <p>Description: 风机大类</p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年10月24日下午2:09:06
 * @version 1.0
 */
@Entity
@Table(name="shop_product_category")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductCategory implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//分类id
    private String text;//分类名称
    private String pid;//分类父id,如果该类没有子类，则pid为null
    @Transient
    private List<ProductCategory> children = new ArrayList<ProductCategory>();
    @CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
