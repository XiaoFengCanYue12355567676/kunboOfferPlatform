package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
/**
 * 
 * <p>Title: ManHour</p>
 * <p>Description:工时大类 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月2日下午4:02:53
 * @version 1.0
 */
@Entity
@Table(name="shop_manhour")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ManHour implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//id
	private String encoded;//工时编码
    private String name;//工时名称
    private Double price;//工时单价
    @CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间
    private String pid;//父id

}
