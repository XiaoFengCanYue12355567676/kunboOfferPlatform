package com.kunbo.offer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * 风机收藏 Title: FanOfferCollect Description: Company: www.kunbo.cn
 * 
 * @author xuej
 * @date 2019年07月18日
 * @version 1.0
 */
@Entity
@Table(name = "db_offer_sort_collect")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class FanOfferCollect implements Comparable<FanOfferCollect>{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
	@Column(length = 32)
	private String id;	  // 收藏主健
	private String fanId; // 风机主键
	private Integer userId; // 用户ID
	@CreatedDate
    private Date createTime;//创建时间
	private String productNumber; // 风机编号
	private String text; // 风机名称
	private String yelunzhijing; // 叶轮直径
	private String j_yps; // 叶片数
	private String j_jd; // 角度
	private Integer isTransshape; // 变形设计0否1是
	private Double blowingRate; // 风量值
	private String state; // 风量单位类型
	private String windPressure; // 全压/静夺类型
	private Double totalHeadQuery; // 全压/静压值
	private String errorMinRange; // 风机误差设置（小）
	private String errorMaxRange; // 风机误差设置（大）
	private Integer speedQuery; // 转速
	private Double airDensityQuery; // 进口空气密度
	private String minYlzj; // 叶轮直径范围（小）
	private String maxYlzj; // 叶轮直径范围（大）
	@Override
	public int compareTo(FanOfferCollect o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
