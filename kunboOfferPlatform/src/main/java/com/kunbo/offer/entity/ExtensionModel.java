package com.kunbo.offer.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
/**
 * 
 * <p>Title: ProductCategory</p>
 * <p>Description: 风机模型id</p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年10月24日下午2:09:06
 * @version 1.0
 */
@Entity
@Table(name="shop_extension_model")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ExtensionModel implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//模型id
	private String pc_pid;//大类id
    @Column(name = "name")
    private String name;//模型名称
    @CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
