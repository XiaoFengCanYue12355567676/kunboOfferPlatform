package com.kunbo.offer.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * 
 * <p>Title: MachiningItem</p>
 * <p>Description:风机产品bom实体 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月12日下午3:11:25
 * @version 1.0
 */
@Entity
@Table(name="shop_product_mix")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@EntityListeners(AuditingEntityListener.class)
@Data
public class ProductMix implements Serializable{
	@Id
	@GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
	private String id;//主键uuid
	/**
	 * 描述产品结构的类型，有产品、半成品、原材料、配套件、协作件、易耗件,工时
	 */
	private String type;
	private String encoded;//物料编码
	private String text;//物料名称
	private String specificationsAndModels;//规格型号
	/**
	 * 单位，又称为主计量
	 * 个、只、捆、块、公斤、瓶，罐、升、小时、套、台、条等
	 */
	private String unit;
	private double dosage;//用量
	private BigDecimal price;//价格
	private String pid;//父id，关联自身的id
	private double unitPrice;//材料单价
	private String designFormulas;//计算公式
	private String jsonParameter;//参数json
	private String fanNumber;//风机编号（风机下面的所有配件中必须保存该编号，方便于查找）
	private String modelNumber;//风机模型编号（风机下面的所有配件中必须保存该编号，方便于查找）
	private String fanTypeId;//风机类型id
	private String state;//树形菜单的状态
	private boolean ifMustChoose;//是否必选
	@Transient
	private List<ProductMix> children;
	@CreatedDate
    private Date createTime;//创建时间
    @LastModifiedDate
    private Date lastModifiedTime;//修改时间

}
