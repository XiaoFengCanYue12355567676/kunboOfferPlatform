package com.kunbo.offer.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.FanOfferCollectDao;
import com.kunbo.offer.entity.FanOfferCollect;
import com.kunbo.offer.service.FanOfferCollectService;

/**
 * 选型收藏的实现
 * 
 * @author xuejun
 *
 */
@Service
@Transactional
public class FanOfferCollectServiceImpl implements FanOfferCollectService {
	@Autowired
	private FanOfferCollectDao fanOfferCollectDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void save(FanOfferCollect fanOfferCollect) {
		fanOfferCollectDao.save(fanOfferCollect);
	}

	@Override
	public void delete(String id) {
		fanOfferCollectDao.delete(id);

	}

	@Override
	public List<FanOfferCollect> findByUserId(Integer userId) {
		return fanOfferCollectDao.findByUserId(userId);
	}

}
