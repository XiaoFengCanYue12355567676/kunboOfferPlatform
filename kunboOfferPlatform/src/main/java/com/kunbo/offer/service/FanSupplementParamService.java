package com.kunbo.offer.service;

import java.sql.SQLException;
import java.util.List;

import com.kunbo.offer.entity.FanSupplementParam;

public interface FanSupplementParamService {
	void save(FanSupplementParam fanSupplementParam);

	void update(FanSupplementParam fanSupplementParam);

	List<FanSupplementParam> findByFanId(String fanId);
	/**
	 * 导入数据
	 * @param datas
	 * @throws SQLException 
	 */
	public void importExcel(String[][] datas,String typeId) throws SQLException;
}
