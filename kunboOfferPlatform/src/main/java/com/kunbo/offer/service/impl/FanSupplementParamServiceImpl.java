package com.kunbo.offer.service.impl;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.CapabilityParamDao;
import com.kunbo.offer.dao.FanSupplementParamDao;
import com.kunbo.offer.entity.CapabilityParam;
import com.kunbo.offer.entity.FanSupplementParam;
import com.kunbo.offer.service.CapabilityParamService;
import com.kunbo.offer.service.FanSupplementParamService;
import com.kunbo.shop.dao.sort.ProductSortDao;
import com.kunbo.shop.entity.sort.ProductSort;
import com.kunbo.shop.entity.sort.ProductSort.SORT_TYPE;
import com.kunbo.shop.service.sort.ProductSortService;

/**
 * 选型补充参数的实现
 * 
 * @author xuejun
 *
 */
@Service
@Transactional
public class FanSupplementParamServiceImpl implements FanSupplementParamService {
	@Autowired
	private FanSupplementParamDao fanSupplementParamDao;

	// private Fan
	// 分类+产品
	@Autowired
	private ProductSortService productSortService;
	@Autowired
	private ProductSortDao productSortDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private CapabilityParamDao capabilityParamDao;
	@Autowired
	private CapabilityParamService capabilityParamService;

	@Override
	public void save(FanSupplementParam fanSupplementParam) {
		fanSupplementParamDao.save(fanSupplementParam);

	}

	@Override
	public void update(FanSupplementParam fanSupplementParam) {
		fanSupplementParamDao.save(fanSupplementParam);

	}

	@Override
	public List<FanSupplementParam> findByFanId(String fanId) {
		return fanSupplementParamDao.findByFanId(fanId);
	}
	/**
	 * 导入选型Excel
	 * @param datas excel转换后的数组
	 * @@param typeId 分类ID
	 */
	@Override
	public void importExcel(String[][] datas, String typeId)
			throws SQLException {
		if (datas == null || datas.length == 0) {
			return;
		}
		// 设定变量和前置集合
		List<ProductSort> listProduct = productSortService.getForTree();
		boolean isHaveCode = true;// 是否有设备编号的第一条
		boolean isRepeat = false;// 是否是重复记录
		String curFanId = "";// 当前的风机ID
		String tempCode = "";//当前的风机编号
		List<CapabilityParam> listCapability = new ArrayList<CapabilityParam>();//此处是为了生成计算公式
		for (int i = 4; i < datas.length; i++) {
			if (StringUtils.isBlank(datas[i][0].toString())) {
				isHaveCode = false;
			} else {
				isHaveCode = true;
			}
			// 第一步，判断有无此产品，不做变更，直接过滤 text;//类型名称 productNumber; //产品编码
			if (isHaveCode) {
				for (ProductSort productSort : listProduct) {
					if (productSort.getText().equals(datas[i][2].trim())
							&& productSort.getProductNumber().equals(
									datas[i][3].trim())) {
						curFanId = "";
						isRepeat = true;
						break;
					} else {
						isRepeat = false;
					}
				}
			}
			// 如果是重复
			if (isRepeat)
				continue;
			// 第二步，不用考虑重复。入库操作--生成fanid
			
			if (isHaveCode) {
				//先去执行方法，去存储计算公式
				try{
					capabilityParamService.save(listCapability, curFanId, tempCode);
					listCapability.clear();
				}catch(Exception e){
					
				}
				
				isRepeat = false;
				tempCode = datas[i][3].trim();
				ProductSort entity = new ProductSort();
				entity.setProductNumber(datas[i][3].trim());
				entity.setText(datas[i][2].trim());
				entity.setSort_type(SORT_TYPE.PRODUCT);
				entity.setPid(typeId);
				if (StringUtils.isNotBlank(datas[i][35].trim())) {
					entity.setNoise63s(Double.parseDouble(datas[i][35].trim()));
					entity.setNoise63(Double.parseDouble(datas[i][35].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][36].trim())) {
					entity.setNoise125s(Double.parseDouble(datas[i][36].trim()));
					entity.setNoise125(Double.parseDouble(datas[i][36].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][37].trim())) {
					entity.setNoise250s(Double.parseDouble(datas[i][37].trim()));
					entity.setNoise250(Double.parseDouble(datas[i][37].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][38].trim())) {
					entity.setNoise500s(Double.parseDouble(datas[i][38].trim()));
					entity.setNoise500(Double.parseDouble(datas[i][38].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][39].trim())) {
					entity.setNoise1KHzs(Double.parseDouble(datas[i][39].trim()));
					entity.setNoise1KHz(Double.parseDouble(datas[i][39].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][40].trim())) {
					entity.setNoise2KHzs(Double.parseDouble(datas[i][40].trim()));
					entity.setNoise2KHz(Double.parseDouble(datas[i][40].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][41].trim())) {
					entity.setNoise4KHzs(Double.parseDouble(datas[i][41].trim()));
					entity.setNoise4KHz(Double.parseDouble(datas[i][41].trim()) + 8.0);
				}
				if (StringUtils.isNotBlank(datas[i][42].trim())) {
					entity.setNoise8KHzs(Double.parseDouble(datas[i][42].trim()));
					entity.setNoise8KHz(Double.parseDouble(datas[i][42].trim()) + 8.0);
				}
				ProductSort productSortentity = productSortDao.save(entity);
				if(productSortentity != null){
					curFanId = productSortentity.getId();
				}
				// 第二步2其它参数入库
				String uuid = UUID.randomUUID().toString();
				String sql = "insert into shop_fan_other_param (uuid,fanId,taskid,midu,zhuansu,yelunzhijing) values ('"
						+ uuid
						+ "','"
						+ curFanId
						+ "','0','"
						+ datas[i][10].trim()
						+ "','"
						+ datas[i][15].trim()
						+ "','" + datas[i][4].trim() + "')";
				Statement stmt = jdbcTemplate.getDataSource().getConnection()
						.createStatement();
				stmt.executeUpdate(sql);
				stmt.close();
				//第二步3保存性能参数
				CapabilityParam capEntity =saveCapabilityParam(datas[i][3].trim(),
						datas[i][11].trim(), datas[i][13].trim(),
						datas[i][12].trim(), datas[i][14].trim(),
						datas[i][17].trim(), datas[i][18].trim(),
						datas[i][26].trim(), datas[i][23].trim(),
						datas[i][19].trim(), datas[i][21].trim(),
						datas[i][20].trim(), datas[i][22].trim(),
						datas[i][16].trim(), datas[i][24].trim());
				listCapability.add(capEntity);
				
				//第三步4补充参数
				FanSupplementParam fan = new FanSupplementParam();
				fan.setFanId(curFanId);
				
				if(datas[i][1].trim().contains("离心轴流")){
					fan.setJ_fjdl("离心轴流");
				}else if(datas[i][1].trim().contains("轴流叶轮")){
					fan.setJ_fjdl("轴流叶轮");
				}else if(datas[i][1].trim().contains("斜流")){
					fan.setJ_fjdl("斜流风机");
				}else if(datas[i][1].trim().contains("轴流")){
					fan.setJ_fjdl("轴流风机");
				}else if(datas[i][1].trim().contains("离心")){
					fan.setJ_fjdl("离心风机");
				}else{
					fan.setJ_fjdl("轴流风机");
				}
				fan.setJ_cpxh(datas[i][0].trim());
				fan.setJ_cdfs(datas[i][5].trim());
				fan.setJ_yps(datas[i][27].trim());
				fan.setJ_jd(datas[i][28].trim());
				fan.setJ_csbg(datas[i][29].trim());
				fan.setJ_zjzl(datas[i][147].trim());
				fan.setJ_wxth(datas[i][148].trim());
				fan.setD_djxh(datas[i][46].trim());
				fan.setD_djcj(datas[i][47].trim());
				fan.setD_djazxs(datas[i][48].trim());
				fan.setD_sfsfbdj(datas[i][49].trim());
				fan.setD_fbdj(datas[i][50].trim());
				fan.setD_edgl(datas[i][51].trim());
				fan.setD_edzs(datas[i][52].trim());
				fan.setD_eddy(datas[i][53].trim());
				fan.setD_eddl(datas[i][54].trim());
				fan.setD_dypl(datas[i][55].trim());
				fan.setD_glys(datas[i][56].trim());
				fan.setD_fhdj(datas[i][57].trim());
				fan.setD_jydj(datas[i][58].trim());
				fan.setD_jrd(datas[i][59].trim());
				fan.setD_dlcd(datas[i][60].trim());
				fan.setD_zdwd(datas[i][61].trim());
				fan.setD_zgwd(datas[i][62].trim());
				fan.setD_yxzdsysd(datas[i][63].trim());
				fan.setD_yxzdsyhb(datas[i][64].trim());
				fan.setD_qzcxh(datas[i][65].trim());
				fan.setD_hzcxh(datas[i][66].trim());
				fan.setD_djnxdj(datas[i][67].trim());
				fan.setD_djkdcz(datas[i][68].trim());
				fan.setD_djzl(datas[i][69].trim());
				fan.setC_jkcz(datas[i][76].trim());
				fan.setC_ylcz(datas[i][77].trim());
				fan.setC_jfkcz(datas[i][78].trim());
				fan.setC_fhwcz(datas[i][79].trim());
				fan.setC_bzj(datas[i][80].trim());
				fan.setZ_qlfx(datas[i][84].trim());
				fan.setZ_tthd(datas[i][85].trim());
				fan.setZ_flhd(datas[i][86].trim());
				fan.setZ_ftlx(datas[i][87].trim());
				fan.setZ_sffbfj(datas[i][88].trim());
				fan.setZ_fbwjjxhcj(datas[i][89].trim());
				fan.setZ_qfhw(datas[i][90].trim());
				fan.setZ_qfhwjj(datas[i][91].trim());
				fan.setZ_hfhw(datas[i][92].trim());
				fan.setZ_hfhwjj(datas[i][93].trim());
				fan.setZ_ftnj(datas[i][94].trim());
				fan.setZ_fl1wj(datas[i][95].trim());
				fan.setZ_fl1lskszyzj(datas[i][96].trim());
				fan.setZ_fl1lskzj(datas[i][97].trim());
				fan.setZ_fl1lskgs(datas[i][98].trim());
				fan.setZ_fl2wj(datas[i][99].trim());
				fan.setZ_fl2lskszyzj(datas[i][100].trim());
				fan.setZ_fl2lskzj(datas[i][101].trim());
				fan.setZ_fl2lskgs(datas[i][102].trim());
				fan.setZ_ftcd(datas[i][103].trim());
				fan.setZ_zjgd(datas[i][104].trim());
				fan.setZ_jxglxh(datas[i][105].trim());
				fan.setZ_jxglcz(datas[i][106].trim());
				fan.setL_ckfx(datas[i][123].trim());
				fan.setL_cdfs(datas[i][124].trim());
				fan.setL_k(datas[i][125].trim());
				fan.setL_c(datas[i][126].trim());
				fan.setL_g(datas[i][127].trim());
				fan.setL_zxg(datas[i][128].trim());
				fan.setL_ckc(datas[i][129].trim());
				fan.setL_ckk(datas[i][130].trim());
				fan.setL_ckazkzj(datas[i][131].trim());
				fan.setL_ckzaksl(datas[i][132].trim());//l_ckzaksl
				fan.setL_jkzj(datas[i][133].trim());
				fan.setL_jkflwj(datas[i][134].trim());
				fan.setL_jkflazkszyzj(datas[i][135].trim());
				fan.setL_jkflazkzj(datas[i][136].trim());
				fan.setL_jkflazksl(datas[i][137].trim());
				fan.setL_azkzj(datas[i][138].trim());
				fan.setL_azksl(datas[i][139].trim());
				fan.setL_jxglxh(datas[i][140].trim());
				fan.setL_jxglcz(datas[i][141].trim());
				this.fanSupplementParamDao.save(fan);
			} else {
				if (StringUtils.isNotBlank(datas[i][11].trim())) {
					CapabilityParam capEntity =saveCapabilityParam(tempCode,
							datas[i][11].trim(), datas[i][13].trim(),
							datas[i][12].trim(), datas[i][14].trim(),
							datas[i][17].trim(), datas[i][18].trim(),
							datas[i][26].trim(), datas[i][23].trim(),
							datas[i][19].trim(), datas[i][21].trim(),
							datas[i][20].trim(), datas[i][22].trim(),
							datas[i][16].trim(), datas[i][24].trim());
					listCapability.add(capEntity);					
				}
			}
		}
	}
	/**
	 * 保存性能参数
	 * @param fanNumber 编号
	 * @param flux 流量
	 * @param totalHead 全压
	 * @param staticPressure 静压
	 * @param internalPower 功率
	 * @param tProductiveness 全效率
	 * @param sProductiveness 静效率
	 * @param noise 噪声
	 * @param soundLevel 比A声级
	 * @param fluxRatio 流量系数
	 * @param totalRatio 全压系数
	 * @param staticRatio 静压系数
	 * @param powerRatio 功率系数
	 * @param fullSpeed 全比转速
	 * @param staticSpeed 静比转速
	 */
	public CapabilityParam saveCapabilityParam(String fanNumber, String flux,
			String totalHead, String staticPressure, String internalPower,
			String tProductiveness, String sProductiveness, String noise,
			String soundLevel, String fluxRatio, String totalRatio,
			String staticRatio, String powerRatio, String fullSpeed,
			String staticSpeed) {
		CapabilityParam capabilityParam = new CapabilityParam();
		capabilityParam.setFanNumber(fanNumber);
		capabilityParam.setFlux(Double.parseDouble(flux));
		capabilityParam.setTotalHead(Double.parseDouble(totalHead));
		capabilityParam.setStaticPressure(Double.parseDouble(staticPressure));
		capabilityParam.setInternalPower(Double.parseDouble(internalPower));
		capabilityParam.setTProductiveness(Double.parseDouble(tProductiveness));
		capabilityParam.setSProductiveness(Double.parseDouble(sProductiveness));
		capabilityParam.setNoise(Double.parseDouble(noise));
		capabilityParam.setSoundLevel(Double.parseDouble(soundLevel));
		capabilityParam.setFluxRatio(Double.parseDouble(fluxRatio));
		capabilityParam.setTotalRatio(Double.parseDouble(totalRatio));
		capabilityParam.setStaticRatio(Double.parseDouble(staticRatio));
		capabilityParam.setPowerRatio(Double.parseDouble(powerRatio));
		capabilityParam.setFullSpeed(Double.parseDouble(fullSpeed));
		capabilityParam.setStaticSpeed(Double.parseDouble(staticSpeed));
		return capabilityParam;
		//capabilityParamDao.save(capabilityParam);
	}
}
