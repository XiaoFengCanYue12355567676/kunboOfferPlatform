package com.kunbo.offer.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.dao.DraughtFanBasePriceDao;
import com.kunbo.offer.dao.DraughtFanDao;
import com.kunbo.offer.dao.FanOfferDao;
import com.kunbo.offer.dao.ManHourDao;
import com.kunbo.offer.dao.MaterialsDao;
import com.kunbo.offer.dao.OfferGeneralViewDao;
import com.kunbo.offer.entity.DraughtFan;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.entity.FanOffer;
import com.kunbo.offer.entity.ManHour;
import com.kunbo.offer.entity.Materials;
import com.kunbo.offer.entity.OfferGeneralView;
import com.kunbo.offer.service.FanOfferService;
import com.shuohe.entity.system.user.User;
@Service
@Transactional
public class FanOfferServiceImpl implements FanOfferService {
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private FanOfferDao fanOfferDao;
	@Autowired
	private DraughtFanDao draughtFanDao;
	
	@Autowired
	private MaterialsDao materialsDao;
	@Autowired
	private ManHourDao manHourDao;
    
	@Autowired
	private DraughtFanBasePriceDao draughtFanBasePriceDao;
	
	@Autowired
	private OfferGeneralViewDao offerGeneralViewDao;
	
	@Override
	public void save(FanOffer fanOffer) {
		fanOfferDao.save(fanOffer);

	}

	@Override
	public void update(FanOffer fanOffer) {
		fanOfferDao.save(fanOffer);

	}

	@Override
	public void delete(String id) {
		fanOfferDao.delete(id);

	}

	@Override
	public FanOffer findById(String id) {
		// TODO Auto-generated method stub
		return fanOfferDao.findOne(id);
	}
    /**
     * 
     * <p>Title: offer</p>
     * <p>Description:风机报价 </p>
     * @param id
     * @return
     * @see com.kunbo.offer.service.FanOfferService#offer(java.lang.String)
     */
	@Override
	public Map<String, Object> offer(String id,List<FanOffer> list) {
		Map<String, Object> map = new HashMap<String, Object>();
//		OfferGeneralView ogv = new OfferGeneralView();
//		double totalPrice = 0;
//		DraughtFan draughtFan = draughtFanDao.findOne(id);
//		for (FanOffer fanOffer : list) {
//			if("标准".equals(fanOffer.getPartsType())) {
//			totalPrice+=fanOffer.getStandardPrice();
//			}else if("材料".equals(fanOffer.getPartsType())) {
//				Materials m = materialsDao.findOne(fanOffer.getNsp_pd());
//				fanOffer.setStandardPrice(fanOffer.getPartsValue()*m.getPrice());
//				totalPrice+=fanOffer.getPartsValue()*m.getPrice();
//			}else if("工时".equals(fanOffer.getPartsType())) {
//				ManHour h = manHourDao.findOne(fanOffer.getNsp_pd());
//				fanOffer.setStandardPrice(fanOffer.getPartsValue()*h.getPrice());
//				totalPrice+=fanOffer.getPartsValue()*h.getPrice();
//			}
//			fanOffer.setDf_pid(id);
//			
//		}
//		User user = (User)request.getSession().getAttribute("user");
//		ogv.setOfferePerson(user.getActual_name());
//		ogv.setFanNumber(id);
//		ogv.setGrossPrice(new BigDecimal(totalPrice));
//		offerGeneralViewDao.save(ogv);
//		fanOfferDao.save(list);
//		map.put("fanData", draughtFan);
//		map.put("offerDetail", list);
//		map.put("totalPrice", totalPrice);
		return map;
		
	}

}
