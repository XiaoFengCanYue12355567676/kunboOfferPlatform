package com.kunbo.offer.service;

import com.kunbo.offer.entity.Materials;

public interface MaterialsService {
	
	void save(Materials materials);
	void update(Materials materials);
	void delete(String id);
	Materials findById(String id);

}
