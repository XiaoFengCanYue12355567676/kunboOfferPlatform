package com.kunbo.offer.service;

import java.util.List;
import java.util.Map;

import com.kunbo.offer.entity.CapabilityParam;

public interface CapabilityParamService {
	void save(CapabilityParam capabilityParam);
	void save(List<CapabilityParam> list,String fanId, String fanNumber) throws Exception;
	void update(CapabilityParam capabilityParam);
	void delete(String id);
	CapabilityParam findById(String id);
	public Map<String, Object> findByFanNumber(String fanNumber, String fanId);
	
	
	public List<CapabilityParam> findByFanNumber(String fanNumber);
	

}
