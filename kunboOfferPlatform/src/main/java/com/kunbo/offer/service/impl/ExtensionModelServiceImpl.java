package com.kunbo.offer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.ExtensionModelDao;
import com.kunbo.offer.entity.ExtensionModel;
import com.kunbo.offer.service.ExtensionModelService;
@Service
public class ExtensionModelServiceImpl implements ExtensionModelService {
	@Autowired
	private ExtensionModelDao extensionModelDao;

	@Override
	public void save(ExtensionModel extensionModel) {
		extensionModelDao.save(extensionModel);

	}

	@Override
	public void update(ExtensionModel extensionModel) {
		extensionModelDao.save(extensionModel);

	}

	@Override
	public void delete(String id) {
		extensionModelDao.delete(id);

	}

	@Override
	public ExtensionModel findById(String id) {
		// TODO Auto-generated method stub
		return extensionModelDao.findOne(id);
	}

}
