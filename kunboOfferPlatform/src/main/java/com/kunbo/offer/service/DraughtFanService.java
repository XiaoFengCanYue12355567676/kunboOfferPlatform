package com.kunbo.offer.service;

import java.util.List;

import com.kunbo.offer.entity.DraughtFan;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.entity.FanBaseParameter;

public interface DraughtFanService {
	void save(DraughtFan draughtFan,List<DraughtFanBasePrice> list);
	void update(DraughtFan draughtFan);
	void delete(String id);
	DraughtFan findById(String id);
	/**
	 * <p>Title: saveFanAndParameter</p>
	 * <p>Description:新增风机和风机的基本参数 </p>
	 * @param draughtFan
	 * @param fanBaseParameter
	 */
	void saveFanAndParameter(DraughtFan draughtFan,FanBaseParameter fanBaseParameter);
	

}
