package com.kunbo.offer.service;

import java.util.List;
import java.util.Map;

import com.kunbo.offer.entity.FanBaseParameter;
import com.kunbo.offer.entity.ProductCategory;

public interface ProductCategoryService {
	void save(ProductCategory productCategory,FanBaseParameter fanBaseParameter);
	void update(ProductCategory productCategory);
	void delete(String id);
	ProductCategory findById(String id);
	List<ProductCategory> getTreeProductCategoryList();
	/**
	 * 
	 * <p>Title: findByPcPid</p>
	 * <p>Description: 根据分类的id过去参数</p>
	 * @param pid
	 * @return
	 */
	Map<String,Object> findByPcPid(String pid);
	/**
	 * 
	 * <p>Title: getFanParameterStrByPcPid</p>
	 * <p>Description:获取风机参数json串 </p>
	 * @param pid
	 * @return
	 */
	String getFanParameterStrByPcPid(String pid);
	

}
