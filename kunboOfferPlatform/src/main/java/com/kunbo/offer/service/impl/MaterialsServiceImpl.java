package com.kunbo.offer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.MaterialsDao;
import com.kunbo.offer.entity.Materials;
import com.kunbo.offer.service.MaterialsService;
/**
 * 
 * <p>Title: MaterialsServiceImpl</p>
 * <p>Description:风机材料service </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月3日下午3:38:46
 * @version 1.0
 */
@Service
public class MaterialsServiceImpl implements MaterialsService {
	@Autowired
	private MaterialsDao materialsDao;

	@Override
	public void save(Materials materials) {
		materialsDao.save(materials);

	}

	@Override
	public void update(Materials materials) {
		materialsDao.save(materials);

	}

	@Override
	public void delete(String id) {
//		materialsDao.delete(id);

	}

	@Override
	public Materials findById(String id) {
		// TODO Auto-generated method stub
		return materialsDao.findById(id);
	}

}
