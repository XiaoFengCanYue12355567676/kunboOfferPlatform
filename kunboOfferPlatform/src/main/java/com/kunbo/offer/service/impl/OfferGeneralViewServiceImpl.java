package com.kunbo.offer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.dao.OfferGeneralViewDao;
import com.kunbo.offer.entity.OfferGeneralView;
import com.kunbo.offer.service.OfferGeneralViewService;
@Service
@Transactional
public class OfferGeneralViewServiceImpl implements OfferGeneralViewService {
    @Autowired
    private OfferGeneralViewDao offerGeneralViewDao;
	@Override
	public void save(OfferGeneralView offerGeneralView) {
		offerGeneralViewDao.save(offerGeneralView);

	}

	@Override
	public void update(OfferGeneralView offerGeneralView) {
		offerGeneralViewDao.save(offerGeneralView);

	}

	@Override
	public void delete(String id) {
		offerGeneralViewDao.delete(id);

	}

	@Override
	public OfferGeneralView findById(String id) {
		// TODO Auto-generated method stub
		return offerGeneralViewDao.findOne(id);
	}

}
