package com.kunbo.offer.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.kunbo.offer.dao.FanModelPartsDao;
import com.kunbo.offer.entity.FanModelParts;
import com.kunbo.offer.service.FanModelPartsService;
/**
 * 
 * <p>Title: ExtensionModelServiceImpl</p>
 * <p>Description:风机模型业务层 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午5:46:15
 * @version 1.0
 */
@Service
@Transactional
public class FanModelPartsServiceImpl implements FanModelPartsService {
	@Autowired
	private FanModelPartsDao fanModelPartsDao;

	@Override
	public void save(FanModelParts fanModelParts) {
		fanModelPartsDao.save(fanModelParts);

	}

	@Override
	public void update(FanModelParts fanModelParts) {
		fanModelPartsDao.save(fanModelParts);

	}

	@Override
	public void delete(String id) {
		fanModelPartsDao.delete(id);

	}

	@Override
	public FanModelParts findById(String id) {
		// TODO Auto-generated method stub
		return fanModelPartsDao.findOne(id);
	}

}
