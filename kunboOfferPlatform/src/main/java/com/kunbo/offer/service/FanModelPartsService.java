package com.kunbo.offer.service;

import com.kunbo.offer.entity.FanModelParts;

public interface FanModelPartsService {
	void save(FanModelParts fanModelParts);
	void update(FanModelParts fanModelParts);
	void delete(String id);
	FanModelParts findById(String id);

}
