package com.kunbo.offer.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.dao.ManHourDao;
import com.kunbo.offer.entity.ManHour;
import com.kunbo.offer.service.ManHourService;
/**
 * <p>Title: ManHourServiceImpl</p>
 * <p>Description:工时service </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月3日下午3:29:45
 * @version 1.0
 */
@Service
@Transactional
public class ManHourServiceImpl implements ManHourService {
	
	@Autowired
	private ManHourDao manHourDao;

	@Override
	public void save(ManHour manHour) {
		manHourDao.save(manHour);

	}

	@Override
	public void update(ManHour manHour) {
		manHourDao.save(manHour);

	}

	@Override
	public void delete(String id) {
		manHourDao.delete(id);

	}

	@Override
	public ManHour findById(String id) {
		// TODO Auto-generated method stub
		return manHourDao.findOne(id);
	}

	@Override
	public List<ManHour> findAll() {
		// TODO Auto-generated method stub
		return (List<ManHour>) manHourDao.findAll();
	}

}
