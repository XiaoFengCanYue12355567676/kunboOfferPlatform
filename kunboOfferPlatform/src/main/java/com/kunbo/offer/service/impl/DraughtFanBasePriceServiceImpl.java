package com.kunbo.offer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.dao.DraughtFanBasePriceDao;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.service.DraughtFanBasePriceService;
@Service
@Transactional
public class DraughtFanBasePriceServiceImpl implements DraughtFanBasePriceService {
	@Autowired
	private DraughtFanBasePriceDao draughtFanBasePriceDao;
	

	@Override
	public void save(DraughtFanBasePrice t) {
		draughtFanBasePriceDao.save(t);

	}

	@Override
	public void update(DraughtFanBasePrice t) {
		draughtFanBasePriceDao.save(t);

	}

	@Override
	public void delete(String id) {
		draughtFanBasePriceDao.delete(id);

	}

	@Override
	public DraughtFanBasePrice findById(String id) {
		// TODO Auto-generated method stub
		return draughtFanBasePriceDao.findOne(id);
	}

}
