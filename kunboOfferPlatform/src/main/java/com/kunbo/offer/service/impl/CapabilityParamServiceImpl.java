package com.kunbo.offer.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.CapabilityParamDao;
import com.kunbo.offer.entity.CapabilityParam;
import com.kunbo.offer.service.CapabilityParamService;
import com.kunbo.shop.dao.sort.ProductSortDao;
import com.kunbo.shop.entity.sort.ProductSort;
import com.shuohe.util.fitting.FittingUtils;
@Service
public class CapabilityParamServiceImpl implements CapabilityParamService {
	@Autowired
	private CapabilityParamDao capabilityParamDao;
	@Autowired
	private ProductSortDao productSortDao;

	@Override
	public void save(CapabilityParam capabilityParam) {
		capabilityParamDao.save(capabilityParam);

	}

	@Override
	public void save(List<CapabilityParam> list,String fanId,String fanNumber) throws Exception {
		ArrayList<Double> listFlux=new ArrayList<Double>();//存流量
		ArrayList<Double> listIP=new ArrayList<Double>();//存内功率
		ArrayList<Double> listPT=new ArrayList<Double>();//存效率
		ArrayList<Double> listTH=new ArrayList<Double>();//存全压
		ArrayList<Double> listSH=new ArrayList<Double>();//存静压
		for (CapabilityParam capabilityParam : list) {
			listFlux.add(capabilityParam.getFlux());//流量
			listIP.add(capabilityParam.getInternalPower());//输出功率(轴功率没有用过，先当轴功率使用)
			listPT.add(capabilityParam.getTProductiveness());//全压效率
			listTH.add(capabilityParam.getTotalHead());//全压
			listSH.add(capabilityParam.getStaticPressure());//静压
			capabilityParam.setFanNumber(fanNumber);
		}
		//获取流量性能参数中的最小值和最大值
		Double min = Collections.min(listFlux);//获取最小值 
		Double max = Collections.max(listFlux);//获取最大值
		System.out.println("最小值为："+min);
		System.out.println("最大值为："+max);
		Double[] arrFlux = new Double[listFlux.size()];
		listFlux.toArray(arrFlux);
		Double[] arrIP = new Double[listIP.size()];
		listIP.toArray(arrIP);
		Double[] arrPT = new Double[listPT.size()];
		listPT.toArray(arrPT);
		Double[] arrTH= new Double[listTH.size()];
		listTH.toArray(arrTH);
		Double[] arrSH= new Double[listSH.size()];
		listSH.toArray(arrSH);
		String a = FittingUtils.getFitting(arrFlux, arrTH, 2);
		String b = FittingUtils.getFitting(arrFlux, arrPT, 2);
		String c = FittingUtils.getFitting(arrFlux, arrIP, 2);
		String d = FittingUtils.getFitting(arrFlux, arrSH, 2);
		ProductSort ps = productSortDao.findOne(fanId);
		ps.setPressureCurveFormula(a);
		ps.setEfficiencyCurveFormula(b);
		ps.setInternalPowerCurveFormula(c);
		ps.setStaticCurveFormula(d);
		ps.setMinFlux(min);
		ps.setMaxFlux(max);
		System.out.println("全压拟合公式为:"+a+"=====全压值："+arrTH[0]);
		System.out.println("静压拟合公式为:"+d+"=====静压值："+arrSH[0]);
		System.out.println("效率拟合公式为:"+b);
		System.out.println("内功率拟合公式为:"+c);
		productSortDao.save(ps);//将拟合公式保存到风机实体
		capabilityParamDao.save(list);

	}

	@Override
	public void update(CapabilityParam capabilityParam) {
		capabilityParamDao.save(capabilityParam);

	}

	@Override
	public void delete(String id) {
		capabilityParamDao.delete(id);

	}

	@Override
	public CapabilityParam findById(String id) {
		// TODO Auto-generated method stub
		return capabilityParamDao.findOne(id);
	}
	//	private Double totalHead;//全压
	//private Double staticPressure;//静压
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String,Object> findByFanNumber(String fanNumber, String fanId) {
		Map<String,Object> rm = new HashMap<String,Object>();
		List<CapabilityParam> resultList = capabilityParamDao.findByFanNumber(fanNumber);
		ProductSort ps = productSortDao.findOne(fanId);
		//获取实际情况压力系数值
		double pc = ps.getPressureRealityRatio();
		//Collections.sort(resultList, Comparator.comparing(CapabilityParam::getFlux));
		Collections.sort(resultList);
		List listPressure = new ArrayList();//压力
		List listStaticPressure = new ArrayList();//静压压力
		List listProductiveness = new ArrayList();//效率
		List listPower = new ArrayList();//功率
		List listNoise = new ArrayList();//噪声
        System.out.println("after sort:");
        for (int i=0;i<resultList.size();i++) {
        	List<Double> arrPressure = new ArrayList<Double>();
        	List<Double> arrStaticPressure = new ArrayList<Double>();
			List<Double> arrProductiveness = new ArrayList<Double>();
			List<Double> arrPower = new ArrayList<Double>();
			List<Double> arrNoise = new ArrayList<Double>();
            System.out.println("after sort---->flux: " + resultList.get(i).getFlux());
			arrPressure.add(resultList.get(i).getFlux());//流量
			arrPressure.add(resultList.get(i).getTotalHead()*pc);//实际压力
			System.out.println("理论压力："+resultList.get(i).getTotalHead());
			System.out.println("实际压力："+resultList.get(i).getTotalHead()*pc);
			//静压--开始
			arrStaticPressure.add(resultList.get(i).getFlux());//流量
			arrStaticPressure.add(resultList.get(i).getStaticPressure()*pc);//实际压力
			//静压--结束			
			arrProductiveness.add(resultList.get(i).getFlux());//流量
			arrProductiveness.add(resultList.get(i).getTProductiveness());//效率
			
			arrPower.add(resultList.get(i).getFlux());//流量
			arrPower.add(resultList.get(i).getInternalPower());//内功率
			
			arrNoise.add(resultList.get(i).getFlux());//流量
			arrNoise.add(resultList.get(i).getNoise());//噪声
			
			listPressure.add(arrPressure);
			listStaticPressure.add(arrStaticPressure);
			listProductiveness.add(arrProductiveness);
			listPower.add(arrPower);
			listNoise.add(arrNoise);
        }
        rm.put("pressureData", listPressure);
        rm.put("pressureStaticData", listStaticPressure);
        rm.put("productivenessData", listProductiveness);
        rm.put("powerData", listPower);
        rm.put("noiseData", listNoise);
		return rm;
	}

	@Override
	public List<CapabilityParam> findByFanNumber(String fanNumber) {
		// TODO Auto-generated method stub
		List<CapabilityParam> list = capabilityParamDao.findByFanNumber(fanNumber);
		return list;
	}

}
