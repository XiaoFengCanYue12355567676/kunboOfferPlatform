package com.kunbo.offer.service;

import java.util.List;

import com.kunbo.offer.entity.FanOfferCollect;

public interface FanOfferCollectService {
	/**
	 * 保存收藏
	 * 
	 * @param fanOfferCollect
	 *            实体
	 */
	void save(FanOfferCollect fanOfferCollect);

	/**
	 * 删除收藏
	 * 
	 * @param id
	 *            主键
	 */
	void delete(String id);

	/**
	 * 通过用户查询收藏
	 * 
	 * @param userId
	 *            必填-用户ID
	 * @return
	 */
	List<FanOfferCollect> findByUserId(Integer userId);

}
