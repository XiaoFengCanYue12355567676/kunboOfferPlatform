package com.kunbo.offer.service;

import com.kunbo.offer.entity.FanBaseParameter;
import com.kunbo.offer.entity.ProductMix;

public interface ProductMixService {
	/**
	 * 
	 * <p>Title: getProductTreeById</p>
	 * <p>Description:获取产品树结构 </p>
	 * @return
	 */
	ProductMix getProductTreeById(String pid);
	/**
	 * 
	 * <p>Title: save</p>
	 * <p>Description:保存产品 </p>
	 * @param p
	 * @param f
	 */
	void save(ProductMix p,FanBaseParameter f);
	/**
	 * 保存产品下面的配件
	 * <p>Title: save</p>
	 * <p>Description: </p>
	 * @param p
	 */
	void save(ProductMix p);
	/**
	 * 
	 * <p>Title: fanModelOffer</p>
	 * <p>Description:风机模型报价 </p>
	 * @param fanTypeId
	 */
	void fanModelOffer(String modelNumber);

}
