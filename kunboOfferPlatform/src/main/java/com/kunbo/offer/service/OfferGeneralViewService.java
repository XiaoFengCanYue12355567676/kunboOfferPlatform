package com.kunbo.offer.service;
import com.kunbo.offer.entity.OfferGeneralView;

public interface OfferGeneralViewService {
	void save(OfferGeneralView offerGeneralView);
	void update(OfferGeneralView offerGeneralView);
	void delete(String id);
	OfferGeneralView findById(String id);

}
