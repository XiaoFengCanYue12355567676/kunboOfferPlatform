package com.kunbo.offer.service;

import java.util.List;

import com.kunbo.offer.entity.ManHour;

public interface ManHourService {
	void save(ManHour manHour);
	void update(ManHour manHour);
	void delete(String id);
	List<ManHour> findAll();
	ManHour findById(String id);

}
