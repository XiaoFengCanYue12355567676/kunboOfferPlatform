package com.kunbo.offer.service;

import com.kunbo.offer.entity.DraughtFanBasePrice;

public interface DraughtFanBasePriceService {
	void save(DraughtFanBasePrice t);
	void update(DraughtFanBasePrice t);
	void delete(String id);
	DraughtFanBasePrice findById(String id);

}
