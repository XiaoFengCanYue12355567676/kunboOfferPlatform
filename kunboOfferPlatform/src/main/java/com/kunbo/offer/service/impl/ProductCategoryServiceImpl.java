package com.kunbo.offer.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.FanBaseParameterDao;
import com.kunbo.offer.dao.ProductCategoryDao;
import com.kunbo.offer.entity.FanBaseParameter;
import com.kunbo.offer.entity.ProductCategory;
import com.kunbo.offer.service.ProductCategoryService;
/**
 * 
 * <p>Title: ProductCategoryServiceImpl</p>
 * <p>Description: </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月3日下午3:43:06
 * @version 1.0
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
	@Autowired
	private ProductCategoryDao productCategoryDao;
	@Autowired
	private FanBaseParameterDao fanBaseParameterDao;

	@Override
	public void save(ProductCategory productCategory,FanBaseParameter fanBaseParameter) {
		ProductCategory p = productCategoryDao.save(productCategory);
		fanBaseParameter.setPc_pid(p.getId());
		fanBaseParameterDao.save(fanBaseParameter);

	}
	@Override
	public void update(ProductCategory productCategory) {
		productCategoryDao.save(productCategory);

	}

	@Override
	public void delete(String id) {
		productCategoryDao.delete(id);

	}
	@Override
	public ProductCategory findById(String id) {
		// TODO Auto-generated method stub
		return productCategoryDao.findOne(id);
	}
	@Override
	public List<ProductCategory> getTreeProductCategoryList() {
		List<ProductCategory> rootMenus  = (List<ProductCategory>) productCategoryDao.findAll();//获取所有的风机大类的列表
		List<ProductCategory> list = getTreeList(rootMenus);
		return list;
	}
	
	
	private List<ProductCategory> getTreeList(List<ProductCategory> rootMenus){
		List<ProductCategory> list = new ArrayList<ProductCategory>();
		//获取一级菜单
		for (ProductCategory productCategory : rootMenus) {
			if (StringUtils.isEmpty(productCategory.getPid())){
                list.add(productCategory);
            }
		}
		//查找二级菜单
		/**
		 * 利用递归找出所有子菜单
		 */
		for (ProductCategory productCategory : list) {
			productCategory.setChildren(getChild(productCategory.getId(),rootMenus));
		}
		return list;
		
	}
	
	
	private List<ProductCategory> getChild(String id, List<ProductCategory> rootMenu) {
        // 子菜单
        List<ProductCategory> childList = new ArrayList<ProductCategory>();
 
        for (ProductCategory productCategory : rootMenu) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (!StringUtils.isEmpty(productCategory.getPid())) {
                if (productCategory.getPid().equals(id)) {
                    childList.add(productCategory);
                }
            }
        }
 
        // 把子菜单的子菜单再循环一遍
        for (ProductCategory productCategory : childList) {// 没有url子菜单还有子菜单
            if (!StringUtils.isEmpty(productCategory.getPid())) {
                // 递归
            	productCategory.setChildren(getChild(productCategory.getId(), rootMenu));
            }
        }
 
        // 递归退出条件
        if (childList.size() == 0) {
            return childList;
        }
 
        return childList;
    }
	@Override
	public Map<String,Object> findByPcPid(String pid) {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String,Object>();
		FanBaseParameter f = fanBaseParameterDao.findByPc_pid(pid);
		map.put("内径", f.getBoreSize());
		map.put("外径", f.getExternalDiameter());
		map.put("风筒板厚", f.getAirDuctThickness());
		map.put("风筒高度", f.getAirDuctHeight());
		map.put("叶片数", f.getBladeQuantity());
		map.put("叶片单价", f.getBladeUnitPrice());
		map.put("轮毂", f.getHub());
		map.put("轴套", f.getAxleSleeve());
		map.put("叶片标价", f.getNormalPrice());
		//map.put("风机型号", f.getModel());
		return map;
	}
	@Override
	public String getFanParameterStrByPcPid(String pid) {
		FanBaseParameter f = fanBaseParameterDao.findByPc_pid(pid);
		return f.getFanParameterJsonStr();
	}

}
