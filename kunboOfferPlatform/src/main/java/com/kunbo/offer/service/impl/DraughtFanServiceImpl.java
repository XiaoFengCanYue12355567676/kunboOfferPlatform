package com.kunbo.offer.service.impl;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kunbo.offer.dao.DraughtFanBasePriceDao;
import com.kunbo.offer.dao.DraughtFanDao;
import com.kunbo.offer.dao.FanBaseParameterDao;
import com.kunbo.offer.dao.ManHourDao;
import com.kunbo.offer.dao.MaterialsDao;
import com.kunbo.offer.entity.DraughtFan;
import com.kunbo.offer.entity.DraughtFanBasePrice;
import com.kunbo.offer.entity.FanBaseParameter;
import com.kunbo.offer.entity.ManHour;
import com.kunbo.offer.entity.Materials;
import com.kunbo.offer.service.DraughtFanService;
/**
 * 
 * <p>Title: DraughtFanServiceImpl</p>
 * <p>Description:风机业务层 </p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月5日下午5:34:52
 * @version 1.0
 */
@Service
@Transactional
public class DraughtFanServiceImpl implements DraughtFanService {
	@Autowired
	private DraughtFanDao draughtFanDao;
	
	@Autowired
	private FanBaseParameterDao fanBaseParameterDao;
	
	@Autowired
	private MaterialsDao materialsDao;
	@Autowired
	private ManHourDao manHourDao;
	
	@Autowired
	private DraughtFanBasePriceDao draughtFanBasePriceDao;

	@Override
	public void save(DraughtFan draughtFan,List<DraughtFanBasePrice> list) {
		double totalPrice = 0;
//		//此处不进总价的合计，只计算需要合计配件的价格
//		draughtFan =  draughtFanDao.save(draughtFan);
//		String fanId = draughtFan.getId();
//		for (DraughtFanBasePrice draughtFanBasePrice : list) {
//			if("标准".equals(draughtFanBasePrice.getPartsType())) {
//				draughtFanBasePrice.setFmp_pid(draughtFanBasePrice.getId());
//				draughtFanBasePrice.setDf_pid(fanId);
//				draughtFanBasePrice.setStandardPrice(draughtFanBasePrice.getPartsValue());
//				totalPrice+=draughtFanBasePrice.getStandardPrice();
//			}else if("材料".equals(draughtFanBasePrice.getPartsType())) {
//				draughtFanBasePrice.setFmp_pid(draughtFanBasePrice.getId());
//				draughtFanBasePrice.setDf_pid(fanId);
//				Materials m = materialsDao.findOne(draughtFanBasePrice.getNsp_pd());
//				draughtFanBasePrice.setStandardPrice(draughtFanBasePrice.getPartsValue()*m.getPrice());
//				totalPrice+=draughtFanBasePrice.getPartsValue()*m.getPrice();
//			}else if("工时".equals(draughtFanBasePrice.getPartsType())) {
//				draughtFanBasePrice.setFmp_pid(draughtFanBasePrice.getId());
//				draughtFanBasePrice.setDf_pid(fanId);
//				ManHour h = manHourDao.findOne(draughtFanBasePrice.getNsp_pd());
//				draughtFanBasePrice.setStandardPrice(draughtFanBasePrice.getPartsValue()*h.getPrice());
//				totalPrice+=draughtFanBasePrice.getPartsValue()*h.getPrice();
//			}
//		}
//		draughtFan = draughtFanDao.findOne(fanId);
//		draughtFan.setFanReferenceTotalPrice(new BigDecimal(totalPrice));
//		draughtFanDao.save(draughtFan);
//		draughtFanBasePriceDao.save(list);
//		
		

	}

	@Override
	public void update(DraughtFan draughtFan) {
		draughtFanDao.save(draughtFan);

	}

	@Override
	public void delete(String id) {
		draughtFanDao.delete(id);

	}

	@Override
	public DraughtFan findById(String id) {
		// TODO Auto-generated method stub
		return draughtFanDao.findOne(id);
	}

	@Override
	public void saveFanAndParameter(DraughtFan draughtFan, FanBaseParameter fanBaseParameter) {
		draughtFanDao.save(draughtFan);
		fanBaseParameterDao.save(fanBaseParameter);
		
		
	}

}
