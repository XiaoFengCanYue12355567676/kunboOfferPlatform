package com.kunbo.offer.service;

import java.util.List;
import java.util.Map;

import com.kunbo.offer.entity.FanOffer;

public interface FanOfferService {
	void save(FanOffer fanOffer);
	void update(FanOffer fanOffer);
	void delete(String id);
	FanOffer findById(String id);
	
	Map<String,Object> offer(String id,List<FanOffer> list);

}
