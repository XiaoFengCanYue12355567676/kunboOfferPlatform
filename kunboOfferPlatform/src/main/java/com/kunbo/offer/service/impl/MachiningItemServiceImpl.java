package com.kunbo.offer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.MachiningItemDao;
import com.kunbo.offer.entity.MachiningItem;
import com.kunbo.offer.service.MachiningItemService;
/**
 * 
 * <p>Title: MachiningItemServiceImpl</p>
 * <p>Description: 风机加工件业务层</p>
 * <p>Company: www.kunbo.cn</p> 
 * @author	yang'f
 * @date	2018年11月13日上午9:50:48
 * @version 1.0
 */
@Service
public class MachiningItemServiceImpl implements MachiningItemService {
	@Autowired
	private MachiningItemDao machiningItemDao;

	@Override
	public void save(MachiningItem machiningItem) {
		machiningItemDao.save(machiningItem);

	}

	@Override
	public void update(MachiningItem machiningItem) {
		machiningItemDao.save(machiningItem);

	}

	@Override
	public void delete(String id) {
		machiningItemDao.delete(id);

	}

	@Override
	public MachiningItem findById(String id) {
		// TODO Auto-generated method stub
		return machiningItemDao.findOne(id);
	}

	@Override
	public List<MachiningItem> findAll() {
		// TODO Auto-generated method stub
		return (List<MachiningItem>) machiningItemDao.findAll();
	}

	@Override
	public List<MachiningItem> getTypeList() {
		return machiningItemDao.getTypeList();
	}

	@Override
	public List<MachiningItem> findListByType(String type) {
		// TODO Auto-generated method stub
		return machiningItemDao.findListByType(type);
	}

}
