package com.kunbo.offer.service;

import java.util.List;

import com.kunbo.offer.entity.MachiningItem;


public interface MachiningItemService {
	void save(MachiningItem machiningItem);
	void update(MachiningItem machiningItem);
	void delete(String id);
	MachiningItem findById(String id);
	List<MachiningItem> findAll();
	
	
	List<MachiningItem> getTypeList();
	List<MachiningItem> findListByType(String type);

}
