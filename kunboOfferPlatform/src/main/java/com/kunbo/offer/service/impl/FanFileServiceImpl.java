package com.kunbo.offer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.kunbo.offer.dao.FanFileDao;
import com.kunbo.offer.entity.FanFile;
import com.kunbo.offer.service.FanFileService;
@Service
public class FanFileServiceImpl implements FanFileService {
	
	@Autowired
	private FanFileDao fanFileDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void save(List<FanFile> fanFiles) {
		fanFileDao.save(fanFiles);

	}

	@Override
	public void update(FanFile fanFile) {
		fanFileDao.save(fanFile);

	}

	@Override
	public void delete(String id) {
		fanFileDao.delete(id);

	}

	@Override
	public FanFile findById(String id) {
		// TODO Auto-generated method stub
		return fanFileDao.findOne(id);
	}

	@Override
	public List<FanFile> findListByFanNumberAndFileType(String fanNumber,
			String fileType) {
		// TODO Auto-generated method stub
		return fanFileDao.findListByFanNumberAndFileType(fanNumber, fileType);
	}

	@Override
	public void deleteByNumberAndType(String fanNumber, String fileType) {
		String sql = " delete from shop_fan_file where fan_number='"+ fanNumber +"' and fileType = '"+ fileType +"'";
		jdbcTemplate.execute(sql);
		
	}

}
