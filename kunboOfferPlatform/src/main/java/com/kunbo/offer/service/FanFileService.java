package com.kunbo.offer.service;

import java.util.List;

import com.kunbo.offer.entity.FanFile;

public interface FanFileService {
	void save(List<FanFile> fanFiles);
	void update(FanFile fanFile);
	void delete(String id);
	FanFile findById(String id);
	List<FanFile> findListByFanNumberAndFileType(String fanNumber,String fileType);
	void deleteByNumberAndType(String fanNumber,String fileType);
}
