package com.kunbo.offer.service;

import com.kunbo.offer.entity.ExtensionModel;

public interface ExtensionModelService {
	void save(ExtensionModel extensionModel);
	void update(ExtensionModel extensionModel);
	void delete(String id);
	ExtensionModel findById(String id);

}
