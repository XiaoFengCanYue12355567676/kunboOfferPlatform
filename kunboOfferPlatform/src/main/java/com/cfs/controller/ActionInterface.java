package com.cfs.controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cfs.service.CfsServiceImpl;
import com.cfs.service.CfsServiceImpl.XiaLa;
import com.shuohe.service.util.sql.EasyuiServiceImpl;

@Controller  
@RequestMapping("/pages/crminterface/*")  
public class ActionInterface
{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
    @Resource
    private EasyuiServiceImpl easyUiDto;
    
    @Resource
    private CfsServiceImpl cfsServiceImpl;
	
	 
	/**
	 * 根据表名新增数据的接口
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="creatSelectTable.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String creatSelectTable(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String jsonStr = request.getParameter("jsonStr");
	/* jsonStr ="{\"type\":\"1\",\"tableName\":\"abba\",\"title\":\"数据表描述\",\"textField\":\"显示字段name\",\"valueField\":\"显示value_id\"}" ;*/	
		System.out.println(jsonStr);
		try
		{
			int ret = cfsServiceImpl.doSelect(jsonStr);
			if(ret == -2){
				return ("表名已存在");   
			}else if(ret == 1){
				return ("创建成功");
			}else{
				return ("创建失败");
			} 
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return (callback+"("+e+")");    
		}
	}
	
	@RequestMapping(value="creatTable.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String creatTable(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String jsonStr = request.getParameter("jsonStr");
	  	 
	/*  	jsonStr = "{\"title\":\"ww\",\"name\":\"表单标题\",\"type\":\"0\","
					+"\"field\":[{\"fieldType\":\"textbox\",\"text\":\"name\","
					+"\"title\":\"标题 \",\"width\":\"278\",\"height\":\"32\",\"editable\":\"true\","
					+"\"disabled\":\"false\",\"readonly\":\"false\",\"required\":\"false\","
					+"\"listDisplay\":\"true\",\"type\":\"varchar(255)\"}]}";  
	   */
		try
		{
			boolean ret = cfsServiceImpl.doMethod(jsonStr);
			if(ret){
				return ("true");   
			}else{
				return ("false");
			} 
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return (callback+"("+e+")");    
		}
	}
	
	@RequestMapping(value="deleteSelectTable.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String deleteSelectTable(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String tableName = request.getParameter("tableName");
		String type = request.getParameter("type");

		try
		{
			int ret = cfsServiceImpl.deleteSelectTable(tableName, type); 
			if(ret == 1){
				return ("删除成功");
			}else{
				return ("删除失败");
			} 
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return (callback+"("+e+")");    
		}
	}
	
	@RequestMapping(value="getDatagridForJson.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String getDatagridForJson(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String tableName = request.getParameter("tableName");

		try
		{
			String str = (cfsServiceImpl.getJsonByTableName(tableName));
			System.err.println(str);
			return str;     
		}
		catch(Exception e)
		{
			e.printStackTrace();    
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="IsExistenceForTableName.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String IsExistenceForTableName(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String tableName = request.getParameter("tableName");
		try
		{
			return (cfsServiceImpl.tableNameExist(tableName)+"");     
		}
		catch(Exception e)
		{
			e.printStackTrace();  
			return e.getMessage();
		}
	}
	/**
	 * 获取所有表名
	 * @param table_name
	 * @return
	 */
	@RequestMapping(value="getAllTableName.do",produces="application/json; charset=UTF-8")  
    public @ResponseBody ArrayList<XiaLa> getAllTableName(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		 ArrayList<XiaLa> arr = cfsServiceImpl.getTableName();
			return arr;
		 
	 }
	
}
