package com.cfs.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cfs.entity.ButtonFramworkework;
import com.cfs.service.CbfServiceImpl; 
import com.shuohe.service.EasyUiDTO;
import com.tyteam.apps.common.JsonResult;

@Controller  
@RequestMapping("/pages/button/framework/*")  
public class ActionButtonFramework
{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
    @Resource
    private EasyUiDTO easyUiDto;
    
    @Resource
	private CbfServiceImpl cbfServiceImpl;
    	
    /**
     * 创建框架和更新框架
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
	@RequestMapping(value="create.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String create(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String jsonStr = request.getParameter("jsonStr");
		String jsCode = request.getParameter("jsCode");
	   /* jsonStr = "{\"title\":\"dd\","
				+ "\"columns\":[{\"title\":\"部门\",\"field\":\"department\",\"formatter\":\"\"},"
				+ "{\"title\":\"产品经理\",\"field\":\"product_manager\",\"formatter\":\"\"}],"
				+ "\"buttons\":[{\"buttonName\":\"新增\",\"buttonId\":\"add\",\"buttonAttr\":\"fa fa-plus\","
				+ "\"buttonFunction\":\"functionAdd()\"},{\"buttonName\":\"修改\",\"buttonId\":\"edit\","
				+ "\"buttonAttr\":\"fa fa-pencil\",\"buttonFunction\":\"functionModify()\"}],"
				+ "\"searchs\":[{\"searchName\":\"部门\",\"searchId\":\"departmentS\",\"searchAttr\":\"\"}],"
			    + "\"search_buttons\":[{\"buttonName\":\"搜索\",\"buttonId\":\"search\",\"buttonAttr\":\"fa fa-search\","
	        	+ "\"buttonFunction\":\"functionSearch()\"}]}" ; 
	    
	    jsCode = "更新" ;*/

		try {
			int ret = cbfServiceImpl.doMethod(jsonStr,jsCode);
			if(1==ret){
				return ("true");   
			}else{
				return ("false");
			} 
		}catch(Exception e){
			e.printStackTrace();
			return (callback+"("+e+")");    
		}
	}
	  /**
     * 删除框架
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
	@RequestMapping(value="delete.do",produces="text/html; charset=UTF-8")  
    public @ResponseBody String delete(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		String callback=request.getParameter("callback");
		String title = request.getParameter("title");
		try {
			int ret = cbfServiceImpl.delete(title);
			if(1==ret){
				return ("true");   
			}else{
				return ("false");
			} 
		}catch(Exception e){
			e.printStackTrace();
			return (callback+"("+e+")");    
		}
	}
	@RequestMapping(value="get.do")  
    public @ResponseBody JsonResult get(HttpServletRequest request,HttpServletResponse response) throws Exception{  
		JsonResult result = new JsonResult();
		ButtonFramworkework bf =null;
		String callback=request.getParameter("callback");
		String title = request.getParameter("title");
		try {
			bf =  cbfServiceImpl.get(title);
		}catch(Exception e){
			e.printStackTrace();
		}
		result.setObj(bf);
		result.setStatus("0");
		return result ;
	}
 }
