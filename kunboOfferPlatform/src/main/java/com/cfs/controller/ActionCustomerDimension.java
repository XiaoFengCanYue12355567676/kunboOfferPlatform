package com.cfs.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cfs.entity.HistoricalData;
import com.shuohe.service.EasyUiDTO;
import com.shuohe.util.db.DbQuerySql;
import com.shuohe.util.easyuiBean.EasyUiDataGrid;

/**
 * 客户维度调研
 * @author zz
 *
 */
@Controller  
@RequestMapping("/market/customer/dimension*")  
public class ActionCustomerDimension
{
	 @Autowired
     private JdbcTemplate jdbcTemplate; 
	
	 @Resource
	 private EasyUiDTO easyUiDto;
    
	@RequestMapping(value="getVison.do")  
    public @ResponseBody int getVison(HttpServletRequest request,HttpServletResponse response) throws SQLException{  
		int version = 1;
		String customer_id = request.getParameter("id"); 
		String sql = "select  max(version) from  business_cloud_customer_dimension  where customer_code="+Integer.parseInt(customer_id);
		Statement stmt = jdbcTemplate.getDataSource().getConnection().createStatement();  
        ResultSet rs = stmt.executeQuery(sql);
        if (rs.next()) {  
        	 //存在记录 rs就要向上移一条记录 因为rs.next会滚动一条记录了  
        	 rs.previous();  
        	 //在执行while 循环  
        	while(rs.next()){  
        		version = rs.getInt(1)+1;
        	 System.out.println(rs.getInt(1)+"=="+version);  
        	}
        }else{
        	 return version;
        } 
        stmt.close();
        return version;
	}
	 
 }
