package com.cfs.entity;

/**
 * 历史数据分析实体
 * @author zz
 *
 */
public class HistoricalData {

	String competitor;  //竞争对手信息
	String  customer ;  //客户名称
	String products ;   //供货产品
	String salesVolume; //销售额
	String competitiveEdge ; // 竞争优势
	String weakness ; //竞争劣势
	String remark; //备注
	
	
	public HistoricalData() {
		super();
		// TODO Auto-generated constructor stub
	}


	public HistoricalData(String competitor, String customer, String products, String salesVolume,
			String competitiveEdge, String weakness, String remark) {
		super();
		this.competitor = competitor;
		this.customer = customer;
		this.products = products;
		this.salesVolume = salesVolume;
		this.competitiveEdge = competitiveEdge;
		this.weakness = weakness;
		this.remark = remark;
	}


	public String getCompetitor() {
		return competitor;
	}


	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}


	public String getCustomer() {
		return customer;
	}


	public void setCustomer(String customer) {
		this.customer = customer;
	}


	public String getProducts() {
		return products;
	}


	public void setProducts(String products) {
		this.products = products;
	}


	public String getSalesVolume() {
		return salesVolume;
	}


	public void setSalesVolume(String salesVolume) {
		this.salesVolume = salesVolume;
	}


	public String getCompetitiveEdge() {
		return competitiveEdge;
	}


	public void setCompetitiveEdge(String competitiveEdge) {
		this.competitiveEdge = competitiveEdge;
	}


	public String getWeakness() {
		return weakness;
	}


	public void setWeakness(String weakness) {
		this.weakness = weakness;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
	
}
