package com.cfs.entity;


/**
 * 按钮框架 实体类
 * @author zz
 *
 */
public class ButtonFramworkework {
	String title ;
	String columns	;
	String buttons	;
	String searchs	;
	String search_buttons	;
	String js_code	;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getColumns() {
		return columns;
	}
	public void setColumns(String columns) {
		this.columns = columns;
	}
	public String getButtons() {
		return buttons;
	}
	public void setButtons(String buttons) {
		this.buttons = buttons;
	}
	public String getSearchs() {
		return searchs;
	}
	public void setSearchs(String searchs) {
		this.searchs = searchs;
	}
	public String getSearch_buttons() {
		return search_buttons;
	}
	public void setSearch_buttons(String search_buttons) {
		this.search_buttons = search_buttons;
	}
	public String getJs_code() {
		return js_code;
	}
	public void setJs_code(String js_code) {
		this.js_code = js_code;
	}
	
	public ButtonFramworkework() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ButtonFramworkework(String title, String columns, String buttons, String searchs, String search_buttons,
			String js_code) {
		super();
		this.title = title;
		this.columns = columns;
		this.buttons = buttons;
		this.searchs = searchs;
		this.search_buttons = search_buttons;
		this.js_code = js_code;
	}
	
	
}
