package com.tyteam.base.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Created by liuruijie on 2017/3/5.
 */
@Configuration
public class Cfg_View extends WebMvcConfigurerAdapter {
	
		//调用路径
		@Value("${myPath.rfPath}")
		private String rfPath;
		
		@Value("${myPath.riPath}")
		private String riPath;
		
		@Value("${myPath.rePath}")
		private String rePath;
		
		@Value("${myPath.rzPath}")
		private String rzPath;
	
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("login");
    }
    
    
    @Override  
    public void addResourceHandlers(ResourceHandlerRegistry registry) {  
        registry.addResourceHandler("/upfile/files/**").addResourceLocations(rfPath); 
        registry.addResourceHandler("/upfile/img/**").addResourceLocations(riPath); 
        registry.addResourceHandler("/upfile/excel/**").addResourceLocations(rePath); 
        registry.addResourceHandler("/upfile/zip/**").addResourceLocations(rzPath); 
        super.addResourceHandlers(registry);
    }  
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
//        ObjectMapper objectMapper = builder.build();
//        SimpleModule simpleModule = new SimpleModule();
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        objectMapper.registerModule(simpleModule);
//        objectMapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);// 忽略 transient 修饰的属性
//        converters.add(new MappingJackson2HttpMessageConverter(objectMapper));
//        super.configureMessageConverters(converters);
//    }
}
