package com.tyteam.base.config;

import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

@Configuration
@EnableJpaRepositories(
		basePackages= {"com.tyteam.apps.dao","com.shuohe.dao"},
		entityManagerFactoryRef = "entityManagerFactory")
@EnableTransactionManagement
public class HibernateJpaConfig implements TransactionManagementConfigurer{
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactoryBean;
	
	@Bean(name="sessionFactory")
	public LocalSessionFactoryBean factory() {
		LocalSessionFactoryBean factory = new LocalSessionFactoryBean();
		factory.setDataSource(dataSource);
		String[] packages= {"com.shuohe.entity","com.crm.entity", "com.maintenance","com.tyteam.apps.model","com.bstek.uflo.model"};
		factory.setPackagesToScan(packages);
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		hibernateProperties.setProperty("hibernate.show_sql", "false");
		hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "update");
		hibernateProperties.setProperty("hibernate.jdbc.batch_size", "25");
		hibernateProperties.setProperty("hibernate.format_sql", "true");
		hibernateProperties.setProperty("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
		hibernateProperties.setProperty("hibernate.implicit_naming_strategy", "org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyHbmImpl");
		hibernateProperties.setProperty("hibernate.current_session_context_class", "org.springframework.orm.hibernate5.SpringSessionContext");
		factory.setHibernateProperties(hibernateProperties);
		return factory;
	}
	
	
	// 创建事务管理器1
    @Bean(name = "txManager1")
    public PlatformTransactionManager txManager() {
    	HibernateTransactionManager HiTransactionManager = new HibernateTransactionManager();
    	HiTransactionManager.setDataSource(dataSource);
    	HiTransactionManager.setSessionFactory(factory().getObject());
        return HiTransactionManager;
    }

    // 创建事务管理器2
    @Bean(name = "transactionManager")
    public PlatformTransactionManager txManager2() {
    	JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(dataSource);
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
        return jpaTransactionManager;
    }
	
    // 实现接口 TransactionManagementConfigurer 方法，其返回值代表在拥有多个事务管理器的情况下默认使用的事务管理器,为兼容hibernate事务管理器，优先指定hibernate管理器来对事务管理
    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return txManager2();
    }
}
