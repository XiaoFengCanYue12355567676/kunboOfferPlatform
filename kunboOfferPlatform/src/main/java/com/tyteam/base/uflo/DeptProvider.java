package com.tyteam.base.uflo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.bstek.uflo.process.assign.impl.AbstractAssigneeProvider;
import com.bstek.uflo.service.IdentityService;
import com.shuohe.entity.system.user.Position;
import com.shuohe.entity.system.user.User;
import com.shuohe.service.system.user.PositionService;
import com.shuohe.service.system.user.UserService;

/**
 * @author Jacky.gao
 * @since 2013年8月20日
 */
@Component
public class DeptProvider extends AbstractAssigneeProvider {
	@Autowired
	private PositionService positionService;
	@Autowired
	private UserService userService;
	
	private boolean disabledDeptAssigneeProvider = false;
	public boolean isTree() {
		return false;
	}
	
	public String getName() {
		return "指定部门";
	}
	
	public void queryEntities(PageQuery<Entity> pageQuery, String parentId) {
		positionService.page(pageQuery);
		
	}
	
	public Collection<String> getUsers(String entityId,Context context,ProcessInstance processInstance) {
		List<User> user = userService.findByPositionId(Integer.valueOf(entityId));
		Collection<String> list = new ArrayList<String>();
		for(User u:user) {
			list.add(u.getName());
		}
		return list;
	}

	public boolean disable() {
		return disabledDeptAssigneeProvider;
	}
	public void setIdentityService(PositionService positionService) {
		this.positionService = positionService;
	}

	public boolean isDisabledDeptAssigneeProvider() {
		return disabledDeptAssigneeProvider;
	}

	public void setDisabledDeptAssigneeProvider(boolean disabledDeptAssigneeProvider) {
		this.disabledDeptAssigneeProvider = disabledDeptAssigneeProvider;
	}
}