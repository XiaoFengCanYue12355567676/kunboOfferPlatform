package com.tyteam.base.uflo.eventBean;

import org.springframework.stereotype.Component;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.handler.DecisionHandler;
@Component("positionJudge")
public class PositionJudge implements DecisionHandler {

	@Override
	public String handle(Context context, ProcessInstance processInstance) {
		String position = (String) context.getProcessService().getProcessVariable("position", processInstance);
		String strFlow = "司机";
		if(position.equals("总监")){
			strFlow = "总监";
		}
		// TODO Auto-generated method stub
		return strFlow;
	}

}
