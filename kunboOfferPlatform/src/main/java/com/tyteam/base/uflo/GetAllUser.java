package com.tyteam.base.uflo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.shuohe.entity.system.user.User;
import com.shuohe.service.system.user.UserService;
@Component("getAllUser")
public class GetAllUser implements AssignmentHandler {
	@Autowired
	private UserService userService;
	@Override
	public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {
		// TODO Auto-generated method stub
		List<User> listUser = userService.findAll();
		Collection<String> list = new ArrayList<String>();
		for(User user:listUser){
			list.add(user.getName());
		}
		return list;
	}

}
