package com.tyteam.base.uflo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import com.shuohe.entity.system.user.User;
import com.shuohe.service.system.user.PositionService;
import com.shuohe.service.system.user.UserService;

@Component
public class CustomAssignee implements AssigneeProvider{
	
	@Autowired
	public UserService userService;
	
	@Autowired
	public PositionService positionService;

	@Override
	public boolean isTree() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "所有用户";
	}

	@Override
	public void queryEntities(PageQuery<Entity> pageQuery, String parentId) {
		// TODO Auto-generated method stub
		userService.page(pageQuery);
//		identityService.userPageQuery(pageQuery);
	}

	@Override
	public Collection<String> getUsers(String entityId, Context context, ProcessInstance processInstance) {
		// TODO Auto-generated method stub
		Collection<String> list = new ArrayList<String>();
		User user  = userService.findById(Integer.valueOf(entityId));
		List<User> u = new ArrayList<User>();
		list.add(user.getName());
		return list;
	}

	@Override
	public boolean disable() {
		// TODO Auto-generated method stub
		return false;
	}

}
