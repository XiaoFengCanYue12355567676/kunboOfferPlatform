package com.tyteam.base.uflo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import com.bstek.uflo.env.EnvironmentProvider;
import com.shuohe.entity.system.user.User;

/** @author Deament
 * @date 2016/10/31 */
public class UfloEnvironmentProvider implements EnvironmentProvider {
		
	 	private SessionFactory sessionFactory;
	 	
	    private PlatformTransactionManager platformTransactionManager;
	    
	    @Autowired  
	    private  HttpServletRequest request;  
	    
	    public SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }
	 
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	 
	    public PlatformTransactionManager getPlatformTransactionManager() {
	        return platformTransactionManager;
	    }
	 
	    public void setPlatformTransactionManager(
	            PlatformTransactionManager platformTransactionManager) {
	        this.platformTransactionManager = platformTransactionManager;
	    }
	    public String getCategoryId() {
	        return null;
	    }
	    public String getLoginUser() {
	    	User user = (User) request.getSession().getAttribute("user");
	    	System.out.println(user.getName()+"获得人物名称！");
	        return user.getName();
	    }
	
}
