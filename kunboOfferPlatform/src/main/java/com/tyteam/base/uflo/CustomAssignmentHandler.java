package com.tyteam.base.uflo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.handler.AssignmentHandler;
import com.bstek.uflo.process.node.TaskNode;
import com.shuohe.entity.system.user.User;

@Component("getUp")
public class CustomAssignmentHandler implements AssignmentHandler{
	
	@Autowired  
    private  HttpServletRequest request;  
	//通过bean指定任务办理人（这里用作指定上级接收审批）
	@Override
	public Collection<String> handle(TaskNode taskNode, ProcessInstance processInstance, Context context) {
		User user = (User) request.getSession().getAttribute("user");
		// TODO Auto-generated method stub
		Collection<String> list = new ArrayList<String>();
		list.add(user.getName());
		return list;
	}

}
