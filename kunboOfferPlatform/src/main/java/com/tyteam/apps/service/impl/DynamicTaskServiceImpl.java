package com.tyteam.apps.service.impl;

import com.tyteam.apps.model.DynamicTask;
import com.tyteam.apps.dao.DynamicTaskDao;
import com.tyteam.apps.service.DynamicTaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务表 服务实现类
 * </p>
 *
 * @author liuyu
 * @since 2018-01-22
 */
@Service
public class DynamicTaskServiceImpl implements DynamicTaskService {

}
