package com.tyteam.apps.service;

import com.tyteam.apps.model.ProfFormRel;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * <p>
 * 流程-表单关联表 服务类
 * </p>
 *
 * @author liuyu
 * @since 2018-01-17
 */
public interface ProfFormRelService {
	Page<ProfFormRel> getPage(Pageable pageable);
	
	List<ProfFormRel> findAll();
	
	ProfFormRel findByformTableName(String formTableName);
	
	ProfFormRel findById(Long id);
	
	void deleteById(Long id);
	
	ProfFormRel insertOrUpdate(ProfFormRel profFormRel);
	
}
