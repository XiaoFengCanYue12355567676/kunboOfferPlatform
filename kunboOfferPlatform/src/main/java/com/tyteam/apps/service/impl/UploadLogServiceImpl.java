package com.tyteam.apps.service.impl;

import com.tyteam.apps.model.ProfFormRel;
import com.tyteam.apps.model.UploadLog;
import com.tyteam.apps.dao.UploadLogDao;
import com.tyteam.apps.service.UploadLogService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传文件记录表 服务实现类
 * </p>
 *
 * @author liuyu
 * @since 2018-01-25
 */
@Service
public class UploadLogServiceImpl implements UploadLogService {

	@Autowired
	UploadLogDao dao;
	
	@Override
	public List<UploadLog> findAll() {
		return dao.findAll();
	}
	@Override
	public Page<UploadLog> getPage(Pageable pageable) {
		Page<UploadLog> list =dao.getPage(pageable);
		return list;
	}
	@Override
	public void delete(Integer id) {
		dao.delete(id);
	}
	@Override
	public UploadLog update(UploadLog uploadLog) {
		return dao.saveAndFlush(uploadLog);
	}

}
