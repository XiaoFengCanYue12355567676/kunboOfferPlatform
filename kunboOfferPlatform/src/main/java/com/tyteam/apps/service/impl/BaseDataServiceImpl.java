package com.tyteam.apps.service.impl;

import com.tyteam.apps.model.BaseData;
import com.tyteam.apps.dao.BaseDataDao;
import com.tyteam.apps.service.BaseDataService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuyu
 * @since 2017-12-21
 */
@Service
public class BaseDataServiceImpl implements BaseDataService {

}
