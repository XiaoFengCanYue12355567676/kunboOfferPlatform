package com.tyteam.apps.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tyteam.apps.model.UploadLog;

/**
 * <p>
 * 上传文件记录表 服务类
 * </p>
 *
 * @author liuyu
 * @since 2018-01-25
 */
public interface UploadLogService {
	List<UploadLog> findAll();
	Page<UploadLog> getPage(Pageable pageable);
	void delete(Integer id);
	UploadLog update(UploadLog uploadLog);
}
