package com.tyteam.apps.service.impl;

import com.tyteam.apps.model.ProfFormRel;
import com.tyteam.apps.dao.ProfFormRelDao;
import com.tyteam.apps.service.ProfFormRelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 流程-表单关联表 服务实现类
 * </p>
 *
 * @author liuyu
 * @since 2018-01-17
 */
@Service
public class ProfFormRelServiceImpl implements ProfFormRelService {
	@Autowired
	private ProfFormRelDao profFormRelDao;
	
	@Override
	public Page<ProfFormRel> getPage(Pageable pageable) {
		Page<ProfFormRel> list = profFormRelDao.getPage(pageable);
		return list;
	}

	@Override
	public List<ProfFormRel> findAll() {
		List<ProfFormRel> list = profFormRelDao.findAll();
		return list;
	}

	@Override
	public ProfFormRel findByformTableName(String formTableName) {
		return profFormRelDao.findByformTableName(formTableName);
	}

	@Override
	public ProfFormRel findById(Long id) {
		return profFormRelDao.findOne(id);
	}

	@Override
	public void deleteById(Long id) {
		profFormRelDao.delete(id);;
	}

	@Override
	public ProfFormRel insertOrUpdate(ProfFormRel profFormRel) {
		return profFormRelDao.saveAndFlush(profFormRel);
	}

	

}
