package com.tyteam.apps.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;

import com.shuohe.util.easyuiBean.EasyUiDataGrid2;
import com.tyteam.apps.service.ProfFormRelService;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONString;
import com.tyteam.apps.common.JsonResult;
import com.tyteam.apps.model.ProfFormRel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *code is far away from bug with the animal protecting
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃ 　
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 *　　
 *   @description : ProfFormRel 控制器
 *   ---------------------------------
 *      @author liuyu
 *   @since 2018-01-17
 */
@RestController
@RequestMapping("/profFormRel")
public class ProfFormRelController {
    private final Logger logger = LoggerFactory.getLogger(ProfFormRelController.class);

    @Autowired
    public ProfFormRelService profFormRelService;
    
//    public ModelAndView mv = new ModelAndView(); 
    
    @GetMapping("")
    public ModelAndView index() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("/activiti/profFormRel/list");
    	return mv;
    }
    /**
     * @description : 获取分页列表
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @GetMapping("getProfFormRelList")
    public EasyUiDataGrid2 getProfFormRelList(HttpServletRequest request,HttpServletResponse response) {
    	String _page=request.getParameter("page");
		String _rows=request.getParameter("rows");
		int currentpage = Integer.parseInt(_page);
		int rows = Integer.parseInt(_rows);
		int firstResult=(currentpage-1)*rows;
		Pageable pageable = new PageRequest(firstResult,rows);
		Page<ProfFormRel> page = profFormRelService.getPage(pageable);
    	List<ProfFormRel> list = page.getContent();
    	long count = page.getTotalElements();
        return new EasyUiDataGrid2(count,list);
    }
    /**
     * 获取流程实体-表单关联表列表，下拉菜单用
     * @return
     */
    @PostMapping("getList")
    public JsonResult<ProfFormRel> getList() {
    	JsonResult result = new JsonResult();
    	List<ProfFormRel> list = profFormRelService.findAll();
    	result.setList(list);
    	result.setStatus("0");
    	return result;
    }
    
    /**
     * 获取流程实体-表单关联表列表，下拉菜单用
     * @return
     */
    @RequestMapping("getInfo")
    @ResponseBody
    public JsonResult<ProfFormRel> getInfo(String tableName) {
    	JsonResult result = new JsonResult();
    	ProfFormRel pfr = profFormRelService.findByformTableName(tableName);
    	result.setObj(pfr);
    	result.setStatus("0");
    	return result;
    }

    /**
     * @description : 通过id获取ProfFormRel
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @RequestMapping(value = "/getProfFormRelById",method = RequestMethod.GET)
    public JsonResult getProfFormRelById(String id) {
    	JsonResult result = new JsonResult();
            ProfFormRel pfr = profFormRelService.findById(Long.valueOf(id));
            result.setObj(pfr);
            result.setStatus("0");
        	return result;
    }

    /**
     * @description : 通过id删除ProfFormRel
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @RequestMapping("/deleteProfFormRelById")
    @ResponseBody
    public JsonResult deleteProfFormRelById(Long id) {
    	JsonResult result = new JsonResult();
            profFormRelService.deleteById(id);
            result.setStatus("0");
            result.setMsg("删除成功");
            return result;
    }

    /**
     * @description : 打开修改页面
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @RequestMapping(value = "/update/{id}",method = RequestMethod.GET)
    public ModelAndView toUpdate(@PathVariable("id")Long id) {
    	ModelAndView mv = new ModelAndView();
    	ProfFormRel param = profFormRelService.findById(id);
    	mv.addObject("old", param);
    	mv.setViewName("/activiti/profFormRel/edit");
    	return mv;
    }
    
    /**
     * @description : 通过id更新ProfFormRel
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @RequestMapping(value = "/updateProfFormRelById",method = RequestMethod.POST)
    public JsonResult updateProfFormRelById(String str) {
    	JsonResult result = new JsonResult();
    	JSONObject str1 = new JSONObject(str);
		ProfFormRel param = str1.toBean(ProfFormRel.class,false);
        profFormRelService.insertOrUpdate(param);
        result.setMsg("更新成功");
        result.setStatus("0");
        return result;
    }
    
    @RequestMapping("add")
    public ModelAndView add() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("/activiti/profFormRel/add");
    	return mv;
    }
    /**
     * @description : 添加ProfFormRel
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-17
     */
    @RequestMapping(value = "/addProfFormRel",method = RequestMethod.POST)
    @ResponseBody
    public JsonResult addProfFormRel(String str) {
    	JsonResult result = new JsonResult();
    		JSONObject str1 = new JSONObject(str);
    		ProfFormRel param = str1.toBean(ProfFormRel.class,false);
            profFormRelService.insertOrUpdate(param);
            result.setMsg("插入成功");
            result.setStatus("0");
            return result;
    }
    /**
     * 判断表单是否已经关联了流程模型
     * @param tabName
     * @return
     */
    @RequestMapping(value = "/getModelRelForm",method = RequestMethod.POST)
    @ResponseBody
    public JsonResult modelIfRelForm(String tabName) {
    	JsonResult result = new JsonResult();
    	ProfFormRel pfr = new ProfFormRel();
    	pfr.setFormTableName(tabName);
    	pfr = profFormRelService.findByformTableName(tabName);
    	if(null!=pfr){
    		result.setStatus("1");
    	}else {
    		result.setStatus("0");
    	}
    	return result;
    }
    
    /**
	 * 查询表单是否存在任务对应关系
	 * @param tableName
	 * @return
	 * @author liuyu
	 */
	@PostMapping("isAct")
	public String isAct(String tableName) {
		String isAct ="";
    	ProfFormRel pfr = new ProfFormRel();
    	pfr.setFormTableName(tableName);
    	ProfFormRel exist = profFormRelService.findByformTableName(tableName);
    	if(null!=exist) {
    		System.out.println(exist.toString());
    		isAct="0";
    	}else {
    		isAct="1";
    	}
		return isAct;
	}
    
}