package com.tyteam.apps.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.shuohe.entity.system.user.User;
import com.shuohe.util.easyuiBean.EasyUiDataGrid2;
import com.tyteam.apps.service.MailService;
import com.tyteam.apps.util.MailTool;

import cn.hutool.extra.mail.MailAccount;
import freemarker.template.Template;

import com.tyteam.apps.common.JsonResult;
import com.tyteam.apps.model.Mail;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *code is far away from bug with the animal protecting
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃ 　
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃神兽保佑
 *　　┃　　　┃代码无BUG！
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　　　┗┻┛　┗┻┛
 *　　
 *   @description : Mail 控制器
 *   ---------------------------------
 *      @author liuyu
 *   @since 2018-01-26
 */
//@RestController
//@RequestMapping("/mail")
public class MailController{
/*    private final Logger logger = LoggerFactory.getLogger(MailController.class);
    
    @Autowired
    public MailService mailService;

    *//**
     * @description : 获取分页列表
     * ---------------------------------
     * @author : liuyu
     * @since : Create in 2018-01-26
     *//*
    @RequestMapping("")
    public ModelAndView index() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("mailManager/list");
    	return mv;
    }
    @RequestMapping("getMailList")
    public EasyUiDataGrid2 getMailList(HttpServletRequest request, HttpServletResponse response ) {
    	User user = (User) request.getSession().getAttribute("user");
		String activitiUUID = user.getActiviti_uuid();
		String _page = request.getParameter("page");
		String _rows = request.getParameter("rows");
		int currentpage = Integer.parseInt(_page);
		int rows = Integer.parseInt(_rows);
		Page<Mail> page = new Page(currentpage, rows, "send_time",false);
		page = mailService.selectPage(page);
		List<Mail> list = page.getRecords();
		long count = list.size();
		return new EasyUiDataGrid2(count, list);
    }
    
    @RequestMapping("add")
    public ModelAndView add() {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("mailManager/add");
    	return mv;
    }
    
    @RequestMapping("sendMail")
    @ResponseBody
    public JsonResult senMail(Mail mail,String mailType) {
    	JsonResult result = new JsonResult();
    	MailTool tool = new MailTool();
    	MailAccount acc = new MailAccount();
    	acc.setUser("liudashi2006@163.com");
    	acc.setPass("liuyu2006");
    	acc.setFrom(mail.getSendBy());
    	acc.setAuth(true);
    	acc.setHost("smtp.163.com");
    	acc.setPort(25);
    	acc.setStartttlsEnable(false);
    	if("template".equals(mailType)) {
    		tool.sendTemplateMail(acc, mail);
    		mailService.insert(mail);
    		result.setMsg("邮件发送成功");
    		result.setStatus("0");
    	}else {
    		tool.sendSimpleMail(acc, mail);
    		result.setMsg("邮件发送成功");
    		result.setStatus("0");
    	}
    	return result;
    }*/
}