package com.tyteam.apps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * <p>
 * 上传文件记录表
 * </p>
 *
 * @author liuyu
 * @since 2018-01-25
 */
@Entity
@Table(name="upload_log")
@Data
public class UploadLog {


    /**
     * 文件编号
     */
	@Id 
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    /**
     * 文件名称
     */
	@Column(name="file_name")
	private String fileName;
    /**
     * 文件类型
     */
	@Column(name="file_type")
	private String fileType;
    /**
     * 文件路径
     */
	@Column(name="file_path")
	private String filePath;
	/**
     * 文件真实路径
     */
	@Column(name="file_real_path")
	private String fileRealPath;
    /**
     * 上传时间
     */
	@Column(name="upload_time")
	private Date uploadTime;
    /**
     * 上传者
     */
	@Column(name="upload_by")
	private Integer uploadBy;


//	public Integer getId() {
//		return id;
//	}
//
//	public UploadLog setId(Integer id) {
//		this.id = id;
//		return this;
//	}
//
//	public String getFileName() {
//		return fileName;
//	}
//
//	public UploadLog setFileName(String fileName) {
//		this.fileName = fileName;
//		return this;
//	}
//
//	public String getFileType() {
//		return fileType;
//	}
//
//	public UploadLog setFileType(String fileType) {
//		this.fileType = fileType;
//		return this;
//	}
//
//	public String getFilePath() {
//		return filePath;
//	}
//
//	public UploadLog setFilePath(String filePath) {
//		this.filePath = filePath;
//		return this;
//	}
//
//	public String getFileRealPath() {
//		return fileRealPath;
//	}
//
//	public void setFileRealPath(String fileRealPath) {
//		this.fileRealPath = fileRealPath;
//	}
//
//	public Date getUploadTime() {
//		return uploadTime;
//	}
//
//	public UploadLog setUploadTime(Date uploadTime) {
//		this.uploadTime = uploadTime;
//		return this;
//	}
//
//	public Integer getUploadBy() {
//		return uploadBy;
//	}
//
//	public UploadLog setUploadBy(Integer uploadBy) {
//		this.uploadBy = uploadBy;
//		return this;
//	}
//
//	@Override
//	protected Serializable pkVal() {
//		return this.id;
//	}
//
//	@Override
//	public String toString() {
//		return "UploadLog{" +
//			", id=" + id +
//			", fileName=" + fileName +
//			", fileType=" + fileType +
//			", filePath=" + filePath +
//			", uploadTime=" + uploadTime +
//			", uploadBy=" + uploadBy +
//			"}";
//	}
}
