package com.tyteam.apps.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;

import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author liuyu
 * @since 2017-12-21
 */
@Entity
@Table(name="base_data")
@Data
public class BaseData{

	@Id 
	@Column(name="id")
	private String id;
    /**
     * 字段名称
     */
	@Column(name="item_name")
	private String itemName;
    /**
     * 字段值
     */
	@Column(name="item_value")
	private String itemValue;
    /**
     * 字段附加值
     */
	@Column(name="item_addtional")
	private String itemAddtional;


//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public String getItemName() {
//		return itemName;
//	}
//
//	public void setItemName(String itemName) {
//		this.itemName = itemName;
//	}
//
//	public String getItemValue() {
//		return itemValue;
//	}
//
//	public void setItemValue(String itemValue) {
//		this.itemValue = itemValue;
//	}
//
//	public String getItemAddtional() {
//		return itemAddtional;
//	}
//
//	public void setItemAddtional(String itemAddtional) {
//		this.itemAddtional = itemAddtional;
//	}
//
//	@Override
//	protected Serializable pkVal() {
//		return this.id;
//	}
//
//	@Override
//	public String toString() {
//		return "BaseData{" +
//			", id=" + id +
//			", itemName=" + itemName +
//			", itemValue=" + itemValue +
//			", itemAddtional=" + itemAddtional +
//			"}";
//	}
}
