package com.tyteam.apps.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * <p>
 * 流程-表单关联表
 * </p>
 *
 * @author liuyu
 * @since 2018-01-17
 */
@Entity
@Table(name="prof_form_rel")
@Data
public class ProfFormRel{

    /**
     * 唯一id
     */
    @Id 
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    /**
     * 流程关键字
     */
    @Column(name="pro_def_key")
	private String proDefKey;
    /**
     * 表单关联表名
     */
    @Column(name="form_table_name")
	private String formTableName;
    /**
     * 表单地址
     */
    @Column(name="form_url")
	private String formUrl;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProDefKey() {
		return proDefKey;
	}

	public void setProDefKey(String proDefKey) {
		this.proDefKey = proDefKey;
	}

	public String getFormTableName() {
		return formTableName;
	}

	public void setFormTableName(String formTableName) {
		this.formTableName = formTableName;
	}

	public String getFormUrl() {
		return formUrl;
	}

	public void setFormUrl(String formUrl) {
		this.formUrl = formUrl;
	}
	
/*	public ProfFormRel() {
    }
	
	public ProfFormRel(Long id, String proDefKey, String formTableName, String formUrl) {
        this.id = id;
        this.proDefKey = proDefKey;
        this.formTableName = formTableName;
        this.formUrl = formUrl;
    }
	
	@Override
	public String toString() {
		return "ProfFormRel{" +
			", id=" + id +
			", proDefKey=" + proDefKey +
			", formTableName=" + formTableName +
			", formUrl=" + formUrl +
			"}";
	}*/
}
