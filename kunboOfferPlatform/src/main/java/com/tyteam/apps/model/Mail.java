package com.tyteam.apps.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuyu
 * @since 2018-01-26
 */
@Entity
@Table(name="mail")
@Data
public class Mail{

    /**
     * 邮件编号
     */
    @Id 
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    /**
     * 邮件主题
     */
    @Column(name="subject")
	private String subject;
    /**
     * 邮件内容
     */
    @Column(name="content")
	private String content;
    /**
     * 发送给
     */
    @Column(name="send_to")
	private String sendTo;
    /**
     * 抄送
     */
    @Column(name="carbon_copy")
	private String carbonCopy;
    /**
     * 密送人
     */
    @Column(name="b_carbon_copy")
	private String bCarbonCopy;
    /**
     * 附件
     */
    @Column(name="attachment")
	private String attachment;
    /**
     * 发送时间
     */
    @Column(name="send_time")
    @Temporal(TemporalType.DATE) 
	private Date sendTime;
    /**
     * 发送者
     */
    @Column(name="send_by")
	private String sendBy;


//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getSubject() {
//		return subject;
//	}
//
//	public void setSubject(String subject) {
//		this.subject = subject;
//	}
//
//	public String getContent() {
//		return content;
//	}
//
//	public void setContent(String content) {
//		this.content = content;
//	}
//
//	public String getTo() {
//		return to;
//	}
//
//	public void setTo(String to) {
//		this.to = to;
//	}
//
//	public String getCarbonCopy() {
//		return carbonCopy;
//	}
//
//	public void setCarbonCopy(String carbonCopy) {
//		this.carbonCopy = carbonCopy;
//	}
//
//	public String getbCarbonCopy() {
//		return bCarbonCopy;
//	}
//
//	public void setbCarbonCopy(String bCarbonCopy) {
//		this.bCarbonCopy = bCarbonCopy;
//	}
//
//	public String getAttachment() {
//		return attachment;
//	}
//
//	public void setAttachment(String attachment) {
//		this.attachment = attachment;
//	}
//
//	public Date getSendTime() {
//		return sendTime;
//	}
//
//	public void setSendTime(Date sendTime) {
//		this.sendTime = sendTime;
//	}
//
//	public String getSendBy() {
//		return sendBy;
//	}
//
//	public void setSendBy(String sendBy) {
//		this.sendBy = sendBy;
//	}
//
//	@Override
//	protected Serializable pkVal() {
//		return this.id;
//	}
//
//	@Override
//	public String toString() {
//		return "Mail{" +
//			", id=" + id +
//			", subject=" + subject +
//			", content=" + content +
//			", to=" + to +
//			", carbonCopy=" + carbonCopy +
//			", bCarbonCopy=" + bCarbonCopy +
//			", attachment=" + attachment +
//			", sendTime=" + sendTime +
//			", sendBy=" + sendBy +
//			"}";
//	}
}
