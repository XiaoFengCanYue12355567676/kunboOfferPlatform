package com.tyteam.apps.model;


import lombok.Data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * <p>
 * 定时任务表
 * </p>
 *
 * @author liuyu
 * @since 2018-01-24
 */
@Entity
@Table(name="dynamic_task")
@Data
public class DynamicTask{


    /**
     * 任务id
     */
	@Id 
	@Column(name="id")
	private String id;
    /**
     * 任务名称
     */
	@Column(name="task_name")
	private String taskName;
    /**
     * 任务执行参数
     */
	@Column(name="scheduling_pattern")
	private String schedulingPattern;
    /**
     * 任务分组
     */
	@Column(name="task_group")
	private String taskGroup;
    /**
     * 调用类名称
     */
	@Column(name="class_name")
	private String className;
    /**
     * 任务状态
     */
	@Column(name="state")
	private String state;
    /**
     * 补偿规则
     */
	@Column(name="miss_rule")
	private String missRule;
    /**
     * 任务描述
     */
	@Column(name="description")
	private String description;
    /**
     * 任务创建时间
     */
	@Column(name="create_time")
	private Date createTime;
    /**
     * 任务创建者
     */
	@Column(name="create_by")
	private Integer createBy;
    /**
     * 修改时间
     */
	@Column(name="update_time")
	private Date updateTime;
    /**
     * 修改者
     */
	@Column(name="update_by")
	private Integer updateBy;
    /**
     * 删除标志
     */
	@Column(name="delete_flag")
	private String deleteFlag;


//	public String getId() {
//		return id;
//	}
//
//	public DynamicTask setId(String id) {
//		this.id = id;
//		return this;
//	}
//
//	public String getTaskName() {
//		return taskName;
//	}
//
//	public DynamicTask setTaskName(String taskName) {
//		this.taskName = taskName;
//		return this;
//	}
//
//	public String getSchedulingPattern() {
//		return schedulingPattern;
//	}
//
//	public DynamicTask setSchedulingPattern(String schedulingPattern) {
//		this.schedulingPattern = schedulingPattern;
//		return this;
//	}
//
//	public String getTaskGroup() {
//		return taskGroup;
//	}
//
//	public DynamicTask setTaskGroup(String taskGroup) {
//		this.taskGroup = taskGroup;
//		return this;
//	}
//
//	public String getClassName() {
//		return className;
//	}
//
//	public DynamicTask setClassName(String className) {
//		this.className = className;
//		return this;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public DynamicTask setState(String state) {
//		this.state = state;
//		return this;
//	}
//
//	public String getMissRule() {
//		return missRule;
//	}
//
//	public DynamicTask setMissRule(String missRule) {
//		this.missRule = missRule;
//		return this;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public DynamicTask setDescription(String description) {
//		this.description = description;
//		return this;
//	}
//
//	public Date getCreateTime() {
//		return createTime;
//	}
//
//	public DynamicTask setCreateTime(Date createTime) {
//		this.createTime = createTime;
//		return this;
//	}
//
//	public Integer getCreateBy() {
//		return createBy;
//	}
//
//	public DynamicTask setCreateBy(Integer createBy) {
//		this.createBy = createBy;
//		return this;
//	}
//
//	public Date getUpdateTime() {
//		return updateTime;
//	}
//
//	public DynamicTask setUpdateTime(Date updateTime) {
//		this.updateTime = updateTime;
//		return this;
//	}
//
//	public Integer getUpdateBy() {
//		return updateBy;
//	}
//
//	public DynamicTask setUpdateBy(Integer updateBy) {
//		this.updateBy = updateBy;
//		return this;
//	}
//
//	public String getDeleteFlag() {
//		return deleteFlag;
//	}
//
//	public DynamicTask setDeleteFlag(String deleteFlag) {
//		this.deleteFlag = deleteFlag;
//		return this;
//	}
//
//	@Override
//	protected Serializable pkVal() {
//		return this.id;
//	}
//
//	@Override
//	public String toString() {
//		return "DynamicTask{" +
//			", id=" + id +
//			", taskName=" + taskName +
//			", schedulingPattern=" + schedulingPattern +
//			", taskGroup=" + taskGroup +
//			", className=" + className +
//			", state=" + state +
//			", missRule=" + missRule +
//			", description=" + description +
//			", createTime=" + createTime +
//			", createBy=" + createBy +
//			", updateTime=" + updateTime +
//			", updateBy=" + updateBy +
//			", deleteFlag=" + deleteFlag +
//			"}";
//	}
}
