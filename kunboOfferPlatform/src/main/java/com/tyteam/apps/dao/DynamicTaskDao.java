package com.tyteam.apps.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tyteam.apps.model.DynamicTask;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2018-01-22
 */
public interface DynamicTaskDao extends JpaRepository<DynamicTask, String> {

}
