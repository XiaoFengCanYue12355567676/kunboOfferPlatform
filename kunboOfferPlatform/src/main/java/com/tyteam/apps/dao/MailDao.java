package com.tyteam.apps.dao;

import com.tyteam.apps.model.Mail;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2018-01-26
 */
public interface MailDao extends JpaRepository<Mail, Long>{

}