package com.tyteam.apps.dao;

import com.tyteam.apps.model.UploadLog;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 * <p>
 * 上传文件记录表 Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2018-01-25
 */
public interface UploadLogDao extends JpaRepository<UploadLog, Integer>{
	
	@Query( value = "from UploadLog", countQuery = "select count(*) from UploadLog" )  
	Page<UploadLog> getPage(Pageable pageable);
}
