package com.tyteam.apps.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tyteam.apps.model.BaseData;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2017-12-21
 */
public interface BaseDataDao extends JpaRepository<BaseData, String>{

}
