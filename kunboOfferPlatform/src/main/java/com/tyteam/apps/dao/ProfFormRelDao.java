package com.tyteam.apps.dao;

import com.tyteam.apps.model.ProfFormRel;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 * <p>
 * 流程-表单关联表 Mapper 接口
 * </p>
 *
 * @author liuyu
 * @since 2018-01-17
 */
public interface ProfFormRelDao extends JpaRepository<ProfFormRel, Long> {
	
	@Query( value = "from ProfFormRel", countQuery = "select count(*) from ProfFormRel" )  
	Page<ProfFormRel> getPage(Pageable pageable);
	
	ProfFormRel findByformTableName(String formTableName);
	
}

