package com.tyteam.apps.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.tyteam.apps.model.Mail;

import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.utility.StringUtil;
public class MailTool {
	private MailUtil util;
	
	@Autowired 
	private FreeMarkerConfigurer freeMarkerConfigurer; // 自动注入;
	
	@SuppressWarnings("static-access")
	public Integer sendSimpleMail(MailAccount mailAccount,Mail mail) {
		Integer result =null;
		File[] file = null;
		if(!"".equals(mail.getAttachment())&&null!=mail.getAttachment()) {
			String[] temp = mail.getAttachment().split(",");
			file= new File[temp.length];
			for(int i = 0;i<temp.length;i++) {
				File tempfile = new File(temp[i]);
				file[i]=tempfile;
			}
		}
		try {
			util.send(mailAccount, castToCollection(mail.getSendTo()),castToCollection(mail.getCarbonCopy()),castToCollection(mail.getBCarbonCopy()), mail.getSubject(), mail.getContent(), false, file);
			result=0;
		}catch(Exception e) {
			e.printStackTrace();
			result=1;
		}
		return result;
	}
	
	public Integer sendTemplateMail(MailAccount mailAccount,Mail mail) {
		Integer result =null;
		File[] file = null;
		if(!"".equals(mail.getAttachment())&&null!=mail.getAttachment()) {
			String[] temp = mail.getAttachment().split(",");
			file= new File[temp.length];
			for(int i = 0;i<temp.length;i++) {
				File tempfile = new File(temp[i]);
				file[i]=tempfile;
			}
		}
		try {
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("content", mail.getContent());
			Configuration configuration = new Configuration();
	        TemplateLoader c1 = new ClassTemplateLoader(TemplateLoader.class,"/templates/mail");
	        configuration.setTemplateLoader(c1);
	        Template template = configuration.getTemplate("simple.ftl");
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
			util.send(mailAccount, castToCollection(mail.getSendTo()),castToCollection(mail.getCarbonCopy()),castToCollection(mail.getBCarbonCopy()), mail.getSubject(), mail.getContent(), false, file);
			result=0;
		}catch(Exception e) {
			e.printStackTrace();
			result=1;
		}
		return result;
	}
	
	private Collection<String> castToCollection(String addr){
		Collection<String> addrs = new ArrayList<String>();
		if(!"".equals(addr)&&null!=addr) {
			String[] temp = addr.split(",");
			for(String add:temp) {
				addrs.add(add);
			}
		}
		return addrs;
	}
}
