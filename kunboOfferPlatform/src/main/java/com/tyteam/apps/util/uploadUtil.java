package com.tyteam.apps.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.tyteam.apps.model.UploadLog;


public class uploadUtil {
	
	private final static Logger log = Logger.getLogger(uploadUtil.class.getName());
	//单个文件上传
	public static UploadLog upload(MultipartFile file,String path,String sidePath) {
		UploadLog up = new UploadLog();
		if(file.isEmpty()) {
			return up;
		}
		String fileName = file.getOriginalFilename();
		if(fileName.equals("")) {
			// 如果没有传入自定义文件名，则自动获取文件名称
	        fileName = file.getOriginalFilename();
		}
        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        // 文件上传后的路径
        // 解决中文问题，liunx下中文路径，图片显示问题
        fileName = UUID.randomUUID() + fileName;
        File dest = new File(path + fileName);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
            
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("调用路径为"+sidePath + fileName);
        up.setFileName(file.getOriginalFilename());
        up.setFileType(suffixName);
        up.setFilePath(sidePath + fileName);
        up.setFileRealPath(path + fileName);
        return up;
    }
}
