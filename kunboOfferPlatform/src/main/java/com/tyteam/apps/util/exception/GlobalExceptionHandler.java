package com.tyteam.apps.util.exception;

import com.shuohe.util.returnBean.ReturnBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import com.tyteam.apps.util.Status;
import com.tyteam.apps.util.ToWeb;

import java.sql.SQLException;

/**
 * Created by liuruijie on 2016/12/28.
 * 全局异常处理，捕获所有Controller中抛出的异常。
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	//处理自定义的异常
	@ExceptionHandler(BaseException.class)
	public Object customHandler(BaseException e){
//		e.printStackTrace();
		return ToWeb.buildResult().status(e.getCode()).msg(e.getMessage());
	}

	//其他未处理的异常
	@ExceptionHandler(Exception.class)
	public Object exceptionHandler(Exception e){
		log.error(e.getMessage(),e);
		return ToWeb.buildResult().status(Status.FAIL).msg("系统错误");
	}

//	@ExceptionHandler(value = BizException.class)
//	public Object bizErrorHandler(BizException ex) {
//		return ToWeb.buildResult().status(Status.FAIL).msg(ex.getMessage());
//	}
	@ExceptionHandler(value = SQLException.class)
	public Object sqlErrorHandler(SQLException ex) {
		log.error(ex.getMessage(),ex);
		return ToWeb.buildResult().status(Status.FAIL).msg("数据库错误");
	}
	@ExceptionHandler(value = EmptyResultDataAccessException.class)
	public ReturnBean emptyResultDataAccessExceptionHandler(EmptyResultDataAccessException ex) {
		return ReturnBean.genReturnbean("没有指定的数据");
	}
}
