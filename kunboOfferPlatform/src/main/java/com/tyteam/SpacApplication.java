package com.tyteam;


import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@ComponentScan({"com.shuohe.controller","com.shuohe.service","com.tyteam","com.cfs"
	,"com.pm2.controller","com.pm2.service","com.crm.service","com.maintenance","com.office.knowledge.controller","com.office.knowledge.service","com.plugin","com.kunbo.offer","com.kunbo.shop"
	,"com.kunbo.balance","com.kunbo.encode"})
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class,
        FreeMarkerAutoConfiguration.class
})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories({"com.shuohe.dao","com.tyteam.apps.dao","com.kunbo.offer.dao","com.kunbo.shop.dao","com.kunbo.balance.dao","com.kunbo.encode.dao"}) // JPA扫描该包路径下的Repositorie
@EntityScan({"com.shuohe.entity","com.tyteam.apps.model","com.kunbo.offer.entity","com.kunbo.shop.entity","com.kunbo.balance.entity","com.kunbo.encode.entity"}) // 扫描实体类
@MapperScan({"com.maintenance"}) //扫描mybatis mapper
@SpringBootApplication
@EnableScheduling
@EnableAsync
@ServletComponentScan//扫描配置的servlet
@EnableJpaAuditing//开启数据修改的时间注解
public class SpacApplication {

	private Logger logger = LoggerFactory.getLogger(SpacApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SpacApplication.class, args);
		System.out.println("项目启动成功！！！！！！！！！！1");
	}
	
//	@Bean
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		logger.info("********************* configureMessageConverters init ... *********************"); 
//        converters.add(new StringHttpMessageConverter());
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
//        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//        converter.setObjectMapper(objectMapper);
//        converter.setSupportedMediaTypes(new ArrayList<MediaType>() {
//            {
//                add(MediaType.APPLICATION_JSON_UTF8);
//            }
//        });
//        converter.setDefaultCharset(Charset.forName("UTF-8"));
//        converters.add(converter);
//    }
	
	
//	@Bean
//	public HttpMessageConverters fastJsonHttpMessageConverters(){
//		logger.info("********************* fastJsonHttpMessageConverters init ... *********************");
//	    //1.需要定义一个convert转换消息的对象;
//	    FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
//	    //2:添加fastJson的配置信息;
//	    FastJsonConfig fastJsonConfig = new FastJsonConfig();
//	    fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
//	    fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
//	    //3处理中文乱码问题
//	    List<MediaType> fastMediaTypes = new ArrayList<>();
//	    fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
//	    //4.在convert中添加配置信息.
//	    fastJsonHttpMessageConverter.setSupportedMediaTypes(fastMediaTypes);
//	    fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
//	    HttpMessageConverter<?> converter = fastJsonHttpMessageConverter;
//	    return new HttpMessageConverters(converter); 
//	}
//	
//	@Bean
//	public ServletRegistrationBean konwledgeFileServlet() {
//		return new ServletRegistrationBean(new StreamServlet(), "/upload");
//	}
}
 