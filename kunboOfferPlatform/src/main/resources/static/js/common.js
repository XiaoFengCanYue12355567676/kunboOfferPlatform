//获取根目录
function getRootPath() {
	var scripts = document.getElementsByTagName("script");
	var js = ""
	var currentPath = "/js/common.js";
	for(var i = 0; i < scripts.length; i++) {
		if(scripts[i].src.indexOf(currentPath) > 0) {
			var hostlength = window.document.location.protocol.length + 2;
			hostlength += window.document.location.hostname.length;
			var port = window.document.location.port; 
			if(port != 80 && port != "") {
				hostlength += 1 + window.document.location.port.length;
			}
			js = scripts[i].src.substring(hostlength);
		
			return js.replace(currentPath, "");
		} else {
			continue;
		}
	}
}
function getBaseData(id,itemId,itemvalue,defValue){
		$.ajax({
            url:"/baseData/getDrop",
            type:"post",
            data:{
            	id:itemId,
            	value:itemvalue
            },
            success:function(result){
                if(result.status==0){
                	var op = '';
                	$.each(result.list, function (i, group) { 
                		op+='<option value="'+result.list[i].itemValue+'">'+result.list[i].itemName+'</option>';
                	});
                	$(''+id+'').append(op);
                }
                else{
                	layer.msg("下拉框获取失败");
                }
            },
            error:function(e){
                alert("错误！！");
            }
        });        
}
function getProKeyList(id){
	$.ajax({
        url: "/deployments/getKeyList",
				type : "POST",
				dataType : 'json',
				success:function(result){
	                if(result.status==0){
	                	var op = '';
	                	$.each(result.list, function (i, group) { 
	                		op+='<option value="'+result.list[i]+'">'+result.list[i]+'</option>';
	                	});
	                	$(''+id+'').append(op);
	                }
	                else{
	                	layer.msg("下拉框获取失败");
	                }
	            }
			});
}
/**去除两边空格!
 * @param val
 * */
function trim(val){
  //JQuery
  var trimRegExp = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
  if (val) {
	  return val.replace(trimRegExp,"");
  }
  return "";
}

/**去除两边空格!
 * @param val
 * */
function trimToNull(val){
  //JQuery
  var ret = trim(val);
  return ret == '' ? null : ret;
}
/**
 * 去除字符串第一个和最后一个字符
 * @param val
 * @returns
 */
function trimFN(val){
	var len = val.length;//str为要处理的字符串
	result = val.substring(1,len-1);//result为你需要的字符串
	return result;
}
/**
 * 弹出消息框。
 * 
 * @param msg 消息。
 */
function alertMsg(msg, yes)
{
	layer.alert(msg,{
		icon:0
	},
	yes);
}

/**
 * 弹出确认框。
 * 
 * @param msg 消息。
 * @param fun 确定后执行的方法。
 */
function confirmMsg(msg, yes, cancel)
{
	layer.confirm(msg, {
		icon:3,
		btn: ['确定','取消'] //按钮
	}, yes, cancel);
}
//去除字符串中所有html标签，保留文字，并截取前40个字符，之后用。。。显示
function expHtmlTab(str){
	var con ="";
	var con=str.replace(/<\/?.+?>/g,"");
	var con=con.replace(/ /g,"");//dds为得到后的内容
	con=con.substring(0,40)+"......"; 
	return con;
}
//去除字符串中所有html标签，保留文字，并截取前40个字符，之后用。。。显示
function expHtmlTabAtFront(str){
	var con ="";
	var con=str.replace(/<\/?.+?>/g,"");
	var con=con.replace(/ /g,"");//dds为得到后的内容
	con="......"+con.substring(0,40); 
	return con;
}
function getyyyyMMdd(ttDate){
	var date = ttDate.replace(/(\d{4}).(\d{1,2}).(\d{1,2}).+/mg, '$1-$2-$3');
	return date;
}    
