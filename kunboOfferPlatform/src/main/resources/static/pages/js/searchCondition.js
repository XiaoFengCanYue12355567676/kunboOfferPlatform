var CrmLang = {
  "GT":'大于',
	"LT":'小于',
	"EQ":'等于',
	"NEQ":'不等于',
  'CONTAINS':'包含',
  'NOT_CONTAIN':'不包含',
  'IS':'是',
  'ISNOT':'否',
  'START_WITH':'开始字符',
  'END_WITH':'结束字符',
  'IS_EMPTY':'为空',
  'IS_NOT_EMPTY':'不为空',
  'BEHIND':'晚于',
  'BEFORE':'早于',
  'EXIST':'在',
  'ABSENT':'不在',
  'MAN':'男',
  'WOMAN':'女',
  'LADY':'女士',
  'SIR':'先生',
  'TEACHER':'老师',
  'DOCTOR':'医生',
  'DOCTORS':'博士',
  'PROFESSOR':'教授',
  'TRY_TO_CONTACT':'试图联系',
  'CONTACT_IN_THE_FUTURE':'将来联系',
  'ALREADY_CONTACTED':'已联系',
  'FALSE_CLUES':'虚假线索',
  'MISSING_CLUES':'丢失线索',
  'WUXING':'五星',
  'SIXING':'四星',
  'SANXING':'三星',
  'ERXING':'二星',
  'YIXING':'一星',
  'NOT_STARTED':'未启动',
  'RETARDATION':'推迟',
  'UNDERWAY':'进行中',
  'COMPLETED':'已完成',
  'HIGH':'高',
  'GENERAL':'普通',
  'LOW':'低',
  'NOT_PAYING':'未付款',
  'PART_OF_THE_PREPAID':'部分已付',
  'ACCOUNT_PAID':'已付款',
  'NOT_CHECK':'未结账',
  'NOT_RECEIVE_PAYMENT':'未收款',
  'PART_OF_THE_RECEIVED':'部分已收',
  'HAS_BEEN_RECEIVING':'已收款',
  'HAS_THE_INVOICING':'已结账',
  'ACCOUNT_PAID':'已付款',
  'NO':'无',
  'STREET_INFORMATION':'街道信息',
  'SELECT_REGION':'请选择地区',
  'FILL_IN_THE_SEARCH_CONTENT':'请填写搜索内容',
  'SELECT_FILTER_CONDITION':'请选择筛选条件！',
  'STATE_OWNED_ENTERPRISES':'国有企业!',
  'FOREIGN_CAPITAL_ENTERPRISE':'外资企业!',
  'PRIVATE_ENTERPRISE':'民营企业!',
  'COLLECTIVE_ENTERPRISE':'集体企业',
  'JOINT_STOCK_COMPANY':'股份制企业',
  'JOINT_VENTURE':'合资企业',
  'SOLE_PROPRIETORSHIP_ENTERPRISE':'独资企业',
  'OTHER':'其他',
  'ANALYSTS':'分析者',
  'COMPETITOR':'竞争者',
  'CUSTOMER':'客户',
  'INTEGRATORS':'集成商',
  'INVESTORS':'投资商',
  'PARTNERS':'合作伙伴',
  'PUBLISHERS':'出版商',
  'TARGET':'目标',
  'SUPPLIER':'供应商',
  'NOT_OUT_OF_WAREHOUSE':'未出库',
  'HAVE_OUT_OF_WAREHOUSE':'已出库',
  'NOT_ENTER_WAREHOUSE':'未入库',
  'HAVE_ENTER_WAREHOUSE':'已入库',
	'CONFIRM_DELETE':'确定要删除么？'
}
function changeCondition () {
  var searchType = $('#field option:selected').attr('class')
  var searchVal = $('#field option:selected').val()
  if (searchType === 'field-text') {
    $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
			+'<option value="contains">'+CrmLang.CONTAINS+'</option>'
			+'<option value="not_contain">'+CrmLang.NOT_CONTAIN+'</option>'
			+'<option value="is">'+CrmLang.IS+'</option>'
			+'<option value="isnot">'+CrmLang.ISNOT+'</option>'
			+'<option value="start_with">'+CrmLang.START_WITH+'</option>'
			+'<option value="end_with">'+CrmLang.END_WITH+'</option>'
			+'<option value="is_empty">'+CrmLang.IS_EMPTY+'</option>'
			+'<option value="is_not_empty">'+CrmLang.IS_NOT_EMPTY+'</option>'
      +'</select>')
    $('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>')
  } else if (searchType === 'field-number') {
    $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
      +'<option value="gt">'+CrmLang.GT+'</option>'
      +'<option value="lt">'+CrmLang.LT+'</option>'
      +'<option value="eq">'+CrmLang.EQ+'</option>'
      +'<option value="neq">'+CrmLang.NEQ+'</option>'
      +'</select>')
    $('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>')
  } else if (searchType === 'field-date') {
    $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
      +'<option value="tgt">  '+CrmLang.BEHIND+'  </option>'
			+'<option value="lt">  '+CrmLang.BEFORE+'  </option>'
			+'<option value="between">  '+CrmLang.EXIST+'  </option>'
			+'<option value="nbetween">  '+CrmLang.ABSENT+'  </option>'
			+'</select>')
      $('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>');
      $('#search').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'zh-CN',
        minView: 'month'
      })
  } else if (searchType === 'field-select') {
    $('#searchContent').html('')
  } else if (searchType === 'field-boolean') {
    $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
      +'<option value="1">'+CrmLang.IS+'</option>'
			+'<option value="0">'+CrmLang.ISNOT+'</option>'
			+'</select>')
    $('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>')
  } else if (searchType === 'field-sex') {
   $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
     +'<option value="1">'+CrmLang.MAN+'</option>'
     +'<option value="0">'+CrmLang.WOMAN+'</option>'
     +'</select>')
   $('#searchContent').html('')
 } else if (searchType === 'field-address') {
   $('#conditionContent').html('<select id="condition" class="form-control" name="condition" onchange="changeSearch()">'
     +'<option value="contains">'+CrmLang.EXIST+'</option>'
     +'<option value="not_contain">'+CrmLang.ABSENT+'</option>'
     +'</select>')
     $("#searchContent").html('<select name="state" class="form-control" id="state"></select>'
			+'<select name="city" class="form-control" id="city"></select>'
			+'<select name="area" class="form-control" id="area"></select>'
			+'<input type="text" id="search" name="search" placeholder='+CrmLang.STREET_INFORMATION+' class="form-control">')
    new PCAS("state","city","area")
 } else if (searchType === 'field-role') {
   $('#searchContent').html('')
 } else if (searchType === 'field-customer') {
   $('#searchContent').html('')
 } else if (searchType === 'field-business-status') {
   $('#searchContent').html('')
 }
}
function changeSearch() {
  var searchType = $('#field option:selected').attr('class')
	var searchVal = $('#condition option:selected').val()
	if(searchVal === 'is_empty' || searchVal === 'is_not_empty') {
		$('#searchContent').html('')
	} else {
		if(searchType === 'field-date') {
			$('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>');
      $('#search').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'zh-CN',
        minView: 'month'
      })
		}  else if (searchType === 'field-text') {
			$('#searchContent').html('<input id="search" type="text" class="form-control" name="search"/>');
		}
	}
}
