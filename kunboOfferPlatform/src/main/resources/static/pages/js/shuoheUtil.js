
var shuoheUtil =
{
	layer_customer_difference:function(width,height) {
		var size = new Array();
		size[0] = (document.body.clientWidth - width)+'px';     
		size[1] = (document.body.clientHeight - height)+'px';
		return size;
	},
	layer_customer_size:function(width,height) {
		var size = new Array();
		size[0] = width+'px';     
		size[1] = height+'px';
		return size;
	},
	cpm_size:function() {
		var size = new Array();
		size[0] = (document.body.clientWidth - 300)+'px';     
		size[1] = (document.body.clientHeight - 100)+'px';
		return size;
	},
	//GMT时间转换为普通时间
	GMTToStr:function(time) {
		let date = new Date(time)
		let Str = date.getFullYear() + '-' +
			(date.getMonth() + 1) + '-' +
			date.getDate() + ' ' +
			date.getHours() + ':' +
			date.getMinutes() + ':' +
			date.getSeconds()
		return Str
	},
	formatDateTime:function(date)
	{
		if(checkNull(date))
		{
		    return '';
		}
		else
		{
		    var tmp = date.replace(':000','');
		    var ret = moment(tmp).format('YYYY-MM-DD HH:mm:ss');
		    console.info('ret = '+ret);
		    return ret;
		}
	},
	getXMonthLaterDate:function(date,xmonth)
	{
		var d = moment(this.formatDateTime(date));
		var t = d.add(xmonth, "months").format("YYYY-MM-DD HH:mm:ss");
		console.log(t);
    	return t;
	},
	getXMonthLaterDateYMD:function(date,xmonth)
	{
		var d = moment(this.formatDateTime(date));
		var t = d.add(xmonth, "months").format("YYYY-MM-DD");
		console.log(t);
    	return t;
	},	
	formatDateTimeYMD:function(date)
	{
		if(checkNull(date))
	    {
	        return '';
	    }
	    else
	    {
	        var tmp = date.replace(':000','');
	        var ret = moment(tmp).format('YYYY-MM-DD');
	        console.info('ret = '+ret);
	        return ret;
	    }
	},
	datagridFormatDate:function(val,row,index)
	{
		return formatDateTime(val);
	},
	datagridFormatDateYMD:function(val,row,index)
	{
		return formatDateTimeYMD(val);
	},
	getSystemDate:function()
	{
		return moment().format('YYYY-MM-DD HH:mm:ss');
	},
	checkNull:function(v)
	{
		if (v == null || v == undefined || v == '')
	        return true;
	    else
	        return false;
	},
	layerSaveMaskOn:function()
	{
		var index = layer.load(1, {
		    icon: 2,
		    shade: [0.7,'#fff'] //0.1透明度的白色背景
		});
		return index;
	},
	layerSaveMaskOff:function()
	{
		layer.closeAll('loading'); //关闭加载层
	},
	layerTopMaskOn:function()
	{
		var index = top.layer.load(1, {
		    icon: 2,
		    shade: [0.3,'#777'] //0.1透明度的白色背景
		});
		return index;
	},
	layerTopMaskOff:function()
	{
		top.layer.closeAll('loading'); //关闭加载层
	},
	layerMsgCustom:function()
	{
		layer.msg(content, function(){});
	},
	//针对使用window.open方法打开的子页面，通过该方法可以在点击按钮后关闭
	layerAlertAndClose:function(msg)
	{
		layer.alert(
		    msg+'<br/>点击确定按钮后将会关闭本页面',
		    {icon: 2,closeBtn: 0 },
		    function () {
		        console.info('');
		        window.close();
		});
	},
	errorClosePage:function(msg)
	{
		layer.alert(
		    msg+'<br/>点击确定按钮后将会关闭本页面',
		    {icon: 2,closeBtn: 0,offset: '20px'},
		    function () {
	            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	            parent.layer.close(index); //再执行关闭
		});
	},
	layerClose:function() {
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭
	},
	confirmCustom2Btn:function(okBtn,cancelBtn,title,showMsg,okFunction,cancelFunction){
		$.messager.defaults = { ok: okMsg,cancel: cancelMsg};
	    $.messager.confirm(title, "<font size='3px'>"+showMsg+"</font>",
	    function (defaults) {
	        if (defaults)
	        {
	            okFunction();
	        }
	        else
	        {
	            cancelFunction();
	        }
	    });
	},
	//删除前的重新确定
	confirmRecheck:function(okBtn,cancelBtn,title,showMsg,doFunction){
	    $.messager.defaults = { ok: okBtn,cancel: cancelBtn};
	    $.messager.confirm(title, "<font size='3px'>"+showMsg+"</font>", function(r){
	    	console.info('r = '+r);
	        if (r){
	            console.info('doFunction');
	            doFunction();
	        }
	    });
	},
	//layer 消息提示框
	layerMsgCustom:function(argument) {
		layer.msg(argument, function(){});
	},
	layerMsgSaveOK:function(argument) {
		layer.msg(argument);
	},
	layerMsgOK:function(argument) {
		this.layerMsgSaveOK(argument);
	},
	layerMsgError:function(argument) {
		this.layerMsgCustom(argument);
	},		
	layerTopMsgOK:function(argument) {
		top.layer.msg(argument);
	},
	layerTopMsgError:function(argument) {
		top.layer.msg(argument, function(){});
	},		
	//url 连接数据的获取
	getUrlHeader:function() {
		return new this.UrlSearch();
	},
	UrlSearch: function() {
		var name, value;
		var str = location.href; //取得整个地址栏
		var num = str.indexOf("?")
		str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]

		var arr = str.split("&"); //各个参数放到数组里
		for (var i = 0; i < arr.length; i++) {
			num = arr[i].indexOf("=");
			if (num > 0) {
				name = arr[i].substring(0, num);
				value = arr[i].substr(num + 1);
				this[name] = decodeURIComponent(value);
			}
		}
	},
	//去除字符串中所有html标签，保留文字，并截取前40个字符，之后用。。。显示
	lessString:function(str) {
		console.log('lessString = '+str);
		var con = "";
		var con = str.replace(/<\/?.+?>/g, "");
		var con = con.replace(/ /g, ""); //dds为得到后的内容
		con = con.substring(0, 40) + "......";
		return con;
	},
	addRules:function()
	{
	    var obj = $('.easyui-linkbutton').each(function(){
	        // alert(this)//这里 this获得的就是每一个dom对象 如果需要jquery对象 需要写成$(this)
	        for(var i=0;i<rules.length;i++)
	        {
	            if(rules[i].component_style == 'linkbutton'&&rules[i].component_id == this.id)
	            {
	                console.info('component_style = '+rules[i].component_style);
	                console.info('component_id = '+rules[i].component_id);
	                console.info('id = '+this.id);

	                if(rules[i].is_use == '0')
	                    $('#'+rules[i].component_id).linkbutton('disable'); 
	                if(rules[i].is_use == '1')             
	                    $('#'+rules[i].component_id).linkbutton('enable'); 
	            }
	        }
	    });
	},
	addRules:function(rules)
	{
	    var obj = $('.easyui-linkbutton').each(function(){
	        // alert(this)//这里 this获得的就是每一个dom对象 如果需要jquery对象 需要写成$(this)
	        for(var i=0;i<rules.length;i++)
	        {
	            if(rules[i].component_style == 'linkbutton'&&rules[i].component_id == this.id)
	            {
	                console.info('component_style = '+rules[i].component_style);
	                console.info('component_id = '+rules[i].component_id);
	                console.info('id = '+this.id);

	                if(rules[i].is_use == '0')
	                    $('#'+rules[i].component_id).linkbutton('disable'); 
	                if(rules[i].is_use == '1')             
	                    $('#'+rules[i].component_id).linkbutton('enable'); 
	            }
	        }
	    });
	},
	getPageName:function()
	{
		var strUrl=location.href;
		var arrUrl=strUrl.split("/");
		var strPage=arrUrl[arrUrl.length-1];
		return strPage;
	},
	decimal:function (num,v){
        var vv = Math.pow(10,v);
        return Math.round(num*vv)/vv;
	},
	checkRequired:function()
	{
		var ret = true;
		console.info('checkRequired');
		var obj = $('.easyui-textbox').each(function(){
			// console.info($('#'+this.id).textbox('options').prompt);
			var prompt = $('#'+this.id).textbox('options').prompt;
			var required = $('#'+this.id).textbox('options').required;
			if(required == true)
			{
				var value = $('#'+this.id).textbox('getValue')
				if(checkNull(value))
				{
					shuoheUtil.layerTopMsgError(prompt+'是必填项');
					console.info(prompt+'是必填项'); 
					ret = false;
					return false;         
				}
			}
		});
		$('.easyui-combobox').each(function(){
			// console.info($('#'+this.id).textbox('options').prompt);
			var prompt = $('#'+this.id).combobox('options').prompt;
			var required = $('#'+this.id).combobox('options').required;
			if(required == true)
			{
				var value = $('#'+this.id).combobox('getValue')
				if(checkNull(value))
				{
					shuoheUtil.layerTopMsgError(prompt+'是必填项');
					console.info(prompt+'是必填项'); 
					ret = false;
					return false;         
				}
			}
		});		
		return ret;
	}
}
$(function(){

    $.ajax({
        url: '/system/rule/roleRule/getRoleRuleByPostation.do',
        data: {
            uri:shuoheUtil.getPageName()
        },
        success: function(data) {
            console.info(data)    
            shuoheUtil.addRules(data);              
        }
    });
})


var Easyui = {
	Js:{
		//加载远程代码
		loadJsCode:function(code){
	        var script = document.createElement('script');
	        script.type = 'text/javascript';
	        script.appendChild(document.createTextNode(code));
	        document.body.appendChild(script);
	    }
	},
    Datagraid: {
        deleteSeleectRow:function(datagraid)
        {
			var row = $('#'+datagraid).datagrid('getSelected')
	        if (row) {
	            var rowIndex = $('#'+datagraid).datagrid('getRowIndex', row);
	            $('#'+datagraid).datagrid('deleteRow',rowIndex);
	            $('#'+datagraid).datagrid('autoSizeColumn');
	        }
	        else
	        {
	            layerMsgCustom('必须选择一条数据');
	        }
        },
        getSelectRow:function(datagraid)
        {
			return $('#'+datagraid).datagrid('getSelected');
        },
        getAllRows:function(datagraid)
        {
			return $('#'+datagraid).datagrid('getRows');
        },
        getSelectRowIndex:function(datagraid)
        {
			var row = $('#'+datagraid).datagrid('getSelected');
			return $('#'+datagraid).datagrid('getRowIndex', row);
        },
        getParentSelectRowIndex:function(datagraid)
        {
        	var row = parent.$('#'+datagraid).datagrid('getSelected');
			return parent.$('#'+datagraid).datagrid('getRowIndex', row);
        },
        rowUp:function(datagraid)
        {
			var row = $('#'+datagraid).datagrid('getSelected')
			var index = $('#'+datagraid).datagrid('getRowIndex', row)
			if (index > 0) {
				$('#'+datagraid).datagrid('insertRow', {
					index: index-1,
					row: row
				})
				$('#'+datagraid).datagrid('deleteRow', index+1)
				$('#'+datagraid).datagrid('selectRow', index-1)
			}
        },
        rowDown:function(datagraid)
        {
			var row = $('#'+datagraid).datagrid('getSelected')
			var index = $('#'+datagraid).datagrid('getRowIndex', row)
			if (index < $('#'+datagraid).datagrid('getRows').length-1) {
				$('#'+datagraid).datagrid('deleteRow', index)
				$('#'+datagraid).datagrid('insertRow', {
					index: index+1,
					row: row
				})
				$('#'+datagraid).datagrid('selectRow', index+1)
			}
		},
		loadColumes:function(datagraid,columes)
		{
			//     var data = new Array();
			//     for(var i=0;i<ProductSortParam.length;i++)
			//     {
			//         var column = {};
			//         column['title'] = ProductSortParam[i].text;
			//         column['field'] = ProductSortParam[i].text;
			//         // column['width'] = 130;
			//         // col['width'] 
			//         // col['align']
			//         // col['hidden']
			//         // console.info('text = '+ ProductSortParam[i].text);
			//         data.push(column);
			//     }
			//     console.info('data = '+ data);

			$('#'+datagraid).datagrid({
				columns:  [columes]
			})
		},
		loadData:function(datagraid,total,rows)
		{
			var obj = new Object();
			obj.total = total;
			obj.rows = rows;	
			$('#'+datagraid).datagrid('loadData',obj);
		},
		formatterDecimal2:function(value,row,index)
		{
			console.info('formatterDecimal2 = '+ value);
			return shuoheUtil.decimal(value,2);          
		}
	},
	Treegraid: {
		getSelectRow:function(treegrid)
        {
			return $('#'+treegrid).treegrid('getSelected');
		},
		getData:function(treegrid)
		{
			return $('#'+treegrid).treegrid('getData');
		}
	},
    Button:{
    	createButtonHtml:function(id,iconCls,plain,onclick,value)
    	{
    		var html = '';
 // <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
 			html += '<a id=\''+id+'\' href="javascript:void(0)" \
 					class="easyui-linkbutton" data-options="iconCls:\''+iconCls+'\',plain:'+plain+'"\
 					onclick="'+onclick+'">'+value+'</a>';
	        console.info("html="+html);
	        return html;
    	}
    },
    TextBox:{
    	createHtml:function(id,prompt)
    	{
			var html = '';
			html += '<input class="easyui-textbox" id='+id+' data-options="prompt:\''+prompt+'\'"  style="margin-left: 25px">'
			return html;
		},
		getValue:function(id)
		{
			var value = $('#'+id).textbox('getValue');
			if(checkNull(value))
				return "";
			else
				return value;
		}
    },
    DateBox:{
    	createHtml:function(id)
    	{
			var html = '';
			html += '<input class="easyui-datebox" id='+id+'  style="margin-left: 25px">'
			return html;
    	}
    },
    DatetimeBox:{
    	createHtml:function(id)
    	{
			var html = '';
			html += '<input class="easyui-datetimebox" id='+id+'  style="margin-left: 25px">'
			return html;
    	}
    },
    ComboTree:{
    	createHtml:function(id,value,url,iconCls,prompt,hidden)
    	{
			var html = '';
			html += '<input class="easyui-combotree" id='+id+' data-options="prompt:\''+prompt+'\'"  style="margin-left: 25px">'
			return html;
    	}
    },
    Layer:{
    	openAddLayerWindow:function(title,content,area)
    	{
			layer.open({
				type: 2,
				title: title,
				shadeClose: false,
				shade: 0.3,
				maxmin: true, //开启最大化最小化按钮s
				area: area,
				content: content
			})
    	},
    	openLayerWindow:function(title,content,area)
    	{
			layer.open({
				type: 2,
				title: title,
				shadeClose: false,
				shade: 0.3,
				maxmin: true, //开启最大化最小化按钮s
				area: area,
				content: content
			})
    	},  
    	sizeOneRow:function(height)
    	{
    		var _height = 0;
    		if(height == undefined){
    			_height = (document.body.clientHeight - 200);
    		}
    		else{
    			var max_high = (document.body.clientHeight - 200);
    			if(_height > max_high)
    			{
    				_height = max_high;
    			}
    			else
    			{
    				_height = height;
    			}    			
    		}
    		return ['500px',_height+'px']
    	},
    	sizeTwoRow:function(height)
    	{
    		var _height = 0;
    		if(height == undefined){
    			_height = (document.body.clientHeight - 200);
    		}
    		else{
    			var max_high = (document.body.clientHeight - 200);
    			if(_height > max_high)
    			{
    				_height = max_high;
    			}
    			else
    			{
    				_height = height;
    			}    			
    		}
    		return ['850px',_height+'px']
    	},    	
    	openNewFromOrderWindows:function(formName,relate_id)
    	{

    		if(shuoheUtil.checkNull(formName))
    		{
    			console.info('不能没有包名');
    			return;
    		}
    		var url = '';
    		url = "/pages/formDevelopment/newFormOrder.jsp?title=" + formName;
    		if(!shuoheUtil.checkNull(relate_id))
    			url+= "&relate_id="+relate_id;


    		layer.open({
			    type: 2,
			    title: "添加数据",
			    shadeClose: false,
			    shade: 0.3,
			    maxmin: true, //开启最大化最小化按钮
			    area: ["900px", document.body.clientHeight-20+"px"],
			    btn: ["提交并开启流程","保存"],
			    content: url,
			    yes: function(index){ //或者使用btn2
			    	var ifname="layui-layer-iframe"+index//获得layer层的名字
				    var Ifame=window.frames[ifname]//得到框架
				    Ifame.insert(true)
				    return false
			    },btn2: function(index){ //或者使用btn2
				      var ifname="layui-layer-iframe"+index//获得layer层的名字
				      var Ifame=window.frames[ifname]//得到框架
				      Ifame.insert(false)
				      return false
				    }
			  })
    	},
    	openMoifyFromOrderWindows:function(formName)
    	{

    		if(shuoheUtil.checkNull(formName))
    		{
    			console.info('不能没有包名');
    			return false;
    		}
			var row = $('#dg').datagrid('getSelected')
			if (row == null) {
				shuoheUtil.layerMsgCustom('请先选择一条数据');
				return false
			}
			var isAct='1';
			$.ajax({
				url: "/profFormRel/isAct",
				type: "POST",
				dataType: 'json',
				async: false,
				data: {
					'tableName': formName
				},
				error:function(error)
				{
					shuoheUtil.layerMsgCustom('通讯错误');
					return false;
				},
				success: function(result) //成功
				{
					isAct = result;
				}
			})

			var flag = true
			if (isAct == '0') {
				$.ajax({
					url: "/flow/getStats",
					type: "POST",
					dataType: 'json',
					async: false,
					data: {
						'uuid': row.uuid
					},
					success: function(result) //成功
					{
						if (result.msg != '未开启') {
							flag = false
						}
					}
				})
			}
			if (flag) {
				var url = '';
				url = "/pages/formDevelopment/editFormOrder.jsp?title=" + formName + '&uuid=' + row.uuid;

				if (isAct == '0') {
					layer.open({
						type: 2,
						title: '修改数据',
						shadeClose: false,
						shade: 0.3,
						maxmin: true, //开启最大化最小化按钮
						area: ['900px', document.body.clientHeight - 20 + 'px'],
						btn: ['提交并开启流程', '保存'],
						content:url,
						yes: function(index) { //或者使用btn2
							var ifname = "layui-layer-iframe" + index //获得layer层的名字
							var Ifame = window.frames[ifname] //得到框架
							Ifame.update(true)
							return false
						},
						btn2: function(index) { //或者使用btn2
							var ifname = "layui-layer-iframe" + index //获得layer层的名字
							var Ifame = window.frames[ifname] //得到框架
							Ifame.update(false)
							return false
						}
					})
				} else {
					layer.open({
						type: 2,
						title: '修改数据',
						shadeClose: false,
						shade: 0.3,
						maxmin: true, //开启最大化最小化按钮
						area: ['900px', document.body.clientHeight - 20 + 'px'],
						btn: '保存',
						content: url,
						yes: function(index) {
							var ifname = "layui-layer-iframe" + index //获得layer层的名字
							var Ifame = window.frames[ifname] //得到框架
							Ifame.update(false)
							return false
						}
					})
				}
			} else {
				shuoheUtil.layerMsgCustom('流程已开启，不能修改数据。');
			}
    	}
    },
    Flow:{
    	isFlowStart:function()
    	{

    	}
    },
    User:{
    	getUser:function()
    	{
			var ret = new Object();
    		$.ajax({
	            url:'/system/user/getUser.do',
	            type: "POST",
	            dataType: 'json',
	            async: false,
	            data: {
	            },
	            error: function() //失败
	            {
					console.info('获取不到用户信息');
					ret = null;
	            },
	            success: function(data) //成功
	            {
	            	ret = data;
	            }
			});
			return ret;
    	}
    },
    Form:{
    	getInfo:function(formName)
    	{
	    	$.ajax({
	            url: "/profFormRel/getInfo",
	            type: "POST",
	            dataType:'json',
	            data:
	            {
	              'tableName': formName
	            },
	            error: function() //失败
	            {
	              messageloadError()
	            },
	            success: function(result)//成功
	            {
					key = result.obj.proDefKey;
					formUrl = result.obj.formUrl;
					tableName = result.obj.formTableName;
					console.info(key+formUrl+tableName);
	            }
	        })
    	}
    },
    Ajax:{
    	get:function(url)
    	{
    		var ret = new Object();

			$.ajax({
	            url: url,
	            type: "POST",
	            dataType:'json',
	            async: false,
	            error: function() //失败
	            {
	            	ret.result = false
	            },
	            success: function(obj)//成功
	            {
	            	ret.obj = obj;
	            	ret.result = true;
	            }
	        })
	        return ret;
    	},
		post:function(url,succe,fail)
		{
			var o;
			$.ajax({
				url: url,
				type: "POST",
				dataType:'json',
				async: false,
				error: function() //失败
				{
					console.info('error');
					fail;
				},
				success: function(obj)//成功
				{
					console.info('success');
					o = obj;
					succe;
				}
			})
			return o;
		}
	},
	Object:{
		isEmptyObject:function (e) {
			var t;
			for (t in e)
				return !1;
			return !0
		}		
	}
}

//用JS获取地址栏参数的方法 
function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg); 
     if(r!=null)return  unescape(r[2]); return null;
}
