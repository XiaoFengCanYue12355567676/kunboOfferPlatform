
var isEmptyData = [
  {
    'id': 'is null',
    'text': '为空'
  },
  {
    'id': 'is not null',
    'text': '不为空'
  }
]
var mannerData = [
  {
    'text': '并且',
    'value': 'and'
  },
  {
    'text': '或者',
    'value': 'or'
  }
]
var conditionData = [
  {
    'text': '包含',
    'value': 'like'
  },
  {
    'text': '不包含',
    'value': 'not like'
  },
  {
    'text': '等于',
    'value': '='
  },
  {
    'text': '不等于',
    'value': '<>'
  },
  {
    'text': '大于',
    'value': '>'
  },
  {
    'text': '大于等于',
    'value': '>='
  },
  {
    'text': '小于',
    'value': '<'
  },
  {
    'text': '小于等于',
    'value': '<='
  }
]
// 数据格式化
function formatterManner (value, row, index) {
  if (value == 'and') {
    return '并且'
  } else if (value == 'or') {
    return '或者'
  }
}
function formatterCondition (value, row, index) {
  if (value == 'like') {
    return '包含'
  } else if (value == 'not like') {
    return '不包含'
  } else if (value == '=') {
    return '等于'
  } else if (value == '<>') {
    return '不等于'
  } else if (value == '>') {
    return '大于'
  } else if (value == '>=') {
    return '大于等于'
  } else if (value == '<') {
    return '小于'
  } else if (value == '<=') {
    return '小于等于'
  }
}
function formatterWhether (value, row, index) {
  if (value == 'false') {
    return '否'
  } else if (value == 'true') {
    return '是'
  }
}
function formatterWidth (value, row, index) {
  if (value == '278') {
    return '普通'
  } else if (value == '703') {
    return '整行'
  }
}
function formatterHeight (value, row, index) {
  if (value == '32') {
    return '一行'
  } else if (value == '64') {
    return '两行'
  } else if (value == '96') {
    return '三行'
  } else if (value == '128') {
    return '四行'
  } else if (value == '160') {
    return '五行'
  }
}
function formatterFieldType (value, row, index) {
	  if (value == 'textbox') {
	    return '文本框'
	  } else if (value == 'textboxMultiline') {
	    return '多行文本框'
	  } else if (value == 'combobox') {
	    return '下拉列表框'
	  } else if (value == 'comboboxMultiple') {
	    return '多选下拉框'
	  } else if (value == 'numberbox') {
	    return '数值输入框'
	  } else if (value == 'datebox') {
	    return '日期输入框'
	  } else if (value == 'datetimebox') {
	    return '日期时间输入框'
	  } else if (value == 'monthbox') {
	    return '月份输入框'
	  } else if (value == 'tablebox') {
	    return '子表'
	  } else if (value == 'filebox') {
	    return '附件上传'
	  }
	}

//新增一条字段
function fieldAdd () {
  var index = layer.open({
    type: 2,
    title: '添加字段',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: 'addField.jsp',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
  //layer.full(index)
}
//新增成功
function addFieldDialogCallBackSuccess (info_str) {
  var obj = JSON.parse(info_str)
  //console.log(obj)
  $('#dg').datagrid('appendRow', obj)
  refreshFields()
  messageSaveOK()
}
//修改一条字段
function fieldModify () {
  var row = $('#dg').datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条字段')
    return false
  }
  var index = layer.open({
    type: 2,
    title: '修改字段',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: 'editField.jsp',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
  //layer.full(index)
}
// 传输修改数据
function editData () {
  var row = $('#dg').datagrid('getSelected')
  return JSON.stringify(row)
}
//修改成功
function editFieldDialogCallBackSuccess (info_str) {
  var obj = JSON.parse(info_str)
  console.table(obj)
  var row = $('#dg').datagrid('getSelected')
  var index = $('#dg').datagrid('getRowIndex', row)
  $('#dg').datagrid('updateRow', {
    index: index,
    row: obj
  })
  refreshFields()
  messageSaveOK()
}
//删除一条字段
function fieldDelete () {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条字段')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除字段'+row.title+'吗？',function(r){
      if (r){
        var index = $('#dg').datagrid('getRowIndex', row)
        $('#dg').datagrid('deleteRow', index)
        refreshFields()
      }
    })
  }
}
// 上移
function fieldUp () {
  var row = $('#dg').datagrid('getSelected')
  var index = $('#dg').datagrid('getRowIndex', row)
  if (index > 0) {
    $('#dg').datagrid('insertRow', {
      index: index-1,
      row: row
    })
    $('#dg').datagrid('deleteRow', index+1)
    $('#dg').datagrid('selectRow', index-1)
  }
}
// 下移
function fieldDown () {
  var row = $('#dg').datagrid('getSelected')
  var index = $('#dg').datagrid('getRowIndex', row)
  if (index < $('#dg').datagrid('getRows').length-1) {
    $('#dg').datagrid('deleteRow', index)
    $('#dg').datagrid('insertRow', {
      index: index+1,
      row: row
    })
    $('#dg').datagrid('selectRow', index+1)
  }
}
// 字段刷新
function refreshFields () {
  $('#treeField').combobox('loadData',removeFieldForTree(fields))
  $('#relateField').combobox('loadData',removeFieldForTree(fields))
}
// 选择下拉数据来源
function chooseSelectType (rec) {
  $('#selectType0').hide()
  $('#selectType1').hide()
  $('#selectType' + rec.value).show()
  if(rec.value==0){
    selectCount++
  }
}
// 初始化已存在下拉数据
function initexistSelect (flag) {
  $.ajax({
    url: basePath + "/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'tableName': 'table_manage_select'
    },
    error: function() //失败
    {
    },
    success: function(data)//成功
    {
      $('#normalSelect').combobox({
        data: data.rows,
        editable: false,
        valueField:'table_name',
        textField:'title'
      })
      if (flag){
        $('#normalSelect').combobox('setValue', result.selectID)
      }
    }
  })
  $('#existSelect').combobox({
    url: basePath + '/develop/url/getAllUrl.do',
    editable: false,
    valueField:'name',
    textField:'disc'
  })
  if (flag){
    $('#existSelect').combobox('setValue', result.selectID)
  }
}
function isBringData (newValue, oldValue) {
  if (newValue == 'true') {
    $('#bringFieldCon').show()
  } else {
    $('#bringFieldCon').hide()
  }
}
function initBringFields (rec) {
  if (selectCount>0) {
    $.ajax({
      url: basePath + "/develop/url/getUrlById.do",
      type: "POST",
      dataType:'json',
      async: false,
      data:
      {
        'id': rec.id
      },
      error: function() //失败
      {
      },
      success: function(data)//成功
      {
        selectTableData = data.urlOutputPara
      }
    })
    var temp = '<table id="selectTable"><thead><tr>'
    temp += '<th data-options="field:\'name\',width:\'33%\'">字段名</th><th data-options="field:\'type\',width:\'33%\'">类型</th>'
    temp += '<th data-options="field:\'inputName\',width:\'33%\'" editor="{type:\'combobox\',options:{valueField:\'text\',textField:\'text\',editable:false,data:fieldsForCombobox(parent.fields)}}">组件名</th></tr></thead></table>'
    $('#selectTableCon').html(temp)
    var tempJs = '$(\'#selectTable\').edatagrid({data:selectTableData,rownumbers:true,singleSelect:true,autoRowHeight:false,pagination:false,fitColumns:true,striped:true,autoSave:true})'
    var script = document.createElement("script")
    script.type = "text/javascript"
    try {
      script.appendChild(document.createTextNode(tempJs))
    } catch (ex) {
      script.text = tempJs
    }
    document.body.appendChild(script)
  }
  selectCount++

  // $.ajax({
  //   url: basePath+"/pages/crminterface/getDatagridForJson.jsp",
  //   type: "POST",
  //   dataType:'json',
  //   data:
  //   {
  //     'tableName': newValue
  //   },
  //   error: function() //失败
  //   {
  //     messageloadError()
  //   },
  //   success: function(data)//成功
  //   {
  //     var bringFieldsTemp = []
  //     for (var i = 0; i < data.field.length; i++) {
  //       var objTemp = new Object()
  //       objTemp.text = data.field[i].text
  //       objTemp.title = data.field[i].title
  //       bringFieldsTemp.push(objTemp)
  //     }
  //     $('#bringFields').combobox({
  //       data: bringFieldsTemp,
  //       width: 278,
  //       editable: false,
  //       multiple: true,
  //       valueField:'text',
  //       textField:'title'
  //     })
  //   }
  // })
}
// 去除字段disabled属性
function removeFieldDisable (fieldArr) {
  for (var i = 0; i < fieldArr.length; i++) {
    delete fieldArr[i].disabled
  }
  return fieldArr
}
// 字段转为下拉数据格式
function fieldsForCombobox (fieldArr) {
  var arr = []
  for (var i = 0; i < fieldArr.length; i++) {
    if (fieldArr[i].fieldType != 'tablebox' && fieldArr[i].text != 'pid') {
      arr.push({
        'title': fieldArr[i].title,
        'text': fieldArr[i].text
      })
    }
  }
  //console.log('父页面字段')
  //console.log(arr)
  return arr
}
// 字段筛选为图表字段
function fieldsForChart (fieldArr) {
  var arr = []
  for (var i = 0; i < fieldArr.length; i++) {
    if (fieldArr[i].fieldType == 'textbox' || fieldArr[i].fieldType == 'numberbox') {
      delete fieldArr[i].disabled
      arr.push(fieldArr[i])
    }
  }
  return arr
}
//  去除字段disabled属性和pid字段
function removeFieldForTree (fieldArr) {
  var arr = fieldArr
  for (var i = 0; i < arr.length; i++) {
    delete arr[i].disabled
    if (arr[i].text == 'pid') {
      arr.splice(i, 1)
    }
  }
  return arr
}
// 增加下拉选项
function addOption () {
  var title = $('#addComboboxTitle').textbox('getValue')
  if (title !== '') {
    $('#addCombobox').append('<option>' + title + '</option>')
    $('#addComboboxTitle').textbox('setValue', '')
  }
}
// 删除下拉选项
function deleteOption () {
  var index = $('#addCombobox').get(0).selectedIndex
  if (index >= 0) {
    $('#addCombobox').find('option:selected').remove()
  }
}
// 修改下拉选项
function modifyOption () {
  var index = $('#addCombobox').get(0).selectedIndex
  if (index >= 0) {
    $('#addComboboxTitle').textbox('setValue', $('#addCombobox').find('option:selected').text())
    $('#modifyOptionIndex').val(index)
  }
}
// 修改保存下拉选项
function modifySave () {
  var title = $('#addComboboxTitle').textbox('getValue')
  var obj = document.getElementById("addCombobox")
  if (title !== '') {
    var index = $('#modifyOptionIndex').val()
    obj.options[index].innerHTML = title
    $('#addComboboxTitle').textbox('setValue', '')
  }
}
// 上移下拉选项
function upOption () {
  var index = $('#addCombobox').get(0).selectedIndex
  if (index > 0) {
    var obj = document.getElementById("addCombobox")
    var temp = obj.options[index].innerHTML
    obj.options[index].innerHTML = obj.options[index - 1].innerHTML
    obj.options[index - 1].innerHTML = temp
    obj.options[index].selected = false
    obj.options[index - 1].selected = true
  }
}
// 下移下拉选项
function downOption () {
  var index = $('#addCombobox').get(0).selectedIndex
  var obj = document.getElementById("addCombobox")
  if (index >= 0 && index < obj.options.length-1) {
    var temp = obj.options[index].innerHTML
    obj.options[index].innerHTML = obj.options[index + 1].innerHTML
    obj.options[index + 1].innerHTML = temp
    obj.options[index].selected = false
    obj.options[index + 1].selected = true
  }
}
// 新建属性对象
function propertyObjct (text, value) {
  var obj = new Object()
  obj.text = text
  obj.value = value
  return obj
}
// 字段组转换
function fieldsToObject (fields) {
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    if (field.fieldType == 'filebox') {
      field.type = 'longtext'
    } else {
      field.type = 'varchar(255)'
    }
    // if (field.fieldType == 'textbox' || field.fieldType == 'textboxMultiline' || field.fieldType == 'tablebox' || field.fieldType == 'combobox' || field.fieldType == 'filebox') {
    //   field.type = 'varchar(255)'
    // } else if (field.fieldType == 'numberbox') {
    //   if (field.precision == '0') {
    //     field.type = 'int'
    //   } else {
    //     field.type = 'float'
    //   }
    // } else if (field.fieldType == 'datebox') {
    //   field.type = 'date'
    // } else if (field.fieldType == 'datetimebox') {
    //   field.type = 'datetime'
    // }
  }
  return fields
}
function fieldsToObject1 (fieldsArr, flag) {
  var arr = []
  for (var i = 0; i < fieldsArr.length; i++) {
    var field = fieldsArr[i]
    if (field.text == 'pid') {

    } else {
      if (field.fieldType == 'filebox') {
        field.type = 'longtext'
      } else {
        field.type = 'varchar(255)'
      }
      arr.push(field)
    }
  }
  return arr
}
// 修改表单初始化数据
function initEditForm (formJson) {
  var obj = new Object()
  var formObj = JSON.parse(formJson)
  var fieldsObj = []
  for (var i = 0; i < formObj.formProperties.length; i++) {
    var formProperty = formObj.formProperties[i]
    if (formProperty.text == 'fields') {
      var fieldstmp = formProperty.value
      for (var j = 0; j < fieldstmp.length; j++) {
        var fieldtmp = fieldstmp[j].properties
        var field = new Object()
        for (var z = 0; z < fieldtmp.length; z++) {
          field[fieldtmp[z].text] = fieldtmp[z].value
        }
        fieldsObj.push(field)
      }
      obj.fields = fieldsObj
    } else {
      obj[formProperty.text] = formProperty.value
    }
  }
  //console.log(obj)
  return obj
}
// 增加栏目的js
var specialJSFlag = 0
function addSpecialJS (formObj,field) {
  var tempJs = ''
  if (formObj.title == 'business_cloud_industry_dimension' && field.text == 'industry') {
    tempJs += '$("#industry").combobox({onSelect:industrySelectChange}) \n '
    tempJs += 'function ' + field.text + 'SelectChange(rec){ \n '
    tempJs += 'if(specialJSFlag>0){ \n '
    tempJs += '$.ajax({ \n url: basePath+"/version/getACB.do?id="+rec.id+"&tname='+formObj.title+'&text='+field.text+'", \n type: "POST", \n '
    tempJs += 'dataType:"text", \n data:{}, \n error:function(){ \n messageloadError(); \n '
    tempJs += '}, \n success:function(data){ \n $("#version").textbox("setValue", data); \n '
    tempJs += ' \n } \n }); \n '
    tempJs += '} \n '
    tempJs += '} \n '
    }
  if (formObj.title == 'business_cloud_exhibition_manage' && field.text == 'theme') {
    tempJs += '$("#theme").combobox({onSelect:themeSelectChange}) \n '
    tempJs += 'function ' + field.text + 'SelectChange(rec){ \n '
    tempJs += 'if(specialJSFlag>0){ \n '
    tempJs += '$.ajax({ \n url: basePath+"/version/getACB.do?id="+rec.id+"&tname='+formObj.title+'&text='+field.text+'", \n type: "POST", \n '
    tempJs += 'dataType:"text", \n data:{}, \n error:function(){ \n messageloadError(); \n '
    tempJs += '}, \n success:function(data){ \n $("#version").textbox("setValue", data); \n '
    tempJs += ' \n } \n }); \n '
    tempJs += '} \n '
    tempJs += '} \n '
    }
   if (formObj.title == 'business_cloud_customer_dimension' && field.text == 'customer_name') {
    tempJs += 'function ' + field.text + 'SelectChange(rec){ \n '
    tempJs += 'if(specialJSFlag>0){ \n '
    tempJs += '$.ajax({ \n url: basePath+"/version/getACB.do?id="+rec.id+"&tname='+formObj.title+'&text='+field.text+'", \n type: "POST", \n '
    tempJs += 'dataType:"text", \n data:{}, \n error:function(){ \n messageloadError(); \n '
    tempJs += '}, \n success:function(data){ \n $("#version").textbox("setValue", data); \n '
    tempJs += ' \n } \n }); \n '
    tempJs += '} \n '
    tempJs += 'if (' + field.text + 'SelectFields.length>0) { \n '
    tempJs += 'for (var prop in rec) { \n '
    tempJs += 'for (var i = 0; i < ' + field.text + 'SelectFields.length; i++) { \n '
    tempJs += 'if(' + field.text + 'SelectFields[i].name==prop){ \n '
    tempJs += '$(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').textbox(\'setValue\',\'\'+rec[prop]); \n '
    tempJs += '} \n '
    tempJs += '} \n '
    tempJs += '} \n '
    tempJs += '} \n '
    tempJs += '} \n '
    }
 /*if (formObj.title == 'business_cloud_xiaoshoujihua_manager' && field.text == 'department') {
      tempJs += '$("#department").combobox({onSelect:departmentSelectChange}) \n '
      tempJs += 'function ' + field.text + 'SelectChange(rec){ \n '
      tempJs += 'if(specialJSFlag>0){ \n '
      tempJs += '$.ajax({ \n url: basePath+"/version/getACB.do?id="+rec.id+"&tname='+formObj.title+'&text='+field.text+'", \n type: "POST", \n '
      tempJs += 'dataType:"text", \n data:{}, \n error:function(){ \n messageloadError(); \n '
      tempJs += '}, \n success:function(data){ \n $("#version").textbox("setValue", data); \n '
      tempJs += ' \n } \n }); \n '
      tempJs += '} \n '
      tempJs += '} \n '
      }*/
// 客户信息客户编码自动填充
   if (formObj.title == 'business_cloud_customer_information_middle' && field.text == 'classify') {
     tempJs += '$("#classify").combobox({onSelect:classifySelectChange}) \n '
     tempJs += 'function ' + field.text + 'SelectChange(rec){ \n '
     tempJs += 'if(specialJSFlag>0){ \n '
     tempJs += '$.ajax({ \n url: basePath+"/market/customer/dimension/getOrder.do?str="+rec.classify_code+"&tableName='+formObj.title+'&order=customer_code", \n type: "POST", \n '
     tempJs += 'dataType:"text", \n data:{}, \n error:function(){ \n messageloadError(); \n '
     tempJs += '}, \n success:function(data){ \n $("#customer_code").textbox("setValue", data); \n '
     tempJs += ' \n } \n }); \n '
     tempJs += '} \n '
     tempJs += '} \n '
  }
   // console.log(tempJs)
   return tempJs
}
// 生成表单
function initFormOrder (formObj, recordID) {
  var temp = ''
  var tempJs = ''
  var fields = formObj.field
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    // console.log(field)
    if (field.text != 'create_user_id' && field.text != 'taskid' && field.text != 'pid' && field.text != 'relate_id' && field.text != 'uuid') {
    //if (field.fieldType != 'tablebox' && field.text != 'uuid' && field.text != 'create_user_id' && field.text != 'taskid') {
    //temp += (field, formObj.columnNumber, true, recordID)
      temp += createTextbox(field, formObj.columnNumber, true, recordID)
      
      
      //console.log(temp)
      tempJs += createTextboxJs(field, true, recordID, formObj.columnNumber) + ' \n '
      tempJs += addSpecialJS(formObj,field)
    }
  }
  temp += '<div class="clear"></div>'
  $('#fieldCon').append(temp)
  var script = document.createElement("script")
  script.type = "text/javascript"
  // console.log(tempJs)
  try {
    script.appendChild(document.createTextNode(tempJs))
  } catch (ex) {
    script.text = tempJs
  }
  document.body.appendChild(script)
  // 客户投诉管理投诉单号自动填充
   if (formObj.title == 'business_cloud_kehutousuguanli') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "TS",
         'order':"no"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#no").textbox("setValue", data)
       }
     })
   }
   // 竞争对手编码自动填充
   if (formObj.title == 'business_cloud_competitor_information') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "JZDS",
         'order':"competitor_code"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#competitor_code").textbox("setValue", data)
       }
     })
   }
   // 出差申请单号自动填充
   if (formObj.title == 'business_cloud_chuchai_shengqing') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "CCSQ",
         'order':"no"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#no").textbox("setValue", data)
       }
     })
   }
// 出差报告单号自动填充
   if (formObj.title == 'business_cloud_chuchai_baogao') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "CCBG",
         'order':"report_number"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#report_number").textbox("setValue", data)
       }
     })
   }
   // 会议申请单号自动填充
   if (formObj.title == 'business_cloud_huiyi_shenqing') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "HYSQ",
         'order':"no"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#no").textbox("setValue", data)
       }
     })
   }
// 会议报告单号自动填充
   if (formObj.title == 'business_cloud_huiyi_baogao') {
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "HYBG",
         'order':"report_number"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#report_number").textbox("setValue", data)
       }
     })
   }

// 任务下达单号自动填充  (任务下达为自定义表单)
/*   if (formObj.title == 'business_cloud_renwuxiada') {
	   console.info(" order");
     $.ajax({
       url: basePath+"/market/customer/dimension/getOrder.do",
       type: "POST",
       dataType:'text',
       data:
       {
         'tableName': title,
         'str': "RWXD",
         'order':"renwuno"
       },
       error: function() //失败
       {
         messageloadError()
       },
       success: function(data)//成功
       {
         $("#wok_order").textbox("setValue", data)
       }
     })
   }*/
   // 提成计算 单号自动填充
   if (formObj.title == 'business_cloud_percentage_calculation_main') {
	     $.ajax({
	       url: basePath+"/market/customer/dimension/getOrder.do",
	       type: "POST",
	       dataType:'text',
	       data:
	       {
	         'tableName': title,
	         'str': "TCJSO",
	         'order':"calculation_order"
	       },
	       error: function() //失败
	       {
	         messageloadError()
	       },
	       success: function(data)//成功
	       {
	         $("#calculation_order").textbox("setValue", data)
	       }
	     })
	   }
   // 工资计算 单号自动填充
   if (formObj.title == 'business_cloud_salary_calculation_main') {
	     $.ajax({
	       url: basePath+"/market/customer/dimension/getOrder.do",
	       type: "POST",
	       dataType:'text',
	       data:
	       {
	         'tableName': title,
	         'str': "GZJSO",
	         'order':"salary_order"
	       },
	       error: function() //失败
	       {
	         messageloadError()
	       },
	       success: function(data)//成功
	       {
	         $("#salary_order").textbox("setValue", data)
	       }
	     })
	   }
  return fields
}
function initFormOrderComboboxText (formObj, recordID) {
  var temp = ''
  var tempJs = ''
  var fields = formObj.field
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    console.log(field)
    if (field.text != 'create_user_id' && field.text != 'taskid' && field.text != 'pid' && field.text != 'relate_id' && field.text != 'uuid') {
    //if (field.fieldType != 'tablebox' && field.text != 'uuid' && field.text != 'create_user_id' && field.text != 'taskid') {
    //temp += (field, formObj.columnNumber, true, recordID)
      temp += createTextbox(field, formObj.columnNumber, true, recordID)
      
      
      //console.log(temp)
      tempJs += createTextboxJsComboboxText(field, true, recordID, formObj.columnNumber) + ' \n '
      tempJs += addSpecialJS(formObj,field)
    }
  }
  temp += '<div class="clear"></div>'
  $('#fieldCon').append(temp)
  var script = document.createElement("script")
  script.type = "text/javascript"
  console.log(tempJs)
  try {
    script.appendChild(document.createTextNode(tempJs))
  } catch (ex) {
    script.text = tempJs
  }
  document.body.appendChild(script)
  return fields
}
function initViewFormOrder (formObj, recordID) {
  var temp = ''
  var tempJs = ''
  var fields = formObj.field
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    if (field.text != 'create_user_id' && field.text != 'taskid' && field.text != 'pid' && field.text != 'relate_id' && field.text != 'uuid') {
      temp += createTextbox(field, formObj.columnNumber, false, recordID)
      tempJs += createTextboxJs(field, false, recordID, formObj.columnNumber) + ' \n '
    }
  }
  //console.log(temp)
  temp += '<div class="clear"></div>'
  $('#fieldCon').append(temp)
  var script = document.createElement("script")
  script.type = "text/javascript"
  console.log(tempJs)
  try {
    script.appendChild(document.createTextNode(tempJs))
  } catch (ex) {
    script.text = tempJs
  }
  document.body.appendChild(script)
  return fields
}
//生成表单标签的js
function createTextboxJs (field, flag, recordID, columnNumber) {
  var temp = ''
  if (!flag && field.fieldType == 'filebox') {
    return temp
  }
  if (field.fieldType == 'monthbox') {
  	temp += 'jeDate("#'+field.text+'",{format:"YYYY-MM"}) \n '
    return temp
  }
  if ((field.fieldType == 'combobox' && field.selectType == '0') || field.fieldType == 'comboboxMultiple') {
    temp += 'var ' + field.text + 'ComboboxData=[]; \n '
    temp += '$.ajax({ \n url: basePath+"/crm/ActionFormUtil/getByTableName.do?tableName='+field.selectID+'", \n type: "POST", \n '
    temp += 'dataType:"json", \n data:{}, \n async:false, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.text + 'ComboboxData=data.rows; \n '
    temp += ' \n } \n }); \n '
  }
  if (field.fieldType == 'combobox' && field.selectType == '1') {
    temp += 'var ' + field.text + 'ComboboxData=[]; \n '
    temp += '$.ajax({ \n url: basePath+"/develop/url/getUrl.do?name='+field.selectID+'", \n type: "POST", \n '
    temp += 'dataType:"json", \n data:{}, \n async:false, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.text + 'ComboboxData=data; \n '
    temp += ' \n } \n }); \n '
  }
  if (field.fieldType == 'combobox' && field.selectType == '1') {
    //if (!(formObj.title == 'business_cloud_customer_dimension' && field.text == 'customer_name')) {
      temp += 'function ' + field.text + 'SelectChange(rec){ \n '
      temp += 'if (' + field.text + 'SelectFields.length>0) { \n '
      temp += 'for (var prop in rec) { \n '
      temp += 'for (var i = 0; i < ' + field.text + 'SelectFields.length; i++) { \n '
      temp += 'if(' + field.text + 'SelectFields[i].name==prop){ \n '
      temp += 'if($(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').hasClass("easyui-combobox")){ \n '
      temp += '$(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').combobox(\'setValue\',\'\'+rec[prop]); \n '
      temp += '}else{ \n '
      temp += '$(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').textbox(\'setValue\',\'\'+rec[prop]); \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
    //}
    // for (var i = 0; i < field.selectFields.length; i++) {
    //   temp += '$("#'+field.selectFields[i].inputName+'").textbox({ \n '
    //   if (columnNumber == 3) {
    //     temp += 'width:182,'
    //   } else if (columnNumber == 4) {
    //     temp += 'width:112,'
    //   } else if (columnNumber == 1) {
    //     temp += 'width:750,'
    //   } else {
    //     temp += 'width:325,'
    //   }
    //   temp += 'readonly:true \n '
    //   temp += '}) \n '
    // }
  }
  if (field.fieldType == 'numberbox' && field.needCalculate == 'true') { // 数字字段需要计算
    if (field.calculateType == 'multiply' || field.calculateType == 'plus') {
    	temp += 'function ' + field.text + 'Calculate(){ \n '
    	temp += "var arr = ["
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      if (i != 0) {
	        temp += ","
	      }
	      temp += "$('#"+field.calculateFields[i]+"').textbox('getValue')"
	    }
	    temp += "]; \n "
	    temp += 'if('
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      if (i != 0) {
	        temp += " && "
	      }
	      temp += "!checkNull(arr["+i+"])"
	    }
	    temp += '){ \n '
	    if (field.calculateType == 'multiply') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatMuls(arr)); \n "
	    } else if (field.calculateType == 'plus') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatAdds(arr)); \n "
	    }
	    temp += '} \n '
	    temp += '}; \n '
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      temp += "$('#"+field.calculateFields[i]+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
	    }
    } else if (field.calculateType == 'divide' || field.calculateType == 'minus') {
    	temp += 'function ' + field.text + 'Calculate(){ \n '
    	temp += 'if('
    	temp += "!checkNull($('#"+field.calculateFirstField+"').textbox('getValue')) && "
    	temp += "!checkNull($('#"+field.calculateLastField+"').textbox('getValue'))){ \n "
    	if (field.calculateType == 'divide') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatDiv($('#"+field.calculateFirstField+"').textbox('getValue'),$('#"+field.calculateLastField+"').textbox('getValue'))); \n "
	    } else if (field.calculateType == 'minus') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatSub($('#"+field.calculateFirstField+"').textbox('getValue'),$('#"+field.calculateLastField+"').textbox('getValue'))); \n "
	    }
	    temp += '} \n '
	    temp += '}; \n '
	    temp += "$('#"+field.calculateFirstField+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
	    temp += "$('#"+field.calculateLastField+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
    }
  }
  if (field.fieldType == 'tablebox') {
    temp += 'var ' + field.tableTitle + 'Columns; \n '
    temp += '$.ajax({ \n url: basePath+"/pages/crminterface/getDatagridForJson.do", \n type: "POST", \n '
    temp += 'dataType:"json",async:false, \n data:{ \n "tableName": "' + field.tableTitle + '" \n }, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.tableTitle + 'Columns=initColumns(data.field, false);initNormalSelectData(data.field); \n '
    temp += '$("#' + field.tableTitle + '").datagrid({ \n '
    temp += 'rownumbers:true, \n singleSelect:true, \n autoRowHeight:false, \n pagination:false, \n fitColumns:true, \n striped:true, \n '
    temp += 'columns:' + field.tableTitle + 'Columns, \n url:basePath+"/crm/ActionFormUtil/getSonDataByUuid.do?tableName='+field.tableTitle+'&uuid='+recordID+'" \n'
    temp += ',toolbar:"#tb_' + field.tableTitle + '" \n '
    temp += ' }) \n '
    temp += ' \n } \n }); \n '
    if(flag){
      // temp += 'function ' + field.tableTitle + 'TableAdd(tableName,recordID){ \n layer.open({ \n type: 2, \n title:"添加数据", \n shadeClose:true, \n shade:false, \n maxmin:true, \n '
      // temp += 'area: ["880px", "380px"], \n btn:"保存", \n content:"childTable/addChildTableOrder.jsp?title="+tableName+"&recordID="+recordID, \n '
      // temp += 'yes:function(index){ \n var ifname="layui-layer-iframe"+index; \n var Ifame=window.frames[ifname]; \n Ifame.insert(); \n '
      // temp += '} \n }) \n } \n '
      //
      // temp += 'function addFieldTableDialogCallBackSuccess(){ \n messageSaveOK(); \n '
      // temp += '$("#"+tableID).datagrid("reload"); \n } \n '
      //
      // temp += ''
      //
      // temp += 'function fieldTableDelete(tableName){ \n var row = $("#"+tableID).datagrid("getSelected"); \n '
      // temp += 'if(row == null){ \n alert("请先选择一条数据");return false; \n }else{ \n '
      // temp += '$.messager.confirm("确认","您确认想要删除吗？",function(r){ \n '
      // temp += 'if(r){ \n $.ajax({ \n url: basePath+"/crm/ActionFormUtil/delete.do", \n type: "POST",dataType:"text", \n '
      // temp += 'data:{ \n "tableName": tableID, \n "id": row.id \n }, \n error:function(){ \n messageDeleteError(); \n }, \n '
      // temp += 'success:function(data){ \n data = $.trim(data); \n if(data == "0"){ \n messageDeleteError(); \n }else{ \n '
      // temp += 'messageDeleteOK(); \n $("#"+tableID).datagrid("reload"); \n } \n } \n }) \n } \n }) \n '
      // temp += '} \n } \n '
    }
    //console.log(temp)
  } else {
    // temp += 'console.log("' + field.text + '"+$("#' + field.text + '").length);'
    temp += '$("#' + field.text + '").'
    if (field.fieldType == 'textboxMultiline' || field.fieldType == 'filebox') {
      temp += 'textbox'
    } else if (field.fieldType == 'comboboxMultiple') {
      temp += 'combobox'
    } else {
      temp += field.fieldType
    }
    temp += '({'
    if (field.fieldType == 'filebox') {
      if (field.width == '703') {
        temp += 'width:685,'
      } else {
        if (columnNumber == 3) {
          temp += 'width:122,'
        } else if (columnNumber == 4) {
          temp += 'width:54,'
        } else if (columnNumber == 1) {
          temp += 'width:685,'
        } else {
          temp += 'width:263,' 
        }
      }
    } else {
      if (field.width == '703') {
        temp += 'width:740,'
      } else {
        if (columnNumber == 3) {
          temp += 'width:177,'
        } else if (columnNumber == 4) {
          temp += 'width:109,'
        } else if (columnNumber == 1) {
          temp += 'width:740,'
        } else {
          temp += 'width:318,'
        }
      }
    }
    if (!checkNull(field.height)) {
      var heightTemp = field.height
      if (heightTemp>32) {
        heightTemp = heightTemp/32*22
      }
      temp += 'height:' + heightTemp + ','
    }
//  if(field.fieldType != 'numberbox'){
      if (field.fieldType == 'filebox') {
        temp += 'editable:false,'
      } else {
        if (!checkNull(field.editable)) {
          temp += 'editable:' + field.editable + ','
        }
      }
      if (!checkNull(field.disabled)) {
        temp += 'disabled:' + field.disabled + ','
      }
      if (flag) {
        if (!checkNull(field.readonly)) {
          temp += 'readonly:' + field.readonly + ','
        }
      } else {
        temp += 'readonly:true,'
      }
      if (!checkNull(field.required)) {
        temp += 'required:' + field.required + ','
      }
      if (!checkNull(field.prompt)) {
        temp += 'prompt:\'' + field.prompt + '\','
      }
//  }

    if (field.fieldType == 'textboxMultiline') {
      temp += 'multiline:true,'
     } else if (field.fieldType == 'combobox') {
      if (field.selectType == '0') {
        temp += "data:"+field.text+"ComboboxData,"
        temp += "valueField:'id',"
        temp += "textField:'text',"
      } else if (field.selectType == '1') {
        //temp += "url:basePath+'/develop/url/getUrl.do?name="+field.selectID+"',"
        temp += "data:"+field.text+"ComboboxData,"
    	  temp += "valueField:'id',"
    	  temp += "textField:'text',"
    	  temp += "onSelect:"+field.text+"SelectChange,"
    	  temp += "panelWidth:400,"
      }
    } else if (field.fieldType == 'comboboxMultiple') {
      temp += "data:"+field.text+"ComboboxData,"
      temp += "valueField:'id',"
      temp += "textField:'text',"
      temp += "multiple:true,"
    } else if (field.fieldType == 'numberbox') {
      if (!checkNull(field.min)) {
        temp += 'min:' + field.min + ','
      }
      if (!checkNull(field.max)) {
        temp += 'max:' + field.max + ','
      }
      if (!checkNull(field.precision)) {
        temp += 'precision:' + field.precision + ','
      }
      if (!checkNull(field.prefix)) {
        temp += "prefix:'" + field.prefix + "',"
      }
      if (!checkNull(field.suffix)) {
        temp += "suffix:'" + field.suffix + "',"
      }
      temp += "value:0,"
    } else if (field.fieldType == 'datebox') {
      if (flag) {
        if (!checkNull(field.currentDate)) {
          temp += 'value:\''+getSystemDate()+'\','
        }
      }
    } else if (field.fieldType == 'datetimebox') {
      if (!checkNull(field.showSeconds)) {
        temp += 'showSeconds:' + field.showSeconds + ','
      }
      if (flag) {
        if (!checkNull(field.currentDate)) {
          temp += 'value:\''+getSystemDate()+'\','
        }
      }
    }
    if (!checkNull(field.defaultValue) && (field.fieldType != 'datebox' && field.fieldType != 'datetimebox')) {
      if (field.defaultValue == 'actualName') {
        temp += 'value:\''+user.actual_name+'\','
      } else if (field.defaultValue == 'department') {
        $.ajax({
          url: basePath+"/crm/ActionFormUtil/getDataById.do",
          type: "POST",
          dataType:'json',
          async: false,
          data:
          {
            'tableName': 'db_system_user_department',
            'id': user.department_id
          },
          error: function() //失败
          {
            messageloadError()
          },
          success: function(data)//成功,
          {
            temp += 'value:\''+data.rows[0].text+'\','
          }
        })
      }
    }
    temp = removeComma(temp)
    temp += '});'
  }
  return temp
}
function createTextboxJsComboboxText (field, flag, recordID, columnNumber) {
  var temp = ''
  if (!flag && field.fieldType == 'filebox') {
    return temp
  }
  if (field.fieldType == 'monthbox') {
  	temp += 'jeDate("#'+field.text+'",{format:"YYYY-MM"}) \n '
    return temp
  }
  if ((field.fieldType == 'combobox' && field.selectType == '0') || field.fieldType == 'comboboxMultiple') {
    temp += 'var ' + field.text + 'ComboboxData=[]; \n '
    temp += '$.ajax({ \n url: basePath+"/crm/ActionFormUtil/getByTableName.do?tableName='+field.selectID+'", \n type: "POST", \n '
    temp += 'dataType:"json", \n data:{}, \n async:false, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.text + 'ComboboxData=data.rows; \n '
    temp += ' \n } \n }); \n '
  }
  if (field.fieldType == 'combobox' && field.selectType == '1') {
    temp += 'var ' + field.text + 'ComboboxData=[]; \n '
    temp += '$.ajax({ \n url: basePath+"/develop/url/getUrl.do?name='+field.selectID+'", \n type: "POST", \n '
    temp += 'dataType:"json", \n data:{}, \n async:false, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.text + 'ComboboxData=data; \n '
    temp += ' \n } \n }); \n '
  }
  if (field.fieldType == 'combobox' && field.selectType == '1') {
    //if (!(formObj.title == 'business_cloud_customer_dimension' && field.text == 'customer_name')) {
      temp += 'function ' + field.text + 'SelectChange(rec){ \n '
      temp += 'if (' + field.text + 'SelectFields.length>0) { \n '
      temp += 'for (var prop in rec) { \n '
      temp += 'for (var i = 0; i < ' + field.text + 'SelectFields.length; i++) { \n '
      temp += 'if(' + field.text + 'SelectFields[i].name==prop){ \n '
      temp += 'if($(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').hasClass("easyui-combobox")){ \n '
      temp += '$(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').combobox(\'setValue\',\'\'+rec[prop]); \n '
      temp += '}else{ \n '
      temp += '$(\'#\'+' + field.text + 'SelectFields[i].inputName+\'\').textbox(\'setValue\',\'\'+rec[prop]); \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
      temp += '} \n '
  }
  if (field.fieldType == 'numberbox' && field.needCalculate == 'true') { // 数字字段需要计算
    if (field.calculateType == 'multiply' || field.calculateType == 'plus') {
    	temp += 'function ' + field.text + 'Calculate(){ \n '
    	temp += "var arr = ["
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      if (i != 0) {
	        temp += ","
	      }
	      temp += "$('#"+field.calculateFields[i]+"').textbox('getValue')"
	    }
	    temp += "]; \n "
	    temp += 'if('
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      if (i != 0) {
	        temp += " && "
	      }
	      temp += "!checkNull(arr["+i+"])"
	    }
	    temp += '){ \n '
	    if (field.calculateType == 'multiply') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatMuls(arr)); \n "
	    } else if (field.calculateType == 'plus') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatAdds(arr)); \n "
	    }
	    temp += '} \n '
	    temp += '}; \n '
	    for (let i = 0; i < field.calculateFields.length; i++) {
	      temp += "$('#"+field.calculateFields[i]+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
	    }
    } else if (field.calculateType == 'divide' || field.calculateType == 'minus') {
    	temp += 'function ' + field.text + 'Calculate(){ \n '
    	temp += 'if('
    	temp += "!checkNull($('#"+field.calculateFirstField+"').textbox('getValue')) && "
    	temp += "!checkNull($('#"+field.calculateLastField+"').textbox('getValue'))){ \n "
    	if (field.calculateType == 'divide') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatDiv($('#"+field.calculateFirstField+"').textbox('getValue'),$('#"+field.calculateLastField+"').textbox('getValue'))); \n "
	    } else if (field.calculateType == 'minus') {
	      temp += "$('#"+field.text+"').textbox('setValue',FloatSub($('#"+field.calculateFirstField+"').textbox('getValue'),$('#"+field.calculateLastField+"').textbox('getValue'))); \n "
	    }
	    temp += '} \n '
	    temp += '}; \n '
	    temp += "$('#"+field.calculateFirstField+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
	    temp += "$('#"+field.calculateLastField+"').textbox({onChange:function(){"+field.text+"Calculate()}}); \n "
    }
  }
  if (field.fieldType == 'tablebox') {
    temp += 'var ' + field.tableTitle + 'Columns; \n '
    temp += '$.ajax({ \n url: basePath+"/pages/crminterface/getDatagridForJson.do", \n type: "POST", \n '
    temp += 'dataType:"json",async:false, \n data:{ \n "tableName": "' + field.tableTitle + '" \n }, \n error:function(){ \n messageloadError(); \n '
    temp += '}, \n success:function(data){ \n ' + field.tableTitle + 'Columns=initColumns(data.field, false);initNormalSelectData(data.field); \n '
    temp += '$("#' + field.tableTitle + '").datagrid({ \n '
    temp += 'rownumbers:true, \n singleSelect:true, \n autoRowHeight:false, \n pagination:false, \n fitColumns:true, \n striped:true, \n '
    temp += 'columns:' + field.tableTitle + 'Columns, \n url:basePath+"/crm/ActionFormUtil/getSonDataByUuid.do?tableName='+field.tableTitle+'&uuid='+recordID+'" \n'
    temp += ',toolbar:"#tb_' + field.tableTitle + '" \n '
    temp += ' }) \n '
    temp += ' \n } \n }); \n '
  } else {
    // temp += 'console.log("' + field.text + '"+$("#' + field.text + '").length);'
    temp += '$("#' + field.text + '").'
    if (field.fieldType == 'textboxMultiline' || field.fieldType == 'filebox') {
      temp += 'textbox'
    } else if (field.fieldType == 'comboboxMultiple') {
      temp += 'combobox'
    } else {
      temp += field.fieldType
    }
    temp += '({'
    if (field.fieldType == 'filebox') {
      if (field.width == '703') {
        temp += 'width:685,'
      } else {
        if (columnNumber == 3) {
          temp += 'width:122,'
        } else if (columnNumber == 4) {
          temp += 'width:54,'
        } else if (columnNumber == 1) {
          temp += 'width:685,'
        } else {
          temp += 'width:263,' 
        }
      }
    } else {
      if (field.width == '703') {
        temp += 'width:740,'
      } else {
        if (columnNumber == 3) {
          temp += 'width:177,'
        } else if (columnNumber == 4) {
          temp += 'width:109,'
        } else if (columnNumber == 1) {
          temp += 'width:740,'
        } else {
          temp += 'width:318,'
        }
      }
    }
    if (!checkNull(field.height)) {
      var heightTemp = field.height
      if (heightTemp>32) {
        heightTemp = heightTemp/32*22
      }
      temp += 'height:' + heightTemp + ','
    }
//  if(field.fieldType != 'numberbox'){
      if (field.fieldType == 'filebox') {
        temp += 'editable:false,'
      } else {
        if (!checkNull(field.editable)) {
          temp += 'editable:' + field.editable + ','
        }
      }
      if (!checkNull(field.disabled)) {
        temp += 'disabled:' + field.disabled + ','
      }
      if (flag) {
        if (!checkNull(field.readonly)) {
          temp += 'readonly:' + field.readonly + ','
        }
      } else {
        temp += 'readonly:true,'
      }
      if (!checkNull(field.required)) {
        temp += 'required:' + field.required + ','
      }
      if (!checkNull(field.prompt)) {
        temp += 'prompt:\'' + field.prompt + '\','
      }
//  }

    if (field.fieldType == 'textboxMultiline') {
      temp += 'multiline:true,'
     } else if (field.fieldType == 'combobox') {
      if (field.selectType == '0') {
        temp += "data:"+field.text+"ComboboxData,"
        temp += "valueField:'text',"
        temp += "textField:'text',"
      } else if (field.selectType == '1') {
        //temp += "url:basePath+'/develop/url/getUrl.do?name="+field.selectID+"',"
        temp += "data:"+field.text+"ComboboxData,"
    	  temp += "valueField:'text',"
    	  temp += "textField:'text',"
    	  temp += "onSelect:"+field.text+"SelectChange,"
    	  temp += "panelWidth:400,"
      }
    } else if (field.fieldType == 'comboboxMultiple') {
      temp += "data:"+field.text+"ComboboxData,"
      temp += "valueField:'text',"
      temp += "textField:'text',"
      temp += "multiple:true,"
    } else if (field.fieldType == 'numberbox') {
      if (!checkNull(field.min)) {
        temp += 'min:' + field.min + ','
      }
      if (!checkNull(field.max)) {
        temp += 'max:' + field.max + ','
      }
      if (!checkNull(field.precision)) {
        temp += 'precision:' + field.precision + ','
      }
      if (!checkNull(field.prefix)) {
        temp += "prefix:'" + field.prefix + "',"
      }
      if (!checkNull(field.suffix)) {
        temp += "suffix:'" + field.suffix + "',"
      }
      temp += "value:0,"
    } else if (field.fieldType == 'datebox') {
      if (flag) {
        if (!checkNull(field.currentDate)) {
          temp += 'value:\''+getSystemDate()+'\','
        }
      }
    } else if (field.fieldType == 'datetimebox') {
      if (!checkNull(field.showSeconds)) {
        temp += 'showSeconds:' + field.showSeconds + ','
      }
      if (flag) {
        if (!checkNull(field.currentDate)) {
          temp += 'value:\''+getSystemDate()+'\','
        }
      }
    }
    if (!checkNull(field.defaultValue) && (field.fieldType != 'datebox' && field.fieldType != 'datetimebox')) {
      if (field.defaultValue == 'actualName') {
        temp += 'value:\''+user.actual_name+'\','
      } else if (field.defaultValue == 'department') {
        $.ajax({
          url: basePath+"/crm/ActionFormUtil/getDataById.do",
          type: "POST",
          dataType:'json',
          async: false,
          data:
          {
            'tableName': 'db_system_user_department',
            'id': user.department_id
          },
          error: function() //失败
          {
            messageloadError()
          },
          success: function(data)//成功,
          {
            temp += 'value:\''+data.rows[0].text+'\','
          }
        })
      }
    }
    temp = removeComma(temp)
    temp += '});'
  }
  return temp
}
// 去除最后的逗号
function removeComma (str) {
  if (str.lastIndexOf(',') == (str.length-1)) {
    str = str.substring(0, str.length-1)
  }
  return str
}
// 生成表单标签
function createTextbox (field, columnNumber, flag, recordID) {
	  var fieldType = field.fieldType
	  var temp = ''
	  if (columnNumber == 3) {
	    temp += '<div class="formThreeColumn">'
	  } else if (columnNumber == 4) {
	    temp += '<div class="formFourColumn">'
	  }
	  if (fieldType == 'tablebox') {
	    temp += '<div class="clear"></div>'
	    temp += '<div class="field-con-title">' + field.title + '</div>'
	    temp += '<div class="easyui-panel form-table-con" data-options="fit:true"><table id="' + field.tableTitle + '" style="width:100%;height:300px;"></table></div>'
	    if (flag) {
	      temp += '<div id="tb_' + field.tableTitle + '" style="padding:0px 30px;height: 35px">'
	      temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-plus" onclick="fieldChildTableAdd(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">新增</span><span class="l-btn-icon fa fa-plus">&nbsp;</span></span></a>'
	      temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableModify(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">修改</span><span class="l-btn-icon fa fa-pencil">&nbsp;</span></span></a>'
	      temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-info-circle" onclick="fieldChildTableView(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">查看</span><span class="l-btn-icon fa fa-info-circle">&nbsp;</span></span></a>'
	      temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-trash" onclick="fieldChildTableDelete(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">删除</span><span class="l-btn-icon fa fa-trash">&nbsp;</span></span></a>'
	      if (field.import == 'true') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableImport(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">导入</span><span class="l-btn-icon fa fa-cloud-upload">&nbsp;</span></span></a>'
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableExport(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">导出</span><span class="l-btn-icon fa fa-cloud-download">&nbsp;</span></span></a>'
	      }
	      if (field.tableTitle == 'business_cloud_percentage_calculation') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableSelect(\'' + field.tableTitle + '\',\'' + recordID + '\',\'business_cloud_sales_return\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">选择导入</span><span class="l-btn-icon fa fa-list">&nbsp;</span></span></a>'
	      }
	      if (field.tableTitle == 'business_cloud_salary_calculation') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableSelect(\'' + field.tableTitle + '\',\'' + recordID + '\',\'business_cloud_sale_deliver_manager_header\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">选择导入</span><span class="l-btn-icon fa fa-list">&nbsp;</span></span></a>'
	      }
	      if (field.tableTitle == 'business_cloud_overprice_calculation' || field.tableTitle == 'business_cloud_percentage_calculation' || field.tableTitle == 'business_cloud_salary_calculation') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableCalculate(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">计算</span><span class="l-btn-icon fa fa-cog">&nbsp;</span></span></a>'
	      }
	      if (field.tableTitle == 'business_cloud_percentage_calculation' || field.tableTitle == 'business_cloud_salary_calculation') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-pencil" onclick="fieldChildTableTotal(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">合计</span><span class="l-btn-icon fa fa-list">&nbsp;</span></span></a>'
	      }
	      temp += '</div>'
	    } else {
	      temp += '<div id="tb_' + field.tableTitle + '" style="padding:0px 30px;height: 35px">'
	      temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-info-circle" onclick="fieldChildTableView(\'' + field.tableTitle + '\',\'' + recordID + '\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">查看</span><span class="l-btn-icon fa fa-info-circle">&nbsp;</span></span></a>'
	      temp += '</div>'
	    }

	  } else {
	    if (fieldType == 'textboxMultiline' || fieldType == 'filebox') {
	      fieldType = 'textbox'
	    } else if (fieldType == 'comboboxMultiple') {
	      fieldType = 'combobox'
	    }
	    if (field.width == '703' || columnNumber == 1) {
	      temp += '<div class="clear"></div>'
	      temp += '<div class="form-order-line form-order-line-one">'
	    } else {
	      temp += '<div class="form-order-line">'
	    }
	    temp += '<label>' + field.title + '</label>'
	    if (!flag && field.fieldType == 'filebox') {
	      temp += '<a href="javascript:void(0)" id="' + field.text + '" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconCls="fa fa-cog" style="height: 32px;line-height:32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top:0!important;height: 32px;line-height:32px;"><span class="l-btn-text" style="height: 30px;line-height:30px;">查看</span><span class="l-btn-icon fa fa-cog" style="height: 26px;line-height:26px;">&nbsp;</span></span></a>'
	      //temp += '<a href="javascript:void(0)" id="' + field.text + '" style="display:inline-block;height:32px;line-height:32px;">查看</a>'
	    } else if (field.fieldType == 'monthbox') {
	    	temp += '<input type="text" id="' + field.text + '" class="form-order-input" />'
	    } else {
	      temp += '<input type="text" id="' + field.text + '" class="easyui-' + fieldType + '" />'
	      if (field.fieldType == 'filebox') {
	        temp += '<a href="javascript:void(0)" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconcls="fa fa-upload" onclick="openUploadFile(\''+field.text+'\')" group="" id="" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top: 4px;"><span class="l-btn-text">上传</span><span class="l-btn-icon fa fa-upload">&nbsp;</span></span></a>'
	      }
	    }
	    temp += '</div>'
	    if (field.width == '703' || columnNumber == 1) {
	      temp += '<div class="clear"></div>'
	    }
	    if (columnNumber == 3 || columnNumber == 4) {
	      temp += '</div>'
	    }
	  }
	  if (fieldType == 'combobox' && field.selectType == '1') {

	    // for (var i = 0; i < field.selectFields.length; i++) {
	    //   if (columnNumber == 3) {
	    //     temp += '<div class="formThreeColumn">'
	    //   } else if (columnNumber == 4) {
	    //     temp += '<div class="formFourColumn">'
	    //   }
	    //   temp += '<div class="form-order-line">'
	    //   temp += '<label>' + field.selectFields[i].title + '</label>'
	    //   temp += '<input type="text" id="' + field.selectFields[i].inputName + '" class="easyui-textbox" />'
	    //   temp += '</div>'
	    //   if (columnNumber == 3 || columnNumber == 4) {
	    //     temp += '</div>'
	    //   }
	    // }
	  }
	  return temp
	}

// 表单取值
function getValue (fields, obj) {
	  var data = []
	  for (var i = 0; i < fields.length; i++) {
	    //if (fields[i].fieldType != 'tablebox' && fields[i].text != 'uuid' && fields[i].text != 'create_user_id' && fields[i].text != 'taskid') {
	    if (fields[i].text != 'uuid' && fields[i].text != 'create_user_id' && fields[i].text != 'taskid' && fields[i].text != 'pid' && fields[i].text != 'relate_id') {
	      var fieldType = fields[i].fieldType
	      if (fieldType == 'textboxMultiline' || fieldType == 'filebox') {
	        fieldType = 'textbox'
	      }
	      var temp = new Object()
	      // if (fieldType != 'tablebox') {
	        temp.text = fields[i].text
	      // }
	      if (fieldType == 'textbox') {
	        temp.value = "'" + $.trim($('#' + fields[i].text).textbox('getValue')) + "'"
	      } else if (fieldType == 'combobox') {
	        temp.value = "'" + $('#' + fields[i].text).combobox('getValue') + "'"
	      } else if (fieldType == 'comboboxMultiple') {
	        temp.value = "'" + $('#' + fields[i].text).combobox('getValues') + "'"
	      } else if (fieldType == 'numberbox') {
	        temp.value = $('#' + fields[i].text).numberbox('getValue')
	      } else if (fieldType == 'datebox') {
	        temp.value = "'" + $('#' + fields[i].text).datebox('getValue') + "'"
	      } else if (fieldType == 'datetimebox') {
	        temp.value = "'" + $('#' + fields[i].text).datetimebox('getValue') + "'"
	      } else if (fieldType == 'monthbox') {
	        temp.value = "'" + $('#' + fields[i].text).val() + "'"
	      } else if (fieldType == 'tablebox') {
	        // var rows = $('#' + fields[i].tableTitle).datagrid('getRows')
	        // console.log(rows)
	        temp.value = "'" + fields[i].tableTitle + "'"
	      }
	      // if (fieldType != 'tablebox') {
	        data.push(temp)
	      // }
	    }
	  }
	  obj.field = data
	  return obj
	}

// 表单验证
function formCheckInput (fields) {
  for (var i = 0; i < fields.length; i++) {
    if (fields[i].required == 'true') {
      var fieldType = fields[i].fieldType
      if (fieldType == 'textboxMultiline') {
        fieldType = 'textbox'
      }
      if (fieldType == 'textbox') {
        if (checkNull($('#' + fields[i].text).textbox('getValue'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      } else if (fieldType == 'combobox') {
        if (checkNull($('#' + fields[i].text).combobox('getValue'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      } else if (fieldType == 'comboboxMultiple') {
        if (checkNull($('#' + fields[i].text).combobox('getValues'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      } else if (fieldType == 'numberbox') {
        if (checkNull($('#' + fields[i].text).numberbox('getValue'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      } else if (fieldType == 'datebox') {
        if (checkNull($('#' + fields[i].text).datebox('getValue'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      } else if (fieldType == 'datetimebox') {
        if (checkNull($('#' + fields[i].text).datetimebox('getValue'))) {
           layerMsgCustom('' + fields[i].title + '不能为空')
           return false
      	}
      }
    }
  }
  return true
}
// 根据字段生成表格头部
var columnUuid;
function initColumns (fields, processFlag) {
  var temp = []
  var showColumnsCount = 0
  var arr = [{field:'id',title:'id',hidden:true}]
  for (var i = 0; i < fields.length; i++) {
    var column = new Object()
    column = (function () {
     var ii = i
     var field = fields[ii]
     var tmp = new Object()
     if (field.fieldType == 'tablebox') {
       tmp.formatter = function (value,row,index) {
         return '<a href="javascript:void(0)" onclick="childTableManage(\''+value+'\',\''+row.uuid+'\')" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconCls="fa fa-cog" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top:0!important"><span class="l-btn-text">查看详情</span><span class="l-btn-icon fa fa-cog">&nbsp;</span></span></a>'
       }
     } else if (field.fieldType == 'combobox' && field.selectType == '0') {
       tmp.formatter = function (value,row,index) {
         var svalue = value
         if (svalue!='') {
          try {
            var sdata = normalSelectData[field.text]
            svalue = sdata[value]
           } catch (error) {
             
           }
         }
         return svalue
       }
     } else if (field.fieldType == 'combobox' && field.selectType == '1') {
       tmp.formatter = function (value,row,index) {
         var svalue = value
         if (svalue!='') {
           try {
            var sdata = normalSelectData[field.text]
            svalue = sdata[value]
           } catch (error) {
             
           }
         }
         return svalue
       }
     } else if (field.fieldType == 'comboboxMultiple') {
      tmp.formatter = function (value,row,index) {
        var svalue = value
        var temp = ''
        if (svalue!='' && svalue!=null) {
          var arr = svalue.split(',')
          var sdata = normalSelectData[field.text]
          for (var i = 0; i < arr.length; i++) {
            var tmp = arr[i]
            temp += sdata[tmp] + ','
          }
        }
        if (temp.length > 0) {
          temp = removeComma(temp)
        }
        return temp
      }
    } else if (field.fieldType == 'filebox') {
       tmp.formatter = function (value,row,index) {
         var svalue = ''
         if (value!='' && value!=null) {
           var fielList = value.split(',')
           for (var i = 0; i < fielList.length; i++) {
            var name = fielList[i].split('/')[3].substring(36)
             svalue += '<a href="'+basePath+fielList[i]+'" download="'+name+'" target="_blank">'+name+'</a> '
           }
           //svalue = '<a href="javascript:void(0)" onclick="viewFiles(\''+svalue+'\')" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconCls="fa fa-cog" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top:0!important"><span class="l-btn-text">查看</span><span class="l-btn-icon fa fa-cog">&nbsp;</span></span></a>'
         }
         return svalue
       }
     }
     tmp.field = field.text
     tmp.title = field.title
     //console.log(tmp)
     return tmp
   })(i);
    // var field = fields[i]
    // if (field.fieldType == 'tablebox') {
    //   column.field = field.text
    //   column.title = field.title
    //   column.formatter = function (value,row,index) {
    //     return '<a href="javascript:void(0)" onclick="childTableManage(\''+value+'\',\''+row.id+'\')" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" btncls="topjui-btn-normal" plain="true" iconCls="fa fa-cog" style="height: 32px;"><span class="l-btn-left l-btn-icon-left" style="margin-top:0!important"><span class="l-btn-text">查看详情</span><span class="l-btn-icon fa fa-cog">&nbsp;</span></span></a>'
    //   }
    // } else if (field.fieldType == 'combobox' && field.selectType == '0') {
    //   column.field = field.text
    //   column.title = field.title
    //   column.formatter = function (value,row,index) {
    //     var a = column.field
    //     console.log(a)
    //     var temp = selectFormatter(value,column.field,fields)
    //     console.log(temp)
    //     return temp
    //   }
    // } else {
    //   column.field = field.text
    //   column.title = field.title
    // }
    var flag = fields[i].listDisplay ==="true" ? true : false
    if (flag) {
      showColumnsCount++
    }
    column.hidden = !flag
    arr.push(column)
  }
  if (processFlag) {
    showColumnsCount += 2
  }
  var columnWidth = 0
  if (showColumnsCount <= 12) {
    columnWidth = 100/showColumnsCount
  } else {
    columnWidth = 8.5
  }
  for (var i = 0; i < arr.length; i++) {
    arr[i].width = columnWidth+'%'
  }
  temp.push(arr)
  return temp
}
function initProcessColumns (cloumnarr) {
  var showColumnsCount = 0
  for (var i = 0; i < cloumnarr[0].length; i++) {
    if (cloumnarr[0][i].hidden == false) {
      showColumnsCount++
    }
  }
  showColumnsCount += 2
  var columnWidth = 0
  if (showColumnsCount <= 12) {
    columnWidth = 100/showColumnsCount
  } else {
    columnWidth = 8.5
  }
  cloumnarr[0].push({
    field: 'uuid',
    title: '流程进度',
    width: columnWidth+'%',
    formatter: function (value,row,index) {
      columnUuid = row.uuid;
      var svalue = value
      $.ajax({
        url: basePath+"/flow/getStats",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
          'uuid': row.uuid
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          svalue = data.msg
        }
      })
      return svalue
    }
  })
  cloumnarr[0].push({
    field: 'proUuid',
    title: '流程详情',
    align: 'center',
    width: columnWidth+'%',
    formatter: function (value,row,index) {
      return "<a href=\"#\" class=\"easyui-linkbutton\" onclick=\"viewProcessDetail('"+columnUuid+"')\" plain=\"true\" iconCls=\"icon-view\">查看</a>"
    }
  })
  console.log(cloumnarr)
  return cloumnarr
}
function selectFormatter (value, field, fields) {
  var selectText = ''+value
  if (selectText=='null'){
    selectText=''
  }
  if(selectText!=''){
    for (var i = 0; i < fields.length; i++) {
      if (fields[i].text == field) {
        var selectTableName = fields[i].selectID
        $.ajax({
          url: basePath+"/crm/ActionFormUtil/getDataById.do",
          type: "POST",
          dataType:'json',
          async:false,
          data:
          {
            'tableName': selectTableName,
            'id': value
          },
          error: function() //失败
          {
            messageloadError()
          },
          success: function(data)//成功
          {
            var arr = data.rows
            if (arr.length>0) {
              selectText = arr[0].text
            }
          }
        })
        break
      }
    }
  }
  return selectText
}
//初始化表单数据列表普通下拉字段数据
function initNormalSelectData (fields) {
  var selectFields = []
  for (var i = 0; i < fields.length; i++) {
    var selectField = fields[i]
    if ((selectField.fieldType == 'combobox' && selectField.selectType == '0') || selectField.fieldType == 'comboboxMultiple') {
      $.ajax({
        url: basePath+"/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          'tableName': selectField.selectID
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          var arr = data.rows
          var tempObj = new Object()
          for (var i = 0; i < arr.length; i++) {
            var tmp = arr[i]
            tempObj[tmp.id] = tmp.text
          }
          normalSelectData[selectField.text] = tempObj
          //console.log(normalSelectData)
        }
      })
    }
    if (selectField.fieldType == 'combobox' && selectField.selectType == '1') {
      $.ajax({
        url: basePath+"/develop/url/getUrl.do",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          'name': selectField.selectID
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          var tempObj = new Object()
          for (var i = 0; i < data.length; i++) {
            var tmp = data[i]
            tempObj[tmp.id] = tmp.text
          }
          normalSelectData[selectField.text] = tempObj
        }
      })
    }
  }
  // var selectFields = []
  // for (var i = 0; i < fields.length; i++) {
  //   if (fields[i].fieldType == 'combobox' && fields[i].selectType == '0') {
  //     var temp = new Object()
  //     temp.text = fields[i].text
  //     temp.selectID = fields[i].selectID
  //     if (selectFields.indexOf(temp) == -1) {
  //       selectFields.push(temp)
  //     }
  //   }
  // }
  // for (var i = 0; i < selectFields.length; i++) {
  //   var selectField = selectFields[i]
  //   $.ajax({
  //     url: basePath+"/crm/ActionFormUtil/getByTableName.do",
  //     type: "POST",
  //     dataType:'json',
  //     async:true,
  //     data:
  //     {
  //       'tableName': selectField.selectID
  //     },
  //     error: function() //失败
  //     {
  //       messageloadError()
  //     },
  //     success: function(data)//成功
  //     {
  //       var arr = data.rows
  //       var tempObj = new Object()
  //       for (var i = 0; i < arr.length; i++) {
  //         var tmp = arr[i]
  //         tempObj[tmp.id] = tmp.text
  //       }
  //       normalSelectData[selectField.text] = tempObj
  //       //console.log(normalSelectData)
  //     }
  //   })
  // }
}
// 管理子表数据
function childTableManage (tableName, uuid) {
  var temp = '管理子表数据'
  var importFlag = ''
  for (var i = 0; i < result.field.length; i++) {
    //console.log(result.field[i].tableTitle)
    if (result.field[i].tableTitle == tableName) {
      temp = result.field[i].title
      importFlag = result.field[i].import
      break
    }
  }
  layer.open({
    type: 2,
    title: temp,
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['960px', document.body.clientHeight-20+'px'],
    btn: '关闭',
    content: '/pages/formDevelopment/childTable/childTableManage.jsp?tableName=' + tableName + '&uuid=' + uuid + '&importFlag=' + importFlag,
    yes:function(index){
      layer.close(index)
    }
  })
}
// 转换成combobox下拉数据
function convertComboboxData (rows) {
  var arr = []
  for (var i = 0; i < rows.length; i++) {
    if (rows[i].type == '1') {
      arr.push(rows[i])
    }
  }
  return arr
}
//新增一条子表数据
function fieldChildTableAdd (tableName, recordID) {
  layer.open({
    type: 2,
    title: '添加数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: '/pages/formDevelopment/childTable/addChildTableOrder.jsp?title='+tableName+'&recordID='+recordID,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
}
//新增成功
function fieldChildTableCallBackSuccess (tableName) {
  messageSaveOK()
  $('#'+tableName).datagrid('reload')
}
//修改一条子表数据
function fieldChildTableModify (tableName, recordID) {
  var row = $('#'+tableName).datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '修改数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: '/pages/formDevelopment/childTable/editChildTableOrder.jsp?title='+tableName+'&recordID='+recordID+'&id='+row.id,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
function fieldChildTranTransferRowData (tableName) {
  var row = $('#'+tableName).datagrid('getSelected')
  return row
}
//查看一条子表数据
function fieldChildTableView (tableName, recordID) {
  var row = $('#'+tableName).datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '查看数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '关闭',
    content: '/pages/formDevelopment/childTable/seeChildTableOrder.jsp?title='+tableName+'&recordID='+recordID+'&id='+row.id,
    yes:function(index){
      layer.close(index)
    }
  })
}
//删除一条子表数据
function fieldChildTableDelete (tableName, recordID) {
  var row = $('#'+tableName).datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: basePath+"/crm/ActionFormUtil/delete.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': tableName,
            'id': row.id
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#'+tableName).datagrid('reload')
            }
          }
        })
      }
    })
  }
}
//导入子表数据
function fieldChildTableImport (tableName, recordID) {
  layer.open({
    type: 2,
    title: '导入数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['400px', '250px'],
    btn: '关闭',
    content: '/pages/formDevelopment/childTable/importChildTable.jsp?title='+tableName+'&recordID='+recordID,
    yes:function(index){
      layer.close(index)
    }
  })
}
//导出子表数据
function fieldChildTableExport (tableName, recordID) {
  window.location.href=basePath+"/crm/ActionFormSelectUtil/Select/excelModel.do?tableName="+tableName;
	/*$.ajax({
    url: basePath+"/crm/ActionFormSelectUtil/Select/excelModel.do",
    type: "POST",
    dataType:'text',
    data:
    {
      'tableName': tableName
    },
    error: function() //失败
    {
    	 messageCustom('导出模板失败')
    },
    success: function(data)//成功
    {  
      data = $.trim(data)
      if (data == 'true') {
        messageOkCustom('导出模板成功','已导出到本地桌面，请查看。')
      }
    }
  })*/
}
//计算子表数据
function fieldChildTableCalculate (tableName, recordID) {
  var div=document.createElement('div')
  div.innerHTML='<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:' + _PageHeight + 'px;top:0;background:#f3f8ff;opacity:1;filter:alpha(opacity=80);z-index:10000;"><div style="position: absolute; cursor1: wait; left: ' + _LoadingLeft + 'px; top:' + _LoadingTop + 'px; width:100px;; height: 57px; line-height: 57px; padding-left: 50px; padding-right: 5px; background: #fff  no-repeat scroll 5px 12px; border: 2px solid #95B8E7; color: #696969; font-family:\'Microsoft YaHei\';">计算中...</div></div>'
  document.body.appendChild(div)
  if (tableName == 'business_cloud_overprice_calculation') {
    $.ajax({
      url: basePath+"/history/data/analysis/toCalculation.do",
      type: "POST",
      dataType:'text',
      data:
      {
        'tableName': tableName,
        'uuid': recordID
      },
      error: function() //失败
      {
        messageCustom('计算失败')
      },
      success: function(data)//成功
      {
        completeLoading()
        if (data == '-1') {
          messageCustom('计算失败')
        } else {
        messageOkCustom('计算成功','')
          $('#'+tableName).datagrid('reload')
        }
      }
    })
  } else if (tableName == 'business_cloud_percentage_calculation') {
    $.ajax({
      url: basePath+"/history/data/analysis/toPercentageCalculation.do",
      type: "POST",
      dataType:'text',
      data:
      {
        'tableName': tableName,
        'uuid': recordID
      },
      error: function() //失败
      {
        messageCustom('计算失败')
      },
      success: function(data)//成功
      {
        completeLoading()
        if (data == '-1') {
          messageCustom('计算失败')
        } else {
        messageOkCustom('计算成功','')
          $('#'+tableName).datagrid('reload')
        }
      }
    })
  } else if (tableName == 'business_cloud_salary_calculation') {
    $.ajax({
      url: basePath+"/history/data/analysis/toSalaryCalculation.do",
      type: "POST",
      dataType:'text',
      data:
      {
        'tableName': tableName,
        'uuid': recordID
      },
      error: function() //失败
      {
        messageCustom('计算失败')
      },
      success: function(data)//成功
      {
        completeLoading()
        if (data == '-1') {
          messageCustom('计算失败')
        } else {
        messageOkCustom('计算成功','')
          $('#'+tableName).datagrid('reload')
        }
      }
    })
  }
  
}
//子表选择数据导入
function fieldChildTableSelect (to, uuid, tableName) {
	console.info("tableName111==="+tableName);
  var p =""
  if(tableName=="business_cloud_sale_deliver_manager_header"){  
	  p="saleDeliverForSalary.jsp"
  }else if(tableName=="business_cloud_sales_return"){
	  p="saleReturnMoneyForPercentage.jsp"
  }
  layer.open({
    type: 2,
    title: '选择导入数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: ['导入','关闭'],
    content: basePath+'/pages/saleManager/'+p+'?tableName='+tableName+'&uuid='+uuid+'&ToTableName='+to,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.childTableImport(index)
      return false
    },btn2: function(index){ //或者使用btn2
      layer.close(index)
    }
  })
}
//子表数据计算合计
function fieldChildTableTotal (tableName, uuid) {
  var url = ''
  var total_id = 'total_amount'
  if (tableName == 'business_cloud_percentage_calculation') {
    url = basePath + '/history/data/analysis/toTotalPercentage.do'
  } else if (tableName == 'business_cloud_salary_calculation') {
    url = basePath + '/history/data/analysis/toTotalSalary.do'
  }else if (tableName == 'business_cloud_xiaoshoujihua_niandu_shouruyusuan') {
	    url = basePath + '/history/data/analysis/toTotalIncomeBudget.do'
	    total_id = "total_income_budget"
  }else if (tableName == 'business_cloud_xiaoshoujihua_niandu_huikuanjihua') {
	    url = basePath + '/history/data/analysis/toTotalReimbursementPlan.do'
	    total_id = "total_reimbursement_plan"
  }
  $.ajax({
    url: url,
    type: "POST",
    dataType:'text',
    data:
    {
      'tableName': tableName,
      'uuid': uuid
    },
    error: function() //失败
    {
      messageCustom('计算失败')
    },
    success: function(data)//成功
    {
   //   $('#total_amount').textbox('setValue',data)
      $('#'+total_id).textbox('setValue',data)
    }
  })
}

function initFieldData (flag) {
	var temp = ''
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    console.log(field)
    if (field.fieldType != 'tablebox' && field.text != 'create_user_id' && field.text != 'taskid') {
      if (field.fieldType != 'filebox') {
        temp += '$("#' + field.text + '").'
        if (field.fieldType == 'monthbox') {
          temp += 'val("' + result[field.text] + '");'
        } else {
        	if (field.fieldType == 'textboxMultiline' || field.fieldType == 'filebox') {
	          temp += 'textbox'
	        } else if (field.fieldType == 'comboboxMultiple') {
	          temp += 'combobox'
	        } else {
	          temp += field.fieldType
	        }
	        if (field.fieldType == 'comboboxMultiple') {
            temp += '("setValues", "' + result[field.text] + '");'
            temp += '("setValue", "' + result[field.text] + '");'
	        }
        }
      } else {
        if (flag) {
          temp += '$("#' + field.text + '").textbox("setValue", "' + result[field.text] + '");'
        } else {
          temp += '$("#' + field.text + '").attr("onclick", "viewFiles(\'' + result[field.text] + '\')");'
        }
      }
    }

 }
 var script = document.createElement("script")
 script.type = "text/javascript"
 try {
   script.appendChild(document.createTextNode(temp))
 } catch (ex) {
   script.text = temp
 }
 document.body.appendChild(script)
 specialJSFlag++
}

//新增一条表格表头
function formAttrAdd (id) {
	$('#dg_'+id).edatagrid('addRow')
}
//删除一条表格表头
function formAttrDelete (id) {
	var row = $('#dg_'+id).edatagrid('getSelected')
	var i = $('#dg_'+id).edatagrid('getRowIndex', row)
	$('#dg_'+id).edatagrid('editRow', i)
	$('#dg_'+id).edatagrid('destroyRow', i)
}
// 表格表头上移
function formAttrUp (id) {
  $('#dg_'+id)
  var row = $('#dg_'+id).edatagrid('getSelected')
  var index = $('#dg_'+id).edatagrid('getRowIndex', row)
  if (index > 0) {
    $('#dg_'+id).edatagrid('insertRow', {
      index: index-1,
      row: row
    })
    $('#dg_'+id).edatagrid('deleteRow', index+1)
    $('#dg_'+id).edatagrid('selectRow', index-1)
  }
}
// 表格表头下移
function formAttrDown (id) {
  var row = $('#dg_'+id).edatagrid('getSelected')
  var index = $('#dg_'+id).edatagrid('getRowIndex', row)
  if (index < $('#dg_'+id).edatagrid('getRows').length-1) {
    $('#dg_'+id).edatagrid('deleteRow', index)
    $('#dg_'+id).edatagrid('insertRow', {
      index: index+1,
      row: row
    })
    $('#dg_'+id).edatagrid('selectRow', index+1)
  }
}
//去除属性isNewRecord
function removeIsNewRecord (rows) {
	for(var i=0; i<rows.length; i++){
		delete rows[i].isNewRecord
	}
	return rows
}
// 生成表格表头
function initTableColumns (columnsStr) {
	var columns = eval(columnsStr)
	var columnWidth = 100/columns.length
	for (var i = 0; i < columns.length; i++) {
	columns[i].width = columnWidth+'%'
	//console.log(columns[i].formatter)
	if(columns[i].formatter=='combobox'){
	columns[i].formatter=function(value,row,index){
	var svalue=value;
	//console.log(row)
	if(svalue!=""){
	var field = ''
	for (var key in row) {
	if (row.hasOwnProperty(key)) {
	if(row[key]==value){
	field = key
	break
	}
	}
	}
	//console.log(field)
	var sdata=normalSelectData[field];
	svalue=sdata[value];
	}
	return svalue;
	}
	}
	}
	//console.log(columns)
	return columns
	} 
// 生成表格按钮
function initTableButtons (buttonsStr) {
  var buttons = eval(buttonsStr)
  var temp = ''
  var tempjs = ''
  for(var i=0; i<buttons.length; i++){
		temp += '<a href="#" id="'+buttons[i].buttonId+'" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="'+buttons[i].buttonAttr+'" onclick="'+buttons[i].buttonFunction+'">'+buttons[i].buttonName+'</a>'
    tempjs += "$('#"+buttons[i].buttonId+"').linkbutton({});"
	}
  $('#tb').html(temp)
  var script = document.createElement("script")
  script.type = "text/javascript"
  try {
    script.appendChild(document.createTextNode(tempjs))
  } catch (ex) {
    script.text = tempjs
  }
  document.body.appendChild(script)
}
// 生成搜索
function initTableSearch (result, formObj) {
	  var searchs = eval(result.searchs)
	  var searchButtons = eval(result.search_buttons)
	  var temp = '<form id="queryForm" class="search-box">'
	  var tempJs = ''
	  for(var i=0; i<searchs.length; i++){
	    if (searchs[i].searchCondition == 'isEmpty') {
	      temp += searchs[i].searchName
	      temp += '<input class="easyui-combobox" id="'+searchs[i].searchId+'" data-options="data:isEmptyData,valueField:\'id\',textField:\'text\',editable:false,width:70">'
	      tempJs += "$('#"+searchs[i].searchId+"').combobox({});"
	    } else {
	      if (searchs[i].searchType == 'combobox') {
	        temp += '<input class="easyui-combobox" id="'+searchs[i].searchId+'" data-options="prompt:\''+searchs[i].searchName+'\'">'
	        for (var j = 0; j < formObj.field.length; j++) {
	          if (formObj.field[j].text == searchs[i].searchField) {
	            if (formObj.field[j].selectType == '0') {
	              tempJs += 'var ' + formObj.field[j].text + 'ComboboxData=[]; \n '
	              tempJs += '$.ajax({ \n url: basePath+"/crm/ActionFormUtil/getByTableName.do?tableName='+formObj.field[j].selectID+'", \n type: "POST", \n '
	              tempJs += 'dataType:"json", \n data:{}, \n async:false, \n error:function(){ \n messageloadError(); \n '
	              tempJs += '}, \n success:function(data){ \n ' + formObj.field[j].text + 'ComboboxData=data.rows; \n '
	              tempJs += ' \n } \n }); \n '
	              tempJs += "$('#"+searchs[i].searchId+"').combobox({data:"+searchs[i].searchField+"ComboboxData,valueField:'id',textField:'text',editable:false});"
	            } else if (formObj.field[j].selectType == '1') {url:basePath+'/develop/url/getUrl.do?name="+field.selectID+"',
	              tempJs += "$('#"+searchs[i].searchId+"').combobox({url:basePath+'/develop/url/getUrl.do?name="+formObj.field[j].selectID+"',valueField:'id',textField:'text',editable:false});"
	            }
	            break
	          }
	        }

	      } else if (searchs[i].searchType == 'datebox') {
	        temp += '<input class="easyui-datebox" id="'+searchs[i].searchId+'" data-options="prompt:\''+searchs[i].searchName+'\'">'
	        tempJs += "$('#"+searchs[i].searchId+"').datebox({editable:false});"
	      } else if (searchs[i].searchType == 'monthbox') {
	        temp += '<input class="form-order-input" id="'+searchs[i].searchId+'" placeholder="'+searchs[i].searchName+'" style="width:70px;vertical-align:top;height:24px;line-height:24px;">'
	        tempJs += "jeDate('#"+searchs[i].searchId+"',{format:'YYYY-MM'});"
	      } else {
	        temp += '<input class="easyui-textbox" id="'+searchs[i].searchId+'" data-options="prompt:\''+searchs[i].searchName+'\'">'
	        tempJs += "$('#"+searchs[i].searchId+"').textbox({});"
	      }
	    }
		}
	  for(var i=0; i<searchButtons.length; i++){
	    temp += '<a href="#" id="'+searchButtons[i].buttonId+'" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="'+searchButtons[i].buttonAttr+'" onclick="'+searchButtons[i].buttonFunction+'" style="margin-left: 0px">'+searchButtons[i].buttonName+'</a>'
	    tempJs += "$('#"+searchButtons[i].buttonId+"').linkbutton({});"
		}
	  temp += '</form>'
	  $('#tb').append(temp)
	  var script = document.createElement("script")
	  script.type = "text/javascript"
	  try {
	    script.appendChild(document.createTextNode(tempJs))
	  } catch (ex) {
	    script.text = tempJs
	  }
	  document.body.appendChild(script)
	}

function initTableJs (jsCode) {
  jsCode = jsCode.replace(/&quot;/g, '"').replace(/换行符/g, '\n')
  var script = document.createElement("script")
  script.type = "text/javascript"
  try {
    script.appendChild(document.createTextNode(jsCode))
  } catch (ex) {
    script.text = jsCode
  }
  document.body.appendChild(script)
}
// 调整数据页结构
function initTableCon (result) {
  if (result.needTree == 'true' && result.treeForm!='') {
    $('#formTableLeft').css('width', '30%')
    $('#formTableRight').css('width', '70%')
    $('#formTableLeft').html('<table id="dg_tree" style="width:100%;height:100%;"></table>')
    $.ajax({
      url: basePath+"/pages/crminterface/getDatagridForJson.do?tableName="+result.treeForm,
      type: "POST",
      dataType:'json',
      error: function() //失败
      {
        messageloadError()
      },
      success: function(data)//成功
      {
        treeField = data.treeField
        var treeColumns = initColumns(data.field, false)
        $.ajax({
          url: basePath+"/crm/ActionFormUtil/getByTableName.do",
          type: "POST",
          dataType:'json',
          data:
          {
            'tableName': result.treeForm
          },
          error: function() //失败
          {
            messageloadError()
          },
          success: function(data)//成功
          {
            var treeData = dataConvertForTree(data)
            $('#dg_tree').treegrid({
              //url: basePath+"/crm/ActionFormUtil/getByTableName.do?tableName="+result.treeForm+"&create_user_id="+user.id,
              data:treeData,
              columns:treeColumns,
              idField:'id',
              treeField:treeField,
              onClickRow: function (rowIndex) {
                var rr_row = $('#dg_tree').treegrid('getSelected')
                /* if (!checkNull(dataUrl)) {
                  $('#dg').datagrid({
                    url:basePath+'/develop/url/getDataUrlByPid.do?name='+dataUrl+'&pid='+rr_row.id,
                    columns:columns,
                    rownumbers:true,
                    singleSelect:true,
                    autoRowHeight:false,
                    pagination:true,
                    fitColumns:true,
                    striped:true,
                    toolbar:'#tb',
                    pageSize:20
                  })
                } else { */
                  $('#dg').datagrid({
                    url:basePath+'/crm/ActionFormUtil/getByTableNameAndUserIdAndPid.do?tableName='+formName+'&create_user_id='+user.id+'&pid='+rr_row.id,
                    columns:columns,
                    rownumbers:true,
                    singleSelect:true,
                    autoRowHeight:false,
                    pagination:true,
                    fitColumns:true,
                    striped:true,
                    toolbar:'#tb',
                    pageSize:20
                  })
                // }
              }
            })
          }
        })
      }
    })
  } else {
    $('#formTableLeft').css('width', '0')
    $('#formTableRight').css('width', '100%')
  }
}
//设置树表格关联字段值
function setRelateFieldValue (formObj, relate_id, flag) {
  $.ajax({
    url: basePath+"/crm/ActionFormUtil/getByTableName.do?tableName="+formObj.treeForm,
    type: "POST",
    dataType:'json',
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      var comboboxData = data.rows
      $.ajax({
        url: basePath+"/pages/crminterface/getDatagridForJson.do",
        type: "POST",
        dataType:'json',
        data:
        {
          'tableName': formObj.treeForm
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          var treeField = data.treeField
          var width = 320
          if (formObj.columnNumber == 3) {
            width = 177
          } else if (formObj.columnNumber == 4) {
            width = 109
          } else if (formObj.columnNumber == 1) {
            width = 740
          } else {
            width = 320
          }
          $('#'+formObj.relateField).combobox({
            valueField: 'id',
            textField: treeField,
            data: comboboxData,
            value: relate_id,
            width: width,
            editable: false,
            readonly:flag
          })
        }
      })
    }
  })
}
// 开始上传文件
function openUploadFile (field) {
  var paths = $('#'+field).textbox('getValue')
  layer.open({
    type: 2,
    title: "管理上传文件",
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ["900px", "440px"],
    btn: "关闭",
    content: basePath+"/pages/formDevelopment/filesManage.jsp?paths="+paths,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      var filePath = Ifame.close()
      $('#'+field).textbox('setValue', filePath)
      layer.close(index)
    }
  })
}
// 查看文件
function viewFiles (paths) {
  layer.open({
    type: 2,
    title: "查看文件",
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ["800px", "360px"],
    btn: "关闭",
    content: basePath+"/pages/formDevelopment/viewFiles.jsp?paths="+paths,
    yes:function(index){
      layer.close(index)
    }
  })
}
// 取数字字段
function getNumberFields (fields) {
  var arr = []
  for (var i = 0; i < fields.length; i++) {
    if (fields[i].fieldType == 'numberbox') {
      delete fields[i].disabled
      arr.push(fields[i])
    }
  }
  return arr
}
// 数据转换为树结构数据格式
function dataConvertForTree (tableData) {
  var datas = tableData.rows
  var arr = []
  for (var i = 0; i < datas.length; i++) {
    datas[i].pid = parseInt(datas[i].pid)
    if (datas[i].pid == 0 || datas[i].pid == '0') {
      datas[i].children = []
      for (var j = 0; j < datas.length; j++) {
        datas[j].children = []
        for (var z = 0; z < datas.length; z++) {
          if (datas[z].pid == datas[j].id) {
            datas[j].children.push(datas[z])
          }
        }
        if (datas[j].pid == datas[i].id) {
          datas[i].children.push(datas[j])
        }
      }
      arr.push(datas[i])
    }
  }
  return arr
}
function addConfigJs (formObject) { // 插入配置js
  $.ajax({
    url: basePath+"/pages/button/framework/get.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'title': formObject.title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      if (data.obj != null) {
        initTableJs(data.obj.js_code)
      }
    }
  })
}
function viewProcessDetail(uuid){
  layer.open({
	    type: 2,
	    title: '查看流程详细',
	    shadeClose: false,
	    shade: 0.3,
	    maxmin: true, //开启最大化最小化按钮
	    area: ['900px', document.body.clientHeight-20+'px'],
	    btn: '关闭',
	    content: '/pages/formDevelopment/processDetail.jsp?uuid=' + uuid,
	    yes:function(index){
	      layer.close(index)
	    }
	  })
	
}
//导入数据
function functionImport (tableName) {
  layer.open({
    type: 2,
    title: '导入数据',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['400px', '250px'],
    btn: '关闭',
    content: '/pages/formDevelopment/import.jsp?title='+tableName,
    yes:function(index){
      layer.close(index)
    }
  })
}