var isAddPremission = function (permissionssJson)
{
    if(permissionssJson !=null)
    {
        console.info(permissionssJson.isAdd);
        if(permissionssJson.isAdd==true)
        {}       
        else
        {
            $.messager.alert("没有权限", "<font size='4px'>您没有添加该数据的权限！</font>", "warning");
        }
        return permissionssJson.isAdd;        
    }
    return false;
}
var isDeletePremission = function (permissionssJson)
{
    if(permissionssJson !=null)
    {
        if(permissionssJson.isDelete==true){}        
        else
        {
            $.messager.alert("没有权限", "<font size='4px'>您没有删除该数据的权限！</font>", "warning");
        }
        return permissionssJson.isDelete;        
    }
    return false;
}
var isModifyPremission = function (permissionssJson)
{
    if(permissionssJson !=null)
    {
        if(permissionssJson.isModify==true){}        
        else
        {
            $.messager.alert("没有权限", "<font size='4px'>您没有修改该数据的权限！</font>", "warning");
        }
        return permissionssJson.isModify;        
    }
    return false;
}
var isQueryPremission = function (permissionssJson)
{
    if(permissionssJson !=null)
    {
        if(permissionssJson.isQuery==true){}        
        else
        {
            $.messager.alert("没有权限", "<font size='4px'>您没有检索该数据的权限！</font>", "warning");
        }
        return permissionssJson.isQuery;        
    }
    return false;
}
var isDeleteAndModifyPremission = function (permissionssJson)
{
    if(permissionssJson !=null)
    {
        if(permissionssJson.isDelete==false&&permissionssJson.isModify==false)        
        {
            $.messager.alert("没有权限", "<font size='4px'>您没有修改/删除该数据的权限！</font>", "warning");
            return false;   
        }
        return true;
    }
    return false;
}