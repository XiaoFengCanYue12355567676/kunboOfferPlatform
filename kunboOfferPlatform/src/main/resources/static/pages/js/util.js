// document.write("<script language=javascript src='../moment.min.js'></script>");


var formatDateTime = function (date)
{
    if(checkNull(date))
    {
        return '';
    }
    else
    {
        var tmp = date.replace(':000','');
        var ret = moment(tmp).format('YYYY-MM-DD HH:mm:ss');
        console.info('ret = '+ret);
        return ret;
    }
}
// 获得date日期x月后的时间
function getXMonthLaterDate(date,xmonth)
{
    var d = moment(formatDateTime(date));
    var t = d.add(xmonth, "months").format("YYYY-MM-DD HH:mm:ss");
    console.log(t);
    return t;
}
var formatDateTimeYMD = function (date)
{
    if(checkNull(date))
    {
        return '';
    }
    else
    {
        var tmp = date.replace(':000','');
        var ret = moment(tmp).format('YYYY-MM-DD');
        console.info('ret = '+ret);
        return ret;
    }
}

var  datagridFormatDate = function(val,row,index)
{
    return formatDateTime(val);
}
var  datagridFormatDateYMD = function(val,row,index)
{
    return formatDateTimeYMD(val);
}

var getSystemDate = function()
{
    return moment().format('YYYY-MM-DD HH:mm:ss');
    // var curr_time = new Date();
    // return formatDateTime(curr_time);
}
var maskOn = function(){
    $("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(document).height()}).appendTo("body");
    $("<div class=\"datagrid-mask-msg\"></div>").html("").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190)/2,top:($(window).height() - 45)/2});
 }
var maskOff = function(){
     $(".datagrid-mask").remove();
     $(".datagrid-mask-msg").remove();
}

var checkNull = function(v)
{
    if (v == null || v == undefined || v == '')
        return true;
    else
        return false;
}


var messageSaveOK = function()
{
    $.messager.show({
        title:'保存',
        msg:'保存成功',
        timeout:4000,
        showType:'slide'
    });
}
var messageDeleteOK = function()
{
    $.messager.show({
        title:'删除',
        msg:'删除成功',
        timeout:4000,
        showType:'slide'
    });
}
var messageSubmitOK = function()
{
    $.messager.show({
        title:'提交',
        msg:'提交成功',
        timeout:4000,
        showType:'slide'
    });
}
var messageModifyOK = function()
{
    $.messager.show({
        title:'修改',
        msg:'修改成功',
        timeout:4000,
        showType:'slide'
    });
}
var messageloadOK = function()
{
    $.messager.alert('提醒',"<font size='4px'>加载成功</font>",'warning');
}

var messageloadOK1 = function()
{
    messageOkCustom('载入','载入成功');
}
var messageOkCustom = function(title,msg)
{
    $.messager.show({
        title:title,
        msg:msg,
        timeout:4000,
        showType:'slide'
    });
}



var messageSaveError = function()
{
    $.messager.alert('提醒',"<font size='4px'>保存失败</font>",'warning');
}
var messageSubmitError = function()
{
    $.messager.alert('提醒',"<font size='4px'>提交失败</font>",'warning');
}
var messageloadError = function()
{
    $.messager.alert('提醒',"<font size='4px'>加载失败</font>",'warning');
}
var messageModifyError = function()
{
    $.messager.alert('提醒',"<font size='4px'>修改失败</font>",'warning');
}
var messageDeleteError = function()
{
    $.messager.alert('提醒',"<font size='4px'>删除失败</font>",'warning');
}

var messageCustom = function(customStr)
{
    $.messager.alert('提醒',"<font size='4px' style='margin-top:10px'>"+customStr+"</font>",'warning');
}

var confirmCustom1Btn = function(btn,title,showMsg,doFunction)
{
    $.messager.defaults = { ok: btn};
    $.messager.confirm(title, "<font size='3px'>"+showMsg+"</font>", function(r){
        if (r){
            console.info('doFunction');
            doFunction();
        }
    });
}
var confirmCustom2Btn = function(okBtn,cancelBtn,title,showMsg,okFunction,cancelFunction)
{
    $.messager.defaults = { ok: okMsg,cancel: cancelMsg};
    $.messager.confirm(title, "<font size='3px'>"+showMsg+"</font>",
    function (defaults) {
        if (defaults)
        {
            okFunction();
        }
        else
        {
            cancelFunction();
        }
    });
}




////////////////////////////////////////////datagrid////////////////////////////////////////////
/*
获得datagrid选中的行数
 */
var datagridGetSelectRowSize = function(datagrid)
{
    try
    {
        var rows = $('#'+datagrid).datagrid("getChecked");
        return rows.length;
    }
    catch(e)
    {
        console.info( e.message);
        return 0;
    }
}

function ajaxInsertClass(url,clazz,json,error,success)
{
    $.ajax({
        url: url+"/pages/interface/data/inserClass.jsp",
        type: "POST",
        dataType:'json',
        data:
        {
            "clazz": clazz,
            "json": json
        },
        error: function() //失败
        {
            error();
        },
        success: function(data)//成功
        {
            success(data);
        }
    });
}


function ajaxUpdateClass(url,clazz,json,error,success)
{
    $.ajax({
      url: url+"/pages/interface/data/updateClass.jsp",
      type: "POST",
      dataType:'json',
      data:
      {
        "clazz": clazz,
        "json": json
      },
      error: function() //失败
      {
        error();
      },
      success: function(data)//成功
      {
        success();
      }
    });
}
function ajaxDeleteClass(url,clazz,json,error,success)
{
    $.ajax({
        url: url+"/pages/interface/data/deleteClass.jsp",
        type: "POST",
        dataType:'json',
        data:
        {
            "clazz": clazz,
            "json": json
        },
        error: function() //失败
        {
            error();
        },
        success: function(data)//成功
        {
            success();
        }
    });
}

////////////////////////////////////////////layer////////////////////////////////////////////
var layerSaveMaskOn = function()
{
    var index = layer.load(1, {
        icon: 2,
        shade: [0.5,'#fff'] //0.1透明度的白色背景
    });
    return index;
}
var layerSaveMaskOff = function()
{
    layer.closeAll('loading'); //关闭加载层
}

var layerMsgCustom = function(content)
{
    layer.msg(content, function(){});
}

var layerAlertAndClose = function(msg)
{
    layer.alert(
        msg+'<br/>点击确定按钮后将会关闭本页面',
        {icon: 2,closeBtn: 0 },
        function () {
            console.info('');
            window.close();
    });
}

////////////////////////////////////////////windows////////////////////////////////////////////
//在屏幕中间打开一个页面
var openWindowsInScreenMiddle = function(url,height,width)
{
    var top=Math.round((window.screen.height-height)/2);
    var left=Math.round((window.screen.width-width)/2);
    window.open(url, '_blank', 'height='+height+'px, width='+width+'px, top='+top+'px,left='+left+'px, scrollbars=yes, resizable=yes');
}


////////////////////////////////////////////链接////////////////////////////////////////////
function UrlSearch()
{
   var name,value;
   var str=location.href; //取得整个地址栏
   var num=str.indexOf("?")
   str=str.substr(num+1); //取得所有参数   stringvar.substr(start [, length ]

   var arr=str.split("&"); //各个参数放到数组里
   for(var i=0;i < arr.length;i++){
    num=arr[i].indexOf("=");
    if(num>0){
     name=arr[i].substring(0,num);
     value=arr[i].substr(num+1);
     this[name]=value;
     }
    }
}


//部门人员搜索
function changeDepartment () {
  var department_id = $("#searchDepartment option:selected").val()
}
function submitSearchRoleForm () {
  var department_id = $("#searchDepartment option:selected").val()
  var role_id = $("#searchRole option:selected").val()
  var startDate = $("#startDate").val()
  var endDate = $("#endDate").val()
  if (!checkNull(startDate) && !checkNull(endDate)) {
    var startTmp = new Date(startDate.replace(/-/g, '/'))
    var endTmp = new Date(endDate.replace(/-/g, '/'))
    if (startTmp > endTmp) {
      layerMsgCustom('开始时间不能大于结束时间')
  		return false
    }
  }
}


function addRules()
{
    var obj = $('.easyui-linkbutton').each(function(){
        // alert(this)//这里 this获得的就是每一个dom对象 如果需要jquery对象 需要写成$(this)
        for(var i=0;i<rules.length;i++)
        {
            if(rules[i].component_style == 'linkbutton'&&rules[i].component_id == this.id)
            {
                console.info('component_style = '+rules[i].component_style);
                console.info('component_id = '+rules[i].component_id);
                console.info('id = '+this.id);

                if(rules[i].is_use == '0')
                    $('#'+rules[i].component_id).linkbutton('disable');
                if(rules[i].is_use == '1')
                    $('#'+rules[i].component_id).linkbutton('enable');
            }
        }
    });
}

//浮点数加法运算
function FloatAdd(arg1,arg2){
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
}
function FloatAdds(arr){
  arr = arrDeletEmpty(arr)
  if (arr.length == 0) {
    return 0.00
  }
  var tmp = arr[0]
  if (arr.length == 1) {
    return tmp
  }
  for (var i = 0; i < arr.length-1; i++) {
    tmp = FloatAdd(tmp,arr[i+1])
  }
  return tmp
}
//浮点数减法运算
function FloatSub(arg1,arg2){
    var r1,r2,m,n;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    //动态控制精度长度
    n=(r1=r2)?r1:r2;
    return ((arg1*m-arg2*m)/m).toFixed(n);
}

//浮点数乘法运算
function FloatMul(arg1,arg2)
{
    var m=0,s1=arg1.toString(),s2=arg2.toString();
    try{m+=s1.split(".")[1].length}catch(e){}
    try{m+=s2.split(".")[1].length}catch(e){}
    return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m);
}
function FloatMuls(arr)
{
    arr = arrDeletEmpty(arr)
    if (arr.length == 0) {
        return 0.00
    }
    var tmp = arr[0]
    if (arr.length == 1) {
        return tmp
    }
    for (var i = 0; i < arr.length-1; i++) {
        tmp = FloatMul(tmp,arr[i+1])
    }
    return tmp
}

 //浮点数除法运算
/*function FloatDiv(arg1,arg2){
    var t1=0,t2=0,r1,r2;
    try{t1=arg1.toString().split(".")[1].length}catch(e){}
    try{t2=arg2.toString().split(".")[1].length}catch(e){}
    with(Math){
        r1=Number(arg1.toString().replace(".",""));
        r2=Number(arg2.toString().replace(".",""));
        return (r1/r2)*pow(10,t2-t1);
    }
}*/
function FloatDiv(arg1,arg2){
    var t1=0,t2=0,r1,r2;
    try{t1=arg1.toString().split(".")[1].length}catch(e){}
    try{t2=arg2.toString().split(".")[1].length}catch(e){}
    with(Math){
        r1=Number(arg1.toString().replace(".",""));
        r2=Number(arg2.toString().replace(".",""));
        var temp = parseFloat((r1/r2)*pow(10,t2-t1)*100)
        console.log(typeof temp)
        return temp.toFixed(2)+'%';
    }
}
//数组去空值
function arrDeletEmpty (arr) {
  for(var i = 0;i<arr.length;i++){
    if(arr[i]==''||arr[i]==null||typeof(arr[i])==undefined){
        arr.splice(i,1)
        i=i-1
    }
  }
  return arr
}
