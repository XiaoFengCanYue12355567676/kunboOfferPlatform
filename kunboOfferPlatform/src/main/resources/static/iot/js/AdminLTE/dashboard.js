/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".box-header, .nav-tabs").css("cursor","move");

    
    /* Morris.js Charts */


    //Bar chart
    var bar = new Morris.Bar({
        element: 'bar-chart',
        resize: true,
        data: [
            {y: '2017-01', a: 100, b: 90},
            {y: '2017-02', a: 75, b: 65},
            {y: '2017-03', a: 50, b: 40},
            {y: '2017-04', a: 75, b: 65},
            {y: '2017-05', a: 50, b: 40},
            {y: '2017-06', a: 75, b: 65},
            {y: '2017-07', a: 100, b: 90},
            {y: '2017-08', a: 100, b: 80},
            {y: '2017-09', a: 90, b: 85},
            {y: '2017-10', a: 85, b: 80},
            {y: '2017-11', a: 80, b: 80},
            {y: '2017-12', a: 90, b: 85}
        ],
        barColors: ['#00a65a', '#f56954'],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['销售额', '回款额'],
        hideHover: 'auto'
    });
    //Fix for charts under tabs
    $('.box ul.nav a').on('shown.bs.tab', function(e) {
        area.redraw();
        donut.redraw();
    });




});
