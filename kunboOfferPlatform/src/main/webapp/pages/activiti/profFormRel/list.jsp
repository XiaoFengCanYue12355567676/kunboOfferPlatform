<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>模型-表单关系管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
	<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>
    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

    <div data-options="region:'center',title:'',split:true" style="width:100%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow
                ">
					<thead>
						<tr href="#">
			                 
							<th field="id" align="center">模型编码</th>
							<th field="proDefKey" align="center">流程关键字</th>
              				<th field="formTableName" align="center">表单关联表名</th>
							<th field="formUrl" align="center">表单地址</th>
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <a id='btnNewPFR' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="profFromRel()">新建</a>
          <a id='btnEditPFR' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="update()">编辑</a>
          <a id='btnDeletePFR' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="deleteById()">删除</a>
        </div>
        <input type="hidden" id="pfrId" >
     </div>
		
 
  
  <script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/profFormRel/getProfFormRelList',
        queryParams:
        {
        },
        method:'get'
      })
    });
  </script>
  <script type="text/javascript">
  	function onClickRow(rowIndex,rowData){
  		$('#pfrId').val(rowData.id);
  	}
  </script>
<script type="text/javascript">
function profFromRel(){
	layer.open({
		type: 2,
        title: '新增流程-表单关系',
        shadeClose: false,
        shade: 0.3,
        maxmin: true, //开启最大化最小化按钮
        area: ['900px', '550px'],
        content:"<%=basePath%>/profFormRel/add",
        btn:['提交','取消'],
        yes: function(index, layero){
        	
            //按钮【按钮一】的回调
       	var res = window["layui-layer-iframe" + index].callbackdata();
       	//最后关闭弹出层
       	layer.close(index);
       	$.ajax({
               url: "<%=basePath%>/profFormRel/addProfFormRel",
       				type : "POST",
       				dataType : 'json',
       				data : {
       					str : JSON.stringify(res)
       				},
       				success : function(result)//成功
       				{
       					layer.msg(result.msg);
       				}
       	});
       	location.reload();
       },
	});
}

function update(id){
	layer.open({
		type: 2,
        title: '新增流程-表单关系',
        shadeClose: false,
        shade: 0.3,
        maxmin: true, //开启最大化最小化按钮
        area: ['900px', '550px'],
        content:"<%=basePath%>/profFormRel/update/"+$("#pfrId").val(),
        btn:['提交','取消'],
        yes: function(index, layero){
            //按钮【按钮一】的回调
       	var res = window["layui-layer-iframe" + index].callbackdata();
       	//最后关闭弹出层
       	layer.close(index);
       	$.ajax({
               url: "<%=basePath%>/profFormRel/updateProfFormRelById",
       				type : "POST",
       				dataType : 'json',
       				data : {
       					str : JSON.stringify(res)
       				},
       				success : function(result)//成功
       				{
       					layer.msg(result.msg);
       				}
       	});
       	location.reload();
       },
	});
}  
function deleteById(){
	  var row = $('#dg').datagrid('getSelected');
		if(null==row){
			alert("您还没选中要操作的项目");
			return false;
		}
		var msgRet = confirmMsg("确定要删除" + row.proDefKey + "这一项吗？",function(index){
			deleteIt(row.id);
			layer.close(index);
		});
}
function deleteIt(id){
	$.ajax({
        		url: "<%=basePath%>/profFormRel/deleteProfFormRelById",
				type : "POST",
				dataType : 'json',
				data : {
					id : id
				},
				success : function(result)//成功
				{
					layer.msg(result.msg);
					location.reload();
				},
	});
	
} 
</script>
</body> 
</html>

