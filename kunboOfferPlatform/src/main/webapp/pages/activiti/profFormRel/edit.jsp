<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加流程-表单关联</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="<%=basePath%>/js/common.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
</head>
<body style="text-align: right;">
	<input id = "oldId" type="hidden" value="${old.id}">
	<input id = "oldKey" type="hidden" value="${old.proDefKey}">
	<input id = "oldformTableName" type="hidden" value="${old.formTableName}">
	<input id = "oldformUrl" type="hidden" value="${old.formUrl}">
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" id="form" style="margin-left: 0px;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">流程-表单关联</span>
                </div>
            </td>
        </tr>         
        <tr>
            <td class="label">流程关键字</td>
            <td >
                <select id='key' name = "key" class="easyui-combobox" 
                data-options="required:true,
                width:400,editable:false,
                valueField: 'name',
    			textField: 'name',
    			url: '<%=basePath%>/flow/getDeployed',
    			onLoadSuccess:onLoadSuccess
    			">
                </select>
            </td>
        </tr>
        <tr>
        	<td class="label">表名称</td>
            <td >
                <input id='formTableName' name='formTableName' class="easyui-textbox" data-options="required:true,width:400,editable:true">
            </td> 
        </tr>   
        <tr>
        	<td class="label">表单地址</td>
            <td >
                <input id='formUrl' name='formUrl' class="easyui-combobox" data-options="required:true,width:400,editable:true">
            </td>  
        </tr>      
    </table>
    <script type="text/javascript">
        var count = 1;
	    $(document).ready(function(){
	    	$("#formTableName").combobox('setValue',$("#oldformTableName").val());
	    	$("#formUrl").textbox('setValue',$("#oldformUrl").val());
		});
        var callbackdata = function () {
            var param =   {
            	id:$("#oldId").val(),
            	proDefKey: $("#key").combobox('getValue'),
            	formTableName:$("#formTableName").val(),
            	formUrl:$("#formUrl").val(),  
            };
            return param;
        }
        var linkUrl = '/pages/formDevelopment/newFormOrder.jsp?title=';
        $('#formTableName').combobox({
        	required:true,
            width:400,
            editable:true,
            valueField: 'text',
			textField: 'text',
			url: '<%=basePath%>/pages/crminterface/getAllTableName.do',
			onSelect: function (rec) {
				count++;
				$.ajax({
		               url: "<%=basePath%>/profFormRel/getModelRelForm",
		       				type : "POST",
		       				dataType : 'json',
		       				data : {
		       					tabName : rec.text
		       				},
		       				success : function(result)//成功
		       				{
		       					if(result.status=='1' && count>2){
		       						layer.msg('该表单已经关联了流程模型，请检查！');
		       					}
		       					
		       				}
		       	});
				 linkUrl += rec.text;
				 $("#formUrl").textbox("setValue", linkUrl);
			}
		 
        })
        function onLoadSuccess(){
        	$("#key").combobox('select',$("#oldKey").val());
        }
        
    </script>
</body> 
</html>