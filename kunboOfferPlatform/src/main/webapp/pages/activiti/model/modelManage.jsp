<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>模型管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script src="<%=basePath%>/js/ajax-util.js"></script>
	<script src="<%=basePath%>/lib/js/template.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

    <div data-options="region:'center',title:'',split:true" style="width:100%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow
                ">
					<thead>
						<tr href="#">
			                 
							<th field="id" align="center">模型编码</th>
							<th field="key" align="center" hidden="true">关键字</th>
              				<th field="name" align="center" formatter="checkFlag">名称</th>
							<th field="createTime" align="center">创建时间</th>
							<th field="lastUpdateTime" align="center">最终编辑时间</th>
							<th field="revision" align="center">修订版本</th>
							<th field="version" align="center">版本</th>
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <a id='btnNewModelar' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="creatModeler('1')">新建</a>
          <a id='btnEditModelar' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="EditModelar('2')">编辑</a>
          <a id='btnDeleteModelar' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" >删除</a>
          <a id='btnPublishModelar' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-send-o',plain:true" onclick="publishModeler()">发布</a>
          <input type="text" id="deployPath" value="" hidden="hidden">
        </div>
     </div>
	
 
  
  <script type="text/javascript">
  	var flag="";
  	var nowId="";
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/models/getList.do',
        queryParams:
        {
        },
        method:'get'
      })
    });
  </script>
  <script type="text/javascript">
  	function onClickRow(rowIndex,rowData){
  		//$('#btnEditModelar').attr("href",'/modeler.html?modelId='+rowData.id);
  		nowId=rowData.id;
  		$('#btnDeleteModelar').attr("href","/models/delete/"+rowData.id);
  		$('#deployPath').attr("value","/models/"+rowData.id+"/deployment");
  	}
  </script>
<script type="text/javascript">
  
function checkFlag(value,row,index){
	if (row.id == flag) {
		value += '<span style="color:red;">(未启动)</span>'
	}
	return value;
}
  function creatModeler(f){
	  $.ajax({
	        url: '<%=basePath%>/models/newModel',
	        type : "POST",
	        dataType : 'json',
	        data : {   
	        },
	        success : function(result){
	            var perContent = layer.open({
	            	type: 2,
	                title: '新建模型',
	                shadeClose: false,
	                shade: 0.3,
	                maxmin: true, //开启最大化最小化按钮
	                area: ['700px', '600px'],
	                content: '<%=basePath%>/modeler.html?modelId='+result.obj,
	            	cancel: function(index, layero){ 
	            		layer.close(index);
	                	$('#dg').datagrid('load');
	                	flag=result.obj
	            	},
	            	end: function(){
	            		$('#dg').datagrid('load');
	            		flag=result.obj
	            	}
	            });
	        	layer.full(perContent);
	        }
	    });
	
  }
  function EditModelar(f){
	  var rows = $('#dg').datagrid('getSelections');
		 if (rows.length == 0 && f=='2') {
			 layer.msg('请先选择要编辑的流程模型');
			return;
		}
	  var perContent = layer.open({
	            	type: 2,
	                title: '修改模型',
	                shadeClose: false,
	                shade: 0.3,
	                maxmin: true, //开启最大化最小化按钮
	                area: ['700px', '600px'],
	                content: '<%=basePath%>/modeler.html?modelId='+nowId,
	            	cancel: function(index, layero){ 
	            		layer.close(index);
	                	$('#dg').datagrid('load');
	                	flag=nowId
	            	},
	            	end: function(){
	            		$('#dg').datagrid('load');
	            		flag=nowId
	            	}
	            });
	   layer.full(perContent);
	        
	
  } 
  
  function publishModeler(){
	  var path = $('#deployPath').val();
	  $.ajax({
	        url: path,
	        type : "POST",
	        dataType : 'json',
	        data : {   
	        },
	        success : function(result)//成功
	        {        
	            layer.msg(result.msg);
	            flag='';
	            $('#dg').datagrid('load');
	        }
	    });
  }
</script>
</body> 
</html>

