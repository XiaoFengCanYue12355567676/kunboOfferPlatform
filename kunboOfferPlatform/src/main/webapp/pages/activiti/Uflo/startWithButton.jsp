<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>启动流程</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/jquery.json.min.js"></script>
    <script src="<%=basePath%>/js/ajax-util.js"></script>
	<script src="<%=basePath%>/lib/js/template.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
	<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

    <div data-options="region:'center',title:'',split:true" style="width:100%;">
        <div class="field-con">
        	<button id="addVariable" class="easyui-linkbutton" onclick="addVariable()">添加变量</button>
        	<form id="form">
            <table id="table" style="cellpadding:40px;cellspacing:40px;">
	            <tr>
	            	<td>变量名</td>
	            	<td>值</td>
	            	<td>操作</td>
	            </tr>
            </table>
            </form>
        </div>
     </div>
	
 

  <script type="text/javascript">
  var p = null;
  var c = null;
  var v = null;
  var count = 1;
  	var callbackdata = function() {
		var data = {
			variable : $("#form").serialize(),
		}
		return data;
	}
  	function addVariable(){
  		var id = "ids"+count;
  		$("#table").append('<tr id="'+id+'"><td><input name="key" class="easyui-textbox"></td><td><input name="value" class="easyui-textbox"></td><td><button id="del'+count+'" class="easyui-linkbutton" onclick="deleteTr('+id+')">删除</button></td></tr>');
  		count+=1;
  	}
  	function deleteTr(id){
  		var child=$(id);
  		child.remove();
  	}
  </script>
</body> 
</html>

