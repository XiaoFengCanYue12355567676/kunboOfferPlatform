<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>待办任务</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>
	<div data-options="region:'west',title:'',split:true"
		style="width: 40%;">
		<!-- 左侧模板列表 -->
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">编号</th>
					<th field="name" align="center">模板名称</th>
					<th field="key" align="center">模板key</th>
					<th field="startProcessUrl" align="center" hidden="true">任务启动地址</th>
					<th field="version" align="center">版本</th>
					<th field="effectDate" align="center" hidden="true">生效时间</th>
					<th field="categoryId" align="center" hidden="true">分类编号</th>
					<th field="createDate" align="center">创建时间</th>
					<th field="description" align="center">描述</th>
					<th field="tag" align="center" hidden="true">标签</th>
					<th data-options="field:'_operate',width:80,align:'center'">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<input id='name' class="easyui-textbox">&nbsp;&nbsp;模板名称
			&nbsp;&nbsp;<button id="search" class="easyui-button" onclick="reload()">查询</button>
			<a id='completeTask' href="javascript:void(0)"
				class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="startProcess()">启动流程</a> 
			<a id='viewProcessDiagram'
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="showPic()">查看流程规划</a>
			<a id='endprocess' href="javascript:void(0)"
				class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="deleteProcess()">删除模板</a>
			<input id='processId' value="" hidden="hidden">
			<input id='key' value="" hidden="hidden">
			<input id='name' value="" hidden="hidden">
		</div>
	</div>
	<!-- 右侧区域  -->
	<div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:60%; ">
		<div data-options="region:'north',iconCls:'icon-reload',title:'运行中的任务',split:true" style="width:100%;height: 50%">

		<!-- 上半部运行中实例列表 -->
      <div id="blockTop" class="easyui-tabs" style="width:100%;height:100%;">  
          <div title="运行中实例与任务" style="padding:0px;display:none;">  
              
                  <div id="topleft" style="height: 35px;text-align: center;">
                      <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="addOrganization()">新增</a>  
                      <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="modifyOrganization()">修改</a>  
                      <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-trash" style="margin-top: 2px" onclick="deteleOrganization()">删除</a>                 
                  </div>              

                  <table id="tableRunning" class="easyui-datagrid" style="height:100%;width: 100%;" title="" data-options="
                          idField:'id',
                          treeField:'text',
                          method:'get',
                          toolbar:'#topleft'
                          ">
                    <thead>
                        <tr>
                            <th field="text">客户列表</th>
                            <th field="text1">客户列表</th>
                            <th field="id" width="0px" hidden="true">序列</th>
                        </tr>
                    </thead>
                  </table>        
           </div>
           <!-- 上半部历史实例列表 -->
          <div title="历史实例与任务" data-options="closable:false" style="overflow:auto;padding:0px">  
                      <div id="dg_tb3" style="height: 35px;text-align: center;">
                        <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="addOrganization()">新增</a>  
                        <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="modifyOrganization()">修改</a>  
                        <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-trash" style="margin-top: 2px" onclick="deteleOrganization()">删除</a>                 
                    </div>              

                    <table id="tt3" class="easyui-datagrid" style="height:100%;width: 100%;" title="" data-options="
                            idField:'id',
                            treeField:'text',
                            method:'get',
                            toolbar:'#dg_tb3'
                            ">
                      <thead>
                          <tr>
                              <th field="text">客户列表222</th>
                              <th field="text1">客户列表222</th>
                              <th field="id" width="0px" hidden="true">序列</th>
                          </tr>
                      </thead>
                    </table>    
          </div>  
      </div>  


		<!-- 下半部任务详情列表  -->
		<div data-options="region:'south',iconCls:'icon-reload',title:'',split:true" style="width:100%;height: 100%">
        <div id="dg_tb2" style="height: 35px;text-align: center;">
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="addOrganization()">新增</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="modifyOrganization()">修改</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-trash" style="margin-top: 2px" onclick="deteleOrganization()">删除</a>                 
        </div>              

        <table id="tt2" class="easyui-datagrid"  style="height:100%;width: 100%;" title="" data-options="
                idField:'id',
                treeField:'text',
                method:'get',
                toolbar:'#dg_tb2'
                ">
          <thead>
              <tr>
                  <th field="text">客户列表1111</th>
                  <th field="text1">客户列表1111</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
	   </div>
	</div>
	</div>
	<script type="text/javascript">
	var processId;
	var compNow = null;
	var variable = null;
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/flow/loadProcess',
        queryParams:
        {
        	name:""
        },
        method:'get'
      })
    });
    function reload(){
    	$('#dg').datagrid('load', {
    		name:$("#name").val()
    	}); 
    }
  </script>
<script type="text/javascript">
	function onClickRow(rowIndex,rowData){
		$("#key").val(rowData.key);
		$("#processId").val(rowData.id);
		$("#name").val(rowData.name);
	}
	function startProcess(){
		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要启动的流程");
  			return false;
  		}
  		layer.open({
            type: 2,
            title: '当前进度',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['600px', '550px'],
            content:"<%=basePath%>/flow/startWithButton",
            btn:['启动并完成第一步','启动流程','取消'],
            yes: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	processId = $("#processId").val();
            	compNow = true;
            	if(res.variable!=""){
            		variable = res.variable
            	}
            	start(processId,compNow,variable,index)
            },btn2: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	processId = $("#processId").val();
            	compNow = false;
            	if(res.variable!=""){
            		variable = res.variable
            	}
            	start(processId,compNow,variable,index)
            	return false;
            }
        });
  		
	}
	function showPic(){
		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要查看的任务");
  			return false;
  		}
		layer.open({
            type: 2,
            title: '查看流程图',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1100px', '550px'],
            content:"<%=basePath%>/uflo/diagram?processKey="+$("#key").val()
        });
	}	
	function start(processId,compNow,variables){
		if(processId==""||processId==null){
			alert("缺少必要参数");
			return false;
		}
		$.ajax({
            url: "<%=basePath%>/flow/startProcess",
    		type : "POST",
    		dataType : 'json',
    		data : {
    				processId : processId,
					compNow : compNow,
					variables : variables,
			},
    		success : function(result)//成功
    		{
    			layer.msg(result.msg);
    			//最后关闭弹出层
    	        layer.close(index);
    		}
    	});
	}
	function deleteProcess(){
		processId=$("#processId").val();
		if(processId==""||processId==null){
			alert("缺少必要参数");
			return false;
		}
		var msgRet = confirmMsg("确定要删除" + $('#name').val() + "模板吗？这将删除所有相关流程信息",function(index){
			deleteIt();
  			layer.close(index);
  		});
	}
	//删除模板及所有相关实例
	function deleteIt(){
		
		$.ajax({
            url: "<%=basePath%>/flow/deleteProcess/"+processId,
    		type : "POST",
    		success : function(result)//成功
    		{
    			layer.msg(result.msg);
    			//最后关闭弹出层
    	        setTimeout("location.reload()",1500);
    		}
    	});
	}
	</script>
	<script>
	$(document).ready(function () {
        $('#name').textbox('textbox').keydown(function (e) {
            if (e.keyCode == 13) {
            	$('#search').click();
            }
        });
    });
  </script>
</body>
</html>
