<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>待办任务</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">任务编号</th>
					<th field="tips" align="center">流程別名</th>
					<th field="taskName" align="center">任务名称</th>
					<th field="description" align="center">任务描述</th>
					<th field="own" align="center">任务所有者</th>
					<th field="assignee" align="center">代理人</th>
					<th field="progress" align="center" hidden="true">进度</th>
					<th field="state" align="center" formatter="format">任务状态</th>
					<th field="processInstanceId" align="center" hidden="true">实例id</th>
					<th field="createDate" align="center">创建时间</th>
					<th field="prevTask" align="center">前置任务</th>
					<th field="nodeType" align="center">下一节点类型</th>
					<th field="nextNode" align="center">下一节点名称</th>
					<th field="url" align="center" hidden="true">办理地址</th>
					<th field="firBusinessId" align="center" hidden="true">前置关联表单id</th>
					<th field="firUrl" align="center" hidden="true">前置关联表单地址</th>
					<th data-options="field:'_operate',width:80,align:'center'" hidden="true">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<input id='taskName' class="easyui-textbox">&nbsp;&nbsp;任务名称&nbsp;&nbsp;
			<input id='tName' class="easyui-textbox">&nbsp;&nbsp;流程別名
			&nbsp;&nbsp;<button id="search" class="easyui-button" onclick="reload()">查询</button>
			<a id='completeTask' href="javascript:void(0)"
				class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="approveTask()">办理任务</a> <a id='viewProcessDiagram'
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="showPic()">查看任务进度</a>
			<!-- <a id='endprocess' href="javascript:void(0)"
				class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="">强制结束任务</a> -->
			<input id='url' value="" hidden="hidden"> <input id='taskId'
				value="" hidden="hidden"> <input id='processInstanceId'
				value="" hidden="hidden"> <input id='firBusinessId' value=""
				hidden="hidden"> <input id='firUrl' value="" hidden="hidden">
			<input id="nextNode" hidden="true">
		</div>
	</div>



	<script type="text/javascript">
	var flowName;
	var nodeName;
	var userName;
	var varaibles;
	var join = false;
	var countLine;
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/flow/loadTodo',
        queryParams:
        {
        },
        method:'get'
      })
    });
    function reload(){
    	$('#dg').datagrid('load', {
    		taskName:$("#taskName").val(),
    		tName:$("#tName").val()
    	}); 
    }
    function format(value, row, index){
    	if(value =="Ready"){
    		return "可领取";
    	}
    	if(value =="Completed"){
    		return "已完成";
    	}
    	if(value =="Forwarded"){
    		return "已跳转";
    	}
    	if(value =="Created"){
    		return "已创建";
    	}if(value =="Reserved"){
    		return "预分配";
    	}if(value =="Withdraw"){
    		return "退回";
    	}
    }
  </script>
	<script type="text/javascript">
	function onClickRow(rowIndex,rowData){
		$("#url").val(rowData.url);
		$("#taskId").val(rowData.id);
		$("#firBusinessId").val(rowData.firBusinessId);
		$("#firUrl").val(rowData.firUrl);
		$("#nextNode").val(rowData.nextNode);
	}
	//办理任务
	function approveTask(){
		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要办理的任务");
  			return false;
  		}
		var url = encodeURIComponent("pages/formDevelopment/seeFormOrder.jsp?title="+$("#firUrl").val()+"&uuid="+$("#firBusinessId").val());
		$.ajax({
            url: "<%=basePath%>/flow/canWithDraw",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			taskId : $('#taskId').val()
    		},
    		success : function(result)//成功
    		{
    			if(result.obj==true){
    				if(row.nodeType=="Fork"){
    					approveMethodWithDrawWithFork(url);
    				}else{
    					approveMethodWithDraw(url);
    				}
    			}else{
    				approveMethodWithOutDraw(url);
    			}
    		}
    	});
	}
	function showPic(){
		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要查看的任务");
  			return false;
  		}
		layer.open({
            type: 2,
            title: '当前进度',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1100px', '550px'],
            content:"<%=basePath%>/uflo/diagram?taskId="+$("#taskId").val()
        });
	}
	//不带驳回功能的页面
	function approveMethodWithOutDraw(url){
		layer.open({
			id:1,
            type: 2,
            title: '办理任务',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1100px', '550px'],
            content:"<%=basePath%>/flow/approve?url="+url+"&taskId="+$("#taskId").val(),
            btn:['选择流向','提交','取消'],
            yes: function(index, layero){
            	appointSequence();
            },btn2:function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	comTaskWithoutViariable(res);
            	layer.close(index);
    			location.reload();
            }
        });
	}
	//打开带驳回功能的页面
	function approveMethodWithDraw(url){
		layer.open({
			id:1,
            type: 2,
            title: '办理任务',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1100px', '550px'],
            content:"<%=basePath%>/flow/approve?url="+url+"&taskId="+$("#taskId").val(),
            btn:['选择流向','驳回','提交','取消'],
            yes: function(index, layero){
            	appointSequence();
            },btn2: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	draw(res);
            	layer.close(index);
    			location.reload();
            },btn3:function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	comTaskWithoutViariable(res);
            	layer.close(index);
    			location.reload();
            }
        });
	}

	
	//打开带驳回功能和分支选择的页面
	function approveMethodWithDrawWithFork(url){
		layer.open({
			id:1,
            type: 2,
            title: '办理任务',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1100px', '550px'],
            content:"<%=basePath%>/flow/approve?url="+url+"&taskId="+$("#taskId").val(),
            btn:['选择分支流向','驳回','提交','取消'],
            yes: function(index, layero){
            	forkTaskSequence();
            },btn2:function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	draw(res);
            	layer.close(index);
    			location.reload();
            },btn3: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	comTaskWithViariable(res);
            	layer.close(index);
    			location.reload();
            }
        });
	}
	
	//选择连线
	function appointSequence(){
		layer.open({
    		id:2,
            type: 2,
            title: '选择连线',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['300px', '350px'],
            content:"<%=basePath%>/flow/toFlows?taskId="+$('#taskId').val(),
            btn:['提交'],
            yes: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	flowName = res.flow;
            	nodeName = res.node;
            	userName = res.name;
            	layer.close(index);
            }
        });
		
	}
	
	//选择分支连线
	function forkTaskSequence(){
		layer.open({
    		id:5,
            type: 2,
            title: '选择分支连线',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['450px', '500px'],
            content:"<%=basePath%>/flow/toForkFlows?taskId="+$('#taskId').val()+"&nextNode="+$('#nextNode').val(),
            btn:['提交'],
            yes: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	varaibles = res;
            	layer.close(index);
            }
        });
		
	}
	//驳回
	function draw(res){
		$.ajax({
            url: "<%=basePath%>/flow/withDrwa",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			taskId : $('#taskId').val(),
    			opinion : res,
    		},
    		success : function(result)//成功
    		{
    			layer.msg(result.msg);
    			//最后关闭弹出层
    	        layer.close(index);
    			location.reload();
    		}
    	});
	}
	//完成带变量的任务
	function comTaskWithViariable(res){
		$.ajax({
            url: "<%=basePath%>/flow/completeTask",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			taskId : $('#taskId').val(),
    			opinion : res,
    			variables:varaibles,
    			flowName:flowName,
    			nodeName:nodeName,
    			userName:userName,
    		},
    		success : function(result)//成功
    		{
    			layer.msg(result.msg);
    			//最后关闭弹出层
    	        layer.close(index);
    			location.reload();
    		}
    	});
	}
	//完成带不变量的任务
	function comTaskWithoutViariable(res){
		$.ajax({
            url: "<%=basePath%>/flow/completeTask",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			taskId : $('#taskId').val(),
    			opinion : res,
    			variables:'',
    			flowName:flowName,
    			nodeName:nodeName,
    			userName:userName,
    		},
    		success : function(result)//成功
    		{
    			layer.msg(result.msg);
    			//最后关闭弹出层
    	        layer.close(index);
    	        window.parent.todoEvent.eventEmitter.emit(window.parent.todoEvent.RELOAD_TODO);
    		}
    	});
	}
	//选择任务节点(因选择连线功能已获取该信息，所以暂时废弃)
<%-- 	function appointNode(){
		layer.open({
    		id:2,
            type: 2,
            title: '选择流向',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['300px', '350px'],
            content:"<%=basePath%>/flow/toNode?taskId="+$('#taskId').val(),
            btn:['提交','取消'],
            yes: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	nodeName = res;
            	layer.close(index);
            }
        });
		
	} --%>
	
	//选择办理人   暂时废弃
<%-- 	function appointAssignee(){
		layer.open({
    		id:2,
            type: 2,
            title: '选择办理人',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['300px', '350px'],
            content:"<%=basePath%>/flow/toNextNodeClaimUsers?taskId="+$('#taskId').val()+"&taskNodeName="+nodeName,
            btn:['提交','取消'],
            yes: function(index, layero){
            	var res = window["layui-layer-iframe" + index].callbackdata();
            	userName = res;
            	layer.close(index);
            }
        });
	} --%>
	
  </script>
	<script>
	$(document).ready(function () {
        $('#taskName').textbox('textbox').keydown(function (e) {
            if (e.keyCode == 13) {
            	$('#search').click();
            }
        });
    });
  </script>
</body>
</html>
