<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>办理任务</title>

<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>
<script src="<%=basePath%>/js/common.js"></script>
<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
.comment-con{
	margin-bottom: 10px;
	padding: 0 10px;
}
.comment-con span{
	display: inline-block;
	width: 105px;
	text-align: right;
	padding-right: 5px;
}
.comment-span-s{
	display: inline-block;
	width: 15%;
}
.comment-span-l{
	display: inline-block;
	width: 50%;
}
.comment-span-m{
	display: inline-block;
	width: 30%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
		var user = <%=userJson%> ;
		var static_permissionssJson = <%=permissionssJson%>;
	</script>

	<div data-options="region:'center',title:'',split:true" id="content"
		style="width: 100%;">
		<input id="taskId" value="${taskId }" hidden="true">
		<div style="padding: 5px;">
			<!-- <iframe id="taskForm" width="100%" height="300" src="">
			</iframe>
			 -->
			<iframe id="taskForm" width="100%" height="300" src="<%=basePath%>/${url}" frameborder="0">
			</iframe>
		</div>

		<div style="margin-top: 5px; position: realtive">
			<div>
			    <label style="display:inline-block;width:115px;text-align:right;padding-right:5px;"><strong>批注</strong></label>
			    <input type="text" id="comment" name="comment" class="easyui-textbox" data-options="width:750,height:44,multiline:true" />
			  </div>
			  <br>
			  <hr style="width:96%;height:2px;border:none;border-top:1px dashed #DCDCDC;">
			<p id="com">
			</p>
				
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
            url: "<%=basePath%>/flow/getOpinions",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			taskId : $('#taskId').val()
    		},
    		success : function(result)//成功
    		{	var com = $("#com");
    			op="";
    			for(var i = 0;i<result.list.length;i++){
    				if(result.list[i].opinion==""||result.list[i].opinion==null){
    					if(result.list[i].assignee==""||result.list[i].assignee==null){
    						op+="<div style='margin-left:30px;float:left;display:inline'><strong>处理人：</strong>管理员退回&nbsp;&nbsp;&nbsp;";
            				op+="<strong>任务名称：</strong>"+result.list[i].taskName+"&nbsp;&nbsp;&nbsp;";
            				op+="<strong>批注意见：</strong>未填写意见</div><div style='float:right;display:inline'>";
            				op+="<strong>任务状态：</strong>"+format(result.list[i].state)+"&nbsp;&nbsp;&nbsp;";
            				op+="<strong>办理时间：</strong>"+result.list[i].endDate+"&nbsp;&nbsp;&nbsp;</div><br>";
    					}else{
    						op+="<div style='margin-left:30px;float:left;display:inline'><strong>处理人：</strong>"+result.list[i].assignee+"&nbsp;&nbsp;&nbsp;";
            				op+="<strong>任务名称：</strong>"+result.list[i].taskName+"&nbsp;&nbsp;&nbsp;";
            				op+="<strong>批注意见：</strong>未填写意见</div><div style='float:right;display:inline'>";
            				op+="<strong>任务状态：</strong>"+format(result.list[i].state)+"&nbsp;&nbsp;&nbsp;";
            				op+="<strong>办理时间：</strong>"+result.list[i].endDate+"&nbsp;&nbsp;&nbsp;</div><br>";
    					}
    				}else{
    					op+="<div style='margin-left:30px;float:left;display:inline'><strong>处理人：</strong>"+result.list[i].assignee+"&nbsp;&nbsp;&nbsp;";
        				op+="<strong>任务名称：</strong>"+result.list[i].taskName+"&nbsp;&nbsp;&nbsp;";
        				op+="<strong>批注意见：</strong>"+result.list[i].opinion+"</div><div style='float:right;display:inline'>";
        				op+="<strong>任务状态：</strong>"+format(result.list[i].state)+"&nbsp;&nbsp;&nbsp;";
        				op+="<strong>办理时间：</strong>"+result.list[i].endDate+"&nbsp;&nbsp;&nbsp;</div><br>";
    				}
    			}
    			op+="<br>";
    			com.append(op);
    			//最后关闭弹出层
    		}
    	});
	});
	function format(value){
    	if(value =="Ready"){
    		return "可领取";
    	}
    	if(value == "Completed"){
    		return "已完成";
    	}
    	if(value == "Forwarded"){
    		return "已跳转";
    	}
    	if(value == "Created"){
    		return "已创建";
    	}if(value == "Reserved"){
    		return "预分配";
    	}if(value == "Withdraw"){
    		return "退回";
    	}
    }
		//页面加载时加载对应任务id的表单内容以及批注
		var callbackdata = function() {
			var data = $("#comment").textbox('getValue');
			return data;
		}
	</script>
</body>
</html>
