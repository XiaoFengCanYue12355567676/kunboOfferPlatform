<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>可选方向列表</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/jquery.json.min.js"></script>
    <script src="<%=basePath%>/js/ajax-util.js"></script>
	<script src="<%=basePath%>/lib/js/template.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
	<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>
	<input hidden='true' id="taskId" value="${taskId}">
	<input hidden='true' id="nextNode" value="${nextNode}">
    <div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect: false,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true
                ">
			<thead>
				<tr href="#">
					<th field="ck" checkbox="true"></th>
					<th field="name" align="center">连线名称</th>
					<th field="toNode" align="center">下一节点</th>
					<th field="expression" align="center">表达式</th>
				</tr>
			</thead>
		</table>
		<a id='checkVar' href="javascript:void(0)"
				class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="checkVar()">查看变量</a>
	</div>
	
 

  <script type="text/javascript">
  	var callbackdata = function() {
		var data = checkVar();
		return data;
	}
  	var count = function() {
  		var rows = $('#dg').datagrid('getSelections');
  		return rows.length;
	}
  	
  	$(function(){
        $('#dg').datagrid({
          url:'<%=basePath%>/flow/loadNextNodeSequenceFlows',
          queryParams:
          {
        	  taskId:$("#taskId").val(),
        	  nextNode:$("#nextNode").val()
          },
          method:'post'
        })
      });
  	function checkVar(){
  		var rows = $('#dg').datagrid('getSelections');
  		var arr = [];
  		for(var i = 0; i<rows.length;i++){
  			var temp = [];
  			var temp1 = [];
  			var j = {};
  			temp = rows[i].expression.split("{");
  			temp1 = temp[1].split("=");
  			var j={};
  			j.key=temp1[0];
  			j.value = true;
  			arr.push(j);
  		}
  		var a = JSON.stringify(arr);
  		return a;
  	}
  </script>
</body> 
</html>

