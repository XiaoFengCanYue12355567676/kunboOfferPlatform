<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>待办任务</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">任务编号</th>
					<th field="processDefinitionId" align="center" >归属流程</th>
					<th field="tip" align="center" >流程别名</th>
					<th field="name" align="center">当前任务</th>
					<th field="description" align="center">描述</th>
					<th field="owner" align="center"  hidden="true">任务发布者</th>
					<th field="assignee" align="center">执行人</th>
					<th field="applyUser" align="center" >提交者</th>
					<th field="applicateTime" align="center" >提交时间</th>
					<th field="dueDate" align="center"  hidden="true">任务到期日期</th>
					<th field="category" align="center"  hidden="true">任务类型</th>
					<th field="formKey" align="center" hidden="true">表单地址</th>
					<th field="processInstanceId" align="center" hidden="true">流程唯一编号</th>
					<th field="taskDefinitionKey" align="center" hidden="true">任务定义id</th>
					<th field="oldTaskId" align="center" hidden="true">表单关联任务id</th>
					<th field="editFlag" align="center" hidden="true">可修改标识</th>
					<th field="formTableName" align="center" hidden="true">关联表单名称</th>
					<th field="formId" align="center" hidden="true">关联表uuid</th>
					<th field="isEndTask" align="center" hidden="true">是否为最后一个节点</th>
					<th data-options="field:'_operate',width:80,align:'center',formatter:showOrHidden">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
		<!-- 	<a id='openform' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="openForm();return false;">打开表单</a> -->
			<a id='completeTask'
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="completeTask();return false;">办理任务</a>
		    <a id='viewProcessDiagram' 
				href="javascript:void(0)" class="easyui-linkbutton" 
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="viewProcessDiagram();">查看任务进度</a> 
			<a id='endprocess'
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="endprocess();">强制结束任务</a>
				<input id='href' value="" hidden="hidden">
				<input id='taskId' value="" hidden="hidden">
				<input id='oldTaskId' value="" hidden="hidden">
				<input id='processInstanceId' value="" hidden="hidden">
				<input id='processDefinitionId' value="" hidden="hidden">
				<input id='taskName' value="" hidden="hidden">
		</div>
	</div>



	<script type="text/javascript">
	var assId;
	var isEnd="";
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/process/getList.do',
        queryParams:
        {
        },
        method:'get'
      })
    });
  </script>
	<script type="text/javascript">
	//控制修改按钮的显示
	function showOrHidden(value,row,index){
		if(row.editFlag=="0"){
			return "";
		}else{
			var uuid = trimFN(row.formId);
			return '<a href="#" onclick="editForm('+row.id+',\''+row.formTableName+'\',\''+uuid+'\')">修改申请</a>'
		}
	};
	var bool = true;
  	function onClickRow(rowIndex,rowData){
  		bool = false;
  		$('#href').val("<%=basePath%>"+rowData.formKey);
  		$('#taskId').val(rowData.id);
  		$('#oldTaskId').val(rowData.oldTaskId);
  		$('#processInstanceId').val(rowData.processInstanceId);
  		$('#processDefinitionId').val(rowData.processDefinitionId);
  		$('#taskName').val(rowData.name);
  		isEnd = rowData.isEndTask;
  	};
  	//填写表单,打开填写完毕后，将填写的表单div内容全部回传，经由controller转码保存到数据库里
 /*   	function openForm(){

  		layer.open({
            type: 2,
            title: '填写表单',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['900px', '550px'],
            content:$('#href').val(),
            btn:['提交','取消'],
            yes: function(index, layero){
            	var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                 Ifame.insert();
                console.info("uuid========"+str);  
            }
        });
  	};  */ 

  	function addDialogCallBackSuccess(uuid){
  		var tableName = "" ;
	     var formUrl = "" ;
	     var strUrl = $('#href').val();
	     console.info("formUrl========"+formUrl);
 		if(strUrl!=null&&strUrl!=""){

 	        var arr1= new Array(); //表名
 	        var arr2= new Array(); //url
 	        arr1 = strUrl.split("=");
 	        arr2 = strUrl.split("/pages");
 	        tableName = arr1[1] ;
 	        formUrl  = "/pages"+arr2[1] ;
 	       console.info("formUrl========"+formUrl);
 		}
 	    var str ="'"+uuid+"',"+formUrl+","+tableName;
 		console.info("=str="+str);
    	$.ajax({
            url: "<%=basePath%>/process/completeTask",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					taskId : $('#taskId').val(),
    					args : '',
    					message:'',
    					form: str
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    					location.reload();
    				}
    			});
  	}
  	//当前无对应表单地址的，应当打开历史任务中填写的表单内容查看，由当前登录人物填写批注，打开表单和完成任务按钮应当互斥，而不是现在这样一同展示
  	function completeTask(){
  		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要办理的任务");
  			return false;
  		}
  		var taskId = $('#taskId').val();
  		var oldTaskId = $('#oldTaskId').val();
  		if(""!=isEnd&&null!=isEnd){
  			layer.open({
  	  			type: 2,
  	            title: '办理任务',
  	            shadeClose: false,
  	            shade: 0.3,
  	            maxmin: true, //开启最大化最小化按钮
  	            area: ['914px', '550px'],
  	            content:'<%=basePath%>/form/showForm?taskId='+taskId+'&oldTaskId='+oldTaskId,
  	            btn:['指定下一环节办理人','提交','驳回到指定节点','取消'],
  	            yes: function(index, layero){
  	            	layer.open({
  	            		type: 2,
  	            		title: "指定经办人",
  	            	    shadeClose: false,
  	            	    shade: 0.3,
  	            	    maxmin: true, //开启最大化最小化按钮
  	            	    area: ["380px","300px"],
  	            	    btn: ["保存"],
  	            	    content: "<%=basePath%>/groups/userList",
  	                    yes: function(index, layero){
  	                         //按钮【按钮一】的回调
  	                    	var res = window["layui-layer-iframe" + index].callbackdata();
  	                    	assId = res;
  	                    	layer.close(index);
  	                    },
  	            	})
  	            },btn2:function(index, layero){
  	                 //按钮【按钮一】的回调
  	            	var res = window["layui-layer-iframe" + index].callbackdata();
  	                if(""==assId||null==assId){
  	             		layer.msg("您还未指定下一步经办人，请选择后再提交审核意见");
  	             		return false;
  	             	}
  	            	$.ajax({
  	                    url: "<%=basePath%>/process/completeTask1",
  	            				type : "POST",
  	            				dataType : 'json',
  	            				data : {
  	            					assId : assId,
  	            					taskId : $('#taskId').val(),
  	            					args : '',
  	            					form:'',
  	            					message:res
  	            				},
  	            				success : function(result)//成功
  	            				{
  	            					layer.msg(result.msg);
  	            					//最后关闭弹出层
  	            	            	layer.close(index);
  	            	            	location.reload(true);
  	            	            	//reloadDg();
  	            				}
  	            	});
  	            	
  	            },btn3:function(index,layero){
  	            	var comment = window["layui-layer-iframe" + index].callbackdata();
  	            	layer.open({
  	          			type: 2,
  	                    title: '可驳回节点列表',
  	                    shadeClose: false,
  	                    shade: 0.3,
  	                    maxmin: true, //开启最大化最小化按钮
  	                    area: ['600px', '550px'],
  	                    content:'<%=basePath%>/form/backAvtivityList?taskId='+taskId,
  	                    btn:['驳回','取消'],
  	                    yes: function(index, layero){
  	                    	var res = window["layui-layer-iframe" + index].callbackdata();
  	                      	$.ajax({
  	                            url: "<%=basePath%>/process/rollBackToAssgin",
  	                        	type : "POST",
  	                        	dataType : 'json',
  	                        	data : {
  	                        			taskId : $('#taskId').val(),
  	                        			activityId : res,
  	                        			comment:comment
  	                        	},
  	                        	success : function(result)//成功
  	                        		{
  	                        			layer.msg(result.msg);
  	                        			layer.close(index);
  	                        		}
  	                        });setTimeout("location.reload()",1500);
  	                    }
  	                  });
  	            	return false;
  	            }
  	  		});
  		}else{
  			layer.open({
  	  			type: 2,
  	            title: '办理任务',
  	            shadeClose: false,
  	            shade: 0.3,
  	            maxmin: true, //开启最大化最小化按钮
  	            area: ['914px', '550px'],
  	            content:'<%=basePath%>/form/showForm?taskId='+taskId+'&oldTaskId='+oldTaskId,
  	            btn:['提交','驳回到指定节点','取消'],
  	            yes:function(index, layero){
  	                 //按钮【按钮一】的回调
  	            	var res = window["layui-layer-iframe" + index].callbackdata();
  	            	$.ajax({
  	                    url: "<%=basePath%>/process/completeTask1",
  	            				type : "POST",
  	            				dataType : 'json',
  	            				data : {
  	            					assId : assId,
  	            					taskId : $('#taskId').val(),
  	            					args : '',
  	            					form:'',
  	            					message:res
  	            				},
  	            				success : function(result)//成功
  	            				{
  	            					layer.msg(result.msg);
  	            					//最后关闭弹出层
  	            	            	layer.close(index);
  	            					location.reload();
  	            				}
  	            	});
  	            	
  	            },btn2:function(index,layero){
  	            	var comment = window["layui-layer-iframe" + index].callbackdata();
  	            	layer.open({
  	          			type: 2,
  	                    title: '可驳回节点列表',
  	                    shadeClose: false,
  	                    shade: 0.3,
  	                    maxmin: true, //开启最大化最小化按钮
  	                    area: ['600px', '550px'],
  	                    content:'<%=basePath%>/form/backAvtivityList?taskId='+taskId,
  	                    btn:['驳回','取消'],
  	                    yes: function(index, layero){
  	                    	var res = window["layui-layer-iframe" + index].callbackdata();
  	                      	$.ajax({
  	                            url: "<%=basePath%>/process/rollBackToAssgin",
  	                        	type : "POST",
  	                        	dataType : 'json',
  	                        	data : {
  	                        			taskId : $('#taskId').val(),
  	                        			activityId : res,
  	                        			comment:comment
  	                        	},
  	                        	success : function(result)//成功
  	                        		{
  	                        			layer.msg(result.msg);
  	                        			layer.close(index);
  	                        		}
  	                        });setTimeout("location.reload()",1500);
  	                    }
  	                  });
  	            	return false;
  	            }
  	  		});
  		}
  		
  	}

  	function viewProcessDiagram(){
  		if(bool){
  			layer.msg("请您先选择要查看的任务");
  			return;
  		}
  		var processInstanceId = $('#processInstanceId').val();
  		var processDefinitionId = $('#processDefinitionId').val();
  		layer.open({
  			type: 2,
            title: '填写表单',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['1000px', '600px'],
            content:'<%=basePath%>/process/read-resource?processDefinitionId='+processDefinitionId+'&pProcessInstanceId='+processInstanceId,
            btn:['关闭'],
            yes: function(index, layero){
           	//最后关闭弹出层
           	layer.close(index);
           },
  		});
  	}

  	//强制结束任务
  	function endprocess(){
  		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要办理的任务");
  			return false;
  		}
  		var msgRet = confirmMsg("确定要结束" + $('#taskName').val() + "任务吗？",function(index){
  			endprocessAct($('#taskId').val());
  			layer.close(index);
  		});

  	}
  	function endprocessAct(id){
  		$.ajax({
            url: "<%=basePath%>/process/endProcess",
        	type : "POST",
        	dataType : 'json',
        	data : {
        			taskId : $('#taskId').val(),
        	},
        	success : function(result)//成功
        		{
        			layer.msg(result.msg);
        			setTimeout("location.reload()",1500);
        		}
        });
  	}
  	//修改申请
  	function editForm(taskId,formTableName,uuid){
  		layer.open({
  			type: 2,
            title: '修改申请',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['914px', '550px'],
            content:'<%=basePath%>/pages/formDevelopment/editFormOrder.jsp?title=' + formTableName + '&uuid=' +uuid ,
            btn:['指定下一环节办理人','保存并提交','取消'],
            yes: function(index, layero){
            	layer.open({
            		type: 2,
            		title: "指定经办人",
            	    shadeClose: false,
            	    shade: 0.3,
            	    maxmin: true, //开启最大化最小化按钮
            	    area: ["380px","300px"],
            	    btn: ["保存"],
            	    content: "<%=basePath%>/groups/userList",
                    yes: function(index, layero){
                         //按钮【按钮一】的回调
                    	var res = window["layui-layer-iframe" + index].callbackdata();
                    	assId = res;
                    	layer.close(index);
                    },
            	})
            },btn2: function(index, layero){
            	if(""==assId||null==assId){
         			layer.msg("您还未指定下一步经办人，请选择后再提交修改");
         			return false;
         		}else{
         			 //按钮【按钮一】的回调
                	var ifname="layui-layer-iframe"+index//获得layer层的名字
          			var Ifame=window.frames[ifname]//得到框架
          			Ifame.update()
                	//最后关闭弹出层
                	layer.close(index);
                	$.ajax({
                        url: "<%=basePath%>/process/completeTask1",
                				type : "POST",
                				dataType : 'json',
                				data : {
                					assId:assId,
                					taskId : taskId,
                					args : '',
                					form:'',
                					message:'修改申请'
                				},
                				success : function(result)//成功
                				{
                					layer.msg(result.msg);
                					reloadDg();
                					//location.reload();
                				}
                	});
         		}
            },
  		});
  	}
  	
  	function reloadDg(){
  		$('#dg').datagrid('reload');
  	}
  </script>
</body>
</html>
