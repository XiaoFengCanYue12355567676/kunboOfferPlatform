<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>历史任务列表</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">任务编号</th>
					<th field="belong" align="center">归属流程</th>
					<th field="pid" align="center" formatter="checkTips">流程名称</th>
					<th field="firstAssignee" align="center">发起人</th>
					<th field="firTaskId" align="center">表单相关任务id</th>
					<th field="name" align="center">任务标题</th>
					<th field="des" align="center">描述</th>
					<th field="startTime" align="center">开始时间</th>
					<th field="endTime" align="center">结束时间</th>
					<th field="claimTime" align="center">任务领取时间</th>
					<th field="costTime" align="center" formatter="addHour">消耗时间</th>
					<th field="reason" align="center" >结果</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<a id='showHisForm'
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true" onclick="showHisForm();return false;">查看详情</a>
				<label><span>别名</span></label><input class="easyui-textbox" id="tip" name="tip">
				<label><span>开始时间</span></label><input class="easyui-datetimebox" id="startTime" name="startTime">
				<label><span>结束时间</span></label><input class="easyui-datetimebox" id="endTime" name="endTime">
				<button class="easyui-button" onclick="reload()">查询</button>
				<input id='taskId' value="" hidden="hidden">
				<input id='firTaskId' value="" hidden="hidden">
		</div>
	</div>



	<script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/process/ownHistoryTaskList',
        queryParams:
        {
        },
        method:'get'
      })
    });
    function reload(){
    	$('#dg').datagrid('load', {
    		tip:$("#tip").val(),
    		createTime:$("#startTime").datetimebox('getValue'),
        	endTime:$("#endTime").datetimebox('getValue')
    	}); 
    }
  </script>
	<script type="text/javascript">
	function addHour(value,row,index){
		return value+'小时';
	}
  	function onClickRow(rowIndex,rowData){
  		$('#taskId').val(rowData.id);
  		$('#firTaskId').val(rowData.firTaskId);
  	}; 
  	function checkTips(value,row,index){
  		var content="";
  		$.ajax({
        	url: "<%=basePath%>/tips/findTipById",
        	async: false,
    		type : "GET",
    		dataType : 'json',
    		data : {
    			pid:value
    		},
    		success : function(result)//成功
    		{
    			//alert(result.obj.tName+"8888888");
    			
    			if(result.status==0&&result.obj!=null){
    				console.log(result);
    				content = result.obj.tName;
    			}
    		}
    	});
  		return content;
  	}
  //当前无对应表单地址的，应当打开历史任务中填写的表单内容查看，由当前登录人物填写批注，打开表单和完成任务按钮应当互斥，而不是现在这样一同展示
  	function showHisForm(){
  		var taskId = $('#taskId').val();
  		var oldTaskId = $('#firTaskId').val();
  		layer.open({
  			type: 2,
            title: '任务详情',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['920px', '550px'],
            content:'<%=basePath%>/form/showHisForm?taskId='+taskId+'&oldTaskId='+oldTaskId,
            btn:['关闭'],
  		});
  	}
  	
  </script>
</body>
</html>
