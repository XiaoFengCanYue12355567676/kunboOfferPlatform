<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>模型管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/jquery.json.min.js"></script>
    <script src="<%=basePath%>/js/ajax-util.js"></script>
	<script src="<%=basePath%>/lib/js/template.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
	<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

    <div data-options="region:'center',title:'',split:true" style="width:100%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:false,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:false,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow,
                onCheckAll:onCheckAll,
                onUncheckAll:onUncheckAll
                ">
					<thead>
						<tr href="#">
							<th field="id" align="center" checkbox="true" >用户编号</th>
							<th field="actual_name" align="center">真实姓名</th>
              				<th field="name" align="center">用户名</th>
							<th field="position_id" align="center">职位编号</th>
							<th field="activiti_uuid" align="center">activiti关联编号</th>
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <select id='position_id' name = "position_id" class="easyui-combobox" 
                data-options="
                width:200,
                valueField: 'id',
    			textField: 'text',
    			url: '<%=basePath%>/system/user/position/getAll.do',
    			onChange:flushPage
    			">
                </select>
          <a id='asyncGroup' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="asyncGroup()">同步所有职位</a>
          <a id='asyncUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="asyncUser()">设置关联</a>
          <input type="text" id="deployPath" value="" hidden="hidden">
        </div>
     </div>
	
 
  
  <script type="text/javascript">
  	var all = [];
   	$(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/system/user/findByPositionId.do',
        queryParams:
        {
        	position_id:""
        },
        method:'get'
      })
    });
  </script>
  <script type="text/javascript">
  	function onClickRow(rowIndex,rowData){
  		
  	}
  	function flushPage(){
  		$('#dg').datagrid({
  	        url:'<%=basePath%>/system/user/findByPositionId.do',
  	        queryParams:
  	        {
  	        	position_id:$('#position_id').combobox('getValue')
  	        },
  	        method:'get'
  	      })
  	    all = [];
  	}
  	function onCheckAll(rows){
  		for(var i=0; i<rows.length; i++){
  			all.push(rows[i]);
  		}
  	}
  	function onUncheckAll(rows){
  		all = [];
  	}
  </script>
<script type="text/javascript">
//同步选中用户到activiti中
	function asyncUser(){
		var msgRet = confirmMsg("确定要同步所有选中的用户吗？",function(index){
			if(all.length<1){alertMsg("您还没有选中数据");};
			if(all.length>=1){asyncUserAct(all);};
			layer.close(index);
		});

	}
	function asyncUserAct(all){
		$.ajax({
        	url: "<%=basePath%>/groups/asyncUser",
    		type : "POST",
    		dataType : 'json',
    		data : {
    			userArr : JSON.stringify(all),
    			positionText : $('#position_id').combobox('getText')
    		},
    		success : function(result)//成功
    		{
    			if(result.status==0){
    				layer.msg(result.msg);
    			}
    		}
    	});
	}
	function asyncGroup(){
		$.ajax({
        	url: "<%=basePath%>/groups/asyncGroup",
    		type : "POST",
    		dataType : 'json',
    		success : function(result)//成功
    		{
    			if(result.status==0){
    				layer.msg(result.msg);
    			}
    		}
    	});
	}
</script>
</body> 
</html>

