<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>部署流程管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
	<script src="<%=basePath%>/js/ajax-util.js"></script>
	<script src="<%=basePath%>/lib/js/template.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

    <div data-options="region:'center',title:'',split:true" style="width:100%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
					<thead>
						<tr href="#">
							<th field="id" align="center">模型编码</th>
							<th field="deploymentId" align="center">部署id</th>
              				<th field="name" align="center">名称</th>
              				<th field="key" align="center">关键字</th>
							<th field="description" align="center">描述</th>
							<th field="version" align="center">版本</th>
							<th field="hasStartFormKey" align="center">是否由表单启动</th>
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <a id='startProcess' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-play',plain:true" onclick="startProcess()">启动</a>
          <a id='startProcess' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-play',plain:true" onclick="deleteDeployment()">删除部署</a>
          <input type="hidden" id="processKey" value=""> 
          <input type="hidden" id="deploymentId" value="">        
        </div>
     </div>
	
 
  
  <script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/deployments/getList.do',
        queryParams:
        {
        },
        method:'get'
      })
    });
  </script>
  <script type="text/javascript">
  	function onClickRow(rowIndex,rowData){
  		$('#processKey').attr("value",rowData.key);
  	}
  </script>
<script type="text/javascript">
  function startProcess()
  {
	var processKey = $("#processKey").val();
	if(processKey!=""){
		$.ajax({
	        url: "<%=basePath%>/deployments/activeProDefinite",
	        type : "POST",
	        dataType : 'json',
	        data : {   
	        	processDefinitionKey:processKey
	        },
	        success : function(result)//成功
	        {        
	            layer.msg(result.msg);
	            
	        }
	    });
	}else{
		layer.msg("请选中一条流程");
	}
    
  }
  function deleteDeployment(){
	  var row = $('#dg').datagrid('getSelected');
		if(null==row){
			alert("您还没选中要操作的实例");
			return false;
		}
		var msgRet = confirmMsg("确定要删除" + row.name + "实例吗？",function(index){
			deleteDefition(row.deploymentId);
			layer.close(index);
		});
  }
  function deleteDefition(id){
	  AJAX.DELETE('/deployments/'+id);
  }

</script>
</body> 
</html>

