<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>任务详情</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>
<script src="<%=basePath%>/js/common.js"></script>
<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
.comment-con{
	margin-bottom: 10px;
	padding: 0 10px;
}
.comment-con span{
	display: inline-block;
	width: 105px;
	text-align: right;
	padding-right: 5px;
}
.comment-span-s{
	display: inline-block;
	width: 18%;
}
.comment-span-l{
	display: inline-block;
	width: 50%;
}
.comment-span-m{
	display: inline-block;
	width: 28%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
		var user =
	<%=userJson%>
		;
		var static_permissionssJson =
	<%=permissionssJson%>
		;
	</script>

	<div data-options="region:'center',title:'',split:true" id="content"
		style="width: 100%;">
		<input id="formUrl" type="hidden" value="${form.formUrl}">
		<input id="formId" type="hidden" value="${form.formId}">
		<input id="formTableName" type="hidden" value="${form.formTableName}">
		<div style="padding: 5px;">
			<!-- <iframe id="taskForm" width="100%" height="300" src="">
			</iframe>
			 -->
			<iframe id="taskForm" width="100%" height="300" src="" frameborder="0">
			</iframe>
		</div>

		<div style="margin-top: 5px; position: realtive">
			<p>
				<c:if test="${(comment)!= null && fn:length(comment) > 0}">
					<c:forEach items="${comment}" var="comment">
						<div class="comment-con">
							<div class="comment-span-s"><span>经办人：</span>${comment.name}</div>
							<div class="comment-span-l"><span>批注：</span>${comment.comment}</div>
							<div class="comment-span-m"><span>批注日期：</span>${comment.date}</div>
						</div>
					</c:forEach>
				</c:if>
			</p>
		</div>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
		var formUrl = $("#formUrl").val();
		var formId = $("#formId").val();
		var formTableName = $("#formTableName").val()
		$('#taskForm').attr('src','<%=basePath%>/pages/activiti/forms/viewFormOrder.jsp?formName='+formTableName+'&uuid='+formId)
	});
	</script>
</body>
</html>
