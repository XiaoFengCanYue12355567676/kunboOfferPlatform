
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String motorId = request.getParameter("motorId");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>电机加热</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true,border:true" 
        style="width:20%;border-width:0px;overflow:hidden">           
        <!-- treegrid表格 -->
        <table id ='standard_type' class="easyui-datagrid" style="width:100%;height:100%;border-width:0px;" 
               data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true
			   ">
            <thead>
                <tr>
                    <th data-options="field:'id',title:'UUID',hidden:true"></th>
                    <th data-options="field:'text',title:'系列'" width='80%'></th>
                </tr>
            </thead>
        </table>     
    </div>

    <div data-options="region:'east',iconCls:'icon-reload',title:'',split:true" 
        style="width:80%;height: 100%;border-width:0px;" >
        <table id="standard_component" class='easyui-datagrid' 
            style="width:100%;height:100%;border-width:0px;" title="" 
            data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            ">
            <thead>
              <tr>
                <th field="id"  hidden ="true">uuid</th>            
                <th field="price">加热器价格</th>                       
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
                <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" 
                onclick="funMgAdd()">新增</a>
                <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" 
                onclick="funDgEdit()">修改</a>
                <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" 
                onclick="funDgDele()">删除</a>                            
            </div>          
       </div>
    </div>

  <script type="text/javascript">
    var typeId;
    var motorId = '<%=motorId%>'

 	//表格初始化 
    $(function(){
        $('#standard_type').datagrid({
            url:"/shop/motor/motorStandardNum/list.do?brandId="+motorId,
            onClickRow:function(index,row)
            {
                console.info(row);
                $('#standard_component').datagrid({
                    url:'/shop/motor/motorHeater/list.do?brandId='+motorId,
                    queryParams: {
                        seriesText: row.text
                    }
                });
            }
        });

        // $('#dg').treegrid('loadData',dgdata);
    }); 
     

    function funMgAdd()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_type');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个电机系列');
            return;
        }
        Easyui.Layer.openLayerWindow(
            '新增潮温系数',
            'motorHeater/addMotorHeater.jsp?text='+row.text+'&motorId='+motorId,
            ['650px','300px']);  
    }
    function funDgEdit()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_component');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个电机系列');
            return;
        }
        console.info(row);
        Easyui.Layer.openLayerWindow(
            '修改潮温系数',
            'motorHeater/addMotorHeater.jsp?id='+row.id+'&motorId='+motorId,
            ['650px','300px']);  
    }
    function funDgDele()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_component');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个电机系列');
            return;
        }
        var r = Easyui.Ajax.get('/shop/motor/motorHeater/delete.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#standard_component').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#standard_component').datagrid('reload');
        }
    }

    function fanModelOffer()
    {

    }

	</script>
</body> 
</html>
