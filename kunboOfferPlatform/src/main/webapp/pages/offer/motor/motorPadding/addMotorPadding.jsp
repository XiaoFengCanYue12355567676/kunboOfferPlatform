<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String motorId = request.getParameter("motorId");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增潮温系数</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 300px;border-width:0px;overflow:hidden" >  
	    <table class="editTable" style="text-align: center; margin-top: 10px;margin-left:30px" >
            <tr>
                <td class="label">填料函</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>	
            <tr>
                <td class="label">价格或比率</td>
                <td >                    
                    <input id='priceOfCoeff' class="easyui-switchbutton" 
                        data-options="required:false,width:150,editable:true,onText:'价格',offText:'比率'" >                      
                </td>
            </tr>	            
            <tr>
                <td class="label">价格</td>
                <td >
                    <input id='price' class="easyui-textbox" data-options="required:false,width:150,editable:true" >
                </td>
            </tr>	 
            <tr>
                <td class="label">比率</td>
                <td >
                    <input id='coeff' class="easyui-textbox" data-options="required:false,width:150,editable:true" >
                </td>
            </tr>	  	                                    
	        <tr>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">
var motorId = '<%=motorId%>'
 
    var text = shuoheUtil.getUrlHeader().text;
    console.info('text = '+text);

    $(function()
    {
        $('#seriesText').textbox('setValue',text);
        $('#price').textbox('disable');


        $('#priceOfCoeff').switchbutton({
            onChange: function(checked)
            {        
                console.info('checked = '+checked);
                if(checked)
                {
                    $('#coeff').textbox('disable');
                    $('#price').textbox('enable');
                }
                else
                {
                    $('#price').textbox('disable');
                    $('#coeff').textbox('enable');
                }
            }
       
        })
    });

    var MotorPadding = new Object();
    MotorPadding.id = '';
    MotorPadding.text = text;
    MotorPadding.priceOfCoeff = false;
    MotorPadding.price = '';
    MotorPadding.coeff = '';    
	
    function save() {
        
        MotorPadding.text = $('#text').textbox('getText');
        MotorPadding.priceOfCoeff = $('#priceOfCoeff').switchbutton('options').checked;
        MotorPadding.price = $('#price').textbox('getValue');
        MotorPadding.coeff = $('#coeff').textbox('getValue');     
        MotorPadding.brandId = motorId;
        console.info(JSON.stringify(MotorPadding));

        $.ajax({
            url: "/shop/motor/motorPadding/save.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(MotorPadding)
            },
            error: function() //失败
            {
            shuoheUtil.layerTopMsgError('通讯失败');
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    parent.$('#dg').datagrid('reload');
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                    shuoheUtil.layerTopMsgOK('保存成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>