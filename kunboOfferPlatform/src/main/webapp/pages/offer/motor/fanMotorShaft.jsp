<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String motorId = request.getParameter("motorId");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>电机轴维护管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:100%;">           
        <!-- treegrid表格 -->
        <table id ='dg' class="easyui-datagrid" style="width:100%;height:100%" 
               data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                onClickRow: onClickRow,
                toolbar:'#tt'
			   ">
            <thead>
                <tr>
                    <th data-options="field:'id',title:'UUID',hidden:true"></th>
                    <th data-options="field:'text',title:'电机轴',editor:'textbox'" width='50%'></th>
                    <th data-options="field:'coeff',title:'轴系数',editor:'textbox'" width='50%'></th>                    
                </tr>
            </thead>
        </table>
        <div id="tt" style="height:35px">
                <a href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-plus',plain:true" onclick="append()">新增</a>
                <a href="javascript:void(0)" 
                class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="removeit()">删除</a>
                <a href="javascript:void(0)" 
                class="easyui-linkbutton" data-options="iconCls:'fa fa-check',plain:true" onclick="accept()">应用</a>
                <!-- <a href="javascript:void(0)" 
                class="easyui-linkbutton" data-options="iconCls:'fa fa-reply',plain:true" onclick="reject()">撤销</a>             -->
        </div>        
    </div>


</body> 
</html>
<script type="text/javascript">
var motorId = '<%=motorId%>'
    var editIndex = undefined;
    /* function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            var ed = $('#dg').datagrid('getEditor', {index:editIndex,field:'text'});
            var ef = $('#dg').datagrid('getEditor', {index:editIndex,field:'coeff'});            
            console.info('ed = '+ed);
            var text = $(ed.target).textbox('getValue');
            var coeff = $(ef.target).textbox('getValue');
            // if(productname == '' || productname == undefined) return true;
            $('#dg').datagrid('getRows')[editIndex]['text'] = text;
            $('#dg').datagrid('getRows')[editIndex]['coeff'] = coeff;            
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            // console.info('111111 = '+productname);
            save(text,coeff);
            return true;
        } else {
            console.info('222222');
            return false;
        }
    } */
 // 第一步endEditing修改
    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }
    function onClickRow(index){
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index)
                        .datagrid('beginEdit', index);
                editIndex = index;
            } else {
                $('#dg').datagrid('selectRow', editIndex);
            }
        }
    }
    function append(){
        if (endEditing()){
            $('#dg').datagrid('appendRow',{status:'P'});
            editIndex = $('#dg').datagrid('getRows').length-1;
            $('#dg').datagrid('selectRow', editIndex)
                    .datagrid('beginEdit', editIndex);
        }
    }
    function removeit(){
        if (editIndex == undefined){return}
        $('#dg').datagrid('cancelEdit', editIndex)
                .datagrid('deleteRow', editIndex);
        editIndex = undefined;
    }
    function accept(){
        if (endEditing()){
            $('#dg').datagrid('acceptChanges');
            save('') // 第二步加保存
        }
    }
    function reject(){
        $('#dg').datagrid('rejectChanges');
        editIndex = undefined;
    }
    function getChanges(){
        var rows = $('#dg').datagrid('getChanges');
        alert(rows.length+' rows are changed!');
    }
</script>

<script>
$(function()
{
    // var data = Easyui.Ajax.get('/shop/motor/motorSeries/list.do');

    // if(data != null)
    {
        $('#dg').datagrid({
            url:"/shop/motor/motorShaft/list.do?brandId="+motorId
        });
    }

});

function save(text,coeff)
{
    var ret = Easyui.Datagraid.getSelectRow('dg');

    console.info(ret);
    var MotorIpLevel = new Object();
    /* MotorIpLevel.text = text;
    MotorIpLevel.coeff = coeff;    */ 
    MotorIpLevel.text = ret.text; // 第三步，本行修改
    MotorIpLevel.coeff = ret.coeff; // 第三步，本行修改
    MotorIpLevel.id = ret.id;
    MotorIpLevel.brandId = motorId;
    // console.info(Easyui.Datagraid.getSelectRow('dg'));

    $.ajax({
        url: "/shop/motor/motorShaft/save.do", 
        type: "POST",
        dataType: 'json',
        data: {
            'data': JSON.stringify(MotorIpLevel)
        },
        error: function() //失败
        {
            shuoheUtil.layerTopMsgError('通讯失败');
        },
        success: function(data) //成功
        {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
                $('#dg').datagrid('reload');     
                shuoheUtil.layerTopMsgOK('保存成功');
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
}
</script>