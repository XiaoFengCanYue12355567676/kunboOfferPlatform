<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>外购件（指定产品）管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:20%;">           
        <!-- treegrid表格 -->
        <table id ='standard_type' class="easyui-datagrid" style="width:100%;height:100%" 
               data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tt'
			   ">
            <thead>
                <tr>
                    <th data-options="field:'id',title:'UUID',hidden:true"></th>
                    <th data-options="field:'text',title:'外购件（指定产品）类型'" width='80%'></th>
                </tr>
            </thead>
        </table>
        <div id="tt" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMMAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funMMEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funMMDele()">删除</a>
        </div>        
    </div>

    <div data-options="region:'east',iconCls:'icon-reload',title:'',split:true" style="width:80%;height: 100%;" >
        <table id="standard_component" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            >
            <thead>
              <tr>
                <th field="id"  hidden ="true">uuid</th>                
                <th field="stockNumber">存货编码</th>                
                <th field="stockName">存货名称</th>
                <th field="specifications">规格型号</th>
                <th field="supplier">供应商</th>
                <th field="unitPrice">单价</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funDgDele()">删除</a>
            <!-- <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> -->
          </div>
       </div>
    </div>

  <script type="text/javascript">
    var typeId;


 	//表格初始化 
    $(function(){
        $('#standard_type').datagrid({
            url:"/shop/standard/standardType/list.do",
            onClickRow:function(index,row)
            {
                console.info(row);
                $('#standard_component').datagrid({
                    url:'/shop/standard/standardComponent/listByTypeId.do',
                    queryParams: {
                        pid: row.id
                    }
                });
            }
        });

        // $('#dg').treegrid('loadData',dgdata);
    }); 
     

    function formatterSortType(value,row,index)
    {
        if(row.sort_type == 'GENERA')
            return "大类";
        if(row.sort_type == 'PRODUCT_LINE')
            return "产品线";
    }

    function formatterAttriType(value,row,index)
    {
        if(row.attriType == 'HALF_PRODUCT')
            return "半成品";
        if(row.attriType == 'PARAMETER')
            return "参数";
        if(row.attriType == 'STANDARD_PORT')
            return "外购件（指定产品）";
        if(row.attriType == 'MANHOUR')
            return "工时";
        if(row.attriType == 'MATERIAL')
            return "材料";
        if(row.attriType == 'ASSORT_PORT')
            return "外购件（指定价格）";
    }


    function funMMAdd() {
        Easyui.Layer.openLayerWindow(
        '新增产品类型',
        'form/addStandardType.jsp',
        ['650px','400px']);
    }  
    
    function funMMEdit()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_type');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('编辑前请先选择一条数据');
        }
        Easyui.Layer.openLayerWindow(
            '编辑产品类型',
            'form/editStandardType.jsp?id='+row.id,
            ['650px','400px']);
    }

    function funMMDele()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_type');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('删除前请先选择一条数据');
        }
        var r = Easyui.Ajax.get('/shop/standard/standardType/delete.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#standard_type').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#standard_type').datagrid('reload');
        }

        
    }
    function funMgAdd()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_type');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个标准品类型');
            return;
        }
        Easyui.Layer.openLayerWindow(
            '新建标准品',
            'form/addStandardComponent.jsp?id='+row.id,
            ['700px','500px']);  
    }
    function funDgEdit()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_component');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个标准品');
            return;
        }
        console.info(row);
        Easyui.Layer.openLayerWindow(
            '修改标准品',
            'form/editStandardComponent.jsp?id='+row.id,
            ['700px','500px']);  
    }
    function funDgDele()
    {
        var row = Easyui.Datagraid.getSelectRow('standard_component');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个模型部件');
            return;
        }
        var r = Easyui.Ajax.get('/shop/standard/standardComponent/delete.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#standard_component').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#standard_component').datagrid('reload');
        }
    }

 	// function loadFanModel(id){
 	// 	/*
 	// 	$.ajax({
    //         url: "/develop/url/getUrl.do",
    //         type: "POST",
    //         dataType: 'json',
    //         data: {
    //         	'name':'getFanModelByTypeId',
    //         	'fanTypeId': id 
    //         },
    //         error: function() //失败
    //         {
    //             shuoheUtil.layerTopMaskOff();
    //         },
    //         success: function(data) {
    //         	var arr = data.rows;
    //         	for(var i=0;i<arr.length;i++){
    //         		if(arr[i]._parentId==""){
    //         			delete arr[i]._parentId;
    //         		}
    //         	}
    //         	data.rows = arr;
    //         	$('#dg').treegrid('loadData', data);
    //         }
    //     });*/
 		
 	// 	$('#dg').treegrid({
    // 		url:'/develop/url/getUrl.do',
    // 		//data: data,
    // 		method:'get',
    // 		queryParams: {
    // 			'name':'getFanModelByTypeId',
    // 			'fanTypeId': id      
    //           }
    // 	})
 	// }
 	// function onContextMenu(e,row){
	// 	e.preventDefault();
	// 	$(this).treegrid('select', row.id);
	// 	$('#mm').menu('show',{
	// 		left: e.pageX,
	// 		top: e.pageY
	// 	});
	// }
 	
 	// function append(){
 	// 	var rootNode = $('#dg').treegrid('getRoot');
 	// 	var node = $('#dg').treegrid('getSelected');
 	// 	var pid = node.id;
 	// 	Easyui.Layer.openLayerWindow(
 	// 		      '新建产品模型',
 	// 		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId+'&pid='+pid+'&fanModelId='+rootNode.id,
 	// 		      ['650px','400px']);
 	// }
 	// function removeIt(){
 	// 	alert("移除");
 	// }
	</script>
</body> 
</html>
<script type="text/javascript">


/*大类*/
// function funTgAdd() {
	
// 	var nodes = $('#tt').tree('getSelected');
      
//       var pid = '0';
//     if(nodes != null){
//     	Easyui.Layer.openLayerWindow(   
//     		      '新建产品大类',
//     		      '/pages/shop/product/type/addProductType.jsp?id='+$('#tt').tree('getSelected').id,
//     		      ['655px','400px']);
//     }else{
//     	Easyui.Layer.openLayerWindow(   
//   		      '新建产品大类',
//   		      '/pages/shop/product/type/addProductType.jsp?',
//   		      ['655px','400px']);
//     } 
// }
// function funTgEdit() {

//     if($('#tt').tree('getSelected') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     console.log($('#tt').tree('getSelected').id)
//   Easyui.Layer.openLayerWindow(
//       '编辑产品大类',
//       '/pages/shop/product/type/editProductType.jsp?id='+$('#tt').tree('getSelected').id,
//        ['400px','280px']);
// }
// function funTgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/productCategory/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
        
//         shuoheUtil.layerMsgSaveOK('删除成功'),
        
//     );
//     $('#tt').tree('reload');
// }


/*模型*/
// function funMgAdd(){
// 	var nodess = $('#tt').tree('getSelected');
//     if(nodess == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     var rootNode = $('#dg').treegrid('getRoot');
//     if(rootNode){
//     	shuoheUtil.layerMsgCustom('风机模型已经存在，请添加配件');
//     }else{
//     	Easyui.Layer.openLayerWindow(
//     		      '新建产品模型',
//     		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId,
//     		      ['650px','400px']);
//     }
    
// }
// function funMgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
// 	 console.info($('#mg').datagrid('getSelected').id);
// 	Easyui.Layer.openLayerWindow(
//        '修改产品模型',
//        '/pages/shop/product/model/editProductModel.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//        ['400px','280px']);
// }
// function funMgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/extensionModel/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#mg').datagrid('reload');
// } 




/*配件*/
// function funDgAdd(){

//     if(Easyui.Datagraid.getSelectRow('dg') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
//     Easyui.Layer.openLayerWindow(
//       '新建模型配件',
//       '/pages/shop/product/parts/addModelParts.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//       Easyui.Layer.sizeTwoRow(600));
// }
// function funDgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个模型配件');
//     }
// 	 console.info("555");
// 	Easyui.Layer.openLayerWindow(
//        '修改模型配件',
//        '/pages/shop/product/parts/editModelParts.jsp?uuid='+$('#dg').datagrid('getSelected').id,
//        Easyui.Layer.sizeTwoRow(600));
// }
// function funDgDele() {
//   // body...
//     var row = Easyui.Datagraid.getSelectRow('dg');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个材料');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/fanModelParts/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#dg').datagrid('reload');
// }
// function fanModelOffer() {
// 	var node = $('#dg').treegrid('getSelected');
// 	console.log(node);
// 	if(node){
// 		$.ajax({
//             url: "/productMix/fanModelOffer",
//             type: "POST",
//             dataType: 'json',
//             data: {
//             	'modelNumber': node.id
 
//             },
//             error: function() //失败
//             {
//                 shuoheUtil.layerTopMaskOff();
//             },
//             success: function(data) {
//             	$('#dg').treegrid('reload');
//             }
//         });
// 	}else{
// 		shuoheUtil.layerMsgCustom('请选择要报价的模型');
// 	}
	
// }


  
</script>