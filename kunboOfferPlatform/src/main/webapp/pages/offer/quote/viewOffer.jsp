<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
		String id = request.getParameter("id");
%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<style type="text/css"> 
body{
	font-size: 14px;
}
.clear:after {
  clear: both;
  display: block;
  content: "\20";
  height: 0;
  visibility: hidden;
}
.clear {
  zoom: 1;
  _line-height: 0;
}
.div-con{
	margin-bottom: 15px;
	padding-bottom: 5px;
	border-bottom: 1px solid #ddd;
}
.div-half-con{
	float: left;
	width: 33.33%;
}
.para-con{
	float: left;
	width: 50%;
	margin-bottom: 8px;
}
.profit-con{
	float: left;
	width: 25%;
}
label{
	display: inline-block;
	width: 90px;
	text-align: right;
	padding-right: 8px;
}
input[type="text"], select{
	height: 30px;
	line-height: 30px;
	width: 170px;
}
input[type="checkbox"]{
	margin-left: 15px;
}
.count-con{
	float: right;
	text-align: right;
	padding-right: 20px;
	font-size: 16px;
	line-height: 34px;
}
.count-con span{
	font-size: 20px;
}
.checkbox-title{
	margin-bottom: 8px;
}
.checkbox-div{
	padding-left: 78px;
}
.checkbox-con{
	float: left;
	width: 25%;
	margin-bottom: 5px;
}
.save-btn{
	text-align: right;
	margin: 15px 0;
}
.save-btn button{
	width: 140px;
	height: 38px;
	line-height: 38px;
	border-radius: 3px;
	border: 1px solid #e3e5e8;
	background: #fff;
	cursor: pointer;
	margin-left: 15px;
}
.half-con{
	width: 100%;
	border: 1px solid #e3e5e8;
	margin-bottom: 15px;
}
.left-price{
	width: 150px;
	text-align: center;
	font-size: 16px;
	vertical-align: middle;
	border-right: 1px solid #e3e5e8;
}
.left-price span{
	font-size: 20px;
}
.right-form{
	padding-top: 10px;
}
</style>
<body>
<div id="app">
	<table class="half-con">
		<tr>
			<td class="left-price">筛选<br>条件</td>
			<td class="right-form">
				<div class="div-con">
					<label>风机大类</label>
					<input class="easyui-combotree" id="fanType" data-options="width:180,url:'<%=basePath%>/shop/sort/getSortForTree.do',readonly:true" />
					<div class="clear"></div>
				</div>
				<div class="div-con" v-show="paraList.length > 0">
					<div class="para-con" v-for="(item, index) in paraList" :key="index">
						<label>{{item.text}}</label>
						<select v-model="item.val" @change="paraChange" disabled>
							<option v-for="(itm, index) in paraOptions[item.templateInfoId]" :value="itm" :key="index">{{itm}}</option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
			</td>
		</tr>
	</table>
	<table class="half-con">
		<tr>
			<td class="left-price">风<br>机<br>
				成本价{{fanCount.toFixed(2)}}<br>
				报价
				<template v-if="fanProfit !== ''">
					{{(fanCount/(1-fanProfit)).toFixed(2)}}
				</template>
				<template v-else>
					{{fanCount.toFixed(2)}}
				</template>
			</td>
			<td class="right-form">
				<!-- <div class="div-con" style="display: none;">
					<label>风机编号</label>
					<select v-model="fanCode" @change="fanCodeChange" disabled>
						<template v-for="(itm, index) in fanCodeList">
							<option v-for="(value, key) in itm" :value="key" :key="index">{{value}}</option>
						</template>
					</select>
				</div> -->
				<div class="div-con" v-if="materialFlag">
					<label>材料</label>
					<select v-model="curMaterial" @change="materialFChange" disabled>
						<option v-for="(itm, index) in materials" :value="itm.id" :key="index">{{itm.name}}</option>
					</select>
				</div>
				<div class="div-con" v-for="(item, index) in list" :key="index">
					<template v-if="item.children.length > 0">
						<template v-if="item.optionType === 'SINGLE_SELECT'">
							<label>{{item.text}}</label>
							<select v-model="item.val" @change="selectChange(item)" disabled>
								<option v-for="itm1 in item.children" :value="itm1.id" :key="itm1.id">{{itm1.text}}(￥{{itm1.price.toFixed(2)}})</option>
							</select>
						</template>
						<template v-else-if="item.optionType === 'MULTI_SELECT'">
							<label class="checkbox-title">{{item.text}}</label>
							<div class="clear"></div>
							<div class="checkbox-div">
								<div class="checkbox-con" v-for="(itm, index) in item.children" :key="index">
									<input type="checkbox" v-model="itm.check" :name="item.id" @click="checkboxChange(item)" :value="itm.price" disabled>{{itm.text}}(￥{{itm.price.toFixed(2)}})
								</div>
							</div>
							<div class="clear"></div>
						</template>
						<template v-else-if="item.optionType === 'ALL_SELECT'">
							<label>{{item.text}}</label>(￥{{item.price.toFixed(2)}})
						</template>
					</template>
				</div>
			</td>
		</tr>
	</table>
	<table class="half-con">
		<tr>
			<td class="left-price">电<br>机<br>
				成本价{{motorCount.toFixed(2)}}<br>
				报价
				<template v-if="motorProfit !== ''">
					{{(motorCount/(1-motorProfit)).toFixed(2)}}
				</template>
				<template v-else>
					{{motorCount.toFixed(2)}}
				</template>
			</td>
			<td class="right-form">
				<div class="div-con div-half-con">
					<label>电机厂家</label>
					<select v-model="motorObj.motorBrand" @change="changeMotorBrand" disabled>
						<option v-for="(itm, index) in motorBrandList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>电机基座号</label>
					<select v-model="motorObj.motor" @change="changeMotor" disabled>
						<option v-for="(itm, index) in motorList" :value="itm.seatNo" :key="index">基座号:{{itm.seatNo}}级数:{{itm.level}}功率:{{itm.power}}中准价:{{itm.standardPrice}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>系列</label>
					<select v-model="motorObj.motorSeries" @change="changeMotorSeries" disabled>
						<option v-for="(itm, index) in motorSeriesList" :value="itm.seriesText" :key="index">{{itm.seriesText}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>异电压</label>
					<select v-model="motorObj.diffeVoltage" @change="jisuan" disabled>
						<!-- <option value="380V/50HZ">380V/50HZ</option>
						<option value="440V/60HZ">440V/60HZ</option> -->
						<option v-for="(itm, index) in diffeVoltageList" :value="itm.id" :key="index">{{itm.voltagePower}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>IP等级</label>
					<select v-model="motorObj.motorIpLevel" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorIpLevelList" :value="itm.id" :key="index">{{itm.ipLevel}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>安装方式</label>
					<select v-model="motorObj.motorInstallation" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorInstallationList" :value="itm.id" :key="index">{{itm.installation}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>潮温系数</label>
					<select v-model="motorObj.moistTemperature" @change="jisuan" disabled>
						<option v-for="(itm, index) in moistTemperatureList" :value="itm.id" :key="index">{{itm.envirRequest}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>防爆系数</label>
					<select v-model="motorObj.motorAntiExplosion" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorAntiExplosionList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>轴承</label>
					<select v-model="motorObj.motorBearing" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorBearingList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>防护</label>
					<select v-model="motorObj.motorDefend" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorDefendList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>填料函</label>
					<select v-model="motorObj.motorPadding" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorPaddingList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>电机轴</label>
					<select v-model="motorObj.motorShaft" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorShaftList" :value="itm.id" :key="index">{{itm.text}}</option>
					</select>
				</div>
				<div class="div-con div-half-con">
					<label>加热器</label>
					<select v-model="motorObj.motorHeater" @change="jisuan" disabled>
						<option v-for="(itm, index) in motorHeaterList" :value="itm.id" :key="index">{{itm.seriesText}}</option>
					</select>
				</div>
				<div class="clear"></div>
			</td>
		</tr>
	</table>
	<div>
		<div class="profit-con">
			<label>风机毛利</label>
			<input class="easyui-numberbox" id="fanProfit" data-options="width:80,min:0,precision:2,readonly:true" />
		</div>
		<div class="profit-con">
			<label>电机毛利</label>
			<input class="easyui-numberbox" id="motorProfit" data-options="width:80,min:0,precision:2,readonly:true" />
		</div>
		<div class="count-con">
			单价：成本价<span>￥{{count.toFixed(2)}}</span> 报价<span>￥{{countProfit.toFixed(2)}}</span>
		</div>
		<div class="clear"></div>
	</div>
	<div style="margin-top: 8px;">
		<div class="profit-con">
			<label>风机数量</label>
			<input class="easyui-numberbox" id="fanNum" data-options="width:80,min:0,precision:0,readonly:true" />
		</div>
	</div>
	<div style="margin-bottom:10px;">
		<div class="count-con">
			报价总价：<span>￥{{countProfitAll.toFixed(2)}}</span>
		</div>
		<div class="count-con">
			成本总价：<span>￥{{countAll.toFixed(2)}}</span>
		</div>
		<div class="clear"></div>
	</div>
</div>
</body>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
<script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>
<script src="/pages/js/vue.js"></script>
<script>
var basePath = '<%=basePath%>'
var id = '<%=id%>'
console.log(id)
var vm = new Vue({
	el: '#app',
	data: {
			result: [], // json数据
			resultNew: [], // 选材料后新json数据
			list: [], // 半成品列表
			paraList: [], // 参数列表
			allList: [],
			fanCount: 0, // 风机总价
			count: 0, // 总价
			materialFlag: false, // 第三极是否有材料
			materials: [], // 材料选项
			curMaterial: '', // 选中材料
			motorObj: {}, // 电机数据对象
			motorBrandList: [], // 电机厂家下拉列表
			motorList: [], // 电机基座号下拉列表
			motorSeriesList: [], // 电机系列下拉列表
			motorIpLevelList: [], // 电机IP等级下拉列表
			motorInstallationList: [], // 电机安装方式下拉列表
			diffeVoltageList: [], // 电机异电压下拉列表
			motorCount: 0, // 电机总价
			fanCodeList: [], // 风机编号列表
			fanCode: '', // 选择风机编号
			paraOptions: {}, // 参数下拉选项
			fanProfit: '', // 风机毛利
			motorProfit: '', // 电机毛利
			fanNum: '', // 风机数量
			moistTemperatureList: [], // 潮温系数下拉列表
			motorAntiExplosionList: [], // 防爆系数下拉列表
			motorBearingList: [], // 轴承下拉列表
			motorDefendList: [], // 防护下拉列表
			motorPaddingList: [], // 填料函下拉列表
			motorShaftList: [], // 电机轴下拉列表
			motorHeaterList: [] // 加热器下拉列表
	},
	methods: {
		init: function () { // vue初始化
			// this.paraList = []
			this.list = []
			this.allList = []
			for (let i = 0; i < this.result.length; i++) {
				this.allList.push(this.result[i])
				if (this.result[i].attriType === 'PARAMETER' && this.result[i].filter) {
					// this.paraList.push(this.result[i])
				} else if (this.result[i].attriType === 'HALF_PRODUCT') {
					this.list.push(this.result[i])
				}
				if (this.result[i].children.length > 0) {
					this.allList = this.allList.concat(this.result[i].children)
					for (let j = 0; j < this.result[i].children.length; j++) {
						if (this.result[i].children[j].children.length > 0) {
							this.allList = this.allList.concat(this.result[i].children[j].children)
							for (let z = 0; z < this.result[i].children[j].children.length; z++) {
								if (this.result[i].children[j].children[z].attriType === 'MATERIAL') {
									this.materialFlag = true
									break
								}
							}
						}
					}
				}
			}
		},
		initNew: function () { // 选材料后初始化
			/* for (let i = 0; i < this.result.length; i++) {
				if (this.result[i].attriType === 'HALF_PRODUCT') {
					this.result[i].price = this.resultNew[i].price
					if (this.result[i].children.length > 0) {
						for (let j = 0; j < this.result[i].children.length; j++) {
							this.result[i].children[j].price = this.resultNew[i].children[j].price
							if (this.result[i].children[j].children.length > 0) {
								for (let z = 0; z < this.result[i].children[j].children.length; z++) {
									this.result[i].children[j].children[z].price = this.resultNew[i].children[j].children[z].price
								}
							}
						}
					}
					if (this.result[i].optionType === 'SINGLE_SELECT') {
						this.selectChange(this.result[i])
					} else if (this.result[i].optionType === 'MULTI_SELECT') {
						this.checkboxChange(this.result[i])
					}
				}
			} */
			for (let i = 0; i < this.result.length; i++) {
				if (this.result[i].attriType === 'HALF_PRODUCT') {
					let temp = {}
					for (let index = 0; index < this.resultNew.length; index++) {
						if (this.resultNew[index].id === this.result[i].id) {
							temp = this.resultNew[index]
							break
						}
					}
					this.result[i].price = temp.price
					if (this.result[i].children.length > 0) {
						for (let j = 0; j < this.result[i].children.length; j++) {
							let temp1 = {}
							for (let index = 0; index < temp.children.length; index++) {
								if (temp.children[index].id === this.result[i].children[j].id) {
									temp1 = temp.children[index]
									break
								}
							}
							this.result[i].children[j].price = temp1.price
							if (this.result[i].children[j].children.length > 0) {
								for (let z = 0; z < this.result[i].children[j].children.length; z++) {
									let temp2 = {}
									for (let index = 0; index < temp1.children.length; index++) {
										if (temp1.children[index].id === this.result[i].children[j].children[z].id) {
											temp2 = temp1.children[index]
											break
										}
									}
									this.result[i].children[j].children[z].price = temp2.price
								}
							}
						}
					}
					if (this.result[i].optionType === 'SINGLE_SELECT') {
						this.selectChange(this.result[i])
					} else if (this.result[i].optionType === 'MULTI_SELECT') {
						this.checkboxChange(this.result[i])
					}
				}
			}
			console.log(this.result)
		},
		load: function () {
			this.paraList = []
			this.list = []
			this.allList = []
			for (let i = 0; i < this.result.length; i++) {
				if (this.result[i].attriType === 'PARAMETER' && this.result[i].filter) {
					this.paraList.push(this.result[i])
				} else if (this.result[i].attriType === 'HALF_PRODUCT') {
					this.list.push(this.result[i])
					if (this.result[i].optionType === 'SINGLE_SELECT') {
						this.selectChange(this.result[i])
					} else if (this.result[i].optionType === 'MULTI_SELECT') {
						this.checkboxChange(this.result[i])
					}
				}
				if (this.result[i].children.length > 0) {
					for (let j = 0; j < this.result[i].children.length; j++) {
						if (this.result[i].children[j].children.length > 0) {
							for (let z = 0; z < this.result[i].children[j].children.length; z++) {
								if (this.result[i].children[j].children[z].attriType === 'MATERIAL') {
									this.materialFlag = true
									break
								}
							}
						}
					}
				}
			}
		},
		paraChange: function () { // 选择材料
			getFanCode()
		},
		fanCodeChange: function () { // 选择风机编号
			getData(this.fanCode)
		},
		selectChange: function (item) { // 单选报价项选择事件
			for (let i = 0; i < item.children.length; i++) {
				if (item.children[i].id === item.val) {
					item.count = item.children[i].price
					item.children[i].check = true
					break
				}
			}
			this.calculate()
		},
		checkboxChange: function (item) { // 多选报价项选择事件
			item.count = 0
			this.$nextTick(function () {
				for (let i = 0; i < item.children.length; i++) {
					//console.log(item.children[i].check)
					if (item.children[i].check) {
						//console.log('parice=' + item.children[i].price)
						item.count = FloatAdd(item.count, item.children[i].price)
					}
				}
				//console.log(item.count)
				this.calculate()
			})
		},
		calculate: function () { // 计算总价
			this.fanCount = 0
			for (let i = 0; i < this.list.length; i++) {
				if (this.list[i].optionType === 'ALL_SELECT') {
					this.fanCount = FloatAdd(this.fanCount, this.list[i].price)
				} else if (this.list[i].count !== undefined && this.list[i].count !== null && this.list[i].count !== '') {
					this.fanCount = FloatAdd(this.fanCount, this.list[i].count)
				}
			}
			this.countFun()
		},
		materialFChange: function () { // 选择材料
			getNewMaterialData()
		},
		changeMotorBrand: function () { // 选择电机厂家
			this.motorSeriesList = []
			this.motorHeaterList = []
			this.diffeVoltageList = []
			this.moistTemperatureList = []
			getMotorData(this.motorObj.motorBrand)
			getMotorAntiExplosion()
			getMotorBearing()
			getMotorDefend()
			getMotorPadding()
			getMotorShaft()
			getIpLevelData()
			getInstallationData()
		},
		changeMotor: function () { // 选择电机基座号
			this.diffeVoltageList = []
			this.moistTemperatureList = []
			getMotorSeriesData()
			getMotorHeater()
			this.jisuan()
		},
		changeMotorSeries: function () { // 选择系列
			getDiffeVoltageData(this.motorObj.motorSeries)
			getMoistTemperature(this.motorObj.motorSeries)
			this.jisuan()
		},
		jisuan: function () { // 计算电机
			let standardPrice = 0
			let standardFactor = 1
			let diffeVoltageCoeff = 1
			let ipLevelCoeff = 1
			let installationCoeff = 1
			let moistTemperatureCoeff = 1
			let motorAntiExplosionCoeff = 1
			let motorBearingCoeff = 1
			let motorDefendCoeff = 1
			let motorPaddingCoeff = 1
			let motorPaddingPrice = 0
			let motorShaftCoeff = 1
			let motorHeaterPrice = 0
			this.motorCount = 0
			let temp1 = undefined
			for (let index = 0; index < this.motorList.length; index++) {
				if (this.motorList[index].seatNo === this.motorObj.motor) {
					temp1 = this.motorList[index]
					break
				}
			}
			if (temp1 !== undefined) {
				standardPrice = temp1.standardPrice
			}
			let temp2 = undefined
			for (let index = 0; index < this.motorSeriesList.length; index++) {
				if (this.motorSeriesList[index].seriesText === this.motorObj.motorSeries) {
					temp2 = this.motorSeriesList[index]
					break
				}
			}
			if (temp2 !== undefined) {
				standardFactor = temp2.standardFactor
			}
			let temp3 = undefined
			for (let index = 0; index < this.diffeVoltageList.length; index++) {
				if (this.diffeVoltageList[index].id === this.motorObj.diffeVoltage) {
					temp3 = this.diffeVoltageList[index]
					break
				}
			}
			if (temp3 !== undefined) {
				diffeVoltageCoeff = temp3.diffeVoltageCoeff
			}
			let temp4 = undefined
			for (let index = 0; index < this.motorIpLevelList.length; index++) {
				if (this.motorIpLevelList[index].id === this.motorObj.motorIpLevel) {
					temp4 = this.motorIpLevelList[index]
					break
				}
			}
			if (temp4 !== undefined) {
				ipLevelCoeff = temp4.ipLevelCoeff
			}
			let temp5 = undefined
			for (let index = 0; index < this.motorInstallationList.length; index++) {
				if (this.motorInstallationList[index].id === this.motorObj.motorInstallation) {
					temp5 = this.motorInstallationList[index]
					break
				}
			}
			if (temp5 !== undefined) {
				installationCoeff = temp5.installationCoeff
			}
			let temp6 = undefined
			for (let index = 0; index < this.moistTemperatureList.length; index++) {
				if (this.moistTemperatureList[index].id === this.motorObj.moistTemperature) {
					temp6 = this.moistTemperatureList[index]
					break
				}
			}
			if (temp6 !== undefined) {
				moistTemperatureCoeff = temp6.moistTemperatureCoeff
			}
			let temp7 = undefined
			for (let index = 0; index < this.motorAntiExplosionList.length; index++) {
				if (this.motorAntiExplosionList[index].id === this.motorObj.motorAntiExplosion) {
					temp7 = this.motorAntiExplosionList[index]
					break
				}
			}
			if (temp7 !== undefined) {
				motorAntiExplosionCoeff = temp7.coeff
			}
			let temp8 = undefined
			for (let index = 0; index < this.motorBearingList.length; index++) {
				if (this.motorBearingList[index].id === this.motorObj.motorBearing) {
					temp8 = this.motorBearingList[index]
					break
				}
			}
			if (temp8 !== undefined) {
				motorBearingCoeff = temp8.coeff
			}
			let temp9 = undefined
			for (let index = 0; index < this.motorDefendList.length; index++) {
				if (this.motorDefendList[index].id === this.motorObj.motorDefend) {
					temp9 = this.motorDefendList[index]
					break
				}
			}
			if (temp9 !== undefined) {
				motorDefendCoeff = temp9.coeff
			}
			let temp10 = undefined
			for (let index = 0; index < this.motorPaddingList.length; index++) {
				if (this.motorPaddingList[index].id === this.motorObj.motorPadding) {
					temp10 = this.motorPaddingList[index]
					break
				}
			}
			if (temp10 !== undefined) {
				this.motorObj.priceOfCoeff = temp10.priceOfCoeff
				if (temp10.priceOfCoeff) {
					motorPaddingPrice = parseFloat(temp10.price)
				} else {
					motorPaddingCoeff = temp10.coeff
				}
			}
			let temp11 = undefined
			for (let index = 0; index < this.motorShaftList.length; index++) {
				if (this.motorShaftList[index].id === this.motorObj.motorShaft) {
					temp11 = this.motorShaftList[index]
					break
				}
			}
			if (temp11 !== undefined) {
				motorShaftCoeff = temp11.coeff
			}
			let temp12 = undefined
			for (let index = 0; index < this.motorHeaterList.length; index++) {
				if (this.motorHeaterList[index].id === this.motorObj.motorHeater) {
					temp12 = this.motorHeaterList[index]
					break
				}
			}
			if (temp12 !== undefined) {
				motorHeaterPrice = parseFloat(temp12.price)
			}
			this.motorCount = standardPrice*standardFactor*diffeVoltageCoeff*ipLevelCoeff*installationCoeff*moistTemperatureCoeff*motorAntiExplosionCoeff*motorBearingCoeff*motorDefendCoeff*motorPaddingCoeff*motorShaftCoeff+motorPaddingPrice+motorHeaterPrice
			this.countFun()
		},
		countFun: function () { // 计算总价
			this.count = this.fanCount + this.motorCount
		},
		newQuote: function () { // 新增报价单
			newQuote()
		},
		addToQuote: function () { // 添加到报价单中
			addToQuote()
		}
	},
	computed: {
		countProfit: function () { // 单价报价
			let temp = 0
			if (this.fanProfit != '') {
				temp += this.fanCount / (1 - this.fanProfit)
			} else {
				temp += this.fanCount
			}
			if (this.motorProfit != '') {
				temp += this.motorCount / (1 - this.motorProfit)
			} else {
				temp += this.motorCount
			}
			return temp
		},
		countAll: function () { // 成本总价
			if (this.fanNum !== '') {
				return this.fanNum * this.count
			} else {
				return this.count
			}
		},
		countProfitAll: function () { // 报价总价
			if (this.fanNum !== '') {
				return this.fanNum * this.countProfit
			} else {
				return this.countProfit
			}
		}
	}
})
// var id = shuoheUtil.getUrlHeader().id
function changeFanType (id) {
	$.ajax({
		url: basePath+"/shop/template/info/getInfoBySortIdForTree.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			sortId: id
		},
		error: function() {},
		success: function(data)//成功
		{
			//console.log(data)
			vm.$data.paraList = []
			for (let i = 0; i < data.length; i++) {
				if (data[i].attriType === 'PARAMETER' && data[i].filter) {
					vm.$data.paraList.push(data[i])
					getParaOptions()
				}
			}
			getFanCode()
		}
	})
}
$(function() {
	// 选择风机大类
	$('#fanType').combotree({
		onSelect: function (row){
			//changeFanType(row.id)
		}
	})
	// 取材料列表
	$.ajax({
		url: basePath+"/develop/url/getUrl.do?name=getMaterialComboList",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.materials = data
		}
	})
	// 取电机厂家
	$.ajax({
		url: basePath+"/shop/motor/brand/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorBrandList = data
		}
	})
	// 风机毛利更改
	$('#fanProfit').numberbox({
		onChange: function (newValue, oldValue) {
			vm.$data.fanProfit = newValue
			vm.countFun()
		}
	})
	// 电机毛利更改
	$('#motorProfit').numberbox({
		onChange: function (newValue, oldValue) {
			vm.$data.motorProfit = newValue
			vm.countFun()
		}
	})
	// 数量更改
	$('#fanNum').numberbox({
		onChange: function (newValue, oldValue) {
			vm.$data.fanNum = newValue
		}
	})
	getData(id)
})
// 取电机基座号
function getMotorData (val) {
	$.ajax({
		url: basePath+"/shop/motor/motor/findByBrandId.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: val
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorList = data
		}
	})
}
// 取系列
function getMotorSeriesData () {
	$.ajax({
		url: basePath+"/shop/motor/seriesStandardRel/findByStandardText.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand,
			standardText: vm.$data.motorObj.motor
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorSeriesList = data
		}
	})
}
// 取异电压
function getDiffeVoltageData (val) {
	$.ajax({
		url: basePath+"/shop/motor/diffeVoltage/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			seriesText: val,
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.diffeVoltageList = data
		}
	})
}
// 取IP等级
function getIpLevelData () {
	$.ajax({
		url: basePath+"/shop/motor/motorIpLevel/list.do",
		type: "POST",
		dataType:'json',
		//async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorIpLevelList = data
		}
	})
}
// 取安装方式
function getInstallationData () {
	$.ajax({
		url: basePath+"/shop/motor/motorInstallation/list.do",
		type: "POST",
		dataType:'json',
		//async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorInstallationList = data
		}
	})
}
//潮温系数
function getMoistTemperature (val) {
	$.ajax({
		url: basePath+"/shop/motor/moistTemperature/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			seriesText: val,
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.moistTemperatureList = data
		}
	})
}
//防爆系数
function getMotorAntiExplosion () {
	$.ajax({
		url: basePath+"/shop/motor/motorAntiExplosion/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorAntiExplosionList = data
		}
	})
}
//轴承
function getMotorBearing () {
	$.ajax({
		url: basePath+"/shop/motor/motorBearing/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorBearingList = data
		}
	})
}
//防护
function getMotorDefend () {
	$.ajax({
		url: basePath+"/shop/motor/motorDefend/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorDefendList = data
		}
	})
}
//填料函
function getMotorPadding () {
	$.ajax({
		url: basePath+"/shop/motor/motorPadding/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorPaddingList = data
		}
	})
}
//电机轴
function getMotorShaft () {
	$.ajax({
		url: basePath+"/shop/motor/motorShaft/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorShaftList = data
		}
	})
}
//加热器
function getMotorHeater () {
	$.ajax({
		url: basePath+"/shop/motor/motorHeater/list.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			seriesText: vm.$data.motorObj.motor,
			brandId: vm.$data.motorObj.motorBrand
		},
		error: function() {},
		success: function(data)//成功
		{
			vm.$data.motorHeaterList = data
		}
	})
}
// 取风机编号列表
function getFanCode () {
	var temp = $('#fanType').combotree('getValue')
	var dataTemp = []
	for (let i = 0; i < vm.$data.paraList.length; i++) {
		if (vm.$data.paraList[i].val !== '' && vm.$data.paraList[i].val !== undefined && vm.$data.paraList[i].val !== null) {
			dataTemp.push({key: vm.$data.paraList[i].text, value: vm.$data.paraList[i].val})
		}
	}
	//console.log(dataTemp)
	$.ajax({
		url: basePath+"/shop/product/getProductByLineAndParams.do",
		type: "POST",
		dataType:'json',
		data:
		{
			productLine: temp,
			data: JSON.stringify(dataTemp)
		},
		error: function() {},
		success: function(data)//成功
		{
			//console.log(data)
			vm.$data.fanCodeList = data
		}
	})
}
// 取json数据
function getData (id) {
	$.ajax({
		url: "/shop/history/getHistoryProductById.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			id: id
		},
		error: function() {},
		success: function(data)//成功
		{
			console.log(data)
			vm.$data.fanType = data.productLineId
			$('#fanType').combotree('setValue', data.productLineId)
			vm.$data.curMaterial = data.materialId
			vm.$data.fanCode = data.productNumber
			vm.$data.motorObj = data.motor
			vm.$data.paraList = data.params
			vm.$data.result = JSON.parse(data.productJson)
			vm.$data.fanNum = data.num
			vm.$data.fanProfit = data.fanProfitRate
			vm.$data.motorProfit = data.motorProfitRate
			$('#fanProfit').numberbox('setValue', data.fanProfitRate)
			$('#motorProfit').numberbox('setValue', data.motorProfitRate)
			$('#fanNum').numberbox('setValue', data.num)
			vm.init()
			getParaOptions()
			getFanCode()
			vm.changeMotorBrand()
			vm.changeMotor()
			vm.changeMotorSeries()
			vm.calculate()
		}
	})
}
// 取参数选项
function getParaOptions () {
	for (let i = 0; i < vm.$data.paraList.length; i++) {
		var temp = vm.$data.paraList[i].templateInfoId
		//vm.$data.paraOptions[temp] = []
		$.ajax({
			url: basePath+"/shop/product/getProductParametersByTemplateInfoId.do",
			type: "POST",
			dataType:'json',
			async: false,
			data:
			{
				templateInfoId: temp
			},
			error: function() {},
			success: function(data)//成功
			{
				vm.$data.paraOptions[temp] = data
			}
		})
	}
}
// 选材料后取数据
function getNewMaterialData () {
	$.ajax({
		url: basePath+"/shop/product/getPorductTreeListByNewMaterial.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			productId: vm.$data.fanCode,
			materialId: vm.$data.curMaterial
		},
		error: function() {},
		success: function(data)//成功
		{
			console.log(data)
			vm.$data.resultNew = data
			vm.initNew()
			vm.calculate()
		}
	})
}
function FloatAdd(arg1,arg2){
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
}
</script>
</html>