<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
</head>
<style type="text/css"> 
body{
	font-size: 14px;
}
.clear:after {
  clear: both;
  display: block;
  content: "\20";
  height: 0;
  visibility: hidden;
}
.clear {
  zoom: 1;
  _line-height: 0;
}
.div-con{
	margin-bottom: 15px;
}
.para-con{
	float: left;
	width: 33.33%;
}
label{
	display: inline-block;
	width: 80px;
	text-align: right;
	padding-right: 8px;
}
input[type="text"], select{
	height: 30px;
	line-height: 30px;
	width: 180px;
}
input[type="checkbox"]{
	margin-left: 15px;
}
.count-con{
	text-align: right;
	padding-right: 20px;
	font-size: 16px;
}
</style>
<body>
<div id="app">
	<div>
		<div class="div-con">
			<div class="para-con" v-for="(item, index) in paraList" :key="index">
				<label>{{item.text}}</label>
				<select v-model="item.val">
					<option v-for="(itm, index) in item.children" :value="itm.id" :key="index">{{itm.text}}--{{itm.price.toFixed(2)}}</option>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="div-con" v-if="materialFlag">
			<label>材料</label>
			<select v-model="curMaterial" @change="materialFChange">
				<option v-for="(itm, index) in materials" :value="itm.id" :key="index">{{itm.name}}</option>
			</select>
		</div>
		<div class="div-con" v-for="(item, index) in list" :key="index">
			<template v-if="item.children.length > 0">
				<template v-if="item.optionType === 'SINGLE_SELECT'">
					<label>{{item.text}}</label>
					<select v-model="item.val" @change="selectChange(item)">
						<template v-for="(itm, index) in item.children">
							<template v-if="itm.quote">
								<option :value="itm.id" :key="index">{{itm.text}}--{{itm.price.toFixed(2)}}</option>
							</template>
						</template>
					</select>
				</template>
				<template v-else-if="item.optionType === 'MULTI_SELECT'">
					<label>{{item.text}}</label>
					<div>
						<template v-for="(itm, index) in item.children">
							<input type="checkbox" v-model="itm.checked" :name="item.id" @click="checkboxChange(item)" :value="itm.price">{{itm.text}}--{{itm.price.toFixed(2)}}
						</template>
					</div>
				</template>
				<template v-else-if="item.optionType === 'ALL_SELECT'">
					<label>{{item.text}}--{{item.price.toFixed(2)}}</label>
				</template>
			</template>
		</div>
	</div>
	<div class="count-con">
		总价：<span>{{count.toFixed(2)}}</span>
	</div>
</div>
</body>
<!-- <script src="/pages/js/jquery.min.js"></script> -->
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>

<script src="/pages/js/vue.js"></script>
<script>
var vm = new Vue({
	el: '#app',
	data () {
		return {
			result: [],
			list: [],
			paraList: [],
			count: 0,
			materialFlag: false,
			materials: [],
			curMaterial: ''
		}
	},
	methods: {
		init: function () { // vue初始化
			this.paraList = []
			this.list = []
			for (let i = 0; i < this.result.length; i++) {
				if (this.result[i].attriType === 'PARAMETER' && this.result[i].filter) {
					this.paraList.push(this.result[i])
				} else if (this.result[i].attriType === 'HALF_PRODUCT') {
					this.list.push(this.result[i])
				}
				if (this.result[i].children.length > 0) {
					for (let j = 0; j < this.result[i].children.length; j++) {
						if (this.result[i].children[j].children.length > 0) {
							for (let z = 0; z < this.result[i].children[j].children.length; z++) {
								if (this.result[i].children[j].children[z].attriType === 'MATERIAL') {
									this.materialFlag = true
									break
								}
							}
						}
					}
				}
			}
			console.log(this.list)
		},
		selectChange: function (item) { // 单选报价项选择事件
			for (let i = 0; i < item.children.length; i++) {
				if (item.children[i].id === item.val) {
					item.count = item.children[i].price
					break
				}
			}
			this.calculate()
		},
		checkboxChange: function (item) { // 多选报价项选择事件
			item.count = 0
			this.$nextTick(function () {
				for (let i = 0; i < item.children.length; i++) {
					//console.log(item.children[i].checked)
					if (item.children[i].checked) {
						//console.log('parice=' + item.children[i].price)
						item.count = FloatAdd(item.count, item.children[i].price)
					}
				}
				//console.log(item.count)
				this.calculate()
			})
		},
		calculate: function () { // 计算总价
			this.count = 0
			for (let i = 0; i < this.list.length; i++) {
				if (this.list[i].optionType === 'ALL_SELECT') {
					this.count = FloatAdd(this.count, this.list[i].price)
				} else if (this.list[i].count !== undefined && this.list[i].count !== null && this.list[i].count !== '') {
					this.count = FloatAdd(this.count, this.list[i].count)
				}
			}
		},
		materialFChange: function () { // 选择材料
			getData()
		}
	}
})
$(function() {
// 297eb3ba67b113440167b13955a70000

	// 取材料列表
	$.getJSON("/shop/product/getProductInfoForTreeByProductId.do?productId=297eb3ba67b113440167b13f783b0041", function (data){
		vm.$data.materials = data
	})
	getData()
})
// 取json数据
function getData () {
	console.log(vm.$data.curMaterial)
	$.getJSON("/shop/product/getProductInfoForTreeByProductId.do?productId=297eb3ba67b113440167b13f783b0041", function (data){
		vm.$data.result = data
		vm.init()
		vm.calculate()
	})
}
function FloatAdd(arg1,arg2){
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
}
</script>
</html>