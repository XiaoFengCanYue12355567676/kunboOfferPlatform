<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
		User user = null;
		String userJson = null;
		PrintWriter ss = response.getWriter();
		try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
         	String topLocation = "<script>top.location.href='"+basePath+"/pages/login.jsp'</script>";
          ss.print(topLocation);
        }else{
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href='"+basePath+"/pages/login.jsp'</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }
%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
</head>
<style type="text/css"> 
body{
	font-size: 14px;
	padding-top: 40px;
}
.div-con{
	margin-bottom: 15px;
	padding-bottom: 5px;
}
label{
	display: inline-block;
	width: 110px;
	text-align: right;
	padding-right: 8px;
}
input[type="text"], select{
	height: 30px;
	line-height: 30px;
	width: 180px;
}
.save-btn{
	text-align: center;
	margin-top: 40px;
}
.save-btn button{
	width: 140px;
	height: 38px;
	line-height: 38px;
	border-radius: 3px;
	border: 1px solid #e3e5e8;
	background: #fff;
	cursor: pointer;
	margin-left: 15px;
}
</style>
<body>
<div id="app">
	<div class="div-con">
		<label>报价单：</label>
		<select v-model="quote">
			<option v-for="(itm, index) in quoteList" :value="itm.id" :key="index">{{itm.text}}</option>
		</select>
	</div>
	<div class="save-btn">
		<button type="button" @click="save">确定</button>
	</div>
</div>
</body>

<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/pages/js/moment.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
<script src="/pages/js/vue.js"></script>
<script>
var user = <%=userJson%>;
var basePath = '<%=basePath%>'
console.log(user)
var vm = new Vue({
	el: '#app',
	data: {
			quote: '', // 报价单
			quoteList: [] // 报价单列表
	},
	methods: {
		init: function () { // vue初始化
			
		},
		save: function () { // 保存
			save(this.quote)
		}
	}
})
var obj = {}
$(function() {
	// 取报价单列表
	$.ajax({
		url: basePath+"/shop/history/order/getOrderByUser.do",
		type: "POST",
		dataType:'json',
		data:
		{
			userId: user.id
		},
		error: function() {},
		success: function(data)//成功
		{
			console.log(data)
			vm.$data.quoteList = data
		}
	})
	if(parent.transData && typeof(parent.transData)=="function"){
		obj = parent.transData()
		console.log(obj)
	}
})
// 保存
function save (id) {
	if (id == '') {
		alert('请选择报价单')
		return false
	}
	shuoheUtil.layerSaveMaskOn();
	$.ajax({
		url: "/shop/history/saveHistoryPorductOfferOrder.do",
		type: "POST",
		dataType:'json',
		async: false,
		data:
		{
			productInfoList: JSON.stringify(obj.fanArr),
			orderId: id,
			motor: JSON.stringify(obj.motor),
			params: JSON.stringify(obj.paraList),
			productLineId: obj.fanType,
			productJson: JSON.stringify(obj.fan),
			productId: obj.fanCode,
			materialId: obj.material,
			totalPrice: obj.countAll,
			unitPrice: obj.count,
			num: obj.fanNum,
			fanProfitRate: obj.fanProfit,
			motorProfitRate: obj.motorProfit,
			offerUnitPrice: obj.countProfit,
			offerTotalPrice: obj.countProfitAll
		},
		error: function() {
			shuoheUtil.layerSaveMaskOff();
            shuoheUtil.layerTopMsgError('通讯失败');
		},
		success: function(data)//成功
		{
			console.log(data)
			shuoheUtil.layerSaveMaskOff();
            if (data.result == true) {
                parent.parent.closeLayer();       
                shuoheUtil.layerTopMsgOK('保存成功');
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
				parent.layer.close(index); //再执行关闭
				parent.closeLayer();       
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
		}
	})
}
</script>
</html>