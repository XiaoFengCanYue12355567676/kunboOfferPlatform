<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>报价管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <link type="text/css" href="buttons/buttons.css" rel="stylesheet">

    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body style="overflow: hidden;" class="easyui-layout">
    <div data-options="region:'center',split:true" style="height:100%;width:50%;">
        <div style="padding: 8px;">
            <form id="queryForm">
                客户名称：<input class="easyui-combobox" id="searchCustomer" data-options="url:'/transmit/getCustomer.do',valueField:'text',textField:'customer_name',panelWidth:400,width:140">
                &nbsp;&nbsp;报价单：<input class="easyui-textbox" id="searchName" data-options="width:120">
                <a href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 15px" data-options="iconCls:'fa fa-search',plain:true" onclick="formsearch()">搜索</a>
            </form>
        </div>
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                idField:'templateInfoId',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                pageSize:30,
                toolbar:'#tb'"
            >
            <thead>
                <tr>
                    <!-- <th field="check"  checkbox="true">uuid</th>                 -->
                    <th field="id"  hidden ="true">uuid</th>                
                    <th field="text">报价单号</th>                
                    <th field="quotePeopleName">填录人</th>
                    <th field="customerName">客户名称</th>
                    <!-- <th field="productTypeCount">种类</th>   -->
                    <th field="productCount">数量</th>  
                    <th field="totalPrice" formatter='Easyui.Datagraid.formatterDecimal2'>成本总价</th>  
                    <th field="offerTotalPrice" formatter='Easyui.Datagraid.formatterDecimal2'>报价总价</th>                      
                    <th field="quoteDate">报价单创建时间</th>  
                    <th field="quoteLastModifyDate">最后修改时间</th>                  
                </tr>
            </thead>
        </table>    
        <div id="tb" style="height:35px">
            <!-- <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a> -->
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" 
                style="margin-left: 35px"
                data-options="iconCls:'fa fa-plus',plain:true" 
                onclick="funDgCreate()">新建</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-edit',plain:true" 
                onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-trash',plain:true" 
                onclick="funDgDelete()">删除</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-jpy',plain:true" 
                onclick="quote()">报价</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
	            data-options="iconCls:'fa fa-download',plain:true" 
	            onclick="exportPdf()">导出内部报价</a>
	        <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
	            data-options="iconCls:'fa fa-download',plain:true" 
	            onclick="exportPdf2()">导出客户报价</a>
	            
	        <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
	            data-options="iconCls:'fa fa-download',plain:true" 
	            onclick="funExportXML()">发起流程</a>
            <!-- <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-hand-pointer-o',plain:true" 
                onclick="funDgEdit()">查看</a>             -->
        </div>
    </div>       
    <div data-options="region:'east',iconCls:'icon-reload',split:true"  style="height:100%;width:50%;">
        <table id="dg2" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                idField:'templateInfoId',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb2   '"
            >
            <thead>
                <tr>
                    <!-- <th field="check"  checkbox="true">uuid</th>                 -->
                    <th field="chakan" formatter='formatterView'>查看</th>
                    <!-- <th field="isChanged" formatter='formatterIsChanged'>变更</th> -->
                    <th field="discard" formatter='formatterDeal'>废弃</th>      
                    <th field="id" hidden>uuid</th>                
                    <th field="productNumber">风机型号</th>                
                    <th field="fanProfitRate">风机利润率</th>  
                    <th field="motorProfitRate">电机利润率</th>        
                    <th field="unitPrice" formatter='Easyui.Datagraid.formatterDecimal2'>成本单价</th>                
                    <th field="offerUnitPrice" formatter='Easyui.Datagraid.formatterDecimal2'>报价单价</th>
                    <th field="num">数量</th>
                    <th field="totalPrice" formatter='Easyui.Datagraid.formatterDecimal2'>成本总价</th>  
                    <th field="offerTotalPrice" formatter='Easyui.Datagraid.formatterDecimal2'>报价总价</th>  
                    <th field="quoteDate">报价时间</th>       
      
                </tr>
            </thead>
        </table>    
  
    </div>
    <!-- 通过form为载体，将所需数据导出成pdf -->  
    <form id="exportPdfForm" style="display:none">
        <input id="quoteId" name="quoteId" type="text"/>
    </form>     
  <script type="text/javascript">
    var typeId;
    var user = new Object();
    var quoteId;
 	//表格初始化 
    $(function(){

        user = Easyui.User.getUser();
        console.info('user = '+JSON.stringify(user));
        getData()
        // $('#dg').treegrid('loadData',dgdata);
    }); 
     function getData () {
        $('#dg').datagrid({
            url:"/shop/history/order/getOrderByUserForPage.do",
            queryParams:{
                userId: '',
                customerName: $('#searchCustomer').combobox('getText'),
                text: $('#searchName').textbox('getValue')
            },
            onClickRow:function(index, row)
            {
            	quoteId = row.id;
                console.info(JSON.stringify(row));
                $('#dg2').datagrid({
                    url:"/shop/history/getHistoryProductByOrderId.do",
                    queryParams:{
                        'orderId':row.id
                    },
                    onLoadSuccess:function(data)
                    {
                        console.info('onLoadSuccess ='+JSON.stringify(data));
                        for(var i=0;i<data.rows.length;i++)
                        {
                            console.info('i = '+i+' id ='+data.rows[i].id);
                            $('#delete_'+data.rows[i].id).linkbutton({});
                            $('#change_'+data.rows[i].id).linkbutton({});
                            $('#view_'+data.rows[i].id).linkbutton({});
                        }
                    },

                });
            }
        });
     }

  
	</script>
</body> 
</html>
<script type="text/javascript">
    function funDgCreate()
    {
        var dg_row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(dg_row))
        {
            dg_row = new Object();
            dg_row.id = '';
        }
        Easyui.Layer.openLayerWindow(
            '新建报价单',
            'form/addQuoteOrder1.jsp',
            ['700px','500px']);  
    }

    function closeLayer()
    {
        $('#dg').datagrid('reload');
        $('#dg2').datagrid('reload');
    }

    function quote()
    {
        var dg_row = Easyui.Datagraid.getSelectRow('dg')
        var id = ''
        var from = 'product'
        if(!Easyui.Object.isEmptyObject(dg_row))
        {
            id = dg_row.id
            from = 'quote'
        }
        Easyui.Layer.openLayerWindow(
            '产品报价',
            '../quote/offer.jsp?from='+from+'&orderId='+id,
            ['1050px',document.body.clientHeight-40+"px"]);
    }

    function funDgEdit()
    {
        var dg_row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(dg_row))
        {
            shuoheUtil.layerTopMsgError('修改前先选择一条数据');
            return;
        }
        Easyui.Layer.openLayerWindow(
            '编辑报价单',
            'form/editQuoteOrder.jsp?id='+dg_row.id,
            ['700px','500px']);  
    }
    function funDgDelete()
    {
        var row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('删除前请先选择一条数据');
        }
        else
        {
            $.messager.defaults = { ok: '确定',cancel: '取消'};
            $.messager.confirm('确认删除', "<font size='3px'>您确认将删除这个报价单么?</font>", 
                function(r){
                    console.info('r = '+r);
                    if (r){
                    console.info('doFunction');
                    deleteHistoryProductById(row);
                }
            });
        }
    }
    function deleteHistoryProductById(row)
    {
        var r = Easyui.Ajax.get('/shop/history/order/deleteOrderById.do?orderId='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#dg').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#dg').datagrid('reload');
        }
    }
    function funExportXML()
    {
        var row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('发起流程前请先选择一条数据');
        }
        else
        {
            $.messager.defaults = { ok: '确定',cancel: '取消'};
            $.messager.confirm('确认发起流程', "<font size='3px'>您确认将发起流程这个流程么?</font>", 
                function(r){
                    console.info('r = '+r);
                    if (r){
                    console.info('doFunction');
                    exportXML(row);
                }
            });
        }
    }
    function exportXML(row)
    {
        var r = Easyui.Ajax.get('/shop/history/order/exportXML.do?orderId='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('发起成功');
            $('#dg').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('发起失败');
            $('#dg').datagrid('reload');
        }
    }
    
    function formatterDeal(value,row,index)
    {
        if(row.discard == true)
        {
            var htmlstr =  '<a id="delete_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="plain:true" \
                            >--</a>';
        }
        else
        {
            var htmlstr =  '<a id="delete_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="iconCls:\'fa fa-trash\',plain:true" \
                            onclick="funDg2Delete('+'\''+row.id+'\')"></a>';
        }
        return htmlstr;
    }
    function formatterView(value,row,index)
    {

        var htmlstr =  '<a id="view_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton "'
                    +        'data-options="iconCls:\'fa fa-hand-pointer-o\',plain:true" '
                    +        'onclick="funDg2View('+'\''+row.id+'\')"></a>';
        return htmlstr;
    }    
    
    function formatterIsChanged(value,row,index)
    {
        var htmlstr;
        /* if(row.changed == false)
        {
             htmlstr =  '<a id="change_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton "'
                        +    'data-options="plain:true" '
                        +    'onclick="funDg2ViewNoChange()">--</a>';
        }
        else
        { */
             htmlstr =  '<a id="change_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="iconCls:\'fa fa-pencil-square-o text-danger\',plain:true" \
                            onclick="funDg2ViewChange('+'\''+row.id+'\')"></a>';
        // }

        return htmlstr;
    }
    function funDg2ViewNoChange()
    {
        shuoheUtil.layerTopMsgError('没有产生变更');
    }
    function funDg2ViewChange(id)
    {
        Easyui.Layer.openLayerWindow(
            '产品报价',
            'editOffer.jsp?from=quote&id='+id,
            ['1050px',document.body.clientHeight-40+"px"])
    }
    function funDg2View(id)
    {
        console.info('funDg2View = '+id);
        Easyui.Layer.openLayerWindow(
            '查看报价单',
            'viewOffer.jsp?id='+id,
            ['1050px',document.body.clientHeight-40+"px"]);  
    }
    function funDg2Delete(id)
    {
        console.info('funDg2Delete = '+id);
        shuoheUtil.layerSaveMaskOn();
        $.ajax({
            url: "/shop/history/discardProductById.do",
            type: "POST",
            dataType:'json',
            async: false,
            data:
            {            
                id: id
            },
            error: function() {
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data1)//成功
            {
                console.log(data1)
                shuoheUtil.layerSaveMaskOff();
                shuoheUtil.layerTopMsgOK('保存成功');
                $('#dg2').datagrid('reload');
                $('#dg').datagrid('reload');
            }
        })        
    }
    //导出weipdf
    function exportPdf(){
    	var dg_row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(dg_row))
        {
            shuoheUtil.layerTopMsgError('请选择要导出的数据');
            return;
        }
        var exportUrl = '/shop/history/exportPdf';
        $('#exportPdfForm').attr('action',exportUrl).attr('method', 'post');//设置提交到的url地址
    	$('#exportPdfForm').find('input[name="quoteId"]').val(quoteId);//将风机id传到后台
    	$('#exportPdfForm').submit();
    	
    }
    //导出weipdf
    function exportPdf2(){
    	var dg_row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(dg_row))
        {
            shuoheUtil.layerTopMsgError('请选择要导出的数据');
            return;
        }
        var exportUrl = '/shop/history/exportPdf2';
        $('#exportPdfForm').attr('action',exportUrl).attr('method', 'post');//设置提交到的url地址
    	$('#exportPdfForm').find('input[name="quoteId"]').val(quoteId);//将风机id传到后台
    	$('#exportPdfForm').submit();
    	
    }
    // 搜索
    function formsearch () {
        getData()
    }
</script>