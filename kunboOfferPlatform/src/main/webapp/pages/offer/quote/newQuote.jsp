<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
</head>
<style type="text/css"> 
body{
	font-size: 14px;
	padding-top: 40px;
}
.div-con{
	margin-bottom: 15px;
	padding-bottom: 5px;
}
label{
	display: inline-block;
	width: 110px;
	text-align: right;
	padding-right: 8px;
}
.save-btn{
	text-align: center;
	margin-top: 30px;
}
.save-btn button{
	width: 140px;
	height: 38px;
	line-height: 38px;
	border-radius: 3px;
	border: 1px solid #e3e5e8;
	background: #fff;
	cursor: pointer;
	margin-left: 15px;
}
</style>
<body>
<div>
	<div class="div-con">
		<label>报价人员：</label>
		<span id="userName"></span>
	</div>
	<div class="div-con">
		<label>客户：</label>
		<input class="easyui-combobox" name="" id="customer" data-options="width:280" />
	</div>
	<div class="div-con">
		<label>报价单别称：</label>
		<input class="easyui-textbox" id="quoteName" data-options="width:280">
	</div>
	<div class="save-btn" >
		<button type="button" onclick="save">确定</button>
	</div>
</div>
</body>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
<script>

var obj = {}
var customerList = []
$(function() {

	user = Easyui.User.getUser();
    console.info('user = '+JSON.stringify(user));

	$('#userName').text(user.actual_name)

	$('#customer').combobox({
		url:'/transmit/getCustomer.do',
		valueField:'text',    
		textField:'customer_name',
		onSelect: function(rec){    
			console.info(rec);

		}
	});


	// // // 取客户列表
	// // $.ajax({
	// // 	url: 'http://27.221.114.67:9003/crm/ActionFormUtil/getByTableNameAll.do',
	// // 	type: "POST",
	// // 	dataType:'json',
	// // 	data:
	// // 	{
	// // 		tableName: 'business_cloud_customer_information',
	// // 		create_user_id: 1010
	// // 	},
	// // 	error: function() {},
	// // 	success: function(data)//成功
	// // 	{
	// // 		customerList = data.rows
	// // 		$('#customer').combobox({          
	// // 	    valueField:'id',    
	// // 			textField:'customer_name', 
	// // 			data: customerList,
	// // 			panelWidth: 320
	// // 		})
	// // 	}
	// // })
	// if(parent.transData && typeof(parent.transData)=="function"){
	// 	obj = parent.transData()
	// 	console.log(obj)
	// }
})
// 保存
function save () {
	
}
</script>
</html>