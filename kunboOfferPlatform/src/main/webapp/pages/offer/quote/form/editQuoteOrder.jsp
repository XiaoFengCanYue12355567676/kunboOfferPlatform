<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建工时</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >  
        <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px" >
        	<tr>
                <td colspan="1" class="label">报价单号</td>
	            <td colspan="4" >
                    <input id='text' class="easyui-textbox" 
                        data-options="required:true,width:450,editable:true" 
                        prompt='报价单别称'>  
	            </td>
            </tr>	
            <tr>
            	<td class="label">报价人</td>
	            <td >
                    <input id='quotePeople' class="easyui-textbox" 
                        data-options="required:true,width:150,editable:true" 
                        prompt='报价人'>     
                </td>
                <td class="label">报价人BPM账号</td>
                <td >
                    <input id='quotePeopleAccount' class="easyui-textbox" 
                        data-options="required:true,width:150,editable:true" 
                        prompt='报价人BPM账号'>     
                </td>
            </tr>	            
	        <tr>
                <td colspan="1" class="label">客户</td>
	            <td colspan="4" >
                    <input id='customer' class="easyui-combobox" 
                        data-options="required:true,width:450,editable:true"
                        prompt='客户'>
	            </td>
            </tr>	
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='partsName' class="easyui-numberbox" data-options="required:false,width:150">      -->
                </td>
            </tr>	            
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:150,editable:true" > -->
                </td>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">


    var id = shuoheUtil.getUrlHeader().id;

    // var HistoryOfferOrder = new Object();
    // HistoryOfferOrder.id = '';
    // HistoryOfferOrder.text = '';
    // HistoryOfferOrder.quotePeopleId = '';
    // HistoryOfferOrder.quotePeopleName = '';
    // HistoryOfferOrder.quoteDate = '';
    // HistoryOfferOrder.quoteLastModifyDate = '';
    // HistoryOfferOrder.customerName = '';
    // HistoryOfferOrder.customerId = '';
    // HistoryOfferOrder.totalPrice = '';


    var user = new Object();

 	$(function() {

        user = Easyui.User.getUser();
        console.info('user = '+JSON.stringify(user));

        //$('#quotePeople').textbox('setValue',user.actual_name);
        $('#quotePeople').textbox('setValue',user.actual_name);
        //textbox('getText');quotePeopleName
        $('#customer').combobox({
            url:'/transmit/getCustomer.do',
            valueField:'text',    
            textField:'customer_name',
            onSelect: function(rec){    
                console.info(rec);

            }
        });

        var re = Easyui.Ajax.get('/shop/history/order/getOrderById.do?orderId='+id);
        HistoryOfferOrder = re.obj;
        console.info('HistoryOfferOrder = '+JSON.stringify(HistoryOfferOrder));
        $('#quotePeople').textbox('setValue',HistoryOfferOrder.quotePeopleName);
        $('#quotePeopleAccount').textbox('setValue',HistoryOfferOrder.quotePeopleAccount);
        if(re.result == false)
        {
            
        }
        else
        {
            $('#customer').combobox('setValue',HistoryOfferOrder.customerId);
            $('#text').textbox('setValue',HistoryOfferOrder.text);
        }        
	}); 

 	// $(function() {
	//   $('#pc_pid').textbox('setValue',GetQueryString('uuid'));
	// }); 
	
  function save() {

    if(shuoheUtil.checkRequired() == false) return;
    HistoryOfferOrder.text = $('#text').textbox('getText');
    HistoryOfferOrder.customerName = $('#customer').combobox('getText');
    HistoryOfferOrder.customerId = $('#customer').combobox('getValue');;
    HistoryOfferOrder.quotePeopleName = $('#quotePeople').textbox('getText');
    HistoryOfferOrder.quotePeopleAccount = $('#quotePeopleAccount').textbox('getText');
    shuoheUtil.layerSaveMaskOn();
    $.ajax({
        url: "/shop/history/order/saveOrder.do", 
        type: "POST",
        dataType: 'json',
        data: {
            'data': JSON.stringify(HistoryOfferOrder)
        },
        error: function() //失败
        {
            shuoheUtil.layerSaveMaskOff();
            shuoheUtil.layerTopMsgError('通讯失败');

        },
        success: function(data) //成功
        {
            shuoheUtil.layerSaveMaskOff();
            if (data.result == true) {
                parent.closeLayer();       
                shuoheUtil.layerTopMsgOK('保存成功');
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                parent.layer.close(index); //再执行关闭
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
  }
</script>