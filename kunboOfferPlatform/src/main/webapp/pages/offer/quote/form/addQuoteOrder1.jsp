<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建工时</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >  
        <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px" >
        	<tr>
                <td colspan="1" class="label">报价单号</td>
	            <td colspan="4" >
                    <input id='text' class="easyui-textbox" 
                        data-options="required:true,width:450,editable:true" 
                        prompt='报价单别称'>  
	            </td>
            </tr>	
            <tr>
            	<td class="label">报价人</td>
	            <td >
                    <input id='quotePeople' class="easyui-textbox" 
                        data-options="required:true,width:150,editable:true" 
                        prompt='报价人'>     
                </td>
                <td class="label">报价人BPM账号</td>
                <td >
                    <input id='quotePeopleAccount' class="easyui-textbox" 
                        data-options="required:true,width:150,editable:true" 
                        prompt='报价人BPM账号'>     
                </td>
            </tr>	            
	        <tr>
                <td colspan="1" class="label">客户</td>
	            <td colspan="4" >
                    <input id='customer' class="easyui-combobox" 
                        data-options="required:true,width:450,editable:true"
                        prompt='客户'>
	            </td>
            </tr>	
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='partsName' class="easyui-numberbox" data-options="required:false,width:150">      -->
                </td>
            </tr>	            
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:150,editable:true" > -->
                </td>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">
var obj = {}
    var HistoryOfferOrder = new Object();
    HistoryOfferOrder.id = '';
    HistoryOfferOrder.text = '';
    HistoryOfferOrder.quotePeopleId = '';
    HistoryOfferOrder.quotePeopleName = '';
    HistoryOfferOrder.quoteDate = '';
    HistoryOfferOrder.quoteLastModifyDate = '';
    HistoryOfferOrder.customerName = '';
    HistoryOfferOrder.customerId = '';
    HistoryOfferOrder.totalPrice = '';
    HistoryOfferOrder.quotePeopleAccount = '';


    var user = new Object();
    var Encode = new Object();

 	$(function() {

        var re = Easyui.Ajax.get('/encode/no/getNo.do?templateName=001');
        Encode = re.obj;
        $('#text').textbox('setValue',Encode.no);

        user = Easyui.User.getUser();
        console.info('user = '+JSON.stringify(user));
        if(parent.transData && typeof(parent.transData)=="function"){
            obj = parent.transData()
            console.log(obj)
        }
        //$('#quotePeople').textbox('setValue',user.actual_name);
        /* $.ajax({
            url: 'http://27.221.114.67:9003/develop/url/getUrl.do?name=getCusUpdate',
            type: "POST",
            dataType:'json',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data:
            {
            },
            error: function() {},
            success: function(data)//成功
            {
                console.log(data)
            }
        }) */
        $('#customer').combobox({
            url:'/transmit/getCustomer.do',
            valueField:'text',    
            textField:'customer_name',
            onSelect: function(rec){    
                console.info(rec);

            }
        });
        
	}); 

 	// $(function() {
	//   $('#pc_pid').textbox('setValue',GetQueryString('uuid'));
	// }); 
	
  function save() {

    if(shuoheUtil.checkRequired() == false) return;
    HistoryOfferOrder.text = $('#text').textbox('getText');
    //HistoryOfferOrder.quotePeopleId = user.id;
    //HistoryOfferOrder.quotePeopleName = user.actual_name;
    HistoryOfferOrder.quotePeopleId = '';
    HistoryOfferOrder.quotePeopleName = $('#quotePeople').textbox('getText');
    HistoryOfferOrder.customerName = $('#customer').combobox('getText');
    HistoryOfferOrder.customerId = $('#customer').combobox('getValue');;
    HistoryOfferOrder.totalPrice = 0;
    HistoryOfferOrder.quotePeopleAccount = $('#quotePeopleAccount').textbox('getText');
    
    shuoheUtil.layerSaveMaskOn();
    $.ajax({
        url: "/shop/history/order/saveOrder.do", 
        type: "POST",
        dataType: 'json',
        data: {
            'data': JSON.stringify(HistoryOfferOrder)
        },
        error: function() //失败
        {
            shuoheUtil.layerSaveMaskOff();
            shuoheUtil.layerTopMsgError('通讯失败');

        },
        success: function(data) //成功
        {
            shuoheUtil.layerSaveMaskOff();
            if (data.result == true) {
                if(parent.transData && typeof(parent.transData)=="function"){
                    $.ajax({
                        url: "/shop/history/saveHistoryPorductOfferOrder.do",
                        type: "POST",
                        dataType:'json',
                        async: false,
                        data:
                        {
                            productInfoList: JSON.stringify(obj.fanArr),
                            orderId: data.object.id,
                            motor: JSON.stringify(obj.motor),
                            params: JSON.stringify(obj.paraList),
                            productLineId: obj.fanType,
                            productJson: JSON.stringify(obj.fan),
                            productId: obj.fanCode,
                            materialId: obj.material,
                            totalPrice: obj.countAll,
                            unitPrice: obj.count,
                            num: obj.fanNum,
                            fanProfitRate: obj.fanProfit,
                            motorProfitRate: obj.motorProfit,
                            offerUnitPrice: obj.countProfit,
                            offerTotalPrice: obj.countProfitAll
                        },
                        error: function() {},
                        success: function(data1)//成功
                        {
                            console.log(data1)
                            parent.closeLayer();       
                            shuoheUtil.layerTopMsgOK('保存成功');
                            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                            parent.layer.close(index); //再执行关闭
                        }
                    })
                } else {
                    parent.closeLayer();       
                    shuoheUtil.layerTopMsgOK('保存成功');
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                }
                
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
  }
</script>