<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改原材料</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 300px;border-width:0px;overflow:hidden" >  
         <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px" >
	        <tr>
	            <td class="label">材料名称</td>
	            <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150" disabled>     
                </td>
                <td class="label">材料用量</td>
	            <td >
	                <input id='usage_amount' class="easyui-textbox" data-options="required:false,width:150">
	            </td>
            </tr>		            
            <tr>
                <td colspan="1"  class="label">公式</td>
                <td colspan="3" >
                    <input id='formula' class="easyui-textbox" data-options="multiline:true,required:false,width:450,height:100" disabled>
                </td>
            </tr>	    
	        <tr>
	            <td class="label">默认材料</td>
	            <td >
                    <input id='defaultMateName' class="easyui-combobox" data-options="required:false,width:150">     
                </td>
                <td class="label">默认材料单价</td>
	            <td >
	                <input id='defaultMatePrice' class="easyui-textbox" data-options="required:false,width:150" disabled >
	            </td>
            </tr>	  
	        <tr>
	            <td class="label">默认价格</td>
	            <td >
                    <input id='defaultPrice' class="easyui-textbox" data-options="required:false,width:150" disabled>     
                </td>
                <td class="label"></td>
	            <td >
	                <!-- <input id='defaultMatePrice' class="easyui-textbox" data-options="required:false,width:150" disabled > -->
	            </td>
            </tr>	                                             
	        <tr>

                <td colspan="1" class="label"></td>
                <td colspan="3" style="text-align:right">
                    <!-- <a onclick='calculator()' class="button button-primary button-rounded button-small"                        
                    style="text-align: center"><i class="fa fa-calculator"></i></a>                     -->
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">

    var id = parent.id;
    console.info('id = '+parent.id);
    // $('#usage_amount').attr("disabled",'disabled');
    // $("#usage_amount").removeAttr("disabled");
 	$(function() {
        initViewCompanent();
        var r = Easyui.Ajax.get('/shop/product/material/findByProductInfoId.do?productInfoId='+id);
        TemplateMaterial = r.obj;
        console.info('TemplateParameter = '+JSON.stringify(TemplateMaterial));
        if(r.result == false)
        {
            
        }
        else
        {
 
            $('#text').textbox('setValue',TemplateMaterial.text);
            $('#formula').textbox('setValue',TemplateMaterial.formula);
            $('#defaultMateName').combobox('setValue',TemplateMaterial.defaultMateId);

            if(checkNull(TemplateMaterial.formula))
            {
                $('#usage_amount').textbox("enable");               
                console.info('formula 为空');

            }
            else 
            {
                console.info('formula 不为空');                  
                $("#usage_amount").textbox("disable");
            }
            $('#usage_amount').textbox('setValue',TemplateMaterial.usage_amount);                
            
            // loadFormula();
        }
   
    }); 
    
    function initViewCompanent()
    {
        $('#defaultMateName').combobox({
            valueField: 'id',    
            textField: 'name',    
            url:'/develop/url/getUrl.do?name=getMaterialComboList',
            async: false,
            onSelect: function(rec){    
                console.info(rec);
                $('#defaultMatePrice').textbox('setValue',rec.price);
                TemplateMaterial.defaultMateId = rec.id;
                TemplateMaterial.defaultMateName = rec.name;
                TemplateMaterial.defaultMatePrice = rec.price;
                var price = TemplateMaterial.defaultMatePrice*TemplateMaterial.usage_amount;
                TemplateMaterial.defaultPrice = price;
                $('#defaultPrice').textbox('setValue',price);
            }
        });
        $('#usage_amount').textbox({
            onChange:function(newValue, oldValue)
            {
                TemplateMaterial.usage_amount = newValue;
                var price = TemplateMaterial.defaultMatePrice*TemplateMaterial.usage_amount;
                TemplateMaterial.defaultPrice = price;
                $('#defaultPrice').textbox('setValue',price);
            }
        });
        
    }

    function save()
    {
        $.ajax({
            url: "/shop/product/material/save.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(TemplateMaterial)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMsgError('通讯失败');
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    parent.closeLayer();       
                    shuoheUtil.layerTopMsgOK('保存成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>