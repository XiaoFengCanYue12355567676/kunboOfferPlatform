<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改参数</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 300px;border-width:0px;overflow:hidden" >  
	    <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px" >
	        <tr>
	            <td class="label">名称</td>
	            <td >
	                <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true" disabled>
	            </td>
	            <td class="label">单位</td>
	            <td >
	                <input id='unit' class="easyui-textbox" data-options="required:false,width:150,editable:true" disabled>       
	            </td>
            </tr>	
	        <tr>
	            <td class="label">筛选条件</td>
	            <td >
                    <input id='isfilter' class="easyui-combobox" data-options="required:false,width:150,editable:false,
                        valueField: 'value',
                        textField: 'label',
                        data: [{
                            label: '是',
                            value: true
                        },{
                            label: '否',
                            value: false
                        }]"  disabled/>
	            </td>
	            <td class="label">参数值</td>
	            <td >
	                <input id='value' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >       
	            </td>
            </tr>	            
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:150,editable:true" > -->
                </td>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">

    var id = parent.id;
    console.info('id = '+parent.id);

 	$(function() {
        var r = Easyui.Ajax.get('/shop/product/parameter/findByProductInfoId.do?productInfoId='+id);
        TemplateParameter = r.obj;
        console.info('TemplateParameter = '+JSON.stringify(TemplateParameter));
        if(r.result == false)
        {
            
        }
        else
        {
            $('#text').textbox('setValue',TemplateParameter.text);
            $('#unit').textbox('setValue',TemplateParameter.unit);
            $('#isfilter').combobox('setValue',TemplateParameter.isfilter);
            $('#value').textbox('setValue',TemplateParameter.value);
        }    
	}); 
	
    function save() {
        
        TemplateParameter.text = $('#text').textbox('getValue');
        TemplateParameter.unit = $('#unit').textbox('getValue');
        TemplateParameter.isfilter = $('#isfilter').combobox('getValue');
        TemplateParameter.value =  $('#value').textbox('getValue');

        $.ajax({
            url: "/shop/product/parameter/save.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(TemplateParameter)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMsgError('通讯失败');
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    parent.closeLayer();       
                    shuoheUtil.layerTopMsgOK('保存成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>