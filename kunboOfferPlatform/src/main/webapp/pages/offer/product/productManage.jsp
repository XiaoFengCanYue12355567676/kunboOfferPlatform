<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>产品管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:30%;">           
        <!-- treegrid表格 -->
        <table id ='sort_list' class="easyui-treegrid" style="width:100%;height:100%" 
               data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tt'
			   ">
            <thead>
            <tr>
                <th data-options="field:'id',title:'UUID',hidden:true"></th>
                <th data-options="field:'text',title:'风机列表'"></th>
                <th data-options="field:'productNumber',title:'风机编码'"></th>
                <th data-options="field:'sort_type',title:'类型'" formatter='formatterSortType'></th>
            </tr>
            </thead>
        </table>
        <div id="tt" style="height:35px;text-align: center">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMMAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funMMEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funMMDele()">删除</a>
             <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> 
          </div>        
    </div>

    <div data-options="region:'east',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%;" >
        <table id="dg" class='easyui-treegrid' style="width:100%;height:100%" title="" data-options="
                idField:'templateInfoId',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            >
            <thead>
              <tr>
                <!-- <th field="check"  checkbox="true">uuid</th>                 -->
                <th field="id"  hidden ="true">uuid</th>                
                <th field="text">名称</th>                
                <th field="attriType" formatter='formatterAttriType'>类型</th>
                <th field="optionType" formatter='formatterOptionType'>装配模式</th>
                <th field="unit" formatter='formatterUnit'>单位</th>
                <!-- <th field="is_filter" formatter='formatterFilter'>筛选条件</th> -->
                <th field="value" formatter='formatterValue'>参数</th>
                <th field="usage_amount" formatter='formatterUsage_amount'>材料用量</th>
                <th field="unitPrice" formatter='formatterUnitPrice'>工时单价</th>
                <th field="number" formatter='formatterNumber'>工时数量</th>
                <th field="price" formatter='formatterPrice'>价格</th>
                <th field="filter" formatter='formatterFilter'>是否筛选</th>
                <th field="quote" formatter='formatterQuote'>是否报价</th>
                <!-- <th field="encoded">编号</th>
                <th field="specificationsAndModels">规格</th>
                <th field="dosage">用量</th>
                <th field="price">价格</th> -->
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <!-- <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a> -->
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" 
                style="margin-left: 35px"
                data-options="iconCls:'fa fa-pencil',plain:true" 
                onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-jpy',plain:true" 
                onclick="quote()">报价</a>
            <a id='btnDgPriceAdjust' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-percent',plain:true" 
                onclick="priceAdjust()">外购件调价</a>    
           <a id='btnDgCopyProduct' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-copy',plain:true" 
                onclick="CopyProduct()">复制</a>                                
            <!-- <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> -->
          </div>
          <!-- <div id="mm" class="easyui-menu" style="width:80px;">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="append()" data-options="iconCls:'fa fa-plus',plain:true">添加</a>
		 </div> -->
       </div>
    </div>

  <script type="text/javascript">
    var typeId;


 	//表格初始化 
    $(function(){
        $('#sort_list').treegrid({
            url:"/shop/sort/getAllForTree.do",
            onLoadSuccess: function(row, data){
                // 折叠第二级
                for (var i = 0; i < data.length; i++) {
                    if (data[i].children.length > 0) {
                        for (var j = 0; j < data[i].children.length; j++) {
                            $('#sort_list').treegrid('collapse', data[i].children[j].id)
                        }
                    }
                }
            },
            onClickRow:function(row)
            {
                $('#dg').treegrid('clearSelections');
                $('#dg').treegrid('clearChecked');
                console.info(row);
                $.ajax({
				url: "/shop/product/getProductInfoForTreeByProductId.do",
				type: "POST",
				dataType:'json',
				//async: false,
				data:
				{
					productId: row.id
				},
				error: function() {},
				success: function(data)//成功
				{
					console.log(data)
				}
			})
                $('#dg').treegrid({
                    url:'/shop/product/getProductInfoForTreeByProductId.do',
                    queryParams: {
                        productId: row.id
                    }
                });
            }
        });

        // $('#dg').treegrid('loadData',dgdata);
    }); 
     
    function priceAdjust()
    {
        Easyui.Layer.openLayerWindow(
            '外购件调价',
            'assortPort/priceAdjust.jsp',
            ['600px','250px']);
    }

    function formatterSortType(value,row,index)
    {
        if(row.sort_type == 'GENERA')
            return "大类";
        if(row.sort_type == 'PRODUCT_LINE')
            return "产品线";
        if(row.sort_type == 'PRODUCT')
            return "产品";
    }

    function formatterAttriType(value,row,index)
    {
        if(row.attriType == 'HALF_PRODUCT')
            return "半成品";
        if(row.attriType == 'PARAMETER')
            return "参数";
        if(row.attriType == 'STANDARD_PORT')
            return "外购件（指定产品）";
        if(row.attriType == 'MANHOUR')
            return "工时";
        if(row.attriType == 'MATERIAL')
            return "材料";
        if(row.attriType == 'ASSORT_PORT')
            return "外购件（指定价格）";
    }
    function formatterOptionType(value,row,index)
    {
        if(row.optionType == 'SINGLE_SELECT')
            return "单选";
        if(row.optionType == 'ALL_SELECT')
            return "全选";
        if(row.optionType == 'MULTI_SELECT')
            return "多选";
    }

    function quote()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        /* if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个产品');
            return;
        }
        if(row.sort_type == 'GENERA'||row.sort_type == 'PRODUCT_LINE') 
        {
            shuoheUtil.layerMsgError('只能给风机报价，大类及产品线不能报价');
            return;
        }  */       

        Easyui.Layer.openLayerWindow(
            '产品报价',
            '../quote/offer.jsp?from=product',
            ['1050px',document.body.clientHeight-40+"px"]);
    }

    function funMMAdd() {

        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一条产品线');
        }
        else
        {
            if(row.sort_type == 'GENERA'||row.sort_type == 'PRODUCT')
            {
                shuoheUtil.layerMsgError('必须在产品线上创建风机');
                return;
            }
            Easyui.Layer.openLayerWindow(
            '新增产品',
            'product/addProduct.jsp?id='+row.id,
            ['650px','400px']);
        }
    }  
    
    function funMMEdit()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('编辑前请先选择一条数据');
        }
        else
        {
            if(row.sort_type != 'PRODUCT') 
            {
                shuoheUtil.layerMsgError('只能修改产品');
                return;
            }
            else
            {
                Easyui.Layer.openLayerWindow(
                '编辑产品',
                'product/editProduct.jsp?id='+row.id,
                ['650px','400px']);
            }
        }

    }

    function funMMDele()
    {
        
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('删除前请先选择一条数据');
        }
        else
        {
            if(row.sort_type != 'PRODUCT') 
            {
                shuoheUtil.layerMsgError('只能删除产品');
                return;
            }
            else
            {
                $.messager.defaults = { ok: '确定',cancel: '取消'};
                $.messager.confirm('确认删除', "<font size='3px'>您确认将删除这个产品么?</font>", 
                    function(r){
                        console.info('r = '+r);
                        if (r){
                        console.info('doFunction');
                        deleteProductById(row);
                    }
                });
            }
        }
    }
    function deleteProductById(row)
    {
        var r = Easyui.Ajax.get('/shop/sort/delete.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#sort_list').treegrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#sort_list').treegrid('reload');
        }
    }

    function funMgAdd()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个风机风机产品线');
            return;
        }
        if(row.sort_type != 'PRODUCT_LINE')
        {
            shuoheUtil.layerMsgError('您选择的不是产品线');
            return;
        }
        var pid_row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(pid_row))
        {
            pid_row = new Object();
            pid_row.id = '';
        }
        Easyui.Layer.openLayerWindow(
            '新建模型部件',
            'addFormHeader.jsp?id='+row.id+'&pid='+pid_row.id,
            ['700px','500px']);  
    }
    function funDgEdit()
    {
        var row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个模型部件');
            return;
        }
        console.info(row);
        Easyui.Layer.openLayerWindow(
            '修改模型部件',
            'editFormHeader.jsp?id='+row.id+'&attriType='+row.attriType,
            ['700px','500px']);  
    }
    function funDgDele()
    {
        var row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个模型部件');
            return;
        }
        var r = Easyui.Ajax.get('/shop/template/info/deleteById.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#dg').treegrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#dg').treegrid('reload');
        }
    }
    function formatterUnit(value,row,index)
    {
        if(row.attriType != 'PARAMETER')
            return row.unit = '-';
        else
            return row.unit;
    }
    function formatterFilter(value,row,index)
    {
        if(row.filter == true)
            return row.is_filter = '是';
        else
            return row.is_filter = '-';
    }
    function formatterQuote(value,row,index)
    {
        if(row.quote == true)
            return row.quote = '是';
        else
            return row.quote = '-';
    }

    function formatterValue(value,row,index)
    {
        if(row.attriType != 'PARAMETER')
            return row.value = '-';
        else
            return row.value;
    }
    function formatterUsage_amount(value,row,index)
    {
        if(row.attriType != 'MATERIAL')
            return row.usage_amount = '-';
        else
            return shuoheUtil.decimal(row.usage_amount,2);

        // if(row.attriType == 'HALF_PRODUCT')
        //     return "半成品";
        // if(row.attriType == 'PARAMETER')
        //     return "参数";
        // if(row.attriType == 'STANDARD_PORT')
        //     return "外购件（指定产品）";
        // if(row.attriType == 'MANHOUR')
        //     return "工时";
        // if(row.attriType == 'MATERIAL')
        //     return "材料";
        // if(row.attriType == 'ASSORT_PORT')
        //     return "外购件（指定价格）";
    }
    function formatterUnitPrice(value,row,index)
    {
        if(row.attriType != 'MANHOUR')
            return row.unitPrice = '-';
        else
            return row.unitPrice;
        // if(row.attriType == 'HALF_PRODUCT')
        //     return "半成品";
        // if(row.attriType == 'PARAMETER')
        //     return "参数";
        // if(row.attriType == 'STANDARD_PORT')
        //     return "外购件（指定产品）";
        // if(row.attriType == 'MANHOUR')
        //     return "工时";
        // if(row.attriType == 'MATERIAL')
        //     return "材料";
        // if(row.attriType == 'ASSORT_PORT')
        //     return "外购件（指定价格）";
    }
    function formatterNumber(value,row,index)
    {
        if(row.attriType != 'MANHOUR')
            return row.number = '-';
        else
            return row.number;        
        // if(row.attriType == 'HALF_PRODUCT')
        //     return "半成品";
        // if(row.attriType == 'PARAMETER')
        //     return "参数";
        // if(row.attriType == 'STANDARD_PORT')
        //     return "外购件（指定产品）";
        // if(row.attriType == 'MANHOUR')
        //     return "工时";
        // if(row.attriType == 'MATERIAL')
        //     return "材料";
        // if(row.attriType == 'ASSORT_PORT')
        //     return "外购件（指定价格）";
    }
    function formatterPrice(value,row,index)
    {
        if(row.attriType != 'PARAMETER')
            return shuoheUtil.decimal(row.price,2);     
        else
            return row.price = '-';           
    }

 	// function loadFanModel(id){
 	// 	/*
 	// 	$.ajax({
    //         url: "/develop/url/getUrl.do",
    //         type: "POST",
    //         dataType: 'json',
    //         data: {
    //         	'name':'getFanModelByTypeId',
    //         	'fanTypeId': id 
    //         },
    //         error: function() //失败
    //         {
    //             shuoheUtil.layerTopMaskOff();
    //         },
    //         success: function(data) {
    //         	var arr = data.rows;
    //         	for(var i=0;i<arr.length;i++){
    //         		if(arr[i]._parentId==""){
    //         			delete arr[i]._parentId;
    //         		}
    //         	}
    //         	data.rows = arr;
    //         	$('#dg').treegrid('loadData', data);
    //         }
    //     });*/
 		
 	// 	$('#dg').treegrid({
    // 		url:'/develop/url/getUrl.do',
    // 		//data: data,
    // 		method:'get',
    // 		queryParams: {
    // 			'name':'getFanModelByTypeId',
    // 			'fanTypeId': id      
    //           }
    // 	})
 	// }
 	// function onContextMenu(e,row){
	// 	e.preventDefault();
	// 	$(this).treegrid('select', row.id);
	// 	$('#mm').menu('show',{
	// 		left: e.pageX,
	// 		top: e.pageY
	// 	});
	// }
 	
 	// function append(){
 	// 	var rootNode = $('#dg').treegrid('getRoot');
 	// 	var node = $('#dg').treegrid('getSelected');
 	// 	var pid = node.id;
 	// 	Easyui.Layer.openLayerWindow(
 	// 		      '新建产品模型',
 	// 		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId+'&pid='+pid+'&fanModelId='+rootNode.id,
 	// 		      ['650px','400px']);
 	// }
 	// function removeIt(){
 	// 	alert("移除");
 	// }
	</script>
</body> 
</html>
<script type="text/javascript">


/*大类*/
// function funTgAdd() {
	
// 	var nodes = $('#tt').tree('getSelected');
      
//       var pid = '0';
//     if(nodes != null){
//     	Easyui.Layer.openLayerWindow(   
//     		      '新建产品大类',
//     		      '/pages/shop/product/type/addProductType.jsp?id='+$('#tt').tree('getSelected').id,
//     		      ['655px','400px']);
//     }else{
//     	Easyui.Layer.openLayerWindow(   
//   		      '新建产品大类',
//   		      '/pages/shop/product/type/addProductType.jsp?',
//   		      ['655px','400px']);
//     } 
// }
// function funTgEdit() {

//     if($('#tt').tree('getSelected') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     console.log($('#tt').tree('getSelected').id)
//   Easyui.Layer.openLayerWindow(
//       '编辑产品大类',
//       '/pages/shop/product/type/editProductType.jsp?id='+$('#tt').tree('getSelected').id,
//        ['400px','280px']);
// }
// function funTgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/productCategory/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
        
//         shuoheUtil.layerMsgSaveOK('删除成功'),
        
//     );
//     $('#tt').tree('reload');
// }


/*模型*/
// function funMgAdd(){
// 	var nodess = $('#tt').tree('getSelected');
//     if(nodess == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     var rootNode = $('#dg').treegrid('getRoot');
//     if(rootNode){
//     	shuoheUtil.layerMsgCustom('风机模型已经存在，请添加配件');
//     }else{
//     	Easyui.Layer.openLayerWindow(
//     		      '新建产品模型',
//     		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId,
//     		      ['650px','400px']);
//     }
    
// }
// function funMgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
// 	 console.info($('#mg').datagrid('getSelected').id);
// 	Easyui.Layer.openLayerWindow(
//        '修改产品模型',
//        '/pages/shop/product/model/editProductModel.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//        ['400px','280px']);
// }
// function funMgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/extensionModel/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#mg').datagrid('reload');
// } 




/*配件*/
// function funDgAdd(){

//     if(Easyui.Datagraid.getSelectRow('dg') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
//     Easyui.Layer.openLayerWindow(
//       '新建模型配件',
//       '/pages/shop/product/parts/addModelParts.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//       Easyui.Layer.sizeTwoRow(600));
// }
// function funDgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个模型配件');
//     }
// 	 console.info("555");
// 	Easyui.Layer.openLayerWindow(
//        '修改模型配件',
//        '/pages/shop/product/parts/editModelParts.jsp?uuid='+$('#dg').datagrid('getSelected').id,
//        Easyui.Layer.sizeTwoRow(600));
// }
// function funDgDele() {
//   // body...
//     var row = Easyui.Datagraid.getSelectRow('dg');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个材料');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/fanModelParts/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#dg').datagrid('reload');
// }
 function fanModelOffer() {
// 	var node = $('#dg').treegrid('getSelected');
// 	console.log(node);
// 	if(node){
// 		$.ajax({
//           url: "/productMix/fanModelOffer",
//           type: "POST",
//           dataType: 'json',
//           data: {
//           	'modelNumber': node.id
// 
//           },
//           error: function() //失败
//           {
//               shuoheUtil.layerTopMaskOff();
//           },
//           success: function(data) {
//           	$('#dg').treegrid('reload');
//           }
//       });
// 	}else{
// 		shuoheUtil.layerMsgCustom('请选择要报价的模型');
// 	}
	var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('编辑前请先选择一条数据');
        }
        else
        {
            if(row.sort_type != 'PRODUCT') 
            {
                shuoheUtil.layerMsgError('只能修改产品');
                return;
            }
            else
            {
                Easyui.Layer.openLayerWindow(
                '模型报价',
                '../quote/quoteOrder_1.jsp?id='+row.id,
                ['1200px','700px']);
            }
        }
 }


 function CopyProduct()
 {
    var row = Easyui.Treegraid.getSelectRow('sort_list');
     if(Easyui.Object.isEmptyObject(row))
    {
        shuoheUtil.layerMsgError('复制前请先选择一个风机');
        return;
    }
    if(row.sort_type != 'PRODUCT')
    {
        shuoheUtil.layerMsgError('只能复制产品');
        return;
    }
    Easyui.Layer.openLayerWindow(
        '复制产品',
        'copyProduct/copyProduct.jsp?id='+row.id+"&pid="+row.pid,
        ['650px','400px']);

 }

  
</script>