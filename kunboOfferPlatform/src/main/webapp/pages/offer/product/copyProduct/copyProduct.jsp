<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建产品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>

    <script type="text/javascript" src="/pages/js/offerConfig.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true"  " style="width:100%;border-width:0px" >  
        <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:0px;" >
            <tr>
                <td class="label">产品线</td>
                <td >
                    <input id='pid_text' class="easyui-textbox" data-options="required:false,width:150,editable:true" disabled="true"  >
                </td>
                <td class="label"></td>
                <td >
                    <!-- <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>         
            <tr>
                <td class="label">产品名称</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:true,width:150,editable:true" 
                        prompt='产品名称' >       
                </td>                
                <td class="label">产品编码</td>
                <td >
                    <input id='productNumber' class="easyui-textbox" data-options="required:true,width:150,editable:true" 
                        prompt='产品编码'>     
                </td>
            </tr>         
        </table>        
    </div>
    <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>复制</a>
        </div>        
    </div>
</body> 
</html>
<script type="text/javascript">
 
    var pid_sort_id = shuoheUtil.getUrlHeader().pid;
    var copy_from_id = shuoheUtil.getUrlHeader().id;
    var ProductSort = new Object();
    ProductSort.id = '';
    ProductSort.pid = '';
    ProductSort.text = '';
    ProductSort.sort_type = 'PRODUCT';
    ProductSort.productNumber = '';

    $(function() {
        console.info('pid_sort_id = '+pid_sort_id);
        if(checkNull(pid_sort_id)) pid_sort_id = '';
        var pid_sort = Easyui.Ajax.get('/shop/sort/findById.do?id='+pid_sort_id);
        if(!Easyui.Object.isEmptyObject(pid_sort.obj))
        {
            console.info('111');
            $('#pid_text').textbox('setValue',pid_sort.obj.text);
        }
        else
        {
            console.info('222');
        }


    }); 
    
    function save() {

        if(shuoheUtil.checkRequired() == false) return;

        ProductSort.pid = pid_sort_id;
        ProductSort.text = Easyui.TextBox.getValue('text');
        ProductSort.productNumber = Easyui.TextBox.getValue('productNumber');
        shuoheUtil.layerSaveMaskOn();
        $.ajax({
            url: "/shop/product/copyProduct.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'productId': copy_from_id,
                'productNumber': Easyui.TextBox.getValue('productNumber'),
                'text': Easyui.TextBox.getValue('text')
            },
            error: function() //失败
            {
                shuoheUtil.layerSaveMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data) //成功
            {
                shuoheUtil.layerSaveMaskOff();
                if (data.result == true) {
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                    parent.$('#sort_list').treegrid('reload');
                    shuoheUtil.layerMsgSaveOK('保存成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
  }



</script>