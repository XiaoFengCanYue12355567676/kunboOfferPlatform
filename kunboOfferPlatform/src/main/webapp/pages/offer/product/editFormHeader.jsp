<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建半成品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">

        <div data-options="region:'north',title:'',split:false,border:true" 
            style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >
            <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px;" >
                <tr>
                    <td class="label">选择内容类型</td>
                    <td >
                        <select id='pc_pid' class="easyui-combobox" data-options="required:false,width:150,editable:false" disabled>
                            <option value="PARAMETER">参数</option>  
                            <option value="HALF_PRODUCT">半成品</option>         
                            <option value="MATERIAL">原材料</option>       
                            <option value="STANDARD_PORT">外购件（指定产品）</option>  
                            <option value="MANHOUR">工时</option>
                            <option value="ASSORT_PORT">外购件（指定价格）</option>   
                        </select>
                    </td>
                </tr>	      
            </table>
        </div>   
  
        <div data-options="region:'center',title:'',border:true,split:false,overflow:false" 
            style="padding:0px;height: 400px;width: 100%;border-width:0px">
            <iframe id='son_page' style="width: 100%;height:400px;overflow:hidden;border-width:0px"></iframe>
        </div>   
        <!-- <div data-options="region:'south',title:'',split:false,border:true" 
            style="height:100px;width: 100%;height: 45px;border-width:0px">
            <div style="text-align: right;margin-top: 5px;margin-right: 10px">
                <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
            </div>                              
        </div>          -->
</body> 
</html>
<script type="text/javascript">

    var id = shuoheUtil.getUrlHeader().id;
    var attriType = shuoheUtil.getUrlHeader().attriType;

    $(function(){
        $('#pc_pid').combobox('setValue',attriType);

        console.info('attriType = '+attriType);

        if(attriType == 'PARAMETER')
            $('#son_page').attr('src',"params/editParam.jsp");
        else if(attriType == 'HALF_PRODUCT')
            $('#son_page').attr('src',"halfProduct/editHalfProduct.jsp");
        else if(attriType == 'STANDARD_PORT')
            $('#son_page').attr('src',"standard/editStandard.jsp");
        else if(attriType == 'MANHOUR')
            $('#son_page').attr('src',"manhour/editManhour.jsp");
        else if(attriType == 'ASSORT_PORT')
            $('#son_page').attr('src',"assortPort/editAssortPort.jsp");
        else if(attriType == 'MATERIAL')
            $('#son_page').attr('src',"materials/editMaterials.jsp");                  

    });

     function closeLayer()
     {
        parent.$('#dg').treegrid('reload');
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
        parent.layer.close(index); //再执行关闭
     }
</script>