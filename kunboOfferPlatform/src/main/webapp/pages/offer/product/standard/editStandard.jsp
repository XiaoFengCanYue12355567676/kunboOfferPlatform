<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改原材料</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >  
        <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px" >            
	        <tr>
                <td class="label">类型别名</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:true,width:150"
                        prompt='类型' disabled>      
                </td>
                <td class="label"></td>
                <td >
                 </td>
            </tr>	                        
	        <tr>
	            <td class="label">类型</td>
	            <td >
                    <input id='standardName' class="easyui-combobox" data-options="required:false,width:150" disabled>     
                </td>
                <td class="label">配件价格</td>
	            <td >
	                <input id='unitPrice' class="easyui-textbox" data-options="required:false,width:150" disabled>
	            </td>
            </tr>	
	        <tr>
	            <td colspan="1" class="label">具体配件</td>
	            <td colspan="3">
                    <input id='standardId' class="easyui-combobox" data-options="required:false,width:450,editable:false" >     
                </td>

            </tr>	            
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:150,editable:true" > -->
                </td>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">
 
    var id = parent.id;
    console.info('id = '+parent.id);

    var TemplateStandard = new Object();
    // TemplateStandard.id = '';
    // TemplateStandard.text = '';
    // TemplateStandard.sortId = sort_id;
    // TemplateStandard.templateInfoId = '';
    // TemplateStandard.standardId = '';
    // TemplateStandard.price = '';

 	$(function() {

        var re = Easyui.Ajax.get('/shop/product/standard/findByProductInfoId.do?productInfoId='+id);
        TemplateStandard = re.obj;
        console.info('TemplateStandard = '+JSON.stringify(TemplateStandard));
        if(re.result == false)
        {
            
        }
        else
        {
            $('#text').textbox('setValue',TemplateStandard.text);
            $('#standardName').combobox({
                url:'/shop/standard/standardType/list.do',
                valueField:'id',    
                textField:'text',
                onLoadSuccess:function(){
                    $('#standardName').combobox('setValue',TemplateStandard.standardSortId);
                    // var ret = $('#text').combobox('getValue');
                    // console.info(ret);
                    $('#standardId').combobox({
                        url:'/shop/standard/standardComponent/listByTypeId.do',
                        valueField:'id',    
                        textField:'stockName',
                        queryParams: {
                            pid: TemplateStandard.standardSortId
                        },
                        onSelect: function(rec){
                            
                            $('#unitPrice').textbox('setValue',rec.unitPrice);  
                            // TemplateStandard.text = rec.unitPrice;
                            TemplateStandard.standardId = rec.id;
                            TemplateStandard.unitPrice = rec.unitPrice;
                            // TemplateStandard.text = rec.type;
                            // TemplateStandard.standardId = rec.id;
                        },
                        onLoadSuccess:function(){
                            // console.info();
                            $('#standardId').combobox('setValue',TemplateStandard.standardId);
                            $('#unitPrice').textbox('setValue',TemplateStandard.unitPrice);  
                        }
                    });                    
                },
                onSelect: function(rec){    
                    console.info(rec);
                    // TemplateStandard.standardName = rec.text;
                 
                }
            });
        }
	}); 
	
    function save() {
        console.info('save');
        $.ajax({
            url: "/shop/product/standard/save.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(TemplateStandard)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMsgError('通讯失败');
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                        parent.closeLayer();       
                        shuoheUtil.layerTopMsgOK('保存成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>