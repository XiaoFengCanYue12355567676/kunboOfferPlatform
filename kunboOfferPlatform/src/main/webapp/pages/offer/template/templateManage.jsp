<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>产品模板管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:20%;">           
        <!-- treegrid表格 -->
        <table id ='sort_list' class="easyui-treegrid" style="width:100%;height:100%" 
               data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tt'
			   ">
            <thead>
            <tr>
                <th data-options="field:'id',title:'UUID',hidden:true"></th>
                <th data-options="field:'text',title:'风机类别'" ></th>
                <th data-options="field:'sort_type',title:'类型'" formatter='formatterSortType'></th>
                <th data-options="field:'responsible_name',title:'责任人'"></th>
            </tr>
            </thead>
        </table>
        <div id="tt" style="height:35px;text-align: center">
            <a id='btnSlAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMMAdd()">新增</a>
            <a id='btnSlEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funMMEdit()">修改</a>
            <a id='btnSlDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funMMDele()">删除</a>
            <!-- <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> -->
          </div>        
    </div>

    <div data-options="region:'east',iconCls:'icon-reload',title:'',split:true" style="width:80%;height: 100%;" >
        <table id="dg" class='easyui-treegrid' style="width:100%;height:100%" title="" data-options="
                idField:'id',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            >
            <thead>
              <tr>
                <th field="id"  hidden ="true">uuid</th>                
                <th field="text">名称</th>                
                <th field="attriType" formatter='formatterAttriType'>类型</th>
                <!-- <th field="optionType">装配模式</th> -->
                <!-- <th field="unit">单位</th> -->
                <th field="isFilter" formatter='formatterIsFilter'>是否筛选</th>
                <th field="calculateSequ" formatter='formatterCalculateSequ'>计算顺序</th>                
                <!-- <th field="isQuote" formatter='formatterIsQuote'>是否报价</th>                 -->
                <th field="formula">算式</th>
                <!-- <th field="specificationsAndModels">规格</th>
                <th field="dosage">用量</th>
                <th field="price">价格</th> -->
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funDgDele()">删除</a>
            <!-- <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> -->
          </div>
          <!-- <div id="mm" class="easyui-menu" style="width:80px;">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="append()" data-options="iconCls:'fa fa-plus',plain:true">添加</a>
		 </div> -->
       </div>
    </div>

  <script type="text/javascript">
    var typeId;
    var user = new Object();

 	//表格初始化 
    $(function(){
        getCurrentUser();
        $('#sort_list').treegrid({
            url:"/shop/sort/getSortForTree.do",
            onClickRow:function(row)
            {
                console.info(row);
                if(user == null)
                {
                    shuoheUtil.layerMsgError('登录失效，请重新登录');
                    return;
                } 

                userRole();
                $('#dg').treegrid('clearSelections');
                $('#dg').treegrid('clearChecked');
                $('#dg').treegrid({
                    url:'/shop/template/info/getInfoBySortIdForTree.do',
                    queryParams: {
                        sortId: row.id
                    },
                    onLoadSuccess:function(row, data)
                    {
                        // console.info('onLoadSuccess = '+JSON.stringify(data));
                        // console.info('onLoadSuccess = '+JSON.stringify(row));
                        traverse(data);
                        $('#dg').treegrid("autoSizeColumn");  

                    }
                });
            }
        });

        // $('#dg').treegrid('loadData',dgdata);
    }); 
     
    var traverse = function(list)
    {
        for(var i in list)
        {
            initFilterSwitchButton(list[i]);
            initQuoteSwitchButton(list[i]);
            initCalculateSequButton(list[i]);
            if(list[i].children!=null&&list[i].children!=undefined)
            {
                if(list[i].children.length != 0)
                {
                    traverse(list[i].children);
                }
            }
        }
    }
    function initCalculateSequButton(data)
    {
        // console.info('data = '+JSON.stringify(data));
        $('#calculateSequ_plus_'+data.id).linkbutton({});
        $('#calculateSequ_subtract_'+data.id).linkbutton({});
        $('#calculateSequ_text_'+data.id).textbox({});
        $('#calculateSequ_text_'+data.id).textbox('setValue',data.calculateSequ);
    }
    function initFilterSwitchButton(data)
    {
        var is_check;
        if(data.filter == '1')
        is_check = true;
        if(data.filter == '0')
        is_check = false;        
        $('#sw_filter_'+data.id).switchbutton({
            checked: is_check,      
            onText:'是',
            offText:'否',
            height:'23px',
            onChange: function(checked)
            {        
                console.log('checked = '+checked);
                console.log(this.id);
                Easyui.Ajax.get('/shop/template/info/updateFilter.do?id='+data.id+'&filter='+checked);
            }
        })
    }
    function initQuoteSwitchButton(data)
    {
        var is_check;
        if(data.quote == '1')
        is_check = true;
        if(data.quote == '0')
        is_check = false;        
        $('#sw_quote_'+data.id).switchbutton({
            checked: is_check,      
            onText:'是',
            offText:'否',
            height:'23px',
            onChange: function(checked)
            {        
                console.log('checked = '+checked);
                console.log(this.id);
                Easyui.Ajax.get('/shop/template/info/updateQuote.do?id='+data.id+'&quote='+checked);
            }
        })
    }


    function formatterSortType(value,row,index)
    {
        if(row.sort_type == 'GENERA')
            return "大类";
        if(row.sort_type == 'PRODUCT_LINE')
            return "产品线";
    }

    function formatterAttriType(value,row,index)
    {
        if(row.attriType == 'HALF_PRODUCT')
            return "半成品";
        if(row.attriType == 'PARAMETER')
            return "参数";
        if(row.attriType == 'STANDARD_PORT')
            return "外购件（指定产品）";
        if(row.attriType == 'MANHOUR')
            return "工时";
        if(row.attriType == 'MATERIAL')
            return "材料";
        if(row.attriType == 'ASSORT_PORT')
            return "外购件（指定价格）";
    }
    function formatterIsFilter(value,row,index)
    {
        if(row.attriType == 'PARAMETER')
            var htmlstr = '<input id="sw_filter_'+row.id+'" class="easyui-switchbutton" checked>';  
        else 
        var htmlstr = '-';
        return htmlstr;
    }
    function formatterIsQuote(value,row,index)
    {
        var htmlstr = '<input id="sw_quote_'+row.id+'" class="easyui-switchbutton" checked>';  
        return htmlstr;
    } 
    function formatterCalculateSequ(value,row,index)
    {
        if(row.attriType == 'HALF_PRODUCT'||row.attriType == 'MATERIAL')
        {
            var htmlstr =  '<a id="calculateSequ_plus_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="iconCls:\'fa fa-plus text-danger\',plain:true" \
                            onclick="funCalculateSequPlus('+'\''+row.id+'\',\''+row.calculateSequ+'\')"></a>';
            htmlstr+= '<input id="calculateSequ_text_'+row.id+'" type="text" style="width:30px;height:20px" disabled>';
            // htmlstr+= '||';
            htmlstr+=  '<a id="calculateSequ_subtract_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                                data-options="iconCls:\'fa fa-minus text-danger\',plain:true" \
                                onclick="funCalculateSequSubtract('+'\''+row.id+'\',\''+row.calculateSequ+'\')"></a>';     
        }
        else
        {
            var htmlstr = '-';
        }
        return htmlstr;
    }
    function funCalculateSequPlus(id,calculateSequ)
    {
        console.info('funCalculateSequPlus = '+id);
        calculateSequ ++;
        shuoheUtil.layerSaveMaskOn();
        $.ajax({
            url: "/shop/template/info/updateCalculateSequ.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'id': id,
                'calculateSequ':calculateSequ
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMsgError('通讯失败');
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerSaveMaskOff();
                if (data.result == true) {
                    shuoheUtil.layerTopMsgOK('保存成功');
                    $('#dg').treegrid('reload');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
    function funCalculateSequSubtract(id,calculateSequ)
    {
        console.info('funCalculateSequSubtract = '+id);
        if(calculateSequ <= 0) return;
        else
        {
            calculateSequ --;
            shuoheUtil.layerSaveMaskOn();
            $.ajax({
                url: "/shop/template/info/updateCalculateSequ.do", 
                type: "POST",
                dataType: 'json',
                data: {
                    'id': id,
                    'calculateSequ':calculateSequ
                },
                error: function() //失败
                {
                    shuoheUtil.layerTopMsgError('通讯失败');
                    shuoheUtil.layerSaveMaskOff();
                },
                success: function(data) //成功
                {
                    shuoheUtil.layerSaveMaskOff();
                    if (data.result == true) {
                        shuoheUtil.layerTopMsgOK('保存成功');
                        $('#dg').treegrid('reload');
                    } else {
                        shuoheUtil.layerTopMsgError(data.describe);
                    }
                }
            });
        }
    }


    function funMMAdd() {

        var row = Easyui.Treegraid.getSelectRow('sort_list');
        //var row = $('#sort_list').treegraid('');
        

        if(Easyui.Object.isEmptyObject(row))
        {
            Easyui.Layer.openLayerWindow(
            '新增产品类型',
            'type/addType.jsp',
            ['650px','400px']);
        }
        else
        {
            if(row.sort_type == 'PRODUCT_LINE') 
            {
                shuoheUtil.layerTopMsgError('产品线下不能创建产品线及大类');
                return;
            }
            Easyui.Layer.openLayerWindow(
            '新增产品类型',
            'type/addType.jsp?id='+row.id,
            ['650px','400px']);
        }
    }  
    
    function funMMEdit()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('编辑前请先选择一条数据');
        }
        Easyui.Layer.openLayerWindow(
            '编辑产品类型',
            'type/editType.jsp?id='+row.id,
            ['650px','400px']);
    }

    function funMMDele()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('删除前请先选择一条数据');
            return;
        }
        //检查模板下是否有部件数据，如果有数据则不允许删除2018-12-14
        var dg_row = Easyui.Treegraid.getData('dg');
        if(dg_row==undefined||dg_row==null||dg_row==""){
            console.info('11 dg_row = '+dg_row);
        }
        else
        {
            console.info('22 dg_row = '+dg_row);
            shuoheUtil.layerMsgError('必须删除部件才能删除模板');
            return;
        }


        $.messager.defaults = { ok: '确定',cancel: '取消'};
	    $.messager.confirm('确认删除', "<font size='3px'>"+'删除将导致您录入的模板消失，您确认么'+"</font>", function(r){
	    	console.info('r = '+r);
	        if (r){
	            console.info('doFunction');
	            deleteProductSortById(row);
	        }
	    });
    }
    function deleteProductSortById(row)
    {
        var r = Easyui.Ajax.get('/shop/sort/delete.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#sort_list').treegrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#sort_list').treegrid('reload');
        }
    }
    function funMgAdd()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个风机风机产品线');
            return;
        }
        if(row.sort_type != 'PRODUCT_LINE')
        {
            shuoheUtil.layerMsgError('您选择的不是产品线');
            return;
        }
        var pid_row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(pid_row))
        {
            pid_row = new Object();
            pid_row.id = '';
        }
        else
        {
            if(pid_row.attriType != 'HALF_PRODUCT')
            {
                shuoheUtil.layerMsgError('只能在半成品下创建配件');
                return;
            }     
            // if(pid_row.level >= 2)
            // {
            //     shuoheUtil.layerMsgError('半成品只能创建2级');
            //     return;
            // }       
        }
        Easyui.Layer.openLayerWindow(
            '新建模型部件',
            'addFormHeader.jsp?id='+row.id+'&pid='+pid_row.id+'&level='+pid_row.level,
            ['700px','500px']);  
    }
    function funDgEdit()
    {
        var row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个模型部件');
            return;
        }
        console.info(row);
        Easyui.Layer.openLayerWindow(
            '修改模型部件',
            'editFormHeader.jsp?id='+row.id+'&attriType='+row.attriType,
            ['700px','500px']);  
    }
    function funDgDele()
    {
        var row = Easyui.Treegraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('请先选择一个模型部件');
            return;
        }
        var r = Easyui.Ajax.get('/shop/template/info/deleteById.do?id='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#dg').treegrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#dg').treegrid('reload');
        }
    }

    function saveOk()
    {
        shuoheUtil.layerMsgSaveOK('保存成功');
    }

    //不同责任人的人不能修改别人的产品线
    function getCurrentUser()
    {
        user = Easyui.User.getUser();
        if(user == null) shuoheUtil.layerMsgError('登录失效，请重新登录');

        console.info('user = '+JSON.stringify(user));
    }
    function userRole()
    {
        var row = Easyui.Treegraid.getSelectRow('sort_list');
        console.info('user.id = '+user.id);
        console.info('row.responsible_id = '+row.responsible_id);

        if(user.id!=row.responsible_id)
        {
            $('#btnDgAdd').linkbutton('disable');
            $('#btnDgEdit').linkbutton('disable');
            $('#btnDgDelete').linkbutton('disable');
        }
        else
        {
            $('#btnDgAdd').linkbutton('enable');
            $('#btnDgEdit').linkbutton('enable');
            $('#btnDgDelete').linkbutton('enable');
        }
    }

 	// function loadFanModel(id){
 	// 	/*
 	// 	$.ajax({
    //         url: "/develop/url/getUrl.do",
    //         type: "POST",
    //         dataType: 'json',
    //         data: {
    //         	'name':'getFanModelByTypeId',
    //         	'fanTypeId': id 
    //         },
    //         error: function() //失败
    //         {
    //             shuoheUtil.layerTopMaskOff();
    //         },
    //         success: function(data) {
    //         	var arr = data.rows;
    //         	for(var i=0;i<arr.length;i++){
    //         		if(arr[i]._parentId==""){
    //         			delete arr[i]._parentId;
    //         		}
    //         	}
    //         	data.rows = arr;
    //         	$('#dg').treegrid('loadData', data);
    //         }
    //     });*/
 		
 	// 	$('#dg').treegrid({
    // 		url:'/develop/url/getUrl.do',
    // 		//data: data,
    // 		method:'get',
    // 		queryParams: {
    // 			'name':'getFanModelByTypeId',
    // 			'fanTypeId': id      
    //           }
    // 	})
 	// }
 	// function onContextMenu(e,row){
	// 	e.preventDefault();
	// 	$(this).treegrid('select', row.id);
	// 	$('#mm').menu('show',{
	// 		left: e.pageX,
	// 		top: e.pageY
	// 	});
	// }
 	
 	// function append(){
 	// 	var rootNode = $('#dg').treegrid('getRoot');
 	// 	var node = $('#dg').treegrid('getSelected');
 	// 	var pid = node.id;
 	// 	Easyui.Layer.openLayerWindow(
 	// 		      '新建产品模型',
 	// 		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId+'&pid='+pid+'&fanModelId='+rootNode.id,
 	// 		      ['650px','400px']);
 	// }
 	// function removeIt(){
 	// 	alert("移除");
 	// }
	</script>
</body> 
</html>
<script type="text/javascript">


/*大类*/
// function funTgAdd() {
	
// 	var nodes = $('#tt').tree('getSelected');
      
//       var pid = '0';
//     if(nodes != null){
//     	Easyui.Layer.openLayerWindow(   
//     		      '新建产品大类',
//     		      '/pages/shop/product/type/addProductType.jsp?id='+$('#tt').tree('getSelected').id,
//     		      ['655px','400px']);
//     }else{
//     	Easyui.Layer.openLayerWindow(   
//   		      '新建产品大类',
//   		      '/pages/shop/product/type/addProductType.jsp?',
//   		      ['655px','400px']);
//     } 
// }
// function funTgEdit() {

//     if($('#tt').tree('getSelected') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     console.log($('#tt').tree('getSelected').id)
//   Easyui.Layer.openLayerWindow(
//       '编辑产品大类',
//       '/pages/shop/product/type/editProductType.jsp?id='+$('#tt').tree('getSelected').id,
//        ['400px','280px']);
// }
// function funTgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/productCategory/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
        
//         shuoheUtil.layerMsgSaveOK('删除成功'),
        
//     );
//     $('#tt').tree('reload');
// }


/*模型*/
// function funMgAdd(){
// 	var nodess = $('#tt').tree('getSelected');
//     if(nodess == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     var rootNode = $('#dg').treegrid('getRoot');
//     if(rootNode){
//     	shuoheUtil.layerMsgCustom('风机模型已经存在，请添加配件');
//     }else{
//     	Easyui.Layer.openLayerWindow(
//     		      '新建产品模型',
//     		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId,
//     		      ['650px','400px']);
//     }
    
// }
// function funMgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
// 	 console.info($('#mg').datagrid('getSelected').id);
// 	Easyui.Layer.openLayerWindow(
//        '修改产品模型',
//        '/pages/shop/product/model/editProductModel.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//        ['400px','280px']);
// }
// function funMgDelete() {
//   // body...
//     var row = $('#tt').tree('getSelected');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个模型');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/extensionModel/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#mg').datagrid('reload');
// } 




/*配件*/
// function funDgAdd(){

//     if(Easyui.Datagraid.getSelectRow('dg') == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品模型');
//     }
//     Easyui.Layer.openLayerWindow(
//       '新建模型配件',
//       '/pages/shop/product/parts/addModelParts.jsp?uuid='+$('#mg').datagrid('getSelected').id,
//       Easyui.Layer.sizeTwoRow(600));
// }
// function funDgEdit() {
// 	if(Easyui.Datagraid.getSelectRow('dg') == null){
// 	        shuoheUtil.layerMsgCustom('必须选择一个模型配件');
//     }
// 	 console.info("555");
// 	Easyui.Layer.openLayerWindow(
//        '修改模型配件',
//        '/pages/shop/product/parts/editModelParts.jsp?uuid='+$('#dg').datagrid('getSelected').id,
//        Easyui.Layer.sizeTwoRow(600));
// }
// function funDgDele() {
//   // body...
//     var row = Easyui.Datagraid.getSelectRow('dg');
//     if(row == null){
//         shuoheUtil.layerMsgCustom('必须选择一个材料');
//         return;
//     }
//     Easyui.Ajax.post(
//         '/fanModelParts/delete?id='+row.id,
//         shuoheUtil.layerMsgError('删除失败'),
//         shuoheUtil.layerMsgSaveOK('删除成功')
//     );
//     $('#dg').datagrid('reload');
// }
// function fanModelOffer() {
// 	var node = $('#dg').treegrid('getSelected');
// 	console.log(node);
// 	if(node){
// 		$.ajax({
//             url: "/productMix/fanModelOffer",
//             type: "POST",
//             dataType: 'json',
//             data: {
//             	'modelNumber': node.id
 
//             },
//             error: function() //失败
//             {
//                 shuoheUtil.layerTopMaskOff();
//             },
//             success: function(data) {
//             	$('#dg').treegrid('reload');
//             }
//         });
// 	}else{
// 		shuoheUtil.layerMsgCustom('请选择要报价的模型');
// 	}
	
// }


  
</script>