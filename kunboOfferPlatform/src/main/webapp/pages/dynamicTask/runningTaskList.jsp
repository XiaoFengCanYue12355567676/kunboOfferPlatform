<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>正在运行任务列表</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center" hidden="true">任务编号</th>
					<th field="taskName" align="center">任务名称</th>
					<th field="taskGroup" align="center">任务分组</th>
					<th field="schedulingPattern" align="center">任务执行规则</th>
					<th field="className" align="center"  hidden="true">执行类名称</th>
					<th field="state" align="center" formatter="formatter">任务状态</th>
					<th field="missRule" align="center" hidden="true">任务状态</th>
					<th field="createTime" align="center" hidden="true">创建时间</th>
					<th field="createBy" align="center"  hidden="true">创建人</th>
					<th field="updateTime" align="center" hidden="true">修改时间</th>
					<th field="updateBy" align="center" hidden="true">修改人</th>
					<th data-options="field:'_operate',width:80,align:'center',formatter:showOrHidden" hidden="true">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<!-- <a id='addTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="addTask();return false;">建立任务</a>
			<a id='startTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="createAndStart();return false;">启动任务</a>
			<a id='pauseTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="pauseTask();return false;">暂停任务</a>
			<a id='resumeTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="resumeTask();return false;">恢复任务</a>
			<a id='deleteTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="deleteTask();return false;">删除任务</a>
			<a id='planTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="planTask();return false;">待运行任务</a>
			<a id='runTask' href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'fa fa-send-o',plain:true"
				onclick="runTask();return false;">运行中任务</a> -->
		</div>
	</div>



	<script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/dynamicTask/getRunningJobList',
        queryParams:
        {
        },
        method:'get'
      })
    });
    function formatter(value, row, index){
    	if(value=="NORMAL")
			return "正常";
    	if(value=="PAUSED")
			return "暂停";
    	if(value=="BLOCKED")
			return "阻塞";
    	if(value=="COMPLETE")
			return "完成";
    	if(value=="ERROR")
			return "错误";
    	if(value=="NONE")
			return "不存在";
    }
  </script>
	<script type="text/javascript">
	var id = null;//任务编号
	var taskName = "";//任务名称
	var schedulingPattern = null;//表达式
	var className = null;//运行类名称
	var taskGroup = "";
	
	//控制修改按钮的显示
	function showOrHidden(value,row,index){
	};
	
  	function onClickRow(rowIndex,rowData){
  		id = rowData.id
  		taskGroup = rowData.taskGroup
  		schedulingPattern = rowData.schedulingPattern
  		className = rowData.className
  		taskName = rowData.taskName
  	};

  	//新的任务建立方法
  	function createAndStart(){
  		$.ajax({
            url: "<%=basePath%>/dynamicTask/createAndStart",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					id:id,
    					taskName:taskName,
    					taskGroup:taskGroup,
    					startTime:schedulingPattern,
    					className:className
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    				}
    	});
    	//location.reload();    	
  	} 
  	
  //暂停任务
  	function pauseTask(){
  		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要操作的任务");
  			return false;
  		}
  		$.ajax({
            url: "<%=basePath%>/dynamicTask/pause",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					taskName:taskName,
    					taskGroup:taskGroup,
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    				}
    	});
    	//location.reload();    	
  	}
  
  	//恢复任务
  	function resumeTask(){
  		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要操作的任务");
  			return false;
  		}
  		$.ajax({
            url: "<%=basePath%>/dynamicTask/resume",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					taskName:taskName,
    					taskGroup:taskGroup,
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    				}
    	});
    	//location.reload();    	
  	}
  	
  	//删除任务
  	function deleteTask(){
  		var row = $('#dg').datagrid('getSelected');
  		if(null==row){
  			alert("您还没选中要操作的任务");
  			return false;
  		}
  		$.ajax({
            url: "<%=basePath%>/dynamicTask/delete",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					taskName:taskName,
    					taskGroup:taskGroup,
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    				}
    	});
    	//location.reload();    	
  	}
  </script>
</body>
</html>
