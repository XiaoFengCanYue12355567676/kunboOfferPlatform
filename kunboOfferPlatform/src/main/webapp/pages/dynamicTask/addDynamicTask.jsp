<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>新增定时任务</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<link type="text/css"
	href="<%=basePath%>/pages/js/layui/css/layui.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layui/layui.js"></script>
<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>
</head>
<body style="text-align: right;">
	<!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
	<table class="editTable" id="form" style="margin-left: 0px;">
		<tr>
			<td class="label" colspan="4"
				style="text-align: left; font-size: 20px;">
				<div class="divider">
					<span style="margin-left: 50px">新增定时任务</span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="label">任务名称</td>
			<td><input id='taskName' name='taskName' class="easyui-textbox"
				data-options="required:true,width:400,editable:true"></td>
		</tr>
		<tr>
			<td class="label">任务分组</td>
			<td><input id='taskGroup' name='taskGroup' class="easyui-textbox"
				data-options="required:true,width:400,editable:true"></td>
		</tr>
		<tr>
			<td class="label">任务规则</td>
			<td><input id='schedulingPattern' name='schedulingPattern'
				class="easyui-textbox"
				data-options="required:true,width:320,editable:true"><a
				href="#" onclick="openCron()"><span>点击配置规则</span></a></td>
		</tr>
		<tr>
			<td class="label">任务调用方法</td>
			<td><input id='className' name='className' style=""
				class="easyui-textbox"
				data-options="required:true,width:200,editable:true">
			</td>
		</tr>
		<tr>
			<td class="label">补偿措施</td>
			<td><select id='missRule' name='missRule'
				class="easyui-combobox"
				data-options="required:true,width:400,editable:true">
					<option value="0">立即执行一次，之后以设定周期执行</option><!-- withMisfireHandlingInstructionFireAndProceed -->
					<option value="1">补偿所有未执行，补偿完毕后按周期执行</option><!-- withMisfireHandlingInstructionIgnoreMisfires -->
					<option value="2">不执行，之后以设定频率执行</option><!-- withMisfireHandlingInstructionDoNothing -->
			</select></td>
		</tr>
	</table>
	<div style="width:100%;align:center">
		<input type="file" name="file" class="layui-upload-file">
		<img src="" id="img">
	</div>
	<script type="text/javascript">
		var callbackdata = function() {
			var param = {
				taskName : $("#taskName").val(),
				taskGroup : $("#taskGroup").val(), 
				schedulingPattern : $("#schedulingPattern").val(),
				className : $("#className").val(),
				missRule : $("#missRule").combobox('getValue')
			};
			return param;
		}
		function openCron(){
			layer.open({
	  			type: 2,
	            title: '生成定时规则',
	            anim: 1,
	            zIndex:2,
	            maxmin: false, //开启最大化最小化按钮
	            area: ['880px', '500px'],
	            content:'<%=basePath%>/pages/js/corn/cron.htm',
	            btn:['提交','取消'],
	            yes: function(index, layero){
	                 //按钮【按钮一】的回调
	            	var res = window["layui-layer-iframe" + index].callbackdata();
	            	//最后关闭弹出层
	            	layer.close(index);
	            	$("#schedulingPattern").textbox('setValue',res);
	            },
	  		});
		}
		layui.use('upload', function(){
			layui.upload({
				  url: '<%=basePath%>/upload/file?fileType=file',
				  type: 'file',
				  before: function(input){
				    //返回的参数item，即为当前的input DOM对象
				    console.log('文件上传中');
				  },
				  success: function(result){
				    console.log(result.obj);
				    $("#img").attr('src',result.obj);
				  }
				});  
			});
		
	</script>
</body>
</html>