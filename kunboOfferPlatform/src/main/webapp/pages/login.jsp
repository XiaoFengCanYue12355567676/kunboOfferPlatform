﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<meta charset="utf-8">
    <title>克莱特菲尔报价管理系统</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<script type="text/javascript" src="<%=basePath%>/custom/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script> 
</head>
<body>
    <div class="main">
    	<div class="mainin">
        	<h1>
				<font size="5" color="white">克莱特菲尔报价管理系统</font>
			   <!-- <img src="/images/ht_name.png">  -->
			</h1>
            <div class="mainin1">
            	<ul>
                	<li><span>用户名：</span><input name="loginName" type="text" id="userName" placeholder="登录名" class="SearchKeyword"></li>
                    <li><span>密码：</span><input type="password" name="Possword" id="password" placeholder="密码"/ class="SearchKeyword2"></li>
                    <div id="errorPointOut" style="display: none"><span style="color:#ff0000">用户名或者密码错误，请重新输入...</span></div>
                    <li><button class="tijiao" onclick="login()">提交</button></li>
                </ul>
            </div>
            <div class="footpage"></div>
        </div>
    </div>
<img src="images/loading.gif" id="loading" style=" display:none;position:absolute;" />
<div id="POPLoading" class="cssPOPLoading">
    <div style=" height:110px; border-bottom:1px solid #9a9a9a">
        <div class="showMessge"></div>
    </div>
    <div style=" line-height:40px; font-size:14px; letter-spacing:1px;">
        <a onclick="puc()">确定</a>
    </div>
</div>
<!-- <div style="text-align:center;"> -->
<!-- <p>建议使用IE8及以上版本浏览器&nbsp;</p> -->
<!-- </div> -->
<script type="text/javascript">
$(function(){		
	checkCookie();
})
function login()
{
	console.info("login");

	if(!checkInput()) return;

	console.info('if(!checkInput()) return;');
	var userName = $('#userName').val();
	var password = $('#password').val();
	var isSave = $("input[type='checkbox']").is(':checked');
	if(isSave == true)
	{
		console.info("userName 111= "+userName);
		console.info("password 111= "+password);
		console.info('isSave 111 = '+isSave);
		setCookie('username',userName,365)
		setCookie('password',password,365)
		setCookie('saveFlag',isSave,365)
	}
	else
	{
		console.info("userName 222= "+userName);
		console.info("password 222= "+password);
		console.info('isSave 222 = '+isSave);
		setCookie('username','',365)
		setCookie('password','',365)
		setCookie('saveFlag','',365)
	}

	//ajaxLoading();
	$.get
    (
         "<%=basePath%>/system/user/login.do",
        {	
            userName:userName,
            password:password
        },           
        function(data)
        {
			console.info(data.result);
			if(data.result==true)
			{					
				// ajaxLoadEnd();
				top.location='<%=basePath%>/pages/main.jsp';
			}
			if(data.result==false)
			{
				$("#errorPointOut").attr("style","display:block;");
				ajaxLoadEnd();
				$.messager.alert('提醒',"<font size='4px'>登录失败</font>",'warning');
			}
        },
		'json'
    );
}

function getCookie(c_name)
{
	if (document.cookie.length>0)
	{
		c_start=document.cookie.indexOf(c_name + "=")
		if (c_start!=-1)
		{ 
			c_start=c_start + c_name.length+1 
			c_end=document.cookie.indexOf(";",c_start)
			if (c_end==-1) c_end=document.cookie.length
			return unescape(document.cookie.substring(c_start,c_end))
		} 
	}
	return ""
}

function setCookie(c_name,value,expiredays)
{
	var exdate=new Date()
	exdate.setDate(exdate.getDate()+expiredays)
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
}

function checkCookie()
{
	var username=getCookie('username');
	var password=getCookie('password');
	var saveFlag = getCookie('saveFlag');

	console.info('username 222= '+username);
	console.info('password 222= '+password);
	console.info('saveFlag 222= '+saveFlag);

	if(username!=null&&username!='')
	{
		$('#userName').val(username);
	}
	if(password!=null&&password!='')
	{
		$('#password').val(password);
	}
	if(saveFlag == 'true')
	{
		console.info('xxxxxxxx');
		$("[type='checkbox']").attr("checked",true);//全选  
	}
	else
	{
		console.info('yyyyyyy');
		$("[type='checkbox']").attr("checked",false);//全选  
	}
}

$('#password').bind('keypress',function(event){  
    if(event.keyCode == "13") 
    {
    	console.info('回车键按键绑定');
    	login();
    }
});  
$('#userName').bind('keypress',function(event){  
    if(event.keyCode == "13") 
    {
    	console.info('回车键按键绑定');
    	login();
    }
});  
var checkInput = function()
{
	if(checkNull($('#userName').val()))
	{
		messageCustom('请输入用户名');
		return false;
	}
	if(checkNull($('#password').val()))
	{
		messageCustom('请输入密码');
		return false;
	}
	return true;
}

</script>
</body>
</html>
