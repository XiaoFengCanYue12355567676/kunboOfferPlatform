<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
      xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
<head lang="zh-CN">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Access-Control-Allow-Origin" content="localhost:8080"/>
    <title>待部署流程列表</title>
</head>
 <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" rel="stylesheet"/>
 <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="/lib/layer/theme/default/layer.css" rel="stylesheet"/>
 
 <script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="/lib/layer/layer.js"></script>
 <script src="/js/common.js"></script>
<body>
<div class="container">
	<div >当前登录者：<span>${user.firstName}</span>----<span>${group.name}</span></div>
	<div class="col-md-4 col-md-offset-10">
		<button type="button" class="btn btn-default text-right" onclick="add()">新建用户</button>
	</div>
	<form id="formList" class="" action="" method="post">
		<div class="form-group">
    		<label for="resourcePath">资源位置</label>
    		<input type="text" class="form-control" id="resourcePath" name="resourcePath" placeholder="your realname write here"/>
  		</div>
  		<div class="form-group">
    		<label for="name">流程名称</label>
    		<input type="text" class="form-control" id="name" name="name" placeholder="your name write here"/>
  		</div>
	</form>
	<button class="btn btn-default" id="save" type="submit" onclick="addForm()">提交</button>
	<button class="btn btn-default" id="save" type="submit" onclick="deployall()">提交</button>
</div>
<div id="tabletarget">
  	
  	</div>
<script type="text/javascript">
$(document).ready(function(){
	/* 页面加载是获取当前已部署的所有流程 */
	$.ajax({
        url:"/repo/getAllEnabledLst",
        type:"post",
        contentType:'application/json',
        success:function(list){
            if(list){
            	$("#tabletarget").empty();
            	var op='<table id="tablelist" class="table table-bordered"><caption>当前可用流程</caption><tr><th>流程id</th><th>标题</th><th>流程key</th><th>流程描述</th><th>流程版本</th><th>操作</th></tr>';
            	$.each(list, function (i, task) { 
            		op+='<tr><td>'+list[i].id+'</td><td>'+list[i].name+'</td><td>'+list[i].key+'</td><td>'+list[i].description+'</td><td>'+list[i].version+'</td><td><a href="#" onclick="beginProcess(\''+list[i].key+'\');return false;">查询任务节点</a><a href="#" onclick="beginProcess(\''+list[i].key+'\');return false;">启动流程任务</a></td></tr>'
            	});
            	op+='</table>';
            	$("#tabletarget").append(op);
            }else{
            	layer.msg("技术不行啊");
            }
        },
        error:function(e){
            alert("错误！！");
        }
    });   
})
//部署单个流程，需要指定流程文件位置和名称
function addForm(){
		$.ajax({
            url:"/repo/depolyNew",
            type:"post",
            data:$('#formList').serialize(),
            	/* username:$("#userName").val(),
            	password:$("#passWord").val(),
            	group:$("#group").val() ,
            	age:$("#age").val() */
            success:function(result){
                if(result.status==0)
                	layer.msg("部署成功！文件路径为："+result.obj.resourcePath+"，流程名称为："+result.obj.name);
                else
                	layer.msg("创建失败");
            },
            error:function(e){
                alert("错误！！");
                window.clearInterval(timer);
            }
        });        
}	
//部署所有流程，本方法只部署classpath目录下 processes/bpmn/
function deployall(){
	$.ajax({
        url:"/repo/depolyAll",
        type:"post",
        success:function(result){
            if(result.status==0)
            	layer.msg("部署成功！文件路径为："+result.obj.resourcePath+"，流程名称为："+result.obj.name);
            else
            	layer.msg("创建失败");
        },
        error:function(e){
            alert("错误！！");
            window.clearInterval(timer);
        }
    });        
}
//开始一个流程，启动后任务节点会移交给第一个任务角色或指定的任务执行人
function beginProcess(key){
	$.ajax({
        url:"/repo/startProcess",
        type:"post",
        data:{
        	processDefinitionKey:key
        },
        success:function(str){
            	layer.msg(str);
        },
        error:function(e){
            alert("错误！！");
            window.clearInterval(timer);
        }
    });        
}
</script>
</body>
</html>