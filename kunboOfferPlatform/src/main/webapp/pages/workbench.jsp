<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.*"%>
<%@page import="com.shuohe.entity.system.user.User"%>
<%@page import="com.shuohe.util.file.QFile"%>
<%@ page import="com.shuohe.util.json.*"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

    PrintWriter ss = response.getWriter();

    User user = null;
    String userJson = null;
    try
    {
    		user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userJson = Json.toJsonByPretty(user);
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>工作台</title>

<link rel="stylesheet" href="<%=basePath%>/pages/css/base.css">
<link rel="stylesheet" href="<%=basePath%>/../custom/uimaker/easyui.css">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="<%=basePath%>/pages/css/workbench.css">
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<!-- <link rel="stylesheet" href="css/basic_info.css" > -->

<!-- <style type="text/css">
    #banbenrizhi-headerCls{font-weight: bold}}
</style> -->

<script type="text/javascript">
var user = <%=userJson%>;
console.info("user="+ user.name);
console.info("user=" + user.id);
//console.info(static_permissionssJson);

 var openNoticeWindows=function(id)
 {
    // window.open('<%=basePath%>/pages/module/Office/notice/notice.jsp?id=\''+id+'\'', '', 'height=600px, width=500px, scrollbars=false, resizable=false');
    openWindowsInScreenMiddle(
        '<%=basePath%>/pages/module/Office/notice/viewOrder.jsp?id=\''+id+'\''
        ,500
        ,600);
 }


</script>
</head>
<body>
    <div class="container">
        <div id="hd">
        </div>
        <div id="bd">
            <div class="bd-content">
                <div class="right-zone">
                    <div class="inform item-box">
                        <div class="inform-hd">
                            <label>最新知识</label>
                            <a href="javascript:;">更多<span>&gt;</span></a>
                        </div>
                        <div class="easyui-panel" style="width: 100%;height: 195px">
                            <ul id = "newFIleUlId">
                              <li><a class="ellipsis">2017.11.22-新量具使用说明详解</a></li>
                              <li><a class="ellipsis">2017.11.16-如何让量具使用的时间更长，发挥其最大的价值</a></li>
                              <li><a class="ellipsis">2017.11.20-1.1</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="price item-box">
                        <div class="inform-hd">
                            <label>最新公告</label>
                            <a href="javascript:;" onClick='moreReleaseNotice()'>更多<span>&gt;</span></a>
                        </div>
                        <div class="easyui-panel" style="width: 100%;height: 235px">
                            <ul id = "newNoticeUlId">
                              <li><a class="ellipsis">2018.01.24-安全生产最新通告</a></li>
                              <li><a class="ellipsis">2018.01.12-最新量具维修流程步骤通告</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="center-part">
                    <div class="center-items todo">
                        <div class="calendar-part">
                             <div class="easyui-calendar" style="width:205px;height:231px;"></div>
                        </div>
                        <ul class="work-items clearfix">
                            <li>
                                <div class="work-inner">
                                    <div class="work-item green">
                                        <i class="fa fa-tags" style="margin-left: 15px"></i>
                                        <span class="num"  id = "approvalFileId">3650<span class="unit">个</span></span>
                                        <label>量具总数</label>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="work-inner">
                                    <div class="work-item red">
                                        <i class="fa fa-file-o" style="margin-left: 15px"></i>
                                        <span class="num"  id = "spareApplicantId"> 2880<span class="unit">个</span></span>
                                        <label>量具正在检定数</label>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="work-inner">
                                    <div class="work-item yellow">
                                        <i class="fa fa-list-alt" style="margin-left: 15px"></i>
                                        <span class="num"  id = "maintenanceSizeOneId">0<span class="unit">个</span></span>
                                        <label>正在维修数</label>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="work-inner">
                                    <div class="work-item blue">
                                        <i class="fa fa-external-link" style="margin-left: 15px"></i>
                                        <span class="num"  id = "otherApproval"> 150  <span class="unit">个</span></span>
                                        <label>设备总数</label>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="work-inner">
                                    <div class="work-item purple">
                                        <i class="fa fa-folder-open-o" style="margin-left: 15px"></i>
                                        <span title="2000,00万" class="num">90&nbsp;<span class="unit">个</span></span>
                                        <label>正在维护设备数</label>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="work-inner">
                                    <div class="work-item gray">
                                        <i class="fa fa-desktop" style="margin-left: 15px"></i>
                                        <span class="num">56&nbsp;<span class="unit">个</span></span>
                                        <label>待维修</label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div>
                      <div id="indexChart1" style="width:100%;height:300px;"></div>
                    </div>
                    <!-- <div class="center-items chart1">
                        <div class="chart1-inner">
                            <div class="item-hd" >历史30天故障</div>
                            <div style="position: relative;width: 100%;height: 120%" class="chart1-chart" id="chart3"></div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <div id="ft"></div>
    </div>
    <div class="todo-panel" id='todo_panel_file_oper'>
         <div class="todo-title">
            <i class="iconfont">&#xe684;</i>
            <span class="num" id = "todoPanelFileId">    &nbsp; </span>
            <label>待审批文件</label>
        </div>
        <div class="todo-items">
            <ul id = "todo_items_file_oper">
            </ul>
        </div>
    </div>

    <div class="todo-panel" id='todo_panel_spare_app'>
        <div class="todo-title">
            <i class="iconfont">&#xe63c;</i>
                <span class="num" id = "todoPanelApplicantSizeId">    &nbsp; </span>
                <label>备件申请信息</label>
        </div>
        <div class="todo-items">
            <ul id = "todo_items_spare_app">
            </ul>
        </div>
    </div>

    <div class="todo-panel" id='todo_panel_approval_panel'>
        <div class="todo-title">
            <i class="iconfont">&#xe63c;</i>
                <span class="num" id = "otherApprovalPanel">    &nbsp; </span>
                <label>其他申请</label>
        </div>
        <div class="todo-items">
            <ul id = "todo_items_other_approvals">
            </ul>
        </div>
    </div>


    <div class="todo-panel" id='todo_panel_mt_event'>
        <div class="todo-title">
            <i class="iconfont">&#xe612;</i>
            <span class="num" id = "maintenanceSizeId">    &nbsp; </span>
            <label>维修事件信息</label>
        </div>
        <div class="todo-items">
            <ul id = "todo_items_mt_event">
            </ul>
        </div>
    </div>


    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
    <!-- <script type="text/javascript" src="js/menu.js"></script> -->
    <script type="text/javascript" src="<%=basePath%>pages/js/echarts/echarts.js"></script>


    <script type="text/javascript">
    require.config({
            paths: {
                echarts: '<%=basePath%>/pages/js/echarts'
            }
        });
        require(
            [
                'echarts',
                'echarts/chart/line',   // 按需加载所需图表，如需动态类型切换功能，别忘了同时加载相应图表
                'echarts/chart/bar'
            ],
            function (ec) {
                var myChart = ec.init(document.getElementById('indexChart1'));
                option = {
    title : {
        text: '维修统计',
        subtext: ''
    },
    tooltip : {
        trigger: 'axis'
    },
    
    /*legend: {
        data:['销售订单计划总数','销售订单完成数']
    },*/
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'维修计划总数',
            type:'bar',
            data:[170, 150, 145, 155, 167, 176, 145, 162, 178, 186, 179, 168],
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'维修计划完成数',
            type:'bar',
            data:[167, 147, 144, 146, 161, 158, 140, 157, 172, 180, 160, 158],
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};
                myChart.setOption(option);
            }
        );

    $(function(){
    	//获取量具总数和正在检定的量具数目的接口
    	// $.ajax({
     //        url: "/mro/masure/getMeasureCount.do",
     //        type: "POST",
     //        dataType: 'json',
     //        data: {
     //        },
     //        error: function() //失败
     //        {
     //            alert(3333333);
     //        },
     //        success: function(data) //成功
     //        {   $("#approvalFileId").text(data.measureCount);
     //            $("#spareApplicantId").text(data.measureExcuteCount);
     //            console.log(JSON.stringify(data));
     //        }
     //    });

    });


    //chart0
    $(document).ready(function(){
    });

    //     //我的待办点击事件
    //     $(document).on('click','.work-item.green',function(event){
    //         var width = (2 * $(this).width()) + 10;
    //         $("#todo_panel_file_oper").width(width -2).css({top:$(this).offset().top,left:$(this).offset().left}).show();
    //         event.stopPropagation();
    //         $("#todo_panel_mt_event").hide();
    //         $("#todo_panel_spare_app").hide();
    //         $("#todo_panel_approval_panel").hide();
    //     });
    //     $("#todo_panel_file_oper").click(function(){
    //          event.stopPropagation();
    //     });
    //     $(document).click(function(){
    //         $("#todo_panel_file_oper").hide();
    //     });
    //
    //
    // // 备件申请信息
    //     $(document).on('click','.work-item.red',function(event){
    //         var width = (2 * $(this).width()) + 10;
    //         $("#todo_panel_spare_app").width(width -2).css({top:$(this).offset().top,left:$(this).offset().left}).show();
    //         event.stopPropagation();
    //         $("#todo_panel_file_oper").hide();
    //         $("#todo_panel_mt_event").hide();
    //         $("#todo_panel_approval_panel").hide();
    //     });
    //     $("#todo_panel_spare_app").click(function(){
    //          event.stopPropagation();
    //     });
    //     $(document).click(function(){
    //         $("#todo_panel_spare_app").hide();
    //     }) ;
    //
    //
    //     // 维修事件信息
    //     $(document).on('click','.work-item.yellow',function(event){
    //         var width = (2 * $(this).width()) + 10;
    //         $("#todo_panel_mt_event").width(width -2).css({top:$(this).offset().top,left:$(this).offset().left}).show();
    //         event.stopPropagation();
    //         $("#todo_panel_file_oper").hide();
    //         $("#todo_panel_spare_app").hide();
    //         $("#todo_panel_approval_panel").hide();
    //
    //     });
    //     $("#todo_panel_mt_event").click(function(){
    //          event.stopPropagation();
    //     });
    //     $(document).click(function(){
    //         $("#todo_panel_mt_event").hide();
    //     }) ;
    //
    //
    //    // 维修事件信息
    //     $(document).on('click','.work-item.blue',function(event){
    //         var width = (2 * $(this).width()) + 10;
    //         $("#todo_panel_approval_panel").width(width -2).css({top:$(this).offset().top,left:$(this).offset().left}).show();
    //         event.stopPropagation();
    //         $("#todo_panel_file_oper").hide();
    //         $("#todo_panel_spare_app").hide();
    //         $("#todo_panel_mt_event").hide();
    //     });
    //     $("#todo_panel_approval_panel").click(function(){
    //          event.stopPropagation();
    //     });
    //     $(document).click(function(){
    //         $("#todo_panel_approval_panel").hide();
    //     }) ;




    </script>
</body>
</html>
