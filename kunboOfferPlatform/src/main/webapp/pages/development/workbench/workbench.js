/*function addTab(title, url){
    var t = $('#index_tabs'); 
	if (t.iTabs('exists', title)) {
        t.iTabs('select', title);
    } else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tabs').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
*/
  //打开Tab窗口
function addTab(title, url) {
	if (parent.$('#index_tabs').tabs('exists', title)){
		parent.$('#index_tabs').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		parent.$('#index_tabs').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
var config = new Object();
var workbench = {
    // 工作台对象
    // config,
    //将json配置文件转换为工作台对象
    createWorkbenchConfig: function(config_json) {
        var config = {};
        config.infoCard = new Array();
        config.chartCard = new Array();
        return config;
    },
    createPanelCol: function(index, icon, url, backgroud_colour, msg, msg_context) {
        var html = '';
        html += '<div class="panel col">';
        html += '    <a href="#" class="easyui-linkbutton" onclick="addTab(\''+msg_context+'\',\''+url+'\')">';
        html += '        <div class="panel_icon" style="background-color:' + backgroud_colour + ';">';
        html += '            <i class="' + icon + '"></i>';
        html += '        </div>';
        html += '        <div class="panel_word newMessage">';
        html += '            <span id="infoCard_' + index + '">' + msg + '</span>';
        html += '            <cite>' + msg_context + '</cite>';
        html += '        </div>';
        html += '     </a>';
        html += '</div> ';
        console.info("html="+html);
        return html;
        
    },
    createPanelBoxRow: function(argument) {
        var html = '';
        html += '<div class="panel_box row" id="panel_1">'
        html += '</div>'
        return html;
    },
    createChart: function(id,title) {
        var html = '';
        html += '<div class="layui-col-md4">';
        html += '    <blockquote class="layui-elem-quote title">'+title+'</blockquote>';
        html += '    <table class="layui-table" lay-skin="line">';
        html += '        <tbody>';
        html += '        <tr>';
        html += '            <td align="left">';
        html += '                    <div id="echarts_'+id+'" style="width: 100%;height:300px;"></div>';
        html += '            </td>';
        html += '        </tr>';
        html += '        </tbody>';
        html += '    </table>';
        html += '</div>';
        return html;
    },
    initInfoCard: function(info_card) {
        for (var i = 0; i < info_card.length; i++) {
            var icon = info_card[i].icon;
            var url = info_card[i].url;
            var backgroud_colour = info_card[i].backgroud_colour;
            var msg = info_card[i].msg;
            var msg_context = info_card[i].msg_context;
            $('#panel_1').append(workbench.createPanelCol(i, icon, url, backgroud_colour, msg, msg_context));

            $('#infoCard_'+i).load(
                '/develop/url/getUrl.do',
                {
                    'name':msg,
                    'postation_id': shuoheUtil.getUrlHeader().postation_id,
                    'user_id':user.id
                },
                function(response,status,xhr) {
                    console.info(this.id);
                    console.info("response==="+response);
                    // this.text('1111');
                    $('#'+this.id).text(JSON.parse(response).number);
                }
            );
        }
    },
    initECharts: function(json) {
        for (var i = 0; i < json.length; i++) {
            var title = json[i].title;
            var url = json[i].url;
            var chart_style = json[i].chart_style;

            $('#panel_2').append(workbench.createChart(i, title));
            console.info("i==="+i);
            console.info("title==="+title);
            console.info("url==="+url);
            console.info("chart_style==="+chart_style);
            workbench.getRemoteDataAndLoadEchart('echarts_' + i,url,chart_style);
        }
    },
    getRemoteDataAndLoadEchart: function(id,url,chart_style) {
        $.ajax({
            url: '/develop/url/getUrl.do',
            type: "POST",
            dataType: 'json',
            data: {
                name: url,
                user_id:user.id
            },
            error: function() //失败
            {},
            success: function(data) //成功
            {
                console.info(data);

                if (chart_style == 'pie') {
                    workbench.initPie(id , data);
                } else if (chart_style == 'bar') {
                    workbench.initBar(id , data);                    
                } else if (chart_style == 'area_chart') {
                    workbench.initAreaChart(id , data);          
                } else if (chart_style == 'line') {
                    workbench.initLine(id , data);   
                } else if (chart_style == 'horizontal_bar') {
                    workbench.initHorizontalBar(id , data);   
                }
            }
        });
    },
    initRemoteConfig: function() {
        $.ajax({
            url: "/develop/workbench/findByPostationId.do",
            type: "GET",
            dataType: 'json',
            data: {
                'postation_id': shuoheUtil.getUrlHeader().postation_id
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data) //成功
            {
                config = data;
                console.info(config);
                workbench.initInfoCard(JSON.parse(config.config_str).infoCard);
                workbench.initECharts(JSON.parse(config.config_str).charts);
            }
        });
    },
    initPie:function(charts_id,data) {
        console.info('1111'+charts_id);
        var myChart = echarts.init(document.getElementById(charts_id));
        console.info(charts_id);        
        console.info(data);        
        option = {
            tooltip: {
                trigger: 'item'
            },
            // grid: {containLabel: true},
            series: [{
                type: 'pie',
                data: data,
                roseType: 'radius',
                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function(idx) {
                    return Math.random() * 200;
                }
            }]
        };
        myChart.setOption(option);
    },
    initBar: function(charts_id,data) {
        var myChart = echarts.init(document.getElementById(charts_id));
        var xAxis = new Array();
        var yAxis = new Array();
        var yAxis1 = new Array();
        for(var i=0;i<data.length;i++)
        {
            xAxis.push(data[i].name);
            yAxis.push(data[i].value);
           // yAxis1.push(data[i].value);
            console.info("data[i].value=="+JSON.stringify(data));
        }
        // 指定图表的配置项和数据
        var option = {
            tooltip: {},
            xAxis: {
                data: xAxis
            },
            yAxis: {},
            series: [{
                // name: '销量',
                type: 'bar',
                data: yAxis
            },
            {
                // name: '销量',
                type: 'bar',
                data: yAxis
            }]

        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    },
    initTwoBar: function(charts_id,data) {
        var myChart = echarts.init(document.getElementById(charts_id));
        var xAxis = new Array();
        var yAxis = new Array();
        for(var i=0;i<data.length;i++)
        {
            xAxis.push(data[i].name);
            yAxis.push(data[i].value);
        }
        // 指定图表的配置项和数据
        var option = {
            tooltip: {},
            legend: {
                data:['蒸发量','降水量']
            },
            xAxis: {
                data: xAxis
            },
            yAxis: {},
            series: [{
                // name: '销量',
                type: 'bar',
                data: yAxis
            },
            {
                // name: '销量',
                type: 'bar',
                data: yAxis
            }]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    },
    initAreaChart:function(charts_id,data) {
        var myChart = echarts.init(document.getElementById(charts_id));
        var xAxis = new Array();
        var yAxis = new Array();
        for(var i=0;i<data.length;i++)
        {
            xAxis.push(data[i].name);
            yAxis.push(data[i].value);
        }   
        option = {
            tooltip: {
                trigger: 'axis'
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: xAxis//['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: yAxis,//[820, 932, 901, 934, 1290, 1330, 1320],
                type: 'line',
                areaStyle: {}
            }]
        };
        myChart.setOption(option);
    },
    initLine: function(charts_id,data) {
        var myChart = echarts.init(document.getElementById(charts_id));
        var xAxis = new Array();
        var yAxis = new Array();
        for(var i=0;i<data.length;i++)
        {
            xAxis.push(data[i].name);
            yAxis.push(data[i].value);
        }
        option = {
            tooltip: {  
                trigger: 'axis'
            },
            xAxis: {
                type: 'category',
                data: xAxis
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: yAxis,
                type: 'line'
            }]
        };

        myChart.setOption(option);
    },
    initHorizontalBar: function(charts_id,data) {
        var myChart = echarts.init(document.getElementById(charts_id));
        var array_0 = new Array();
        var array_1 = new Array();
        array_1.push('amount');
        array_1.push('product');
        array_0.push(array_1);

        for(var i=0;i<data.length;i++)
        {
            var array_x = new Array();
            array_x.push(data[i].value);
            array_x.push(data[i].name);
            array_0.push(array_x);
        }
        console.info(array_0);
        var option = {
            tooltip: {
                trigger: 'axis'
            },
            dataset: {
                source: array_0
                // [
                //     array_0
                //     // ['amount', 'product'],
                //     // [58212, 'Matcha Latte'],
                //     // [78254, 'Milk Tea'],
                //     // [41032, 'Cheese Cocoa'],
                //     // [12755, 'Cheese Brownie'],
                //     // [20145, 'Matcha Cocoa'],
                //     // [79146, 'Tea'],
                //     // [91852, 'Orange Juice'],
                //     // [101852, 'Lemon Juice'],
                //     // [20112, 'Walnut Brownie']
                // ]
            },
            // grid: {containLabel: true},
            xAxis: {
                name: 'amount'
            },
            yAxis: {
                type: 'category'
            },
            series: [{
                type: 'bar',
                encode: {
                    x: 'amount',
                    y: 'product'
                }
            }]
        };
        myChart.setOption(option);
    }
}

$(function() {

    // $('#panel_1').append(workbench.createPanelCol('fa fa-edit', null, '#009688', '12/3', '新增人数'));
    workbench.initRemoteConfig();
    // workbench.initCharts();
    // workbench.initHorizontalBar();
});