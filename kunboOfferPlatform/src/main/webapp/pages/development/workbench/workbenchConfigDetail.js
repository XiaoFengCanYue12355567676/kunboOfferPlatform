var infoCard_editIndex = undefined;
var echarts_editIndex = undefined;
var config = new Object();


$(function() {
    initWorckbenchConfigJosn();
});


function endEditing() {
    if (infoCard_editIndex == undefined) {
        return true
    }
    if ($('#infoCardDg').datagrid('validateRow', infoCard_editIndex)) {
        $('#infoCardDg').datagrid('endEdit', infoCard_editIndex);
        infoCard_editIndex = undefined;
        return true;
    } else {
        return false;
    }
}

function onClickRow(index) {
    if (infoCard_editIndex != index) {
        if (endEditing()) {
            $('#infoCardDg').datagrid('selectRow', index)
                .datagrid('beginEdit', index);
            infoCard_editIndex = index;
        } else {
            $('#infoCardDg').datagrid('selectRow', infoCard_editIndex);
        }
    }
}

function append() {
    if (endEditing()) {
        $('#infoCardDg').datagrid('appendRow', {
            status: '否'
        });
        infoCard_editIndex = $('#infoCardDg').datagrid('getRows').length - 1;
        $('#infoCardDg').datagrid('selectRow', infoCard_editIndex)
            .datagrid('beginEdit', infoCard_editIndex);
    }
}

function removeit() {
    if (infoCard_editIndex == undefined) {
        return
    }
    $('#infoCardDg').datagrid('cancelEdit', infoCard_editIndex)
        .datagrid('deleteRow', infoCard_editIndex);
    infoCard_editIndex = undefined;
}

function accept() {
    //判断是否填写了内容
    if (endEditing()) {
        $('#infoCardDg').datagrid('acceptChanges');
    }
}

function reject() {
    $('#infoCardDg').datagrid('rejectChanges');
    infoCard_editIndex = undefined;
}

function getChanges() {
    var rows = $('#infoCardDg').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}



///////////////////////////////////////////////////////
function echarts_endEditing() {
    if (echarts_editIndex == undefined) {
        return true
    }
    if ($('#echarts_dg').datagrid('validateRow', echarts_editIndex)) {
        $('#echarts_dg').datagrid('endEdit', echarts_editIndex);
        echarts_editIndex = undefined;
        return true;
    } else {
        return false;
    }
}

function echarts_onClickRow(index) {
    console.info(index);
    if (echarts_editIndex != index) {
        if (echarts_endEditing()) {
            $('#echarts_dg').datagrid('selectRow', index).datagrid('beginEdit', index);
            echarts_editIndex = index;
        } else {
            $('#echarts_dg').datagrid('selectRow', echarts_editIndex);
        }
    }
}

function echarts_append() {
    if (echarts_endEditing()) {
        $('#echarts_dg').datagrid('appendRow', {
            status: '否'
        });
        echarts_editIndex = $('#echarts_dg').datagrid('getRows').length - 1;
        $('#echarts_dg').datagrid('selectRow', echarts_editIndex).datagrid('beginEdit', echarts_editIndex);
    }
}

function echarts_removeit() {
    if (echarts_editIndex == undefined) {
        return
    }
    $('#echarts_dg').datagrid('cancelEdit', echarts_editIndex)
        .datagrid('deleteRow', echarts_editIndex);
    echarts_editIndex = undefined;
}

function echarts_accept() {
    //判断是否填写了内容
    if (echarts_endEditing()) {
        $('#echarts_dg').datagrid('acceptChanges');
    }
}

function echarts_reject() {
    $('#echarts_dg').datagrid('rejectChanges');
    echarts_editIndex = undefined;
}

function echarts_getChanges() {
    var rows = $('#echarts_dg').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}









function getAllInfoCard() {
    var rows = $('#infoCardDg').datagrid('getRows');
    return rows;
}
function getAllCharts() {
    var rows = $('#echarts_dg').datagrid('getRows');
    return rows;
}

function putAllInfoCard(json) {
    $('#infoCardDg').datagrid('loadData', json);
}
function putAllCharts(json) {
    $('#echarts_dg').datagrid('loadData', json);
}

function initWorckbenchConfigJosn() {
    $.ajax({
        url: "/develop/workbench/findByPostationId.do",
        type: "GET",
        dataType: 'json',
        data: {
            'postation_id': shuoheUtil.getUrlHeader().postation_id
        },
        error: function() //失败
        {
            // shuoheUtil.layerTopMaskOff();
            // shuoheUtil.layerTopMsgError('远程通信失败');
        },
        success: function(data) //成功
        {
            config = data;
            putAllInfoCard(JSON.parse(config.config_str).infoCard);
            putAllCharts(JSON.parse(config.config_str).charts);
            console.info(config);
        }
    });
}


function postAllInfoCardForJson() {

    console.info("config="+config);

    config.postation_id = shuoheUtil.getUrlHeader().postation_id;
    config.config_str = JSON.stringify(getConfig());
    var json_str = JSON.stringify(config);

    $.ajax({
        url: "/develop/workbench/save.do",
        type: "POST",
        dataType: 'json',
        data: {
            'data': json_str
        },
        error: function() //失败
        {
            shuoheUtil.layerTopMaskOff();
            shuoheUtil.layerTopMsgError('远程通信失败');
        },
        success: function(data) //成功
        {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
                shuoheUtil.layerTopMsgOK('保存成功'); 
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
}
//获取整个配置页面的config实例
function getConfig() {
    var obj = new Object();
    obj.infoCard = getAllInfoCard();
    obj.charts = getAllCharts();
    return obj;
}



function quickView() {

    top.layer.open({
        type: 2,
        title: '新建URL',
        shadeClose: false,
        shade: 0.3,
        maxmin: true, //开启最大化最小化按钮 
        area: shuoheUtil.layer_customer_difference(0,200),
        // btn: '保存',
        content: '/pages/development/workbench/workbench.jsp?postation_id='+shuoheUtil.getUrlHeader().postation_id,
        yes: function(index) {
        }
    })
}
