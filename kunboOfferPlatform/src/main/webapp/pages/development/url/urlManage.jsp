
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>链接管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">           

        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                  idField:'id',
                  treeField:'text',
                  method:'get'
                ">
          <thead>
              <tr>
                  <th field="text" width="100%">项目</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20">
            <thead>
              <tr href="#">
                         
                <th field="id"  hidden ="true">id</th>
                <th field="pid" >pid</th>
                <th field="name" >名称</th>
                <th field="descr" >描述</th>
                <th field="type" >URL类型</th>
                <th field="creater" >创建人</th>
                <th field="create_date" >创建时间</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-hand-pointer-o',plain:true" onclick="view()">查看</a>
            <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
            <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit()">修改</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-magic',plain:true" onclick="run()">测试</a>
          </div>
       </div>
    </div>
    



  <script type="text/javascript">
     $('#tt').treegrid({
         url:'<%=basePath%>/topJUI/index/getMenuList.do',
         onClickRow: function (rowIndex)  
         { 
            var rr_row = $('#tt').treegrid('getSelected');  
            $('#dg').datagrid({
                url:'<%=basePath%>/develop/url/getByPid.do',
                queryParams:
                {
                  pid:rr_row.id
                },
                method:'get'
            })
         },
         onLoadSuccess:function(data)
         {  
         }  
      })
	</script>
  



</body> 
</html>
<script type="text/javascript">
  function add()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '新建URL',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/addUrl.jsp?pid='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function view()
  {
      var row = $('#dg').datagrid('getSelected');  
        if(row == null)
        {
          layerMsgCustom('必须选择一个目录');
          return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            content: 'form/viewUrl.jsp?id='+row.id
        })
  }


  function edit()
  {

      var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/editUrl.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.msg('你确定删除么？', {
      time: 0, //不自动关闭
      btn: ['确定', '取消'],
      yes: function(index) {
        layer.close(index);
        shuoheUtil.layerTopMaskOn();
        $.ajax({
          url: "/develop/url/delete.do",
          type: "POST",
          dataType: 'json',
          data: {
            'id': row.id
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
              shuoheUtil.layerMsgSaveOK(data.describe);
            } else {
              shuoheUtil.layerMsgCustom(data.describe);
            }
            $('#dg').datagrid('reload');
          }
        });
      }
    });
  }


  function updateSuccess()
  {
    $('#dg').datagrid('reload'); 

  }

  function run()
  {
      var row = $('#dg').datagrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个URL');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'form/runUrl.jsp?name='+row.name        
      })

    //    //示范一个公告层
    // layer.open({
    //   type: 1
    //   ,title: false //不显示标题栏
    //   ,closeBtn: true
    //   ,area: [layer_width, layer_height]
    //   ,shade: 0.8
    //   ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
    //   ,resize: false
    //   ,btnAlign: 'c'
    //   ,moveType: 1 //拖拽模式，0或者1
    //   ,content: 'form/runUrl.jsp?name='+row.name,
    // });
      // $.ajax({
      //     url: "<%=basePath%>/develop/url/getUrl.do",
      //     type : "POST",
      //     dataType : 'json',
      //     data : {
      //         'name' : row.name
      //     },
      //     error : function() //失败
      //     {
            
      //     },
      //     success : function(data)//成功
      //     {
      //         var log = format(JSON.stringify(data))
      //         console.info(log);
      //         viewRun(log);
      //     }
      // });
  }
  // function viewRun(data)
  // {
  //   //示范一个公告层
  //   layer.open({
  //     type: 1
  //     ,title: false //不显示标题栏
  //     ,closeBtn: true
  //     ,area: [layer_width, layer_height]
  //     ,shade: 0.8
  //     ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
  //     ,resize: false
  //     ,btnAlign: 'c'
  //     ,moveType: 1 //拖拽模式，0或者1
  //     ,content: 'form/runUrl.jsp?id='+row.name,
  //   });
  // }

</script>
<script>
    //格式化代码函数,已经用原生方式写好了不需要改动,直接引用就好
    var formatJson = function (json, options) {
        var reg = null,
                formatted = '',
                pad = 0,
                PADDING = '    ';
        options = options || {};
        options.newlineAfterColonIfBeforeBraceOrBracket = (options.newlineAfterColonIfBeforeBraceOrBracket === true) ? true : false;
        options.spaceAfterColon = (options.spaceAfterColon === false) ? false : true;
        if (typeof json !== 'string') {
            json = JSON.stringify(json);
        } else {
            json = JSON.parse(json);
            json = JSON.stringify(json);
        }
        reg = /([\{\}])/g;
        json = json.replace(reg, '\r\n$1\r\n');
        reg = /([\[\]])/g;
        json = json.replace(reg, '\r\n$1\r\n');
        reg = /(\,)/g;
        json = json.replace(reg, '$1\r\n');
        reg = /(\r\n\r\n)/g;
        json = json.replace(reg, '\r\n');
        reg = /\r\n\,/g;
        json = json.replace(reg, ',');
        if (!options.newlineAfterColonIfBeforeBraceOrBracket) {
            reg = /\:\r\n\{/g;
            json = json.replace(reg, ':{');
            reg = /\:\r\n\[/g;
            json = json.replace(reg, ':[');
        }
        if (options.spaceAfterColon) {
            reg = /\:/g;
            json = json.replace(reg, ':');
        }
        (json.split('\r\n')).forEach(function (node, index) {
                    var i = 0,
                            indent = 0,
                            padding = '';

                    if (node.match(/\{$/) || node.match(/\[$/)) {
                        indent = 1;
                    } else if (node.match(/\}/) || node.match(/\]/)) {
                        if (pad !== 0) {
                            pad -= 1;
                        }
                    } else {
                        indent = 0;
                    }

                    for (i = 0; i < pad; i++) {
                        padding += PADDING;
                    }

                    formatted += padding + node + '\r\n';
                    pad += indent;
                }
        );
        return formatted;
    };
    // //引用示例部分
    // //(1)创建json格式或者从后台拿到对应的json格式
    // var originalJson = {"name": "binginsist", "sex": "男", "age": "25"};
    // //(2)调用formatJson函数,将json格式进行格式化
    // var resultJson = formatJson(originalJson);
    // //(3)将格式化好后的json写入页面中
    // document.getElementById("writePlace").innerHTML = '<pre>' +resultJson + '<pre/>';
</script>
<script type="text/javascript">
  function format(txt,compress/*是否为压缩模式*/){/* 格式化JSON源码(对象转换为JSON文本) */  
        var indentChar = '    ';   
        if(/^\s*$/.test(txt)){   
            alert('数据为空,无法格式化! ');   
            return;   
        }   
        try{var data=eval('('+txt+')');}   
        catch(e){   
            alert('数据源语法错误,格式化失败! 错误信息: '+e.description,'err');   
            return;   
        };   
        var draw=[],last=false,This=this,line=compress?'':'\n',nodeCount=0,maxDepth=0;   
           
        var notify=function(name,value,isLast,indent/*缩进*/,formObj){   
            nodeCount++;/*节点计数*/  
            for (var i=0,tab='';i<indent;i++ )tab+=indentChar;/* 缩进HTML */  
            tab=compress?'':tab;/*压缩模式忽略缩进*/  
            maxDepth=++indent;/*缩进递增并记录*/  
            if(value&&value.constructor==Array){/*处理数组*/  
                draw.push(tab+(formObj?('"'+name+'":'):'')+'['+line);/*缩进'[' 然后换行*/  
                for (var i=0;i<value.length;i++)   
                    notify(i,value[i],i==value.length-1,indent,false);   
                draw.push(tab+']'+(isLast?line:(','+line)));/*缩进']'换行,若非尾元素则添加逗号*/  
            }else   if(value&&typeof value=='object'){/*处理对象*/  
                    draw.push(tab+(formObj?('"'+name+'":'):'')+'{'+line);/*缩进'{' 然后换行*/  
                    var len=0,i=0;   
                    for(var key in value)len++;   
                    for(var key in value)notify(key,value[key],++i==len,indent,true);   
                    draw.push(tab+'}'+(isLast?line:(','+line)));/*缩进'}'换行,若非尾元素则添加逗号*/  
                }else{   
                        if(typeof value=='string')value='"'+value+'"';   
                        draw.push(tab+(formObj?('"'+name+'":'):'')+value+(isLast?'':',')+line);   
                };   
        };   
        var isLast=true,indent=0;   
        notify('',data,isLast,indent,false);   
        return draw.join('');   
    }  

</script>

