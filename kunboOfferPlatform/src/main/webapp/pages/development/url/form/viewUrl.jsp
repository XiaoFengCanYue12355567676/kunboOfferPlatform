<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    PrintWriter ss = response.getWriter();
    String id = request.getParameter("id"); 
    User user = null;
    String userStr = null;
    try
    {

        user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
            //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userStr = Json.toJsonByPretty(user);
    }
        catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加URL</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>



<body>
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL概览</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">接口名</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
			<td class="label">名称</td>
            <td >
                <input id='descr' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
            </td>
        </tr>


        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">SQL语句</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <input type="text" id="sql_str" class="easyui-textbox" data-options="width:554,height:300,multiline:true" AcceptsReturn="true">
                </div>
            </td>
        </tr>  

        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL输入参数</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg3" class='easyui-datagrid'
                        style="height: 300px; width: 554px" title=""
                        data-options="
                            method: 'get',
                            rownumbers:true,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:true,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            pageSize:10">
                        <thead>
                            <tr>
                                <th field="id" hidden="true">序列</th>
                                <th field="id">描述</th>
                                <th field="mt_project">名称</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
        <!---->
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL输出参数</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg4" class='easyui-datagrid'
                        style="height: 300px; width: 554px" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:true,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true">
                        <thead>
                            <tr>
                                <th field="id" hidden="true">序列</th>
                                <th field="pid" hidden="true"></th>
                                <th field="name">名称</th>
                                <th field="type">类型</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </td>
        </tr>
    
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">输出排序(SQL order by desc or inc)</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <input type="text" id="arrange_str" class="easyui-textbox" data-options="width:554,height:100,multiline:true">
                </div>
            </td>
        </tr>  
    </table>
	
</body> 
</html>
<script type="text/javascript">
    var user = <%=userStr%>;
    var id = <%=id%>;

    $(function(){

        $.ajax({
            url: "<%=basePath%>/develop/url/findUrlById.do",
            type : "POST",
            dataType : 'json',
            data : {
                'id': id        
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                $('#name').textbox('setValue',data.name);
                $('#descr').textbox('setValue',data.descr);
                $('#type').combobox('setText',data.type);
                $('#arrange_str').textbox('setValue',data.arrange_str);     
                console.info(data.descr);              
            }
        });
        $.ajax({
            url: "<%=basePath%>/develop/url/findUrlSqlById.do",
            type : "POST",
            dataType : 'text',
            data : {
                'id': id        
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                $('#sql_str').textbox('setValue',data);
            }
        });
        $('#dg3').treegrid({
            url:'<%=basePath%>/develop/url/findUrlIutputParaByPid.do',
            queryParams:
            {
                id:id
            }
        })
        $('#dg4').treegrid({
        	url:'<%=basePath%>/develop/url/findUrlOutputParaByPid.do',
            queryParams:
            {
                pid:id
            }
        })



    });




</script>
