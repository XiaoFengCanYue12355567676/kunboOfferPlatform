<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    PrintWriter ss = response.getWriter();
    String pid = request.getParameter("pid"); 
    User user = null;
    String userStr = null;
    try
    {

        user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
            //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userStr = Json.toJsonByPretty(user);
    }
        catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加URL</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>



<body>
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL概览</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">接口名(英文)</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
            <td class="label">汉语简称</td>
            <td >
                <input id='descr' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
            </td>
        </tr>
        <tr>            
            <td class="label">返回类型</td>
            <td >
                <select id='type' class="easyui-combobox" data-options="required:false,width:200,editable:false">    
                    <option>easyui-combobox</option>  
                    <option>easyui-datagrid</option>         
                    <option>Object</option>  
                    <option>List-Object</option>
                    <option>easyui-treegrid</option>      
                </select>          
            </td>            
        </tr>


        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">SQL语句</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <input type="text" id="sql_str" class="easyui-textbox" data-options="width:554,height:300,multiline:true">
                </div>
            </td>
        </tr>  

        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL输入参数</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg3" class='easyui-datagrid'
                        style="width: 554px" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:true,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            toolbar:'#tb3'">
                        <thead>
                            <tr>
                                <th field="id" hidden="true">序列</th>
                                <th field="pid" hidden="true"></th>
                                <th field="name">名称</th>
                                <th field="sql_para_name">数据库字段</th>
                                <th field="type">条件</th>
                                <th field="is_necessary">是否必须</th>
                            </tr>
                        </thead>
                    </table>
                    <div id="tb3" style="height:35px">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add3()">新增</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit3()">修改</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele3()">删除</a>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL输出参数</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                   <table id="dg4" class='easyui-datagrid'
                        style="width: 554px" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:true,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            toolbar:'#dg4_tb'">
                        <thead>
                            <tr>
                                <th field="id" hidden="true">序列</th>
                                <th field="pid" hidden="true"></th>
                                <th field="name">名称</th>
                                <th field="type">类型</th>
                            </tr>
                        </thead>
                    </table>
                    <div id="dg4_tb" style="height:35px">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="addOutPutPara()">新增</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="editOutPutPara()">修改</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="deleOutPutPara()">删除</a>
                    </div>
                </div>
            </td>
        </tr>
   
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">输出排序(SQL order by desc or inc)</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <input type="text" id="arrange_str" class="easyui-textbox" data-options="width:554,height:100,multiline:true">
                </div>
            </td>
        </tr>  
    </table>
	
</body> 
</html>
<script type="text/javascript">
    var user = <%=userStr%>;
    var pid = <%=pid%>;
</script>
<script type="text/javascript">
    function add()
    {

        var row = $('#tt').treegrid('getSelected');  
        if(row == null)
        {
            layerMsgCustom('必须选择一个目录');
            return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['800px', '700px'],
            btn: '保存',
            content: 'form/addUrlPara.jsp?pid='+row.id,
            yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
                  var Ifame=window.frames[ifname]//得到框架
                Ifame.update();
            }
        })
    }


</script>
<script type="text/javascript">
    function update() {
        shuoheUtil.layerTopMaskOn();
        var url = new Object();
        url.id = 0;
        url.pid = pid;
        url.name = $('#name').textbox('getValue');
        url.descr = $('#descr').textbox('getValue');
        url.type = $('#type').combobox('getText');
        url.creater = user.actual_name;
        url.create_date = getSystemDate();
        url.sql_str = $('#sql_str').textbox('getValue');
        url.arrange_str = $('#arrange_str').textbox('getValue');

        var url_str = JSON.stringify(url);
        $.ajax({
            url: "<%=basePath%>/develop/url/save.do",
            type : "POST",
            dataType : 'json',
            data : {
                'url' : url_str,               
                'UrlIutputPara':getUrlIutputParaList(),
                'UrlOutputPara':getUrlOutputParaList()
            },
            error : function() //失败
            {
                shuoheUtil.layerTopMaskOff();
                shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success : function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();
                if(data.result == true)
                {
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                    parent.updateSuccess();
                    shuoheUtil.layerTopMsgOK('保存成功');
                }
                else
                {
                    shuoheUtil.layerTopMsgError(data.describe);
                }

            }
        });

    }

    function getUrlIutputParaList()
    {
        var rows = $("#dg3").datagrid("getRows");
        var l = new Array();
        for(var i=0;i<rows.length;i++)
        {
            rows[i].pid = pid;
            l.push(rows[i]);
        }
        return JSON.stringify(l);
    }
    function getUrlOutputParaList()
    {
        var rows = $("#dg4").datagrid("getRows");
        var l = new Array();
        for(var i=0;i<rows.length;i++)
        {
            rows[i].pid = pid;
            l.push(rows[i]);
        }
        return JSON.stringify(l);
    }

</script>
<script type="text/javascript">
    function addOutPutPara() {
        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['500px', '300px'],
            btn: '保存',
            content: 'form/addOutPutUrlPara.jsp',
            yes: function(index) {
                var ifname = "layui-layer-iframe" + index //获得layer层的名字
                var Ifame = window.frames[ifname]; //得到框架
                Ifame.update();
            }
        })
    }
    function editOutPutPara() {
        var row = $("#dg4").datagrid("getSelected");
        var dg_i = $('#dg4').datagrid('getRowIndex', $("#dg4").datagrid('getSelected'))

        if (row == null) {
            layerMsgCustom('请先选择一条数据');
            return;
        }
        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮 
            area: ['500px', '300px'],
            btn: '保存',
            content: 'form/editOutPutUrlPara.jsp?name='+row.name+'&type='+row.type+'&id='+dg_i,
            yes: function(index) {
                var ifname = "layui-layer-iframe" + index //获得layer层的名字
                var Ifame = window.frames[ifname]; //得到框架
                Ifame.update();
            }
        })
    }
    function deleOutPutPara() {

    }    

</script>
<script type="text/javascript">
  function add3()
  {
      layer.open({
          type: 2,
          title: '新建URL',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['800px', '400px'],
          btn: '保存',
          content: 'form/addUrlPara.jsp',
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname];//得到框架
            Ifame.updateDg3();
          }
      })
  }    
  function add4()
  {
      layer.open({
          type: 2,
          title: '新建URL',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['500px', '300px'],
          btn: '保存',
          content: 'form/addUrlPara.jsp',
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname];//得到框架
            Ifame.updateDg4();
          }
      })
  }   
    function edit3()
    {
        var row = $("#dg3").datagrid("getSelected");
        var dg_i = $('#dg3').datagrid('getRowIndex', $("#dg3").datagrid('getSelected'))

        if(row==null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['800px', '400px'],
            btn: '保存',
            content: 'form/editUrlPara.jsp?id=\''+dg_i+'\'&name=\''+row.name+'\'&type=\''+row.type+'\'',
            yes:function(index)
            {
                console.info(index);
                var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                Ifame.updateDg3();
            }
        })
    }    
  function edit4()
    {
        var row = $("#dg4").datagrid("getSelected");
        var dg_i = $('#dg4').datagrid('getRowIndex', $("#dg4").datagrid('getSelected'))
        
        if(row==null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['500px', '300px'],
            btn: '保存',
            content: 'form/editUrlPara.jsp?id=\''+dg_i+'\'&name=\''+row.name+'\'&type=\''+row.type+'\'',
            yes:function(index)
            {
                var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                Ifame.updateDg4();
            }
        })
    }   
    function dele3()
    {
        var dg_i = $('#dg3').datagrid('getRowIndex', $("#dg3").datagrid('getSelected'))
        $("#dg3").datagrid('deleteRow',dg_i)

    }
     function dele4()
    {
        var dg_i = $('#dg4').datagrid('getRowIndex', $("#dg4").datagrid('getSelected'))
        $("#dg4").datagrid('deleteRow',dg_i)
    }


</script>