<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>自定义数据表</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20">
            <thead>
                <tr href="#">                      
                <th field="id" align="center" hidden ="true">id</th>
                <th field="name" align="center">名称</th>
                <th field="url" align="center">连接地址</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
            <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit()">修改</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="test()">测试</a>
          </div>
       </div>
    </div>
    


</body>

<script type="text/javascript">
  
$(function() {
    loadDatagraid();
});

</script>
<script type="text/javascript">
    function loadDatagraid() {

        $('#dg').datagrid({
            url: '/develop/url/getUrl.do',
            queryParams: {
                name: 'getAllPageTemplate'
            },
            method: 'get',
            onLoadSuccess:function(data)
            {

            }
        })
    }
</script>
<script type="text/javascript">
    function add() {
        layer.open({
              type: 2,
              title: '新增',
              shadeClose: false,
              shade: 0.3,
              maxmin: true, //开启最大化最小化按钮s
              area: ['800px','600px'],
              content: 'form/addDateTemplate.jsp'
        })
    }
    function edit() {

        var row = $('#dg').datagrid('getSelected');
        if (row == null) {
            layerMsgCustom('必须选择一条数据');
            return;
        }

        layer.open({
            type: 2,
            title: '修改',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: ['800px','600px'],
            content: 'form/editDateTemplate.jsp?id=' + row.id
        })
    }    


</script>
<script type="text/javascript">
    function updateSuccess() {
        loadDatagraid();
    }

</script>