<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加表格模板</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- 图片上传样式 -->
    <link type="text/css" href="/TopJUI/upload/css/upload.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
    <!-- 图片上传js -->
    <script type="text/javascript" src="/TopJUI/upload/js/upload.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: auto;}
</style>



<body>
   <table class="editTable" style="text-align: center;" >
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">表信息</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">模板名称</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
         <tr>
            <td class="label">数据加载链接地址</td>
            <td colspan="3">
                <input id='url' class="easyui-textbox" style="width: 554px" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
         <tr>
            <td class="label">rowNumbers</td>
            <td >                
                <select id='rowNumbers' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">singleSelect</td>
            <td >                
                <select id='singleSelect' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>          
         <tr>
            <td class="label">autoRowHeight</td>
            <td >                
                <select id='autoRowHeight' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">pagination</td>
            <td >                
                <select id='pagination' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>   
         <tr>
            <td class="label">fitColumns</td>
            <td >                
                <select id='fitColumns' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">striped</td>
            <td >                
                <select id='striped' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>           
         <tr>
            <td class="label">checkOnSelect</td>
            <td >                
                <select id='checkOnSelect' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">selectOnCheck</td>
            <td >                
                <select id='selectOnCheck' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>     
         <tr>
            <td class="label">collapsible</td>
            <td >                
                <select id='collapsible' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>             


        
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">表头信息</span>
                </div>
            </td>
        </tr>         
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg3" class="easyui-datagrid" title="" style="width:554px;height:auto"
                        data-options="
                            iconCls: 'icon-edit',
                            singleSelect: true,
                            toolbar: '#tb',
                            method: 'get'
                        ">
                    <thead>
                    <tr>
                        <th field="id" hidden="true">编号</th>
                        <th field="template_id" hidden="true">template_id</th>
                        <th field="field">field</th>
                        <th field="title">title</th>
                        <th field="align">align</th>
                        <th field="width">width</th>
                        <th field="hidden">hidden</th>
                    </tr>
                    </thead>
                    </table>
                    <div id="tb" style="height:35px">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-check',plain:true" onclick="edit()">修改</a>
                    </div>
                </div>
            </td>
        </tr>

        <tr style="margin-top: 100px">
            <td></td>
            <td >
            </td>
            <td>
            </td>
            <td  style="text-align: right;margin-top: 100px">
                  <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-save',plain:true" onclick="save()" style="color:black;width:80px;height:auto;background-color: #00CD66">保存</a>
            </td>               
        </tr>          
    </table>
    
</body> 
</html>
<script type="text/javascript">

    var project = new Object();


    $(function(){

        $.ajax({
            url: '/develop/url/getUrl.do',
            type: "POST",
            dataType: 'json',
            data: {
                'name': 'getProjctForObjectById',
                'id':shuoheUtil.getUrlHeader().id
            },
            error: function() //失败
            {
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerSaveMaskOff();
                project = data;
                $('#project_id').textbox('setValue',data.project_id);
                $('#project_name').textbox('setValue',data.project_name);
            }
        });

    })


    function save() {

        shuoheUtil.layerTopMaskOn();

        var o = new Object();
        o.id = 0;
        o.name = $('#name').textbox('getValue');
        o.url = $('#url').textbox('getValue');
        o.rowNumbers = $('#rowNumbers').combobox('getValue');
        o.singleSelect = $('#singleSelect').combobox('getValue');
        o.autoRowHeight = $('#autoRowHeight').combobox('getValue');
        o.pagination = $('#pagination').combobox('getValue');
        o.fitColumns = $('#fitColumns').combobox('getValue');
        o.striped = $('#striped').combobox('getValue');
        o.checkOnSelect = $('#checkOnSelect').combobox('getValue');
        o.selectOnCheck = $('#selectOnCheck').combobox('getValue');
        o.collapsible = $('#collapsible').combobox('getValue');
        // o.toolbar = $('#toolbar').textbox('getValue');
        o.pageSize = 10;
        
        var rows = $('#dg3').datagrid('getRows');

        $.ajax({
            url:'/develop/pagetemplate/saveDatagraidAndColumns.do',
            type: "POST",
            dataType: 'json',
            data: {
                'datagraid': JSON.stringify(o),
                'datagraidColumns':JSON.stringify(rows)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    shuoheUtil.layerClose();
                    parent.updateSuccess();
                } else
                    shuoheUtil.layerMsgCustom(data.describe);
            }
        });
    }
</script>
<script type="text/javascript">


// var a.b = xx;
// // x.c();


// var a = function()
// {
//     var b = function()
//     {
//         var c = function()
//         {
//             console.info('c');
//         }
//     }   
// }


　// 作为对象方法，函数写法，这里创建了两个函数外面用{}包裹起来
// var Test = {
//     run1: function() {
//         // var x = new Object();
//         this.x1 = function() {
//             alert('这个必须放在一个对象内部，放在外边会出错！'); //这里是你函数的内容
//         }
//     },
//     run2: function() {
//         alert('这个必须放在一个对象内部，放在外边会出错！'); //这里是你函数的内容
//     }
// }
// //调用
// Test.run1().x1(); //调用第1个函数
// Test.run2(); //调用第2个函数

// var runx =
// {
//     x1:function()
//     {

//     }
// }
// var Test = function(){
//     this.run1 = new runx();
// }

// console.info(Test.run1);


// var util = {
//     math: {
//         random: {
//             a:function()
//             {
//                 console.info(33333);
//                 return '4444';
//             }
//         },
//         c:function()
//         {
//             console.info(55555);
//             return '6666';
//         }
//     }
// }
// console.log(util.math.c())
    
// var Easyui = {
//     Datagraid:function(){
//         deleteSeleectRow : function(datagraid_id)
//         {
//             var row = $('#'+datagraid_id).datagrid('getSelected')
//             if (row) {
//                 var rowIndex = $('#'+datagraid_id).datagrid('getRowIndex', row);
//                 $('#'+datagraid_id).datagrid('deleteRow',rowIndex);
//                 $('#'+datagraid_id).datagrid('autoSizeColumn');
//             }
//             else
//             {
//                 layerMsgCustom('必须选择一条数据');
//             }
//         }
//     }
// }

</script>
<script type="text/javascript">
    function add()
    {
          layer.open({
              type: 2,
              title: '新增表头',
              shadeClose: false,
              shade: 0.3,
              maxmin: true, //开启最大化最小化按钮s
              area: ['800px','300px'],
              content: 'form/addColumn.jsp'
          })
    }
    function edit()
    {

    }




    function dele()
    {
        console.info('dele');
        Easyui.Datagraid.deleteSeleectRow('dg3');
        // var row = $('#dg3').datagrid('getSelected')
        // if (row) {
        //     var rowIndex = $('#dg3').datagrid('getRowIndex', row);
        //     $('#dg3').datagrid('deleteRow',rowIndex);
        //     // $('#dg3').datagrid('reload');
        //     $('#dg3').datagrid('autoSizeColumn');
        // }
        // else
        // {
        //     layerMsgCustom('必须选择一条数据');
        // }
    }
</script>
<script type="text/javascript">
    
// var Easyui = function () {
//     Datagraid:function()
//     {
//         deleteSeleectRow:function(datagraid_id)
//         {
//             var row = $('#'+datagraid_id).datagrid('getSelected')
//             if (row) {
//                 var rowIndex = $('#'+datagraid_id).datagrid('getRowIndex', row);
//                 $('#'+datagraid_id).datagrid('deleteRow',rowIndex);
//                 $('#'+datagraid_id).datagrid('autoSizeColumn');
//             }
//             else
//             {
//                 layerMsgCustom('必须选择一条数据');
//             }
//         }
//     }
//     var Datagraid = function() {
//         Datagraid.deleteSeleectRow = function(datagraid_id) {
//             var row = $('#'+datagraid_id).datagrid('getSelected')
//             if (row) {
//                 var rowIndex = $('#'+datagraid_id).datagrid('getRowIndex', row);
//                 $('#'+datagraid_id).datagrid('deleteRow',rowIndex);
//                 $('#'+datagraid_id).datagrid('autoSizeColumn');
//             }
//             else
//             {
//                 layerMsgCustom('必须选择一条数据');
//             }
//         }
//     }
// }

</script>