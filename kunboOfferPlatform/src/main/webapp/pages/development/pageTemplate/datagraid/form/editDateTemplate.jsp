<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改表格模板</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- 图片上传样式 -->
    <link type="text/css" href="/TopJUI/upload/css/upload.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
    <!-- 图片上传js -->
    <script type="text/javascript" src="/TopJUI/upload/js/upload.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: auto;}
</style>



<body>
   <table class="editTable" style="text-align: center;" >
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">表信息</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">模板名称</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
         <tr>
            <td class="label">数据加载链接地址</td>
            <td colspan="3">
                <input id='url' class="easyui-textbox" style="width: 554px" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
         <tr>
            <td class="label">rowNumbers</td>
            <td >                
                <select id='rowNumbers' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">singleSelect</td>
            <td >                
                <select id='singleSelect' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>          
         <tr>
            <td class="label">autoRowHeight</td>
            <td >                
                <select id='autoRowHeight' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">pagination</td>
            <td >                
                <select id='pagination' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>   
         <tr>
            <td class="label">fitColumns</td>
            <td >                
                <select id='fitColumns' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">striped</td>
            <td >                
                <select id='striped' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>           
         <tr>
            <td class="label">checkOnSelect</td>
            <td >                
                <select id='checkOnSelect' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>

            <td class="label">selectOnCheck</td>
            <td >                
                <select id='selectOnCheck' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>     
         <tr>
            <td class="label">collapsible</td>
            <td >                
                <select id='collapsible' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value=true>true</option>  
                    <option value=false>false</option>         
                </select>
            </td>                
            </td>
        </tr>             


        
        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">表头信息</span>
                </div>
            </td>
        </tr>         
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg3" class="easyui-datagrid" title="" style="width:554px;height:auto"
                        data-options="
                            iconCls: 'icon-edit',
                            singleSelect: true,
                            toolbar: '#tb',
                            method: 'get'
                        ">
                    <thead>
                    <tr>
                        <th field="id" hidden="true">编号</th>
                        <th field="template_id" hidden="true">template_id</th>
                        <th field="field">field</th>
                        <th field="title">title</th>
                        <th field="align">align</th>
                        <th field="width">width</th>
                        <th field="hidden">hidden</th>
                    </tr>
                    </thead>
                    </table>
                    <div id="tb" style="height:35px">
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-check',plain:true" onclick="edit()">修改</a>
                    </div>
                </div>
            </td>
        </tr>

        <tr style="margin-top: 100px">
            <td></td>
            <td >
            </td>
            <td>
            </td>
            <td  style="text-align: right;margin-top: 100px">
                  <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-save',plain:true" onclick="save()" style="color:black;width:80px;height:auto;background-color: #00CD66">保存</a>
            </td>               
        </tr>          
    </table>
    
</body> 
</html>
<script type="text/javascript">

    var project = new Object();


    $(function(){

        $.ajax({
            url: '/develop/url/getUrl.do',
            type: "POST",
            dataType: 'json',
            data: {
                'name': 'getPageTemplateById',
                'id':shuoheUtil.getUrlHeader().id
            },
            error: function() //失败
            {
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerSaveMaskOff();
                project = data;

                $('#name').textbox('setValue',data.name);
                $('#url').textbox('setValue',data.url);
                $('#rowNumbers').combobox('setValue',data.rowNumbers);
                $('#singleSelect').combobox('setValue',data.singleSelect);
                $('#autoRowHeight').combobox('setValue',data.autoRowHeight);
                $('#pagination').combobox('setValue',data.pagination);
                $('#fitColumns').combobox('setValue',data.fitColumns);
                $('#striped').combobox('setValue',data.striped);
                $('#checkOnSelect').combobox('setValue',data.checkOnSelect);
                $('#selectOnCheck').combobox('setValue',data.selectOnCheck);
                $('#collapsible').combobox('setValue',data.collapsible);
                loadDatagraid();
            }
        });

    })
    function loadDatagraid() {
        $('#dg3').datagrid({
            url: '/develop/url/getUrl.do',
            queryParams: {
                name: 'getDatagraidColumnByTemplateId',
                template_id:project.id
            },
            method: 'get',
            onLoadSuccess:function(data)
            {

            }
        })
    }

    function save() {

        shuoheUtil.layerTopMaskOn();

        project.name = $('#name').textbox('getValue');
        project.url = $('#url').textbox('getValue');
        project.rowNumbers = $('#rowNumbers').combobox('getValue');
        project.singleSelect = $('#singleSelect').combobox('getValue');
        project.autoRowHeight = $('#autoRowHeight').combobox('getValue');
        project.pagination = $('#pagination').combobox('getValue');
        project.fitColumns = $('#fitColumns').combobox('getValue');
        project.striped = $('#striped').combobox('getValue');
        project.checkOnSelect = $('#checkOnSelect').combobox('getValue');
        project.selectOnCheck = $('#selectOnCheck').combobox('getValue');
        project.collapsible = $('#collapsible').combobox('getValue');
        // project.toolbar = $('#toolbar').textbox('getValue');
        project.pageSize = 10;
        
        var rows = $('#dg3').datagrid('getRows');

        $.ajax({
            url:'/develop/pagetemplate/updateDatagraidAndColumns.do',
            type: "POST",
            dataType: 'json',
            data: {
                'datagraid': JSON.stringify(project),
                'datagraidColumns':JSON.stringify(rows)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    shuoheUtil.layerClose();
                    parent.updateSuccess();
                } else
                    shuoheUtil.layerMsgCustom(data.describe);
            }
        });
    }
</script>
<script type="text/javascript">
    function add()
    {
          layer.open({
              type: 2,
              title: '新增表头',
              shadeClose: false,
              shade: 0.3,
              maxmin: true, //开启最大化最小化按钮s
              area: ['800px','300px'],
              content: 'form/addColumn.jsp'
          })
    }
    function edit()
    {

    }
    function dele()
    {

    }
</script>