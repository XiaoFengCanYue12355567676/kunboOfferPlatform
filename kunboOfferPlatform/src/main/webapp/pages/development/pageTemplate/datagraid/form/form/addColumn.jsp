<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增表头</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- 图片上传样式 -->
    <link type="text/css" href="/TopJUI/upload/css/upload.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
    <!-- 图片上传js -->
    <script type="text/javascript" src="/TopJUI/upload/js/upload.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: auto;}
</style>



<body>
   <table class="editTable" style="text-align: center;" >
        <tr>
            <td class="label">field</td>
            <td >
                <input id='field' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
            <td class="label">title</td>
            <td >
                <input id='title' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
        <tr>
            <td class="label">align</td>
            <td >
                <select id='align' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value="center">center</option>       
                    <option value='left'>left</option>  
                    <option value="right">right</option>         
                </select>                     
            </td>
            <td class="label">width</td>
            <td >
                <input id='width' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
        <tr>
            <td class="label">hidden</td>
            <td >
                <select id='hidden' class="easyui-combobox" data-options="required:false,width:200,editable:true">
                    <option value='true'>true</option>  
                    <option value="false">false</option>         
                </select>                
            </td>
            <td>
            </td>            
        </tr>                
         
        <tr style="margin-top: 100px">
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td  style="text-align: right;margin-top: 100px">
                  <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-save',plain:true" onclick="save()" style="color:black;width:80px;height:auto;background-color: #00CD66">确定</a>
            </td>               
        </tr>          
    </table>
    
</body> 
</html>
<script type="text/javascript">

$(function(){
    $('#developer').combobox({
        url: '/system/user/getAllSelectUser.do',
        valueField: 'id',
        textField: 'name',
        panelHeight: 200,
        method: 'get',
        multiple: false,
        queryParams: {
            // 'name': 'getCompanyForList'
        },
        onLoadSuccess: function() {

        },
        onSelect: function(ret) {}
    });
});
function save()
{
    parent.$('#dg3').datagrid('appendRow',{
        id: 0,
        field: $('#field').textbox('getValue'),
        title: $('#title').textbox('getValue'),
        align: $('#align').combobox('getValue'),
        width: $('#width').textbox('getValue'),
        hidden: $('#hidden').combobox('getValue'),
    });
    parent.$('#dg3').datagrid('autoSizeColumn');
    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    parent.layer.close(index); //再执行关闭
}


</script>