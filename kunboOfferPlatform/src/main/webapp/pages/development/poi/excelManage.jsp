<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>Excel导出管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">           

        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                  idField:'id',
                  treeField:'text',
                  method:'get'
                ">
          <thead>
              <tr>
                  <th field="text" width="100%">项目</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20">
            <thead>
              <tr href="#">
                         
                <th field="id" align="center" hidden ="true">id</th>
                <th field="pid" align="center">pid</th>
                <th field="name" align="center">名称</th>
                <th field="descr" align="center">描述</th>
                <th field="fileName" align="center">文件名称</th>
                <th field="creater" align="center">创建人</th>
                <th field="create_date" align="center">创建时间</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="view()">查看</a>
            <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
            <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit()">修改</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
            <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="test()">测试</a>
          </div>
       </div>
    </div>
    



  <script type="text/javascript">
     $('#tt').treegrid({
         url:'<%=basePath%>/topJUI/index/getMenuList.do',
         onClickRow: function (rowIndex)  
         { 
            var rr_row = $('#tt').treegrid('getSelected');  
            $('#dg').datagrid({
                url:'<%=basePath%>/develop/url/poi/getByPid.do',
                queryParams:
                {
                  pid:rr_row.id
                },
                method:'get'
            })
         },
         onLoadSuccess:function(data)
         {  
         }  
      })
	</script>
  



</body> 
</html>
<script type="text/javascript">
  function add() {

    var row = $('#tt').treegrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.open({
      type: 2,
      title: '新建URL',
      shadeClose: true,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: shuoheUtil.cpm_size(),
      btn: '保存',
      content: 'form/addUrl.jsp?pid=' + row.id,
      yes: function(index) {
        var ifname = "layui-layer-iframe" + index //获得layer层的名字
        var Ifame = window.frames[ifname] //得到框架
        Ifame.update();
      }
    })
  }

  function view() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.open({
      type: 2,
      title: '新建URL',
      shadeClose: true,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: shuoheUtil.cpm_size(),
      content: 'form/viewUrl.jsp?id=' + row.id
    })
  }


  function edit() {

    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.open({
      type: 2,
      title: '修改目录',
      shadeClose: true,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: shuoheUtil.cpm_size(),
      btn: '保存',
      content: 'form/editUrl.jsp?id=' + row.id,
      yes: function(index) {
        var ifname = "layui-layer-iframe" + index //获得layer层的名字
        var Ifame = window.frames[ifname] //得到框架
        Ifame.update();
      }
    })
  }

  function dele() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }
    shuoheUtil.layerTopMaskOn();
    var json_str = JSON.stringify(row);
    $.ajax({
      url: "<%=basePath%>/develop/url/poi/delete.do",
      type: "POST",
      dataType: 'json',
      data: {
        'id': row.id
      },
      error: function() //失败
      {
        shuoheUtil.layerTopMaskOff();
      },
      success: function(data) //成功
      {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
        parent.layer.close(index); //再执行关闭
        $('#dg').datagrid('reload');
        shuoheUtil.layerTopMaskOff();
      }
    });
  }

  function test() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个下载列');
      return;
    }
    var url = "/develop/url/poi/getExcelByName.do";
    url += '?name=' + row.name;
    window.open(url);
  }

  function updateSuccess() {
    $('#dg').datagrid('reload');

  }

</script>

