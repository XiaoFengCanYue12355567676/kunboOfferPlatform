<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    PrintWriter ss = response.getWriter();
    String id = request.getParameter("id"); 
    String name = request.getParameter("name"); 
    String type = request.getParameter("type"); 
    String sql_para_name = request.getParameter("sql_para_name"); 
    User user = null;
    String userStr = null;
    try
    {

        user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
            //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userStr = Json.toJsonByPretty(user);
    }
        catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加URL</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>



<body>
     <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">参数设置</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">名称</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>
        <tr>
            <td class="label">数据库字段名</td>
            <td >
                <input id='sql_para_name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>             
        <tr>
            <td class="label">条件</td>
            <td >
                <select id="type" class="easyui-combobox" data-options="required:false,width:200,editable:true">  
                    <option value="date>=">date>=</option>  
                    <option value="date<=">date<=</option>  
                    <option value=">">></option>  
                    <option value="<"><</option>  
                    <option value="=">=</option>  
                    <option value="like">like</option>  
                </select>        
            </td>       
            <td class="label">是否必须输入</td>
            <td >
                <select id="is_necessary" class="easyui-combobox" data-options="required:false,width:200,editable:true">  
                    <option value=false>false</option>  
                    <option value=true>true</option>  
                </select>        
            </td>        
        </tr>

    </table>
    
</body> 
</html>
<script type="text/javascript">
    var user = <%=userStr%>;
    var id = <%=id%>;
</script>
<script type="text/javascript">

    $(function(){
        $('#name').textbox('setValue',<%=name%>);
        $('#type').textbox('setValue',<%=type%>);
        $('#sql_para_name').textbox('setValue',<%=sql_para_name%>);
    })


 function updateDg3() {
        parent.$('#dg3').datagrid('updateRow',{
            index: id,
            row: {
                name: $('#name').textbox('getValue'),
                type: $('#type').textbox('getValue'),
                sql_para_name: $('#sql_para_name').textbox('getValue'),
                is_necessary:$('#is_necessary').textbox('getValue')
            }
        });
        parent.$('#dg3').datagrid('autoSizeColumn');    
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭

    }
</script>