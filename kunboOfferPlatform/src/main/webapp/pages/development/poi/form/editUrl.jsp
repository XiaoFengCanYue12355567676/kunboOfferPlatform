
<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    PrintWriter ss = response.getWriter();
    String id = request.getParameter("id"); 
    User user = null;
    String userStr = null;
    try
    {

        user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
            //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userStr = Json.toJsonByPretty(user);
    }
        catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>添加URL</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>



<body>
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL概览</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">接口名(英文)</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
            <td class="label">汉语简称</td>
            <td >
                <input id='descr' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
            </td>
        </tr>
        <tr>
            <td class="label">导出文件名</td>
            <td >
                <input id='fileName' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>


        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">SQL语句</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <input type="text" id="sql_str" class="easyui-textbox" data-options="width:554,height:300,multiline:true">
                </div>
            </td>
        </tr>  

        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">Excel导出表格表头设置</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg4" class='easyui-datagrid'
                        style="width: 554px" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:false,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            toolbar:'#tb4'">
                        <thead>
                            <tr>
                                <th field="id" hidden="true">序列</th>
                                <th field="pid" hidden="true"></th>
                                <th field="header_str">表头名称</th>
                                <th field="key_str">对应数据库字段名</th>
                            </tr>
                        </thead>
                    </table>
                    <div id="tb4" style="height:35px">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add4()">新增</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit4()">修改</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele4()">删除</a>
                    </div>
                </div>
            </td>
        </tr> 



        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">URL输入参数</span>
                </div>
            </td>
        </tr>  
        <tr>
            <td colspan="4">
                <div class="easyui-panel" style="width: 700px; margin-left: 143px; margin-top: 0px" data-options=" border:false">
                    <table id="dg3" class='easyui-datagrid'
                        style="width: 554px" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:false,
                            fitColumns:false,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            toolbar:'#tb3'">
                        <thead>
                            <tr>
                               <th field="id" hidden="true">序列</th>
                                <th field="pid" hidden="true"></th>
                                <th field="name">名称</th>
                                <th field="sql_para_name">数据库字段</th>
                                <th field="type">条件</th>
                                <th field="is_necessary">是否必须</th>
                            </tr>
                        </thead>
                    </table>
                    <div id="tb3" style="height:35px">
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add3()">新增</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit3()">修改</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele3()">删除</a>
                    </div>
                </div>
            </td>
        </tr>
       
    </table>
	
</body> 
</html>
<script type="text/javascript">
    var user = <%=userStr%>;
    var id = <%=id%>;

    var url = null;

    $(function(){
        addRules();
        $.ajax({
            url: "<%=basePath%>/develop/url/poi/findUrlById.do",
            type : "POST",
            dataType : 'json',
            data : {
                'id': id        
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                url = data;
                $('#name').textbox('setValue',data.name);
                $('#descr').textbox('setValue',data.descr);
                $('#fileName').textbox('setValue',data.fileName);
                console.info(data.descr);              
            }
        });
        $.ajax({
            url: "<%=basePath%>/develop/url/poi/findUrlSqlById.do",
            type : "POST",
            dataType : 'text',
            data : {
                'id': id        
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                $('#sql_str').textbox('setValue',data);
            }
        });
        $('#dg3').datagrid({
            url:'<%=basePath%>/develop/url/poi/findUrlPoiIutputParaByPid.do',
            queryParams:
            {
                pid:id
            }
        })      
        $('#dg4').datagrid({
            url:'<%=basePath%>/develop/url/poi/findUrlPoiHeaderByPid.do',
            queryParams:
            {
                pid:id
            }
        })           
    });
</script>
<script type="text/javascript">
    function update() {
        shuoheUtil.layerTopMaskOn();
        url.name = $('#name').textbox('getValue');
        url.descr = $('#descr').textbox('getValue');
        url.fileName = $('#fileName').textbox('getValue');
        url.creater = user.actual_name;
        url.create_date = getSystemDate();
        url.sql_str = $('#sql_str').textbox('getValue');
        var url_str = JSON.stringify(url);
        $.ajax({
            url: "<%=basePath%>/develop/url/poi/update.do",
            type : "POST",
            dataType : 'json',
            data : {
                'url' : url_str,               
                'UrlPoiIutputPara':getUrlPoiHeaderList(),
                'UrlPoiHeader':getUrlPoiIutputParaList()
            },
            error : function() //失败
            {
                shuoheUtil.layerTopMaskOff();
            },
            success : function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();   
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                parent.layer.close(index); //再执行关闭
                parent.updateSuccess();
            }
        });

    }

    function getUrlPoiHeaderList()
    {
        var rows = $("#dg3").datagrid("getRows");
        var l = new Array();
        for(var i=0;i<rows.length;i++)
        {
            rows[i].pid = 0;
            l.push(rows[i]);
        }
        return JSON.stringify(l);
    }

    function getUrlPoiIutputParaList()
    {
        var rows = $("#dg4").datagrid("getRows");
        var l = new Array();
        for(var i=0;i<rows.length;i++)
        {
            rows[i].pid = 0;
            l.push(rows[i]);
        }
        return JSON.stringify(l);
    }
   
</script>
<script type="text/javascript">
  function add3()
  {
      layer.open({
          type: 2,
          title: '新建URL',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/addUrlPara.jsp',
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname];//得到框架
            Ifame.updateDg3();
          }
      })
  }    
  
    function edit3()
    {
        var row = $("#dg3").datagrid("getSelected");
        var dg_i = $('#dg3').datagrid('getRowIndex', $("#dg3").datagrid('getSelected'))

        if(row==null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            btn: '保存',
            content: 'form/editUrlPara.jsp?id=\''+dg_i+'\'&header_str=\''+row.header_str+'\'&key_str=\''+row.key_str+'\'',
            yes:function(index)
            {
                console.info(index);
                var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                Ifame.updateDg3();
            }
        })
    }    
  
    function dele3()
    {
        var dg_i = $('#dg3').datagrid('getRowIndex', $("#dg3").datagrid('getSelected'))
        $("#dg3").datagrid('deleteRow',dg_i)

    }
    function add4()
    {
        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            btn: '保存',
            content: 'form/addUrlPoiHeader.jsp',
            yes:function(index){
                var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                Ifame.updateDg4();
            }
        })
    }
    function edit4()
    {
        var row = $("#dg4").datagrid("getSelected");
        var dg_i = $('#dg4').datagrid('getRowIndex', $("#dg4").datagrid('getSelected'));

        if(row==null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
            type: 2,
            title: '新建URL',
            shadeClose: true,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            btn: '保存',
            content: 'form/editUrlPoiHeader.jsp?id='+dg_i,
            yes:function(index)
            {
                console.info(index);
                var ifname="layui-layer-iframe"+index//获得layer层的名字
                var Ifame=window.frames[ifname];//得到框架
                Ifame.updateDg4();
            }
        })        
    }
    function dele4() {
        var dg_i = $('#dg4').datagrid('getRowIndex', $("#dg4").datagrid('getSelected'))
        $("#dg4").datagrid('deleteRow', dg_i)
    }
    
    function addRules() {
        shuoheUtil.layerTopMaskOn();
        var obj = $('.easyui-datagrid').each(function() {
        });
        console.info(obj);
        var size = obj.length;
        var count = 0;
        for(var i=0;i<obj.length;i++)
        {
            console.info(obj[i].id);
            $('#' + obj[i].id).datagrid({
                onLoadSuccess:function () {
                    console.info('onLoadSuccess');
                    count++;
                    if(count == size)
                    {
                        shuoheUtil.layerTopMaskOff();
                    }
                }
            });
        }
    }
</script>