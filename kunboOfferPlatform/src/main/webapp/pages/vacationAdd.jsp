<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
	xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
<head lang="zh-CN">
<meta charset="UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta http-equiv="Access-Control-Allow-Origin" content="localhost:8080" />
<title>请假申请</title>
</head>
<link
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	rel="stylesheet" />
<link
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="https://cdn.bootcss.com/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
<link href="/lib/layer/theme/default/layer.css" rel="stylesheet" />



<script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.19.4/moment.min.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.19.4/moment-with-locales.min.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.19.4/locale/zh-cn.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<script src="/lib/layer/layer.js"></script>
<script src="/js/common.js"></script>
<body>
	<div class="container">
		<form id="formList" action="" method="post">
			<div class="form-group">
				<label for="lastName">申请人姓名</label> 
				<input type="text" class="form-control" id="lastName" name="lastName" value="${sessionScope.user.firstName}"/>
				<input type="text" class="form-control" id="userId" name="userId" value="${sessionScope.user.id}"/>
			</div>
			<div class="form-group" >
				<label for="startDate">开始日期</label> 
				<input type="text" id="startDate" class="form-control floating-label" name="startDate"/>
			</div>
			<div class="form-group">
				<label for="passwd">结束日期</label> 
				<input type="text" class="form-control floating-label" id="endDate" name="endDate" />
			</div>
			<div class="form-group">
				<label for="days">天数</label> 
				<input type="number" class="form-control" id="days" name="days" placeholder="1" />
			</div>
			<div class="form-group">
				<label for="vacationType">请假类型</label> 
				<select class="form-control" id="vacationType" name="vacationType">
					<option>--情选择--</option>
				</select>
			</div>
			<div class="form-group">
				<label for="reason">请假原因</label>
				<textarea class="form-control" rows="3" id="reason" name="reason"></textarea>
			</div>
		</form>
		<button class="btn btn-default" id="save" type="submit"
			onclick="addForm()">提交</button>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			getBaseData('#vacationType', 'M01');
			$(".floating-label").bootstrapMaterialDatePicker(); 
		})
		
		function addForm() {
			$.ajax({
				url : "/vacation/startVac",
				type : "post",
				data : $('#formList').serialize(),
				success : function(result) {
					if (result.status == 0)
						layer.msg(result.msg);
					else
						layer.msg("创建失败");
				},
				error : function(e) {
					alert("错误！！");
					window.clearInterval(timer);
				}
			});
		}
	</script>
</body>
</html>