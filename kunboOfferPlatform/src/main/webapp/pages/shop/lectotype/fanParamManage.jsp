<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>原材料管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <script type="text/javascript" src="/pages/js/materials/materialsManage.js"></script>
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20,
                  onDblClickRow:onDblClickRow">
            <thead>
              <tr href="#">
                         
                <th field="id"  hidden ="true">uuid</th>
                <th field="productNumber" width="49%">风机编号</th>
                <th field="text" width="49%">风机名称</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="addParam()">修改参数</a>
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-cloud-upload',plain:true" onclick="openImportData()">导入</a>
          </div>
       </div>
    </div>
</body> 
</html>
<script type="text/javascript">
$(function(){
	$('#dg').datagrid({
	    url:'/develop/url/getUrl.do',
	    queryParams:
	    {
	      name:'getAllFanList'
	    },
	    method:'post'
	});
});
  
  
  function addParam()
  {
      var row = $('#dg').datagrid('getSelected');  
        if(row == null)
        {
          layerMsgCustom('必须选择一个风机');
          return;
        }

        layer.open({
            type: 2,
            title: '修改风机参数',
            shadeClose: false,
            shade: 0.3,
            maxmin: true, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            content: 'addParameter.jsp?fanId='+row.id+'&fanNumber='+row.productNumber
        })
  }
  // 打开导入
  function openImportData () {
    layer.open({
        type: 2,
        title: '导入',
        shadeClose: false,
        shade: 0.3,
        maxmin: true, //开启最大化最小化按钮
        area: ['600px', '500px'],
        content: 'importData.jsp'
    })
  }

  function edit()
  {

      var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/editUrl.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.msg('你确定删除么？', {
      time: 0, //不自动关闭
      btn: ['确定', '取消'],
      yes: function(index) {
        layer.close(index);
        shuoheUtil.layerTopMaskOn();
        $.ajax({
          url: "/develop/url/delete.do",
          type: "POST",
          dataType: 'json',
          data: {
            'id': row.id
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
              shuoheUtil.layerMsgSaveOK(data.describe);
            } else {
              shuoheUtil.layerMsgCustom(data.describe);
            }
            $('#dg').datagrid('reload');
          }
        });
      }
    });
  }


  function updateSuccess()
  {
    $('#dg').datagrid('reload'); 

  }

  function run()
  {
      var row = $('#dg').datagrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个URL');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'form/runUrl.jsp?name='+row.name        
      })
  }
  //datagrid双击事件
  function onDblClickRow(rowIndex, rowData){
	  layer.open({
          type: 2,
          title: '新增风机参数',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'addParameter.jsp?fanId='+rowData.id+'&fanNumber='+rowData.productNumber
      })
  }

</script>

