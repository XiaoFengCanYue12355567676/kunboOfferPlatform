<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path; 
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"></meta>
<title>风机图片上传页面</title>
<!-- 引入文件上传的css -->
<link href="<%=basePath%>/pages/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>/pages/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=basePath%>/custom/jquery.min.js"></script>
<!--引入JS-->
<script type="text/javascript" src="<%=basePath%>/pages/fileUpload/js/fileUpload.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>
<style>
.fileUp-con{
    float: left;
    width: 25%;
}
.fileUp-title{
    text-align: center;
    line-height: 2;
    font-size: 16px;
}
</style>
</head>
<body>
<div class="fileUp-con"><div id="fileUploadContent" class="fileUploadContent"></div><div class="fileUp-title">空气略图</div></div>
<div class="fileUp-con"><div id="fileUploadContent1" class="fileUploadContent"></div><div class="fileUp-title">安装简图</div></div>
<div class="fileUp-con"><div id="fileUploadContent2" class="fileUploadContent"></div><div class="fileUp-title">产品介绍</div></div>
<div class="fileUp-con"><div id="fileUploadContent3" class="fileUploadContent"></div><div class="fileUp-title">其他信息</div></div>
<!-- <br/> -->
<!-- <button onclick="testUpload()">函数提交</button> -->
<!-- <button id="selfUploadBt">设定id提交</button> -->
<!-- <button onclick="tt()">显示错误</button> -->
<!-- <br/> -->
<!-- <h5><a href="doc.html">文档说明</a> -->
</body>
</html>
<script type="text/javascript">
// 空气略图上传初始化
$("#fileUploadContent").initUpload({
    "uploadUrl":"/knowledgeFile/upload.do",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":true,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
    "beforeUpload":beforeUploadFun,//在上传前执行的函数
    "onUpload":onUploadFun,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

});
// 安装简图上传初始化
$("#fileUploadContent1").initUpload({
    "uploadUrl":"/knowledgeFile/upload.do",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":true,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
    "beforeUpload":beforeUploadFun1,//在上传前执行的函数
    "onUpload":onUploadFun1,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

});
// 产品介绍上传初始化
$("#fileUploadContent2").initUpload({
    "uploadUrl":"/knowledgeFile/upload.do",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":true,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
    "beforeUpload":beforeUploadFun2,//在上传前执行的函数
    "onUpload":onUploadFun2,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

});
// 其他信息上传初始化
$("#fileUploadContent3").initUpload({
    "uploadUrl":"/knowledgeFile/upload.do",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":true,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
    "beforeUpload":beforeUploadFun3,//在上传前执行的函数
    "onUpload":onUploadFun3,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

});
function beforeUploadFun(opt){
    opt.otherData =[{"name":"张三"},{"age":23}];
    return true;
}
// 空气略图上传成功
function onUploadFun(opt,data){
    //写入硬盘成功后保存到数据库
    console.log(JSON.stringify(data));
    //uploadTools.uploadError(opt);//显示上传错误
}
// 安装简图上传成功
function onUploadFun1(opt,data){
    console.log(JSON.stringify(data));
}
// 产品介绍上传成功
function onUploadFun2(opt,data){
    console.log(JSON.stringify(data));
}
// 其他信息上传成功
function onUploadFun3(opt,data){
    console.log(JSON.stringify(data));
}
function testUpload(){
    var opt = uploadTools.getOpt("fileUploadContent");
    uploadEvent.uploadFileEvent(opt);
}
function tt() {
    var opt = uploadTools.getOpt("fileUploadContent");
    uploadTools.uploadError(opt);//显示上传错误
}

//显示文件，设置删除事件
//uploadTools.showFileResult("fileUploadContent","img/a2.png","1",true,deleteFileByMySelf);
//如果不需要删除
//uploadTools.showFileResult("fileUploadContent","img/a3.png","1",false);
//多文件需要自己进行循环
function deleteFileByMySelf(fileId){
    alert("要删除文件了："+fileId);
}
</script>