<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改参数</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <table id="dg" style="width:100%;height:600px">
    <thead>
      <tr>
        <th field="title" width="33%">字段名</th>
        <th field="text" width="33%">字段标题</th>
        <th field="fieldType" width="33%">字段类型</th>
      </tr>
    </thead>
  </table>
  <div id="tb" style="padding:0px 30px;height: 35px">
    <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="paramAdd()">新增</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="paramModify()">修改</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="paramDelete()">删除</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-floppy-o" onclick="save()">保存</a>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var fields = []
var result
var resultJson = ''
$(function(){
  // 初始化数据
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'shop_fan_other_param'
    },
    error: function() //失败
    {
      //messageloadError()
    },
    success: function(data)//成功
    {
      console.log(data)
      result = data
      fields = result.field
    }
  })
  init()
})
function init () {
  //表格初始化
  $('#dg').datagrid({
    data:fields,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:true,
    fitColumns:true,
    striped:true,
    toolbar:'#tb'
  })
}
function paramAdd() {
  layer.open({
    type: 2,
    title: '添加参数',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['600px', '300px'],
    btn: '保存',
    content: 'editParam.jsp?method=add',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
function paramDelete() {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条字段')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        var index = $('#dg').datagrid('getRowIndex', row)
        $('#dg').datagrid('deleteRow', index)
      }
    })
  }
}
function paramModify() {
  var row = $('#dg').datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '修改参数',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['600px', '300px'],
    btn: '保存',
    content: 'editParam.jsp?method=edit',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
function editParamDialogCallBackSuccess (info_str, method) {
  var obj = JSON.parse(info_str)
  if (method == 'add') {
    $('#dg').datagrid('appendRow', obj)
  } else if (method == 'edit') {
    var row = $('#dg').datagrid('getSelected')
    var index = $('#dg').datagrid('getRowIndex', row)
    $('#dg').datagrid('updateRow', {
      index: index,
      row: obj
    })
  }
  messageSaveOK()
}
// 保存
function save () {
  var obj = new Object()
  obj.title = 'shop_fan_other_param'
  obj.name = '风机其他参数'
  obj.type = '0'
  obj.columnNumber = '3'
  var temp = fieldsToObject1(fields, false)
  obj.field = temp
  //console.log(obj.jsCode)
  var info_str = JSON.stringify(obj)
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/creatTable.do",
    type: "POST",
    dataType:'text',
    data:
    {
      "jsonStr": info_str
    },
    error: function(e) //失败
    {
    console.info("异常="+JSON.stringify(e));
    console.info("异常");
      messageSaveError();
    },
    success: function(data)//成功
    {
    	console.log(data);
      data = $.trim(data)
      var flag = data ==="true" ? true : false
      if(flag){
    	  $('#dg').datagrid('reload');
    	  shuoheUtil.layerMsgSaveOK('保存成功');
      }else{
        messageSaveError();
      }
    }
  })
}
</script>

