<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.shuohe.entity.system.user.User"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = UUID.randomUUID().toString();
	User user = null;
	String userJson = null;
	try
	{
	    user = (User)session.getAttribute("user");
	    if(user==null)
	    {
	      System.out.println("user = null");
	      String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    }
	    else
	    {
	        userJson = Json.toJsonByPretty(user);
	    }
	}
	catch(Exception e)
	{
	    String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    e.printStackTrace();
	}
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增风机参数</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>
    <link type="text/css" href="/pages/css/add-form.css" rel="stylesheet"/>  
    <!-- 文件上传 -->
    <link href="<%=basePath%>/pages/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>
    <link href="<%=basePath%>/pages/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">  
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
	<script type="text/javascript" src="/pages/js/jeDate/jedate.min.js"></script>
	<script type="text/javascript" src="/pages/js/add-form.js"></script>
	<script type="text/javascript" src="/pages/js/echarts.min.js"></script>
	<!-- echarts导出pdf所需js -->
	<script type="text/javascript" src="/pages/js/html2canvas.min.js"></script>
	<script type="text/javascript" src="/pages/js/jspdf.min.js"></script>
	
	<!--引入文件上传JS-->
	<script type="text/javascript" src="<%=basePath%>/pages/fileUpload/js/fileUpload.js"></script>
  <script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>
  
  <script type="text/javascript" src="/pages/js/drag.js"></script>
<style type="text/css">
html, body{ margin:0; height:100%;width: 100%}
.fileUp-con{
float: left;
width: 25%;
}
.fileUp-title{
    text-align: center;
    line-height: 2;
    font-size: 16px;
}
.datagrid-body{
	overflow: hidden;
}
.img-btn-con{
  position: absolute;
  top: 5px;
  left: 5px;
  z-index: 10;
  border: 2px solid #333;
}
.img-btn-con a{
  display: inline-block;
  width: 40px;
  height: 37px;
  line-height: 37px;
  text-align: center;
  cursor: pointer;
  background-color: #fff;
  padding-top: 3px;
}
.img-btn-con a .fa{
  font-size: 28px;
}
</style>
<body class="easyui-layout">
 <div class="easyui-tabs" style="height: 100%;overflow-x:hidden">
  <div title="性能曲线" style="position: relative;overflow-x:hidden">
	   <div style="position: absolute;right: 50px;top: 13px;z-index: 9999;">
	      <button onclick="convertCanvasToImage()" class="button button-primary button-rounded button-small" style="width: 150px;height: 35px;font-size: 20px;">导出pdf</button>
	   </div>
	<div id="main" style="float:left;width:50%;height:400px;margin-top: 50px;"></div>
	<div id="main1" style="float:left;width:50%;height:400px;margin-top: 50px;"></div>
	<div id="main2" style="float:left;width:50%;height:400px;margin-top: 50px;"></div>
  <div id="main4" style="float:left;width:50%;height:400px;margin-top: 50px;margin-bottom: 100px;"></div>
  <div class="clear"></div>
	<div style="position: fixed;left: 0;right: 0;bottom: 0;background-color: #fff;">
		<div class="field-form-line">
			<label>流量：</label><input type="text" id="xData" class="easyui-textbox" data-options="width:278">
		</div>
		<div class="field-form-line">
			<label>效率：</label><input type="text" id="yData1" class="easyui-textbox" data-options="width:278">
		</div>
		<div class="field-form-line">
			<label>内功率：</label><input type="text" id="yData2" class="easyui-textbox" data-options="width:278">
		</div>
		<div class="field-form-line">
			<label>全压：</label><input type="text" id="yData3" class="easyui-textbox" data-options="width:278">
		</div>
	</div>
   </div>
   <div title="外形图" style="height:100%">
   <div style="position: relative;z-index: 100;">
    <table id="dg_file" class='easyui-datagrid' data-options="
                  rownumbers:false,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:false,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:false, 
                  toolbar:'',
                  pageSize:20">
            <thead>
              <tr href="#">
                    <th data-options="field:'ck',checkbox:false"></th>
                    <th field="id" align="center" hidden ="true">id</th>
                    <th field="filePath" align="center" hidden ="true">id</th>
                    <!--<th field="fileName" align="center" width="19.5%">文件名称</th>
                    <th field="fileType" align="center" width="19.5%">文件类型</th>-->
                    <th field="uploadTime" width="19.5%">上传时间</th>
                    <th field="handle" width="19.5%" formatter="downloadFile">操作</th>
              </tr>
            </thead>
		  </table>
    </div>
    <div style="position: relative;">
      <div class="img-btn-con">
        <a onclick="imgEnlarge()"><i class="fa fa-search-plus"></i></a><br>
        <a onclick="imgNarrow()"><i class="fa fa-search-minus"></i></a>
      </div>
      <div class="fielImg" id="box" style="cursor: move; position: absolute; top: 0; left: 0;">
        <img src="" alt="" id="processImg">
      </div>
    </div>
   </div>
   <div title="噪音参数" style="height:100%">
	<div id="singleMachineNoise" style="width:600px;height:320px;margin-top: 50px;"></div>
   </div>
   <div class="field-con" title="结构参数" data-options="closable:false" style="margin-left: 10px;position: relative;">
    <div class="supplement-param-form">
      <div class="formThreeColumn">
        <div class="form-legend"><hr><span>基本信息</span></div>
        <div class="form-order-line">
          <label>风机大类</label>
          <select id="j_fjdl" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="轴流风机">轴流风机</option>
            <option value="离心风机">离心风机</option>
            <option value="斜流风机">斜流风机</option>
            <option value="离心轴流">离心轴流</option>
            <option value="轴流叶轮">轴流叶轮</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>产品类型</label>
          <select id="j_cpxh" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="船用轴流风机">船用轴流风机</option>
            <option value="船用离心风机">船用离心风机</option>
            <option value="核电轴流风机">核电轴流风机</option>
            <option value="制冷轴流风机">制冷轴流风机</option>
            <option value="制冷离心风机">制冷离心风机</option>
            <option value="空冷器风机">空冷器风机</option>
            <option value="冷却塔叶轮">冷却塔叶轮</option>
            <option value="机车离心风机">机车离心风机</option>
            <option value="机车轴流风机">机车轴流风机</option>
            <option value="无蜗壳离心风机">无蜗壳离心风机</option>
            <option value="风电轴流风机">风电轴流风机</option>
            <option value="风电离心风机">风电离心风机</option>
            <option value="核电轴流风机">核电轴流风机</option>
            <option value="核电离心风机">核电离心风机</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>传动方式</label>
          <select id="j_cdfs" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="直联">直联</option>
            <option value="皮带传动">皮带传动</option>
            <option value="减速机">减速机</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>叶片数</label>
          <input type="text" id="j_yps" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>角度</label>
          <input type="text" id="j_jd" class="easyui-textbox" style="width: 175px" />(°)
        </div>
        <div class="form-order-line">
          <label>测试报告</label>
          <input type="text" id="j_csbg" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>整机重量</label>
          <input type="text" id="j_zjzl" class="easyui-textbox" style="width: 175px" />(kg)
        </div>
        <div class="form-order-line">
          <label>外形图号</label>
          <input type="text" id="j_wxth" class="easyui-textbox" style="width: 175px" />
        </div>        
        <div class="clear"></div>
        
        <div class="form-legend"><hr><span>电机参数</span></div>
        <div class="form-order-line">
          <label>电机型号</label>
          <input type="text" id="d_djxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机厂家</label>
          <input type="text" id="d_djcj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机安装型式</label>
          <input type="text" id="d_djazxs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>是否是防爆电机</label>
          <select id="d_sfsfbdj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="是">是</option>
            <option value="否">否</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>防爆等级</label>
          IP<input type="text" id="d_fbdj" class="easyui-textbox" style="width: 161px" />
        </div>
        <div class="form-order-line">
          <label>额定功率</label>
          <input type="text" id="d_edgl" class="easyui-textbox" style="width: 175px" />(kw)
        </div>
        <div class="form-order-line">
          <label>额定转速</label>
          <input type="text" id="d_edzs" class="easyui-textbox" style="width: 175px" />(rpm)
        </div>
        <div class="form-order-line">
          <label>额定电压</label>
          <input type="text" id="d_eddy" class="easyui-textbox" style="width: 175px" />(V)
        </div>
        <div class="form-order-line">
          <label>额定电流</label>
          <input type="text" id="d_eddl" class="easyui-textbox" style="width: 175px" />(A)
        </div>
        <div class="form-order-line">
          <label>电压频率</label>
          <input type="text" id="d_dypl" class="easyui-textbox" style="width: 175px" />(Hz)
        </div>
        <div class="form-order-line">
          <label>功率因数</label>
          <input type="text" id="d_glys" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>防护等级</label>
          <input type="text" id="d_fhdj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>绝缘等级</label>
          <input type="text" id="d_jydj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>加热带</label>
          <select id="d_jrd" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>电缆长度</label>
          <input type="text" id="d_dlcd" class="easyui-textbox" style="width: 175px" />(m)
        </div>
        <div class="form-order-line">
          <label>最低使用温度</label>
          <input type="text" id="d_zdwd" class="easyui-textbox" style="width: 175px" />(℃)
        </div>
        <div class="form-order-line">
          <label>最高使用温度</label>
          <input type="text" id="d_zgwd" class="easyui-textbox" style="width: 175px" />(℃)
        </div>
        <div class="form-order-line">
          <label>允许最大使用湿度</label>
          <input type="text" id="d_yxzdsysd" class="easyui-textbox" style="width: 175px" />(%)
        </div>
        <div class="form-order-line">
          <label>允许最大使用海拔</label>
          <input type="text" id="d_yxzdsyhb" class="easyui-textbox" style="width: 175px" />(m)
        </div>
        <div class="form-order-line">
          <label>前轴承型号</label>
          <input type="text" id="d_qzcxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>后轴承型号</label>
          <input type="text" id="d_hzcxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机能效等级</label>
          <input type="text" id="d_djnxdj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机壳体材质</label>
          <input type="text" id="d_djkdcz" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机重量</label>
          <input type="text" id="d_djzl" class="easyui-textbox" style="width: 175px" />(kg)
        </div>
        <div class="clear"></div>
        <div class="form-legend"><hr><span>材质及表面处理</span></div>
        <div class="form-order-line">
          <label>机壳材质</label>
           <select id="c_jkcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="碳钢">碳钢</option>
            <option value="304">304</option>
            <option value="316">316</option>
            <option value="铝合金">铝合金</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>叶轮材质</label>
          <select id="c_ylcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="碳钢">碳钢</option>
            <option value="304">304</option>
            <option value="316">316</option>
            <option value="铝合金">铝合金</option>
            <option value="塑料">塑料</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>进风口材质</label>
          <input type="text" id="c_jfkcz" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>防护网材质</label>
          <select id="c_fhwcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="不锈钢">不锈钢</option>
            <option value="喷漆">喷漆</option>
            <option value="热浸锌">热浸锌</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>标准件</label>
          <select id="c_bzj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="A2-70">A2-70</option>
            <option value="A4-70">A4-70</option>
            <option value="热浸锌">热浸锌</option>
            <option value="镀锌">镀锌</option>
            <option value="发黑">发黑</option>
          </select>
        </div>
        <div class="clear"></div>
        <div class="form-legend zljg"><hr><span>轴流风机结构参数</span></div>
        <div class="form-order-line zljg">
          <label>气流方向</label>
          <select id="z_qlfx" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="前吹">前吹</option>
            <option value="后吹">后吹</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>筒体厚度</label>
          <input type="text" id="z_tthd" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>法兰厚度</label>
          <input type="text" id="z_flhd" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>风筒类型</label>
          <select id="z_ftlx" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="整体">整体</option>
            <option value="开箱">开箱</option>
            <option value="管道">管道</option>
            <option value="低噪">低噪</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>是否是防爆电机</label>
          <select id="z_sffbfj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="是">是</option>
            <option value="否">否</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>防爆外接接线盒厂家</label>
          <input type="text" id="z_fbwjjxhcj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>前防护网</label>
          <select id="z_qfhw" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>前防护网间距</label>
          <input type="text" id="z_qfhwjj" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>后防护网</label>
          <select id="z_hfhw" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>后防护网间距</label>
          <input type="text" id="z_hfhwjj" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>风筒内径</label>
          <input type="text" id="z_ftnj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1外径</label>
          <input type="text" id="z_fl1wj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔所在圆直径（叶轮端）</label>
          <input type="text" id="z_fl1lskszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔直径</label>
          <input type="text" id="z_fl1lskzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔个数</label>
          <input type="text" id="z_fl1lskgs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2外径</label>
          <input type="text" id="z_fl2wj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔所在圆直径</label>
          <input type="text" id="z_fl2lskszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔直径</label>
          <input type="text" id="z_fl2lskzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔个数</label>
          <input type="text" id="z_fl2lskgs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>风筒长度</label>
          <input type="text" id="z_ftcd" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>整机高度</label>
          <input type="text" id="z_zjgd" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>接线格兰型号</label>
          M<input type="text" id="z_jxglxh" class="easyui-textbox" style="width: 167px" />
        </div>
        <div class="form-order-line zljg">
          <label>接线格兰材质</label>
          <select id="z_jxglcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="塑料">塑料</option>
            <option value="不锈钢">不锈钢</option>
            <option value="其它金属">其它金属</option>
          </select>
        </div>
        <div class="clear zljg"></div>
        <div class="form-legend lxjg"><hr><span>离心风机结构参数</span></div>
        <div class="form-order-line">
          <label>出口方向</label>
          <input type="text" id="l_ckfx" class="easyui-textbox" style="width: 175px" />
        </div>
        <!-- 传动方式不用  l_cdfs-->
        <div class="form-order-line lxjg">
          <label>宽</label>
          <input type="text" id="l_k" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>长</label>
          <input type="text" id="l_c" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>高</label>
          <input type="text" id="l_g" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>中心高</label>
          <input type="text" id="l_zxg" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口长</label>
          <input type="text" id="l_ckc" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口宽</label>
          <input type="text" id="l_ckk" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口安装孔直径</label>
          <input type="text" id="l_ckazkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口安装孔数量</label>
          <input type="text" id="l_ckzaksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口直径</label>
          <input type="text" id="l_jkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰外径</label>
          <input type="text" id="l_jkflwj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔所在圆直径</label>
          <input type="text" id="l_jkflazkszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔直径</label>
          <input type="text" id="l_jkflazkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔数量</label>
          <input type="text" id="l_jkflazksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>安装孔直径</label>
          <input type="text" id="l_azkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>安装孔数量</label>
          <input type="text" id="l_azksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>接线格兰型号</label>
          M<input type="text" id="l_jxglxh" class="easyui-textbox" style="width: 167px" />
        </div>
        <div class="form-order-line lxjg">
          <label>接线格兰材质</label>
          <select id="l_jxglcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="塑料">塑料</option>
            <option value="不锈钢">不锈钢</option>
            <option value="其它金属">其它金属</option>
          </select>
        </div>
      </div>
    </div>
    <div class="clear"></div>
   </div>
   <div title="原始数据" data-options="region:'center',closable:false" style="height: 100%;width: 100%;overflow-x:hidden">
    <table id="dg" style="height: 100%;width: 100%;"></table>
   </div>
   <div class="field-con" title="其他参数" data-options="closable:false" style="margin-left: 10px;overflow-x:hidden">
	   	<div id="fieldCon">
			<!--<div class="formThreeColumn">
				<div class="form-order-line">
					<label>风机类型</label><input type="text" id="fengjileixing" class="easyui-combobox">
				</div>
			</div>-->

		</div>
   </div>
   
   <!-- 作为pdf导出的form -->
   <form id="chartForm" style="display:none">
    <input id="imageValue" name="base64Info" type="text" maxlength="500000"/>
    <input id="imageValue2" name="base64Info2" type="text" maxlength="500000"/>
    <input id="fanSelectParam" name="fanSelectParam" type="text" maxlength="500"/>
    <input id="fanId" name="fanId" type="text"/>
	<input id="fanNumber" name="fanNumber" type="text"/>
	<input id="zhijing" name="zhijing" type="text"/>
   </form>
  
   
 </div>
</body> 
</html>
<script type="text/javascript">
var fanId = shuoheUtil.getUrlHeader().fanId;
var queryX = shuoheUtil.getUrlHeader().queryX;
var queryY = shuoheUtil.getUrlHeader().queryY;
var queryXlY;
var queryGlY;
var queryZyY;
var isTransshape = shuoheUtil.getUrlHeader().isTransshape;
var fanSelectParam = shuoheUtil.getUrlHeader().fanSelectParam;
var fanSelectParamObj = parent.fanSelectParamObj
var user = <%=userJson%>;
var uuid = '<%=uuid%>'
var formObj
var fields = []
var hasParam = false;
var result;//接收其他参数返回的数据
var formJson = ''
var title = 'shop_fan_other_param'
var noiseData
var datas
var basePath = '<%=basePath%>'
var supplementParamObj = {} // 风机补充参数数据对象
var supplementParams = ['j_fjdl','j_cpxh','j_cdfs','j_yps','j_jd','j_csbg','j_zjzl','j_wxth','d_djxh','d_djcj', 'd_djazxs', 'd_sfsfbdj', 'd_fbdj', 'd_edgl', 'd_edzs', 'd_eddy', 'd_eddl', 'd_dypl', 'd_glys', 'd_fhdj', 'd_jydj', 'd_jrd', 'd_dlcd',
 'd_zdwd', 'd_zgwd', 'd_yxzdsysd', 'd_yxzdsyhb', 'd_qzcxh', 'd_hzcxh', 'd_djnxdj', 'd_djkdcz', 'd_djzl', 'c_jkcz', 'c_ylcz', 'c_jfkcz', 'c_fhwcz', 'c_bzj', 'z_qlfx', 'z_tthd',
  'z_flhd', 'z_ftlx', 'z_sffbfj', 'z_fbwjjxhcj', 'z_qfhw', 'z_qfhwjj', 'z_hfhw', 'z_hfhwjj', 'z_ftnj', 'z_fl1wj', 'z_fl1lskszyzj', 'z_fl1lskzj', 'z_fl1lskgs', 'z_fl2wj',
   'z_fl2lskszyzj', 'z_fl2lskzj', 'z_fl2lskgs', 'z_ftcd', 'z_zjgd', 'z_jxglxh', 'z_jxglcz', 'l_ckfx', 'l_k', 'l_c', 'l_g', 'l_zxg', 'l_ckc', 'l_ckk', 'l_ckazkzj',
    'l_ckzaksl', 'l_jkzj', 'l_jkflwj', 'l_jkflazkszyzj', 'l_jkflazkzj', 'l_jkflazksl', 'l_azkzj', 'l_azksl', 'l_jxglxh', 'l_jxglcz'] // 风机补充参数字段
 $(function(){
	 // 外形图获取
  $("#processImg").attr("src","/fanFile/preview.do?fan_number="+fanId);
	/* $('#main').css('width',document.body.clientWidth + 'px');
	$('#main1').css('width',document.body.clientWidth + 'px');
	$('#main2').css('width',document.body.clientWidth + 'px');
	$('#main4').css('width',document.body.clientWidth + 'px'); */
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      formObj = data
      fields = formObj.field
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
	  initFormOrder(formObj,uuid)
	  $('#jihao').parent().append('(cm)')
	  $('#yelunzhijing').parent().append('(m)')
    }
  })
    //风机大类切换事件
	$('#j_fjdl').combobox({
      onChange: function(newValue,oldValue){
        if(newValue == "轴流风机"){
        	$(".zljg").show();
  		    $(".lxjg").hide();
        }else if(newValue == "离心风机"){
        	$(".zljg").hide();
  		  	$(".lxjg").show();
        }else{
			$(".zljg").show();
			$(".lxjg").show();
	    }
      }
    });
  $.ajax({
    url: "<%=basePath%>/shop/product/getProductSortEntity.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
		productId: fanId
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
	  console.log(data)
	  noiseData = data
    }
  })


  //js渲染性能曲线图
  loadCurveChart();
  getFanOtherParam(fanId);//加载风机参数
	getFanSupplementParam(fanId) // 加载风机补充参数
  loadFileList();
  //渲染单机噪声图
  loadNoise();
})

function fielData1() {
  var temp = '';
  for (var i=0;i<fields.length;i++){
    var field = fields[i];
    // console.log(field)
    if (field.fieldType == 'textbox') {
      // console.log(result[field.text])
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").textbox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").textbox("setValue", "' + result[field.text] + '");'
      }
    } else {
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").combobox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").combobox("setValue", "' + result[field.text] + '");'
      }
    }
  }
  var script = document.createElement("script")
  script.type = "text/javascript"
  try {
    script.appendChild(document.createTextNode(temp))
  } catch (ex) {
    script.text = temp
  }
  document.body.appendChild(script)
  //隐藏信息---开始
      $('#fengjileixing').parent().css('display','none');
      $('#chanpinleixing').parent().css('display','none');
      $('#shifufangbao').parent().css('display','none');
      $('#dianya').parent().css('display','none');
      $('#pinlv').parent().css('display','none');
      $('#fanghudengji').parent().css('display','none');
      $('#chuandongfangshi').parent().css('display','none');
      $('#jikebanhou').parent().css('display','none');
      $('#jikeleixing').parent().css('display','none');
      $('#fengjianzhuangxingshi').parent().css('display','none');
      $('#jinkoufanghuwang').parent().css('display','none');
      $('#chukoufanghuwang').parent().css('display','none');
      $('#kongjianjiareqi').parent().css('display','none');
      //隐藏信息---结束
  if (isTransshape == '1') {
	$("#midu").textbox("setValue", fanSelectParamObj.airDensityQuery)
	$("#zhuansu").textbox("setValue", fanSelectParamObj.speedQuery)
	$("#yelunzhijing").textbox("setValue", datas[0].shaftPower)
  }
}

function loadFileList(){
	$('#dg_file').datagrid({
	    url:'/develop/url/getUrl.do',
	    queryParams:
	    {
	      name:'getFanFileByFanId',
	      fanId:fanId
	    },
	    method:'post'
	});
}
function getFanOtherParam(fanId){
	$.ajax({
        url: "/capabilityParam/getDataByFanId",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          'tableName': title,
          'fanId':fanId
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
        	if(!data.rows.length==0){
        		hasParam = true;
        		result = data.rows[0];
				// initFieldData(true);
				fielData1()
        	}
          
        }
      })
}
// 加载风机补充参数
function getFanSupplementParam (fanId) {
  $.ajax({
    url: "/FanSupplementParam/findByFanId",
    type: "POST",
    dataType:'json',
    async:false,
    data:
    {
      'fanId':fanId
    },
    error: function() //失败
    {
      // messageloadError()
    },
    success: function(data)//成功
    {
      console.log(data)
      if(data){ // 有数据
        supplementParamObj = data
        initSupplementParam(true)
      }
    }
  })
}
// 风机补充参数填充
function initSupplementParam () {
  for (var i = 0; i < supplementParams.length; i++) {
    if (supplementParamObj[supplementParams[i]] !== null) {
      $("#" + supplementParams[i]).textbox("setValue", supplementParamObj[supplementParams[i]])
      if (supplementParams[i] == 'j_fjdl') {
    	  if(supplementParamObj[supplementParams[i]] == "轴流风机"){
          		$(".zljg").show();
    		    $(".lxjg").hide();
          }else if(supplementParamObj[supplementParams[i]] == "离心风机"){
          		$(".zljg").hide();
    		  	$(".lxjg").show();
          }else{
	  		  $(".zljg").show();
	  		  $(".lxjg").show();
	  	  }
      }
    }
  }
}
//变形设计，此处需要重新定义URL===============
var columns = []
if (isTransshape == '1') {
	fanSelectParamObj.fanNumber = parent.fanNumber
	fanSelectParamObj.fanId = fanId
	$.ajax({
        url: "/shop/sort/getPerforParam.do",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
			paramJson: JSON.stringify(fanSelectParamObj)
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
        	if(data){
				console.log(data)
				datas = data
				if (data.length > 0) {
					$("#yelunzhijing").textbox("setValue", data[0].shaftPower)
				}
        	}
          
        }
	  })
	  columns = [[
		{field:'flux',title:'流量<br>Q[m&sup3/h]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalHead',title:'全压<br>P[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticPressure',title:'静压<br>P<sub>st</sub>[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		// {field:'speed',title:'转速<br>n[rpm]',width:'11%',align:'center',editor:'textbox'},
		{field:'internalPower',title:'轴功率<br>P<sub>z</sub>[kw]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'fullSpeed',title:'比转速<br>（全压）n<sub>s</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticSpeed',title:'比转速<br>（静压）n<sub>s2</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'tproductiveness',title:'全压效率<br>η<sub>t</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'sproductiveness',title:'静压效率<br>η<sub>st</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'noise',title:'噪声<br>L<sub>A</sub>[dB(A)]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'soundLevel',title:'比A声级<br>Lsa[dB(A)]',width:'6%	',align:'center',editor:'textbox',sortable: true},
		{field:'fluxRatio',title:'流量系数<br>φ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalRatio',title:'全压系数<br>ψ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticRatio',title:'静压系数<br>ψs',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'powerRatio',title:'功率系数<br>λ',width:'6%',align:'center',editor:'textbox',sortable: true}
	]]
} else {
	$.ajax({
        url: "/develop/url/getUrl.do",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
			'name':'getFanCapabilityParam',
			'fanNumber':parent.fanNumber
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
        	if(data){
				datas = data
        	}
        }
	  })
	  columns = [[
		{field:'flux',title:'流量<br>Q[m&sup3/h]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalHead',title:'全压<br>P[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticPressure',title:'静压<br>P<sub>st</sub>[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		// {field:'speed',title:'转速<br>n[rpm]',width:'11%',align:'center',editor:'textbox'},
		{field:'internalPower',title:'轴功率<br>P<sub>z</sub>[kw]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'fullSpeed',title:'比转速<br>（全压）n<sub>s</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticSpeed',title:'比转速<br>（静压）n<sub>s2</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'tProductiveness',title:'全压效率<br>η<sub>t</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'sProductiveness',title:'静压效率<br>η<sub>st</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'noise',title:'噪声<br>L<sub>A</sub>[dB(A)]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'soundLevel',title:'比A声级<br>Lsa[dB(A)]',width:'6%	',align:'center',editor:'textbox',sortable: true},
		{field:'fluxRatio',title:'流量系数<br>φ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalRatio',title:'全压系数<br>ψ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticRatio',title:'静压系数<br>ψs',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'powerRatio',title:'功率系数<br>λ',width:'6%',align:'center',editor:'textbox',sortable: true}
	]]
}
$('#dg').datagrid({
	idField : 'id',
	data: datas,
	// rownumbers : true,
	remoteSort: false, // 点击排序
	// sortName : 'soundLevel',
	sortOrder : 'desc',
	columns: columns,
	pagination:true,
	toolbar:''//,
	//onClickRow: onClickRow
})

var efficiencyCurveFormula = '', internalPowerCurveFormula = '', pressureCurveFormula = ''
var reg = new RegExp("a","g")
function sortByFlux (a, b) { // 数组排序
	return a[0]-b[0]
}
function loadCurveChart(){
	var pressure = [];
	var staticPressure = [];
	var productiveness = [];
	var power = [];
	var noise = [];
	$.ajax({
        url: "/productMix/getEchartsData", 
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
        	'fanId':fanId,
        	'fanNumber':parent.fanNumber
        },
        error: function() //失败
        {
        	shuoheUtil.layerMsgSaveOK('异常无法保存');
        },
        success: function(data)//成功
        {
        	//alert(isTransshape);变形设计
        	if (isTransshape == '1') {
				for (const iterator of datas) {
					pressure.push([iterator.flux, iterator.totalHead])
					staticPressure.push([iterator.flux, iterator.staticPressure])
					productiveness.push([iterator.flux, iterator.tproductiveness])
					power.push([iterator.flux, iterator.internalPower])
					noise.push([iterator.flux, iterator.noise])					
				}
				pressure.sort(sortByFlux)
				staticPressure.sort(sortByFlux)
				productiveness.sort(sortByFlux)
				power.sort(sortByFlux)
				noise.sort(sortByFlux)
			} else {
				pressure = data.pressureData;
				staticPressure = data.pressureStaticData;
				productiveness = data.productivenessData;
				power = data.powerData;
				noise = data.noiseData;
				for(var i = 0;i<data.productivenessData.length;i++){  //循环LIST
					if(data.productivenessData[i][0] == queryX){
						queryXlY = data.productivenessData[i][1];
					}
                 }
				//alert(queryXlY);
				for(var i = 0;i<data.powerData.length;i++){  //循环LIST
					if(data.powerData[i][0] == queryX){
						queryGlY = data.powerData[i][1];
					}
                 }
				for(var i = 0;i<data.noiseData.length;i++){  //循环LIST
					if(data.noiseData[i][0] == queryX){
						queryZyY = data.noiseData[i][1];
					}
                 }
				//alert("queryXlY="+queryXlY+";queryGlY="+queryGlY+";queryZyY="+queryZyY);
			}
        	efficiencyCurveFormula = data.fanModel.efficiencyCurveFormula
        	efficiencyCurveFormula = efficiencyCurveFormula.substring(efficiencyCurveFormula.indexOf('return')+7,efficiencyCurveFormula.length-1)
        	internalPowerCurveFormula = data.fanModel.internalPowerCurveFormula
        	internalPowerCurveFormula = internalPowerCurveFormula.substring(internalPowerCurveFormula.indexOf('return')+7,internalPowerCurveFormula.length-1)
        	pressureCurveFormula = data.fanModel.pressureCurveFormula
        	pressureCurveFormula = pressureCurveFormula.substring(pressureCurveFormula.indexOf('return')+7,pressureCurveFormula.length-1)
        }
    });
	var myChart = echarts.init(document.getElementById('main'))
	var option = {
  backgroundColor: 'white',
  grid: {
    right: '13%'
  },
  tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    //animation:false,
    legend: {
        data:['全压曲线', '静压曲线']
    },
    xAxis: {
        type: 'value',
        name:'流量(m3/h)',
        min: function(value) {
            return Math.floor(value.min / 100) * 100;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(/a/g,e.value)
					var temp2 = internalPowerCurveFormula.replace(/a/g,e.value)
					var temp3 = pressureCurveFormula.replace(/a/g,e.value)
					console.log(temp2)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	{
	        type: 'value',
	        name:'压力(pa)',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'全压曲线',
            type:'line',
            smooth: true,
            data:pressure
		},
		{
			name:'静压曲线',
			type:'line',
			smooth: true,
			data:staticPressure
		},
        {
        	name:'工况点',
        	type:'line',
        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
        	symbolSize: 50,
        	label: {
	        	show: true,
	        	formatter: '{c}',
	        	fontSize: 18
        	},
        	data: [[queryX, queryY]]
        }
    ]
}
	myChart.setOption(option)
	
	var myChart1 = echarts.init(document.getElementById('main1'))
	var option1 = {
  backgroundColor: 'white',
  grid: {
    right: '13%'
  },
	tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    legend: {
        data:['效率曲线']
    },
    xAxis: {
        type: 'value',
        name:'流量(m3/h)',
        min: function(value) {
            return Math.floor(value.min / 100) * 100;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(/a/g,e.value)
					var temp2 = internalPowerCurveFormula.replace(/a/g,e.value)
					var temp3 = pressureCurveFormula.replace(/a/g,e.value)
					console.log(temp2)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	
	    {
	        type: 'value',
	        name:'效率(%)',
	        min: 0,
	        max: 100,
	        position: 'left',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'效率曲线',
            type:'line',
            smooth: true,
            data:productiveness
        },
        {
        	name:'工况点',
        	type:'line',
        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
        	symbolSize: 50,
        	label: {
	        	show: true,
	        	formatter: '{c}',
	        	fontSize: 18
        	},
        	data: [[queryX, queryXlY]]
        }
    ]
}
	myChart1.setOption(option1)
	
	var myChart2 = echarts.init(document.getElementById('main2'))
	var option2 = {
  backgroundColor: 'white',
  grid: {
    right: '13%'
  },
	tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    legend: {
        data:['功率曲线']
    },
    xAxis: {
        type: 'value',
        name:'流量(m3/h)',
        min: function(value) {
            return Math.floor(value.min / 100) * 100;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(/a/g,e.value)
					var temp2 = internalPowerCurveFormula.replace(/a/g,e.value)
					var temp3 = pressureCurveFormula.replace(/a/g,e.value)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	{
	        type: 'value',
	        name:'功率',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'功率曲线',
            type:'line',
            smooth: true,
            data:power
        },
        {
        	name:'工况点',
        	type:'line',
        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
        	symbolSize: 50,
        	label: {
	        	show: true,
	        	formatter: '{c}',
	        	fontSize: 18
        	},
        	data: [[queryX, queryGlY]]
        }
    ]
}
	myChart2.setOption(option2)

	
	
	var myChart4 = echarts.init(document.getElementById('main4'))
	var option4 = {
    backgroundColor: 'white',
    grid: {
    right: '13%'
  },
		tooltip: {
	        trigger: 'item',
	        show: true,
	        axisPointer: {
	            snap: false,
	            type: 'cross'
	        }
	    },
	    legend: {
	        data:['噪音曲线']
	    },
	    xAxis: {
	        type: 'value',
	        name:'流量(m3/h)',
	        min: function(value) {
            return Math.floor(value.min / 100) * 100;
        },
	        axisPointer: {
	            show: true,
	            snap: false,
	            label: {
	                show: true,
	                formatter: function (e) {
						var temp1 = efficiencyCurveFormula.replace(/a/g,e.value)
						var temp2 = internalPowerCurveFormula.replace(/a/g,e.value)
						var temp3 = pressureCurveFormula.replace(/a/g,e.value)
						console.log(temp2)
						$('#xData').textbox('setValue', e.value)
						$('#yData1').textbox('setValue', eval(temp1))
						$('#yData2').textbox('setValue', eval(temp2))
						$('#yData3').textbox('setValue', eval(temp3))
	                    return e.value
	                }
	            }
	        }
	    },
	    yAxis: [
	    	
		    {
		        type: 'value',
		        name:'dB(A)',
				axisPointer: {
					show: false
				}
		    }
	    ],
	    series: [
	        {
	            name:'噪音曲线',
	            type:'line',
	            smooth: true,
	            data:noise
	        },
	        {
	        	name:'工况点',
	        	type:'line',
	        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
	        	symbolSize: 50,
	        	label: {
		        	show: true,
		        	formatter: '{c}',
		        	fontSize: 18
	        	},
	        	data: [[queryX, queryZyY]]
	        }
	    ]
	}
	myChart4.setOption(option4)

}
//文件下载
function downloadFile(value,row,index){
	ret =  '<a href="/fanFile/downloadFile.do?fileRealPath='+row.filePath+'&fileName='+row.fileName+'" class="easyui-linkbutton">下载</a>';   
    return ret;
}
function convertCanvasToImage() {
	var thisChart = echarts.init(document.getElementById('main'));
	var thisChart1 = echarts.init(document.getElementById('main1'));
	var thisChart2 = echarts.init(document.getElementById('main2'));
	var thisChart4 = echarts.init(document.getElementById('main4'));
	var chartExportUrl = '/productMix/exportPdf'; 
	var picBase64Info = thisChart.getDataURL();//获取echarts图的base64编码，为png格式。  
	var picBase64Info1 = thisChart1.getDataURL();//效率。
	var picBase64Info2 = thisChart2.getDataURL();//功率。
	var picBase64Info4 = thisChart4.getDataURL();//噪音。 
	$('#chartForm').find('input[name="base64Info"]').val(picBase64Info+"&&"+picBase64Info1+"&&"+picBase64Info2+"&&"+picBase64Info4);//将编码赋值给输入框
	$('#chartForm').find('input[name="fanSelectParam"]').val(JSON.stringify(parent.fanSelectParam));
	$('#chartForm').find('input[name="fanId"]').val(fanId); // 将风机id传到后台
	$('#chartForm').find('input[name="fanNumber"]').val(parent.fanNumber); // 将风机编号传到后台
	if (isTransshape == '1') {
		$('#chartForm').find('input[name="zhijing"]').val(datas[0].shaftPower); // 将直径传到后台
	}
	$('#chartForm').attr('action',chartExportUrl).attr('method', 'post'); // 设置提交到的url地址
	$('#chartForm').submit();
}

function loadNoise(){
	var noiseChart = echarts.init(document.getElementById('singleMachineNoise'))
	var option = {
	    title : {
	        text: '单机噪声直方图'
	    },
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
	        data:['声功率','进出口声压级']
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            dataView : {show: true, readOnly: false},
	            magicType : {show: true, type: ['line', 'bar']},
	            restore : {show: true},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true,
	    xAxis : [
	        {
	            type : 'category',
	            data : ['63','125','250','500','1000','2000','4000','8000']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'声功率',
	            type:'bar',
	            data:[noiseData.noise63, noiseData.noise125, noiseData.noise250, noiseData.noise500, noiseData.noise1KHz, noiseData.noise2KHz, noiseData.noise4KHz, noiseData.noise8KHz]
	        },
	        {
	            name:'进出口声压级',
	            type:'bar',
	            data:[noiseData.noise63s, noiseData.noise125s, noiseData.noise250s, noiseData.noise500s, noiseData.noise1KHzs, noiseData.noise1KHzs, noiseData.noise4KHzs, noiseData.noise8KHzs]
	        }
	    ]
	};
	noiseChart.setOption(option);
}
// 图片缩放比例
var imgScale = 1
// 外形图宽度
$('#processImg').css('width', document.body.clientWidth - 15 + 'px')
var imgW = document.body.clientWidth - 15
// 图片放大
function imgEnlarge () {
  imgScale = Number((imgScale + 0.1).toFixed(1))
  var w = Number((imgScale * imgW).toFixed(1))
  $('#processImg').css('width', w + 'px')
}
// 图片缩小
function imgNarrow () {
  if (imgScale > 0.1) {
    imgScale = Number((imgScale - 0.1).toFixed(1))
    var w = Number((imgScale * imgW).toFixed(1))
    $('#processImg').css('width', w + 'px')
  }
}

</script>