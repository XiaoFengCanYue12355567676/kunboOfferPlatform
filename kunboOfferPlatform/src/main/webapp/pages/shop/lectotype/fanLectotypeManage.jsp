<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.shuohe.entity.system.user.User"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = UUID.randomUUID().toString();
	User user = null;
	String userJson = null;
	try
	{
	    user = (User)session.getAttribute("user");
	    if(user==null)
	    {
	      System.out.println("user = null");
	      String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    }
	    else
	    {
	        userJson = Json.toJsonByPretty(user);
	    }
	}
	catch(Exception e)
	{
	    String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    e.printStackTrace();
	}
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>原材料管理</title> 


<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<!-- FontAwesome字体图标 -->
<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="/pages/css/add-form.css" rel="stylesheet"/>
<!-- jQuery相关引用 -->
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript" src="/pages/js/moment.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>
<script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

<script type="text/javascript" src="/pages/js/base-loading.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>


<style type="text/css">
    html, body{ margin:0; height:100%; }
    .search-line{
      height: 1px;
      background-color: #ddd;
      margin: 15px 0;
    }
    .text-right{
      text-align: right;
      padding-right: 5px;
      width: 104px;
    }
    .form-legend{
      margin-bottom: 0;
    }
    .toggleSearchBtn{
      display: inline-block;
      padding: 0 10px;
      position: absolute;
      top: 0;
      right: 15px;
      color: #333;
      height: 40px;
      line-height: 40px;
      background-color: #fff;
    }
    .datagrid-cell{
      cursor: pointer;
    }
</style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:25%;">           

        <table style="width:100%;margin-top: 15px">
          <tr id="transshapeDiv" style="display: none;">
            <td class="text-right">相似设计</td>
            <td>
              <input type="radio" value="0" name="isTransshape" checked="checked" onclick="selectTransshape(0)"/>&nbsp;否&nbsp;&nbsp;
              <input type="radio" value="1" name="isTransshape" onclick="selectTransshape(1)" />&nbsp;是
            </td>
          </tr>
          <!-- <tr>
            <td class="text-right">风机类型</td>
            <td>
              <input id="fanType" class="easyui-combobox" style="width:185px;height:32px;"
                  data-options="required:true,valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getFanTypeSelect'">
            </td>
          </tr> -->
              <tr>
                <td class="text-right">风量(P)</td>
                <td>
                  <input id="blowingRate" type="text" class="easyui-textbox" style="width:100px;height:32px;" data-options="required:true">
                  <span><select class="easyui-combobox" id="state" name="state" style="width:80px;">
						<option value="m3/h">m&sup3/h</option>
						<option value="m3/s">m&sup3/s</option>
					</select>
	             </span>
                </td>
              </tr>
              <tr>
                <td class="text-right">
	                <input type="radio" value="全压" name="windPressure" checked="checked"/>全压
	                <input type="radio" value="静压" name="windPressure" />静压(Q)
                </td>
                <td>
                  <input id="totalHeadQuery" type="text" class="easyui-textbox" style="width:100px;height:32px;" data-options="required:true">
                  <span><select class="easyui-combobox" name="state" style="width:80px;">
						<option value="Pa">Pa</option>
					</select>
	             </span>
                </td>
              </tr>
              <tr>
                <td class="text-right">风机误差设置(%)</td>
                <td>
                  <input id="errorMinRange" type="text" class="easyui-textbox" value="-5" style="width:83.6px;height:32px;" data-options="prompt:'-5',required:true">
                  <span id="rangeIdentification">~</span>
                  <input id="errorMaxRange" type="text" class="easyui-textbox" value="10" style="width:83.6px;height:32px;" data-options="prompt:'10',required:true">
                </td>
              </tr>
              <tr>
                <td class="text-right">转速</td>
                <td>
                  <input id="speedQuery" type="text" class="easyui-textbox" style="width:185px;height:32px;" data-options="required:true">
                </td>
              </tr>
              <!-- <tr>
                <td class="text-right">传动方式</td>
                <td>
                  <select id="drivingMode" class="easyui-combobox" name="dept" style="width:185px;">   
                    <option value="aa">直连</option>   
                    <option>皮带传送</option>
                    <option>减速机</option>
                </select>
              </td>
              </tr> -->
              <tr>
                <td class="text-right">进口空气密度</td>
                <td>
                  <input id="airDensityQuery" type="text" class="easyui-textbox" style="width:185px;height:32px;" value="1.2"  data-options="prompt:'1.2',required:true">
                </td>
              </tr>
               <tr id="ylzjDiv" style="display: none;">
                <td class="text-right">叶轮直径范围(m)</td>
                <td>
                  <input id="minYlzj" type="text" class="easyui-textbox" style="width:83.6px;height:32px;" data-options="required:true">
                  <span>~</span>
                  <input id="maxYlzj" type="text" class="easyui-textbox" style="width:83.6px;height:32px;" data-options="required:true">
                </td>
              </tr>
              <tr>
                  <td colspan="2"><div class="search-line"></div></td>
                </tr>
              <tr>
                <td colspan="2"><div class="form-legend"><hr><span>基本信息</span></div></td>
              </tr>
              <tr>
                <td class="text-right">风机大类</td>
                <td>
                    <select id="j_fjdl" class="easyui-combobox" style="width:185px;height:32px;">
			            <option value="">请选择</option>
			            <option value="轴流风机">轴流风机</option>
			            <option value="离心风机">离心风机</option>
			            <option value="斜流风机">斜流风机</option>
			            <option value="离心轴流">离心轴流</option>
			            <option value="轴流叶轮">轴流叶轮</option>
			         </select>
                </td>
              </tr>
              <tr>
                <td class="text-right">产品类型</td>
                <td>
                    <select id="j_cpxh" class="easyui-combobox" style="width:185px;height:32px;">
			            <option value="">请选择</option>
			            <option value="船用轴流风机">船用轴流风机</option>
			            <option value="船用离心风机">船用离心风机</option>
			            <option value="核电轴流风机">核电轴流风机</option>
			            <option value="制冷轴流风机">制冷轴流风机</option>
			            <option value="制冷离心风机">制冷离心风机</option>
			            <option value="空冷器风机">空冷器风机</option>
			            <option value="冷却塔叶轮">冷却塔叶轮</option>
			            <option value="机车离心风机">机车离心风机</option>
			            <option value="机车轴流风机">机车轴流风机</option>
			            <option value="无蜗壳离心风机">无蜗壳离心风机</option>
			            <option value="风电轴流风机">风电轴流风机</option>
			            <option value="风电离心风机">风电离心风机</option>
			            <option value="核电轴流风机">核电轴流风机</option>
			            <option value="核电离心风机">核电离心风机</option>
			          </select>
                </td>
              </tr>
              <tr>
                <td class="text-right">传动方式</td>
                <td>
                    <select id="j_cdfs" class="easyui-combobox" style="width:185px;height:32px;">
			            <option value="">请选择</option>
			            <option value="直联">直联</option>
			            <option value="皮带传动">皮带传动</option>
			            <option value="减速机">减速机</option>
			          </select>
                </td>
              </tr>
             <tr>
                <td class="text-right">叶片数</td>
                <td>
                  <input id="j_yps" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                </td>
              </tr>
              <tr>
                <td class="text-right">角度</td>
                <td>
                  <input id="j_jd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                </td>
              </tr>
              <tr>
                <td colspan="2"><div class="form-legend"><hr><span>电机参数</span><a href="javascript:void(0)" class="toggleSearchBtn" onclick="toggleSearchCon(this, 'searchCon0')">展开</a></div></td>
              </tr>
             <tr id="searchCon0" style="display: none;">
                <td colspan="2">
                  <table>
                    <tr>
                      <td class="text-right">电机型号</td>
                      <td>
                        <input id="d_djxh" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电机厂家</td>
                      <td>
                        <input id="d_djcj" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电机安装型式</td>
                      <td>
                        <input id="d_djazxs" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">是否是防爆电机</td>
                      <td>
                        <select id="d_sfsfbdj" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="是">是</option>
				            <option value="否">否</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">防爆等级</td>
                      <td>
                        IP<input id="d_fbdj" type="text" class="easyui-textbox" style="width:171px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">额定功率</td>
                      <td>
                        <input id="d_edgl" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">额定转速</td>
                      <td>
                        <input id="d_edzs" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">额定电压</td>
                      <td>
                        <input id="d_eddy" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">额定电流</td>
                      <td>
                        <input id="d_eddl" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电压频率</td>
                      <td>
                        <input id="d_dypl" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">功率因数</td>
                      <td>
                        <input id="d_glys" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">防护等级</td>
                      <td>
                        <input id="d_fhdj" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">绝缘等级</td>
                      <td>
                        <input id="d_jydj" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">加热带</td>
                      <td>
                        <select id="d_jrd" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="有">有</option>
				            <option value="无">无</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电缆长度</td>
                      <td>
                        <input id="d_dlcd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">最低使用温度</td>
                      <td>
                        <input id="d_zdwd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">最高使用温度</td>
                      <td>
                        <input id="d_zgwd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">允许最大使用湿度</td>
                      <td>
                        <input id="d_yxzdsysd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">允许最大使用海拔</td>
                      <td>
                        <input id="d_yxzdsyhb" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">前轴承型号</td>
                      <td>
                        <input id="d_qzcxh" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">后轴承型号</td>
                      <td>
                        <input id="d_hzcxh" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电机能效等级</td>
                      <td>
                        <input id="d_djnxdj" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电机壳体材质</td>
                      <td>
                        <input id="d_djkdcz" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">电机重量</td>
                      <td>
                        <input id="d_djzl" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>              
              <tr>
                <td colspan="2"><div class="form-legend"><hr><span>材质及表面处理</span><a href="javascript:void(0)" class="toggleSearchBtn" onclick="toggleSearchCon(this, 'searchCon1')">展开</a></div></td>
              </tr>
              <tr id="searchCon1" style="display: none;">
                <td colspan="2">
                  <table>
                    <tr>
                      <td class="text-right">机壳材质</td>
                      <td>
                        <select id="c_jkcz" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="碳钢">碳钢</option>
				            <option value="304">304</option>
				            <option value="316">316</option>
				            <option value="铝合金">铝合金</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">叶轮材质</td>
                      <td>
                        <select id="c_ylcz" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="碳钢">碳钢</option>
				            <option value="304">304</option>
				            <option value="316">316</option>
				            <option value="铝合金">铝合金</option>
				            <option value="塑料">塑料</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">进风口材质</td>
                      <td>
                        <input id="c_jfkcz" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">防护网材质</td>
                      <td>
                        <select id="c_fhwcz" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="不锈钢">不锈钢</option>
				            <option value="喷漆">喷漆</option>
				            <option value="热浸锌">热浸锌</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">标准件</td>
                      <td>
                        <select id="c_bzj" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="A2-70">A2-70</option>
				            <option value="A4-70">A4-70</option>
				            <option value="热浸锌">热浸锌</option>
				            <option value="镀锌">镀锌</option>
				            <option value="发黑">发黑</option>
				          </select>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
               <tr>
                <td colspan="2"><div class="form-legend"><hr><span>轴流风机结构参数</span><a href="javascript:void(0)" class="toggleSearchBtn" onclick="toggleSearchCon(this, 'searchCon3')">展开</a></div></td>
              </tr>
              <tr id="searchCon3" style="display: none;">
                <td colspan="2">
                  <table>
                    <tr>
                      <td class="text-right">气流方向</td>
                      <td>
                        <select id="z_qlfx" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="前吹">前吹</option>
				            <option value="后吹">后吹</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">筒体厚度</td>
                      <td>
                        <input id="z_tthd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰厚度</td>
                      <td>
                        <input id="z_flhd" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">风筒类型</td>
                      <td>
                        <select id="z_ftlx" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="整体">整体</option>
				            <option value="开箱">开箱</option>
				            <option value="管道">管道</option>
				            <option value="低噪">低噪</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">是否是防爆电机</td>
                      <td>
                       <select id="z_sffbfj" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="是">是</option>
				            <option value="否">否</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">防爆外接接线盒厂家</td>
                      <td>
                       <input id="z_fbwjjxhcj" type="text" class="easyui-textbox" style="width:185px;height:32px;">
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">前防护网</td>
                      <td>
				          <select id="z_qfhw" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="有">有</option>
				            <option value="无">无</option>
				          </select>
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">前防护网间距</td>
                      <td>
				          <input type="text" id="z_qfhwjj" class="easyui-textbox" style="width:185px;height:32px;">(mm)
				       </td>
                    </tr>
                    <tr>
                      <td class="text-right">后防护网</td>
                      <td>
				          <select id="z_hfhw" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="有">有</option>
				            <option value="无">无</option>
				          </select>
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">后防护网间距</td>
                      <td>
				          <input type="text" id="z_hfhwjj" class="easyui-textbox" style="width:185px;height:32px;">(mm)
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">风筒内径</td>
                      <td>
				          <input type="text" id="z_ftnj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰1外径</td>
                      <td>
				          <input type="text" id="z_fl1wj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰1螺栓孔所在圆直径（叶轮端）</td>
                      <td>
				          <input type="text" id="z_fl1lskszyzj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰1螺栓孔直径</td>
                      <td>
				          <input type="text" id="z_fl1lskzj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰1螺栓孔个数</td>
                      <td>
				          <input type="text" id="z_fl1lskgs" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰2外径</td>
                      <td>
				          <input type="text" id="z_fl2wj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰2螺栓孔所在圆直径</td>
                      <td>
				          <input type="text" id="z_fl2lskszyzj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰2螺栓孔直径</td>
                      <td>
				          <input type="text" id="z_fl2lskzj" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">法兰2螺栓孔个数</td>
                      <td>
				          <input type="text" id="z_fl2lskgs" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">风筒长度</td>
                      <td>
				          <input type="text" id="z_ftcd" class="easyui-textbox" style="width:185px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">整机高度</td>
                      <td>
				          <input type="text" id="z_zjgd" class="easyui-textbox" style="width:185px;height:32px;">
			        	</td>
                    </tr>
                    <tr>
                      <td class="text-right">接线格兰型号</td>
                      <td>
				          M<input type="text" id="z_jxglxh" class="easyui-textbox" style="width:177px;height:32px;">
				        </td>
                    </tr>
                    <tr>
                      <td class="text-right">接线格兰材质</td>
                      <td>
				          <select id="z_jxglcz" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="塑料">塑料</option>
				            <option value="不锈钢">不锈钢</option>
				            <option value="其它金属">其它金属</option>
				          </select>
				        </td>
                    </tr>
                    
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="2"><div class="form-legend"><hr><span>离心风机结构参数</span><a href="javascript:void(0)" class="toggleSearchBtn" onclick="toggleSearchCon(this, 'searchCon4')">展开</a></div></td>
              </tr>
              <tr id="searchCon4" style="display: none;">
                <td colspan="2">
                  <table>
                    <tr>
                      <td class="text-right">机壳材质</td>
                      <td>
                        <select id="c_jkcz" class="easyui-combobox" style="width:185px;height:32px;">
				            <option value="">请选择</option>
				            <option value="碳钢">碳钢</option>
				            <option value="304">304</option>
				            <option value="316">316</option>
				            <option value="铝合金">铝合金</option>
				          </select>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-right">出口方向</td>
                      <td>
			          <input type="text" id="l_ckfx" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">宽</td>
                      <td>
			          <input type="text" id="l_k" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">长</td>
                      <td>
			          <input type="text" id="l_c" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">高</td>
                      <td>
			          <input type="text" id="l_g" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">中心高</td>
                      <td>
			          <input type="text" id="l_zxg" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">出口长</td>
                      <td>
			          <input type="text" id="l_ckc" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">出口宽</td>
                      <td>
			          <input type="text" id="l_ckk" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">出口安装孔直径</td>
                      <td>
			          <input type="text" id="l_ckazkzj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">出口安装孔数量</td>
                      <td>
			          <input type="text" id="l_ckzaksl" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">进口直径</td>
                      <td>
			          <input type="text" id="l_jkzj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">进口法兰外径</td>
                      <td>
			          <input type="text" id="l_jkflwj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">进口法兰安装孔所在圆直径</td>
                      <td>
			          <input type="text" id="l_jkflazkszyzj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">进口法兰安装孔直径</td>
                      <td>
			          <input type="text" id="l_jkflazkzj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">进口法兰安装孔数量</td>
                      <td>
			          <input type="text" id="l_jkflazksl" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">安装孔直径</td>
                      <td>
			          <input type="text" id="l_azkzj" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">安装孔数量</td>
                      <td>
			          <input type="text" id="l_azksl" class="easyui-textbox" style="width:185px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">接线格兰型号</td>
                      <td>
			          M<input type="text" id="l_jxglxh" class="easyui-textbox" style="width:177px;height:32px;">
			        </td>
                    </tr>
                    <tr>
                      <td class="text-right">接线格兰材质</td>
                      <td>
			          <select id="l_jxglcz" class="easyui-combobox" style="width:185px;height:32px;">
			            <option value="">请选择</option>
			            <option value="塑料">塑料</option>
			            <option value="不锈钢">不锈钢</option>
			            <option value="其它金属">其它金属</option>
			          </select>
			          </td>
			        </tr>
                  </table>
                </td>
              </tr>
              <tr>
                  <td colspan="2" style="text-align:center;">
                      <a id='btnQuery' href="#" class="easyui-linkbutton" plain="true" iconCls="fa fa-search" onclick="functionQuery()">搜索</a>          
                      <a id='btnClear' href="#" class="easyui-linkbutton"  plain="true" iconCls="fa fa-refresh" onclick="functionClear()" style="margin: 15px 0 15px 20px;">清除</a> 
                  </td>
                </tr>
            </table>     
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20,
                  onClickRow:onDblClickRow">
          </table>    
       </div>
    </div>
</body> 
</html>
<script type="text/javascript">
var user = <%=userJson%>;
console.log(user)
var searchFields = ['j_fjdl','j_cpxh','j_cdfs','j_yps','j_jd','d_djxh','d_djcj', 'd_djazxs', 'd_sfsfbdj', 'd_fbdj', 'd_edgl', 'd_edzs', 'd_eddy', 'd_eddl', 'd_dypl', 'd_glys', 'd_fhdj', 'd_jydj', 'd_jrd', 'd_dlcd',
 'd_zdwd', 'd_zgwd', 'd_yxzdsysd', 'd_yxzdsyhb', 'd_qzcxh', 'd_hzcxh', 'd_djnxdj', 'd_djkdcz', 'd_djzl', 'c_jkcz', 'c_ylcz', 'c_jfkcz', 'c_fhwcz', 'c_bzj', 'z_qlfx', 'z_tthd',
  'z_flhd', 'z_ftlx', 'z_sffbfj', 'z_fbwjjxhcj', 'z_qfhw', 'z_qfhwjj', 'z_hfhw', 'z_hfhwjj', 'z_ftnj', 'z_fl1wj', 'z_fl1lskszyzj', 'z_fl1lskzj', 'z_fl1lskgs', 'z_fl2wj',
   'z_fl2lskszyzj', 'z_fl2lskzj', 'z_fl2lskgs', 'z_ftcd', 'z_zjgd', 'z_jxglxh', 'z_jxglcz', 'l_ckfx', 'l_k', 'l_c', 'l_g', 'l_zxg', 'l_ckc', 'l_ckk', 'l_ckazkzj',
    'l_ckzaksl', 'l_jkzj', 'l_jkflwj', 'l_jkflazkszyzj', 'l_jkflazkzj', 'l_jkflazksl', 'l_azkzj', 'l_azksl', 'l_jxglxh', 'l_jxglcz'] // 搜索字段
if (user.position_id == 117 || user.position_id == 9) {
  $('#transshapeDiv').show()
}
// 表格数据
var tableData = []
// 表格表头
var columns = [[
  {
    field:'productNumber',
    title:'风机编号'
  },
  {
    field:'text',
    title:'风机名称'
  },
  {
    field:'yelunzhijing',
    title:'叶轮直径'
  },
  {
    field:'j_yps',
    title:'叶片数'
  },
  {
    field:'j_jd',
    title:'角度'
  },
  {
    field:'id',
    title:'操作',
    formatter: function (value,row,index) {
      return '<a onclick="collectData(' + index + ')">收藏</a>'
    }
  }
]]
$('#dg').datagrid({
	columns: columns
})

// 选择是否变形
function selectTransshape (flag) {
  if (flag == 1) {
    $('#ylzjDiv').show()
  } else {
    $('#ylzjDiv').hide()
  }
}
/*
  var obj = {"total":5,"rows":[{id:"1",fannumber:"11111",name:"轴流1号风机",model:"GZ-30",fanType:"轴流",blowingRate:"18000",totalHead:"3001",staticPressure:"",productiveness:"70"},
	  {id:"2",fannumber:"22222",name:"轴流2号风机",model:"GZ-45",fanType:"轴流",blowingRate:"18100",totalHead:"3215",staticPressure:"",productiveness:"79"},
	  {id:"3",fannumber:"33333",name:"离心风机1号",model:"CZ-12",fanType:"离心",blowingRate:"18500",totalHead:"3356",staticPressure:"",productiveness:"36"},
	  {id:"4",fannumber:"44444",name:"离心风机2号",model:"CZ-13",fanType:"离心",blowingRate:"21300",totalHead:"3412",staticPressure:"",productiveness:"54"},
	  {id:"5",fannumber:"55555",name:"排气风机",model:"CZ-14",fanType:"离心",blowingRate:"165000",totalHead:"3607",staticPressure:"",productiveness:"63"}]};*/
  $(function(){
	  //$('#dg').datagrid('loadData',obj);
//	  风筒板厚为 自定义时  可编辑
	  $('#fthb').combobox({
	  	onChange: function (n,o){
	  		var fthb =$('#fthb').val();
	  		if(fthb == 'zdy'){
	  			$('.ductThick').attr("disabled",false)
	  		} if(fthb !== 'zdy'){
	  			$('.ductThick').attr("disabled","disabled")
	  		}
	  	}
	  })
    // 获取机壳类型
    var fanData;
    $.ajax({
      url: '/develop/url/getUrl.do?name='+ 'getjikeleixing',
      type: 'get',
      dataType: 'json',
      data: {},
      success: function(data) {
        console.log(data)
        fanData = data
      }
    })
    // 获取风机安装形式
    var fanlist;
    $.ajax({
      url: '/develop/url/getUrl.do?name='+ 'getanzhuangxs',
      type: 'get',
      dataType: 'json',
      data: {},
      success: function(data) {
        console.log(data)
        fanlist = data
      }
    })
    // 风机类型类型为 轴流/离心
   /*  $('#fanType').combobox({
      onChange: function (n,o){
        var jikeValue = $('#fanType').val();
        console.log(jikeValue+"====="+  fanData.slice(0,3) )
        if (jikeValue == 2) { // 离心
          $('#jike').combobox({ // 机壳类型
            valueField:'id',
            textField:'text',
            data: fanData.slice(3,5)
          })
          $('#fjazxs').combobox({ // 风机安装形式
            valueField:'id',
            textField:'text',
            data: fanlist
          })
        } else {
          $('#jike').combobox({
            valueField:'id',    
            textField:'text',
            data: fanData.slice(0,3)
          })
          $('#fjazxs').combobox({
            valueField:'id',
            textField:'text',
            data: []
          })
        }
      }
    }) */
  });

  function addParameter()
  {

      var row = $('#dg').treegrid('getSelected');
      if(row == null)
      {
        layerMsgCustom('请选择要添加参数的风机');
        return;
      }

      layer.open({
          type: 2,
          title: '新增参数',
          shadeClose: false,
          shade: 0.3,
          maxmin: false, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '',
          content: 'addParameter.jsp?fanId='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function viewCurveGraph()
  {
      var row = $('#dg').datagrid('getSelected');  
        if(row == null)
        {
          layerMsgCustom('必须选择一个风机');
          return;
        }

        layer.open({
            type: 2,
            title: '查看性能曲线',
            shadeClose: false,
            shade: 0.3,
            maxmin: false, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            content: 'performancePurve.jsp?fanId='+row.id
        })
  }
  
  function uploadFanFile(){
	  var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('请选择风机');
        return;
      }

      layer.open({
          type: 2,
          title: '风机附件上传',
          shadeClose: false,
          shade: 0.3,
          maxmin: false, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'uploadFanFile.jsp?fanId='+row.id
      })
  }

	function customParam(){
		layer.open({
	        type: 2,
	        title: '自定义参数',
	        shadeClose: false,
	        shade: 0.3,
	        maxmin: false, //开启最大化最小化按钮
	        area: shuoheUtil.cpm_size(),
	        //btn: '保存',
	        content: 'parameter.jsp'
	    })
	}
  function edit()
  {

      var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/editUrl.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.msg('你确定删除么？', {
      time: 0, //不自动关闭
      btn: ['确定', '取消'],
      yes: function(index) {
        layer.close(index);
        shuoheUtil.layerTopMaskOn();
        $.ajax({
          url: "/develop/url/delete.do",
          type: "POST",
          dataType: 'json',
          data: {
            'id': row.id
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
              shuoheUtil.layerMsgSaveOK(data.describe);
            } else {
              shuoheUtil.layerMsgCustom(data.describe);
            }
            $('#dg').datagrid('reload');
          }
        });
      }
    });
  }


  function updateSuccess()
  {
    $('#dg').datagrid('reload'); 

  }
  function run()
  {
      var row = $('#dg').datagrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个URL');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'form/runUrl.jsp?name='+row.name        
      })
  }
  //执行风机选型方法进行风机选型
  var queryX,queryY;
  var fanSelectParam;
  var fanSelectParamObj // 选择参数对象
  function functionQuery(){
	  fanSelectParam = [];
	  var objParam = new Object();
	  var state = $('#state').combobox('getValue');
	  var val = $('input[name="windPressure"]:checked').val();//获取静压风压选中的值
	  var blowingRate = $('#blowingRate').textbox('getValue');
	  //var staticPressureQuery = $('#staticPressureQuery').textbox('getValue');
	  var totalHeadQuery = $('#totalHeadQuery').textbox('getValue');
	  var errorMinRange = $('#errorMinRange').textbox('getValue');
	  var errorMaxRange = $('#errorMaxRange').textbox('getValue');
	  var speedQuery = $('#speedQuery').textbox('getValue');//风机实际转速
	  var airDensityQuery = $('#airDensityQuery').textbox('getValue');//风机实际工作环境密度
   // var fanType = $('#fanType').combobox('getText');
   // var productType = $('#productType').combobox('getText');
	  // var reviseFactor = $('#reviseFactor').combobox('getText');

     // var drivingMode = $('#drivingMode').combobox('getText'); // 传动方式
     
     // var jikebanhou = $('#jikebanhou').combobox('getText'); // 机壳板厚
     // var jikeType = $('#jike').combobox('getText'); // 机壳类型
      //var fjazxs = $('#fjazxs').combobox('getText'); // 风机安装形式
      //var ckfhw = $('#ckfhw').combobox('getText'); // 出口防护网
      //var jkfhw = $('#jkfhw').combobox('getText'); // 进口防护网
      var minYlzj = $('#minYlzj').textbox('getText'); //叶轮直径最小值
      var maxYlzj = $('#maxYlzj').textbox('getText'); // 叶轮直径最大值
    

	 // var ifExplosionProof = $('#ifExplosionProof').combobox('getText');
	 // var voltage = $('#voltage').combobox('getText');
	 // var frequency = $('#frequency').combobox('getText');
	  //var levelsofprotection = $('#levelsofprotection').combobox('getText');
	  // var insulationGrade = $('#insulationGrade').combobox('getText');
	  // var airDuctply = $('#airDuctply').combobox('getText');
	  // var airDuctType = $('#airDuctType').combobox('getText');
	  // var aerationType = $('#aerationType').combobox('getText');
	  // var protectiveScreening = $('#protectiveScreening').combobox('getText');
	  // var authentication = $('#authentication').combobox('getText');
	 // var spaceHeater = $('#spaceHeater').combobox('getText');
	  if(blowingRate==""){
		  shuoheUtil.layerMsgCustom('风量必须填写');
		  return;
	  }else{
		  var obj = {};
		  obj.key = "风量";
		  obj.value = blowingRate;
		  fanSelectParam.push(obj);
	  }
	  if(totalHeadQuery==""){
		  shuoheUtil.layerMsgCustom('风压必须填写');
		  return;
	  }else{
		  var obj = {};
		  obj.key = val;
		  obj.value = totalHeadQuery;
		  fanSelectParam.push(obj);
	  }
	  if(errorMinRange==""){
		  shuoheUtil.layerMsgCustom('误差最小范围必须设置');  
		  return;
	  }
	  if(errorMaxRange==""){
		  shuoheUtil.layerMsgCustom('误差最大范围必须设置');  
		  return;
	  }else{
		  var obj = {};
		  obj.key = "误差范围";
		  obj.value = errorMinRange+"~"+errorMaxRange; 
		  fanSelectParam.push(obj);
	  }
	  if(airDensityQuery==""){
		  shuoheUtil.layerMsgCustom('风机密度必须输入');  
		  return;
	  }else{
		  var obj = {};
		  obj.key = "风机密度";
		  obj.value = airDensityQuery;
		  fanSelectParam.push(obj);
	  }
	  
	  if ($('input[name="isTransshape"]:checked').val() == '1') {
	      if(minYlzj==""){
	        shuoheUtil.layerMsgCustom('叶轮直径最小值必须输入');  
	        return;
	      }
	      if(maxYlzj==""){
	        shuoheUtil.layerMsgCustom('叶轮直径最大值必须输入');  
	        return;
	      } else {
	        var obj = {};
	        obj.key = "叶轮直径范围";
	        obj.value = minYlzj+"~"+maxYlzj;
	        fanSelectParam.push(obj);
	      }
	      
	      if(speedQuery==""){
			  shuoheUtil.layerMsgCustom('风机实际要求转速必须输入');  
			  return;
		  }else{
			  var obj = {};
			  obj.key = "风机转速";
			  obj.value = speedQuery;
			  fanSelectParam.push(obj);
		  }
      }
	  
	  objParam.state = state;
	  objParam.selectWindPresssure = val;
	  objParam.blowingRate = blowingRate;
	  //objParam.staticPressureQuery = staticPressureQuery;
	  objParam.totalHeadQuery = totalHeadQuery;
	  queryX = blowingRate;
	  queryY = totalHeadQuery;
	  objParam.errorRange = errorMinRange+$('#rangeIdentification').text()+errorMaxRange;
	  objParam.airDensityQuery = airDensityQuery;
	  objParam.speedQuery = speedQuery;
      //objParam.jikebanhou = jikebanhou; // 机壳板厚
      //objParam.jikeleixing = jikeType; // 机壳类型
      //objParam.fengjianzhuangxingshi = fjazxs; // 风机安装形式
      //objParam.chukoufanghuwang = ckfhw; // 出口防护网
      //objParam.jinkoufanghuwang = jkfhw; // 进口防护网
     // objParam.fanType = fanType;
	  //objParam.productType = productType;    
	  // objParam.reviseFactor = reviseFactor;
	  //objParam.ifExplosionProof = ifExplosionProof;
	  //objParam.voltage = voltage;
	  //objParam.frequency = frequency;
	  //objParam.levelsofprotection = levelsofprotection;
	  // objParam.insulationGrade = insulationGrade;
	  // objParam.airDuctply = airDuctply;
	  // objParam.airDuctType = airDuctType;
	  // objParam.aerationType = aerationType;
	  // objParam.protectiveScreening = protectiveScreening;
	  // objParam.authentication = authentication;
	  //objParam.spaceHeater = spaceHeater;
	  objParam.minYlzj = minYlzj;
      objParam.maxYlzj = maxYlzj;
      fanSelectParamObj = objParam
    var obj = new Object()
    for (var i = 0; i < searchFields.length; i++) {
      obj[searchFields[i]] = $("#" + searchFields[i]).textbox("getValue")
    }
    //alert(obj);
    var url = $('input[name="isTransshape"]:checked').val() == '1' ? '/shop/sort/getFanSelectList2.do' : '/shop/sort/getFanSelectList.do'
	  $.ajax({
          url: url,
          type: "POST",
          dataType: 'json',
          data: {
            'paramJson': JSON.stringify(objParam),
            'obj':JSON.stringify(obj)
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            if(data.result){
                if(data.object.total==0){
                	shuoheUtil.layerMsgCustom('抱歉，没有符合你条件的风机');
                	$('#dg').datagrid('loadData',{total:0,rows:[]});
                }else{
                  tableData = data.object.rows
                	shuoheUtil.layerMsgCustom('符合您条件的风机共有:'+data.object.total+"个");
                	$('#dg').datagrid('loadData',data.object.rows);
                }
            	
            }else{
            	alert("异常，获取数据失败");
            }
          }
        });
  }
  
  var fanNumber;
  function onDblClickRow(rowIndex, rowData){
	  fanNumber = rowData.productNumber;
	  layer.open({
          type: 2,
          title: '风机参数',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '',
          content: 'showParam.jsp?fanId='+rowData.id+'&queryX='+queryX+'&queryY='+queryY+'&isTransshape='+$('input[name="isTransshape"]:checked').val(),
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  //清空搜索框中的值
  function functionClear(){
	$('#blowingRate').textbox('clear');
	//var staticPressureQuery = $('#staticPressureQuery').textbox('clear');
	$('#totalHeadQuery').textbox('clear');
	/* $('#errorMinRange').textbox('clear');
	$('#errorMaxRange').textbox('clear'); */
	$('#airDensityQuery').textbox('clear');
  /* $('#fanType').combobox('clear');
  $('#productType').combobox('clear');
	$('#reviseFactor').combobox('clear');
	$('#ifExplosionProof').combobox('clear');
	$('#voltage').combobox('clear');
	$('#frequency').combobox('clear');
	$('#levelsofprotection').combobox('clear');
	$('#insulationGrade').combobox('clear');
	$('#airDuctply').combobox('clear');
	$('#airDuctType').combobox('clear');
	$('#aerationType').combobox('clear');
	$('#protectiveScreening').combobox('clear');
	$('#authentication').combobox('clear');
	$('#spaceHeater').combobox('clear'); */
  }
  // 切换显示搜索框
  function toggleSearchCon (that, id) {
    var str = $(that).text()
    var temp = str === '展开' ? '收起' : '展开'
    $(that).text(temp)
    $('#' + id).toggle()
  }
  // 收藏
  function collectData (index) {
    var obj = {
      fanId: tableData[index].id,
      userId: user.id,
      productNumber: tableData[index].productNumber,
      text: tableData[index].text,
      yelunzhijing: tableData[index].yelunzhijing !== undefined ? tableData[index].yelunzhijing : '',
      j_yps: tableData[index].j_yps !== undefined ? tableData[index].j_yps : '',
      j_jd: tableData[index].j_jd !== undefined ? tableData[index].j_jd : '',
      isTransshape: $('input[name="isTransshape"]:checked').val(),
      blowingRate: $('#blowingRate').textbox('getValue'),
      state: $('#state').combobox('getValue'),
      windPressure: $('input[name="windPressure"]:checked').val(),
      totalHeadQuery: $('#totalHeadQuery').textbox('getValue'),
      errorMinRange: $('#errorMinRange').textbox('getValue'),
      errorMaxRange: $('#errorMaxRange').textbox('getValue'),
      speedQuery: $('#speedQuery').textbox('getValue'),
      airDensityQuery: $('#airDensityQuery').textbox('getValue'),
      minYlzj: $('#minYlzj').textbox('getText'),
      maxYlzj: $('#maxYlzj').textbox('getText')
    }
    console.log(obj)
    $.ajax({
      url: "/fanOfferCollect/save",
      type: "POST",
      dataType: 'json',
      data: {
        data: JSON.stringify(obj)
      },
      error: function() //失败
      {
        shuoheUtil.layerTopMaskOff();
      },
      success: function(data) //成功
      {
        console.log(data)
        if (data.result == true) {
          shuoheUtil.layerMsgSaveOK(data.describe);
        } else {
          shuoheUtil.layerMsgCustom(data.describe);
        }
      }
    })
    event.stopPropagation()
  }
</script>
