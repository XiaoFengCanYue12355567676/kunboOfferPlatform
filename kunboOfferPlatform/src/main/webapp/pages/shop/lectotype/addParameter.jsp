<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.shuohe.entity.system.user.User"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = UUID.randomUUID().toString();
	User user = null;
	String userJson = null;
	try
	{
	    user = (User)session.getAttribute("user");
	    if(user==null)
	    {
	      System.out.println("user = null");
	      String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    }
	    else
	    {
	        userJson = Json.toJsonByPretty(user);
	    }
	}
	catch(Exception e)
	{
	    String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    e.printStackTrace();
	}
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改风机参数</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>
    <link type="text/css" href="/pages/css/add-form.css" rel="stylesheet"/>  
    <!-- 文件上传 -->
    <link href="<%=basePath%>/pages/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>
    <link href="<%=basePath%>/pages/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">  
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
	<script type="text/javascript" src="/pages/js/jeDate/jedate.min.js"></script>
	<script type="text/javascript" src="/pages/js/add-form.js"></script>
	<script type="text/javascript" src="/pages/js/echarts.min.js"></script>
	
	<!--引入文件上传JS-->
	<script type="text/javascript" src="<%=basePath%>/pages/fileUpload/js/fileUpload.js"></script>
	<script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>
	
    
<style type="text/css">
html, body{ margin:0; height:100%;width: 100%}
.fileUp-con{
float: left;
width: 25%;
}
.fileUp-title{
    text-align: center;
    line-height: 2;
    font-size: 16px;
}
.datagrid-body{
	overflow: hidden;
  
}
.noiseTop{
  border-left: 1px solid black;
  padding: 0;
}
li{
  list-style: none;
  height: 50px;
  line-height: 50px;
  text-align: center;
}
.noiseTop >li{
  border-right: 1px solid black;
  border-top: 1px solid black;
  float: left;
}
.noiseCenter{
  width: 100%;
}

</style>


<body class="easyui-layout">
 <div class="easyui-tabs" style="overflow-x:hidden;height: 100%;">
   <div title="性能参数" data-options="region:'center',closable:false" style="margin: 0 auto;height: 100%;">
    <table id="dg" style="height: 100%;"></table>
   </div>
   
   <div class="field-con" title="其他参数" data-options="closable:false" style="margin-left: 10px;position: relative;">
   	<div id="fieldCon"></div>
      <a href="#" class="button button-primary button-rounded button-small" onclick="saveOtherParam()" style="position: absolute;bottom: 15px;right: 27px;">保存</a>
   </div>
   <div class="field-con" title="补充参数" data-options="closable:false" style="margin-left: 10px;position: relative;">
    <div class="supplement-param-form">
      <div class="formThreeColumn">
        <div class="form-legend"><hr><span>基本信息</span></div>
        <div class="form-order-line">
          <label>风机大类</label>
          <select id="j_fjdl" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="轴流风机">轴流风机</option>
            <option value="离心风机">离心风机</option>
            <option value="斜流风机">斜流风机</option>
            <option value="离心轴流">离心轴流</option>
            <option value="轴流叶轮">轴流叶轮</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>产品类型</label>
          <select id="j_cpxh" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="船用轴流风机">船用轴流风机</option>
            <option value="船用离心风机">船用离心风机</option>
            <option value="核电轴流风机">核电轴流风机</option>
            <option value="制冷轴流风机">制冷轴流风机</option>
            <option value="制冷离心风机">制冷离心风机</option>
            <option value="空冷器风机">空冷器风机</option>
            <option value="冷却塔叶轮">冷却塔叶轮</option>
            <option value="机车离心风机">机车离心风机</option>
            <option value="机车轴流风机">机车轴流风机</option>
            <option value="无蜗壳离心风机">无蜗壳离心风机</option>
            <option value="风电轴流风机">风电轴流风机</option>
            <option value="风电离心风机">风电离心风机</option>
            <option value="核电轴流风机">核电轴流风机</option>
            <option value="核电离心风机">核电离心风机</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>传动方式</label>
          <select id="j_cdfs" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="直联">直联</option>
            <option value="皮带传动">皮带传动</option>
            <option value="减速机">减速机</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>叶片数</label>
          <input type="text" id="j_yps" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>角度</label>
          <input type="text" id="j_jd" class="easyui-textbox" style="width: 175px" />(°)
        </div>
        <div class="form-order-line">
          <label>测试报告</label>
          <input type="text" id="j_csbg" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>整机重量</label>
          <input type="text" id="j_zjzl" class="easyui-textbox" style="width: 175px" />(kg)
        </div>
        <div class="form-order-line">
          <label>外形图号</label>
          <input type="text" id="j_wxth" class="easyui-textbox" style="width: 175px" />
        </div>        
        <div class="clear"></div>
        
        <div class="form-legend"><hr><span>电机参数</span></div>
        <div class="form-order-line">
          <label>电机型号</label>
          <input type="text" id="d_djxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机厂家</label>
          <input type="text" id="d_djcj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机安装型式</label>
          <input type="text" id="d_djazxs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>是否是防爆电机</label>
          <select id="d_sfsfbdj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="是">是</option>
            <option value="否">否</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>防爆等级</label>
          IP<input type="text" id="d_fbdj" class="easyui-textbox" style="width: 161px" />
        </div>
        <div class="form-order-line">
          <label>额定功率</label>
          <input type="text" id="d_edgl" class="easyui-textbox" style="width: 175px" />(kw)
        </div>
        <div class="form-order-line">
          <label>额定转速</label>
          <input type="text" id="d_edzs" class="easyui-textbox" style="width: 175px" />(rpm)
        </div>
        <div class="form-order-line">
          <label>额定电压</label>
          <input type="text" id="d_eddy" class="easyui-textbox" style="width: 175px" />(V)
        </div>
        <div class="form-order-line">
          <label>额定电流</label>
          <input type="text" id="d_eddl" class="easyui-textbox" style="width: 175px" />(A)
        </div>
        <div class="form-order-line">
          <label>电压频率</label>
          <input type="text" id="d_dypl" class="easyui-textbox" style="width: 175px" />(Hz)
        </div>
        <div class="form-order-line">
          <label>功率因数</label>
          <input type="text" id="d_glys" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>防护等级</label>
          <input type="text" id="d_fhdj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>绝缘等级</label>
          <input type="text" id="d_jydj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>加热带</label>
          <select id="d_jrd" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>电缆长度</label>
          <input type="text" id="d_dlcd" class="easyui-textbox" style="width: 175px" />(m)
        </div>
        <div class="form-order-line">
          <label>最低使用温度</label>
          <input type="text" id="d_zdwd" class="easyui-textbox" style="width: 175px" />(℃)
        </div>
        <div class="form-order-line">
          <label>最高使用温度</label>
          <input type="text" id="d_zgwd" class="easyui-textbox" style="width: 175px" />(℃)
        </div>
        <div class="form-order-line">
          <label>允许最大使用湿度</label>
          <input type="text" id="d_yxzdsysd" class="easyui-textbox" style="width: 175px" />(%)
        </div>
        <div class="form-order-line">
          <label>允许最大使用海拔</label>
          <input type="text" id="d_yxzdsyhb" class="easyui-textbox" style="width: 175px" />(m)
        </div>
        <div class="form-order-line">
          <label>前轴承型号</label>
          <input type="text" id="d_qzcxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>后轴承型号</label>
          <input type="text" id="d_hzcxh" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机能效等级</label>
          <input type="text" id="d_djnxdj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机壳体材质</label>
          <input type="text" id="d_djkdcz" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>电机重量</label>
          <input type="text" id="d_djzl" class="easyui-textbox" style="width: 175px" />(kg)
        </div>
        <div class="clear"></div>
        <div class="form-legend"><hr><span>材质及表面处理</span></div>
        <div class="form-order-line">
          <label>机壳材质</label>
           <select id="c_jkcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="碳钢">碳钢</option>
            <option value="304">304</option>
            <option value="316">316</option>
            <option value="铝合金">铝合金</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>叶轮材质</label>
          <select id="c_ylcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="碳钢">碳钢</option>
            <option value="304">304</option>
            <option value="316">316</option>
            <option value="铝合金">铝合金</option>
            <option value="塑料">塑料</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>进风口材质</label>
          <input type="text" id="c_jfkcz" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line">
          <label>防护网材质</label>
          <select id="c_fhwcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="不锈钢">不锈钢</option>
            <option value="喷漆">喷漆</option>
            <option value="热浸锌">热浸锌</option>
          </select>
        </div>
        <div class="form-order-line">
          <label>标准件</label>
          <select id="c_bzj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="A2-70">A2-70</option>
            <option value="A4-70">A4-70</option>
            <option value="热浸锌">热浸锌</option>
            <option value="镀锌">镀锌</option>
            <option value="发黑">发黑</option>
          </select>
        </div>
        <div class="clear"></div>
        <div class="form-legend zljg"><hr><span>轴流风机结构参数</span></div>
        <div class="form-order-line zljg">
          <label>气流方向</label>
          <select id="z_qlfx" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="前吹">前吹</option>
            <option value="后吹">后吹</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>筒体厚度</label>
          <input type="text" id="z_tthd" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>法兰厚度</label>
          <input type="text" id="z_flhd" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>风筒类型</label>
          <select id="z_ftlx" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="整体">整体</option>
            <option value="开箱">开箱</option>
            <option value="管道">管道</option>
            <option value="低噪">低噪</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>是否是防爆电机</label>
          <select id="z_sffbfj" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="是">是</option>
            <option value="否">否</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>防爆外接接线盒厂家</label>
          <input type="text" id="z_fbwjjxhcj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>前防护网</label>
          <select id="z_qfhw" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>前防护网间距</label>
          <input type="text" id="z_qfhwjj" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>后防护网</label>
          <select id="z_hfhw" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="有">有</option>
            <option value="无">无</option>
          </select>
        </div>
        <div class="form-order-line zljg">
          <label>后防护网间距</label>
          <input type="text" id="z_hfhwjj" class="easyui-textbox" style="width: 175px" />(mm)
        </div>
        <div class="form-order-line zljg">
          <label>风筒内径</label>
          <input type="text" id="z_ftnj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1外径</label>
          <input type="text" id="z_fl1wj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔所在圆直径（叶轮端）</label>
          <input type="text" id="z_fl1lskszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔直径</label>
          <input type="text" id="z_fl1lskzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰1螺栓孔个数</label>
          <input type="text" id="z_fl1lskgs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2外径</label>
          <input type="text" id="z_fl2wj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔所在圆直径</label>
          <input type="text" id="z_fl2lskszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔直径</label>
          <input type="text" id="z_fl2lskzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>法兰2螺栓孔个数</label>
          <input type="text" id="z_fl2lskgs" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>风筒长度</label>
          <input type="text" id="z_ftcd" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>整机高度</label>
          <input type="text" id="z_zjgd" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line zljg">
          <label>接线格兰型号</label>
          M<input type="text" id="z_jxglxh" class="easyui-textbox" style="width: 167px" />
        </div>
        <div class="form-order-line zljg">
          <label>接线格兰材质</label>
          <select id="z_jxglcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="塑料">塑料</option>
            <option value="不锈钢">不锈钢</option>
            <option value="其它金属">其它金属</option>
          </select>
        </div>
        <div class="clear zljg"></div>
        <div class="form-legend lxjg"><hr><span>离心风机结构参数</span></div>
        <div class="form-order-line lxjg">
          <label>出口方向</label>
          <input type="text" id="l_ckfx" class="easyui-textbox" style="width: 175px" />
        </div>
        <!-- 传动方式不用  l_cdfs-->
        <div class="form-order-line lxjg">
          <label>宽</label>
          <input type="text" id="l_k" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>长</label>
          <input type="text" id="l_c" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>高</label>
          <input type="text" id="l_g" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>中心高</label>
          <input type="text" id="l_zxg" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口长</label>
          <input type="text" id="l_ckc" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口宽</label>
          <input type="text" id="l_ckk" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口安装孔直径</label>
          <input type="text" id="l_ckazkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>出口安装孔数量</label>
          <input type="text" id="l_ckzaksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口直径</label>
          <input type="text" id="l_jkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰外径</label>
          <input type="text" id="l_jkflwj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔所在圆直径</label>
          <input type="text" id="l_jkflazkszyzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔直径</label>
          <input type="text" id="l_jkflazkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>进口法兰安装孔数量</label>
          <input type="text" id="l_jkflazksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>安装孔直径</label>
          <input type="text" id="l_azkzj" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>安装孔数量</label>
          <input type="text" id="l_azksl" class="easyui-textbox" style="width: 175px" />
        </div>
        <div class="form-order-line lxjg">
          <label>接线格兰型号</label>
          M<input type="text" id="l_jxglxh" class="easyui-textbox" style="width: 167px" />
        </div>
        <div class="form-order-line lxjg">
          <label>接线格兰材质</label>
          <select id="l_jxglcz" class="easyui-combobox" style="width: 175px">
            <option value="">请选择</option>
            <option value="塑料">塑料</option>
            <option value="不锈钢">不锈钢</option>
            <option value="其它金属">其它金属</option>
          </select>
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <div style="text-align: right;">
      <a href="#" class="button button-primary button-rounded button-small" onclick="saveSupplementParam()">保存</a>
    </div>
   </div>
   <!-- <div title="文件上传" style="height: 100%;margin-top: 30px;">
	<div class="fileUp-con"><div id="fileUploadContent" class="fileUploadContent" style="height:255px;overflow:auto"></div><div class="fileUp-title" id="airOutline">空气略图</div></div>
	<div class="fileUp-con"><div id="fileUploadContent1" class="fileUploadContent" style="height:255px;overflow:auto"></div><div class="fileUp-title" id="installationDiagram" >安装简图</div></div>
	<div class="fileUp-con"><div id="fileUploadContent2" class="fileUploadContent" style="height:255px;overflow:auto"></div><div class="fileUp-title" id="introductionChart">产品介绍</div></div>
	<div class="fileUp-con"><div id="fileUploadContent3" class="fileUploadContent" style="height:255px;overflow:auto"></div><div class="fileUp-title" id="otherMsg">其他信息</div></div>
   </div> -->
   <div title="噪音特性" style="height: 99%;margin-top: 30px;">
   <div class="noiseBox" style="width:100%;padding-left: 5%;">
    <ul class="noiseTop" style="width:100%">
      <li style="width:10%;">类型</li>
      <li style="width:10%;">63</li>
      <li style="width:10%;">125</li>
      <li style="width:10%;">250</li>
      <li style="width:10%;">500</li>
      <li style="width:10%;">1000</li>
      <li style="width:10%;">2000</li>
      <li style="width:10%;">4000</li>
      <li style="width:10%;">8000</li>
      <div style="clear: both"></div>
    </ul>
    <ul class="noiseTop" style="width:100%">
      <li style="width:10%;">PWL(dB)</li>
      <li style="width:10%;"><input class="easyui-numberbox noise63 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise125 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise250 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise500 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise1000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise2000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise4000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;"><input class="easyui-numberbox noise8000 noise" type="text" data-options="precision:2" /></li>
      <div style="clear: both"></div>
    </ul>
    <ul class="noiseTop" style="width:100%">
      <li style="width:10%;border-bottom: 1px solid black;">SPL(dB)</li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises63 noise" type="text" data-options="precision:2"  /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises125 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises250 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises500 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises1000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises2000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises4000 noise" type="text" data-options="precision:2" /></li>
      <li style="width:10%;border-bottom: 1px solid black;"><input class="easyui-numberbox noises8000 noise" type="text" data-options="precision:2" /></li>
      <div style="clear: both"></div>
    </ul>
   </div>
    <!--<form id="ff" method="post">
      
    </form> -->
    <div>
      <a href="#" class="button button-primary button-rounded button-small" onclick="noiseSave()" style="position: absolute;bottom: 15px;right: 27px;">提交</a>
    </div>
   </div>
   <div title="外形图" style="height: 99%;margin-top: 30px;">
  <div id="appearancePicture" class="fileUploadContent" style="height:255px;overflow:auto"></div>
  <div class="fielImg">
      <img src="" alt="" id="processImg" style="max-width:600px;max-height:600px">
  </div>
  
   </div>
 </div>
  
  <div id="tb">
	<a href="#" class="easyui-linkbutton" onclick="append()" data-options="iconCls:'fa fa-plus',plain:true">新增</a>
	<a href="#" class="easyui-linkbutton" onclick="save()" data-options="iconCls:'fa fa-floppy-o',plain:true">保存</a>
	<a href="#" class="easyui-linkbutton" onclick="removeit()" data-options="iconCls:'fa fa-trash',plain:true">删除</a>
  </div>
</body> 
</html>
<script type="text/javascript">
var fanId = shuoheUtil.getUrlHeader().fanId;
var fanNumber = shuoheUtil.getUrlHeader().fanNumber;
console.log(fanId);
var user = <%=userJson%>;
var uuid = '<%=uuid%>'
var formObj
var fields = []
var hasParam = false;
var hasSupplementParam = false // 是否有风机补充参数数据
var result;//接收其他参数返回的数据
var formJson = ''
var title = 'shop_fan_other_param'
var basePath = '<%=basePath%>'
var supplementParamObj = {} // 风机补充参数数据对象
var supplementParams = ['j_fjdl','j_cpxh','j_cdfs','j_yps','j_jd','j_csbg','j_zjzl','j_wxth','d_djxh','d_djcj', 'd_djazxs', 'd_sfsfbdj', 'd_fbdj', 'd_edgl', 'd_edzs', 'd_eddy', 'd_eddl', 'd_dypl', 'd_glys', 'd_fhdj', 'd_jydj', 'd_jrd', 'd_dlcd',
 'd_zdwd', 'd_zgwd', 'd_yxzdsysd', 'd_yxzdsyhb', 'd_qzcxh', 'd_hzcxh', 'd_djnxdj', 'd_djkdcz', 'd_djzl', 'c_jkcz', 'c_ylcz', 'c_jfkcz', 'c_fhwcz', 'c_bzj', 'z_qlfx', 'z_tthd',
  'z_flhd', 'z_ftlx', 'z_sffbfj', 'z_fbwjjxhcj', 'z_qfhw', 'z_qfhwjj', 'z_hfhw', 'z_hfhwjj', 'z_ftnj', 'z_fl1wj', 'z_fl1lskszyzj', 'z_fl1lskzj', 'z_fl1lskgs', 'z_fl2wj',
   'z_fl2lskszyzj', 'z_fl2lskzj', 'z_fl2lskgs', 'z_ftcd', 'z_zjgd', 'z_jxglxh', 'z_jxglcz', 'l_ckfx', 'l_k', 'l_c', 'l_g', 'l_zxg', 'l_ckc', 'l_ckk', 'l_ckazkzj',
    'l_ckzaksl', 'l_jkzj', 'l_jkflwj', 'l_jkflazkszyzj', 'l_jkflazkzj', 'l_jkflazksl', 'l_azkzj', 'l_azksl', 'l_jxglxh', 'l_jxglcz'] // 风机补充参数字段
$(function(){
  // 外形图获取
  $("#processImg").attr("src","/fanFile/preview.do?fan_number="+fanId);

  $('#main').css('width',document.body.clientWidth + 'px')
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      formObj = data
      fields = formObj.field
      console.log(fields)
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
      //隐藏信息---开始
      //$('#fengjileixing').parent().css('display','none');
      //$('#chanpinleixing').parent().css('display','none');
      //隐藏信息---结束
      
      initFormOrderComboboxText(formObj,uuid)
      //隐藏信息---开始
      $('#fengjileixing').parent().css('display','none');
      $('#chanpinleixing').parent().css('display','none');
      $('#shifufangbao').parent().css('display','none');
      $('#dianya').parent().css('display','none');
      $('#pinlv').parent().css('display','none');
      $('#fanghudengji').parent().css('display','none');
      $('#chuandongfangshi').parent().css('display','none');
      $('#jikebanhou').parent().css('display','none');
      $('#jikeleixing').parent().css('display','none');
      $('#fengjianzhuangxingshi').parent().css('display','none');
      $('#jinkoufanghuwang').parent().css('display','none');
      $('#chukoufanghuwang').parent().css('display','none');
      $('#kongjianjiareqi').parent().css('display','none');
      //隐藏信息---结束
      //选择风机大类
      var jgType = $('#jikeleixing').combobox('getData');
      $('#fengjileixing').combobox({
        onChange: function(newValue,oldValue){
          if(newValue == "轴流风机"){
              $('#fengjianzhuangxingshi').parent().css('display','none') // 风机安装形式不显示
              var jgType1 = jgType.slice(2,5) // 机壳类型取前两个
              $('#jikeleixing').combobox({
                data: jgType1
              })
            } else {
              $('#fengjianzhuangxingshi').parent().css('display','block')
              var jgType2 = jgType.slice(0,2) // 机壳类型取后三个
              $('#jikeleixing').combobox({
                data: jgType2
              })
            }
        }
      });
      $('#jihao').parent().append('(cm)')
	  $('#yelunzhijing').parent().append('(m)')
	  
    }
  })
  //风机大类切换事件
  $('#j_fjdl').combobox({
      onChange: function(newValue,oldValue){
        if(newValue == "轴流风机"){
        	$(".zljg").show();
  		    $(".lxjg").hide();
        }else if(newValue == "离心风机"){
        	$(".zljg").hide();
  		  $(".lxjg").show();
        }else{
		  $(".zljg").show();
		  $(".lxjg").show();
	    }
      }
    });
  
  //js渲染性能曲线图
  //loadCurveChart();
	console.log(fanId)
  getFanOtherParam(fanId);//加载风机参数
  getFanSupplementParam(fanId) // 加载风机补充参数

  // 噪音特性数据展示
  $.ajax({
        url: "<%=basePath%>/shop/product/getProductSortEntity.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        productId: fanId
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          $('.noise125').numberbox('setValue',data.noise125),
          $('.noises125').numberbox('setValue',data.noise125s),
          $('.noise63').numberbox('setValue',data.noise63),
          $('.noises63').numberbox('setValue',data.noise63s),
          $('.noise250').numberbox('setValue',data.noise250),
          $('.noises250').numberbox('setValue',data.noise250s),
          $('.noise500').numberbox('setValue',data.noise500),
          $('.noises500').numberbox('setValue',data.noise500s),
          $('.noise1000').numberbox('setValue',data.noise1KHz),
          $('.noises1000').numberbox('setValue',data.noise1KHzs),
          $('.noise2000').numberbox('setValue',data.noise2KHz),
          $('.noises2000').numberbox('setValue',data.noise2KHzs),
          $('.noise4000').numberbox('setValue',data.noise4KHz),
          $('.noises4000').numberbox('setValue',data.noise4KHzs),
          $('.noise8000').numberbox('setValue',data.noise8KHz),
          $('.noises8000').numberbox('setValue',data.noise1KHzs)
        }
      })
})


   
// 噪音特性添加

function noiseSave () {
  var check = false;
  var noiseData = {
    'noise125': $('.noise125').numberbox('getValue'),
    'noise125s': $('.noises125').numberbox('getValue'),
    'noise63': $('.noise63').numberbox('getValue'),
    'noise63s': $('.noises63').numberbox('getValue'),
    'noise63s': $('.noises63').numberbox('getValue'),
    'noise250': $('.noise250').numberbox('getValue'),
    'noise250s': $('.noises250').numberbox('getValue'),
    'noise250s': $('.noises250').numberbox('getValue'),
    'noise500': $('.noise500').numberbox('getValue'),
    'noise500s': $('.noises500').numberbox('getValue'),
    'noise1KHz': $('.noise1000').numberbox('getValue'),
    'noise1KHzs': $('.noises1000').numberbox('getValue'),
    'noise2KHz': $('.noise2000').numberbox('getValue'),
    'noise2KHzs': $('.noises2000').numberbox('getValue'),
    'noise4KHz': $('.noise4000').numberbox('getValue'),
    'noise4KHzs': $('.noises4000').numberbox('getValue'),
    'noise8KHz': $('.noise8000').numberbox('getValue'),
    'noise8KHzs': $('.noises8000').numberbox('getValue'),
  }
  var empty = $('.noise');
  console.log(empty)
    if (checkInputsValue(empty)){
      $.ajax({
        url: "/shop/product/saveNoise.do",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          productId: fanId,
          data: JSON.stringify(noiseData)
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          console.log(data)
          alert('添加成功')
        }
      })
    } else {
      alert('不允许数据为空')
    }
}
// 验证input为空
function checkInputsValue(req){
  var result = true;
  if(req){
    req.each(function(){
      var input = $(this).val();			
      if(!input){
        result = false;
        return false;
      }
    });
  }else{
    result = false;
  }
  return result;
}


// 其他参数
function fielData1() {
  var temp = '';
  for (var i=0;i<fields.length;i++){
    var field = fields[i];
    console.log(field)
    if (field.fieldType == 'textbox') {
      console.log(result[field.text])
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").textbox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").textbox("setValue", "' + result[field.text] + '");'
      }
    } else {
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").combobox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").combobox("setValue", "' + result[field.text] + '");'
      }
    }
  }
  var script = document.createElement("script")
  script.type = "text/javascript"
  try {
    script.appendChild(document.createTextNode(temp))
  } catch (ex) {
    script.text = temp
  }
  document.body.appendChild(script)
  specialJSFlag++
}

function getFanOtherParam(fanId){
	$.ajax({
        url: "/capabilityParam/getDataByFanId",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          'tableName': title,
          'fanId':fanId
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          console.log(data)
        	if(!data.rows.length==0){
        		hasParam = true;
        		result = data.rows[0];
            console.log(result)
            // initFieldData(true);
            fielData1()
        	}
        }
      })
}
// 加载风机补充参数
function getFanSupplementParam (fanId) {
  $.ajax({
    url: "/FanSupplementParam/findByFanId",
    type: "POST",
    dataType:'json',
    async:false,
    data:
    {
      'fanId':fanId
    },
    error: function() //失败
    {
      // messageloadError()
    },
    success: function(data)//成功
    {
      console.log(data)
      if(data){ // 有数据
        hasSupplementParam = true;
        supplementParamObj = data
        initSupplementParam(true)
      }
    }
  })
}
// 风机补充参数填充
function initSupplementParam () {
  for (var i = 0; i < supplementParams.length; i++) {
    if (supplementParamObj[supplementParams[i]] !== null) {
      $("#" + supplementParams[i]).textbox("setValue", supplementParamObj[supplementParams[i]])
      if (supplementParams[i] == 'j_fjdl') {
    	  if(supplementParamObj[supplementParams[i]] == "轴流风机"){
          		$(".zljg").show();
    		    $(".lxjg").hide();
          }else if(supplementParamObj[supplementParams[i]] == "离心风机"){
          		$(".zljg").hide();
    		  $(".lxjg").show();
          }else{
	  		  $(".zljg").show();
	  		  $(".lxjg").show();
	  	  }
      }
    }
  }
}

// 保存
function saveOtherParam () {
    if(hasParam){
    	updateParam();
    }else{
    	insert();
    }
}

function insert(){
	var obj = new Object()
    obj.title = formObj.title
    obj = getValue(fields, obj)
    obj.field.push({'text':'create_user_id','value': "'"+user.id+"'"})
    obj.field.push({'text':'taskid','value':'0'})
    obj.field.push({'text':'uuid','value':"'"+uuid+"'"})
    obj.field.push({'text':'fanId','value':"'"+fanId+"'"})
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormUtil/insert.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
    	data = $.trim(data)
    	  if(data == '1'){
        	alert("保存成功");
        }else{
			  	 messageSaveError();
        }
      }
    })
}

function updateParam () {
    var obj = new Object()
    obj.title = formObj.title
    obj = getValue(fields, obj)
    obj.field.push({'text':'uuid','value':"'"+result.uuid+"'"})
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormUtil/update.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str,
        "id": result.id
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        if(data == '1'){
        	alert("更新成功");
        }else{
        	 messageSaveError();
        }
      }
    })
  }

// 保存补充参数
function saveSupplementParam () {
	 alert("111");
  var obj = new Object()
  for (var i = 0; i < supplementParams.length; i++) {
    obj[supplementParams[i]] = $("#" + supplementParams[i]).textbox("getValue")
  }
  obj.fanId = fanId
  if (hasSupplementParam) {
    obj.id = supplementParamObj.id
    var info_str = JSON.stringify(obj)
    $.ajax({
      url: "<%=basePath%>/FanSupplementParam/update",
      type: "POST",
      dataType:'text',
      data:
      {
        "data": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
        if(data.result){
        	alert("更新成功");
        }else{
        	 messageSaveError();
        }
      }
    })
  } else {
    var info_str = JSON.stringify(obj)
   
    $.ajax({
      url: "<%=basePath%>/FanSupplementParam/save",
      type: "POST",
      dataType:'text',
      data:
      {
        "data": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
        if(data.result){
        	getFanSupplementParam(fanId)
        	alert("更新成功");
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}

var editIndex = undefined;
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#dg').datagrid('validateRow', editIndex)){
		$('#dg').datagrid('endEdit', editIndex);
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
$('#dg').datagrid({
	url:'/develop/url/getUrl.do',
    queryParams:
    {
    	'name':'getFanCapabilityParam',
    	'fanNumber': fanNumber 
    },
    remoteSort: false, // 点击排序
    // sortName : 'soundLevel',
    sortOrder : 'desc',
    columns:[[
    	{field:'flux',title:'流量<br>Q[m&sup3/h]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalHead',title:'全压<br>P[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticPressure',title:'静压<br>P<sub>st</sub>[Pa]',width:'6%',align:'center',editor:'textbox',sortable: true},
		// {field:'speed',title:'转速<br>n[rpm]',width:'11%',align:'center',editor:'textbox'},
		{field:'internalPower',title:'轴功率<br>P<sub>z</sub>[kw]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'fullSpeed',title:'比转速<br>（全压）n<sub>s</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticSpeed',title:'比转速<br>（静压）n<sub>s2</sub>',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'tProductiveness',title:'全压效率<br>η<sub>t</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'sProductiveness',title:'静压效率<br>η<sub>st</sub>[%]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'noise',title:'噪声<br>L<sub>A</sub>[dB(A)]',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'soundLevel',title:'比A声级<br>Lsa[dB(A)]',width:'6%	',align:'center',editor:'textbox',sortable: true},
		{field:'fluxRatio',title:'流量系数<br>φ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'totalRatio',title:'全压系数<br>ψ',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'staticRatio',title:'静压系数<br>ψs',width:'6%',align:'center',editor:'textbox',sortable: true},
		{field:'powerRatio',title:'功率系数<br>λ',width:'6%',align:'center',editor:'textbox',sortable: true}
    ]],
    pagination:true,
	toolbar:'#tb',
	onClickRow: onClickRow
});

function add(){
	
}
function onClickRow(index){
	if (editIndex != index){
		if (endEditing()){
			$('#dg').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
			editIndex = index;
		} else {
			$('#dg').datagrid('selectRow', editIndex);
		}
	}
}
function append(){
	if (endEditing()){
		$('#dg').datagrid('appendRow',{});
		editIndex = $('#dg').datagrid('getRows').length-1;
		$('#dg').datagrid('selectRow', editIndex)
				.datagrid('beginEdit', editIndex);
	}
}
function save(){
	if (endEditing()){
		$('#dg').datagrid('acceptChanges');
	}
	//将数据保存到数据库
	var rows = $('#dg').datagrid('getRows');
	$.ajax({
        url: "/capabilityParam/save", 
        type: "POST",
        dataType: 'json',
        data: {
        	
            'fanId':fanId,
            'fanNumber':fanNumber,
            'paramRows':JSON.stringify(rows)
        },
        error: function() //失败
        {
        	shuoheUtil.layerMsgSaveOK('异常无法保存');
        },
        success: function(data)//成功
        {
            if (data.result == true) {     
                $('#dg').datagrid('reload');
                shuoheUtil.layerMsgSaveOK('保存成功');
            } else {
            	shuoheUtil.layerMsgSaveOK('保存失败');
            }  
            
           
        }
    });
}
function removeit(){
	if (editIndex == undefined){return}
	$('#dg').datagrid('cancelEdit', editIndex)
			.datagrid('deleteRow', editIndex);
	editIndex = undefined;
}

function update(){
	var currentRows = $('#dg').datagrid('getRows');
	if(currentRows.length==0){
		layerMsgCustom('风机参数不可为空');
	}else if(currentRows.length==1){
		layerMsgCustom('风机参数不可少于两条');
	}
}

function loadCurveChart(){
	var pressure;
	var productiveness;
	$.ajax({
        url: "/productMix/getEchartsData", 
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
        	
        },
        error: function() //失败
        {
        	shuoheUtil.layerMsgSaveOK('异常无法保存');
        },
        success: function(data)//成功
        {
        	console.log(data);
        	pressure = data.pressureData;
        	productiveness = data.productivenessData;
           
        }
    });
	var myChart = echarts.init(document.getElementById('main'))
	var option = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['压力曲线','效率曲线']
    },
    xAxis: {
        type: 'value',
        name:'流量(m3/h)'
    },
    yAxis: {
        type: 'value',
        name:'压力(pa)/效率(%)'
    },
    series: [
        {
            name:'压力曲线',
            type:'line',
            smooth: true,
            data:pressure
        },
        {
            name:'效率曲线',
            type:'line',
            smooth: true,
            data:productiveness
        }
    ]
}
	myChart.setOption(option)
}

//空气略图上传初始化
// $("#fileUploadContent").initUpload({
//    "uploadUrl":"/fanFile/uploadFanFile",//上传文件信息地址
//    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
//    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
//    "isHiddenUploadBt":false,//是否隐藏上传按钮
//    "isHiddenCleanBt":true,//是否隐藏清除按钮
//    "isAutoClean":false,//是否上传完成后自动清除
//    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
//    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
//    "beforeUpload":beforeUploadFun,//在上传前执行的函数
//    "onUpload":onUploadFun,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

//});
// 安装简图上传初始化
//$("#fileUploadContent1").initUpload({
//    "uploadUrl":"/fanFile/uploadFanFile",//上传文件信息地址
//    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
//    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
//    "isHiddenUploadBt":false,//是否隐藏上传按钮
//    "isHiddenCleanBt":true,//是否隐藏清除按钮
//    "isAutoClean":false,//是否上传完成后自动清除
//    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
//    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
//   "beforeUpload":beforeUploadFun1,//在上传前执行的函数
//    "onUpload":onUploadFun1,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

//});
// 产品介绍上传初始化
//$("#fileUploadContent2").initUpload({
//    "uploadUrl":"/fanFile/uploadFanFile",//上传文件信息地址
//    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
//    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
//    "isHiddenUploadBt":false,//是否隐藏上传按钮
//    "isHiddenCleanBt":true,//是否隐藏清除按钮
//    "isAutoClean":false,//是否上传完成后自动清除
//    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
//    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
//    "beforeUpload":beforeUploadFun2,//在上传前执行的函数
//    "onUpload":onUploadFun2,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

//});
// 其他信息上传初始化
//$("#fileUploadContent3").initUpload({
//    "uploadUrl":"/fanFile/uploadFanFile",//上传文件信息地址
//    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
//    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
//    "isHiddenUploadBt":false,//是否隐藏上传按钮
//   "isHiddenCleanBt":true,//是否隐藏清除按钮
//    "isAutoClean":false,//是否上传完成后自动清除
//    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
//    "maxFileNumber":5,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
//    "beforeUpload":beforeUploadFun3,//在上传前执行的函数
//    "onUpload":onUploadFun3,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

//});
$("#appearancePicture").initUpload({
    "uploadUrl":"/fanFile/uploadFanFile",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":false,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "velocity":10,//模拟进度上传数据
    //"rememberUpload":true,//记住文件上传
   // "showFileItemProgress":false,
    //"showSummerProgress":false,//总进度条，默认限制
    //"scheduleStandard":true,//模拟进度的方式，设置为true是按总进度，用于控制上传时间，如果设置为false,按照文件数据的总量,默认为false
    //"size":350,//文件大小限制，单位kb,默认不限制
    "maxFileNumber":1,//文件个数限制，为整数
    //"filelSavePath":"",//文件上传地址，后台设置的根目录
    "beforeUpload":beforeUploadFun4,//在上传前执行的函数
    "onUpload":onUploadFun3,//在上传后执行的函数
     //autoCommit:true,//文件是否自动上传
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀

});
function beforeUploadFun(opt){
	var paramArr = [];
	var obj1 = {};
	obj1.name = "fanNumber";
	obj1.value=fanId;
	var obj2 = {};
	obj2.name = "fileType";
	obj2.value=$('#airOutline').text();
	paramArr.push(obj1);
	paramArr.push(obj2);
    opt.otherData =paramArr;
    return true;
}
function beforeUploadFun1(opt){
	var paramArr = [];
	var obj1 = {};
	obj1.name = "fanNumber";
	obj1.value=fanId;
	var obj2 = {};
	obj2.name = "fileType";
	obj2.value=$('#installationDiagram').text();//IntroductionChart
	paramArr.push(obj1);
	paramArr.push(obj2);
    opt.otherData =paramArr;
    return true;
}
function beforeUploadFun2(opt){
	var paramArr = [];
	var obj1 = {};
	obj1.name = "fanNumber";
	obj1.value=fanId;
	var obj2 = {};
	obj2.name = "fileType";
	obj2.value=$('#introductionChart').text();//otherMsg
	paramArr.push(obj1);
	paramArr.push(obj2);
    opt.otherData =paramArr;
    return true;
}
function beforeUploadFun3(opt){
	var paramArr = [];
	var obj1 = {};
	obj1.name = "fanNumber";
	obj1.value=fanId;
	var obj2 = {};
	obj2.name = "fileType";
	obj2.value=$('#otherMsg').text();//otherMsg
	paramArr.push(obj1);
	paramArr.push(obj2);
    opt.otherData =paramArr;
    return true;
}
function beforeUploadFun4(opt){
	var paramArr = [];
	var obj1 = {};
	obj1.name = "fanNumber";
	obj1.value=fanId;
	var obj2 = {};
	obj2.name = "fileType";
	obj2.value="外形图";//otherMsg
	paramArr.push(obj1);
	paramArr.push(obj2);
    opt.otherData =paramArr;
    return true;
}
// 空气略图上传成功
function onUploadFun(opt,data){
    //写入硬盘成功后保存到数据库
    console.log(JSON.stringify(data));
    shuoheUtil.layerMsgSaveOK('上传成功');
}
// 安装简图上传成功
function onUploadFun1(opt,data){
    console.log(JSON.stringify(data));
    shuoheUtil.layerMsgSaveOK('上传成功');
}
// 产品介绍上传成功
function onUploadFun2(opt,data){
    console.log(JSON.stringify(data));
    shuoheUtil.layerMsgSaveOK('上传成功');
}
// 其他信息上传成功
function onUploadFun3(opt,data){
    console.log(data);
    console.log(opt);
    shuoheUtil.layerMsgSaveOK('上传成功');
}
function testUpload(){
    var opt = uploadTools.getOpt("fileUploadContent");
    uploadEvent.uploadFileEvent(opt);
}
function tt() {
    var opt = uploadTools.getOpt("fileUploadContent");
    uploadTools.uploadError(opt);//显示上传错误
}

//显示文件，设置删除事件
// uploadTools.showFileResult("fileUploadContent","img/a2.png","1",true,deleteFileByMySelf);
//如果不需要删除
//uploadTools.showFileResult("fileUploadContent","img/a3.png","1",false);
//多文件需要自己进行循环
function deleteFileByMySelf(fileId){
    alert("要删除文件了："+fileId);
}
</script>