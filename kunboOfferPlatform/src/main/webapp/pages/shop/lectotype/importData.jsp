<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>原材料管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>
    <!-- 文件上传 -->
    <link href="/pages/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>
    <link href="/pages/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">  
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!--引入文件上传JS-->
	  <script type="text/javascript" src="/pages/fileUpload/js/fileUpload1.js"></script>
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="padding: 30px;">
      <form id="fileForm" method="POST" action="" enctype="multipart/form-data">
        <div class="form-order-line" style="margin-top: 30px;margin-bottom: 40px;">
          <label style="padding-left: 90px;">风机类别</label>
          <input id="tree" name="fanType">
        </div>
        <div class="form-order-line" style="padding: 0 90px;">
          <!-- <label style="padding-left: 90px;">选择文件</label>
          <input type="file" name="file"> -->
          <div id="appearancePicture" class="fileUploadContent"></div>
        </div>
        <!-- <div style="margin-top: 40px;text-align: center;">
          <a href="#" class="button button-primary button-rounded button-small" onclick="importData()">保存</a>
        </div> -->
      </form>
    </div>
</body> 
</html>
<script type="text/javascript">
var flag = false
$(function(){
	$('#tree').combotree({
    width: '300px',
    url:"/shop/sort/getSortForTree.do",
    onSelect: function(param){
      if (param.sort_type == 'PRODUCT_LINE') {
        flag = true
      } else {
        flag = false
      }
    }
  })
  $("#appearancePicture").initUpload({
    "uploadUrl":"/fanFile/importFanParams",//上传文件信息地址
    "progressUrl":"#",//获取进度信息地址，可选，注意需要返回的data格式如下（{bytesRead: 102516060, contentLength: 102516060, items: 1, percent: 100, startTime: 1489223136317, useTime: 2767}）
    "selfUploadBtId":"selfUploadBt",//自定义文件上传按钮id
    "isHiddenUploadBt":false,//是否隐藏上传按钮
    "isHiddenCleanBt":false,//是否隐藏清除按钮
    "isAutoClean":false,//是否上传完成后自动清除
    "maxFileNumber":1,//文件个数限制，为整数
    "beforeUpload":beforeUploadFun4,//在上传前执行的函数
    "onUpload":onUploadFun3,//在上传后执行的函数
    //"fileType":['png','jpg','docx','doc']，//文件类型限制，默认不限制，注意写的是文件后缀
  });
});
function beforeUploadFun4(opt){
  if (!flag) {
    layerMsgCustom('请选择风机类别');
    return false
  } else {
    var paramArr = [];
    var obj1 = {};
    obj1.name = "typeId";
    obj1.value=$('#tree').combotree('getValue');
    paramArr.push(obj1);
    opt.otherData =paramArr;
    return true;
  }
}
function onUploadFun3(opt,data){
    // console.log(data);
    // console.log(opt);
    shuoheUtil.layerMsgSaveOK('上传成功');
    parent.updateSuccess()
}
</script>

