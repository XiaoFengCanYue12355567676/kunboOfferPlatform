<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
</head>
<style>

</style>
<body>
<div>
	<div id="main" style="height:400px;margin-top: 50px;"></div>
</div>
</body>
<!-- jQuery相关引用 -->
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/pages/js/echarts.min.js"></script>
<script>

$(function(){
	var myChart = echarts.init(document.getElementById('main'))
	var option = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['压力曲线','效率曲线']
    },
    xAxis: {
        type: 'value',
        name:'流量(m3/h)'
    },
    yAxis: {
        type: 'value',
        name:'压力(pa)/效率(%)'
    },
    series: [
        {
            name:'压力曲线',
            type:'line',
            smooth: true,
            data:[[40, 120], [120, 132], [127, 101], [250, 134], [340, 90], [410, 230], [530, 210]]
        },
        {
            name:'效率曲线',
            type:'line',
            smooth: true,
            data:[[10, 120], [150, 132], [175, 101], [350, 134], [440, 90], [510, 700], [650, 210]]
        }
    ]
}
	myChart.setOption(option)
})
</script>
</html>