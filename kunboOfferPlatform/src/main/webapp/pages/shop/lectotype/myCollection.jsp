<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.shuohe.entity.system.user.User"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	User user = null;
	String userJson = null;
	try
	{
	    user = (User)session.getAttribute("user");
	    if(user==null)
	    {
	      System.out.println("user = null");
	      String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    }
	    else
	    {
	        userJson = Json.toJsonByPretty(user);
	    }
	}
	catch(Exception e)
	{
	    String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    e.printStackTrace();
	}
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>原材料管理</title> 


<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<!-- FontAwesome字体图标 -->
<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="/pages/css/add-form.css" rel="stylesheet"/>
<!-- jQuery相关引用 -->
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript" src="/pages/js/moment.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>
<script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

<script type="text/javascript" src="/pages/js/base-loading.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>


<style type="text/css">
    html, body{ margin:0; height:100%; }
    .datagrid-cell{
      cursor: pointer;
    }
</style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
  <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:false,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  pageSize:20,
                  onClickRow:onDblClickRow">
                  <thead>
                    <tr>
                      <th data-options="field:'productNumber'">风机编号</th>
                      <th data-options="field:'text'">风机名称</th>
                    </tr>  
                  </thead>
          </table>    
       </div>
    </div>
</body> 
</html>
<script type="text/javascript">
var user = <%=userJson%>;
var fanSelectParam = []
var fanSelectParamObj = {}
var fanNumber = ''
// 表格数据
var tableData = []
// 表格表头
var columns = [[
  {
    field:'productNumber',
    title:'风机编号'
  },
  {
    field:'text',
    title:'风机名称'
  },
  {
    field:'yelunzhijing',
    title:'叶轮直径'
  },
  {
    field:'j_yps',
    title:'叶片数'
  },
  {
    field:'j_jd',
    title:'角度'
  },
  {
    field:'id',
    title:'操作',
    formatter: function (value,row,index) {
      return '<a onclick="delData(\'' + value + '\')">删除</a>'
    }
  }
]]
$('#dg').datagrid({
  columns: columns
})
function getData () {
  $.ajax({
    url: "/fanOfferCollect/findByUserId",
    type: "POST",
    dataType: 'json',
    async: false,
    data: {
      userId: user.id
    },
    error: function() //失败
    {
      shuoheUtil.layerTopMaskOff();
    },
    success: function(data) //成功
    {
      tableData = data
    }
  })
  $('#dg').datagrid({
    data: tableData
  })
}
getData()
// 删除
function delData (id) {
  $.ajax({
    url: "/fanOfferCollect/delete",
    type: "POST",
    dataType: 'json',
    async: false,
    data: {
      id: id
    },
    error: function() //失败
    {
      shuoheUtil.layerTopMaskOff();
    },
    success: function(data) //成功
    {
      if(data.result){
    	  getData()
    	  shuoheUtil.layerMsgSaveOK('删除成功');
      }else{
        messageSaveError();
      }
    }
  })
  event.stopPropagation()
}
function onDblClickRow(rowIndex, rowData){
  fanSelectParam = [
    {
      key: '风量',
		  value: rowData.blowingRate + ''
    },
    {
      key: rowData.windPressure,
		  value: rowData.totalHeadQuery + ''
    },
    {
      key: '误差范围',
		  value: rowData.errorMinRange + '~' + rowData.errorMaxRange
    },
    {
      key: '风机密度',
		  value: rowData.airDensityQuery + ''
    }
  ]
  fanNumber = rowData.productNumber + ''
  fanSelectParamObj = {
    state: rowData.state + '',
    selectWindPresssure: rowData.windPressure + '',
    blowingRate: rowData.blowingRate + '',
    totalHeadQuery: rowData.totalHeadQuery + '',
    errorRange: rowData.errorMinRange + '~' + rowData.errorMaxRange,
    airDensityQuery: rowData.airDensityQuery + '',
    speedQuery: rowData.speedQuery + '',
    minYlzj: rowData.minYlzj === null ? '' : rowData.minYlzj,
    maxYlzj: rowData.maxYlzj === null ? '' : rowData.maxYlzj
  }
  layer.open({
      type: 2,
      title: '风机参数',
      shadeClose: false,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: shuoheUtil.cpm_size(),
      btn: '',
      content: 'showParam.jsp?fanId='+rowData.fanId+'&queryX='+rowData.blowingRate+'&queryY='+rowData.totalHeadQuery+'&isTransshape=' + rowData.isTransshape,
      yes:function(index){
        var ifname="layui-layer-iframe"+index//获得layer层的名字
          var Ifame=window.frames[ifname]//得到框架
        Ifame.update();
      }
  })
}
</script>