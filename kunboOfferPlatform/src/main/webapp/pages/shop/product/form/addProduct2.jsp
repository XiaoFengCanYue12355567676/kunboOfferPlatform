<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>

<title>创建产品</title>

<style type="text/css">
*, *:before, *:after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

html, body {
    height: 100%;
}

body {
    font: 12px/1 'Roboto', sans-serif;
    color: #555;
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

ul {list-style: none;}

.cf:before, .cf:after {
    content: ' ';
    display: table;
}
.cf:after {
    clear: both;
}

.title {
    padding: 0 0;
    font: 24px 'Open Sans', sans-serif;
    text-align: center;
}



.inner {
    max-width: 820px;
    margin: 0 auto;
}
.breadcrumbs {
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    background-color: #f5f5f5;
}
.breadcrumbs ul {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
}
.breadcrumbs li {
    float: left;
    width: 20%;
}
.breadcrumbs a {
    position: relative;
    display: block;
    padding: 20px;
    padding-right: 0 !important;
    /* important overrides media queries */
    font-size: 13px;
    font-weight: bold;
    text-align: center;
    color: #aaa;
    cursor: pointer;
}

.breadcrumbs a:hover {
    background: #eee;
}
.breadcrumbs a.active {
    color: #777;
    background-color: #fafafa;
}
.breadcrumbs a span:first-child {
    display: inline-block;
    width: 22px;
    height: 22px;
    padding: 2px;
    margin-right: 5px;
    border: 2px solid #aaa;
    border-radius: 50%;
    background-color: #fff;
}

.breadcrumbs a.active span:first-child {
    color: #fff;
    border-color: #777;
    background-color: #777;
}

.breadcrumbs a:before,
.breadcrumbs a:after {
    content: '';
    position: absolute;
    top: 0;
    left: 100%;
    z-index: 1;
    display: block;
    width: 0;
    height: 0;
    border-top: 32px solid transparent;
    border-bottom: 32px solid transparent;
    border-left: 16px solid transparent;
}

.breadcrumbs a:before {
    margin-left: 1px;
    border-left-color: #d5d5d5;
}
.breadcrumbs a:after {
    border-left-color: #f5f5f5;
}
.breadcrumbs a:hover:after {
    border-left-color: #eee;
}
.breadcrumbs a.active:after {
    border-left-color: #fafafa;
}
.breadcrumbs li:last-child a:before,
.breadcrumbs li:last-child a:after {
    display: none;
}

@media (max-width: 720px) {
    .breadcrumbs a {
        padding: 15px;
    }

    .breadcrumbs a:before,
    .breadcrumbs a:after {
        border-top-width: 26px;
        border-bottom-width: 26px;
        border-left-width: 13px;
    }
}
@media (max-width: 620px) {
    .breadcrumbs a {
        padding: 10px;
        font-size: 12px;
    }

    .breadcrumbs a:before,
    .breadcrumbs a:after {
        border-top-width: 22px;
        border-bottom-width: 22px;
        border-left-width: 11px;
    }
}
@media (max-width: 520px) {
    .breadcrumbs a {
        padding: 5px;
    }
    .breadcrumbs a:before,
    .breadcrumbs a:after {
        border-top-width: 16px;
        border-bottom-width: 16px;
        border-left-width: 8px;
    }
    .breadcrumbs li a span:first-child {
        display: block;
        margin: 0 auto;
    }
    .breadcrumbs li a span:last-child {
        display: none;
    }
}
</style>

<script src="/pages/js/prefixfree.min.js"></script>

</head>

<body class="easyui-layout"  style="width: 100%;height: 100%;">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:100%;">  
        <div class='breadcrumbs'>
            <div class='inner'>
                <ul class='cf'>
                    <li><a id='ss1' class='active'><span>1</span><span>产品信息</span></a></li>
                    <li><a id='ss2'><span>2</span><span>原材料及加工费</span></a></li>
                    <li><a id='ss3'><span>2</span><span>替换部件</span></a></li>
                    <li><a id='ss4'><span>3</span><span>参数</span></a></li>
                    <li><a id='ss5'><span>4</span><span>价格计算</span></a></li>
                </ul>
            </div>
        </div>       
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:100%;height: 490px">
         <iframe id='son_page' src="../compent/product.jsp" style="width: 100%;height: 100%"></iframe>
    </div>        
  
   
</body>
</html>
<script type="text/javascript">
    

var Product = new Object();
var ProductSort = new Object();
var ProductMaterials = new Object();
var ProductSortParam = new Object();

$(function()
{
    var result = Easyui.Ajax.get('/product/sort/getByUuid.do?uuid='+shuoheUtil.getUrlHeader().uuid);
    if(result.result == false)
    {
        shuoheUtil.layerMsgCustom('产品大类数据价值失败');
        return;
    }
    //加载产品部件内容类型
    var sort_list = Easyui.Ajax.get('/product/sort/sort_list.do?uuid='+shuoheUtil.getUrlHeader().uuid);
    if(sort_list.result == false)
    {
        shuoheUtil.layerMsgCustom('产品部件类型加载失败');
        return;
    }


    {
        ProductSort = result.obj;
        ProductSortParam = sort_list.obj;
        console.info('ProductSort = '+JSON.stringify(ProductSort));
        console.info('ProductSortParam = '+JSON.stringify(ProductSortParam));
    }
})





function s1() {
    console.info('s1');
    disActiveAll();
    $('#ss1').addClass("active");$('#son_page').attr('src',"../compent/product.jsp");
}
function s2() {
    console.info('s2');
    disActiveAll();
    $('#ss2').addClass("active");    
    $('#son_page').attr('src',"../compent/machinePort.jsp");
}
function s3() {
    console.info('s3');
    disActiveAll();
    $('#ss3').addClass("active");
    $('#son_page').attr('src',"../compent/material.jsp");
}
function s4() {
    console.info('s4');
    disActiveAll();
    $('#ss4').addClass("active");
    $('#son_page').attr('src',"../compent/params.jsp");
}
function s5() {
    console.info('s4');
    disActiveAll();
    $('#ss5').addClass("active");
    $('#son_page').attr('src',"../compent/calculation.jsp");
}
// function s4() {
//     console.info('s4');
//     disActiveAll();
//     $('#ss4').addClass("active");
// }
function disActiveAll()
{
    $('#ss1').removeClass("active");
    $('#ss2').removeClass("active");
    $('#ss3').removeClass("active");
    $('#ss4').removeClass("active");
    $('#ss5').removeClass("active");
    // $('#ss4').removeClass("active");
}


</script>