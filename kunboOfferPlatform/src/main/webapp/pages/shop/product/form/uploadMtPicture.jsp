<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>上传维修图片</title> 

<link rel="stylesheet" type="text/css" href="/pages/css/base.css">
<link rel="stylesheet" type="text/css" href="/custom/uimaker/easyui.css">
<link rel="stylesheet" type="text/css" href="/custom/uimaker/icon.css">
<link rel="stylesheet" type="text/css" href="/pages/css/process.css">
<link rel="stylesheet" type="text/css" href="/pages/js/umeditor/themes/default/css/umeditor.css">
<!-- <link rel="stylesheet" type="text/css" href="/pages/component/stream/css/stream-v1.css"> -->


<script type="text/javascript" src="/custom/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/custom/jquery.min.js"></script>
<script type="text/javascript" src="/custom/ajaxfileupload.js"></script>
<script type="text/javascript" src="/custom/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/custom/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" src="/pages/js/umeditor/umeditor.min.js"></script>
<script type="text/javascript" src="/pages/js/umeditor/umeditor.config.js"></script>
<script type="text/javascript" src="/pages/js/base-loading.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>


<style type="text/css">
	html, body{ margin:0; height:100%; }
</style>
<script type="text/javascript">
	// 用户信息
	var user ;
    var order_id ;
    // var order_id = '6';
	console.info("user="+ user.name);
	console.info("user=" + user.id);
	var static_permissionssJson;
	console.info(static_permissionssJson);				
</script>
</head> 
<body style="text-align: center;">
    <!-- <div id="dlgCreate" class="easyui-panel" title="修改客户资料" > -->
<!--         <div class="conditions" style="margin-top: 10px;">
            <div style="margin-left: 250px;font-size: 25px">上传维修图片</div>
            <hr style="height:1px;border:none;border-top:1px dashed #0066CC;" />
        </div>  -->
        <div class="conditions" style="margin-top: 10px;">
            <div id='app_order_id'></div>
        </div> 

<!--     	<div class="conditions" style="margin-top: 10px;">
        	<span >&#12288;上传人: </span>
       		<input class="easyui-textbox" style="width:470px;height:35px;line-height:35px;"  id = "applicantMan">     
    	</div>  -->
<!--     	<div class="conditions" style="margin-top: 10px;">
    		<span >选择文件: </span>
    	</div> -->
    	<div class="conditions" style="margin-top: 0px;">
        	<iframe id="fileUpload" style="width: 500px;height: 350px;margin-left:60px"></iframe>
    	</div>
        
</body> 
</html>

<script type="text/javascript">
    var fileList = new Array();       
    $(function(){

        var url ="/pages/component/stream/fileUpload.jsp?order_id="+order_id
                +'&'+'type='+'MtPicture';
        document.getElementById("fileUpload").src = url;       

        // $('#applicantMan').textbox('setValue',user.actual_name);
        // $('#applicantMan').textbox('disable');
    });
</script>

<script type="text/javascript">
    function setFileList(list) 
    {        
        var fileListxxx = new Array();    
        for(var i=0;i<list.length;i++)
        {
            file = new Object();
            file.order_id = order_id;
            file.file_name = list[i].name;
            file.user_id = user.id;
            fileListxxx.push(file);
        }

        console.info('list.length = '+list.length);
        $.ajax({
            url: "/pages/interface/maintenance/mtPicture/submitFileLlist.jsp",
            type: "POST", 
            dataType:'json',
            data:  
            { 
                order_id:order_id,//文件申请单号
                applicantMan:user.id,//申请人
                fileList:JSON.stringify(fileListxxx),//文件列表
            },
            error: function() //失败
            { 
                maskOff();
                messageCustom('保存文件信息失败');
            },
            success: function(data)//成功
            { 
                maskOff();
                if(data.result == true)
                {
                    messageCustom('保存文件信息成功');                      
                }
                else
                {
                    messageCustom('保存文件信息失败');      
                }
            } 
        });
    }
</script>

<script type="text/javascript">
    var checkInput = function(approverManJson)
    {
        if(fileList.length == 0)
        {
            messageCustom("不能没有审批文件");
            return false;   
        }

        var approverMan = jQuery.parseJSON(approverManJson);
        console.info('approverManJson = '+approverManJson);
        console.info('approverMan.length = '+approverMan.length);
        if(approverMan.length == 0)
        {
            messageCustom("不能没有审批人");
            return false;   
        }
        return true;
    }
</script>