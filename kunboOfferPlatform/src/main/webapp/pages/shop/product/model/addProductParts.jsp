<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增模型配件</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true,height:800" >     
        <table class="editTable" style="text-align: center;margin-top: 30px;margin-left:30px;" >
            <!-- <tr>
                <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                    <div class="divider">
                        <span style="margin-left: 50px">产品大类</span>
                    </div>
                </td>
            </tr>-->               
            <tr>
                <td class="label">物料类型1</td>
                <td >
                    <select class="easyui-combobox ycl" id="selected" name="language" style="width: 150px;">
                    	<option value="cp">产品</option>
                    	<option value="bcp">半成品</option>
                    	<option value="yl">原材料</option>
                    	<option value="ptj">配套件</option>
                    	<option value="xzj">协作件</option>
                    	<option value="yhj">易耗件</option>
                    	<option value="gs">工时</option>
                    </select>
                </td>
            </tr>
            <tr> 
                <td class="label">产品模型名称</td>
                <td >
                    <input id='text1' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>  
            <tr> 
                <td class="label">物料编号</td>
                <td >
                    <input id='text2' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr> 
            <tr> 
                <td class="label">物料名称</td>
                <td >
                    <input id='text3' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr> 
            <tr> 
                <td class="label">规格</td>
                <td >
                    <input id='text4' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>
            <td class="label">单位</td>
                <td >
                    <select class="easyui-combobox"  name="language" id="danwei" style="width: 150px;">
                    	<option value="ge">个</option>
                    	<option value="zhi">只</option>
                    	<option value="kun">捆</option>
                    	<option value="kuai">块</option>
                    	<option value="ping">瓶</option>
                    	<option value="guan">罐</option>
                    	<option value="sheng">升</option>
                    	<option value="shi">小时</option>
                    	<option value="tao">套</option>
                    	<option value="tai">台</option>
                    	<option value="tiao">条</option>
                    </select>
                </td>
            <tr class="gs" hidden="hidden" id="text5"> 
                <td class="label">公式输入</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>
        </table>
	</div>
    <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>
<script type="text/javascript">
	$(function(){
		$('#shangji_text').combo({    
	    required:true,    
	    multiple:true   
	})
		
	$('#selected').combobox({
		onSelect: function (row) {
			if(row.value=='yl'){
				$('.gs').show();
			}
		}
	})
			
		
	
});  


	    var ProductModel= new Object();
	    ProductModel.lxName = '';
	    ProductModel.mxName = '';
	    ProductModel.wlNum = '';
	    ProductModel.wlName = '';
	    ProductModel.ggName = '';
	    ProductModel.dwName = '';
	    ProductModel.gsName = '';
	    ProductModel.pc_pid = '';
	    function save() {
        ProductModel.lxName= $('#selected').combobox('getValue');
        ProductModel.mxName= $('#text1').textbox('getValue');
        ProductModel.wlNum= $('#text2').textbox('getValue');
        
        ProductModel.wlName= $('#text3').textbox('getValue');
        ProductModel.lxName= $('#danwei').combobox('getValue');
        ProductModel.ggName= $('#text4').textbox('getValue');
        ProductModel.gsName= $('#text5').textbox('getValue');
        
        ProductModel.pc_pid= GetQueryString('uuid');
       console.log($('#text').textbox('getValue')+GetQueryString('uuid'))
        $.ajax({
            url: "/extensionModel/save", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(ProductModel)
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                	
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                    parent.$('#mg').datagrid('reload');
                    shuoheUtil.layerMsgSaveOK('保存成功');
                    console.log("222")
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>

<script type="text/javascript">

        var editIndex = undefined;
        function endEditing(){
            if (editIndex == undefined){return true}
            if ($('#dg').datagrid('validateRow', editIndex)){
                // var ed = $('#dg').datagrid('getEditor', {index:editIndex,field:'productid'});
                // var productname = $(ed.target).combobox('getText');
                // $('#dg').datagrid('getRows')[editIndex]['productname'] = productname;
                $('#dg').datagrid('endEdit', editIndex);
                editIndex = undefined;
                return true;
            } else {
                return false;
            }
        }
        function onClickRow(index){
            console.info('editIndex = '+editIndex +' index = '+index);
            if (editIndex != index){
                if (endEditing()){
                    $('#dg').datagrid('selectRow', index)
                            .datagrid('beginEdit', index);
                    editIndex = index;
                } else {
                    $('#dg').datagrid('selectRow', editIndex);
                }
            }
        }
        function append(){
            if (endEditing()){
                $('#dg').datagrid('appendRow',{text:''});
                editIndex = $('#dg').datagrid('getRows').length-1;
                $('#dg').datagrid('selectRow', editIndex)
                        .datagrid('beginEdit', editIndex);
            }
        }
        function removeit(){
            if (editIndex == undefined){return}
            $('#dg').datagrid('cancelEdit', editIndex)
                    .datagrid('deleteRow', editIndex);
            editIndex = undefined;
        }
        function accept(){
            if (endEditing()){
                $('#dg').datagrid('acceptChanges');
            }
        }
        function reject(){
            $('#dg').datagrid('rejectChanges');
            editIndex = undefined;
        }
        function getChanges(){
            var rows = $('#dg').datagrid('getChanges');
            alert(rows.length+' rows are changed!');
        }
    </script>