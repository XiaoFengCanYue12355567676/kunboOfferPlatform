<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%> 
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>编辑产品模型</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" ">     
        <table class="editTable" style="text-align: center;margin-top: 30px;margin-left:30px;" >
            <tr>
                <td class="label">上级产品模型</td>
                <td >
                    <input id='shangji_text' class="easyui-textbox" data-options="required:false,width:150,editable:true" disabled>
                </td>
            </tr>
            <tr>
                <td class="label">产品模型名称</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>        

        </table>
	</div>
    <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>

<script type="text/javascript">
	var pc_pid ='';
	var createTime ='';
	$(function(){
        $.ajax({
            url: '/develop/url/getUrl.do?name='+'getModelNameByUuid'+'&uuid='+GetQueryString('uuid'),
            type: "GET",
            dataType: 'json',
            success: function(data) //成功
            {console.log(data);
            	if(null!=data){
                    $('#text').textbox('setValue',data[0].name);
                    pc_pid=data[0].pc_pid;
                    createTime=data[0].createTime;
            	}
            	
            }
        });
         
    });
    
    function save() {
        var ProductModel  = new Object();
        ProductModel.id = GetQueryString('uuid'); 
        ProductModel.pc_pid = pc_pid; 
    	ProductModel.name= $('#text').textbox('getValue');
    	ProductModel.createTime= createTime;
        $.ajax({
        	url: "/extensionModel/update",
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(ProductModel)
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭
                    parent.$('#tg').datagrid('reload');
                    shuoheUtil.layerMsgSaveOK('修改成功');
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>

<script type="text/javascript">

        var editIndex = undefined;
        function endEditing(){
            if (editIndex == undefined){return true}
            if ($('#dg').datagrid('validateRow', editIndex)){
                // var ed = $('#dg').datagrid('getEditor', {index:editIndex,field:'productid'});
                // var productname = $(ed.target).combobox('getText');
                // $('#dg').datagrid('getRows')[editIndex]['productname'] = productname;
                $('#dg').datagrid('endEdit', editIndex);
                editIndex = undefined;
                return true;
            } else {
                return false;
            }
        }
        function onClickRow(index){
            console.info('editIndex = '+editIndex +' index = '+index);
            if (editIndex != index){
                if (endEditing()){
                    $('#dg').datagrid('selectRow', index)
                            .datagrid('beginEdit', index);
                    editIndex = index;
                } else {
                    $('#dg').datagrid('selectRow', editIndex);
                }
            }
        }
         
         

 
    </script>