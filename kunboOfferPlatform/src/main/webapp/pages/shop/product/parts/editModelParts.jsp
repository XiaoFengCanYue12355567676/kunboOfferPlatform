<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改模型配件</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true"  ">  
	    <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:30px;" >
	        <tr>
	            <td class="label">风机大类编码</td>
	            <td >
	                <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:200,editable:true" disabled="true"  >
	            </td>
	            <td class="label">配件名称</td>
	            <td >
	                <input id='partsName' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >       
	            </td>
	        </tr>
	        <tr>
	            <td class="label">配件单位</td>
	            <td >
	                <input id='partsUnit' class="easyui-textbox" data-options="required:false,width:200,editable:true">
	            </td>
	            <td class="label">配件/材料/工时</td>
	            <td > 
					<select id='partsType' class="easyui-combobox" data-options="required:false,width:200,editable:false">
							<option value="材料">材料</option>
							<option value="工时">工时</option>
							<option value="标准">标准</option>
					</select>          
	            </td>
	        </tr>        
	        <tr>
	            <td class="label" >是否必选</td>
	            <td >
					<select id='ifMustChoose' class="easyui-combobox" data-options="required:false,width:200,editable:false">
							<option value="true">是</option>
                            <option value="false">否</option>
					</select>
	            </td>
            </tr> 
	    </table>
	</div>
	<div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>

<script type="text/javascript">
	
 	var createTime ='';
	$(function(){
	    $.ajax({
	        url: '/develop/url/getUrl.do?name='+'getModelPartsByUuid'+'&uuid='+GetQueryString('uuid'),
	        type: "GET",
	        dataType: 'json',
	        success: function(data) //成功
	        {
	        	 
	        	if(null!=data){
					$('#pc_pid').textbox('setValue',data[0].pc_pid);
					$('#partsName').textbox('setValue',data[0].partsName);
					$('#partsUnit').textbox('setValue',data[0].partsUnit);
					$('#partsType').textbox('setValue',data[0].partsType);
					$('#ifMustChoose').combobox('setValue',data[0].ifMustChoose);
					createTime = data[0].createTime ;
	        	}
        	
      	    }
    	});
    });
    
	function save(){
		
	    var obj = new Object();
	    console.info('save');
	    obj.id = GetQueryString('uuid');
	    obj.pc_pid = $('#pc_pid').textbox('getValue');  
	    obj.partsName = $('#partsName').textbox('getValue');  
	    obj.partsUnit = $('#partsUnit').textbox('getValue');  
	    obj.partsType = $('#partsType').textbox('getValue');  
	    obj.ifMustChoose = $('#ifMustChoose').combobox('getValue'); 
	    obj.createTime = createTime;
	    $.ajax({
	        url: "/fanModelParts/update", 
	        type: "POST",
	        dataType: 'json',
	        data: {
	            'data': JSON.stringify(obj)
	        },
	        error: function() //失败
	        {
	//          shuoheUtil.layerTopMaskOff();
	//          shuoheUtil.layerTopMsgError('远程通信失败');
	        },
	        success: function(data) //成功
	        {
	            shuoheUtil.layerTopMaskOff();
	            if (data.result == true) {
	                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
	                parent.layer.close(index); //再执行关闭
	                parent.$('#dg').datagrid('reload');
	                shuoheUtil.layerMsgSaveOK('保存成功');
	            } else {
	                shuoheUtil.layerTopMsgError(data.describe);
	            }
	        }
	    });
	}
</script>