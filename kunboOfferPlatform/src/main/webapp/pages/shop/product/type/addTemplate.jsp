<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String id = request.getParameter("id");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增产品大类</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true"  ">     
        <table class="editTable" style="text-align: center;margin-top: 30px;margin-left:30px;" >
            <!-- <tr>
                <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                    <div class="divider">
                        <span style="margin-left: 50px">产品大类</span>
                    </div>
                </td>
            </tr>-->               
            <tr>
                <td class="label">上级产品大类</td>
                <td >
                    <input id='shangji_text' class="easyui-textbox" value="" data-options="required:false,width:150,editable:true" disabled>
                </td>
            </tr>
            <tr> 
                <td class="label">产品大类名称</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
                 <td class="label">风机型号</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
                <td class="label">物料编码</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>        
        </table>
	</div>
    <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>
<script type="text/javascript">
	var id = '<%=id%>';
	$(function(){
		$.ajax({
            url: "/productCategory/findById",
            type: "POST",
            dataType:'json',
            data:  
            { 
              id:id
            },
            error: function() //失败
            { 
                messageSaveError();
            },
            success: function(data)//成功
            { 
            	console.log(data)
                var result = jQuery.parseJSON(JSON.stringify(data));
                department_pid = data;
                if(data != null)
                {
                    $('#shangji_text').textbox('setValue',data.text);
                }
                else messageSaveError();                
            }
            
        });
	})
	 	
    var ProductSort = new Object();
    ProductSort.name = '';
    function save() {       
        ProductSort.text= $('#text').textbox('getValue');
        ProductSort.pid = id;
        console.log(ProductSort)
        $.ajax({
            url: "/productCategory/save", 
            type: "POST",
            dataType: 'json',
            data: {
            	
                'data': JSON.stringify(ProductSort)
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                	
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                   
                    parent.$('#tt').tree('reload');
                    shuoheUtil.layerMsgSaveOK('保存成功');
                    parent.layer.close(index); //再执行关闭
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }  
                
               
            }
        });
    }
</script> 