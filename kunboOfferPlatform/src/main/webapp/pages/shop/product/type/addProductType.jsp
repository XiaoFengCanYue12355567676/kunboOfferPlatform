<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String id = request.getParameter("id");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增产品大类</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%};
    
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true"  ">     
        <table class="editTable" style="text-align: center;margin-top: 30px;margin-left:15px;" >
            <!-- <tr>
                <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                    <div class="divider">
                        <span style="margin-left: 50px">产品大类</span>
                    </div>
                </td>
            </tr>-->               
            <tr>
                <td class="label">上级产品大类</td>
                <td >
                    <input id='shangji_text' class="easyui-textbox" value="" data-options="required:false,width:150,editable:true" disabled>
                </td>
                <td class="label">产品大类名称</td>
                <td >
                    <input id='text' class="easyui-textbox" data-options="required:false,width:150,editable:true">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                    <div class="divider" style="display: flex;justify-content: space-between;">
                        <span style="margin-left: 50px">产品大类参数</span>
                        <a href="javascript:void(0)" style="color: white;width: 60px;height: 30px;background: #3c8dbc;text-align: center;line-height: 30px;border-radius: 5px;" onclick="apendInput()">新增参数</a>
                    </div>
                </td>
            </tr>  
            <!--<tr> 
              <td class="label">风机型号</td>
                <td>
                    <input id='fanModel' class="easyui-textbox" value="" data-options="required:true,width:150,editable:true,precision:2">
                </td>
            </tr>
            <tr> 
              <td class="label">内径</td>
                <td >
                    <input id='boreSize' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true,precision:2">
                </td>
                <td class="label">外径</td>
                <td >
                    <input id='externalDiameter' class="easyui-numberbox" data-options="required:true,width:150,editable:true,precision:2">
                </td>
            </tr>
            <tr> 
              <td class="label">风筒板厚</td>
                <td >
                    <input id='airDuctLand' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true,precision:2">
                </td>
                <td class="label">风筒高度</td>
                <td >
                    <input id='airDuctHeight' class="easyui-numberbox" data-options="required:true,width:150,editable:true,precision:2">
                </td>
            </tr>  
            <tr> 
              <td class="label">叶片数量</td>
                <td >
                    <input id='bladeQuantity' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true">
                </td>
                <td class="label">叶片单价</td>
                <td >
                    <input id='bladeUnitPrice' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true,precision:2">
                </td>
            </tr> 
            <tr> 
              <td class="label">轮毂</td>
                <td >
                    <input id='hub' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true">
                </td>
                <td class="label">轴套</td>
                <td >
                    <input id='axleSleeve' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true,precision:2">
                </td>
            </tr> 
            <tr> 
              <td class="label">叶片标价</td>
                <td >
                    <input id='normalPrice' class="easyui-numberbox" value="" data-options="required:true,width:150,editable:true">
                </td>
            </tr>       -->
        </table>
	</div>
    <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>
<script type="text/javascript">
	
	var id = '<%=id%>';
	$(function(){
		if("null"==id){
		}else{
			getParent(id);
		}
	})
	//新增参数
	 
	function apendInput(){
		var html ="";
		html+='<tr id=xzInput align="left">';
			html+='<td colspan="4" class="addTd" style="height: 50px;">';
				html+='<span style="width: 54px;display: inline-block;">';
				html+='</span>';	
				html+='<input class="label-input easyui-textbox" style="width: 80px;" value="">';
				html+='<span style="width: 10px;display: inline-block;">';
				html+='</span>';	
				html+='<input class="input-text easyui-numberbox" data-options="precision:2,required:true" style="width: 160px;margin-left: 10px;" value="">';
				html+='<a class="deleteAn" href="javascript:void(0)" style="display:inline-block;width: 25px;line-height: 25px;box-sizing: border-box;height: 25px;border:1px solid #E0DFE3;border-radius: 50%;margin-left: 10px;" onclick="deleteInput(this)">';
					html+='<i style="line-height: 20px;display: inline-block;margin: 0 5px;" class="fa fa-minus">';
					html+='</i>';
				html+='</a>';	
			html+='</td>';
		html+='</tr>';	
		
		 $(".editTable").append(html);
		var label = $('.label-input');
		 for(var i= 0;i<label.length;i++){
		 		label.eq(i).attr("id","name_id"+i);
		 		$('#name_id'+i).textbox({
		 		})
		 }
		 var inputs=$('.input-text');
		 for(var j=0;j<inputs.length;j++){
		 	inputs.eq(j).attr("id","value_id"+j);		 	
		 	$('#value_id'+j).textbox({		
		 })
		 }
	}
	<!--删除新增框-->
	function deleteInput(req){	
		$(req).parents('tr').remove();	
	}
	
	
	function getParent(id){
		$.ajax({
            url: "/productCategory/findById",
            type: "POST",
            dataType:'json',
            data:  
            { 
              id:id
            },
            error: function() //失败
            { 
                messageSaveError();
            },
            success: function(data)//成功
            { 
            	console.log(data)
                var result = jQuery.parseJSON(JSON.stringify(data));
                department_pid = data;
                if(data != null)
                {
                    $('#shangji_text').textbox('setValue',data.text);
                }
                else messageSaveError();                
            }
            
        });
	}
	
    var ProductSort = new Object();
    ProductSort.name = '';
    var FanBaseParameter = new Object();
    
    var addProductClass = new Object();
    
    function save() { 
    	var addTd =$('.addTd');
		for(var i=0;i<addTd.length;i++){
			addProductClass[addTd[i].children[1].value] = addTd[i].children[4].value;
		}
    	console.log(addProductClass);
    	//拼装分类信息
        ProductSort.text= $('#text').textbox('getValue');
    	if("null"==id){
    	}else{
    		ProductSort.pid = id;
    	}
        //拼装参数信息
        //FanBaseParameter.pc_pid = id;
        /*
        FanBaseParameter.boreSize = $('#boreSize').numberbox('getValue');
        FanBaseParameter.externalDiameter = $('#externalDiameter').numberbox('getValue');
        FanBaseParameter.airDuctThickness = $('#airDuctLand').numberbox('getValue');
        FanBaseParameter.airDuctHeight = $('#airDuctHeight').numberbox('getValue');
        FanBaseParameter.bladeQuantity = $('#bladeQuantity').numberbox('getValue');
        FanBaseParameter.bladeUnitPrice = $('#bladeUnitPrice').numberbox('getValue');
        FanBaseParameter.hub = $('#hub').numberbox('getValue')
        FanBaseParameter.axleSleeve = $('#axleSleeve').numberbox('getValue')
        FanBaseParameter.normalPrice = $('#normalPrice').numberbox('getValue');*/
        FanBaseParameter.fanParameterJsonStr = JSON.stringify(addProductClass);
        $.ajax({
            url: "/productCategory/save", 
            type: "POST",
            dataType: 'json',
            data: {
            	
                'data': JSON.stringify(ProductSort),
                'fanParameter':JSON.stringify(FanBaseParameter)
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data)//成功
            {
                shuoheUtil.layerTopMaskOff();
                if (data.result == true) {
                	
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                   
                    parent.$('#tt').tree('reload');
                    shuoheUtil.layerMsgSaveOK('保存成功');
                    parent.layer.close(index); //再执行关闭
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }  
                
               
            }
        });
    }
</script> 