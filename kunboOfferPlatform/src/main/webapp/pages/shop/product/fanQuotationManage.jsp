<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>风机报价管理</title> 
    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:100%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20">
            <thead>
              <tr href="#">
                <th field="id"  hidden ="true">uuid</th>
                <th field="number" >风机编号</th>
                <th field="name" >风机名称</th>
                <th field="specificationsAndModels" >规格型号</th>
                <th field="pc_pid" >风机大类</th>
                <th field="em_pid" >风机模型</th>
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funDgAdd()">报价</a>      
	            <form id="queryForm" class="search-box">
	                  <input class="easyui-textbox" id='fanNumber' data-options="prompt:'风机编号'">
	                  <a id='btnQuery' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-search" onclick="functionQuery()" style="margin-left: 0px">搜索</a>          
	                  <a id='btnClear' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-refresh" onclick="funReset()" style="margin-left: 0px">清除</a>                     
	            </form>            
          
          </div>
          
          
       </div>
    </div>
</body>
</html>
<script type="text/javascript">

function funDgAdd(){
    Easyui.Layer.openLayerWindow(
      '新建风机报价',
      '/pages/shop/product/fanQuotation/addFanQuotation.jsp',
      Easyui.Layer.sizeTwoRow(600));
}
//function funDgEdit() {
//	if(Easyui.Datagraid.getSelectRow('dg') == null){
//	        shuoheUtil.layerMsgCustom('必须选择一个风机');
//  }
//	Easyui.Layer.openLayerWindow(
//     '修改风机报价',
//     '/pages/shop/product/fanQuotation/editFanQuotation.jsp?uuid='+$('#dg').datagrid('getSelected').id,
//     Easyui.Layer.sizeTwoRow(600));
//}
//function funDgDele() {
//// body...
//} 

//搜索
function functionQuery(){
			var fanNumber = $('#fanNumber').textbox('getValue');
			
			$.ajax({
				type:"get",
				url:"",
				data:{
					
				},
				success:function(msg){
				console.log(msg.data)
//					if(msg.data.rows ==null){
//						alert("success");
//					}
				},
				error:function (err){
					alert("没有此类型风机");
				}
			});

		}
//	重置
	function funReset(){
		$(".easyui-textbox").textbox('setValue','')
	}
  
</script>