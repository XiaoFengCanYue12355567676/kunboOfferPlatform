<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>材料管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%;}
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:100%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  url:'/develop/url/getUrl.do?name='+'getShopMachiningItem',
                  pageSize:20">
            <thead>
              <tr href="#">
                <th field="id"  hidden ="true">uuid</th>
                <th field="type" style="width: 10%;">加工件类型</th>
                <th field="supplier" style="width: 20%;">供应商</th>
                <th field="stockNumber" style="width: 20%;">存货编码</th>
                <th field="stockName" style="width: 20%;">存货名称</th>
                <th field="specificationsAndModels" style="width: 20%;">规格型号</th> 
                <th field="mainMeasurement" style="width: 5%;">主计量</th> 
                <th field="unitPrice" style="width: 5%;">单价</th> 
                
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funDgAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funDgDele()">删除</a>
          </div>
       </div>
    </div>

</body> 
</html>
<script type="text/javascript">
 
function funDgAdd(){

    Easyui.Layer.openLayerWindow(
      '新增工件',
      '/pages/shop/product/material/addWorkpiece.jsp',
      Easyui.Layer.sizeTwoRow(600));
}
function funDgEdit() {
	if(Easyui.Datagraid.getSelectRow('dg') == null){
	        shuoheUtil.layerMsgCustom('必须选择一个材料');
    }
	 console.info("555");
	Easyui.Layer.openLayerWindow(
       '修改工件',
       '/pages/shop/product/material/editWorkpiece.jsp?uuid='+$('#dg').datagrid('getSelected').id,
       Easyui.Layer.sizeTwoRow(600));
}
function funDgDele() {
  // body...
  console.info('funDgDele');
  var row = Easyui.Datagraid.getSelectRow('dg');
  console.log(row)
  if(row == null){
    shuoheUtil.layerMsgCustom('必须选择一个材料');
  }
  Easyui.Ajax.post(
    '/machiningItem/delete?id='+row.id,
    shuoheUtil.layerMsgError('删除失败'),
    shuoheUtil.layerMsgSaveOK('删除成功')
  );
  $('#dg').datagrid('reload');
}
function funDgSync() {
  // body...
}


  
</script>