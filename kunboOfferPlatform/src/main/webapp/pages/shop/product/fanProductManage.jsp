<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>风机产品管理</title>

		<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
		<link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
		<!-- FontAwesome字体图标 -->
		<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<!-- jQuery相关引用 -->
		<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
		<!-- TopJUI框架配置 -->
		<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
		<!-- TopJUI框架核心-->
		<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
		<script type="text/javascript" src="/TopJUI/topjui/js/datagrid-detailview.js"></script>
		<!-- TopJUI中文支持 -->
		<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

		<script type="text/javascript" src="/pages/js/moment.min.js"></script>
		<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
		<script type="text/javascript" src="/pages/js/util.js"></script>
		<script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

		<script type="text/javascript" src="/pages/js/base-loading.js"></script>
		<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
		<!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->

		<style type="text/css">
			html,
			body {
				margin: 0;
				height: 100%;
			}
		</style>
	</head>

	<body class="easyui-layout" style="overflow: hidden;">
		<script type="text/javascript">
		</script>
		<div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:20%;">
			<!-- <table id="tg" class="easyui-datagrid" style="height:100%;width: 100%;" title="" data-options="
                  singleSelect:true,
                  idField:'id',
                  treeField:'text',
                  method:'get',
                  toolbar:'#tg-button',
                  url:'/develop/url/getUrl.do?name='+'getFanModel'
                ">
          <thead>
              <tr>
              	  <th field="id" width="0px" hidden="true">序列</th>
                  <th field="text" width="100%">模型名称</th>
              </tr>
          </thead>
        </table>        
		<div id="tg-button"   style="height:35px;text-align: center;" > -->
			<!-- <ul id="tt" class="easyui-tree">
        	<li><span field="text" width="100%">大类产品</span> 
        		
        	</li>
        </ul>
        </div> -->

			<!--<table id="tg" class="easyui-treegrid" title="TreeGrid ContextMenu" style="width:700px;height:250px" data-options="
				iconCls: 'icon-ok',
				rownumbers: true,
				animate: true,
				collapsible: true,
				fitColumns: true,
				url: '/productCategory/getProductCategoryTree',
				method: 'get',
				idField: 'id',
				treeField: 'text',
				onContextMenu: onContextMenu
			">
		<thead>
			<tr id="sg">
				<th data-options="field:'text',width:180">Task Name</th>
				<th data-options="field:'number',width:60,align:'right'">风机编号</th>
				<th data-options="field:'begin',width:80">风机型号</th>
				<th data-options="field:'end',width:80">内径</th>
				<th data-options="field:'progress',width:120,formatter:formatProgress">Progress</th> 
			</tr>
		</thead>
	</table>
	<div id="mm" class="easyui-menu" style="width:120px;">
		<div onclick="append()" data-options="iconCls:'icon-add'">Append</div>
		<div onclick="removeIt()" data-options="iconCls:'icon-remove'">Remove</div>
		<div class="menu-sep"></div>
		<div onclick="collapse()">Collapse</div>
		<div onclick="expand()">Expand</div>
	</div>-->
			<ul id="tt" class="easyui-tree">
				<li><span field="text" width="100%">大类产品</span>

				</li>
			</ul>
		</div>
		<div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:60%;height: 100%">
			<!-- <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20">
            <thead>
              <tr href="#">
                <th field="id"  hidden ="true">uuid</th>
                <th field="number" >风机编号</th>
                <th field="name" >风机名称</th>
                <th field="specificationsAndModels" >规格型号</th>
                <th field="pc_pid" >风机大类</th>
                <th field="em_pid" >风机模型</th>
                <th field="cz_pid" >操作</th>
              </tr>
            </thead>
          </table>
         <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funDgAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funDgDele()">删除</a>
         </div> 
      </div> -->
			<!--<div id="tb" style="height:35px;padding-left: 23%;display: flex;align-items: center;" data-options="region:'east'">
				<div class="searchBox" style="float: left;overflow: hidden;line-height: 35px;width: 190px;display: flex;justify-content: space-between;align-items: center;">
					<div style="margin-left: 10px;">类型:</div>
					<input type="text" value="" style="margin-left: 5px;color: #E2E3E3;"/>
				</div>
				<div class="searchBox" style="float: left;overflow: hidden;line-height: 35px;width: 190px;display: flex;justify-content: space-between;align-items: center;">
					<div style="margin-left: 10px;">材料:</div>
					<input id="ssk" type="text" value="" style="margin-left: 5px;color: #E2E3E3;"/>
				</div>
				<div class="searchBox" style="float: left;overflow: hidden;line-height: 35px;width: 190px;display: flex;justify-content: space-between;align-items: center;">
					<div style="margin-left: 10px;">模型:</div>					
					<input type="text" value="" style="margin-left: 5px;color: #E2E3E3;"/>
				</div>
				<a href="javascript:void(0)" style="width: 50px;height: 20px;background: #3c8dbc;color: white;text-align: center;line-height: 20px;margin-left: 10px;border-radius: 4px;" onclick="funReset()">重置</a>
				<a href="javascript:void(0)" style="width: 50px;height: 20px;background: #3c8dbc;color: white;text-align: center;line-height: 20px;margin-left: 10px;border-radius: 4px;" onclick="funSearch()">搜索</a>
			</div>-->
			<div id="tb" style="height:45px;line-height: 45px;">
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-check',plain:true" onclick="accept()">应用</a>
				<a id='btnEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-hand-pointer-o',plain:true" onclick="seeFanMessage()">查看</a>
				<a id='btnDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-undo',plain:true" onclick="reject()">取消</a>
				<form id="queryForm" class="search-box" style="line-height: 45px;">
					<input class="easyui-textbox" id='equipment_lx' data-options="prompt:'类型'">
					<input class="easyui-textbox" id='equipment_cl' data-options="prompt:'材料'">
					<input class="easyui-textbox" id='equipment_mx' data-options="prompt:'模型'">
					<a id='btnQuery' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-search" onclick="functionQuery()" style="margin-left: 0px">搜索</a>
					<a id='btnClear' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-refresh" onclick="funReset()" style="margin-left: 0px">清除</a>
				</form>
			</div>
			<div>
				<table id="infoCardDg" class="easyui-datagrid" style="width:100%;height:auto" data-options="
                            iconCls: 'icon-edit',
                            singleSelect: true,
                            toolbar: '#tb',
                            method: 'get',
                            onDblClickRow: onDblClickRow
                        ">
					<thead>
						<tr>
							<th field="type" width="10%" editor="{type:'validatebox',options:{required:true}}">物料类型</th>
							<th field="encoded" width="10%" editor="text">物料编号</th>
							<th field="text1" width="10%" align="left" editor="{type:'textbox',options:{precision:1}}">物料名称</th>
							<th field="specificationsAndModels" width="20%" align="left" editor="textbox">型号规格</th>
							<th field="unit" width="10%" editor="text">单位</th>
							<th field="designFormulas" width="10%" editor="text">公式输入框</th>
							<th field="price" width="10%" editor="text">价格</th>
							<th field="scienceNum" width="10%" editor="text">材料用量</th>
							<th field="science" data-options="width:'10%',editor:{
		            	type:'combobox',
                        options:{
                        	valueField: 'label',
                            textField: 'value',
                            data:[{
                            	label: '木',
                                value: '木'
                            },{
                            	label:'铁',
                            	value:'铁'
                            }]
                        }
		            }">材料</th>
						</tr>
					</thead>
				</table>
			</div>
			<!--右键菜单定义如下-->
			<div id="mm" class="easyui-menu" style="width:120px;">
				<div onclick="addFan()" data-options="iconCls:'icon-add'">新增风机</div>
				<!--<div onclick="remove()" data-options="iconCls:'icon-remove'">移除</div>-->
			</div>
		</div>
		<script type="text/javascript">
			//表格初始化 
			/*    $(function(){
			    	$('#tg').datagrid({
			        	onClickRow: function (rowIndex)  { 
			          		var row= $('#tg').datagrid('getSelected'); 
			           	$('#dg').datagrid({
			               url:'/develop/url/getUrl.do?name='+'getFanProjectByUuid'+'&uuid='+row.id,   
			               method:'get'
			           }) 
			       	} 
			     	})
			   });  */
		</script>
	</body>

</html>
<script type="text/javascript">
	$(function() {
		$('#tt').tree({
			url: '/productCategory/getProductCategoryTree',
		})
		$('#infoCardDg').datagrid({
			data: {
				"total": 2,
				"rows": [{
						id: 1,
						type: '木质',
						encoded: '112',
						text1: '木头',
						specificationsAndModels: 'xx',
						unit: '个',
						designFormulas: '无',
						price: '20',
						scienceNum: '111',
						science: '木'
					},
					{
						id: 2,
						type: '铁质',
						encoded: '113',
						text1: '铁',
						specificationsAndModels: 'xxx',
						unit: '个',
						designFormulas: '无',
						price: '20',
						scienceNum: '111',
						science: '木'
					}
				]
			},

		});

	})

	//	取消
	function reject() {
		$('#infoCardDg').datagrid('rejectChanges');
		infoCard_editIndex = undefined;
	}

	function accept() {
		//判断是否填写了内容
		if(endEditing()) {
			$('#infoCardDg').datagrid('acceptChanges');
		}
	}

	var infoCard_editIndex = undefined;
	var echarts_editIndex = undefined;
	var config = new Object();

	function endEditing() {
		if(infoCard_editIndex == undefined) {
			return true
		}
		if($('#infoCardDg').datagrid('validateRow', infoCard_editIndex)) {
			$('#infoCardDg').datagrid('endEdit', infoCard_editIndex);
			infoCard_editIndex = undefined;
			return true;
		} else {
			return false;
		}
	}

	function onDblClickRow(index) {
		if(infoCard_editIndex != index) {
			if(endEditing()) {
				$('#infoCardDg').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
				infoCard_editIndex = index;
			} else {
				$('#infoCardDg').datagrid('selectRow', infoCard_editIndex);
			}
		}
	}
	//		右键菜单
	$('#tt').tree({
		onContextMenu: function(e, node) {
			e.preventDefault();
			// 查找节点
			$('#tt').tree('select', node.target);
			// 显示快捷菜单
			$('#mm').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		}
	});
	//新增风机弹框
	function addFan() {
		var rows = $('#tt').tree('getSelected');
		console.log(rows)
		if(rows == null) {
			shuoheUtil.layerMsgCustom('必须选择一个风机模型');
		}
		Easyui.Layer.openLayerWindow(
			'新建风机',
			'/pages/shop/product/fanProduct/addFanProduct.jsp?uuid=' + $('#tt').tree('getSelected').id,
			Easyui.Layer.sizeTwoRow(800));
	}
	//	查看风机信息	
	function seeFanMessage() {
		var nodes = $('#infoCardDg').datagrid('getSelected');
		console.log(nodes)
		if(nodes == null) {
			shuoheUtil.layerMsgCustom('必须选择一个风机');
		}
		Easyui.Layer.openLayerWindow(
			'查看风机',
			'/pages/shop/product/fanProduct/addFanProduct.jsp?uuid=' + $('#tt').tree('getSelected').id,
			Easyui.Layer.sizeTwoRow(800));

	}

	//	搜索
	function functionQuery() {
		var lxValue = $('#equipment_lx').textbox('getValue');
		var clValue = $('#equipment_cl').textbox('getValue');
		var mxValue = $('#equipment_mx').textbox('getValue');
		$.ajax({
			type: "get",
			url: "/machiningItem/findById",
			data: {
				1: 'lxValue',
				text1: 'clValue',
				3: 'mxValue'
			},
			success: function(msg) {
				console.log(msg.data)
				//					if(msg.data.rows ==null){
				//						alert("success");
				//					}
			},
			error: function(err) {
				alert("没有此类型风机");
			}
		});

	}
	//	重置
	//$('.searchBox input').attr('initvalue', function () { return this.value}) 
	function funReset() {
		//		$('.searchBox input').val(function () { 
		//			return this.getAttribute('initvalue');
		//		})

		$(".easyui-textbox").textbox('setValue', '')
	}

	function formatProgress(value) {
		if(value) {
			var s = '<div style="width:100%;border:1px solid #ccc">' +
				'<div style="width:' + value + '%;background:#cc0000;color:#fff">' + value + '%' + '</div>'
			'</div>';
			return s;
		} else {
			return '';
		}
	}

	function onContextMenu(e, row) {
		e.preventDefault();
		$(this).treegrid('select', row.id);
		$('#mm').menu('show', {
			left: e.pageX,
			top: e.pageY
		});
	}
	var idIndex = 100;

	function append() {
		idIndex++;
		var d1 = new Date();
		var d2 = new Date();
		d2.setMonth(d2.getMonth() + 1);
		var node = $('#tg').treegrid('getSelected');
		$('#tg').treegrid('append', {
			parent: node.id,
			data: [{
				id: idIndex,
				name: 'New Task' + idIndex,
				persons: parseInt(Math.random() * 10),
				begin: $.fn.datebox.defaults.formatter(d1),
				end: $.fn.datebox.defaults.formatter(d2),
				progress: parseInt(Math.random() * 100)
			}]
		})
	}

	function removeIt() {
		var node = $('#tg').treegrid('getSelected');
		if(node) {
			$('#tg').treegrid('remove', node.id);
		}
	}

	function collapse() {
		var node = $('#tg').treegrid('getSelected');
		if(node) {
			$('#tg').treegrid('collapse', node.id);
		}
	}

	function expand() {
		var node = $('#tg').treegrid('getSelected');
		if(node) {
			$('#tg').treegrid('expand', node.id);
		}
	}

	/*  $('#dg').datagrid({
		data:[{
			number:'2',
			name:'aa',
			specificationsAndModels:'xx',
			pc_pid:'00',
			em_pid:'qq',
			cz_pid:' '
		}]
	})  */

	function funDgAdd() {

		if(Easyui.Datagraid.getSelectRow('tg') == null) {
			shuoheUtil.layerMsgCustom('必须选择一个风机模型');
		}
		Easyui.Layer.openLayerWindow(
			'新建风机',
			'/pages/shop/product/fanProduct/addFanProduct.jsp?uuid=' + $('#tg').datagrid('getSelected').id,
			Easyui.Layer.sizeTwoRow(800));
	}

	function funDgEdit() {
		if(Easyui.Datagraid.getSelectRow('dg') == null) {
			shuoheUtil.layerMsgCustom('必须选择一个风机');
		}
		console.info("555");
		Easyui.Layer.openLayerWindow(
			'修改风机',
			'/pages/shop/product/fanProduct/editFanProduct.jsp?uuid=' + $('#dg').datagrid('getSelected').id,
			Easyui.Layer.sizeTwoRow(800));
	}

	function funDgDele() {
		// body...

	}
</script>