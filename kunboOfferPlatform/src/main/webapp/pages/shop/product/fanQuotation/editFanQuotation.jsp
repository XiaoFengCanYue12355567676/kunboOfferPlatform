<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = request.getParameter("uuid");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改风机</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true">  
	     <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:30px;" >
	        <tr>
	            <td class="label">工时编码</td>
	            <td >
	                <input id='encoded' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >
	            </td>
	            <td class="label">工时名称</td>
	            <td >
	                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >       
	            </td>
	        </tr>
	        <tr>
	            <td class="label">工时单价</td>
	            <td >
	                <input id='price' class="easyui-numberbox" data-options="required:false,width:200,editable:true">               
	            </td>
	        </tr>        
		</table>
		<div style="padding-left: 112px;padding-right: 115px;">
			<table id="parts" style="width:100%;height:200px;">
				<thead>
					<tr>
						<th field="id" width="0px" hidden="true">序列</th>
						<th field="partsName" data-options="editor:{type:'textbox',options:{editable:false}}">配件名称</th>
						<th field="partsUnit" editor="text">配件单位</th>
						<th field="partsType" editor="text">配件类型</th>
						<th field="text" editor="text">默认材料</th>
						<th field="text" editor="text">材料用量</th>
						<th field="standardPrice" editor="text">标准价格</th>
						<th field="ifMustChoose" data-options="editor:
						{
							type:'combobox',
							options:
							{
								valueField: 'value',
								textField: 'label',
								data: 
									[{
										label: '是',
										value: 'true'
									},{
										label: '否',
										value: 'false'
									}]
							}
						}
					">是否必选</th>
					</tr>
				</thead>
			</table>
			<div id="tb" style="padding:0px 30px;height: 35px">
				<a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionModify()">修改</a>
				<a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-check" onclick="accept()">保存</a>
			</div>
		</div>
	</div>
	<div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>

<script type="text/javascript">
var uuid = '<%=uuid%>'	
 	var createTime ='';
	$(function(){
		$('#parts').edatagrid({
			url: '',
			singleSelect:true,
			autoRowHeight:false,
			pagination:false,
			fitColumns:true,
			striped:true,
			toolbar:'#tb'
		})
	    $.ajax({
	        url: '/develop/url/getUrl.do?name='+'getManHourByUuid'+'&uuid='+GetQueryString('uuid'),
	        type: "GET",
	        dataType: 'json',
	        success: function(data) //成功
	        {
	        	if(null!=data){
					$('#encoded').textbox('setValue',data[0].encoded);
					$('#name').textbox('setValue',data[0].name); 
					$('#price').textbox('setValue',data[0].price);
					createTime = data[0].createTime ;
	        	}
        	
      	    }
    	});
	});
function functionModify() {
    var row= $('#parts').datagrid('getSelected')
    if (row==null) {
        shuoheUtil.layerMsgCustom('必须选择一个风机配件');
        return false
    }
    var index = $('#parts').datagrid('getRowIndex', row)
	$('#parts').edatagrid('editRow', index)
}
function accept() {
    $('#parts').datagrid('acceptChanges');
}   
	function save(){
		
	    var obj = new Object();
	    console.info('save');
	    obj.id = GetQueryString('uuid');
	    obj.createTime = createTime;
	    obj.encoded = $('#encoded').textbox('getValue');  
        obj.name = $('#name').textbox('getValue');   
        obj.price = $('#price').numberbox('getValue'); 
       
	    $.ajax({
	        url: "/ManHour/update", 
	        type: "POST",
	        dataType: 'json',
	        data: {
	            'data': JSON.stringify(obj)
	        },
	        error: function() //失败
	        {
	//          shuoheUtil.layerTopMaskOff();
	//          shuoheUtil.layerTopMsgError('远程通信失败');
	        },
	        success: function(data) //成功
	        {
	            shuoheUtil.layerTopMaskOff();
	            if (data.result == true) {
	                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
	                parent.layer.close(index); //再执行关闭
	                parent.$('#dg').datagrid('reload');
	                shuoheUtil.layerMsgSaveOK('保存成功');
	            } else {
	                shuoheUtil.layerTopMsgError(data.describe);
	            }
	        }
	    });
	}
</script>