<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.UUID"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String modelId = request.getParameter("uuid");
    String uuid = UUID.randomUUID().toString();
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建风机</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
	<div data-options="region:'north',iconCls:'icon-reload',title:'',split:false,border:true" style="width:100%;height:14%">  
        <div style="margin-top: 5px">
        	<table  class="editTable" style="text-align: center;margin-left:30px;"  >
	        <tr>
	            <td class="label">风机编号</td>
	            <td >
	                <input id='fanCode' class="easyui-combobox" data-options="required:false,width:200,editable:true"  >
	            </td>
	            <td class="label"></td>
	            <td class="label">
	            	<a onclick='searchFan()'  class="button button-primary button-rounded button-small">筛选</a>
	            </td> 
	        </tr>
        </table>
            
        </div>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:false,border:true" style="width:100%;height: 72%"> 
	    <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:30px;" >
	        <tr>
	            <td class="label">风机编号</td>
	            <td >
	                <input id='number' class="easyui-textbox" data-options="required:false,width:200" disabled="true" >
	            </td>
	            <td class="label">风机名称</td>
	            <td >
	                <input id='name' class="easyui-textbox" data-options="required:false,width:200" disabled="true" >       
	            </td>
	        </tr>
	        <tr>
	            <td class="label">规格型号</td>
	            <td >
	                <input id='specificationsAndModels' class="easyui-textbox" data-options="required:false,width:200" disabled="true">               
	            </td>
	            <td class="label">风机大类</td>
	            <td >
	                <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:200" disabled="true">               
	            </td>
	        </tr>    
	        <tr>
	            <td class="label">风机模型</td>
	            <td >
	                <input id='em_pid' class="easyui-textbox" data-options="required:false,width:200" disabled="true">               
	            </td>
	        </tr> 
        </table>
        <div style="padding-left: 112px;padding-right: 115px;">
        	<div class="field-con-title" style="margin-bottom: 10px;">配件列表</div> 
            <table id="parts" class='easyui-datagrid' style="width:100%;height:500px" title="" data-options="
                    iconCls: 'icon-edit',
                    singleSelect: false,
                    toolbar: '#tb',
                    method: 'get',
                    fitColumns:false,
                    checkOnSelect:true,
                    selectOnCheck:true,
                    onDblClickRow: onDblClickRow,
                    onClickCell:onClickCell"
                >
                <thead>
                    <tr>
                        <th field="id" width="0px" hidden="true">序列</th>
                        <th field="partsName" >配件名称</th> 
                        <th field="partsUnit"  >配件单位</th>
                        <th field="partsType">配件类型</th> 
	                        
                        <th field="nsp_name">配件/材料/工时</th> 
	                        
                        <th field="partsValue">用量/金额</th> 
                        <th field="standardPrice" data-options="width:50">小计</th>                        
                        <th field="ifMustChoose" data-options="hidden:true">是否必选</th>
                        <th field="isChoose">是否选配</th>
                        <th field="xxxxx" data-options="hidden:true">是否选配</th>
                        <th field="nsp_pd" data-options="hidden:true,width:50">nsp编码</th>
                          
                    </tr>
                </thead>
            </table>
            <div id="tb" style="padding:0px 30px;height: 35px">
                <!-- <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionAdd()">新增</a> -->
                <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionModify()">修改</a>
                <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-check" onclick="accept()">保存</a>
            </div>
        </div>
	</div>
	<div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="width:100%;height: 14%">  
        <div style="margin-top: 5px">
        	<table  class="editTable" style="text-align: center;margin-left:30px;"  >
		        <tr>
		            <td class="label">价格合计</td>
		            <td >
		                <input id='jiageheji' class="easyui-numberbox" data-options="required:false,width:200" disabled="true"  >
		            </td>
		            <td class="label"></td>
		            <td class="label">
		            	<a onclick='calculationPrice()' class="button button-primary button-rounded button-small">计算</a>
		            </td> 
		            <td class="label" >
		            	<a onclick='save()'  class="button button-primary button-rounded button-small" >保存</a>
		            </td> 
		            
		        </tr>
	        </table>
        </div>
    </div>
</body> 
</html>
<script type="text/javascript">


var modelId = '<%=modelId%>';
var uuid = '<%=uuid%>';
var o;
var init_flag = false;
$(function(){
    $('#parts').edatagrid({
        url: '',
        singleSelect:true,
        autoRowHeight:false,
        pagination:false,
        fitColumns:true,
        striped:true,
        toolbar:'#tb',
        columns:[[
            {field:'id',title:'id',hidden:true},
            {field:'partsName',title:'配件名称'},
            {field:'partsUnit',title:'配件单位'},
            {field:'partsType',title:'配件类型'},                                
            {field:'nsp_name', title:'配件/材料/工时'},
            {field:'partsValue',title:'用量/金额'},
            {field:'standardPrice',title:'小计'},
            {field:'nsp_pd',hidden:true},
            {field:'ifMustChoose',hidden:true},
            {field:'isChoose',title:'是否选配',
                formatter: function(value,row,index)
                {
                var htmlstr = '<input id="'+row.id+'" class="easyui-switchbutton" checked>';  
                return htmlstr;
                }
            },
            {field:'nsp_pd',hidden:true},          
            {field:'xxxxx',hidden:true}                               
        ]],
        onLoadSuccess:function(data)
        {
            // for(var i=0;i<o.obj.total;i++)
            // {
            //     console.info(i+' ifMustChoose = '+o.obj.rows[i].ifMustChoose);
            //     var is_check = o.obj.rows[i].ifMustChoose;
            //     console.info(i);

            //     $('#'+o.obj.rows[i].id).switchbutton({
            //         checked: is_check,      
            //         onText:'选配',
            //         offText:'不选',
            //         height:'23px',
            //         onChange: function(checked)
            //         {        
            //             console.log(checked+"   "+i);
            //             console.log(checked);
            //         }
            //     })
            //     if(o.obj.rows[i].ifMustChoose == false)
            //     {
            //         $('#'+o.obj.rows[i].id).switchbutton('disable');

            //     }
            // }
            reloadSwitch();
            calculationPrice();
        }
    })
    $('#pc_p都是id').combobox({
        url:'/develop/url/getUrl.do?name='+'getFanType',  
        valueField:'id',
        textField:'text',
        onSelect: function (row) { 
       		getModel(row);
       }
    });


    $('#fanCode').combobox({
        url:'/develop/url/getUrl.do',
        valueField:'specificationsAndModels',    
        textField:'name',
        mode:'remote',
        queryParams: {
            "name" : 'seachFanByCode',                                
        },
        onSelect: function (row) { 
            // console.log(JSON.stringify(data));
	      	$('#number').textbox('setValue',row.number);
	      	$('#name').textbox('setValue',row.name);
	      	$('#specificationsAndModels').textbox('setValue',row.specificationsAndModels);
	      	$('#pc_pid').textbox('setValue',row.pc_name);
	      	$('#em_pid').textbox('setValue',row.em_name);
	      	console.log("row.df_pid==="+row.id)
	      	// $('#parts').datagrid({
            //     url:'/develop/url/getUrl.do?name='+'getFanPartsByUuid'+'&uuid='+row.id,   
            //     method:'get'
            // }) 
            
            o = Easyui.Ajax.get('/develop/url/getUrl.do?name='+'getFanPartsByUuid'+'&uuid='+row.id);
            console.info(o);
            Easyui.Datagraid.loadData('parts',o.obj.total,o.obj.rows);
       }
    });
    
   
})

function getModel(row) {
	 console.log("dsds "+row.id);
	$('#em_pid').combobox({
         url:'/develop/url/getUrl.do?name='+'getFanModelByUuid'+'&uuid='+row.id,    
        valueField:'id',
        textField:'text',
        onSelect: function (row) { 
       	 	$('#parts').datagrid({
                url:'/develop/url/getUrl.do?name='+'getPartsByModelUuid'+'&uuid='+row.id,   
                method:'get'
            });
       }
    });
}

function functionAdd() {
	$('#parts').datagrid('appendRow', {})
}
function functionModify() {
    var row= $('#parts').datagrid('getSelected')
    if (row==null) {
        shuoheUtil.layerMsgCustom('必须选择一个风机配件');
        return false
    }
    var index = $('#parts').datagrid('getRowIndex', row)
	$('#parts').edatagrid('editRow', index)
}

//配件保存

 //获取风机配件
function getParts() {
    var rows = $('#parts').datagrid('getRows');
    return rows;
}
function searchFan(){
	console.log("搜索风机"+$('#code').textbox('getValue')  );
    $.ajax({
        url:'/develop/url/getUrl.do?name='+'seachFanByCode'+'&code='+$('#code').textbox('getValue'),   
        type: "POST",
        dataType: 'json',
        error: function() //失败
        {
//             shuoheUtil.layerTopMaskOff();
//             shuoheUtil.layerTopMsgError('远程通信失败');
        },
        success: function(data) {  
      	
	      	console.log(JSON.stringify(data));
	      	$('#number').textbox('setValue',data[0].number);
	      	$('#name').textbox('setValue',data[0].name);
	      	$('#specificationsAndModels').textbox('setValue',data[0].specificationsAndModels);
	      	$('#pc_pid').textbox('setValue',data[0].pc_name);
	      	$('#em_pid').textbox('setValue',data[0].em_name);
	      	console.log("data[0].df_pid==="+data[0].id)
	      	$('#parts').datagrid({
                url:'/develop/url/getUrl.do?name='+'getFanPartsByUuid'+'&uuid='+data[0].id,   
                method:'get'
        	}) 
        }
    }); 
}
//价格合计
function calculationPrice(){
    console.log("价格合计");
    
    var rows = Easyui.Datagraid.getAllRows('parts');
    console.info(rows);
    var jageheji = 0;
    for(var i=0;i<rows.length;i++)
    {
        // console.info('isChoose = '+rows[i].isChoose);
        if(rows[i].ifMustChoose == false || (rows[i].ifMustChoose == true&&rows[i].xxxxx == true))
        {
            console.info(rows[i].standardPrice);
            jageheji+=parseFloat(rows[i].standardPrice);
            $('#jiageheji').textbox('setValue',jageheji);
        }
    }


}
function save() {
	//风机信息
    var obj = new Object(); //
    obj.number = $('#number').textbox('getValue');  
    obj.name = $('#name').textbox('getValue');  
    obj.specificationsAndModels = $('#specificationsAndModels').numberbox('getValue');  
    obj.pc_pid = $('#pc_pid').combobox('getValue');  
    obj.em_pid = $('#em_pid').combobox('getValue'); 
    console.log("大类"+$('#pc_pid').combobox('getValue'))
     console.log("模型"+$('#em_pid').combobox('getValue'))
    //风机配件
    var fanObj = new Object(); 
    fanObj  =  getParts() ;
    $.ajax({
        url: "//save", 
        type: "POST",
        dataType: 'json',
        data: {
            'data': JSON.stringify(obj),
            'fanPartsList':JSON.stringify(fanObj),
        },
        error: function() //失败
        {
//             shuoheUtil.layerTopMaskOff();
//             shuoheUtil.layerTopMsgError('远程通信失败');
        },
        success: function(data) {  //成功
      	
        shuoheUtil.layerTopMaskOff();
        if (data.result == true) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
            parent.layer.close(index); //再执行关闭
            parent.$('#dg').datagrid('reload');
            shuoheUtil.layerMsgSaveOK('保存成功');
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
}
</script>
<script>

function onDblClickRow(index){
    console.info('editIndex = '+editIndex +' index = '+index);
    if (editIndex != index){
        changeColumeCombobox();
        if (endEditing()){ 
            console.info('beginEdit');
            // console.info('row.partsType ='+row.partsType);
            $('#parts').datagrid('selectRow', index)
                .datagrid('beginEdit', index);
            editIndex = index;
        } else {
            console.info('onClickRow  selectRow');
            $('#parts').datagrid('selectRow', editIndex);
        }
    }
}
function onClickCell(index,field,value)
{
    // changeColumeComboboxxxxxx(index);
    // $('#parts').datagrid('beginEdit', index);
    // var ed = $('#parts').datagrid('getEditor', {index:index,field:field});
    // $(ed.target).focus();
}

var editIndex = undefined;
function append(){
    if (endEditing()){
        $('#parts').datagrid('appendRow',{text:''});
        editIndex = $('#parts').datagrid('getRows').length-1;
        $('#parts').datagrid('selectRow', editIndex)
                .datagrid('beginEdit', editIndex);
    }
}
function endEditing(){
    if (editIndex == undefined){return true}
    if ($('#parts').datagrid('validateRow', editIndex)){
        // var ed = $('#parts').datagrid('getEditor', {index:editIndex,field:'productid'});
        // var productname = $(ed.target).combobox('getText');
        // $('#parts').datagrid('getRows')[editIndex]['productname'] = productname;
        $('#parts').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function removeit(){
    if (editIndex == undefined){return}
    $('#parts').datagrid('cancelEdit', editIndex)
            .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept(){
    if (endEditing()){
        reloadSwitch();
        $('#parts').datagrid('acceptChanges');
    }

    console.info( Easyui.Datagraid.getAllRows('parts'));
}
function reject(){
    $('#parts').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges(){
    var rows = $('#parts').datagrid('getChanges');
    alert(rows.length+' rows are changed!');
}

function functionAdd() {
	$('#parts').datagrid('appendRow', {})
}
function functionModify() {
    var row= $('#parts').datagrid('getSelected')
    if (row==null) {
        shuoheUtil.layerMsgCustom('必须选择一个风机配件');
        return false
    }
    var index = $('#parts').datagrid('getRowIndex', row)
	$('#parts').edatagrid('editRow', index)
}


function changeColumeComboboxxxxxx(index)
{

}
function changeColumeCombobox()
{
    var row = Easyui.Datagraid.getSelectRow('parts');
    console.info(row.partsType);



    if(row.partsType == '材料')
    {
        console.info(row.partsType);
        // console.info('flag = '+flag);
        $('#parts').datagrid({
            columns:[[
                    {field:'id',title:'id',hidden:true},
                    {field:'partsName',title:'配件名称'},
                    {field:'partsUnit',title:'配件单位'},
                    {field:'partsType',title:'配件类型'},                                
                    {
                        field:'nsp_name', title:'配件/材料/工时',
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'name',
                                textField:'name',
                                method:'get',
                                async: false,
                                url:'/develop/url/getUrl.do?name='+'getMaterialForFan',
                                onSelect: function (row) {
                                    console.info(JSON.stringify(row));
                                    var index = Easyui.Datagraid.getSelectRowIndex('parts');
                                    var r = Easyui.Datagraid.getSelectRow('parts');
                                    r.nsp_pd = row.id
                                    r.standardPrice = parseFloat(row.price)*parseFloat(r.partsValue);
                                    console.info('index = '+index);
                                    $('#parts').datagrid('updateRow',{
                                        index: index,
                                        row: r
                                    });
                                    calculationPrice();
                                },
                                onLoadSuccess:function(data)
                                {
                                    reloadSwitch();
                                }
                            }
                        }
                    },
                    {field:'partsValue',title:'用量/金额'},
                    {field:'standardPrice',title:'小计'},
                    {field:'nsp_pd',hidden:true},
                    {field:'ifMustChoose',hidden:true},
                    {field:'isChoose',title:'是否选配',
                        formatter: function(value,row,index)
                        {
                        var htmlstr = '<input id="'+row.id+'" class="easyui-switchbutton" checked>';  
                        return htmlstr;
                        }
                    },
                    {field:'nsp_pd',hidden:true},          
                    {field:'xxxxx',hidden:true}                  
            ]],
            // onBeforeLoad:function(data)
            // {
            //     reloadSwitch();
            // }
        });
    }
    else if(row.partsType == '工时'){
        // console.info('flag = '+flag);
        console.info(row.partsType);
        $('#parts').datagrid({
            columns:[[
                    {field:'id',title:'id',hidden:true},
                    {field:'partsName',title:'配件名称'},
                    {field:'partsUnit',title:'配件单位'},
                    {field:'partsType',title:'配件类型'},                                
                    {
                        field:'nsp_name', title:'配件/材料/工时',
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'name',
                                textField:'name',
                                method:'get',
                                async: false,
                                url: '/develop/url/getUrl.do?name='+'getManhourForFan',
                                onSelect: function (row) {
                                    console.info(JSON.stringify(row));
                                    var index = Easyui.Datagraid.getSelectRowIndex('parts');
                                    var r = Easyui.Datagraid.getSelectRow('parts');
                                    r.standardPrice = parseFloat(row.price)*parseFloat(r.partsValue);
                                    r.nsp_pd = row.id
                                    console.info('index = '+index);
                                    $('#parts').datagrid('updateRow',{
                                        index: index,
                                        row: r
                                    });
                                    calculationPrice();
                                },
                                onLoadSuccess:function(data)
                                {
                                    reloadSwitch();
                                }
                            }
                        }
                    },
                    {field:'partsValue',title:'用量/金额'},
                    {field:'standardPrice',title:'小计'},
                    {field:'nsp_pd',hidden:true},
                    {field:'ifMustChoose',hidden:true},
                    {field:'isChoose',title:'是否选配',
                        formatter: function(value,row,index)
                        {
                        var htmlstr = '<input id="'+row.id+'" class="easyui-switchbutton" checked>';  
                        return htmlstr;
                        }
                    },
                    {field:'nsp_pd',hidden:true},          
                    {field:'xxxxx',hidden:true}                         
            ]],
            // onBeforeLoad:function(data)
            // {
            //     reloadSwitch();
            // }
        });
    }
    else if(row.partsType == '标准'){
        $('#parts').datagrid({
            columns:[[
                {field:'id',title:'id',hidden:true},
                {field:'partsName',title:'配件名称'},
                {field:'partsUnit',title:'配件单位'},
                {field:'partsType',title:'配件类型'},                                
                {
                    field:'nsp_name', title:'配件/材料/工时',
                },
                {field:'partsValue',title:'用量/金额'},
                {field:'standardPrice',title:'小计'},
                {field:'nsp_pd',hidden:true},
                {field:'ifMustChoose',hidden:true},
                {field:'isChoose',title:'是否选配',
                        formatter: function(value,row,index)
                        {
                        var htmlstr = '<input id="'+row.id+'" class="easyui-switchbutton" checked>';  
                        return htmlstr;
                        }
                },
                {field:'nsp_pd',hidden:true},          
                    {field:'xxxxx',hidden:true}                                          
            ]],
            onBeforeLoad:function(data)
            {
                reloadSwitch();
            }
        });
    }
}

function reloadSwitch()
{
    // o.obj.total,o.obj.rows
    console.info('reloadSwitch');

    var row = Easyui.Datagraid.getSelectRow('parts');
    // if(row.partsType != '标准')
    // {
    //     var is_check = row.ifMustChoose;
    //     // console.info(i);

    //     $('#'+row.id).switchbutton({
    //         checked: is_check,      
    //         onText:'选配',
    //         offText:'不选',
    //         height:'23px',
    //         onChange: function(checked)
    //         {        
    //             // console.log(checked+"   "+i);
    //             console.log(checked);
    //         }
    //     })
    //     if(row.ifMustChoose == false)
    //     {
    //         $('#'+row.id).switchbutton('disable');


    //     }
    // }


    // var data = Easyui.Datagraid.getAllRows('parts');
    for(var i=0;i<o.obj.total;i++)
    {
        // console.info(i+' ifMustChoose = '+o.obj.rows[i].ifMustChoose);
        var is_check = o.obj.rows[i].ifMustChoose;
        // console.info(i);

        $('#'+o.obj.rows[i].id).switchbutton({
            checked: false,      
            onText:'选配',
            offText:'不选',
            height:'23px',
            onChange: function(checked)
            {        
                // console.log(checked+"   "+i);
                // console.log(checked);
                var rows = Easyui.Datagraid.getAllRows('parts');
                for(var i=0;i<rows.length;i++)
                {
                    if(this.id==rows[i].id)
                    {
                        var r = rows[i];
                        r.xxxxx = checked;
                        $('#parts').datagrid('updateRow',{
                            index: i,
                            row: r
                        });
                        calculationPrice();
                        break;
                    }
                }
            }
        })
        if(o.obj.rows[i].ifMustChoose == false)
        {
            $('#'+o.obj.rows[i].id).switchbutton('disable');


        }
    }
}

</script>