<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = request.getParameter("uuid");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改材料</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true"  ">  
	     <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:30px;" >
	        <tr>
                <td class="label">加工件类型</td>
                <td >
                    <select class="easyui-combobox ycl" id="type" name="language" style="width: 200px;">
                    	<option value="软连接">软连接</option>
                    	<option value="橡胶垫">橡胶垫</option>
                    	<option value="填料函">填料函</option>
                    	<option value="法兰">法兰</option>
                    	<option value="接线盒">接线盒</option>
                    	<option value="减振器">减振器</option>
                    	<option value="防护网">防护网</option>
                    	<option value="进风口">进风口</option>
                    	<option value="机壳组">机壳组</option>
                    	<option value="支撑板">支撑板</option>
                    </select>
                </td>
            </tr>
	        <tr>
	            <td class="label">供应商</td>
	            <td >
	                <input id='supplier' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >
	            </td>
	            <td class="label">存货编码</td>
	            <td >
	                <input id='stockNumber' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >       
	            </td>
	        </tr>
	        <tr>
	            <td class="label">存货名称</td>
	            <td >
	                <input id='stockName' class="easyui-textbox" data-options="required:false,width:200,editable:true">
	            </td>
	            <td class="label">规格型号</td>
	            <td >
	                <input id='specificationsAndModels' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
	            </td>
	        </tr>
	        <tr>
	            <td class="label">主计量</td>
	            <td >
	                <input id='mainMeasurement' class="easyui-textbox" data-options="prompt:'如：个、时、捆、根',required:false,width:200,editable:true">
	            </td>
	            <td class="label">单价</td>
	            <td >
	                <input id='unitPrice' class="easyui-numberbox" data-options="required:false,width:200,editable:true,min:0,precision:2">               
	            </td>
	        </tr>      
	    </table>
	</div>
	<div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>

<script type="text/javascript">
var uuid = '<%=uuid%>';
 	var createTime ='';
	$(function(){

	    $.ajax({
	        url: '/machiningItem/findById',
	        type: "post",
	        dataType: 'json',
	        data:{id:uuid},
	        success: function(data) //成功
	       
	        {
	         console.log(data)
	         
	        	if(null!=data){
					$('#type').combobox('setValue',data.type);
					$('#supplier').textbox('setValue',data.supplier);
					$('#stockNumber').textbox('setValue',data.stockNumber);
					$('#stockName').textbox('setValue',data.stockName);
					$('#stockName').textbox('setValue',data.stockName);					
					$('#specificationsAndModels').textbox('setValue',data.specificationsAndModels);
					$('#mainMeasurement').textbox('setValue',data.mainMeasurement);
					$('#unitPrice').numberbox('setValue',data.unitPrice);
					
					createTime = data.createTime ;
	        	}
        	
      	    }
    	});
    });
    
	function save(){
		
	    var obj = new Object();
	    console.info('save');
	    obj.id = GetQueryString('uuid');
	    obj.type = $('#type').combobox('getValue');
        obj.supplier = $('#supplier').textbox('getValue');  
        obj.stockNumber = $('#stockNumber').textbox('getValue');  
        obj.stockName = $('#stockName').textbox('getValue');  
        obj.specificationsAndModels = $('#specificationsAndModels').textbox('getValue');
        obj.mainMeasurement = $('#mainMeasurement').textbox('getValue');  
        obj.unitPrice = $('#unitPrice').numberbox('getValue'); 
       
	    $.ajax({
	        url: "/machiningItem/update", 
	        type: "POST",
	        dataType: 'json',
	        data: {
	            'data': JSON.stringify(obj)
	        },
	        error: function() //失败
	        {
	//          shuoheUtil.layerTopMaskOff();
	//          shuoheUtil.layerTopMsgError('远程通信失败');
	        },
	        success: function(data) //成功
	        {
	            shuoheUtil.layerTopMaskOff();
	            if (data.result == true) {
	                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
	               
	                parent.$('#dg').datagrid('reload');
	                shuoheUtil.layerMsgSaveOK('保存成功');
	                 parent.layer.close(index); //再执行关闭
	            } else {
	                shuoheUtil.layerTopMsgError(data.describe);
	            }
	        }
	    });
	}
</script>