<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.UUID"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String modelId = request.getParameter("uuid");
    String uuid = UUID.randomUUID().toString();
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建风机</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true">  
	    <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:30px;" >
	        <tr>
	            <td class="label">风机编号</td>
	            <td >
	                <input id='number' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >
	            </td>
	            <td class="label">风机名称</td>
	            <td >
	                <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true"  >       
	            </td>
	        </tr>
	        <tr>
	            <td class="label">规格型号</td>
	            <td >
	                <input id='specificationsAndModels' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
	            </td>
	        </tr>    
        </table>
        <div style="padding-left: 112px;padding-right: 115px;">
            <table id="parts" class='easyui-datagrid' style="width:100%;height:500px" title="" data-options="
                    iconCls: 'icon-edit',
                    singleSelect: true,
                    toolbar: '#tb',
                    method: 'get',
                    onClickRow: onClickRow"
                >
                <thead>
                <tr href="#">
                    <th field="id"  hidden ="true"></th>
                    <th field="partsName">配件名称</th>
                    <th field="partsUnit">配件单位</th>
                    <th data-options="field:'partsType',
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'value',
                                textField:'label',
                                data: 
                                [{
                                    label: '材料',
                                    value: '材料'
                                },{
                                    label: '工时',
                                    value: '工时'
                                }]
                            }
                        }">
                        配件类型</th>
                    <th data-options="field:'nsp_name',width:130,
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'name',
                                textField:'name',
                                method:'get',
                                url:'/develop/url/getUrl.do?name='+'getMaterialForFan',
                            }
                        }">
                        配件/材料/工时</th>                    
                    <th field="partsValue" data-options="editor:
                        {
                            type:'numberbox',
                            options:{
                                min:0,
                                precision:2
                            }
                        }
                        ,width:80                    
                    ">用量</th>
                    <th field="nsp_pd" data-options="hidden:true,width:50">nsp编码</th>
                </tr>
                </thead>
            </table>    
            <div id="tb" style="padding:0px 30px;height: 35px">
                <!--<a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-check" onclick="accept()">保存</a>-->
                <div style="line-height: 35px;">配件修改</div>
            </div>
        </div>
	</div>
	<div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px">  
        <div style="text-align: right;line-height: 45px;margin-right: 10px;">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div>
</body> 
</html>
<script type="text/javascript">


$(function(){
    var o = Easyui.Ajax.get('/develop/url/getUrl.do?name=getPartsByModelUuid&uuid='+shuoheUtil.getUrlHeader().uuid);
    console.info(o);
    Easyui.Datagraid.loadData('parts',o.obj.length,o.obj);
})




function changeColumeCombobox()
{
    var row = Easyui.Datagraid.getSelectRow('parts');
    console.info(row.partsType);

    if(row.partsType == '材料')
    {
        console.info(row.partsType);
        // console.info('flag = '+flag);
        $('#parts').datagrid({
            columns:[[
                    {field:'id',title:'id',hidden:true},
                    {field:'partsName',title:'配件名称'},
                    {field:'partsUnit',title:'配件单位'},
                    {field:'partsType',title:'配件类型'},                                
                    {
                        field:'nsp_name', title:'配件/材料/工时',width:130,
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'name',
                                textField:'name',
                                method:'get',
                                url:'/develop/url/getUrl.do?name='+'getMaterialForFan',
                                onSelect: function (row) {
                                    console.info(JSON.stringify(row));
                                    var index = Easyui.Datagraid.getSelectRowIndex('parts');
                                    var r = Easyui.Datagraid.getSelectRow('parts');
                                    r.nsp_pd = row.id
                                    console.info('index = '+index);
                                    $('#parts').datagrid('updateRow',{
                                        index: index,
                                        row: r
                                    });
                                }
                            }
                        }
                    },
                    {field:'partsValue',title:'用量',
                        editor:{
                            type:'numberbox',width:80,
                            options:{
                                min:0,
                                precision:2
                            }
                    }},
                    {field:'nsp_pd',hidden:true}                     
            ]]
        });
    }
    else if(row.partsType == '工时'){
        // console.info('flag = '+flag);
        console.info(row.partsType);
        $('#parts').datagrid({
            columns:[[
                    {field:'id',title:'id',hidden:true},
                    {field:'partsName',title:'配件名称'},
                    {field:'partsUnit',title:'配件单位'},
                    {field:'partsType',title:'配件类型'},                                
                    {
                        field:'nsp_name', title:'配件/材料/工时',width:130,
                        editor:{
                            type:'combobox',
                            options:{
                                valueField:'name',
                                textField:'name',
                                method:'get',
                                url: '/develop/url/getUrl.do?name='+'getManhourForFan',
                                onSelect: function (row) {
                                    console.info(JSON.stringify(row));
                                    var index = Easyui.Datagraid.getSelectRowIndex('parts');
                                    var r = Easyui.Datagraid.getSelectRow('parts');
                                    r.nsp_pd = row.id
                                    console.info('index = '+index);
                                    $('#parts').datagrid('updateRow',{
                                        index: index,
                                        row: r
                                    });
                                }
                            }
                        }
                    },
                    {field:'partsValue',title:'用量',
                        editor:{
                            type:'numberbox',width:80,
                            options:{
                                min:0,
                                precision:2
                            }
                    }},
                    {field:'nsp_pd',hidden:true}        
            ]]
        });
    }
    else if(row.partsType == '标准'){
        $('#parts').datagrid({
            columns:[[
                    {field:'id',title:'id',hidden:true},
                    {field:'partsName',title:'配件名称'},
                    {field:'partsUnit',title:'配件单位'},
                    {field:'partsType',title:'配件类型'},                                
                    {
                        field:'nsp_name', title:'配件/材料/工时',width:130,
                    },
                    {field:'partsValue',title:'用量',
                        editor:{
                            type:'numberbox',width:80,
                            options:{
                                min:0,
                                precision:2
                            }
                    }},
                    {field:'nsp_pd',hidden:true}                         
            ]]
        });
    }
}

function onClickRow(index){
    console.info('editIndex = '+editIndex +' index = '+index);
    if (editIndex != index){
        changeColumeCombobox();
        if (endEditing()){
            $('#parts').datagrid('selectRow', index)
                    .datagrid('beginEdit', index);
            editIndex = index;
            
        } else {
            $('#parts').datagrid('selectRow', editIndex);
        }
    }
}

var editIndex = undefined;
var flag = false;
function append(){
    if (endEditing()){
        $('#parts').datagrid('appendRow',{text:''});
        editIndex = $('#parts').datagrid('getRows').length-1;
        $('#parts').datagrid('selectRow', editIndex)
                .datagrid('beginEdit', editIndex);
    }
}


function endEditing(){
    if (editIndex == undefined){return true}
    if ($('#parts').datagrid('validateRow', editIndex)){
        // var ed = $('#parts').datagrid('getEditor', {index:editIndex,field:'productid'});
        // var productname = $(ed.target).combobox('getText');
        // $('#parts').datagrid('getRows')[editIndex]['productname'] = productname;
        $('#parts').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function removeit(){
    if (editIndex == undefined){return}
    $('#parts').datagrid('cancelEdit', editIndex)
            .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept(){
    if (endEditing()){
        $('#parts').datagrid('acceptChanges');
    }

    console.info( Easyui.Datagraid.getAllRows('parts'));
}
function reject(){
    $('#parts').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges(){
    var rows = $('#parts').datagrid('getChanges');
    alert(rows.length+' rows are changed!');
}

function functionAdd() {
	$('#parts').datagrid('appendRow', {})
}
function functionModify() {
    var row= $('#parts').datagrid('getSelected')
    if (row==null) {
        shuoheUtil.layerMsgCustom('必须选择一个风机配件');
        return false
    }
    var index = $('#parts').datagrid('getRowIndex', row)
	$('#parts').edatagrid('editRow', index)
}

// //配件保存
// function accept() {
//     $('#parts').edatagrid('acceptChanges');
// }
 //获取风机配件
function getParts() {
    var rows = $('#parts').datagrid('getRows');
    return rows;
}	
function save() {
	//风机信息
    var obj = new Object(); //
    obj.number = $('#number').textbox('getValue');  
    obj.name = $('#name').textbox('getValue');  
    obj.specificationsAndModels = $('#specificationsAndModels').numberbox('getValue');  
    // obj.pc_pid = $('#pc_pid').combobox('getValue');  
    obj.em_pid = shuoheUtil.getUrlHeader().uuid;
    //风机配件
    var fanObj = new Object(); 
    fanObj  =  getParts() ;
    $.ajax({
        url: "/draughtFan/save", 
        type: "POST",
        dataType: 'json',
        data: {
            'data': JSON.stringify(obj),
            'fanPartsList':JSON.stringify(fanObj),
        },
        error: function() //失败
        {
//             shuoheUtil.layerTopMaskOff();
//             shuoheUtil.layerTopMsgError('远程通信失败');
        },
        success: function(data) {  //成功
      	
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
            
            parent.$('#dg').datagrid('reload');
            shuoheUtil.layerMsgSaveOK('保存成功');
            parent.layer.close(index); //再执行关闭
            } else {
                shuoheUtil.layerTopMsgError(data.describe);
            }
        }
    });
}
</script>