<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>产品模板管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body class="easyui-layout" style="overflow: hidden;">
  <script type="text/javascript">
  </script>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:20%;">           
        <!-- treegrid表格 -->
        <table id ='orgnizationTg' class="easyui-treegrid" style="width:100%;height:100%" 
               data-options="
                idField:'id',
                treeField:'name',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tt'
			   ">
            <thead>
            <tr>
                <th data-options="field:'id',title:'UUID',hidden:true"></th>
                <th data-options="field:'name',title:'风机'" width='80%'></th>
                <th data-options="field:'type',title:'类型'" width='20%'></th>
            </tr>
            </thead>
        </table>
        <div id="tt" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMMAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funMMEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funMMDele()">删除</a>
            <!-- <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a> -->
          </div>        
    </div>

    <div data-options="region:'east',iconCls:'icon-reload',title:'',split:true" style="width:80%;height: 100%;" >
        <table id="dg" class='easyui-treegrid' style="width:100%;height:100%" title="" data-options="
                idField:'id',
                treeField:'name',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            >
            <thead>
              <tr>
                <th field="id"  hidden ="true">uuid</th>                
                <th field="name">名称</th>                
                <th field="type">类型</th>
                <th field="ato">装配模式</th>
                <th field="unit">单位</th>
                <th field="is_filter">筛选条件</th>
                <!-- <th field="encoded">编号</th>
                <th field="specificationsAndModels">规格</th>
                <th field="dosage">用量</th>
                <th field="price">价格</th> -->
              </tr>
            </thead>
          </table>    
          <div id="tb" style="height:35px">
            <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a>
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="funDgEdit()">修改</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="funDgDele()">删除</a>
            <a id='btnDgSync' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-calculator',plain:true" onclick="fanModelOffer()">模型报价</a>
          </div>
          <div id="mm" class="easyui-menu" style="width:80px;">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="append()" data-options="iconCls:'fa fa-plus',plain:true">添加</a>
<!-- 				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="removeIt()" data-options="iconCls:'fa fa-trash'">移除</a> -->
		 </div>
       </div>
    </div>

  <script type="text/javascript">
    var typeId;

    var data = 
    [
        {
        "id":100,
        "name":"船用风机",
        "type":"大类",
        "size":"",
        "date":"02/19/2010"
        },
        {
        "id":1,
        "name":"轴流风机",
        "type":"大类",
        "size":"",
        "date":"02/19/2010",
        "children":[{
                "id":211,
                "name":"CZ系列",
                "type":"产品线",
                "size":"142 KB",
                "date":"01/13/2010"
            },{
                "id":212,
                "name":"CBZ系列",
                "type":"产品线",
                "size":"5 KB",
                "date":"01/13/2010"
            }]
    }]


 	//表格初始化 
    $(function(){
        $('#orgnizationTg').treegrid('loadData',data);

         $('#dg').treegrid('loadData',dgdata);
    }); 
     
    function funMMAdd() {
        Easyui.Layer.openLayerWindow(
            '新增产品类型',
            'type/addType.jsp',
            ['650px','400px']);
    }   
    function funMgAdd()
    {
        Easyui.Layer.openLayerWindow(
            '新增产品内容类型',
            'formerHeader.jsp',
            ['700px','500px']);  
    }

    



var dgdata = 
[
    {
	"id":1,
    "name":"叶轮金",
    "type":"半成品",
    "ato":"必选",
	"size":"",
    "date":"02/19/2010",
	"children":[{
            "id":11,
            "name":"叶片数",
            "type":"参数",
            "value":0,
            "unit":'个',
            "is_filter":"是",
            "size":"142 KB",
            "date":"01/13/2010"
        },{
            "id":12,
            "name":"单片金额",
            "type":"标准品",
            "value":0,
            "unit":'元',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":13,
            "name":"轮毂金额",
            "type":"标准品",
            "value":0,
            "unit":'元',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":14,
            "name":"叶片标件金额",
            "type":"标准品",
            "value":0,
            "unit":'元',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":15,
            "name":"叶轮工时",
            "type":"工时",
            "value":0,
            "unit":'个',
            "size":"5 KB",
            "date":"01/13/2010"
        }]    
    },
    {
    "id":2,
    "name":"风筒金",
    "type":"半成品",
    "ato":"单选",
	"size":"",
    "date":"02/19/2010",
    "children":[
        {
        "id":21,
        "name":"常规风筒",
        "type":"半成品",
        "size":"",
        "date":"02/19/2010",
        "children":[
            {
                "id":211,
                "name":"内径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"142 KB",
                "date":"01/13/2010"
            },{
                "id":212,
                "name":"外径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":213,
                "name":"风筒板厚",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":214,
                "name":"风筒高度",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":215,
                "name":"风筒材料",
                "type":"材料",
                "value":0,
                "unit":'kg/元',
                "size":"5 KB",
                "date":"01/13/2010"
            }]   
        },
        {
        "id":22,
        "name":"B3安装",
        "type":"半成品",
        "size":"",
        "date":"02/19/2010",
        "children":[
            {
                "id":221,
                "name":"内径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"142 KB",
                "date":"01/13/2010"
            },{
                "id":222,
                "name":"外径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":223,
                "name":"风筒板厚",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":224,
                "name":"风筒高度",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":225,
                "name":" B3安装板重量",
                "type":"标准品",
                "value":0,
                "unit":'千克',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":226,
                "name":"风筒材料",
                "type":"材料",
                "value":0,
                "unit":'kg/元',
                "size":"5 KB",
                "date":"01/13/2010"
            }]   
        },
        {
        "id":23,
        "name":"导叶安装",
        "type":"半成品",
        "size":"",
        "date":"02/19/2010",
        "children":[
            {
                "id":231,
                "name":"内径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"142 KB",
                "date":"01/13/2010"
            },{
                "id":232,
                "name":"外径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":233,
                "name":"风筒板厚",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":234,
                "name":"风筒高度",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":235,
                "name":"导叶风筒重量",
                "type":"标准品",
                "value":0,
                "unit":'千克',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":236,
                "name":"风筒材料",
                "type":"材料",
                "value":0,
                "unit":'kg/元',
                "size":"5 KB",
                "date":"01/13/2010"
            }]   
        },
        {
        "id":24,
        "name":"开箱安装",
        "type":"半成品",
        "size":"",
        "date":"02/19/2010",
        "children":[
            {
                "id":241,
                "name":"内径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"142 KB",
                "date":"01/13/2010"
            },{
                "id":242,
                "name":"外径",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":243,
                "name":"风筒板厚",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":244,
                "name":"风筒高度",
                "type":"参数",
                "value":0,
                "unit":'厘米',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":245,
                "name":"开箱风筒重量",
                "type":"标准品",
                "value":0,
                "unit":'千克',
                "size":"5 KB",
                "date":"01/13/2010"
            },{
                "id":246,
                "name":"风筒材料",
                "type":"材料",
                "value":0,
                "unit":'kg/元',
                "size":"5 KB",
                "date":"01/13/2010"
            }]   
        }  
    ]   
    },        
    {
    "id":3,
    "name":"其他必选件金额",
    "type":"半成品",
    "ato":"必选",
	"size":"",
    "date":"02/19/2010",
    "children":[
        {
            "id":31,
            "name":"配对法兰",
            "type":"标准件",
            "value":0,
            "unit":'元',
            "size":"142 KB",
            "date":"01/13/2010"
        },{
            "id":32,
            "name":"支撑板",
            "type":"标准件",
            "value":0,
            "unit":'元',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":33,
            "name":"标准件",
            "type":"标准件",
            "value":0,
            "unit":'元',
            "size":"5 KB",
            "date":"01/13/2010"
        }]           
    },
    {
    "id":4,
    "name":"其它",
    "type":"选配件",
    "ato":"多选",
	"size":"",
	"date":"02/19/2010",
    "children":[
        {
            "id":41,
            "name":"填料函",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"142 KB",
            "date":"01/13/2010"
        },{
            "id":42,
            "name":"接线盒",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":43,
            "name":"包装核定",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":44,
            "name":"防护网",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":45,
            "name":"减震器",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":46,
            "name":"软连接",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        },{
            "id":47,
            "name":"橡胶垫",
            "type":"配套件",
            "value":0,
            "unit":'金额',
            "size":"5 KB",
            "date":"01/13/2010"
        }]   
    } 
]



































 	function loadFanModel(id){
 		/*
 		$.ajax({
            url: "/develop/url/getUrl.do",
            type: "POST",
            dataType: 'json',
            data: {
            	'name':'getFanModelByTypeId',
            	'fanTypeId': id 
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMaskOff();
            },
            success: function(data) {
            	var arr = data.rows;
            	for(var i=0;i<arr.length;i++){
            		if(arr[i]._parentId==""){
            			delete arr[i]._parentId;
            		}
            	}
            	data.rows = arr;
            	$('#dg').treegrid('loadData', data);
            }
        });*/
 		
 		$('#dg').treegrid({
    		url:'/develop/url/getUrl.do',
    		//data: data,
    		method:'get',
    		queryParams: {
    			'name':'getFanModelByTypeId',
    			'fanTypeId': id      
              }
    	})
 	}
 	function onContextMenu(e,row){
		e.preventDefault();
		$(this).treegrid('select', row.id);
		$('#mm').menu('show',{
			left: e.pageX,
			top: e.pageY
		});
	}
 	
 	function append(){
 		var rootNode = $('#dg').treegrid('getRoot');
 		var node = $('#dg').treegrid('getSelected');
 		var pid = node.id;
 		Easyui.Layer.openLayerWindow(
 			      '新建产品模型',
 			      '/pages/shop/product/model/addProductModel.jsp?id='+typeId+'&pid='+pid+'&fanModelId='+rootNode.id,
 			      ['650px','400px']);
 	}
 	function removeIt(){
 		alert("移除");
 	}
	</script>
</body> 
</html>
<script type="text/javascript">


/*大类*/
function funTgAdd() {
	
	var nodes = $('#tt').tree('getSelected');
      
      var pid = '0';
    if(nodes != null){
    	Easyui.Layer.openLayerWindow(   
    		      '新建产品大类',
    		      '/pages/shop/product/type/addProductType.jsp?id='+$('#tt').tree('getSelected').id,
    		      ['655px','400px']);
    }else{
    	Easyui.Layer.openLayerWindow(   
  		      '新建产品大类',
  		      '/pages/shop/product/type/addProductType.jsp?',
  		      ['655px','400px']);
    } 
}
function funTgEdit() {

    if($('#tt').tree('getSelected') == null)
    {
        shuoheUtil.layerMsgCustom('必须选择一个产品大类');
    }
    console.log($('#tt').tree('getSelected').id)
  Easyui.Layer.openLayerWindow(
      '编辑产品大类',
      '/pages/shop/product/type/editProductType.jsp?id='+$('#tt').tree('getSelected').id,
       ['400px','280px']);
}
function funTgDelete() {
  // body...
    var row = $('#tt').tree('getSelected');
    if(row == null){
        shuoheUtil.layerMsgCustom('必须选择一个模型');
        return;
    }
    Easyui.Ajax.post(
        '/productCategory/delete?id='+row.id,
        shuoheUtil.layerMsgError('删除失败'),
        
        shuoheUtil.layerMsgSaveOK('删除成功'),
        
    );
    $('#tt').tree('reload');
}


/*模型*/
// function funMgAdd(){
// 	var nodess = $('#tt').tree('getSelected');
//     if(nodess == null)
//     {
//         shuoheUtil.layerMsgCustom('必须选择一个产品大类');
//     }
//     var rootNode = $('#dg').treegrid('getRoot');
//     if(rootNode){
//     	shuoheUtil.layerMsgCustom('风机模型已经存在，请添加配件');
//     }else{
//     	Easyui.Layer.openLayerWindow(
//     		      '新建产品模型',
//     		      '/pages/shop/product/model/addProductModel.jsp?id='+typeId,
//     		      ['650px','400px']);
//     }
    
// }
function funMgEdit() {
	if(Easyui.Datagraid.getSelectRow('dg') == null){
	        shuoheUtil.layerMsgCustom('必须选择一个产品模型');
    }
	 console.info($('#mg').datagrid('getSelected').id);
	Easyui.Layer.openLayerWindow(
       '修改产品模型',
       '/pages/shop/product/model/editProductModel.jsp?uuid='+$('#mg').datagrid('getSelected').id,
       ['400px','280px']);
}
function funMgDelete() {
  // body...
    var row = $('#tt').tree('getSelected');
    if(row == null){
        shuoheUtil.layerMsgCustom('必须选择一个模型');
        return;
    }
    Easyui.Ajax.post(
        '/extensionModel/delete?id='+row.id,
        shuoheUtil.layerMsgError('删除失败'),
        shuoheUtil.layerMsgSaveOK('删除成功')
    );
    $('#mg').datagrid('reload');
} 




/*配件*/
function funDgAdd(){

    if(Easyui.Datagraid.getSelectRow('dg') == null)
    {
        shuoheUtil.layerMsgCustom('必须选择一个产品模型');
    }
    Easyui.Layer.openLayerWindow(
      '新建模型配件',
      '/pages/shop/product/parts/addModelParts.jsp?uuid='+$('#mg').datagrid('getSelected').id,
      Easyui.Layer.sizeTwoRow(600));
}
function funDgEdit() {
	if(Easyui.Datagraid.getSelectRow('dg') == null){
	        shuoheUtil.layerMsgCustom('必须选择一个模型配件');
    }
	 console.info("555");
	Easyui.Layer.openLayerWindow(
       '修改模型配件',
       '/pages/shop/product/parts/editModelParts.jsp?uuid='+$('#dg').datagrid('getSelected').id,
       Easyui.Layer.sizeTwoRow(600));
}
function funDgDele() {
  // body...
    var row = Easyui.Datagraid.getSelectRow('dg');
    if(row == null){
        shuoheUtil.layerMsgCustom('必须选择一个材料');
        return;
    }
    Easyui.Ajax.post(
        '/fanModelParts/delete?id='+row.id,
        shuoheUtil.layerMsgError('删除失败'),
        shuoheUtil.layerMsgSaveOK('删除成功')
    );
    $('#dg').datagrid('reload');
}
function fanModelOffer() {
	var node = $('#dg').treegrid('getSelected');
	console.log(node);
	if(node){
		$.ajax({
            url: "/productMix/fanModelOffer",
            type: "POST",
            dataType: 'json',
            data: {
            	'modelNumber': node.id
 
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMaskOff();
            },
            success: function(data) {
            	$('#dg').treegrid('reload');
            }
        });
	}else{
		shuoheUtil.layerMsgCustom('请选择要报价的模型');
	}
	
}


  
</script>