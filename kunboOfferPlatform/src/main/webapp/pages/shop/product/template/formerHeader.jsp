<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建半成品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">

        <div data-options="region:'north',title:'',split:false,border:true" 
            style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >
            <table class="editTable" style="text-align: center; margin-top: 0px;margin-left:0px;" >
                <tr>
                    <td class="label">选择内容类型</td>
                    <td >
                        <select id='pc_pid' class="easyui-combobox" data-options="required:false,width:150,editable:false">
                            <option>参数</option>  
                            <option>半成品</option>         
                            <option>原材料</option>       
                            <option>标准品</option>  
                            <option>工时</option>
                            <option>配套件</option>   
                        </select>
                    </td>
                </tr>	      
            </table>
        </div>   
  
        <div data-options="region:'center',title:'',border:true,split:false,overflow:false" 
            style="padding:0px;height: 400px;width: 100%;border-width:0px">
            <iframe id='son_page' src="halfProduct/addHalfProduct.jsp" style="width: 100%;height:400px;overflow:hidden;border-width:0px"></iframe>
        </div>   
        <!-- <div data-options="region:'south',title:'',split:false,border:true" 
            style="height:100px;width: 100%;height: 45px;border-width:0px">
            <div style="text-align: right;margin-top: 5px;margin-right: 10px">
                <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
            </div>                              
        </div>          -->
</body> 
</html>
<script type="text/javascript">
    $(function(){
        
        $('#pc_pid').combobox({                
            valueField:'id',    
            textField:'text',
            onSelect: function(rec){    
                console.info(rec);
                if(rec.id == '参数')
                    $('#son_page').attr('src',"params/addParams.jsp");
                else if(rec.id == '半成品')
                    $('#son_page').attr('src',"halfProduct/addHalfProduct.jsp");
                else if(rec.id == '标准品')
                    $('#son_page').attr('src',"standard/addStandard.jsp");
                else if(rec.id == '工时')
                    $('#son_page').attr('src',"manhour/addManhour.jsp");
                else if(rec.id == '配套件')
                    $('#son_page').attr('src',"assortPort/addAssortPort.jsp");
                else if(rec.id == '原材料')
                    $('#son_page').attr('src',"materials/addMaterials.jsp");                    
            }
        }); 
    });
</script>