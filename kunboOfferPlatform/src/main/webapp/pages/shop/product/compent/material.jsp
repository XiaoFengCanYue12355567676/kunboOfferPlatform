<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建产品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0;width: 100%;}
</style>


<body>
    <table id="dg" class='easyui-datagrid' style="width:100%;height:450px" title="" data-options="
            rownumbers:true,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            fitColumns:false,
            striped:true,
            checkOnSelect:true,
            selectOnCheck:true,
            collapsible:true,
            onClickRow: onClickRow,
            pageSize:20">
        <thead>
          <tr href="#">                     
            <th field="text">替换部件</th>
            <th field="unit" width='50'>单位</th>
            <th field="param" data-options="editor:'numberbox'" width='130'>参数</th>
            <th field="uuid" hidden=true>uuid</th>                        
            <th field="product_id" hidden=true>product_id</th>
            <th field="sort_param_uuid" hidden=true>sort_param_uuid</th>
          </tr>
        </thead>
    </table>    
    <div style="text-align: right;margin-top: 5px">
        <a onclick='forword()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-left"></i>上一步</a>
        <a onclick='next()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-right"></i>下一步</a>
    </div>               
</body> 
</html>
<script type="text/javascript">

    var ProductSortParam = new Object();

    $(function(){

        // 若从未编辑过产品物料表，则进行初始化加载
        if(Easyui.Object.isEmptyObject(parent.ProductMaterials)){
            console.info('setValue 1 = '+JSON.stringify(parent.ProductSortParam));
            ProductSortParam = parent.ProductSortParam;
            for(var i=0;i<parent.ProductSortParam.length;i++)
            {
                parent.ProductSortParam[i].sort_param_uuid = parent.ProductSortParam[i].uuid;
                parent.ProductSortParam[i].uuid = '';
            }
            Easyui.Datagraid.loadData('dg',parent.ProductSortParam.length,parent.ProductSortParam);
        }
        else{
            Easyui.Datagraid.loadData('dg',parent.ProductMaterials.length,parent.ProductMaterials);
        }
    })

</script>
<script>
    // 改变主模态框的数据
    function changeParentData()
    {
        parent.ProductMaterials = Easyui.Datagraid.getAllRows('dg');
    }
    function forword()
    {
        accept();
        changeParentData();
        parent.s2();
    }
    function next()
    {
        accept();
        changeParentData();
        parent.s4();
    }
</script>
<script>
    var editIndex = undefined;
    function endEditing(){
        if (editIndex == undefined){return true}
        if ($('#dg').datagrid('validateRow', editIndex)){
            // var ed = $('#dg').datagrid('getEditor', {index:editIndex,field:'productid'});
            // var productname = $(ed.target).combobox('getText');
            // $('#dg').datagrid('getRows')[editIndex]['productname'] = productname;
            $('#dg').datagrid('endEdit', editIndex);
            editIndex = undefined;
            return true;
        } else {
            return false;
        }
    }
    function onClickRow(index){
        console.info('editIndex = '+editIndex +' index = '+index);
        if (editIndex != index){
            if (endEditing()){
                $('#dg').datagrid('selectRow', index)
                        .datagrid('beginEdit', index);
                editIndex = index;
            } else {
                $('#dg').datagrid('selectRow', editIndex);
            }
        }
    }
    function append(){
        if (endEditing()){
            $('#dg').datagrid('appendRow',{text:''});
            editIndex = $('#dg').datagrid('getRows').length-1;
            $('#dg').datagrid('selectRow', editIndex)
                    .datagrid('beginEdit', editIndex);
        }
    }
    function removeit(){
        if (editIndex == undefined){return}
        $('#dg').datagrid('cancelEdit', editIndex)
                .datagrid('deleteRow', editIndex);
        editIndex = undefined;
    }
    function accept(){
        if (endEditing()){
            $('#dg').datagrid('acceptChanges');
        }
        changeParentData();
    }
    function reject(){
        $('#dg').datagrid('rejectChanges');
        editIndex = undefined;
    }
    function getChanges(){
        var rows = $('#dg').datagrid('getChanges');
        alert(rows.length+' rows are changed!');
    }

</script>