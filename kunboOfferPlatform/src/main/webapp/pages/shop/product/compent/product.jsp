<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建产品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0;width: 100%;height: 100%;overflow: hidden;}
</style>


<body>
    <div style="width: 100%;height: 440px">
        <table class="editTable" style="text-align: center;width: 100%;">
             <tr>
                <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                    <div class="divider">
                        <span style="margin-left: 50px">产品信息</span>
                    </div>
                </td>
            </tr>               
            <tr>
                <td class="label">产品大类</td>
                <td >
                    <input id='cdepCode' class="easyui-textbox" data-options="required:false,width:200,editable:true" disabled>
                </td>
                <td class="label"></td>
                <td >
                    <!-- <input id='cdepName' class="easyui-textbox" data-options="required:false,width:200,editable:true" disabled>                -->
                </td>
            </tr>
            <tr>
                <td class="label">产品编码</td>
                <td >
                    <input id='code' class="easyui-textbox" data-options="required:false,width:200,editable:true">
                </td>
                <td class="label">产品型号</td>
                <td >
                    <input id='model' class="easyui-textbox" data-options="required:false,width:200,editable:true">               
                </td>
            </tr>        
            <tr>
                <td class="label">产品名称</td>
                <td >
                    <input id='name' class="easyui-textbox" data-options="required:false,width:200,editable:true">
                </td>
            </tr>                        
        </table>
    </div>

	<div style="text-align: right;margin-top: 5px">
        <a onclick='next()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-right"></i>下一步</a>
    </div>   

    <script type="text/javascript">
        var product = new Object();
        var ProductSort = new Object();

        $(function(){

            $('#cdepCode').textbox('setValue',parent.ProductSort.text);

            // 若从未编辑过产品，则进行初始化加载
            console.info('setValue 1 = '+JSON.stringify(parent.ProductSort));

            $('#code').textbox('setValue',parent.Product.code);
            $('#name').textbox('setValue',parent.Product.name);
            $('#model').textbox('setValue',parent.Product.model);

        })

        function next()
        {
            parent.Product.name = $('#name').textbox('getValue');
            parent.Product.product_sort_id = parent.ProductSort.uuid;
            parent.Product.code = $('#code').textbox('getValue');
            parent.Product.model = $('#model').textbox('getValue');
            console.info(JSON.stringify(parent.Product));

            parent.s2();
        }

    </script>    
</body> 
</html>
