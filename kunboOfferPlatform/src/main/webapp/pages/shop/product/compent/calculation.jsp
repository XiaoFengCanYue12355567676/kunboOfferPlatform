<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>价格计算公式</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/import/base.js"></script>
    <script type="text/javascript" src="/pages/import/codemirror.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
    <style type="text/css">
        @import url("/pages/import/base.css");
        @import url("/pages/import/codemirror.css");
        html, body{ margin:0; height:100%; }
    </style>

<style type="text/css">
    html, body{ margin:0;width: 100%;}
</style>

<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: auto;}
    .CodeMirror {
      border: 0px solid #eee;
      height: 100%;
    }
    #footer{  
        position: absolute;  top: 400px; /* 关键 */  
        right: 15px; /* IE下一定要记得 */  
        height: 35px;         /* footer的高度一定要是固定值*/  
        border:1px solid #bbe1f1;background:#eefaff
    }     
</style>

<body>
    <div class="easyui-panel" style="width:100%;height: 450px; margin-top: 0px" data-options=" border:false">
        <!-- <input type="text" id="jsstrengthen" class="easyui-textbox" data-options="width:554,height:300,multiline:true"> -->
        <textarea id="jsstrengthen" style="width: 100%;height: 100%"></textarea>
    </div>
    <div style="text-align: right;margin-top: 5px">
        <a onclick='initCode()' class="button button-primary button-rounded button-small" style="text-align:left"><i class=""></i>初始化代码</a>
        <a onclick='forword()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-left"></i>上一步</a>
        <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
    </div>
</body> 
</html>
<script type="text/javascript">
    var editor;
    $(function(){

        initJsCodeText();
        //加载商品计算公式，如果商品还没有存在，则加载时写入默认的公式内容，若加载时公式已经存在，则加载公式
        if(Easyui.Object.isEmptyObject(parent.Product.price_formula)){
            console.log('价格计算公式为空');
            editor.setValue('\r\n\
                function priceCalculation()\r\n\
                {\r\n\
                    var price = 0;\r\n\
                    return price;\r\n\
                    \r\n}            \
            ');
        }
        else{
            console.log('价格计算公式不为空');
            editor.setValue(parent.Product.price_formula);
        }


    });

    function priceCalculation()
    {
        var price = 0;
        return price;
    }

    function initJsCodeText()
    {
        console.info('initJsCodeText');
        editor=CodeMirror.fromTextArea(document.getElementById("jsstrengthen"),{
            theme:'monokai', 
            styleActiveLine: true,
            mode:'javascript',
            extraKeys: {"Ctrl": "autocomplete"},//输入s然后ctrl就可以弹出选择项  
            lineNumbers: true,
            tabSize:10,
            // readOnly:"nocursor",
            smartIndent:true
            // keymap:"defaule"
        })
    }    

</script>
<script>
    function forword()
    {
        parent.Product.price_formula = editor.getValue();
        parent.s4();
    }
    function initCode()
    {
        editor.setValue('\
        \r\nfunction priceCalculation()\
        \r\n{\
        \r\n        var price = 0;\
        \r\n        return price;\
        \r\n}            \
        ');
    }
    // 向服务器提交产品、产品替换部件等数据
    function save()
    {
        $.ajax({
            url: "/product/product/save.do",
            type: "POST",
            dataType: 'json',
            data: {
                'Product': JSON.stringify(parent.Product),
                'ProductMaterials': JSON.stringify(parent.ProductMaterials)
            },
            error: function() //失败
            {
                // shuoheUtil.layerTopMaskOff();
                // shuoheUtil.layerTopMsgError('远程通信失败');
            },
            success: function(data) //成功
            {

            }
        });
    }
</script>