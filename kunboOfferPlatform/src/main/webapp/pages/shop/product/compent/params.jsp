<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新建产品</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0;width: 100%;}
</style>


<body>
    <table id="dg" class='easyui-datagrid' style="width:100%;height:450px" title="" data-options="
            rownumbers:true,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            fitColumns:false,
            striped:true,
            checkOnSelect:true,
            selectOnCheck:true,
            collapsible:true,
            toolbar:'#tb',
            pageSize:20">
        <thead>
          <tr href="#">
                     
            <th field="id"  hidden ="true">uuid</th>
            <th field="pid" >产品编码</th>
            <th field="name" >产品名称</th>
            <th field="descr" >规格型号</th>
          </tr>
        </thead>
    </table>    
    <div id="tb" style="height:35px">
        <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funDgAdd()">新增</a>
        <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="FunDgEdit()">修改</a>
        <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="FunDgDele()">删除</a>
    </div>
    <div style="text-align: right;margin-top: 5px">
        <a onclick='forword()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-left"></i>上一步</a>
        <a onclick='next()' class="button button-primary button-rounded button-small"><i class="fa fa-arrow-right"></i>下一步</a>
    </div>               
</body> 
</html>
<script type="text/javascript">
    function forword()
    {
        parent.s3();
    }
    function next()
    {
        parent.s5();
    }



</script>