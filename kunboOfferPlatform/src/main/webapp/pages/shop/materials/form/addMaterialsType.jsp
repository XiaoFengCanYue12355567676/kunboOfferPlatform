<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增原材料大类</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>


<body>
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">原材料大类</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">上级原材料大类</td>
            <td >
                <input id='cdepCode' class="easyui-textbox" data-options="required:false,width:200,editable:true" disabled>
            </td>
        </tr>
        <tr>
            <td class="label">原材料大类名称</td>
            <td >
                <input id='cdepLeader' class="easyui-textbox" data-options="required:false,width:200,editable:true">
            </td>
        </tr>        

        <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">操作</span>
                </div>
            </td>
            
        </tr>          
        <tr style="margin-top: 100px">
            <td class="label" colspan="4" style="margin-top: 100px">
                <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa fa-check"></i>保存</a>
            </td>            
        </tr>          
    </table>
	
</body> 
</html>
<script type="text/javascript">

// var user_name_init_load_flag = false;
// $(function() {

//     var row = parent.$('#dg').datagrid('getSelected');
//     if (row == null) {
//         layerMsgCustom('必须选择一条数据');
//         return;
//     }

//     $('#cdepCode').textbox('setValue',row.cdepCode);
//     $('#cdepName').textbox('setValue',row.cdepName);
//     $('#cdepLeader').textbox('setValue',row.cdepLeader);
//     $('#cdepFullName').textbox('setValue',row.cdepFullName);
//     $('#cDepPerson').textbox('setValue',row.cDepPerson);



//     $('#department_name').combobox({
//         url: '/develop/url/getUrl.do?name=getDepartments',
//         valueField: 'id',
//         textField: 'text',
//         panelHeight: 200,
//         method: 'get',
//         multiple: false,
//         mode: 'remote',
//         queryParams: {
//         },
//         onSelect:function(record) {
//             console.info(record);
//             $('#department_id').textbox('setValue',record.id);
//         },
//         onLoadSuccess:function() {
//             console.info('onLoadSuccess');
//             if(!user_name_init_load_flag)
//             {
//                 $('#department_name').combobox('setValue',row.department_id);
//                 user_name_init_load_flag = true;
//             }            
//         }
//     });

// });
// function save() {
//     var obj = new Object();
//     console.info('save');

//     obj.cDepCode = $('#cdepCode').textbox('getValue'); //绑定系统用户名称
//     obj.cDepName = $('#cdepName').textbox('getValue'); //操作员姓名 
//     obj.cDepLeader = $('#cdepLeader').textbox('getValue'); //操作员编码 
//     obj.cDepFullName = $('#cdepFullName').textbox('getValue'); //Email地址 
//     obj.cDepPerson = $('#cDepPerson').textbox('getValue');
//     obj.department_name = $('#department_name').combobox('getText');
//     obj.department_id = $('#department_name').combobox('getValue'); //crm系统对应用户id

//     var obj_str = JSON.stringify(obj);
//     $.ajax({
//         url: "/plugin/yongyou/department/save.do",
//         type: "POST",
//         dataType: 'json',
//         data: {
//             'data': obj_str
//         },
//         error: function() //失败
//         {
//             shuoheUtil.layerTopMaskOff();
//             shuoheUtil.layerTopMsgError('远程通信失败');
//         },
//         success: function(data) //成功
//         {
//             shuoheUtil.layerTopMaskOff();
//             if (data.result == true) {
//                 var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
//                 parent.layer.close(index); //再执行关闭
//                 parent.parent.$('#dg').datagrid('reload');
//                 shuoheUtil.layerTopMsgOK('保存成功');
//             } else {
//                 shuoheUtil.layerTopMsgError(data.describe);
//             }
//         }
//     });
// }
</script>