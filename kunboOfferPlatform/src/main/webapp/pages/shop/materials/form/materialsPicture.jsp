<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title></title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/pages/js/queryBox.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <script type="text/javascript" src="/pages/js/jquery.fix.clone.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>

    

    
    
<style type="text/css">
    html, body{ margin:0;width: 100%}

</style>

<style>    
    .pic_box{ width:900px;}    
    .pic_box li{ width:204px; height:154px;list-style-type:none; float:left; margin:16px; border:2px solid; border-color: #FFFFFF; position:relative}    
    .dele_pic{top:0px; position:absolute; left:167px; width:30px; height:30px; display:none;}    
</style> 

</head> 
<body style="overflow: hidden;">
        <table class="editTable" id="dggggg" border="0" cellspacing="0"
            cellpadding="0" style=" margin-bottom: 0px;" >
            <tr>
                <td>
                    <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal'
                        plain="true" iconCls="fa fa-plus" style="margin-top: 2px;margin-left: 15px"
                        onclick="functionAdd()">新增</a>
                    <a href="#"
                        class="easyui-linkbutton" btnCls='topjui-btn-normal'
                        plain="true" iconCls="fa fa-trash" style="margin-top: 2px"
                        onclick="functionDelete()">删除</a>   
                </td>          
            </tr>            
            <tr>
                <td>
                    <input id='ck1' type="checkbox" style="margin-top: 0" /> 
                    <img id='img1'  onclick="onclick1()" style='width: 140px;height: 120px;cursor: hand'/>                    
                </td>
                <td>
                    <input id='ck2' type="checkbox" style="margin-top: 0" /> 
                    <img id='img2' onclick="onclick2()" style='width: 140px;height: 120px;cursor: hand'/>
                </td>
                <td>
                    <input id='ck3' type="checkbox" style="margin-top: 0" /> 
                    <img id='img3'  onclick="onclick3()" style='width: 140px;height: 120px;cursor: hand'/>         
                </td>
                <td>
                    <input id='ck4' type="checkbox" style="margin-top: 0" /> 
                    <img id='img4'  onclick="onclick4()" style='width: 140px;height: 120px;cursor: hand'/>
                </td>                
            </tr>
            <tr>
                <td>
                    <input id='ck5' type="checkbox" style="margin-top: 0" /> 
                    <img id='img5'  onclick="onclick5()" style='width: 140px;height: 120px;cursor: hand'/>
                </td>
                <td>
                    <input id='ck6' type="checkbox" style="margin-top: 0" /> 
                    <img id='img6'  onclick="onclick6()" style='width: 140px;height: 120px;cursor: hand'/>
                </td>
                <td>
                    <input id='ck7' type="checkbox" style="margin-top: 0" /> 
                    <img id='img7'  onclick="onclick7()" style='width: 140px;height: 120px;cursor: hand'/>         
                </td>
                <td>
                    <input id='ck8' type="checkbox" style="margin-top: 0" /> 
                    <img id='img8'  onclick="onclick8()" style='width: 140px;height: 120px;cursor: hand'/>
                </td>                
            </tr>            
        </td>
</body> 
</html>
<script type="text/javascript">
	var piclist;
    var Request=new UrlSearch(); //实例化
    console.info('Request = '+JSON.stringify(Request));
    console.info('Request.order_id = '+Request.order_id);
    var picllll = new Array();
    $(function(){
        loadAllPicture();
    });

    function clearPictures()
    {
        for(var i=0;i<8;i++)
        {
            $("#img"+i).attr('src',+'&seed='+Math.random()); 
        }
    }
    function loadAllPicture()
    {
        $.ajax({
          url: "/maintenance/maintenanceOrder/getMtPictureByAutoid.do",
            type : "POST",
            dataType : 'json',
            data : {
                auto_id:Request.order_id
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                var piclist = jQuery.parseJSON(JSON.stringify(data));
                picllll = piclist;
                console.info("data = " + JSON.stringify(data));
                for(var i=0;i<piclist.length;i++)
                {
                    var src = '/pages/interface/maintenance/mtPicture/picCreate.jsp?ppath='+piclist[i].file_path+'&seed='+Math.random();
                    console.info(src);
                    var x = i+1;
                    $("#img"+x).attr('src',src); 
                }
            }
        });
    }
    function functionDelete()
    {
        var array = new Array();
        for(var i=0;i<8;i++)
        {
            var x = i+1;
            if(($("#ck"+x).is(':checked')==true)&&!checkNull(picllll[i]))
            {
                var o = new Object();
                o.id = picllll[i].id;
                array.push(o);
            }
        }
        $.ajax({
          url: "/maintenance/maintenanceOrder/deleteMtPicture.do",
            type : "POST",
            dataType : 'json',
            data : {
                data:JSON.stringify(array)
            },
            error : function() //失败
            {

            },
            success : function(data)//成功
            {
                var result = jQuery.parseJSON(JSON.stringify(data));
                console.info("data = " + JSON.stringify(data));
                if (result.result == true) {
                    clearPictures();
                    loadAllPicture();
                } else {

                }

            }
        });        
        console.info(JSON.stringify(array));
    }

    function functionAdd()
    {
        if(picllll.length >= 8)
        {
            layerMsgCustom('保存的图片数量不能超过8个');
            return;
        }

        top.layer.open({
            type: 2,
            title: '上传维修图片',
            shadeClose: true,
            shade: 0.5,
            maxmin: true, //开启最大化最小化按钮
            area: ['630px', '400px'],
            content: 'uploadMtPicture.jsp?order_id='+Request.order_id,
            cancel: function(index, layero)
            { 
                loadAllPicture();
            }    
            // content: '/pages/module/Maintenance/mtPicture/uploadMtPicture.jsp?order_id='+Request.auto_id
        })
    }
    function onclick1()
    {
        console.info('onclick1');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick2()
    {
        console.info('onclick2');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick3()
    {
        console.info('onclick3');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick4()
    {
        console.info('onclick4');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick5()
    {
        console.info('onclick5');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick6()
    {
        console.info('onclick6');
        layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
    function onclick7()
    {
        console.info('onclick7');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }    
    function onclick8()
    {
        console.info('onclick8');
        top.layer.photos({
            photos: '#dggggg'
            ,shade: 0.5
            ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
    }
 
   
</script>
