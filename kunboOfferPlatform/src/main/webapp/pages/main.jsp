<%@page import="com.shuohe.entity.system.user.User"%>
<%@page import="com.shuohe.util.file.QFile"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String contentPath = "'"+basePath+"'";
    PrintWriter ss = response.getWriter();

	String sm = QFile.read(System.getProperty("user.dir")+"/config/ui/index/Workbench.json","UTF-8");
	System.out.println("System.getProperty = "+System.getProperty("user.dir"));

    User user = null;
    String userJson = null;

    try
    {
    		user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userJson = Json.toJsonByPretty(user);
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }

%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="keywords" content='easyui,jui,jquery easyui,easyui demo,easyui中文'/>
   
    <title>软件项目管理系统</title>

    <link rel="shortcut icon" href="<%=basePath%>/TopJUI/topjui/image/favicon.ico"/>
    <!-- TopJUI框架样式 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.blue.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- <script type="text/javascript">topJUI.config.mainPage = true;</script> -->
    <!-- TopJUI框架核心 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
    <!-- 首页js -->
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>

    <script type="text/javascript">
        var basePath = <%=contentPath%>;
    </script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.index.js" charset="utf-8"></script>
    <!-- 报错信息监控/生产环境可去掉 -->


</head>

<body>
<div id="loading" class="loading-wrap">
    <div class="loading-content">
        <div class="loading-round"></div>
        <div class="loading-dot"></div>
    </div>
</div>

<div id="mm" class="submenubutton" style="width: 140px;">
    <div id="mm-tabclose" name="6" iconCls="fa fa-refresh">刷新</div>
    <div class="menu-sep"></div>
    <div id="Div1" name="1" iconCls="fa fa-close">关闭</div>
    <div id="mm-tabcloseother" name="3">关闭其他</div>
    <div id="mm-tabcloseall" name="2">关闭全部</div>
    <div class="menu-sep"></div>
    <div id="mm-tabcloseright" name="4">关闭右侧标签</div>
    <div id="mm-tabcloseleft" name="5">关闭左侧标签</div>
</div>

<style type="text/css">
    /* right */
    .top_right {
        /*width: 748px;*/
    }

    /* top_link */
    .top_link {
        padding-top: 24px;
        height: 26px;
        line-height: 26px;
        padding-right: 35px;
        text-align: right;
    }

    .top_link i {
        color: #686868;
    }

    .top_link span, .top_link a {
        color: #46AAFE;
    }

    .top_link a {
        font-size: 13px;
    }

    .top_link a:hover {
        text-decoration: underline;
    }

    .nav_bar {
        position: relative;
        z-index: 999;
        color: #333;
        margin-right: 10px;
        height: 50px;
        line-height: 50px;
    }

    .nav_bar ul {
        padding: 0;
    }

    .nav {
        position: relative;
        margin: 0 auto;
        font-family: "Microsoft YaHei", SimSun, SimHei;
        font-size: 14px;
    }

    .nav a {
        color: #333;
    }

    .nav h3 {
        font-size: 100%;
        font-weight: normal;
        height: 50px;
        line-height: 50px;
    }

    .nav h3 a {
        display: block;
        padding: 0 20px;
        text-align: center;
        font-size: 14px;
        color: #fff;
        height: 50px;
        line-height: 50px;
    }

    .nav .m {
        float: left;
        position: relative;
        z-index: 1;
        height: 50px;
        line-height: 50px;
        list-style: none;
    }

    .nav .s {
        float: left;
        width: 3px;
        text-align: center;
        color: #D4D4D4;
        font-size: 12px;
        height: 50px;
        line-height: 50px;
        list-style: none;
    }

    .nav .sub, ul.sub {
        display: none;
        position: absolute;
        left: -3px;
        top: 42px;
        z-index: 999;
        width: 128px;
        border: 1px solid #E6E4E3;
        border-top: 0;
        background: #fff;
    }

    .nav .sub li {
        text-align: center;
        padding: 0 8px;
        margin-bottom: -1px;
        list-style: none;
    }

    .nav .sub li a {
        display: block;
        border-bottom: 1px solid #E6E4E3;
        padding: 8px 0;
        height: 28px;
        line-height: 28px;
        color: #666;
    }

    .nav .sub li a:hover {
        color: #1E95FB;
        cursor: pointer;
    }

    .nav .block {
        height: 3px;
        background: #1E95FB;
        position: absolute;
        left: 0;
        top: 47px;
        overflow: hidden;
    }

    .sub {
        padding: 0;
        background: #f5f5f5;
    }

    .sub li {
        padding: 0 8px;
        list-style: none;
    }

    .sub li:hover {
        background: #f3f3f3;
    }

    .sub li a {
        display: block;
        color: #000;
        height: 34px;
        line-height: 34px;
    }

    .sub li a:hover {
        text-decoration-line: none;
    }

    /* 重用类样式 */
    .f_l {
        float: left !important;
    }

    .f_r {
        float: right !important;
    }

    .no_margin {
        margin: 0px !important;
    }

    .no_border {
        border: 0px !important;
    }

    .no_bg {
        background: none !important;
    }

    .clear_both {
        clear: both !important;
    }

    .display_block {
        display: block !important;
    }

    .text_over {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        -o-text-overflow: ellipsis;
        -moz-binding: url('ellipsis.xml#ellipsis');
    }

    /* 重用自定义样式 */
    .w_100 {
        width: 100%;
    }

    .w_95 {
        width: 95%;
    }

    .indextx {
        width: 980px;
        margin: 0 auto;
        margin-top: 10px;
        background: #FFFFFF;
    }

    .w_min_width {
        min-width: 1200px;
    }

    .w_1200 {
        width: 1200px;
    }

    .w_1067 {
        width: 1067px;
    }

    .w_980 {
        width: 980px;
    }

    .header {
        overflow: hidden
    }
    .systemIcon{
      	vertical-align: middle;
    	margin-right: 3px;
    }
</style>
<script>
    $(function () {
        $('#ulMenu>li').hover(
                function () {
                    var m = $(this).data('menu');
                    if (!m) {
                        m = $(this).find('ul').clone();
                        m.appendTo(document.body);
                        $(this).data('menu', m);
                        var of = $(this).offset();
                        m.css({left: of.left, top: of.top + this.offsetHeight});
                        m.hover(function () {
                            clearTimeout(m.timer);
                        }, function () {
                            m.hide()
                        });
                    }
                    m.show();
                }, function () {
                    var m = $(this).data('menu');
                    if (m) {
                        m.timer = setTimeout(function () {
                            m.hide();
                        }, 100);//延时隐藏，时间自定义，100ms
                    }
                }
        );
    });
</script>
<div data-toggle="topjui-layout" data-options="id:'index_layout',fit:true">
    <div id="north" class="banner" data-options="region:'north',border:false,split:false"
         style="height: 50px; padding:0;margin:0; overflow: hidden;">
        <table style="float:left;border-spacing:0px;">
            <tr>
                <td class="webname">

                    <span style="font-size:16px; padding-right:8px;">风机报价选型系统</span>

                    <!-- <span style="font-size:16px; padding-right:8px;line-height:48px;vertical-align:middle;" id="systemName"></span> -->

                </td>
                <td class="collapseMenu" style="text-align: center;cursor: pointer;">
                    <span class="fa fa-chevron-circle-left" style="font-size: 18px;"></span>
                </td>
                <td>
                    <table id="topmenucontent" cellpadding="0" cellspacing="0">
                        <td id="1" title="" class="topmenu selected systemName">
                            <a class="l-btn-text bannerMenu" href="javascript:void(0)">
                                <p>
                                    <lable class="fa fa-suitcase"></lable>
                                </p>
                                <p><span style="white-space:nowrap;" id="workspaceName"></span></p>
                            </a>
                        </td>

                    </table>
                </td>
                <td>
                    <img src="images/logo_ch.png" style="margin-left: 40px;margin-top: 3px;">
                </td>
            </tr>
        </table>
        <div class="top_right f_r">
            <!-- menu -->
            <div class="nav_bar">
                <ul class="nav clearfix" id="ulMenu">

                    <!-- 单一菜单 | end -->
                    <!--<li class="m">
                        <h3><a title="官方网站" class="l-btn-text bannerbtn" href="http://www.topjui.com"
                               target="_blank"><i class="fa fa-home"></i></a></h3>
                    </li>
                    <li class="s">|</li>-->

                    <li class="m">
                        <h3>
                            <a title="切换主题" id="setThemes" class="l-btn-text bannerbtn"
                               href="javascript:void(0)"><i class="fa fa-tree"></i></a>
                        </h3>
                    </li>
                    <li class="s">|</li>

                    <li class="m">
                        <h3>
                            <a class="l-btn-text bannerbtn"
                               href="javascript:void(0)"><i class="fa fa-cog"></i></a>
                        </h3>
                        <ul class="sub">
                            <li><a class="fa fa-info-circle" href="" target="_blank">
                                关于系统</a></li>
                            <li><a class="fa fa-user" href="" target="_blank"> 联系我们</a>
                            </li>
                        </ul>
                    </li>
                    <li class="s">|</li>

                    <li class="m">
                        <h3>
                            <a id="showUserInfo" style="display:inline-block;" class="fa bannerbtn"
                               href="javascript:void(0)">
                                <img src="images/user.png" class="user-image" alt="User Image">
                                <span id="user-name">管理员</span>
                            </a>
                        </h3>
                        <ul class="sub">
                            <li><a class="fa fa-info-circle" href="javascript:showAbout();">联系管理员</a></li>
                            <li><a class="fa fa-key" href="javascript:editPassword()">修改密码</a></li>
                            <li><a class="fa fa-power-off" onclick="logoutFunction()"> 退出系统</a></li>
                        </ul>
                    </li>
                    <li class="block"></li><!-- 滑动块 -->

                </ul>
            </div>
            <!-- menu | end -->
        </div>
    </div>

    <div id="west"
         data-options="region:'west',split:true,width:230,border:false,headerCls:'border_right',bodyCls:'border_right'"
         title="" iconCls="fa fa-dashboard">
        <div id="RightAccordion"></div>
        <!--<div id="menuTab" class="easyui-tabs" data-options="fit:true,border:false">
            <div title="导航菜单" data-options="iconCls:'fa fa-sitemap'" style="padding:0;">
                <div id="RightAccordion" class="easyui-accordion"></div>
            </div>
            <div title="常用链接" data-options="iconCls:'fa fa-star',closable:true">
                <ul id="channgyongLink"></ul>
            </div>
        </div>-->
    </div>

    <div id="center" data-options="region:'center',border:false" style="overflow:hidden;">
        <div id="index_tabs" style="width:100%;height:100%">
            <div title="系统首页" iconCls="fa fa-home" data-options="border:true,
            content:'<iframe id=\'workbench\' src=\'/pages/development/workbench/workbench.jsp\' scrolling=\'auto\' frameborder=\'0\' style=\'width:100%;height:100%;\'></iframe>'"></div>
        </div>

    </div>

    <div data-options="region:'south',border:true"
         style="text-align:center;height:30px;line-height:30px;border-bottom:0;overflow:hidden;">
        <span style="float:left;padding-left:5px;width:30%;text-align: left;" id = 'current_user'>当前用户：管理员</span>
        <span style="padding-right:5px;width:40%">
            版权所有 © 2016-2020
            <a target="_blank">北京鲲博时代信息科技有限公司</a>
            <!--<a href="http://www.miitbeian.gov.cn" target="_blank">粤ICP备16028103号-1</a>-->
        </span>
        <span style="float:right;padding-right:5px;width:30%;text-align: right;">正式版v1.0</span>
    </div>
</div>

<!--[if lte IE 8]>
<div id="ie6-warning">
    <p>您正在使用低版本浏览器，在本页面可能会导致部分功能无法使用，建议您升级到
        <a href="http://www.microsoft.com/china/windows/internet-explorer/" target="_blank">IE9或以上版本的浏览器</a>
        或使用<a href="http://se.360.cn/" target="_blank">360安全浏览器</a>的极速模式浏览
    </p>
</div>
<![endif]-->

<div id="themeStyle" data-options="iconCls:'fa fa-tree'" style="display:none;width:600px;height:340px">
    <table style="width:100%; padding:20px; line-height:30px;text-align:center;">
        <tr>
            <td>
                <div class="skin-common skin-black"></div>
                <input type="radio" name="themes" value="black" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-red"></div>
                <input type="radio" name="themes" value="red" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-green"></div>
                <input type="radio" name="themes" value="green" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-purple"></div>
                <input type="radio" name="themes" value="purple" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-blue"></div>
                <input type="radio" name="themes" value="blue" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-yellow"></div>
                <input type="radio" name="themes" value="yellow" class="topjuiTheme"/>
            </td>
        </tr>
        <tr>
            <td>
                <div class="skin-common skin-blacklight"></div>
                <input type="radio" name="themes" value="blacklight" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-redlight"></div>
                <input type="radio" name="themes" value="redlight" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-greenlight"></div>
                <input type="radio" name="themes" value="greenlight" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-purplelight"></div>
                <input type="radio" name="themes" value="purplelight" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-bluelight"></div>
                <input type="radio" name="themes" value="bluelight" class="topjuiTheme"/>
            </td>
            <td>
                <div class="skin-common skin-yellowlight"></div>
                <input type="radio" name="themes" value="yellowlight" class="topjuiTheme"/>
            </td>
        </tr>
    </table>
    <table style="width: 100%; padding: 20px; line-height: 30px; text-align: center;">
        <tr>
            <td>
                <input type="radio" name="menustyle" value="accordion" checked="checked"/>手风琴
            <td>
                <input type="radio" name="menustyle" value="tree"/>树形
            </td>
            <td>
                <input type="checkbox" checked="checked" name="topmenu" value="topmenu"/>开启顶部菜单
            </td>
        </tr>
    </table>
</div>

<form id="pwdDialog"
      data-options="title: '修改密码',
      iconCls:'fa fa-key',
      width: 400,
      height: 300,
      href: '/html/user/modifyPassword.html'"></form>
      
<!--修改密码窗口-->
    <div id="editPwdWindow" class="easyui-window" title="修改密码" collapsible="false" minimizable="false" modal="true" closed="true" resizable="false"
        maximizable="false" icon="icon-save"  style="width: 300px; height: 160px; padding: 5px;
        background: #fafafa">
        <div class="easyui-layout" fit="true">
            <div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
                <table cellpadding=3>
                    <tr>
                        <td>新密码：</td>
                        <td><input id="txtNewPass" type="Password" class="txt01" /></td>
                    </tr>
                    <tr>
                        <td>确认密码：</td>
                        <td><input id="txtRePass" type="Password" class="txt01" /></td>
                    </tr>
                </table>
            </div>
            <div region="south" border="false" style="text-align: right; height: 30px; line-height: 30px;">
                <a id="btnEp" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)" >确定</a> 
                <a id="btnCancel" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0)">取消</a>
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">
    var userJson = <%=userJson%>;

    $('#current_user').text(userJson.actual_name);
    $('#user-name').text(userJson.actual_name);
$(function(){

    $('#workbench').attr('src',"/pages/development/workbench/workbench.jsp?postation_id="+userJson.position_id);
    


  // 初始化系统设置
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do?tableName=system_set",
    type: "POST",
    dataType:'json',
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      if (data.rows.length > 0) {
        var row = data.rows[0]
        if (row.system_icon != '') {
          $('#systemName').html('<img src="<%=basePath%>' + row.system_icon + '" class="systemIcon" height="40"><span>'+row.system_name+'</span>')
        } else {
          $('#systemName').html('<span>'+row.system_name+'</span>')
        }
        $('#workspaceName').text(row.workspace_name)
      }
    }
  })
})
</script>
<script type="text/javascript">



    function logoutFunction()
    {
        $.ajax({
            url: "<%=basePath%>/system/user/logoutSys.do",
            type: "POST",
            dataType:'json',
            error: function() //失败
            {
              console.info('error');
              $.messager.alert('提醒',"<font size='4px'>退出失败</font>",'warning');
            },
            success: function(data)//成功
            {
            	top.location="login.jsp";
            }
        });
    }
    //管理员信息
    function showAbout(){
		$.messager.alert("维保云 v1.0","电话: 18334723118<br/> 管理员邮箱: yangfeng@king-break.com <br/> QQ: 624939551");
	}
    
    //修改密码
   function editPassword(){
	   $('#editPwdWindow').window('open');
    }
    
   $("#btnCancel").click(function(){
		$('#editPwdWindow').window('close');
	});
   
   $("#btnEp").click(function(){
		var newPass = $("#txtNewPass").val(); // document.getElementById("txtNewPass").value; 
		var rePass = $("#txtRePass").val();
		// 进行校验
		// 新密码是否为空
		if($.trim(newPass)==""){ // 也可以写为 jQuery.trim
			// 新密码输入为空 
			$.messager.alert('警告','新密码不能为空或者空白字符！','warning');
			return ;
		}
		// 两次密码是否一致
		if($.trim(newPass) != $.trim(rePass)){
			$.messager.alert('警告','两次密码输入不一致！','warning');
			return ;
		}
		// 通过 Ajax 将新密码发送到服务器 
		/**/
		$.post("${pageContext.request.contextPath}/system/user/editPassword.do", {password: newPass}, function(data){
			if(data.result == true){
				$.messager.alert("信息", data.describe, "info");
				//密码修改成功之后请重新登录
				location.href = '${pageContext.request.contextPath }/pages/invalidate.jsp';
			}else{
				$.messager.alert("信息", data.describe, "info");
			}
			// 窗口关闭 
			$("#editPwdWindow").window('close');
					
		});
	});

</script>
