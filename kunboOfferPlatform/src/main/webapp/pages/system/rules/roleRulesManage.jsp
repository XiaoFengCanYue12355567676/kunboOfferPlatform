<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
   
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>系统用户</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
    </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">
        <div id="dg_tb" style="height: 35px;text-align: center;">
            <input type="text" id="position_id" class="easyui-combobox" data-options="prompt:'选择角色',editable:false" style="width: 100%">
        </div>              

        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                  idField:'id',
                  treeField:'text',
                  method:'get',
                  toolbar:'#dg_tb'
                ">
          <thead>
              <tr>
                  <th field="text" width="100%">项目</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
      <table id="dg" style="width:100%;height:100%" class="easyui-datagrid" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:true,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  pageSize:10">
          <thead>
          <tr>               
          </tr> 
          </thead>
      </table>       
    </div>
    
<input class="easyui-switchbutton" id="mtAdd" onText="开始" offText="未开始"  style="width:80px;height:35px;line-height:35px;">               


  <script type="text/javascript">
    $(function(){
          //职位
       $('#position_id').combobox( {  
           url:'<%=basePath%>/system/user/position/getAll.do', 
              valueField:'id',
              textField:'text',
              method:'get',
              multiple:false,
              onlyLeafCheck:false           
       });
    });

     $('#tt').treegrid({
         url:'<%=basePath%>/topJUI/index/getMenuList.do',
         onClickRow: function (rowIndex)  
         { 
            if(checkNull($('#position_id').combobox('getValue')))
            {
                layerMsgCustom('必须选择职位');
                return false;
            } 

           $('#dg').datagrid({ 
                url: '<%=basePath%>/system/rule/getRoleRuleByPostAndPageid.do',  
                striped: true,  
                title: "",  
                queryParams: {
                    position_id:$('#position_id').combobox('getValue'),
                    page_id: rowIndex.id
                },
                onLoadSuccess:function(data)
                {
                  console.info(data);
                  for(var i=0;i<data.total;i++)
                  {
                    var is_check;
                    if(data.rows[i].is_use == '1')
                      is_check = true;
                    if(data.rows[i].is_use == '0')
                      is_check = false;



                    console.info(i);
                    $('#'+data.rows[i].id).switchbutton({
                      checked: is_check,      
                      onText:'允许',
                      offText:'阻止',
                      height:'23px',
                      onChange: function(checked)
                      {        
                        console.log(checked+"   "+i);
                        console.log(this.id);
                        up(this.id,checked);
                      }
                    })
                  }
                },
                columns:
                [[
                  {field:'text',title:'权限名称'},
                  {field:'component_id',title:'对应组件id'},
                  {field:'component_style',title:'组件类型'},
                  {
                    field:'is_use',title:'是否可用',
                    formatter: function(value,row,index)
                    {
                      var htmlstr = '<input id="'+row.id+'" class="easyui-switchbutton" checked>';  
                      return htmlstr;
                    }
                  }
                ]]
            });
         },
         onLoadSuccess:function(data)
         {  
         }  
      })
	</script>
  



</body> 
</html>
<script type="text/javascript">
  function up(id,result)
  {
    var oooo = new Object();
    oooo.id = parseInt(id);
    oooo.is_use = result;

    var rows = $('#dg').datagrid('getRows'); 

    var sss = false;

    for(var i=0;i<rows.length;i++)
    {
      if(rows[i].id == id)
      {
        oooo.rule_struct_id =parseInt(rows[i].rule_struct_id);
        oooo.postation_id = parseInt(rows[i].postation_id);
        sss = true;
        break;
      }
    }
    if(sss = false)
    {
      layerMsgCustom('错误！无法找到对应的权限内容');
      return;
    }




    // console.info(rows);
    var json_str = JSON.stringify(oooo);
    console.info(json_str);

    $.ajax({
      url: "<%=basePath%>/system/rule/roleRule/save.do",
      type : "POST",
      dataType : 'json',
      data : {
          'data' : JSON.stringify(oooo),               
      },
      error : function() //失败
      {

      },
      success : function(data)//成功
      {
          $('#dg').datagrid('reload'); 
      }
    });
  }


  function add()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '新建目录',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['750px', '300px'],
          btn: '保存',
          content: 'form/addMenu.jsp?pid='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }

  function edit()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['750px', '300px'],
          btn: '保存',
          content: 'form/editMenu.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele()
  {
      var row = $('#tt').treegrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }
      $.ajax({
          url: "<%=basePath%>/system/rule/roleRule/delete.do",
          type : "POST",
          dataType : 'json',
          data : {
              'data' : json_str,               
          },
          error : function() //失败
          {

          },
          success : function(data)//成功
          {
              
              var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
              parent.layer.close(index); //再执行关闭
              parent.updateSuccess();
          }
      });
  }


  function updateSuccess(tt_pid)
  {
    $('#tt').treegrid('reload',tt_pid); 

  }

</script>

