<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>权限结构管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div style="display:none;overflow:hidden;padding:3px" id="addDialog">
        <iframe id='addDialogIframe' frameborder="no" border="0" marginwidth="0" marginheight="0"  scrolling="auto"  width="100%" height="100%"></iframe>
    </div>
    <div style="display:none;overflow:hidden;padding:3px" id="editDialog">
        <iframe id='editDialogIframe' frameborder="no" border="0" marginwidth="0" marginheight="0"  scrolling="auto"  width="100%" height="100%"></iframe>
    </div>

    <script type="text/javascript">
      var user = <%=userJson%>;
    </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">
        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                  idField:'id',
                  treeField:'text',
                  method:'get'
                ">
          <thead>
              <tr>
                  <th field="text" width="100%">项目</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
      <table id="dg" style="width:100%;height:100%" class="easyui-datagrid" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:true,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:10">
          <thead>
          <tr>               
            <th field="component_id">对应组件id</th>      
            <th field="component_style">组件类型</th>      
            <th field="text">权限名称</th>      
          </tr> 
          </thead>
      </table>
      <div id="tb" style="padding:0px 30px;height: 35px">
              <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="add()">新增</a>  
              <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="edit()">编辑</a>  
              <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-trash" style="margin-top: 2px" onclick="dele()">删除</a>     
      </div>          
    </div>
    



  <script type="text/javascript">
     $('#tt').treegrid({
         url:'<%=basePath%>/topJUI/index/getMenuList.do',
         onClickRow: function (rowIndex)  
         { 
            var url = 'form/viewMenu.jsp?id='+rowIndex.id;

            $('#dg').datagrid({ 
                url: '<%=basePath%>/system/rule/getRulesStructsByPageId.do',  
                striped: true,  
                title: "",  
                queryParams: {
                    page_id: rowIndex.id
                }
            });
         },
         onLoadSuccess:function(data)
         {  
           


         }  
      })
	</script>
  



</body> 
</html>
<script type="text/javascript">
  function add()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      // $("#addDialogIframe").attr('src','form/addRuleStructs.jsp?page_id='+row.id)
      // $("#addDialog").dialog({
      //   bgiframe: true,
      //   resizable: true, //是否可以重置大小
      //   height: 330, //高度
      //   width: 550, //宽度
      //   draggable: false, //是否可以拖动。
      //   title: "检索",
      //   modal: true,
      //   // href: 'form/addRuleStructs.jsp?page_id='+row.id,
      //   open: function(e) { //打开的时候触发的事件
      //     document.body.style.overflow = "hidden"; //隐藏滚动条
      //   },
      //   close: function() { //关闭Dialog时候触发的事件
      //     document.body.style.overflow = "visible"; //显示滚动条
      //   },
      //   buttons: [{
      //     text: '保存',
      //     iconCls: 'fa fa-save',
      //     plain: true,
      //     handler: function() {
      //       $("#addDialogIframe")[0].contentWindow.update();
      //     }
      //   }]
      // });


      layer.open({
          type: 2,
          title: '新建目录',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['550px', '300px'],
          btn: '保存',
          content: 'form/addRuleStructs.jsp?page_id='+row.id,
          yes:function(index){
              shuoheUtil.layerSaveMaskOn();
              var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
              Ifame.update();
          }
      })
  }

  function edit()
  {

      var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
        type: 2,
        title: '修改目录',
        shadeClose: true,
        shade: 0.3,
        maxmin: true, //开启最大化最小化按钮
        area: ['550px', '300px'],
        btn: '保存',
        content: 'form/editRuleStructs.jsp?id=' + row.id,
        yes: function(index) {
          shuoheUtil.layerSaveMaskOn();
          var ifname = "layui-layer-iframe" + index //获得layer层的名字
          var Ifame = window.frames[ifname] //得到框架
          Ifame.update();
        }
      })
  }
  function dele()
  {
      var row = $('#dg').datagrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }
      shuoheUtil.layerSaveMaskOn();
      $.ajax({
          url: "<%=basePath%>/system/rule/deleteRuleStructForAllRole.do",
          type : "POST",
          dataType : 'json',
          data : {
              'data' : json_str              
          },
          error : function() //失败
          {
            shuoheUtil.layerSaveMaskOff();
          },
          success : function(data)//成功
          {
              updateSuccess(row.id);
              shuoheUtil.layerSaveMaskOff();
          }
      });
  }


  function updateSuccess(tt_pid)
  {
    $('#dg').datagrid('reload'); 
    shuoheUtil.layerSaveMaskOff();
  }

</script>

