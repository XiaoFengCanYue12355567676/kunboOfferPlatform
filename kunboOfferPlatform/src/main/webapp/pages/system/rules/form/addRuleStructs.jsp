
<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>

<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    PrintWriter ss = response.getWriter();
    String page_id = request.getParameter("page_id"); 
    User user = null;
    String userStr = null;
    try
    {

        user = (User)session.getAttribute("user");
        if(user == null)
        {
            System.out.println("user = null");
            //response.sendRedirect(basePath+"/pages/login.jsp");
            String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
            ss.print(topLocation);
        }
        userStr = Json.toJsonByPretty(user);
    }
        catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>备件检索</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>
    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%}
</style>



<body>
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="text-align: center;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">权限结构设置</span>
                </div>
            </td>
        </tr>               
        <tr>
            <td class="label">组件ID</td>
            <td >
                <input id='component_id' class="easyui-textbox" data-options="required:true,width:160,editable:true">
            </td>
        </tr>        
        <tr>
            <td class="label">组件类型</td>
            <td >
                <!-- <input id='component_style' class="easyui-combobox" data-options="required:false,width:160,editable:true"> -->
            
                <select id='component_style' class="easyui-combobox" data-options="required:true,width:160,editable:false">
                    <option value="linkbutton">按钮</option>  
                    <option value="page">页面</option>   
                </select>            
            </td>   
        </tr>                 
        <tr>            
			<td class="label">权限名称</td>
            <td >
                <input id='text' class="easyui-textbox" data-options="required:true,width:160,editable:true">               
            </td>
        </tr>       
    </table>
	
</body> 
</html>
<script type="text/javascript">
    var user = <%=userStr%>;
    var page_id = <%=page_id%>;
</script>
<script type="text/javascript">
    function update() {
        var o = new Object();
        o.id = 0;
        o.page_id = page_id;
        o.component_id = $('#component_id').textbox('getValue');
        o.component_style = $('#component_style').textbox('getValue');
        o.text = $('#text').textbox('getValue');

        var json_str = JSON.stringify(o);
        $.ajax({
            url: "<%=basePath%>/system/rule/addRuleStructForAllRole.do",
            type : "POST",
            dataType : 'json',
            data : {
                'data' : json_str
            },
            error : function() //失败
            {
                shuoheUtil.layerSaveMaskOff();
            },
            success : function(data)//成功
            {           
                shuoheUtil.layerSaveMaskOff();     
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                parent.layer.close(index); //再执行关闭
                parent.updateSuccess();
            }
        });

    }
</script>