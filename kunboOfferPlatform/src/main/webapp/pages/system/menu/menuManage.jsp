<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    /* User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
     */


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>菜单管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
<%--       var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>; --%>
    </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">
        <div id="dg_tb" style="height: 35px;text-align: center;">
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="add()">新增</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="edit()">修改</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="dele()">删除</a>                
        </div>              

        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                  idField:'id',
                  treeField:'text',
                  method:'get',
                  toolbar:'#dg_tb'
                ">
          <thead>
              <tr>
                  <th field="text" width="100%">项目</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
      <iframe id='iframe_windows' src='form/viewMenu.jsp' frameborder="no" border="0" marginwidth="0" marginheight="0"  scrolling="no"  width="100%" height="100%"></iframe>        
    </div>
    



  <script type="text/javascript">
     $('#tt').treegrid({
         url:'<%=basePath%>/topJUI/index/getMenuList.do',
         onClickRow: function (rowIndex)  
         { 
            var url = 'form/viewMenu.jsp?id='+rowIndex.id;
            $('#iframe_windows').attr('src',url);
         },
         onLoadSuccess:function(data)
         {  
         }  
      })
	</script>
  



</body> 
</html>
<script type="text/javascript">
  function add()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '新建目录',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['750px', '300px'],
          btn: '保存',
          content: 'form/addMenu.jsp?pid='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }

  function edit()
  {

      var row = $('#tt').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: true,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['750px', '300px'],
          btn: '保存',
          content: 'form/editMenu.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele()
  {
      var row = $('#tt').treegrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }
      $.ajax({
          url: "<%=basePath%>/topJUI/index/delete.do",
          type : "POST",
          dataType : 'json',
          data : {
              'data' : json_str
          },
          error : function() //失败
          {

          },
          success : function(data)//成功
          {
              
              var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
              parent.layer.close(index); //再执行关闭
              $('#tt').treegrid('reload'); 
          }
      });
  }


  function updateSuccess(tt_pid)
  {
    $('#tt').treegrid('reload',tt_pid); 

  }

</script>

