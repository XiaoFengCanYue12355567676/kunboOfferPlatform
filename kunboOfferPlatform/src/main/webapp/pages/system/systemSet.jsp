<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>系统设置</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
<style>
.system-icon{
  margin-right: 5px;
  vertical-align: middle;
}
</style>
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>系统名称</label>
    <input type="text" id="systemName" class="easyui-textbox" data-options="width:740" />
  </div>
  <div class="field-form-line field-form-line-one" style="line-height:45px;">
    <label>系统图标</label>
    <input type="hidden" id="systemIcon" />
    <span id="systemIconCon" style="display:inline-block;"></span>
    <button type="button" class="button-default" onclick="upload()">上传图片</button>
  </div>
  <div class="field-form-line field-form-line-one">
    <label>工作区名称</label>
    <input type="text" id="workspaceName" class="easyui-textbox" data-options="width:740" />
  </div>
  <div class="button-con">
    <button type="button" onclick="save()">保存</button>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
var basePath = '<%=basePath%>'
var id = 1
$(function(){
  // 初始化数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do?tableName=system_set",
    type: "POST",
    dataType:'json',
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      if (data.rows.length > 0) {
        var row = data.rows[0]
        $('#systemName').textbox('setValue', row.system_name)
        $('#systemIcon').val(row.system_icon)
        if (row.system_icon!='') {
          $('#systemIconCon').html('<img src="<%=basePath%>'+row.system_icon+'" height="40" class="system-icon">')
        }
        $('#workspaceName').textbox('setValue', row.workspace_name)
        id = row.id
      } else {
        var obj1 = new Object()
        obj1.title = 'system_set'
        obj1.field = []
        obj1.field.push({'text':'system_name','value':"''"})
        obj1.field.push({'text':'system_icon','value':"''"})
        obj1.field.push({'text':'workspace_name','value':"''"})
        var info_str1 = JSON.stringify(obj1)
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormUtil/insert.do",
          type: "POST",
          dataType:'text',
          data:
          {
            "jsonStr": info_str1
          },
          error: function(e) //失败
          {
           console.info("异常="+JSON.stringify(e));
            messageSaveError();
          },
          success: function(data)//成功
          {

          }
        })
      }
    }
  })
})
function upload(){
  layer.open({
    type: 2,
        title: '上传图片',
        shadeClose: false,
        shade: 0.3,
        zIndex: 1,
        maxmin: true, //开启最大化最小化按钮
        area: ['640px', '350px'],
        content:'<%=basePath%>/pages/fileManager/addSinglePicture.jsp',
        btn:['关闭'],
        yes: function(index, layero){
          var res = window["layui-layer-iframe" + index].transferPath()
          if(res!=''){
            $('#systemIcon').val(res)
            $('#systemIconCon').html('<img src="<%=basePath%>'+res+'" height="40" class="system-icon">')
          }
          //最后关闭弹出层
          layer.close(index);
        },
  });
}
//保存
function save () {
  var obj = new Object()
  obj.title = 'system_set'
  obj.field = []
  obj.field.push({'text':'system_name','value':"'"+$.trim($('#systemName').textbox('getValue'))+"'"})
  obj.field.push({'text':'system_icon','value':"'"+$.trim($('#systemIcon').val())+"'"})
  obj.field.push({'text':'workspace_name','value':"'"+$.trim($('#workspaceName').textbox('getValue'))+"'"})
  var info_str = JSON.stringify(obj)
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/update.do",
    type: "POST",
    dataType:'text',
    data:
    {
      "jsonStr": info_str,
      "id": id
    },
    error: function(e) //失败
    {
     console.info("异常="+JSON.stringify(e));
      messageSaveError();
    },
    success: function(data)//成功
    {
      data = $.trim(data)
      if(data == '1'){
        messageSaveOK()
      }else{
         messageSaveError();
      }
    }
  })
}

</script>
