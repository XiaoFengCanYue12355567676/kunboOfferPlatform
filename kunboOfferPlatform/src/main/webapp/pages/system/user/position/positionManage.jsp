<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>职位管理</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>
  
    <div data-options="region:'center',title:'',split:true" style="width:100%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20">
					<thead>
						<tr href="#">
			                 
							<th field="id" align="center" hidden ="true">id</th>
							<th field="text" align="center">职位名称</th>						
              <th field="activiti_uuid" align="center">UUID</th> 
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="creatFuncation()">新增</a>
          <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="editFuncation()">修改</a>
          <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="deleteFuncation()">删除</a>
        </div>
     </div>
	
 
  
  <script type="text/javascript">
    $(function(){
        $('#dg').datagrid({
          url:'<%=basePath%>/system/user/position/getAll.do',
          queryParams:
          {
          },
          method:'get'
        })
    });
  </script>


  <script type="text/javascript">
    function creatFuncation()
    {
        layer.open({
          type: 2,
          title: '新增职位',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['400px', '200px'],
          btn: '保存',
          content: 'form/createPosition.jsp',
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
    function addDialogCallBackSuccess()
    {
      $('#dg').datagrid('reload');  
    }
  </script>
  <script type="text/javascript">
    function editFuncation()
    {
        var tt_row = $('#dg').datagrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '编辑职位',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['400px', '200px'],
          btn: '保存',
          content: 'form/editPosition.jsp?id='+tt_row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
  </script>
 <script type="text/javascript">
    function deleteFuncation()
    {
        var tt_row = $('#dg').datagrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        $.ajax({
            url: "<%=basePath%>/system/user/position/delete.do",
            type: "GET",
            dataType:'json',
            data:  
            { 
                data:JSON.stringify(tt_row)
            },
            error: function() //失败
            { 
                messageSaveError();
            },
            success: function(data)//成功
            { 
                var result = jQuery.parseJSON(JSON.stringify(data));
                if(result.result == true)
                {
                    addDialogCallBackSuccess();
                    messageSaveOK();    
                }
                else messageSaveError();                
            } 
        });
    }
  </script>




</body> 
</html>

