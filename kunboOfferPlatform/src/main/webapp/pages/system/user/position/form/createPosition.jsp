<%@page import="com.shuohe.service.impl.system.user.UserServiceImpl"%>
<%@page import="com.shuohe.service.system.user.UserService"%>
<%@page import="com.shuohe.service.system.user.DepartmentService"%>
<%@page import="javax.annotation.Resource"%>
<%@page import="com.shuohe.entity.system.user.Department"%>
<%@page import="com.shuohe.service.impl.system.user.DepartmentServiceImpl"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增职位</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: visible;}
</style>



<body style="text-align: right;">
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="margin-left: 0px;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">新增职位</span>
                </div>
            </td>
        </tr>         
        <tr>
            <td class="label">职位名称</td>
            <td >
                <input id='text' class="easyui-textbox" data-options="required:true,width:200,editable:true">
            </td>        
        </tr>         
    </table>
	
</body> 
</html>

<script type="text/javascript">
    var department = null;
</script>
<script type="text/javascript">
    $(function(){    	

    });
        

</script>
<script type="text/javascript">
    

    function submit()
    {
        if(checkInput())
        {
            var o = new Object();
            o.id = 0;
            o.text = $('#text').textbox('getValue');  
            o.activiti_uuid = '';
         
            $.ajax({
                url: "<%=basePath%>/system/user/position/save.do",
                type: "GET",
                dataType:'json',
                data:  
                { 
                    data:JSON.stringify(o)
                },
                error: function() //失败
                { 
                    messageSaveError();
                },
                success: function(data)//成功
                { 
                    var result = jQuery.parseJSON(JSON.stringify(data));
                    if(result.result == true)
                    {
                          var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                          parent.layer.close(index); //再执行关闭
                          parent.addDialogCallBackSuccess();
                    }
                    else messageSaveError();                
                } 
            });

        }
    }

    function checkInput()
    {
		if(checkNull($('#text').textbox('getValue')))
        {
            layerMsgCustom('职位名称必须填写');
            return false;
        }	
        return true;
    }

</script>