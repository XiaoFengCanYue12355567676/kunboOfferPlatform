<%@page import="com.shuohe.service.impl.system.user.UserServiceImpl"%>
<%@page import="com.shuohe.service.system.user.UserService"%>
<%@page import="com.shuohe.service.system.user.DepartmentService"%>
<%@page import="javax.annotation.Resource"%>
<%@page import="com.shuohe.entity.system.user.Department"%>
<%@page import="com.shuohe.service.impl.system.user.DepartmentServiceImpl"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@ page import="java.io.PrintWriter"%> 
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String user_id = request.getParameter("user_id");
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>编辑用户</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;overflow: visible}
</style>



<body style="text-align: right;">
    <!-- <table class="editTable" style="margin-left: 0px;" border="1" cellspacing="0" cellpadding="0"> -->
    <table class="editTable" style="margin-left: 0px;" >
         <tr>
            <td class="label" colspan="4" style="text-align: left;font-size: 20px;">
                <div class="divider">
                    <span style="margin-left: 50px">编辑用户</span>
                </div>
            </td>
        </tr>         
        <tr>
            <td class="label">用户名</td>
            <td >
                <input id='name' class="easyui-textbox" data-options="required:true,width:200,editable:true">
            </td>
            <td class="label">真实姓名</td>
            <td >
                <input id='actual_name' class="easyui-textbox" data-options="required:true,width:200,editable:true">
            </td>            
        </tr>         
        <tr>
<!--             <td class="label">密码</td> -->
<!--             <td > -->
<!--                 <input id='password' class="easyui-textbox" data-options="required:true,width:200,editable:true"> -->
<!--             </td> -->
            <td class="label">电话</td>
            <td >
                <input id='phone_call' class="easyui-textbox" data-options="required:true,width:200,editable:true">
            </td>  
            <td class="label">邮件</td>
            <td >
                <input id='email' class="easyui-textbox" data-options="required:true,width:200,editable:true">
            </td>          
        </tr>         
        <tr>
            
            <td class="label">部门</td>
            <td >
                <input id='department_id' class="easyui-combotree" data-options="required:true,width:200,editable:false" disabled="true">
            </td>  
            <td class="label">职位</td>
            <td >
                <input id='position_id' class="easyui-combobox" data-options="required:true,width:200,editable:false">
            </td>          
        </tr>   
        <tr>
            <td class="label">状态</td>
            <td >
                <input id='status' class="easyui-combobox" data-options="required:true,width:200,editable:false">
            </td>            
        </tr>                               
    </table>
    
</body> 
</html>

<script type="text/javascript">
    var user_id = <%=user_id%>;
    var user = null;
</script>
<script type="text/javascript">
    $(function(){       

        $.ajax({
            url: "<%=basePath%>/system/user/getById.do",
            type: "GET",
            dataType:'json',
            data:  
            { 
              id:user_id
            },
            error: function() //失败
            { 
                messageSaveError();
            },
            success: function(data)//成功
            { 
                var result = jQuery.parseJSON(JSON.stringify(data));
                user = data;
                if(data != null)
                {                    

                    $('#name').textbox('setValue',user.name);
                    $('#actual_name').textbox('setValue',user.actual_name);
                    $('#phone_call').textbox('setValue',user.phone_call);
                    $('#email').textbox('setValue',user.email);

                    $('#department_id').combotree( {  
                        //获取数据URL  
                        url:'<%=basePath%>/system/user/department/getAll.do', 
                        idField:'id',
                        treeField:'text',
                        method:'get',
                        multiple:false,
                        onlyLeafCheck:false,
                        onLoadSuccess:function(data)
                        {  
                            $('#department_id').combotree('setValue',user.department_id);
                        }               
                    });    
                    $('#position_id').combobox({  
                        //获取数据URL  
                        url:'<%=basePath%>/system/user/position/getAll.do', 
                        valueField:'id',
                        textField:'text',
                        method:'get',
                        multiple:false,
                        onlyLeafCheck:false,
                        onLoadSuccess:function(data)
                        {  
                            $('#position_id').combobox('setValue',user.position_id);
                        }                 
                    });
                    $('#status').combobox( {  
                        //获取数据URL  
                        url:'<%=basePath%>/system/user/userStatus/getAll.do', 
                        valueField:'id',
                        textField:'text',
                        method:'get',
                        multiple:false,
                        onlyLeafCheck:false,
                        onLoadSuccess:function(data)
                        {  
                            $('#status').combobox('setValue',user.status);
                        }              
                    }); 
                }
                else messageSaveError();                
            } 
        });   
    });
        

</script>
<script type="text/javascript">
    

    function submit()
    {
        if(checkInput())
        {
            user.name = $('#name').textbox('getValue');        //用户名
            user.actual_name = $('#actual_name').textbox('getValue'); //用户真实姓名
            user.department_id = $('#department_id').combotree('getValue');  //用户部门id
            user.position_id = $('#position_id').combobox('getValue');    //用户职位id
            user.email = $('#email').textbox('getValue');       //用户邮件
            user.phone_call = $('#phone_call').textbox('getValue');  //用户电话
            user.registration_date = getSystemDate(); //用户注册时间
            user.status = $('#status').combobox('getValue');         //用户状态

            $.ajax({
                url: "<%=basePath%>/system/user/update.do",
                type: "GET",
                dataType:'json',
                data:  
                { 
                    data:JSON.stringify(user)
                },
                error: function() //失败
                { 
                    messageSaveError();
                },
                success: function(data)//成功
                { 
                    var result = jQuery.parseJSON(JSON.stringify(data));
                    department_pid = data;
                    if(result.result == true)
                    {
                          var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                          parent.layer.close(index); //再执行关闭
                          parent.addDialogCallBackSuccess();
                    }
                    else messageSaveError();                
                } 
            });

        }
    }

    function checkInput()
    {
        if(checkNull($('#name').textbox('getValue')))
        {
            layerMsgCustom('必须填写用户名');
            return false;
        }   
        if(checkNull($('#actual_name').textbox('getValue')))
        {
            layerMsgCustom('必须填写真实姓名');
            return false;
        }                  
        return true;
    }

</script>