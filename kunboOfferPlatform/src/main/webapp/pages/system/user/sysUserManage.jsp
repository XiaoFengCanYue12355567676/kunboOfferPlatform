<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%> 

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null; 
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter(); 
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);     
        }    
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);       
        e.printStackTrace();
    }
    


%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>系统用户</title> 


    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>

    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:30%;">
        <div id="dg_tb" style="height: 35px;text-align: center;">
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-plus" style="margin-top: 2px" onclick="addOrganization()">新增</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-pencil" style="margin-top: 2px" onclick="modifyOrganization()">修改</a>  
            <a href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-trash" style="margin-top: 2px" onclick="deteleOrganization()">删除</a>                 
        </div>              

        <table id="tt" style="height:100%;width: 100%;" title="" data-options="
                idField:'id',
                treeField:'text',
                method:'get',
                toolbar:'#dg_tb'
                ">
          <thead>
              <tr>
                  <th field="text" width="80%">部门列表</th>
                  <th field="header_name" width="20%">负责人</th>
                  <th field="id" width="0px" hidden="true">序列</th>
              </tr>
          </thead>
        </table>        
	   </div>
    <div data-options="region:'center',title:'',split:true" style="width:70%;">
      <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20">
					<thead>
						<tr href="#">
			                 
							<th field="id" align="center" hidden ="true">id</th>
							<th field="name" align="center">用户名</th>
							<th field="actual_name" align="center">实际姓名</th>
							<!-- <th field="password" width="10%" align="center">密码</th> -->
							<th field="phone_call" align="center">电话</th>
							<th field="email" align="center">邮箱</th>
							<th field="registration_date" align="center">注册时间</th>
							<th field="dept" align="center">所在部门</th>
							<th field="position" align="center">职位</th>
							<th field="state" align="center">状态</th>
							<th field="department_id" align="center" hidden ="true"></th>
							<th field="position_id" align="center" hidden ="true"></th>
							<th field="status" align="center" hidden ="true"></th>
						</tr>
					</thead>
				</table>		
        <div id="tb" style="height:35px">
          <a id='btnNewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="creatUser()">新增</a>
          <a id='btnEditUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="eidtUser()">修改</a>
          <a id='btnViewUser' href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="viewUser()">查看</a>
        </div>
     </div>
	
 
  
  <script type="text/javascript">
    $(function(){
      $('#tt').treegrid({
         url:'<%=basePath%>/system/user/department/getAll.do',
         onClickRow: function (rowIndex)  
         {
            var rr_row = $('#tt').treegrid('getSelected');  
            $('#dg').datagrid({
              url:'<%=basePath%>/system/user/getUserByDepartment.do',
              queryParams:
              {
                department:rr_row.id,
              },
              method:'get'
            })
         },
         onLoadSuccess:function(data)
         {  
         }  
      })
    });
  </script>


  <script type="text/javascript">
    function addOrganization()
    {
        var tt_row = $('#tt').treegrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '新增部门',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['400px', '300px'],
          btn: '保存',
          content: 'form/createDepartment.jsp?pid='+tt_row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
    function addDialogCallBackSuccess()
    {
      $('#tt').treegrid('reload');  
      $('#dg').datagrid('reload');  
    }
  </script>
  <script type="text/javascript">
    function modifyOrganization()
    {
        var tt_row = $('#tt').treegrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '新增部门',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['400px', '300px'],
          btn: '保存',
          content: 'form/editDepartment.jsp?pid='+tt_row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
    function deteleOrganization() {
        var row = $('#tt').treegrid('getSelected');
        if (row == null) {
            layerMsgCustom('必须选择一条数据');
            return;
        }
        //删除操作属于危险操作，给出提示
        $.messager.confirm('友情提示', '您确定要删除这条数据吗?', function(r) {
            if (r) {
                shuoheUtil.layerTopMaskOn();
                $.ajax({
                    url: "/system/user/department/deleteDepartmentById.do",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        'id': row.id
                    },
                    error: function() //失败
                    {
                        shuoheUtil.layerTopMaskOff();
                    },
                    success: function(data) //成功
                    {
                        // updateSuccess();
                        $('#tt').treegrid('reload');
                        shuoheUtil.layerTopMaskOff();
                        if(data.result)
                        {
                            shuoheUtil.layerMsgOK(data.describe);
                        }
                        else
                        {
                            shuoheUtil.layerMsgError(data.describe);
                        }
                    }
                });
            }
        });
    }


  </script>
 <script type="text/javascript">
    function creatUser()
    {
        var tt_row = $('#tt').datagrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '新增用户',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['800px', '350px'],
          btn: '保存',
          content: 'form/createUser.jsp?department_pid='+tt_row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
  </script>
<script type="text/javascript">
    function eidtUser()
    {
        var tt_row = $('#dg').datagrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '编辑用户',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['800px', '350px'],
          btn: '保存',
          content: 'form/editUser.jsp?user_id='+tt_row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index;//获得layer层的名字
            var Ifame=window.frames[ifname];//得到框架
            Ifame.submit();
          }
        })
    }
  </script>
<script type="text/javascript">
    function viewUser()
    {
        var tt_row = $('#dg').datagrid('getSelected');  
        if(tt_row == null)
        {
            layerMsgCustom('请先选择一条数据');
            return;
        }

        layer.open({
          type: 2,
          title: '查看用户',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: ['800px', '350px'],
          content: 'form/viewUser.jsp?user_id='+tt_row.id
        })
    }
  </script>







<script type="text/javascript">
		
		 function formatDes(val,row,index){
		   	 ret =  '<a href="#" class="easyui-linkbutton" onclick="checkFunction('
		            +'\''+row.id+'\''
		            +')">查看</a>'; 
	   		 return ret;    
		}
		 function formatOpr(val,row,index){
		   	  ret =  '<a href="#" class="easyui-linkbutton" onclick="deteleFunction('
			   		+'\''+row.id+'\','
			   		+'\''+row.name+'\','
			   		+'\''+row.actual_name+'\','
			   		+'\''+row.password+'\','
			   		+'\''+row.phone_call+'\','
			   		+'\''+row.email+'\','
            +'\''+row.registration_date+'\','
            +'\''+row.department_id+'\','
            +'\''+row.position_id+'\','
            +'\''+row.status+'\''
            +')">修改/删除</a>'; 
	   		 return ret;  
	  	 }
	  	 
	  	//查看权限详情  
	  	function checkFunction(id){ 
	  		//弹出用户权限详情页面，可以修改相关权限，保存后添加到权限列表
	  		console.info("id  详情查看==: "+id);
	  		 window.open('authorityManage.jsp?id=\''+id+'\'', '', 'height=600px, width=1050px, scrollbars=false, resizable=false');
	  		}
	  

	</script>
  



</body> 
</html>

