<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>原材料管理</title> 


<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<!-- FontAwesome字体图标 -->
<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<!-- jQuery相关引用 -->
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>

<script type="text/javascript" src="/pages/js/moment.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>
<script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

<script type="text/javascript" src="/pages/js/base-loading.js"></script>
<script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>


<style type="text/css">
    html, body{ margin:0; height:100%; }
    .search-line{
      height: 1px;
      background-color: #ddd;
      margin: 15px 0;
    }
    .text-right{
      text-align: right;
      padding-right: 5px;
      width: 246px;
    }
</style>
    
</head> 
<body class="easyui-layout" style="overflow: hidden;">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:true" style="width:25%;">           

        <table style="width:100%;margin-top: 15px">
              <tr>
                <td class="text-right">Air Flow Rate(P)</td>
                <td>
                  <input id="blowingRate" type="text" class="easyui-textbox" style="width:100px;height:32px;" data-options="required:true">
                  <span><select class="easyui-combobox" id="state" name="state" style="width:80px;">
						<option value="m3/h">m&sup3/h</option>
						<option value="m3/m">m&sup3/min</option>
						<option value="m3/s">m&sup3/s</option>
						<option value="1/s">1/s</option>
						<option value="1/h">1/h</option>
						<option value="gallon/min">gallon/min</option>
					</select>
	             </span>
                </td>
              </tr>
              <tr>
                <td class="text-right">
                <input type="radio" value="Total pressure" name="windPressure" checked="checked"/>Total pressure
                <input type="radio" value="Static pressure" name="windPressure" />Static pressure(Q)
                </td>
                <td>
                  <input id="totalHeadQuery" type="text" class="easyui-textbox" style="width:100px;height:32px;" data-options="required:true">
                  <span><select class="easyui-combobox" name="state" style="width:80px;">
						<option value="Pa">Pa</option>
						<option value="mm-Hg">mm Hg</option>
						<option value="mm-H20">mm H20</option>
						<option value="kPa">kPa</option>
						<option value="ft-H20">ft H20</option>
						<option value="bar">bar</option>
						<option value="atm">atm</option>
					</select>
	             </span>
                </td>
              </tr>
              <tr>
                <td class="text-right">Error Setting of Fan（%）</td>
                <td>
                  <input id="errorMinRange" type="text" class="easyui-textbox" value="-5" style="width:83.6px;height:32px;" data-options="prompt:'-5',required:true">
                  <span id="rangeIdentification">~</span>
                  <input id="errorMaxRange" type="text" class="easyui-textbox" value="5" style="width:83.6px;height:32px;" data-options="prompt:'5',required:true">
                </td>
              </tr>
              <tr>
                <td class="text-right">Speed</td>
                <td>
                  <input id="speedQuery" type="text" class="easyui-textbox" style="width:185px;height:32px;" data-options="required:true">
                </td>
              </tr>
              <tr>
                <td class="text-right">Import air density</td>
                <td>
                  <input id="airDensityQuery" type="text" class="easyui-textbox" style="width:185px;height:32px;" data-options="required:true">
                </td>
              </tr>
              <tr>
                  <td colspan="2"><div class="search-line"></div></td>
                </tr>
              <tr>
                <td class="text-right">Fan type</td>
                <td>
					<input id="fanType" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getFanTypeSelect'">
                </td>
              </tr>
              <!-- <tr>
                <td class="text-right">Correction factor</td>
                <td>
                  <input id="reviseFactor" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getReviseFactorSelect'">
                </td>
              </tr> -->
              <tr>
                <td class="text-right">Transmission mode</td>
                <td>
                <input id="drivingMode" class="easyui-combobox" style="width:185px;height:32px;"
                    data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getchuandongfs'">
              </td>
              </tr>
              <tr>
                <td class="text-right">Is explosion proof</td>
                <td>
                    <input id="ifExplosionProof" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getIfExplosionProofSelect'">
                </td>
              </tr>
              <tr>
                <td class="text-right">Voltage</td>
                <td>
                    <input id="voltage" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getVoltageSelevct'">
					<span style="margin-left: 5px;">V</span>
                </td>
              </tr>
              <tr>
                <td class="text-right">Frequency</td>
                <td>
                  <input id="frequency" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getFrequencySelect'">
					<span style="margin-left: 5px;">Hz</span>
                </td>
              </tr>
              <tr>
                <td class="text-right">Protection level</td>
                <td>
                  <input id="levelsofprotection" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getLevelsofprotectionSelect'">
                </td>
              </tr>
              <!-- <tr>
                <td class="text-right">Insulation level</td>
                <td>
                   <input id="insulationGrade" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getInsulationGradeSelect'">
                </td>
              </tr>
              <tr>
                <td class="text-right">Thickness of air duct</td>
                <td style="position: relative;">
                  <input id="airDuctply" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getAirDuctplySelect'">
					<input type="text" name="" class="ductThick" value="" style="width:180px;height:30px;margin-top: 5px;" disabled="disabled" />
					<span style="position: absolute;top: 24px;left: 200px;">mm</span>
                </td>
              </tr> -->
              <tr>
                <td class="text-right">Shell thickness</td>
                <td>
                <input id="jikebanhou" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl2.do?name='+'getjikebanhou'">
              </td>
              </tr>
              <tr class="jike">
                <td class="text-right">Chassis type</td>
                <td>
                  <!-- <select id="jike" class="easyui-combobox" name="dept" style="width:185px;">   
                    <option>轴流</option>   
                    <option>离心</option>
                </select> -->
                <input id="jike" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="">
              </td>
              </tr>
              <!-- <tr>
                <td class="text-right">Air duct type</td>
                <td>
                  <input id="airDuctType" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getAirDuctTypeSelect'">
                </td>
              </tr> -->
              <tr id="fjazxs1">
                <td class="text-right">Installation Form of Fan</td>
                <td>
                  <input id="fjazxs" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl2.do?name='+'getanzhuangxs'">
              </td>
              </tr>
              <tr>
                <td class="text-right">Import protection net</td>
                <td>
                  <input id="jkfhw" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getjinkoufhw'">
              </td>
              </tr>
              <tr>
                <td class="text-right">Export protection net</td>
                <td>
                  <!-- <select id="ckfhw" class="easyui-combobox" name="dept" style="width:185px;">   
                    <option>有</option>   
                    <option>无</option>
                </select> -->
                <input id="ckfhw" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getchukoufhw'">
              </td>
              </tr>
              <tr>
                <td class="text-right">Space heater</td>
                <td>
                  <input id="spaceHeater" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getSpaceHeaterSelect'">
                </td>
              </tr>
              <!-- <tr>
                <td class="text-right">Ventilation type</td>
                <td>
                   <input id="aerationType" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getAerationTypeSelect'">
                </td>
              </tr> -->
              <!-- <tr>
                <td class="text-right">Protective net</td>
                <td>
                  <input id="protectiveScreening" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getProtectiveScreeningSelect'">
                </td>
              </tr>
              <tr>
                <td class="text-right">Authentication</td>
                <td>
                  <input id="authentication" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text',url:'/develop/url/getUrl.do?name='+'getAuthenticationSelect'">
                </td>
              </tr>
              <tr>
                <td class="text-right">Space heater</td>
                <td>
                  <input id="spaceHeater" class="easyui-combobox" style="width:185px;height:32px;"
                            data-options="valueField:'id',textField:'text_en',url:'/develop/url/getUrl2.do?name='+'getSpaceHeaterSelect'">
                </td>
              </tr> -->
              <tr>
                  <td colspan="2" style="text-align:center;">
                      <a id='btnQuery' href="#" class="easyui-linkbutton" plain="true" iconCls="fa fa-search" onclick="functionQuery()">Search</a>          
                      <a id='btnClear' href="#" class="easyui-linkbutton"  plain="true" iconCls="fa fa-refresh" onclick="functionClear()" style="margin: 15px 0 15px 20px;">Reset</a> 
                  </td>
                </tr>
            </table>     
    </div>
    <div data-options="region:'center',iconCls:'icon-reload',title:'',split:true" style="width:70%;height: 100%">
        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                  rownumbers:true,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:true,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:true,
                  toolbar:'#tb',
                  pageSize:20,
                  onDblClickRow:onDblClickRow">
            <thead>
              <tr href="#">
                         
                <th field="id"  hidden ="true">uuid</th>
                <th field="productNumber">Fan number</th>
                <th field="text">Name of fan</th>
              </tr>
            </thead>
          </table>    
       </div>
    </div>
</body> 
</html>
<script type="text/javascript">
/*
  var obj = {"total":5,"rows":[{id:"1",fannumber:"11111",name:"轴流1号风机",model:"GZ-30",fanType:"轴流",blowingRate:"18000",totalHead:"3001",staticPressure:"",productiveness:"70"},
	  {id:"2",fannumber:"22222",name:"轴流2号风机",model:"GZ-45",fanType:"轴流",blowingRate:"18100",totalHead:"3215",staticPressure:"",productiveness:"79"},
	  {id:"3",fannumber:"33333",name:"离心风机1号",model:"CZ-12",fanType:"离心",blowingRate:"18500",totalHead:"3356",staticPressure:"",productiveness:"36"},
	  {id:"4",fannumber:"44444",name:"离心风机2号",model:"CZ-13",fanType:"离心",blowingRate:"21300",totalHead:"3412",staticPressure:"",productiveness:"54"},
	  {id:"5",fannumber:"55555",name:"排气风机",model:"CZ-14",fanType:"离心",blowingRate:"165000",totalHead:"3607",staticPressure:"",productiveness:"63"}]};*/
  $(function(){
	  // 获取机壳类型
    var fanData;
    $.ajax({
      url: '/develop/url/getUrl2.do?name='+ 'getjikeleixing',
      type: 'get',
      dataType: 'json',
      data: {},
      success: function(data) {
        console.log(data)
        fanData = data
      }
    })
    // 获取风机安装形式
    var fanlist;
    $.ajax({
      url: '/develop/url/getUrl.do?name='+ 'getanzhuangxs',
      type: 'get',
      dataType: 'json',
      data: {},
      success: function(data) {
        console.log(data)
        fanlist = data
      }
    })
    // 风机类型类型为 轴流/离心
    $('#fanType').combobox({
      onChange: function (n,o){
        var jikeValue = $('#fanType').val();
        console.log(jikeValue)
        if (jikeValue == 2) { // 离心
          $('#jike').combobox({
            valueField:'id',
            textField:'text_en',
            data: fanData.slice(3,5)
          })
          $('#fjazxs').combobox({ // 风机安装形式
            valueField:'id',
            textField:'text',
            data: fanlist
          })
        } else {
          $('#jike').combobox({
            valueField:'id',    
            textField:'text_en',
            data: fanData.slice(0,3)
          })
          $('#fjazxs').combobox({
            valueField:'id',
            textField:'text',
            data: []
          })
        }
      }
    })
//	  风筒板厚为 自定义时  可编辑
	  $('#fthb').combobox({
	  	onChange: function (n,o){
	  		var fthb =$('#fthb').val();
	  		if(fthb == 'zdy'){
	  			$('.ductThick').attr("disabled",false)
	  		} if(fthb !== 'zdy'){
	  			$('.ductThick').attr("disabled","disabled")
	  		}
	  	}
	  })
  });
 
  function addParameter()
  {

      var row = $('#dg').treegrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('请选择要添加参数的风机');
        return;
      }

      layer.open({
          type: 2,
          title: '新增参数',
          shadeClose: false,
          shade: 0.3,
          maxmin: false, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '',
          content: 'addParameter.jsp?fanId='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function viewCurveGraph()
  {
      var row = $('#dg').datagrid('getSelected');  
        if(row == null)
        {
          layerMsgCustom('必须选择一个风机');
          return;
        }

        layer.open({
            type: 2,
            title: '查看性能曲线',
            shadeClose: false,
            shade: 0.3,
            maxmin: false, //开启最大化最小化按钮
            area: shuoheUtil.cpm_size(),
            content: 'performancePurve.jsp?fanId='+row.id
        })
  }
  
  function uploadFanFile(){
	  var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('请选择风机');
        return;
      }

      layer.open({
          type: 2,
          title: '风机附件上传',
          shadeClose: false,
          shade: 0.3,
          maxmin: false, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'uploadFanFile.jsp?fanId='+row.id
      })
  }

	function customParam(){
		layer.open({
	        type: 2,
	        title: '自定义参数',
	        shadeClose: false,
	        shade: 0.3,
	        maxmin: false, //开启最大化最小化按钮
	        area: shuoheUtil.cpm_size(),
	        //btn: '保存',
	        content: 'parameter.jsp'
	    })
	}
  function edit()
  {

      var row = $('#dg').datagrid('getSelected');  
      if(row == null)
      {
        layerMsgCustom('必须选择一个目录');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '保存',
          content: 'form/editUrl.jsp?id='+row.id,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  function dele() {
    var row = $('#dg').datagrid('getSelected');
    if (row == null) {
      layerMsgCustom('必须选择一个目录');
      return;
    }

    layer.msg('你确定删除么？', {
      time: 0, //不自动关闭
      btn: ['确定', '取消'],
      yes: function(index) {
        layer.close(index);
        shuoheUtil.layerTopMaskOn();
        $.ajax({
          url: "/develop/url/delete.do",
          type: "POST",
          dataType: 'json',
          data: {
            'id': row.id
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            shuoheUtil.layerTopMaskOff();
            if (data.result == true) {
              shuoheUtil.layerMsgSaveOK(data.describe);
            } else {
              shuoheUtil.layerMsgCustom(data.describe);
            }
            $('#dg').datagrid('reload');
          }
        });
      }
    });
  }


  function updateSuccess()
  {
    $('#dg').datagrid('reload'); 

  }
  function run()
  {
      var row = $('#dg').datagrid('getSelected');  
      var json_str = JSON.stringify(row);
      if(row == null)
      {
        layerMsgCustom('必须选择一个URL');
        return;
      }

      layer.open({
          type: 2,
          title: '修改目录',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          content: 'form/runUrl.jsp?name='+row.name        
      })
  }
  //执行风机选型方法进行风机选型
  var queryX,queryY;
  var fanSelectParam;
  function functionQuery(){
	  fanSelectParam = [];
	  var objParam = new Object();
	  var state = $('#state').combobox('getValue');
	  var val = $('input[name="windPressure"]:checked').val();//获取静压风压选中的值
	  var blowingRate = $('#blowingRate').textbox('getValue');
	  //var staticPressureQuery = $('#staticPressureQuery').textbox('getValue');
	  var totalHeadQuery = $('#totalHeadQuery').textbox('getValue');
	  var errorMinRange = $('#errorMinRange').textbox('getValue');
	  var errorMaxRange = $('#errorMaxRange').textbox('getValue');
	  var speedQuery = $('#speedQuery').textbox('getValue');//风机实际转速
	  var airDensityQuery = $('#airDensityQuery').textbox('getValue');//风机实际工作环境密度
	  var fanType = $('#fanType').combobox('getText');
	  // var reviseFactor = $('#reviseFactor').combobox('getText');

    var drivingMode = $('#drivingMode').combobox('getText'); // 传动方式
    console.log(drivingMode)
    var jikebanhou = $('#jikebanhou').combobox('getText'); // 机壳板厚
    var jikeType = $('#jike').combobox('getText'); // 机壳类型
    var fjazxs = $('#fjazxs').combobox('getText'); // 风机安装形式
    var ckfhw = $('#ckfhw').combobox('getText'); // 出口防护网
    var jkfhw = $('#jkfhw').combobox('getText'); // 进口防护网

	  var ifExplosionProof = $('#ifExplosionProof').combobox('getText');
	  var voltage = $('#voltage').combobox('getText');
	  var frequency = $('#frequency').combobox('getText');
	  var levelsofprotection = $('#levelsofprotection').combobox('getText');
	  // var insulationGrade = $('#insulationGrade').combobox('getText');
	  // var airDuctply = $('#airDuctply').combobox('getText');
	  // var airDuctType = $('#airDuctType').combobox('getText');
	  // var aerationType = $('#aerationType').combobox('getText');
	  // var protectiveScreening = $('#protectiveScreening').combobox('getText');
	  // var authentication = $('#authentication').combobox('getText');
	  var spaceHeater = $('#spaceHeater').combobox('getText');
	  if(blowingRate==""){
		  shuoheUtil.layerMsgCustom('风量必须填写');
		  return;
	  }else{
		  var obj = {};
		  obj.key = "风量";
		  obj.value = blowingRate;
		  fanSelectParam.push(obj);
	  }
	  if(totalHeadQuery==""){
		  shuoheUtil.layerMsgCustom('风压必须填写');
		  return;
	  }else{
		  var obj = {};
		  obj.key = val;
		  obj.value = totalHeadQuery;
		  fanSelectParam.push(obj);
	  }
	  if(errorMinRange==""){
		  shuoheUtil.layerMsgCustom('误差最小范围必须设置');  
		  return;
	  }
	  if(errorMaxRange==""){
		  shuoheUtil.layerMsgCustom('误差最大范围必须设置');  
		  return;
	  }else{
		  var obj = {};
		  obj.key = "误差范围";
		  obj.value = errorMinRange+"~"+errorMaxRange; 
		  fanSelectParam.push(obj);
	  }
	  if(airDensityQuery==""){
		  shuoheUtil.layerMsgCustom('风机密度必须输入');  
		  return;
	  }else{
		  var obj = {};
		  obj.key = "风机密度";
		  obj.value = airDensityQuery;
		  fanSelectParam.push(obj);
	  }
	  if(speedQuery==""){
		  shuoheUtil.layerMsgCustom('风机实际要求转速必须输入');  
		  return;
	  }else{
		  var obj = {};
		  obj.key = "风机转速";
		  obj.value = speedQuery;
		  fanSelectParam.push(obj);
	  }
	  objParam.state = state;
	  objParam.selectWindPresssure = val;
	  objParam.blowingRate = blowingRate;
	  //objParam.staticPressureQuery = staticPressureQuery;
	  objParam.totalHeadQuery = totalHeadQuery;
	  queryX = blowingRate;
	  queryY = totalHeadQuery;
	  objParam.errorRange = errorMinRange+$('#rangeIdentification').text()+errorMaxRange;
	  objParam.airDensityQuery = airDensityQuery;
	  objParam.speedQuery = speedQuery;
    objParam.jikebanhou = jikebanhou; // 机壳板厚
    objParam.jikeleixing = jikeType; // 机壳类型
    objParam.fengjianzhuangxingshi = fjazxs; // 风机安装形式
    objParam.chukoufanghuwang = ckfhw; // 出口防护网
    objParam.jinkoufanghuwang = jkfhw; // 进口防护网
	  objParam.fanType = fanType;
	  // objParam.reviseFactor = reviseFactor;
	  objParam.ifExplosionProof = ifExplosionProof;
	  objParam.voltage = voltage;
	  objParam.frequency = frequency;
	  objParam.levelsofprotection = levelsofprotection;
	  // objParam.insulationGrade = insulationGrade;
	  // objParam.airDuctply = airDuctply;
	  // objParam.airDuctType = airDuctType;
	  // objParam.aerationType = aerationType;
	  // objParam.protectiveScreening = protectiveScreening;
	  // objParam.authentication = authentication;
	  objParam.spaceHeater = spaceHeater;
	  $.ajax({
          url: "/shop/sort/getFanSelectListEn.do",
          // url: "/shop/sort/getFanSelectList.do",
          type: "POST",
          dataType: 'json',
          data: {
            'paramJson': JSON.stringify(objParam)
          },
          error: function() //失败
          {
            shuoheUtil.layerTopMaskOff();
          },
          success: function(data) //成功
          {
            console.log(data)
            if(data.result){
                if(data.object.total==0){
                	shuoheUtil.layerMsgCustom('Sorry, there is no fan that meets your requirements');
                	$('#dg').datagrid('loadData',{total:0,rows:[]});
                }else{
                	shuoheUtil.layerMsgCustom('There are all fans that meet your requirements:'+data.object.total+"");
                	$('#dg').datagrid('loadData',data.object.rows);
                }
            	
            }else{
            	alert("Exception, failed to get data");
            }
          }
        });
  }
  var fanNumber;
  function onDblClickRow(rowIndex, rowData){
	  fanNumber = rowData.productNumber;
	  layer.open({
          type: 2,
          title: 'Fan parameters',
          shadeClose: false,
          shade: 0.3,
          maxmin: true, //开启最大化最小化按钮
          area: shuoheUtil.cpm_size(),
          btn: '',
          content: 'showParam.jsp?fanId='+rowData.id+'&queryX='+queryX+'&queryY='+queryY,
          yes:function(index){
            var ifname="layui-layer-iframe"+index//获得layer层的名字
              var Ifame=window.frames[ifname]//得到框架
            Ifame.update();
          }
      })
  }
  //清空搜索框中的值
  function functionClear(){
	$('#blowingRate').textbox('clear');
	//var staticPressureQuery = $('#staticPressureQuery').textbox('clear');
	$('#totalHeadQuery').textbox('clear');
	$('#errorMinRange').textbox('clear');
	$('#errorMaxRange').textbox('clear');
	$('#airDensityQuery').textbox('clear');
	$('#fanType').combobox('clear');
	// $('#reviseFactor').combobox('clear');
	$('#ifExplosionProof').combobox('clear');
	$('#voltage').combobox('clear');
	$('#frequency').combobox('clear');
	$('#levelsofprotection').combobox('clear');
	// $('#insulationGrade').combobox('clear');
	$('#airDuctply').combobox('clear');
	$('#airDuctType').combobox('clear');
	$('#aerationType').combobox('clear');
	$('#protectiveScreening').combobox('clear');
	$('#authentication').combobox('clear');
	$('#spaceHeater').combobox('clear');
  }

</script>
