<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.shuohe.entity.system.user.User"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
	String uuid = UUID.randomUUID().toString();
	User user = null;
	String userJson = null;
	try
	{
	    user = (User)session.getAttribute("user");
	    if(user==null)
	    {
	      System.out.println("user = null");
	      String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    }
	    else
	    {
	        userJson = Json.toJsonByPretty(user);
	    }
	}
	catch(Exception e)
	{
	    String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
	    e.printStackTrace();
	}
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>新增风机参数</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>
    <link type="text/css" href="/pages/css/add-form.css" rel="stylesheet"/>  
    <!-- 文件上传 -->
    <link href="<%=basePath%>/pages/fileUpload/css/iconfont.css" rel="stylesheet" type="text/css"/>
    <link href="<%=basePath%>/pages/fileUpload/css/fileUpload.css" rel="stylesheet" type="text/css">  
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <script type="text/javascript" src="/pages/js/base-loading-en.js"></script>
	<script type="text/javascript" src="/pages/js/jeDate/jedate.min.js"></script>
	<script type="text/javascript" src="/pages/js/add-form.js"></script>
	<script type="text/javascript" src="/pages/js/echarts.min.js"></script>
	<!-- echarts导出pdf所需js -->
	<script type="text/javascript" src="/pages/js/html2canvas.min.js"></script>
	<script type="text/javascript" src="/pages/js/jspdf.min.js"></script>
	
	<!--引入文件上传JS-->
	<script type="text/javascript" src="<%=basePath%>/pages/fileUpload/js/fileUpload.js"></script>
	<script type="text/javascript" src="<%=basePath%>/pages/js/shuoheUtil.js"></script>
	
    
<style type="text/css">
html, body{ margin:0; height:100%;width: 100%}
.fileUp-con{
float: left;
width: 25%;
}
.fileUp-title{
    text-align: center;
    line-height: 2;
    font-size: 16px;
}
.datagrid-body{
	overflow: hidden;
}
.field-form-line label{
    width: 200px;
}
.formThreeColumn .form-order-line label{
    width: 180px;
}
@media screen and (max-width: 1200px) {
	.formThreeColumn .form-order-line{
		width: 50%;
	}
}
@media screen and (min-width: 1201px) and (max-width: 1500px) {
	.formThreeColumn .form-order-line{
		width: 33.33%;
	}
}
@media screen and (min-width: 1501px) {
	.formThreeColumn .form-order-line{
		width: 25%;
	}
}
</style>
<body class="easyui-layout">
 <div class="easyui-tabs" style="height: 100%;overflow-x:hidden">
   <div title="Performance parameters" data-options="region:'center',closable:false" style="height: 100%;width: 100%;overflow-x:hidden">
    <table id="dg" style="height: 100%;width: 100%;"></table>
   </div>
   <div class="field-con" title="Other parameters" data-options="closable:false" style="margin-left: 10px;overflow-x:hidden">
   	<div id="fieldCon">
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Fan type</label>
                <input type="text" id="fengjileixing_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Correction factor</label>
                <input type="text" id="xiuzhengyinsu_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Transmission mode</label>
                <input type="text" id="chuandonfangshi_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Is explosion proof</label>
                <input type="text" id="shifufangbao_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Voltage</label>
                <input type="text" id="dianya" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Frequency</label>
                <input type="text" id="pinlv" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Protection level</label>
                <input type="text" id="fanghudengji" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Insulation level</label>
                <input type="text" id="jueyuandengji" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Shell thickness</label>
                <input type="text" id="jikebanhou" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Cylinder Plate Thickness</label>
                <input type="text" id="fengtongbanhou_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Chassis type</label>
                <input type="text" id="jikeType" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Air duct type</label>
                <input type="text" id="fengtongleixing_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Installation Form of Fan</label>
                <input type="text" id="fenjianzhuangxs" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Ventilation type</label>
                <input type="text" id="huanqileixing_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Import protection net</label>
                <input type="text" id="jinkoufanghu_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <!--<div class="form-order-line">
                <label>Protective net</label>
                <input type="text" id="fanghuwang_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>-->
            <div class="form-order-line">
                <label>Export protection net</label>
                <input type="text" id="chukoufanghu_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <!--<div class="formThreeColumn">
            <div class="form-order-line">
                <label>Authentication</label>
                <input type="text" id="renzheng" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>-->
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Space heater</label>
                <input type="text" id="kongjianjiareqi_en" class="easyui-combobox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Density</label>
                <input type="text" id="midu" class="easyui-textbox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Speed</label>
                <input type="text" id="zhuansu" class="easyui-textbox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Machine number</label>
                <input type="text" id="jihao" class="easyui-textbox" data-options="height:'32px'" />
            </div>
        </div>
        <div class="formThreeColumn">
            <div class="form-order-line">
                <label>Impeller diameter</label>
                <input type="text" id="yelunzhijing" class="easyui-textbox" data-options="height:'32px'" />
            </div>
        </div>
    </div>
   </div>
   <!-- 作为pdf导出的form -->
   <form id="chartForm" style="display:none">
    <input id="imageValue" name="base64Info" type="text" maxlength="500000"/>
    <input id="imageValue2" name="base64Info2" type="text" maxlength="500000"/>
    <input id="fanSelectParam" name="fanSelectParam" type="text" maxlength="500"/>
    <input id="fanId" name="fanId" type="text"/>
    <input id="fanNumber" name="fanNumber" type="text"/>
   </form>
   <div title="Performance curve" style="position: relative;overflow-x:hidden">
	   <div style="position: absolute;right: 50px;top: 13px;z-index: 9999;">
	      <button onclick="convertCanvasToImage()" class="button button-primary button-rounded button-small" style="width: 150px;height: 35px;font-size: 20px;">Export pdf</button>
	   </div>
	<div id="main" style="width:600px;height:320px;margin-top: 50px;"></div>
	<div id="main3" style="width:600px;height:320px;margin-top: 50px;"></div>
	<div id="main1" style="width:600px;height:320px;margin-top: 50px;"></div>
    <div id="main2" style="width:600px;height:320px;margin-top: 50px;"></div>
    <div id="main4" style="width:600px;height:320px;margin-top: 50px;"></div>
	<div class="field-form-line">
		<label>Air Flow Rate：</label><input type="text" id="xData" class="easyui-textbox" data-options="width:278">
	</div>
	<div class="field-form-line">
		<label>Efficiency：</label><input type="text" id="yData1" class="easyui-textbox" data-options="width:278">
	</div>
	<div class="field-form-line">
		<label>Internal power：</label><input type="text" id="yData2" class="easyui-textbox" data-options="width:278">
	</div>
	<div class="field-form-line">
		<label>Total pressure：</label><input type="text" id="yData3" class="easyui-textbox" data-options="width:278">
	</div>
   </div>
   <div title="Outline drawing" style="height:100%">
    <div class="fielImg">
		<img src="" alt="" id="processImg">
	</div>
	  <table id="dg_file" class='easyui-datagrid' data-options="
                  rownumbers:false,
                  singleSelect:true,
                  autoRowHeight:false,
                  pagination:false,
                  fitColumns:false,
                  striped:true,
                  checkOnSelect:true,
                  selectOnCheck:true,
                  collapsible:false, 
                  toolbar:'',
                  pageSize:20">
            <thead>
              <tr href="#">
                    <th data-options="field:'ck',checkbox:false"></th>
                    <th field="id" align="center" hidden ="true">id</th>
                    <th field="filePath" align="center" hidden ="true">id</th>
                    <!--<th field="fileName" align="center" width="19.5%">File name</th>
                    <th field="fileType" align="center" width="19.5%">File type</th>-->
                    <th field="uploadTime" width="19.5%">Upload time</th>
                    <th field="handle" width="19.5%" formatter="downloadFile">Operation</th>
              </tr>
            </thead>
          </table>
   </div>
   <div title="Single machine noise" style="height:100%">
	  <div id="singleMachineNoise" style="width:1200px;height:320px;margin-top: 50px;"></div>
   </div>
 </div>
</body> 
</html>
<script type="text/javascript">
var fanId = shuoheUtil.getUrlHeader().fanId;
var queryX = shuoheUtil.getUrlHeader().queryX;
var queryY = shuoheUtil.getUrlHeader().queryY;
var fanSelectParam = shuoheUtil.getUrlHeader().fanSelectParam;
var user = <%=userJson%>;
var uuid = '<%=uuid%>'
var formObj
var fields = []
var datas
var noiseData
var hasParam = false;
var result;//接收其他参数返回的数据
var formJson = ''
var title = 'shop_fan_other_param'
var basePath = '<%=basePath%>'
$(function(){
	$("#processImg").attr("src","/fanFile/preview.do?fan_number="+fanId);
	$('#main').css('width',document.body.clientWidth + 'px');
	$('#main3').css('width',document.body.clientWidth + 'px');
	$('#main1').css('width',document.body.clientWidth + 'px');
    $('#main2').css('width',document.body.clientWidth + 'px');
    $('#main4').css('width',document.body.clientWidth + 'px');
    $('#singleMachineNoise').css('width',document.body.clientWidth + 'px');
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      formObj = data
      fields = formObj.field
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
      //initFormOrder(formObj,uuid)
    }
  })
  // 噪音特性
  $.ajax({
    url: "<%=basePath%>/shop/product/getProductSortEntity.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
		productId: fanId
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data) // 成功
    {
	  console.log(data)
	  noiseData = data
    }
  })

  // 风机类型下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_fanTypeSelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        fengjileixingList = data.rows
        $('#fengjileixing_en').combobox({data:fengjileixingList,valueField:'id',textField:'text_en'})
    }
  })
  // 修正因素下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_reviseFactorSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         xiuzhengyinsuList = data.rows
//         $('#xiuzhengyinsu_en').combobox({data:xiuzhengyinsuList,valueField:'id',textField:'text_en'})
//     }
//   })
    // 传动方式
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_transmission'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            chuandongfsList = data.rows
            $('#chuandonfangshi_en').combobox({data:chuandongfsList,valueField:'id',textField:'text_en'})
        }
    })
    // 机壳板厚
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_shellthickness'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            jikebanhouList = data.rows
            $('#jikebanhou').combobox({data:jikebanhouList,valueField:'id',textField:'text_en'})
        }
    })
    // 机壳类型
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_chassistype'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            jikeList = data.rows
            $('#jikeType').combobox({data:jikeList,valueField:'id',textField:'text_en'})
        }
    })
    // 风机安装形式
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_installation'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            fenjianzhuangxsList = data.rows
            $('#fenjianzhuangxs').combobox({data:fenjianzhuangxsList,valueField:'id',textField:'text'})
        }
    })
    // 进口防护网
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_importprotection'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            jinkoufanghuList = data.rows
            $('#jinkoufanghu_en').combobox({data:jinkoufanghuList,valueField:'id',textField:'text_en'})
        }
    })
    // 出口防护网
    $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
        type: "POST",
        dataType:'json',
        async: false,
        data:
        {
        'tableName': 'select_exportprotection'
        },
        error: function() //失败
        {
        messageloadError()
        },
        success: function(data)//成功
        {   console.log(data)
            chukoufanghuList = data.rows
            $('#chukoufanghu_en').combobox({data:chukoufanghuList,valueField:'id',textField:'text_en'})
        }
    })


  // 是否防爆下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_ifExplosionProofSelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        shifufangbaoList = data.rows
        $('#shifufangbao_en').combobox({data:shifufangbaoList,valueField:'id',textField:'text_en'})
    }
  })
  // 电压下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_voltageSelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        dianyaList = data.rows
        $('#dianya').combobox({data:dianyaList,valueField:'id',textField:'text'})
    }
  })
  // 频率下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_frequencySelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        pinlvList = data.rows
        $('#pinlv').combobox({data:pinlvList,valueField:'id',textField:'text'})
    }
  })
  // 防护等级下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_levelsOfProtectionSelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        fanghudengjiList = data.rows
        $('#fanghudengji').combobox({data:fanghudengjiList,valueField:'id',textField:'text'})
    }
  })
  // 绝缘等级下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_insulationGradeSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         jueyuandengjiList = data.rows
//         $('#jueyuandengji').combobox({data:jueyuandengjiList,valueField:'id',textField:'text'})
//     }
//   })
  // 风筒板厚下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_airDuctPlySelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         fengtongbanhouList = data.rows
//         $('#fengtongbanhou_en').combobox({data:fengtongbanhouList,valueField:'id',textField:'text_en'})
//     }
//   })
  // 风筒类型下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_airDuctTypeSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         fengtongleixingList = data.rows
//         $('#fengtongleixing_en').combobox({data:fengtongleixingList,valueField:'id',textField:'text_en'})
//     }
//   })
  // 换气类型下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_aerationTypeSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         huanqileixingList = data.rows
//         $('#huanqileixing_en').combobox({data:huanqileixingList,valueField:'id',textField:'text_en'})
//     }
//   })
  // 防护网下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_protectiveScreeningSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         fanghuwangList = data.rows
//         $('#fanghuwang_en').combobox({data:fanghuwangList,valueField:'id',textField:'text_en'})
//     }
//   })
  // 认证下拉数据
//   $.ajax({
//     url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': 'select_authenticationSelect'
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//         renzhengList = data.rows
//         $('#renzheng').combobox({data:renzhengList,valueField:'id',textField:'text'})
//     }
//   })
  // 空间加热器下拉数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    async: false,
    data:
    {
      'tableName': 'select_spaceHeaterSelect'
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
        kongjianjiareqiList = data.rows
        $('#kongjianjiareqi_en').combobox({data:kongjianjiareqiList,valueField:'id',textField:'text_en'})
    }
  })
  //js渲染性能曲线图
  loadCurveChart();
  getFanOtherParam(fanId);//加载风机参数
  loadFileList();
  //渲染单机噪声图
  loadNoise();
})
function loadFileList(){
	$('#dg_file').datagrid({
	    url:'/develop/url/getUrl.do',
	    queryParams:
	    {
	      name:'getFanFileByFanId',
	      fanId:fanId
	    },
	    method:'post'
	});
}

function fielData1() {
  var temp = '';
  for (var i=0;i<fields.length;i++){
    var field = fields[i];
    console.log(field)
    if (field.fieldType == 'textbox') {
      console.log(result[field.text])
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").textbox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").textbox("setValue", "' + result[field.text] + '");'
      }
    } else {
      if (result[field.text] == null){
        temp += '$("#' + field.text + '").combobox("setValue", "");'
      } else {
        temp += '$("#' + field.text + '").combobox("setValue", "' + result[field.text] + '");'
      }
    }
  }
  var script = document.createElement("script")
  script.type = "text/javascript"
  try {
    script.appendChild(document.createTextNode(temp))
  } catch (ex) {
    script.text = temp
  }
  document.body.appendChild(script)
  specialJSFlag++
}

function getFanOtherParam(fanId){
	$.ajax({
        url: "/capabilityParam/getDataByFanId",
        type: "POST",
        dataType:'json',
        async:false,
        data:
        {
          'tableName': title,
          'fanId':fanId
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
        	if(!data.rows.length==0){
        		hasParam = true;
        		result = data.rows[0];
                console.log(result)
                fielData1()
        		// initFieldData(true);
                $("#fengjileixing_en").combobox("setValue", result.fengjileixing_en);
        		// $("#xiuzhengyinsu_en").combobox("setValue", result.xiuzhengyinsu_en);
                $("#jikebanhou").combobox("setValue", result.jikebanhou_en);
                $("#jikeType").combobox("setValue", result.jikeleixing_en);
                $("#fenjianzhuangxs").combobox("setValue", result.fengjianzhuangxingshi);
                $("#jinkoufanghu_en").combobox("setValue", result.jinkoufanghuwang_en);
                $("#chukoufanghu_en").combobox("setValue", result.chukoufanghuwang_en);
        		$("#shifufangbao_en").combobox("setValue", result.shifufangbao_en);
        		// $("#fengtongbanhou_en").combobox("setValue", result.fengtongbanhou_en);
        		// $("#fengtongleixing_en").combobox("setValue", result.fengtongleixing_en);
        		// $("#huanqileixing_en").combobox("setValue", result.huanqileixing_en);
        		// $("#fanghuwang_en").combobox("setValue", result.fanghuwang_en);
        		$("#kongjianjiareqi_en").combobox("setValue", result.kongjianjiareqi_en);
        	}
          
        }
      })
}
$('#dg').datagrid({
	url:'/develop/url/getUrl.do',
    queryParams:
    {
    	'name':'getFanCapabilityParam',
    	'fanNumber':parent.fanNumber
    },
    remoteSort: false, // 点击排序
	// sortName : 'soundLevel',
	sortOrder : 'desc',
    columns:[[
		{field:'flux',title:'Air Flow Rate<br>Q[m&sup3/h]',align:'center',editor:'textbox',sortable: true},
		{field:'totalHead',title:'Total pressure<br>P[Pa]',align:'center',editor:'textbox',sortable: true},
		{field:'staticPressure',title:'Static pressure<br>P<sub>st</sub>[Pa]',align:'center',editor:'textbox',sortable: true},
		{field:'speed',title:'Rotation speed<br>n[rpm]',align:'center',editor:'textbox',sortable: true},
		{field:'internalPower',title:'Shaft power<br>P<sub>z</sub>[kw]',align:'center',editor:'textbox',sortable: true},
		{field:'tProductiveness',title:'Fan Total efficiency<br>η<sub>t</sub>[%]',align:'center',editor:'textbox',sortable: true},
		{field:'sProductiveness',title:'Fan Static efficiency<br>η<sub>st</sub>[%]',align:'center',editor:'textbox',sortable: true},
		{field:'noise',title:'Noise<br>L&supA[dB(A)]',align:'center',editor:'textbox',sortable: true},
		{field:'soundLevel',title:'Lsa<br>Lsa[dB(A)]',align:'center',editor:'textbox',sortable: true}
		
    ]],
    pagination:true,
	toolbar:''//,
	//onClickRow: onClickRow
});
var efficiencyCurveFormula = '', internalPowerCurveFormula = '', pressureCurveFormula = ''
var reg = new RegExp("a","g")
function loadCurveChart(){
	var pressure;
	var staticPressure;
	var productiveness;
    var power;
    var noise;
	$.ajax({
        url: "/productMix/getEchartsData", 
        type: "POST",
        dataType: 'json',
        async: false,
        data: {
        	'fanId':fanId,
        	'fanNumber':parent.fanNumber
        },
        error: function() //失败
        {
        	shuoheUtil.layerMsgSaveOK('Exception, cannot be saved');
        },
        success: function(data)//成功
        {
        	console.log(data);
        	pressure = data.pressureData;
        	staticPressure = data.pressureStaticData;
        	productiveness = data.productivenessData;
            power = data.powerData;
            noise = data.noiseData;
        	efficiencyCurveFormula = data.fanModel.efficiencyCurveFormula
        	efficiencyCurveFormula = efficiencyCurveFormula.substring(efficiencyCurveFormula.indexOf('return')+7,efficiencyCurveFormula.length-1)
        	internalPowerCurveFormula = data.fanModel.internalPowerCurveFormula
        	internalPowerCurveFormula = internalPowerCurveFormula.substring(internalPowerCurveFormula.indexOf('return')+7,internalPowerCurveFormula.length-1)
        	pressureCurveFormula = data.fanModel.pressureCurveFormula
        	pressureCurveFormula = pressureCurveFormula.substring(pressureCurveFormula.indexOf('return')+7,pressureCurveFormula.length-1)
        }
    });
	var myChart = echarts.init(document.getElementById('main'))
	var option = {
	backgroundColor: 'white',
	tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    //animation:false,
    legend: {
        data:['Total Pressure curve']
    },
    xAxis: {
        type: 'value',
        name:'Flow(m3/h)',
        min: function(value) {
            return value.min - 20;
        },
        max: function(value) {
            return value.max + 20;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
					var temp3 = pressureCurveFormula.replace(reg,e.value)
					console.log(temp2)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	{
	        type: 'value',
	        name:'pressure(pa)',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'Total Pressure curve',
            type:'line',
            smooth: true,
            data:pressure
        },
        {
        	name:'Operating point',
        	type:'line',
        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
        	symbolSize: 50,
        	label: {
	        	show: true,
	        	formatter: '{c}',
	        	fontSize: 18
        	},
        	data: [[queryX, queryY]]
        }
    ]
}
	myChart.setOption(option)
	//静压曲线
	var myChart3 = echarts.init(document.getElementById('main3'))
	var option3 = {
		backgroundColor: 'white',
		tooltip: {
	        trigger: 'item',
	        show: true,
	        axisPointer: {
	            snap: false,
	            type: 'cross'
	        }
	    },
	    //animation:false,
	    legend: {
	        data:['Static Pressure curve']
	    },
	    xAxis: {
	        type: 'value',
	        name:'Flow(m3/h)',
	        axisPointer: {
	            show: true,
	            snap: false,
	            label: {
	                show: true,
	                formatter: function (e) {
						var temp1 = efficiencyCurveFormula.replace(reg,e.value)
						var temp2 = internalPowerCurveFormula.replace(reg,e.value)
						var temp3 = pressureCurveFormula.replace(reg,e.value)
						console.log(temp2)
						$('#xData').textbox('setValue', e.value)
						$('#yData1').textbox('setValue', eval(temp1))
						$('#yData2').textbox('setValue', eval(temp2))
						$('#yData3').textbox('setValue', eval(temp3))
	                    return e.value
	                }
	            }
	        }
	    },
	    yAxis: [
	    	{
		        type: 'value',
		        name:'pressure(pa)',
				axisPointer: {
					show: false
				}
		    }
	    ],
	    series: [
	        {
	            name:'Static Pressure curve',
	            type:'line',
	            smooth: true,
	            data:staticPressure
	        },
	        {
	        	name:'Operating point',
	        	type:'line',
	        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
	        	symbolSize: 50,
	        	label: {
		        	show: true,
		        	formatter: '{c}',
		        	fontSize: 18
	        	},
	        	data: [[queryX, queryY]]
	        }
	    ]
	}
	myChart3.setOption(option3)
	
	var myChart1 = echarts.init(document.getElementById('main1'))
	var option1 = {
	backgroundColor: 'white',
	tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    legend: {
        data:['Efficiency curve']
    },
    xAxis: {
        type: 'value',
        name:'Flow(m3/h)',
        min: function(value) {
            return value.min - 20;
        },
        max: function(value) {
            return value.max + 20;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
					var temp3 = pressureCurveFormula.replace(reg,e.value)
					console.log(temp2)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	
	    {
	        type: 'value',
	        name:'efficiency(%)',
	        min: 0,
	        max: 100,
	        position: 'right',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'Efficiency curve',
            type:'line',
            smooth: true,
            data:productiveness
        }
    ]
}
	myChart1.setOption(option1)
	
	var myChart2 = echarts.init(document.getElementById('main2'))
	var option2 = {
	backgroundColor: 'white',
	tooltip: {
        trigger: 'item',
        show: true,
        axisPointer: {
            snap: false,
            type: 'cross'
        }
    },
    legend: {
        data:['Power curve']
    },
    xAxis: {
        type: 'value',
        name:'Flow(m3/h)',
        min: function(value) {
            return value.min - 20;
        },
        max: function(value) {
            return value.max + 20;
        },
        axisPointer: {
            show: true,
            snap: false,
            label: {
                show: true,
                formatter: function (e) {
					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
					var temp3 = pressureCurveFormula.replace(reg,e.value)
					$('#xData').textbox('setValue', e.value)
					$('#yData1').textbox('setValue', eval(temp1))
					$('#yData2').textbox('setValue', eval(temp2))
					$('#yData3').textbox('setValue', eval(temp3))
                    return e.value
                }
            }
        }
    },
    yAxis: [
    	{
	        type: 'value',
	        name:'power',
			axisPointer: {
				show: false
			}
	    }
    ],
    series: [
        {
            name:'Power curve',
            type:'line',
            smooth: true,
            data:power
        }
    ]
}
    myChart2.setOption(option2)
    

    var myChart4 = echarts.init(document.getElementById('main4'))
	var option4 = {
		backgroundColor: 'white',
		tooltip: {
	        trigger: 'item',
	        show: true,
	        axisPointer: {
	            snap: false,
	            type: 'cross'
	        }
	    },
	    legend: {
	        data:['Noise curve']
	    },
	    xAxis: {
	        type: 'value',
	        name:'flow(m3/h)',
	        min: function(value) {
	            return value.min - 20;
	        },
	        max: function(value) {
	            return value.max + 20;
	        },
	        axisPointer: {
	            show: true,
	            snap: false,
	            label: {
	                show: true,
	                formatter: function (e) {
						var temp1 = efficiencyCurveFormula.replace(reg,e.value)
						var temp2 = internalPowerCurveFormula.replace(reg,e.value)
						var temp3 = pressureCurveFormula.replace(reg,e.value)
						console.log(temp2)
						$('#xData').textbox('setValue', e.value)
						$('#yData1').textbox('setValue', eval(temp1))
						$('#yData2').textbox('setValue', eval(temp2))
						$('#yData3').textbox('setValue', eval(temp3))
	                    return e.value
	                }
	            }
	        }
	    },
	    yAxis: [
	    	
		    {
		        type: 'value',
		        name:'dB(A)',
		        // min: 0,
		        // max: 100,
		        // position: 'left',
				axisPointer: {
					show: false
				}
		    }
	    ],
	    series: [
	        {
	            name:'Noise curve',
	            type:'line',
	            smooth: true,
	            data:noise
	        }
	    ]
	}
	myChart4.setOption(option4)
}
//文件下载
function downloadFile(value,row,index){
	ret =  '<a href="/fanFile/downloadFile.do?fileRealPath='+row.filePath+'&fileName='+row.fileName+'" class="easyui-linkbutton">Download</a>';   
    return ret;  
}

function convertCanvasToImage() {
	var thisChart = echarts.init(document.getElementById('main'));
	var thisChart3 = echarts.init(document.getElementById('main3'));
	var thisChart1 = echarts.init(document.getElementById('main1'));
	var thisChart2 = echarts.init(document.getElementById('main2'));
	var chartExportUrl = '/productMix/exportPdf2'; 
	var picBase64Info = thisChart.getDataURL();//获取echarts图的base64编码，为png格式。  
	var picBase64Info3 = thisChart3.getDataURL();//静压。  
	var picBase64Info1 = thisChart1.getDataURL();//效率。
	var picBase64Info2 = thisChart2.getDataURL();//功率。
	$('#chartForm').find('input[name="base64Info"]').val(picBase64Info+"&&"+picBase64Info3+"&&"+picBase64Info1+"&&"+picBase64Info2);//将编码赋值给输入框 
	
	$('#chartForm').find('input[name="fanSelectParam"]').val(JSON.stringify(parent.fanSelectParam));
	$('#chartForm').find('input[name="fanId"]').val(fanId);//将风机id传到后台
	$('#chartForm').find('input[name="fanNumber"]').val(parent.fanNumber);//将风机编号传到后台
	$('#chartForm').attr('action',chartExportUrl).attr('method', 'post');//设置提交到的url地址
	$('#chartForm').submit();
}

function loadNoise(){
	var noiseChart = echarts.init(document.getElementById('singleMachineNoise'))
	var option = {
	    title : {
	        text: 'Single machine noise histogram'
	    },
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
	        data:['Acoustic power','Import and Export Sound Pressure Level']
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            dataView : {show: true, readOnly: false},
	            magicType : {show: true, type: ['line', 'bar']},
	            restore : {show: true},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true,
	    xAxis : [
	        {
	            type : 'category',
	            data : ['63','125','250','500','1000','2000','4000','8000']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'Acoustic power',
	            type:'bar',
	            data:[noiseData.noise63, noiseData.noise125, noiseData.noise250, noiseData.noise500, noiseData.noise1KHz, noiseData.noise2KHz, noiseData.noise4KHz, noiseData.noise8KHz]
	        },
	        {
	            name:'Import and Export Sound Pressure Level',
	            type:'bar',
	            data:[noiseData.noise63s, noiseData.noise125s, noiseData.noise250s, noiseData.noise500s, noiseData.noise1KHzs, noiseData.noise1KHzs, noiseData.noise4KHzs, noiseData.noise8KHzs]
	        }
	    ]
	};
	noiseChart.setOption(option);
}


// $(function(){
// 	 // 外形图获取
// 	$("#processImg").attr("src","/fanFile/preview.do?fan_number="+fanId);
// 	$('#main').css('width',document.body.clientWidth + 'px');
// 	$('#main3').css('width',document.body.clientWidth + 'px');
// 	$('#main1').css('width',document.body.clientWidth + 'px');
// 	$('#main2').css('width',document.body.clientWidth + 'px');
// 	$('#main4').css('width',document.body.clientWidth + 'px');
//   $.ajax({
//     url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
//       'tableName': title
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data)//成功
//     {
//       formObj = data
//       fields = formObj.field
//       for (var i = 0; i < fields.length; i++) {
//         if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
//           window[fields[i].text+'SelectFields'] = fields[i].selectFields
//         }
//       }
//       initFormOrder(formObj,uuid)
//     }
//   })

//   $.ajax({
//     url: "<%=basePath%>/shop/product/getProductSortEntity.do",
//     type: "POST",
//     dataType:'json',
//     async: false,
//     data:
//     {
// 		productId: fanId
//     },
//     error: function() //失败
//     {
//       messageloadError()
//     },
//     success: function(data) // 成功
//     {
// 	  console.log(data)
// 	  noiseData = data
//     }
//   })

//   //js渲染性能曲线图
//   loadCurveChart();
//   getFanOtherParam(fanId); // 加载风机参数
//   loadFileList();
//   //渲染单机噪声图
//   loadNoise();
// })


// function fielData1() {
//   var temp = '';
//   for (var i=0;i<fields.length;i++){
//     var field = fields[i];
//     console.log(field)
//     if (field.fieldType == 'textbox') {
//       console.log(result[field.text])
//       if (result[field.text] == null){
//         temp += '$("#' + field.text + '").textbox("setValue", "");'
//       } else {
//         temp += '$("#' + field.text + '").textbox("setValue", "' + result[field.text] + '");'
//       }
//     } else {
//       if (result[field.text] == null){
//         temp += '$("#' + field.text + '").combobox("setValue", "");'
//       } else {
//         temp += '$("#' + field.text + '").combobox("setValue", "' + result[field.text] + '");'
//       }
//     }
//   }
//   var script = document.createElement("script")
//   script.type = "text/javascript"
//   try {
//     script.appendChild(document.createTextNode(temp))
//   } catch (ex) {
//     script.text = temp
//   }
//   document.body.appendChild(script)
//   specialJSFlag++
// }

// function loadFileList(){
// 	$('#dg_file').datagrid({
// 	    url:'/develop/url/getUrl.do',
// 	    queryParams:
// 	    {
// 	      name:'getFanFileByFanId',
// 	      fanId:fanId
// 	    },
// 	    method:'post'
// 	});
// }
// function getFanOtherParam(fanId){
// 	$.ajax({
//         url: "/capabilityParam/getDataByFanId",
//         type: "POST",
//         dataType:'json',
//         async:false,
//         data:
//         {
//           'tableName': title,
//           'fanId':fanId
//         },
//         error: function() //失败
//         {
//           messageloadError()
//         },
//         success: function(data)//成功
//         {
//         	if(!data.rows.length==0){
//         		hasParam = true;
//         		result = data.rows[0];
// 				// initFieldData(true);
// 				fielData1()
//         	}
          
//         }
//       })
// }
// $('#dg').datagrid({
// 	idField : 'id',
// 	url:'/develop/url/getUrl.do',
//     queryParams:
//     {
//     	'name':'getFanCapabilityParam',
//     	'fanNumber':parent.fanNumber
// 	},
// 	// rownumbers : true,
// 	remoteSort: false, // 点击排序
// 	// sortName : 'soundLevel',
// 	sortOrder : 'desc',
// 	onLoadSuccess: function (data) {
// 		console.log(data)
// 		datas = data
// 	},
//     columns:[[
// 		{field:'flux',title:'流量<br>Q[m&sup3/h]',width:'11%',align:'center',editor:'textbox',sortable: true},
// 		{field:'totalHead',title:'全压<br>P[Pa]',width:'11%',align:'center',editor:'textbox',sortable: true},
// 		{field:'staticPressure',title:'静压<br>P<sub>st</sub>[Pa]',width:'11%',align:'center',editor:'textbox',sortable: true},
// 		// {field:'speed',title:'转速<br>n[rpm]',width:'11%',align:'center',editor:'textbox'},
// 		{field:'internalPower',title:'轴功率<br>P<sub>z</sub>[kw]',width:'11%',align:'center',editor:'textbox',sortable: true},
// 		{field:'tProductiveness',title:'全压效率<br>η<sub>t</sub>[%]',width:'9%',align:'center',editor:'textbox',sortable: true},
// 		{field:'sProductiveness',title:'静压效率<br>η<sub>st</sub>[%]',width:'9%',align:'center',editor:'textbox',sortable: true},
// 		{field:'noise',title:'噪声<br>LA[dB(A)]',width:'9%',align:'center',editor:'textbox',sortable: true},
// 		{field:'soundLevel',title:'比A声级<br>Lsa[dB(A)]',width:'9%',align:'center',editor:'textbox',sortable: true}
// 	]],
//     pagination:true,
// 	toolbar:''//,
// 	//onClickRow: onClickRow
// }).datagrid('loadData', datas);

// var efficiencyCurveFormula = '', internalPowerCurveFormula = '', pressureCurveFormula = ''
// var reg = new RegExp("a","g")
// function loadCurveChart(){
// 	var pressure;
// 	var staticPressure;
// 	var productiveness;
// 	var power;
// 	$.ajax({
//         url: "/productMix/getEchartsData", 
//         type: "POST",
//         dataType: 'json',
//         async: false,
//         data: {
//         	'fanId':fanId,
//         	'fanNumber':parent.fanNumber
//         },
//         error: function() //失败
//         {
//         	shuoheUtil.layerMsgSaveOK('异常无法保存');
//         },
//         success: function(data)//成功
//         {
//         	console.log(data);
//         	pressure = data.pressureData;
//         	staticPressure = data.pressureStaticData;
//         	productiveness = data.productivenessData;
//         	power = data.powerData;
//         	efficiencyCurveFormula = data.fanModel.efficiencyCurveFormula
//         	efficiencyCurveFormula = efficiencyCurveFormula.substring(efficiencyCurveFormula.indexOf('return')+7,efficiencyCurveFormula.length-1)
//         	internalPowerCurveFormula = data.fanModel.internalPowerCurveFormula
//         	internalPowerCurveFormula = internalPowerCurveFormula.substring(internalPowerCurveFormula.indexOf('return')+7,internalPowerCurveFormula.length-1)
//         	pressureCurveFormula = data.fanModel.pressureCurveFormula
//         	pressureCurveFormula = pressureCurveFormula.substring(pressureCurveFormula.indexOf('return')+7,pressureCurveFormula.length-1)
//         }
//     });
// 	var myChart = echarts.init(document.getElementById('main'))
// 	var option = {
// 	backgroundColor: 'white',
// 	tooltip: {
//         trigger: 'item',
//         show: true,
//         axisPointer: {
//             snap: false,
//             type: 'cross'
//         }
//     },
//     //animation:false,
//     legend: {
//         data:['全压曲线']
//     },
//     xAxis: {
//         type: 'value',
//         name:'流量(m3/h)',
//         min: function(value) {
//             return value.min - 20;
//         },
//         max: function(value) {
//             return value.max + 20;
//         },
//         axisPointer: {
//             show: true,
//             snap: false,
//             label: {
//                 show: true,
//                 formatter: function (e) {
// 					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
// 					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
// 					var temp3 = pressureCurveFormula.replace(reg,e.value)
// 					console.log(temp2)
// 					$('#xData').textbox('setValue', e.value)
// 					$('#yData1').textbox('setValue', eval(temp1))
// 					$('#yData2').textbox('setValue', eval(temp2))
// 					$('#yData3').textbox('setValue', eval(temp3))
//                     return e.value
//                 }
//             }
//         }
//     },
//     yAxis: [
//     	{
// 	        type: 'value',
// 	        name:'压力(pa)',
// 			axisPointer: {
// 				show: false
// 			}
// 	    }
//     ],
//     series: [
//         {
//             name:'全压曲线',
//             type:'line',
//             smooth: true,
//             data:pressure
//         },
//         {
//         	name:'工况点',
//         	type:'line',
//         	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
//         	symbolSize: 50,
//         	label: {
// 	        	show: true,
// 	        	formatter: '{c}',
// 	        	fontSize: 18
//         	},
//         	data: [[queryX, queryY]]
//         }
//     ]
// }
// 	myChart.setOption(option)
	
// 	//静压曲线
// 	var myChart3 = echarts.init(document.getElementById('main3'))
// 	var option3 = {
// 		backgroundColor: 'white',
// 		tooltip: {
// 	        trigger: 'item',
// 	        show: true,
// 	        axisPointer: {
// 	            snap: false,
// 	            type: 'cross'
// 	        }
// 	    },
// 	    //animation:false,
// 	    legend: {
// 	        data:['静压曲线']
// 	    },
// 	    xAxis: {
// 	        type: 'value',
// 	        name:'流量(m3/h)',
// 	        axisPointer: {
// 	            show: true,
// 	            snap: false,
// 	            label: {
// 	                show: true,
// 	                formatter: function (e) {
// 						var temp1 = efficiencyCurveFormula.replace(reg,e.value)
// 						var temp2 = internalPowerCurveFormula.replace(reg,e.value)
// 						var temp3 = pressureCurveFormula.replace(reg,e.value)
// 						console.log(temp2)
// 						$('#xData').textbox('setValue', e.value)
// 						$('#yData1').textbox('setValue', eval(temp1))
// 						$('#yData2').textbox('setValue', eval(temp2))
// 						$('#yData3').textbox('setValue', eval(temp3))
// 	                    return e.value
// 	                }
// 	            }
// 	        }
// 	    },
// 	    yAxis: [
// 	    	{
// 		        type: 'value',
// 		        name:'压力(pa)',
// 				axisPointer: {
// 					show: false
// 				}
// 		    }
// 	    ],
// 	    series: [
// 	        {
// 	            name:'静压曲线',
// 	            type:'line',
// 	            smooth: true,
// 	            data:staticPressure
// 	        },
// 	        {
// 	        	name:'工况点',
// 	        	type:'line',
// 	        	symbol: 'image://<%=basePath%>/pages/images/fivePointedStar.png',
// 	        	symbolSize: 50,
// 	        	label: {
// 		        	show: true,
// 		        	formatter: '{c}',
// 		        	fontSize: 18
// 	        	},
// 	        	data: [[queryX, queryY]]
// 	        }
// 	    ]
// 	}
// 	myChart3.setOption(option3)
	
// 	var myChart1 = echarts.init(document.getElementById('main1'))
// 	var option1 = {
// 	backgroundColor: 'white',
// 	tooltip: {
//         trigger: 'item',
//         show: true,
//         axisPointer: {
//             snap: false,
//             type: 'cross'
//         }
//     },
//     legend: {
//         data:['效率曲线']
//     },
//     xAxis: {
//         type: 'value',
//         name:'流量(m3/h)',
//         min: function(value) {
//             return value.min - 20;
//         },
//         max: function(value) {
//             return value.max + 20;
//         },
//         axisPointer: {
//             show: true,
//             snap: false,
//             label: {
//                 show: true,
//                 formatter: function (e) {
// 					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
// 					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
// 					var temp3 = pressureCurveFormula.replace(reg,e.value)
// 					console.log(temp2)
// 					$('#xData').textbox('setValue', e.value)
// 					$('#yData1').textbox('setValue', eval(temp1))
// 					$('#yData2').textbox('setValue', eval(temp2))
// 					$('#yData3').textbox('setValue', eval(temp3))
//                     return e.value
//                 }
//             }
//         }
//     },
//     yAxis: [
    	
// 	    {
// 	        type: 'value',
// 	        name:'效率(%)',
// 	        min: 0,
// 	        max: 100,
// 	        position: 'right',
// 			axisPointer: {
// 				show: false
// 			}
// 	    }
//     ],
//     series: [
//         {
//             name:'效率曲线',
//             type:'line',
//             smooth: true,
//             data:productiveness
//         }
//     ]
// }
// 	myChart1.setOption(option1)
	
// 	var myChart2 = echarts.init(document.getElementById('main2'))
// 	var option2 = {
// 	backgroundColor: 'white',
// 	tooltip: {
//         trigger: 'item',
//         show: true,
//         axisPointer: {
//             snap: false,
//             type: 'cross'
//         }
//     },
//     legend: {
//         data:['功率曲线']
//     },
//     xAxis: {
//         type: 'value',
//         name:'流量(m3/h)',
//         min: function(value) {
//             return value.min - 20;
//         },
//         max: function(value) {
//             return value.max + 20;
//         },
//         axisPointer: {
//             show: true,
//             snap: false,
//             label: {
//                 show: true,
//                 formatter: function (e) {
// 					var temp1 = efficiencyCurveFormula.replace(reg,e.value)
// 					var temp2 = internalPowerCurveFormula.replace(reg,e.value)
// 					var temp3 = pressureCurveFormula.replace(reg,e.value)
// 					$('#xData').textbox('setValue', e.value)
// 					$('#yData1').textbox('setValue', eval(temp1))
// 					$('#yData2').textbox('setValue', eval(temp2))
// 					$('#yData3').textbox('setValue', eval(temp3))
//                     return e.value
//                 }
//             }
//         }
//     },
//     yAxis: [
//     	{
// 	        type: 'value',
// 	        name:'功率',
// 			axisPointer: {
// 				show: false
// 			}
// 	    }
//     ],
//     series: [
//         {
//             name:'功率曲线',
//             type:'line',
//             smooth: true,
//             data:power
//         }
//     ]
// }
// 	myChart2.setOption(option2)

// }
// //文件下载
// function downloadFile(value,row,index){
// 	ret =  '<a href="/fanFile/downloadFile.do?fileRealPath='+row.filePath+'&fileName='+row.fileName+'" class="easyui-linkbutton">download</a>';   
//     return ret;
// }

// function convertCanvasToImage() {
// 	var thisChart = echarts.init(document.getElementById('main'));
// 	var thisChart3 = echarts.init(document.getElementById('main3'));
// 	var thisChart1 = echarts.init(document.getElementById('main1'));
// 	var thisChart2 = echarts.init(document.getElementById('main2'));
// 	var chartExportUrl = '/productMix/exportPdf';
// 	var picBase64Info = thisChart.getDataURL(); // 获取echarts图的base64编码，为png格式。
// 	var picBase64Info3 = thisChart3.getDataURL(); // 静压。
// 	var picBase64Info1 = thisChart1.getDataURL(); // 效率。
// 	var picBase64Info2 = thisChart2.getDataURL(); // 功率。
// 	$('#chartForm').find('input[name="base64Info"]').val(picBase64Info+"&&"+picBase64Info3+"&&"+picBase64Info1+"&&"+picBase64Info2);//将编码赋值给输入框 
	
// 	$('#chartForm').find('input[name="fanSelectParam"]').val(JSON.stringify(parent.fanSelectParam));
// 	$('#chartForm').find('input[name="fanId"]').val(fanId); // 将风机id传到后台
// 	$('#chartForm').find('input[name="fanNumber"]').val(parent.fanNumber); // 将风机编号传到后台
// 	$('#chartForm').attr('action',chartExportUrl).attr('method', 'post'); // 设置提交到的url地址
// 	$('#chartForm').submit();
// }

// function loadNoise(){
// 	var noiseChart = echarts.init(document.getElementById('singleMachineNoise'))
// 	var option = {
// 	    title : {
// 	        text: 'Single machine noise histogram'
// 	    },
// 	    tooltip : {
// 	        trigger: 'axis'
// 	    },
// 	    legend: {
// 	        data:['Acoustic power','Import and Export Sound Pressure Level']
// 	    },
// 	    toolbox: {
// 	        show : true,
// 	        feature : {
// 	            dataView : {show: true, readOnly: false},
// 	            magicType : {show: true, type: ['line', 'bar']},
// 	            restore : {show: true},
// 	            saveAsImage : {show: true}
// 	        }
// 	    },
// 	    calculable : true,
// 	    xAxis : [
// 	        {
// 	            type : 'category',
// 	            data : ['63','125','250','500','1000','2000','4000','8000']
// 	        }
// 	    ],
// 	    yAxis : [
// 	        {
// 	            type : 'value'
// 	        }
// 	    ],
// 	    series : [
// 	        {
// 	            name:'Acoustic power',
// 	            type:'bar',
// 	            data:[noiseData.noise63, noiseData.noise125, noiseData.noise250, noiseData.noise500, noiseData.noise1KHz, noiseData.noise2KHz, noiseData.noise4KHz, noiseData.noise8KHz]
// 	        },
// 	        {
// 	            name:'Import and Export Sound Pressure Level',
// 	            type:'bar',
// 	            data:[noiseData.noise63s, noiseData.noise125s, noiseData.noise250s, noiseData.noise500s, noiseData.noise1KHzs, noiseData.noise1KHzs, noiseData.noise4KHzs, noiseData.noise8KHzs]
// 	        }
// 	    ]
// 	};
// 	noiseChart.setOption(option);
// }
</script>