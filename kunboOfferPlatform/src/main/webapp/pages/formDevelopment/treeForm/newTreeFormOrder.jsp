<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String title = request.getParameter("title");
    String pid = request.getParameter("pid");

    String uuid = UUID.randomUUID().toString();
    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建数据</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="field-con" id="fieldCon">
    <div class="field-con-title">主要信息</div>
  </div>

</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">

</script>
<script type="text/javascript">
var uuid = '<%=uuid%>'
var formObj
var fields = []
var formJson = ''
var title = '<%=title%>'
var pid = '<%=pid%>'
var basePath = '<%=basePath%>'
var normalSelectData = new Object()
var user = <%=userJson%>
console.info("UUID================================="+uuid);
$(function(){
  try{
    if(parent.transferFormJson && typeof(parent.transferFormJson)=="function"){
      formObj = parent.transferFormJson()
      fields = formObj.field
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
      initFormOrder(formObj,uuid)
    }else{
      $.ajax({
        url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
        type: "POST",
        dataType:'json',
        data:
        {
          'tableName': title
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          formObj = data
          fields = formObj.field
          for (var i = 0; i < fields.length; i++) {
            if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
              window[fields[i].text+'SelectFields'] = fields[i].selectFields
            }
          }
          initFormOrder(formObj,uuid)
        }
      })
    }
    if(pid!='0'){
      $.ajax({
        url: "<%=basePath%>/crm/ActionFormUtil/getDataById.do",
        type: "POST",
        dataType:'json',
        data:
        {
          'tableName': title,
          'id': pid
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          var temp = data.rows[0]
          $('#fieldCon').append('<div class="clear"></div><div class="form-order-line form-order-line-one"><label>上级</label><input type="text" id="parentInput" class="easyui-text" /></div>')
          $('#parentInput').textbox({
            width: '320',
            editable: false,
            value: temp[formObj.treeField]
          })
        }
      })
    }
  }catch(e){
  }
})
// 保存
function insert () {
  if (formCheckInput(fields)) {
    var obj = new Object()
    obj.title = formObj.title
    obj = getValue(fields, obj)
    obj.field.push({'text':'create_user_id','value': "'"+user.id+"'"})
    obj.field.push({'text':'taskid','value':'0'})
    obj.field.push({'text':'uuid','value':"'"+uuid+"'"})
    obj.field.push({'text':'pid','value':"'"+pid+"'"})
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormUtil/insert.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
    	data = $.trim(data)
    	  if(data == '1'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.addDialogCallBackSuccess()
          parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
</script>
