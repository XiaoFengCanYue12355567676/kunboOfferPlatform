<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>树形表单数据管理</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-panel" data-options="fit:true">
  <div class="form-order-table-con" style="width:30%;height:100%;">
    <table id="dg_table" style="width:100%;height:100%;">

  	</table>
  </div>
  <div class="form-order-table-con" style="width:70%;height:100%;">
    <table id="dg" style="width:100%;height:100%;">

  	</table>
  	<div id="tb" style="padding:0px 30px;height: 35px">
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="functionAdd()">新增</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionModify()">修改</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-info-circle" onclick="functionView()">查看</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="functionDelete()">删除</a>

    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
<script type="text/javascript">
var fields = []
var columns = []
var result
var formName = ''
var basePath = '<%=basePath%>'
var normalSelectData = new Object()
var key = '';
var formUrl = '';
var user = <%=userJson%>
var treeData = []
$(function(){
  // 初始化数据
  $('#dg_table').datagrid({
    url: "<%=basePath%>/crm/ActionFormUtil/getByType.do?type=2",
    columns:[[
      {
      field:'name',
      title:'表单名',
      width:'100%'
      },
      {
      field:'title',
      title:'',
      hidden:true
      }
    ]],
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    fitColumns:true,
    striped:true,
    onSelect:function (index,row){
      formName = row.title
      init(row.title)
    }
  })
  $('#dg').treegrid({
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    fitColumns:true,
    striped:true
  })
})
function init (title) {
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      result = data
      initNormalSelectData(result.field)
      columns = initColumns(result.field, false)
      initTable(title)
    }
  })
}
function initTable (title) {
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      treeData = dataConvertForTree(data)
      console.log(treeData)
      $('#dg').treegrid({
        data: treeData,
        columns:columns,
        idField: 'id',
        treeField: result.treeField,
        toolbar:'#tb'
      })
    }
  })
  
}
//新增一条数据
function functionAdd () {
  var row = $('#dg').treegrid('getSelected')
  var pid = '0'
  if (row != null) {
    pid = row.id
  }
  layer.open({
    type: 2,
    title: '添加数据',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', document.body.clientHeight-20+'px'],
    btn: '保存',
    content: 'newTreeFormOrder.jsp?title=' + formName+'&pid='+pid,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
}
//新增成功
function addDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').treegrid('reload')
}
//修改一条数据
function functionModify () {
  var row = $('#dg').treegrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '修改数据',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', document.body.clientHeight-20+'px'],
    btn: '保存',
    content: 'editTreeFormOrder.jsp?title=' + formName + '&uuid=' + row.uuid,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
//传输表单Json
function transferFormJson () {
  return result
}
//传输选中数据
function transferRowData () {
  var row = $('#dg').treegrid('getSelected')
  return row
}
//修改成功
function editDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').treegrid('reload')
}
//查看一条数据
function functionView () {
  var row = $('#dg').treegrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '查看数据',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', document.body.clientHeight-20+'px'],
    btn: '关闭',
    content: 'seeTreeFormOrder.jsp?title=' + formName + '&uuid=' + row.uuid,
    yes:function(index){
      layer.close(index)
    }
  })
}
//删除一条数据
function functionDelete () {
  var row = $('#dg').treegrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormUtil/delete.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': formName,
            'id': row.id
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#dg').treegrid('reload')
            }
          }
        })
      }
    })
  }
}

</script>
