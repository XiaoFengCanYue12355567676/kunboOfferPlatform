<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改字段</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line">
    <label>字段类型</label>
    <select class="easyui-combobox" id="fieldType" onchange="changeFieldType()">
      <option value="textbox">文本框</option>
      <option value="textboxMultiline">多行文本框</option>
      <option value="combobox">下拉列表框</option>
      <option value="comboboxMultiple">多选下拉框</option>
      <option value="numberbox">数值输入框</option>
      <option value="datebox">日期输入框</option>
      <option value="datetimebox">日期时间输入框</option>
      <option value="filebox">附件上传</option>
      <option value="tablebox">子表</option>
      <option value="hiddenbox">隐藏框</option>
    </select>
  </div>
  <div class="clear"></div>
  <div class="field-form-line">
    <label>数据库字段名</label>
    <input type="text" id="text" class="easyui-textbox" data-options="width:278" />
  </div>
  <div class="field-form-line">
    <label>字段标题</label>
    <input type="text" id="title" class="easyui-textbox" data-options="width:278" />
  </div>
  <div id="promptCon">
    <div class="field-form-line">
      <label>提示信息</label>
      <input type="text" id="prompt" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="clear"></div>
  </div>
  <div id="textbox">
    <div class="field-form-line">
      <label>字段宽度</label>
      <select class="easyui-combobox" id="width" data-options="width:70,panelHeight:70,editable:false">
        <option value="278">普通</option>
        <option value="703">整行</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>字段高度</label>
      <select class="easyui-combobox" id="height" data-options="width:70,editable:false">
        <option value="32">一行</option>
        <option value="64">两行</option>
        <option value="96">三行</option>
        <option value="128">四行</option>
        <option value="160">五行</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>是否可以编辑</label>
      <select class="easyui-combobox" id="editable" data-options="width:70,panelHeight:70,editable:false">
        <option value="true">是</option>
        <option value="false">否</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>是否禁用</label>
      <select class="easyui-combobox" id="disabled" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>是否只读</label>
      <select class="easyui-combobox" id="readonly" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>是否必填</label>
      <select class="easyui-combobox" id="required" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>是否列表显示</label>
      <select class="easyui-combobox" id="listDisplay" data-options="width:70,panelHeight:70,editable:false">
        <option value="true">是</option>
        <option value="false">否</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>新增时填充数据</label>
      <select class="easyui-combobox" id="defaultValue" data-options="editable:false">
        <option value="">无</option>
        <option value="actualName">登录用户名</option>
        <option value="department">登录用户部门</option>
      </select>
    </div>
    <div class="clear"></div>
  </div>
  <div class="box-con" id="textboxMultiline">

  </div>
  <div class="box-con" id="combobox">
    <div class="field-form-line">
      <label>数据来源</label>
      <select class="easyui-combobox" id="selectType" data-options="width:278,panelHeight:70,onSelect:chooseSelectType,value:''">
        <option value="0">普通</option>
        <option value="1">引用</option>
      </select>
    </div>
    <div class="clear"></div>
  </div>
  <div id="selectType0" style="display:none;">
    <div class="field-form-line">
      <label></label>
      <select class="easyui-combobox" id="normalSelect" data-options="width:278"></select>
    </div>
    <div class="clear"></div>
  </div>
  <div id="selectType1" style="display:none;">
    <div class="field-form-line">
      <label></label>
      <select class="easyui-combobox" id="existSelect" data-options="width:278,onSelect:initBringFields"></select>
    </div>
    <div class="clear"></div>
    <div id="selectTableCon"></div>
  </div>
  <div class="box-con" id="numberbox">
    <div class="field-form-line">
      <label>允许的最小值</label>
      <input type="text" id="min" class="easyui-numberbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>允许的最大值</label>
      <input type="text" id="max" class="easyui-numberbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>小数点后的位数</label>
      <input type="text" id="precision" class="easyui-numberbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>前缀字符</label>
      <input type="text" id="prefix" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>后缀字符</label>
      <input type="text" id="suffix" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>是否需要计算</label>
      <select class="easyui-combobox" id="needCalculate" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
    <div id="calculateDiv" style="display:none">
      <div class="field-form-line">
        <label>计算方式</label>
        <select class="easyui-combobox" id="calculateType" data-options="width:70,panelHeight:140,editable:false">
          <option value="multiply">乘</option>
          <option value="plus">加</option>
          <option value="divide">除</option>
          <option value="minus">减</option>
        </select>
      </div>
      <div class="field-form-line" id="calculateMul" style="display:none;">
        <label>从哪些字段计算</label>
        <select class="easyui-combobox" id="calculateFields" data-options="width:278,editable:false"></select>
      </div>
      <div id="calculateMin" style="display:none;">
      	<div class="field-form-line">
	        <label>前数</label>
	        <select class="easyui-combobox" id="calculateFirstField" data-options="width:278,editable:false"></select>
	      </div>
	      <div class="field-form-line">
	        <label>后数</label>
	        <select class="easyui-combobox" id="calculateLastField" data-options="width:278,editable:false"></select>
	      </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="box-con" id="datebox">
    <div class="field-form-line">
      <label>是否取当前时间</label>
      <select class="easyui-combobox" id="currentDate" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
  </div>
  <div class="box-con" id="datetimebox">
    <div class="field-form-line">
      <label>是否显示秒钟</label>
      <select class="easyui-combobox" id="showSeconds" data-options="width:70,panelHeight:70,editable:false">
        <option value="true">是</option>
        <option value="false">否</option>
      </select>
    </div>
    <div class="clear"></div>
  </div>
  <div class="box-con" id="tablebox">
    <div class="field-form-line">
      <label>子表</label>
      <input type="text" class="easyui-combobox" id="tableTitle" data-options="width:278"></select>
    </div>
    <div class="field-form-line">
      <label>是否加入导入导出</label>
      <select class="easyui-combobox" id="import" data-options="width:70,panelHeight:70,editable:false">
        <option value="false">否</option>
        <option value="true">是</option>
      </select>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var result, resultJson
var basePath = '<%=basePath%>'
var selectTableData = []
var selectCount = 0
var fields = []
$(function(){
  fields = parent.fields
  resultJson = parent.editData()
  console.log(resultJson)
  result = JSON.parse(resultJson)
  // 选择字段类型
  $('#fieldType').combobox({
    editable: false,
    onSelect: function (record) {
      $('.box-con').hide()
      if (record.value == 'combobox') {
        initexistSelect(true)
        selectCount++
      }
      if (record.value == 'comboboxMultiple') {
        $('#selectType0').show()
        initexistSelect(true)
      } else {
        $('#selectType0').hide()
      }
      if (record.value == 'tablebox') {
        $('#textbox').hide()
      } else {
        $('#textbox').show()
      }
      if (record.value == 'textbox' || record.value == 'textboxMultiline') {
        $('#promptCon').show()
      } else {
        $('#promptCon').hide()
      }
      if (record.value == 'datetimebox') {
        $('#datebox').show()
      }
      if (record.value == 'textboxMultiline' && ($('#width').combobox('getValue')=='703')) {
        $('#height').combobox({
          readonly: false
        })
      } else {
        $('#height').combobox('setValue', 32)
        $('#height').combobox({
          readonly: true
        })
      }
      $('#' + record.value).show()
    }
  })
  $('#width').combobox({ // 选择宽度
    onSelect: function (record) {
      if (record.value == '278') {
        $('#height').combobox('setValue', 32)
        $('#height').combobox({
          readonly: true
        })
      } else if (record.value == '703' && ($('#fieldType').combobox('getValue')=='textboxMultiline')) {
        $('#height').combobox({
          readonly: false
        })
      }
    }
  })
  $('#calculateFields').combobox({ // 从哪些字段计算
    data: fields,
    valueField:'text',
    textField:'title',
    multiple: true
  })
  $('#calculateFirstField').combobox({ // 计算前数
    data: fields,
    valueField:'text',
    textField:'title'
  })
  $('#calculateLastField').combobox({ // 计算后数
    data: fields,
    valueField:'text',
    textField:'title'
  })
  $('#needCalculate').combobox({ // 选择是否需要计算
    onSelect: function (record) {
      if (record.value == 'true') {
        $('#calculateDiv').show()
      } else if (record.value == 'false') {
        $('#calculateDiv').hide()
      }
    }
  })
  $('#calculateType').combobox({ // 选择计算方式
    onSelect: function (record) {
      if (record.value == 'multiply' || record.value == 'plus') {
        $('#calculateMul').show()
        $('#calculateMin').hide()
      } else if (record.value == 'divide' || record.value == 'minus') {
      	$('#calculateMin').show()
        $('#calculateMul').hide()
      }
    }
  })
  // 初始化子表数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getChildTableByType.do",
    type: "POST",
    dataType:'json',
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      var childTables = data.rows
      $('#tableTitle').combobox({
        data: childTables,
        valueField:'title',
        textField:'name',
        editable:false
      })
      if (result.fieldType == 'tablebox') {
        $('#tableTitle').combobox('setValue', result.tableTitle)
      }
    }
  })
  // 初始化数据
  $('#fieldType').combobox('setValue', result.fieldType)
  $('#text').textbox('setValue', result.text)
  $('#title').textbox('setValue', result.title)
  if (result.fieldType == 'textbox' || result.fieldType == 'textboxMultiline') {
    $('#prompt').textbox('setValue', result.prompt)
  }
  if (result.fieldType != 'tablebox') {
    $('#width').combobox('setValue', result.width)
    $('#height').combobox('setValue', result.height)
    $('#editable').combobox('setValue', result.editable)
    $('#disabled').combobox('setValue', result.disabled)
    $('#readonly').combobox('setValue', result.readonly)
    $('#required').combobox('setValue', result.required)
    $('#listDisplay').combobox('setValue', result.listDisplay)
    $('#defaultValue').combobox('setValue', result.defaultValue)
  }
  if (result.fieldType == 'combobox') {
    $('#selectType').combobox('setValue', result.selectType)
    if (result.selectType == '0') {
      $('#normalSelect').combobox('setValue', result.selectID)
    }else if(result.selectType == '1') {
      selectCount--
      $('#existSelect').combobox('setValue', result.selectID)
      var temp = '<table id="selectTable"><thead><tr>'
      temp += '<th data-options="field:\'name\',width:\'33%\'">字段名</th><th data-options="field:\'type\',width:\'33%\'">类型</th>'
      //temp += '<th data-options="field:\'inputName\',width:\'33%\'" editor="{type:\'combobox\',options:{valueField:\'text\',textField:\'text\',editable:false,data:removeFieldDisable(parent.fields)}}">组件名</th></tr></thead></table>'
      temp += '<th data-options="field:\'inputName\',width:\'33%\'">组件名</th></tr></thead></table>'
      temp += '<div id="tb_selectTable" style="padding:0px 30px;height: 35px"><a href="javascript:void(0)" id="selectTableModifyBtn" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="selectTableModify()">修改</a></div>'
      $('#selectTableCon').html(temp)
      $('#selectTable').datagrid({data:result.selectFields,rownumbers:true,singleSelect:true,autoRowHeight:false,pagination:false,fitColumns:true,striped:true,toolbar:'#tb_selectTable'})
      $("#selectTableModifyBtn").linkbutton({})
    }
  } else if (result.fieldType == 'numberbox') {
    $('#min').numberbox('setValue', result.min)
    $('#max').numberbox('setValue', result.max)
    $('#precision').numberbox('setValue', result.precision)
    $('#prefix').textbox('setValue', result.prefix)
    $('#suffix').textbox('setValue', result.suffix)
    $('#needCalculate').combobox('setValue', result.needCalculate)
    $('#calculateFields').combobox('setValues', result.calculateFields)
    $('#calculateType').combobox('setValue', result.calculateType)
    $('#calculateFirstField').combobox('setValue', result.calculateFirstField)
    $('#calculateLastField').combobox('setValue', result.calculateLastField)
  } else if (result.fieldType == 'datebox') {
    $('#currentDate').combobox('setValue', result.currentDate)
  } else if (result.fieldType == 'datetimebox') {
    $('#showSeconds').combobox('setValue', result.showSeconds)
    $('#currentDate').combobox('setValue', result.currentDate)
  } else if (result.fieldType == 'tablebox') {
    $('#import').combobox('setValue', result.import)
    //$('#tableTitle').combobox('setValue', "'"+result.tableTitle+"'")
  }
})
//保存
function update () {
	if (checkInput()) {
    var obj = new Object()
    var fieldType = $('#fieldType').combobox('getValue')
    obj.fieldType = fieldType
    obj.text = $.trim($('#text').textbox('getValue'))
    obj.title = $.trim($('#title').textbox('getValue'))
    if (fieldType == 'textbox' || fieldType == 'textboxMultiline') {
      obj.prompt = $.trim($('#prompt').textbox('getValue'))
    }
    if (fieldType != 'tablebox') {
      obj.width = $('#width').combobox('getValue')
  		obj.height = $('#height').combobox('getValue')
  		obj.editable = $('#editable').combobox('getValue')
      obj.disabled = $('#disabled').combobox('getValue')
      obj.readonly = $('#readonly').combobox('getValue')
      obj.required = $('#required').combobox('getValue')
      obj.listDisplay = $('#listDisplay').combobox('getValue')
      obj.defaultValue = $('#defaultValue').combobox('getValue')
    }
    if (fieldType == 'textboxMultiline') {
      obj.multiline = true
    } else if (fieldType == 'combobox') {
      obj.selectType = $('#selectType').combobox('getValue')
      if (obj.selectType == '0') {
        obj.selectID = $('#normalSelect').combobox('getValue')
      } else if (obj.selectType == '1') {
        obj.selectID = $('#existSelect').combobox('getValue')
        var selectTableRows = $('#selectTable').datagrid('getRows')
        obj.selectFields = selectTableRows
      }
    } else if (fieldType == 'comboboxMultiple') {
      obj.selectID = $('#normalSelect').combobox('getValue')
    } else if (fieldType == 'numberbox') {
      obj.min = $('#min').numberbox('getValue')
      obj.max = $('#max').numberbox('getValue')
      obj.precision = $('#precision').numberbox('getValue')
      obj.prefix = $.trim($('#prefix').textbox('getValue'))
      obj.suffix = $.trim($('#suffix').textbox('getValue'))
      obj.needCalculate = $('#needCalculate').combobox('getValue')
      obj.calculateFields = $('#calculateFields').combobox('getValues')
      obj.calculateType = $('#calculateType').combobox('getValue')
      obj.calculateFirstField = $('#calculateFirstField').combobox('getValue')
      obj.calculateLastField = $('#calculateLastField').combobox('getValue')
    } else if (fieldType == 'datebox') {
      obj.currentDate = $('#currentDate').combobox('getValue')
    } else if (fieldType == 'datetimebox') {
      obj.currentDate = $('#currentDate').combobox('getValue')
      obj.showSeconds = $('#showSeconds').combobox('getValue')
    } else if (fieldType == 'tablebox') {
      obj.tableTitle = $('#tableTitle').combobox('getValue')
      obj.import = $('#import').combobox('getValue')
      obj.listDisplay = 'true'
    }
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
    parent.editFieldDialogCallBackSuccess(info_str)
  	parent.layer.close(index) //再执行关闭
  }
}
//验证表单
function checkInput () {
	if (checkNull($('#text').textbox('getValue'))) {
     layerMsgCustom('数据库字段名不能为空')
     return false
	}
	if (checkNull($('#title').textbox('getValue'))) {
     layerMsgCustom('字段标题不能为空')
     return false
	}
  if ($('#fieldType').combobox('getValue') == 'tablebox') {
    if (checkNull($('#tableTitle').combobox('getValue'))) {
       layerMsgCustom('子表不能为空')
       return false
  	}
  }
	return true
}
</script>
