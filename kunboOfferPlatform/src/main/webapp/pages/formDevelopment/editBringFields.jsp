<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改字段</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>字段名</label>
    <input id="name" class="easyui-textbox" data-options="width:320,editable:false" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>类型</label>
    <input id="type" class="easyui-textbox" data-options="width:320,editable:false" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>组件名</label>
    <input id="inputName" class="easyui-combobox" data-options="width:320" />
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var fields = []
var result
$(function(){
  init()
})
function init () {
  fields = fieldsForCombobox(parent.parent.fields)
  console.log(fields)
  result = parent.transferSelectTableRow()
  $('#inputName').combobox({
    valueField: 'text',
    textField: 'text',
    data: fields
  })
  $('#name').textbox('setValue', result.name)
  $('#type').textbox('setValue', result.type)
  $('#inputName').combobox('setValue', result.inputName)
}
//保存
function update () {
  var obj = new Object()
  obj.name = $.trim($('#name').textbox('getValue'))
  obj.type = $.trim($('#type').textbox('getValue'))
  obj.inputName = $('#inputName').combobox('getValue')
  var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
  parent.editSelectTableCallBackSuccess(obj)
  parent.layer.close(index) //再执行关闭
}
</script>
