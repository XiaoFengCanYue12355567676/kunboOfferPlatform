<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String title = request.getParameter("title");
    String uuid = request.getParameter("uuid");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改数据</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="field-con" id="fieldCon">
    <div class="field-con-title">主要信息</div>
  </div>

</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var formObj
var fields = []
var formJson = ''
var uuid = '<%=uuid%>'
var title = '<%=title%>'
var basePath = '<%=basePath%>'
var result
var normalSelectData = new Object()
$(function(){
  try{
    if(parent.transferFormJson && typeof(parent.transferFormJson)=="function"){
      formObj = parent.transferFormJson()
      fields = formObj.field
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
      result = parent.transferRowData()
      initFormOrder(formObj,uuid)
      initFieldData(true)
    }else{
      $.ajax({
        url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
        type: "POST",
        dataType:'json',
        data:
        {
          'tableName': title
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          formObj = data
          fields = formObj.field
          for (var i = 0; i < fields.length; i++) {
            if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
              window[fields[i].text+'SelectFields'] = fields[i].selectFields
            }
          }
          $.ajax({
            url: "<%=basePath%>/crm/ActionFormUtil/getDataByUuid.do",
            type: "POST",
            dataType:'json',
            async:false,
            data:
            {
              'tableName': title,
              'uuid':uuid
            },
            error: function() //失败
            {
              messageloadError()
            },
            success: function(data)//成功
            {
              result = data.rows[0]
            }
          })
          initFormOrder(formObj,uuid)
          initFieldData(true)
        }
      })
    }
    addConfigJs(formObj)
    if (formObj.needTree=='true') {
      // setRelateFieldValue(formObj, result[formObj.relateField], false)
    }
  }catch(e){
  }
})
// 保存
function update (flag) {
  if (formCheckInput(fields)) {
    var obj = new Object()
    obj.title = formObj.title
    obj = getValue(fields, obj)
    obj.field.push({'text':'uuid','value':"'"+result.uuid+"'"})
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormUtil/update.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str,
        "id": result.id
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        if(data == '1'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
        	if(flag){
                parent.editDialogCallBackSuccess(uuid)
              }else{
                parent.editDialogCallBackSuccess1(index)
              }
        	//parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
// 取子表选中信息数据
function fieldChildTransferRowData (title) {
  var row = $('#'+title).datagrid('getSelected')
  return row
}
</script>
