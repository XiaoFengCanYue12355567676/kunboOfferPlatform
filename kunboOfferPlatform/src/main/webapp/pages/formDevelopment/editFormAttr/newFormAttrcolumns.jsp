<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建表头</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>表头名称</label>
    <input id="title" class="easyui-combobox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>表头id</label>
    <input id="field" class="easyui-combobox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>formatter</label>
    <input id="formatter" class="easyui-textbox" data-options="width:360" />
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
var fields = []
$(function(){
  init()
})
function init () {
  fields = parent.tranformFields()
  $('#title').combobox({
    valueField: 'title',
    textField: 'title',
    data: fields
  })
  $('#field').combobox({
    valueField: 'text',
    textField: 'text',
    data: fields
  })
}
//保存
function insert () {
	if (checkInput()) {
    var obj = new Object()
    obj.title = $('#title').combobox('getValue')
    obj.field = $('#field').combobox('getValue')
    obj.formatter = $.trim($('#formatter').textbox('getValue'))
    var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
    parent.addDialogCallBackSuccess('columns', obj)
    parent.layer.close(index) //再执行关闭
  }
}
//验证表单
function checkInput () {
  if (checkNull($('#title').combobox('getValue'))) {
      layerMsgCustom('请填写表头名称')
      return false
  }
  if (checkNull($('#field').combobox('getValue'))) {
      layerMsgCustom('请填写表头id')
      return false
  }
	return true
}
</script>
