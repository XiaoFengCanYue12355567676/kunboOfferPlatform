<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改按钮</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>按钮名称</label>
    <input id="buttonName" class="easyui-textbox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>按钮id</label>
    <input id="buttonId" class="easyui-combobox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>图标属性</label>
    <input id="buttonAttr" class="easyui-textbox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>方法名</label>
    <input id="buttonFunction" class="easyui-textbox" data-options="width:360" />
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
var fields = []
var result
$(function(){
  init()
})
function init () {
  fields = parent.tranformFields()
  result = parent.tranformRow('buttons')
  $('#buttonId').combobox({
    valueField: 'text',
    textField: 'text',
    data: fields
  })
  $('#buttonName').textbox('setValue', result.buttonName)
  $('#buttonId').combobox('setValue', result.buttonId)
  $('#buttonAttr').textbox('setValue', result.buttonAttr)
  $('#buttonFunction').textbox('setValue', result.buttonFunction)
}
//保存
function update () {
	if (checkInput()) {
    var obj = new Object()
    obj.buttonName = $.trim($('#buttonName').textbox('getValue'))
    obj.buttonId = $('#buttonId').combobox('getValue')
    obj.buttonAttr = $.trim($('#buttonAttr').textbox('getValue'))
    obj.buttonFunction = $.trim($('#buttonFunction').textbox('getValue'))
    var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
    parent.editDialogCallBackSuccess('buttons', obj)
    parent.layer.close(index) //再执行关闭
  }
}
//验证表单
function checkInput () {
  if (checkNull($('#buttonName').textbox('getValue'))) {
      layerMsgCustom('请填写按钮名称')
      return false
  }
  if (checkNull($('#buttonFunction').textbox('getValue'))) {
      layerMsgCustom('请填写方法名')
      return false
  }
	return true
}
</script>
