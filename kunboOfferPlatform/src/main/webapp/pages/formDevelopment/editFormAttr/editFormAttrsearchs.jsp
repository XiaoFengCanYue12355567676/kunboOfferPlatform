<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改搜索</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>输入名</label>
    <input id="searchName" class="easyui-combobox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>输入id</label>
    <input id="searchId" class="easyui-combobox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>关联字段</label>
    <input id="searchField" class="easyui-combobox" data-options="width:360,editable:false" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>输入属性</label>
    <input id="searchAttr" class="easyui-textbox" data-options="width:360" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>输入类型</label>
    <input id="searchType" class="easyui-combobox" data-options="width:360,editable:false" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>搜索条件</label>
    <input id="searchCondition" class="easyui-combobox" data-options="width:360,editable:false" />
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var fields = []
var result
var inputType = [
  {
    'id': 'textbox',
    'text': '文本框'
  },
  {
    'id': 'combobox',
    'text': '下拉框'
  },
  {
    'id': 'datebox',
    'text': '日期框'
  }
]
var searchCondition = [
  {
    'id': 'equal',
    'text': '等于'
  },
  {
    'id': 'notEqual',
    'text': '不等于'
  },
  {
    'id': 'contains',
    'text': '包含'
  },
  {
    'id': 'notContains',
    'text': '不包含'
  },
  {
    'id': 'isEmpty',
    'text': '是否为空'
  },
  {
    'id': 'greater',
    'text': '大于'
  },
  {
    'id': 'less',
    'text': '小于'
  }
]
$(function(){
  init()
})
function init () {
  fields = parent.tranformFields()
  result = parent.tranformRow('searchs')
  $('#searchName').combobox({
    valueField: 'title',
    textField: 'title',
    data: fields
  })
  $('#searchId').combobox({
    valueField: 'text',
    textField: 'text',
    data: fields
  })
  $('#searchField').combobox({
    valueField: 'text',
    textField: 'text',
    data: fields
  })
  $('#searchType').combobox({
    valueField: 'id',
    textField: 'text',
    data: inputType
  })
  $('#searchCondition').combobox({
    valueField: 'id',
    textField: 'text',
    data: searchCondition
  })
  $('#searchName').combobox('setValue', result.searchName)
  $('#searchId').combobox('setValue', result.searchId)
  $('#searchField').combobox('setValue', result.searchField)
  $('#searchAttr').textbox('setValue', result.searchAttr)
  $('#searchType').combobox('setValue', result.searchType)
  $('#searchCondition').combobox('setValue', result.searchCondition)
}
//保存
function update () {
	if (checkInput()) {
    var obj = new Object()
    obj.searchName = $('#searchName').combobox('getValue')
    obj.searchId = $('#searchId').combobox('getValue')
    obj.searchField = $('#searchField').combobox('getValue')
    obj.searchAttr = $.trim($('#searchAttr').textbox('getValue'))
    obj.searchType = $('#searchType').combobox('getValue')
    obj.searchCondition = $('#searchCondition').combobox('getValue')
    var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
    parent.editDialogCallBackSuccess('searchs', obj)
    parent.layer.close(index) //再执行关闭
  }
}
//验证表单
function checkInput () {
  if (checkNull($('#searchName').combobox('getValue'))) {
      layerMsgCustom('请填写输入名')
      return false
  }
  if (checkNull($('#searchId').combobox('getValue'))) {
      layerMsgCustom('请填写输入id')
      return false
  }
  if (checkNull($('#searchField').combobox('getValue'))) {
      layerMsgCustom('请填写关联字段')
      return false
  }
  if (checkNull($('#searchType').combobox('getValue'))) {
      layerMsgCustom('请填写输入类型')
      return false
  }
  if (checkNull($('#searchCondition').combobox('getValue'))) {
      layerMsgCustom('请填写搜索条件')
      return false
  }
	return true
}
</script>
