<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<%

    //服务器端接到回调函数名字输出回调函数，客户端根据回调函数进行解析取得函数中json对象      
    response.setContentType("text/html; charset=utf-8");      
    String uuid = request.getParameter("uuid");  
%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>日志</title> 

    <link href="<%=basePath%>/pages/css/base.css" rel="stylesheet">
    <link rel="stylesheet" href="<%=basePath%>/custom/uimaker/easyui.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/custom/uimaker/icon.css">
    <link rel="stylesheet" href="<%=basePath%>/pages/css/providers1.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/pages/css/process.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/pages/js/umeditor/themes/default/css/umeditor.css">

    <script type="text/javascript" src="<%=basePath%>/custom/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/custom/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/custom/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/umeditor/umeditor.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/umeditor/umeditor.config.js"></script>
    <script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
    
    <style type="text/css">
        html, body{ margin:0; height:100%; }
    </style>
    <style type="text/css" media=print>  
        .Noprint{display : none }  
    </style>  

</head> 
<body>    
    <script type="text/javascript">
        // order_id="GN-WXD-20170104-002";
        // var order_id = '00010';
    </script>
    <br></br>
    <div style="font-size: 22px;text-align: center;" id="Logtitle"></div>

    <div class="time-line" id="time-line">
        <!-- 报修 -->
        <div class="time-item date" id="time-item-date">
            <div class="content-left first">
                <span id="doTime"></span>
                <label><span class="dot"></span><i class="line"></i></label>
            </div>
        </div>
        <div class="time-item time" id="time-item-time">
<!--             <div class="content-left"> -->
<!--                 <span id="repairedTime"></span> -->
<!--                 <label><i class="line"></i><span class="dot"></span></label> -->
<!--             </div> -->
            <div class="content-right">
                <span class="left-arrow"></span>
                <div class="detail-outer">
                    <div class="detail">
                        <div>
                            <span class="name" id='project'>流程明细</span>
                            <label></label>
                            <span class="status"></span>
                        </div>  
                        <div>
                            <span class="name">办理人</span>
                            <label id='doPerson'></label>
                            <span class="status"></span>
                        </div> 
                        <div>
                            <span class="name">批注</span>
                            <label id='note'></label>
                            <span class="status"></span>
                        </div>   
                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</body> 
</html>

    <script type="text/javascript">
        var uuid = '<%=uuid%>';
        var repairObject = new Object();

        $("#Logtitle").text("流程执行明细");
                                    
        // 初始化载入客户列表
        $.ajax({
        	url: "<%=basePath%>/flow/getOpinionsByUuid",
            type: "POST",
            dataType:'json',
            data:  { "uuid": uuid},
            error: function() //失败
            { 
                alert('获取失败，请来联系相关人员！！');
            },
            success: function(data)//成功
            { 
                var rows = [];
                var taskArr;
                //var logs = jQuery.parseJSON(JSON.stringify(data));
                //alert(JSON.stringify(data));
                if(data.status=='0'){
                	var taskArr = data.list;
                }/* else if(data.status=='1'){
                	$.messager.alert('温馨提示','该条信息未关联流程!','warning');
                } */else if(data.status=='2'){
                	$.messager.alert('温馨提示','该流程未开启!','warning');
                }
                var nextBool;
                for (var i = 0; i < taskArr.length; i++){       
                    // 填充报修条目
                    if(i==0){
                    	$('#doTime').text(taskArr[i].endDate);
                    	$('#project').text(taskArr[i].taskName);
                        $('#doPerson').text(taskArr[i].assignee);
                        $('#note').text(''+taskArr[i].opinion);
                    } else{
                    	if(taskArr[i].endDate != null){   
                            var repairHtml = $('#time-item-date').clone();
                            repairHtml.find('#doTime').text(taskArr[i].endDate);
                            $("#time-line").append(repairHtml);

                            var repairHtmltime = $('#time-item-time').clone();


                            repairHtmltime.find('#project').text(taskArr[i].taskName);
                            repairHtmltime.find('#doPerson').text(taskArr[i].assignee);
                            repairHtmltime.find('#note').text(''+taskArr[i].opinion);

                            $("#time-line").append(repairHtmltime);
                        
                        }else{
                            var repairHtml = $('#time-item-date').clone();
                            repairHtml.find('#doTime').text("待办").
                            css("font-size", "20px").
                            css("color","red");
                            $("#time-line").append(repairHtml);
                            var repairHtmltime = $('#time-item-time').clone();
                            repairHtmltime.find('#project').text(taskArr[i].taskName);
                            if(taskArr[i].assign!=null){
                            	repairHtmltime.find('#doPerson').text(taskArr[i].assignee);
                            }else{
                            	repairHtmltime.find('#doPerson').text("组任务");
                            }
                            
                            repairHtmltime.find('#note').text(''+taskArr[i].opinion);

                            $("#time-line").append(repairHtmltime);
                        }
                    }
                    
                   }
                $("#time-line").append(
                    "<div class=\"time-item last\">\
                        <div class=\"content-left\">\
                            <label><i class=\"line\"></i><span class=\"dot\"></span></label>\
                        </div>\
                    </div>"
                );


            } 
        });
    </script>