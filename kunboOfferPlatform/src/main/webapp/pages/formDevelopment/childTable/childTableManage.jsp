<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String tableName = request.getParameter("tableName");
    String uuid = request.getParameter("uuid");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>子表数据管理</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-panel" data-options="fit:true">

  	<table id="<%=tableName%>" style="width:100%;height:100%">

  	</table>
    <div id="tb" style="padding:0px 30px;height: 35px">
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="childTableAdd()">新增</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="childTableModify()">修改</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="childTableDelete()">删除</a>

    </div>

</div>

</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var result = []
var tableName = '<%=tableName%>'
var uuid = '<%=uuid%>'
var basePath = '<%=basePath%>'
var normalSelectData = new Object()
$(function(){
  init(tableName)
})
function init (title) {
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'tableName': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      result = data
      columns = initColumns(result.field, false)
      initNormalSelectData(result.field)
      initTable(title)
    }
  })
}
function initTable (title) {
  $('#'+tableName).datagrid({
    url: "<%=basePath%>/crm/ActionFormUtil/getSonDataByUuid.do?tableName="+title+"&uuid="+uuid,
    columns:columns,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:true,
    fitColumns:true,
    striped:true,
    toolbar:'#tb',
    pageSize:20
  })
}
//新增一条数据
function childTableAdd () {
  layer.open({
    type: 2,
    title: '添加数据',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: 'addChildTableOrder.jsp?title='+tableName+'&recordID='+uuid,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
}
//修改一条数据
function childTableModify () {
  var row = $('#'+tableName).datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '修改数据',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['900px', '400px'],
    btn: '保存',
    content: 'editChildTableOrder.jsp?title='+tableName+'&recordID='+uuid+'&id='+row.id,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
//删除一条数据
function childTableDelete () {
  var row = $('#'+tableName).datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormUtil/delete.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': tableName,
            'id': row.id
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#'+tableName).datagrid('reload')
            }
          }
        })
      }
    })
  }
}
// 取子表选中信息数据
function fieldChildTransferRowData (title) {
  var row = $('#'+title).datagrid('getSelected')
  return row
}
</script>
