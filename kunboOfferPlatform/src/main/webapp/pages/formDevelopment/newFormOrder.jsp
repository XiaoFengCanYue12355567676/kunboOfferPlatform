<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String title = request.getParameter("title");
    String relate_id = request.getParameter("relate_id");

    String uuid = UUID.randomUUID().toString();
    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建数据</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="field-con" id="fieldCon">
    <div class="field-con-title">主要信息</div>
  </div>
  
  <div id="dlg" class="easyui-dialog" title="维修流程信息" data-options="iconCls:'icon-save',closed:true" style="width:500px;height:300px;padding:10px">
		<table id="dg" class='easyui-datagrid'
                        style="" title=""
                        data-options="                  
                            rownumbers:false,
                            singleSelect:true,
                            autoRowHeight:false,
                            pagination:true,
                            fitColumns:false,
                            striped:true,
                            checkOnSelect:true,
                            selectOnCheck:true,
                            collapsible:true,
                            toolbar:'#tb'">
                        <thead>
                            <tr>
                                <th data-options="field:'ck',checkbox:true"></th>
                                <th field="id" hidden="true">序列</th>
                                <th field="t_name">流程别名</th>
                                <th field="kehumingcheng">客户名称</th>
                                <th field="uuid" hidden="true" >流程编号</th>
                            </tr>
                        </thead>
                    </table>
	</div>
    <div id="tb" style="height:35px">
         <form id="queryForm" class="search-box">
             <input class="easyui-textbox" id='start_time' data-options="prompt:'流程别名'">
             <a id='btnQuery' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-search" onclick="functionQuery()" style="margin-left: 0px">搜索</a>          
             <a id='btnClear' href="#" class="easyui-linkbutton" btnCls='topjui-btn-normal' plain="true" iconCls="fa fa-refresh" onclick="functionClear()" style="margin-left: 0px">清除</a>                     
         </form>  
	 </div> 

</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">

</script>
<script type="text/javascript">
var uuid = '<%=uuid%>'
var formObj
var fields = []
var formJson = ''
var title = '<%=title%>'
var relate_id = '<%=relate_id%>'
var basePath = '<%=basePath%>'
var bringFieldsArr = []
var normalSelectData = new Object()
var user = <%=userJson%>;
var position_id = user.position_id;
var position_name='';
var jibie= '';
var object = new Object();
console.info("UUID================================="+uuid);
$(function(){
	$.post("/system/user/position/findById.do",{id:position_id},function(data){
	   position_name = data.text;
	});

  try{
    if(parent.transferFormJson && typeof(parent.transferFormJson)=="function"){
      formObj = parent.transferFormJson()
      fields = formObj.field
      for (var i = 0; i < fields.length; i++) {
        if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
          window[fields[i].text+'SelectFields'] = fields[i].selectFields
        }
      }
      initFormOrder(formObj,uuid)
    }else{
      $.ajax({
        url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
        type: "POST",
        dataType:'json',
        data:
        {
          'tableName': title
        },
        error: function() //失败
        {
          messageloadError()
        },
        success: function(data)//成功
        {
          formObj = data
          fields = formObj.field
          for (var i = 0; i < fields.length; i++) {
            if(fields[i].fieldType=='combobox' && fields[i].selectType=='1'){
              window[fields[i].text+'SelectFields'] = fields[i].selectFields
            }
          }
          initFormOrder(formObj,uuid)
        }
      })
    }
    addConfigJs(formObj)
    if (formObj.needTree=='true' && relate_id!='' && relate_id!=null) {
      //setRelateFieldValue(formObj, relate_id, false)
    }
  }catch(e){
  }
  $('#dg').datagrid({
      url: '/develop/url/getUrl.do',
      onClickRow:function(rowIndex,rowData){
    	  $("#ncr_number").textbox('setValue',rowData.t_name);
    	  $('#dlg').dialog('close');
      },
      queryParams: {
          name: 'getAllRepairProcessNumber'
      },
      method: 'get'
  })
  $('#ncr_number').textbox({
	  inputEvents: $.extend({},$.fn.textbox.defaults.inputEvents,{
	  click: function(event){
		  $('#dlg').dialog('open');//打开对话框
	  }})
  });
})

// 保存
function insert (flag) {
  if (formCheckInput(fields)) {
	//捕获任务下达下的异常
	try {
	    jibie = $('#zhongyaochengdu').combobox('getText'); 
	} catch (e) {
		console.log(e.name + ": " + e.message);
	}
	object.type = '1';
	object.bili = 0.11;
	object.position = position_name;
	object.gate = jibie;
	var params = JSON.stringify(object);
    var obj = new Object()
    obj.title = formObj.title
    obj = getValue(fields, obj)
    obj.field.push({'text':'create_user_id','value': "'"+user.id+"'"})
    obj.field.push({'text':'taskid','value':'0'})
    obj.field.push({'text':'uuid','value':"'"+uuid+"'"})
    if (formObj.needTree=='true' && relate_id!='' && relate_id!=null) {
      obj.field.push({'text':'pid','value':"'"+relate_id+"'"})
    }
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormUtil/insert.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
         messageSaveError();
      },
      success: function(data)//成功
      {
    	data = $.trim(data)
    	/*var arr= new Array(); //表名
	     arr = data.split(",") ;
        console.info(JSON.stringify(arr));*/
        if(data == '1'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          if(flag){
            parent.addDialogCallBackSuccess(uuid,params,index);
          }else{
            parent.addDialogCallBackSuccess1(index)
          }
          /* console.info(JSON.stringify(arr[0])); */
        }else{

        	 messageSaveError();
        }
      }
    })
  }
}
// 取子表选中信息数据
function fieldChildTransferRowData (title) {
  var row = $('#'+title).datagrid('getSelected')
  return row
}

</script>
