<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="java.util.UUID"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String paths = request.getParameter("paths");

    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建数据</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="button-con">
    <button type="button" onclick="uploadFile()">上传</button>
  </div>
  <ul class="fileList" id="fileList">

  </ul>

</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">

</script>
<script type="text/javascript">
var paths = '<%=paths%>'
var basePath = '<%=basePath%>'
var user = <%=userJson%>
var files = []
$(function(){
  if (paths=='null' || paths=='undefined') {
    paths=''
  }
  if (paths.length>0) {
    files = paths.split(',')
  }
  init()
})
function init () {
  $('#fileList').html('')
  for (var i = 0; i < files.length; i++) {
    var name = files[i].split('/')[3].substring(36)
    $('#fileList').append('<li><button type="button" class="file-delete" onclick="deleteFile(\''+files[i]+'\')">删除</button><a href="'+basePath+files[i]+'" download="'+name+'" target="_blank">'+name+'</a></li>')
  }
}
function uploadFile () {
  layer.open({
    type: 2,
    title: "上传文件",
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ["850px", "340px"],
    btn: "关闭",
    content: basePath+"/pages/fileManager/add.jsp",
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      var filePath = Ifame.close()
      if (filePath.length>0) {
        files = files.concat(filePath)
        // console.log(files)
        init()
      }
      layer.close(index)
    }
  })
}
function deleteFile (file) {
  $.messager.confirm('确认','您确认想要删除吗？',function(r){
    if (r) {
      files.splice(files.indexOf(file), 1)
      init()
    }
  })
}
function close () {
  // console.log(files.toString())
  return files.toString()
}
</script>
