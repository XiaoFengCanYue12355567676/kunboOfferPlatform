<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String title = request.getParameter("title");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>修改表单</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/js/codemirror/codemirror.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
<style>
.form-item{
  padding-right: 70px;
}
</style>
</head>
<body>
<div class="easyui-layout">
  <div class="field-con">
    <div class="field-form-line field-form-line-one">
      <label style="font-weight:bold;">数据库表单名</label>
      <input type="text" id="title" class="easyui-textbox" data-options="width:765,editable:false" />
    </div>

  </div>
  <div class="form-line">
    <label class="form-label">表格表头</label>
    <div class="form-item">
      <table id="dg_columns" style="width:100%;height:400px">
        <thead>
          <tr>
            <th field="title" halign="center" width="32%">表头名称</th>
            <th field="field" halign="center" width="32%">表头id</th>
            <th field="formatter" halign="center" width="31%">formatter</th>
          </tr>
        </thead>
      </table>
      <div id="tb_columns" style="padding:0px 30px;height: 35px">
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="formAttrAdd('columns')">新增</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="formAttrModify('columns')">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="formAttrDelete('columns')">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-up" onclick="formAttrUp('columns')">上移</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-down" onclick="formAttrDown('columns')">下移</a>
      </div>
    </div>
  </div>
  <div class="form-line">
    <label class="form-label">表格按钮</label>
    <div class="form-item">
      <table id="dg_buttons" style="width:100%;height:200px">
        <thead>
          <tr>
            <th field="buttonName" halign="center" width="24%">按钮名称</th>
            <th field="buttonId" halign="center" width="24%">按钮id</th>
            <th field="buttonAttr" halign="center" width="24%">图标属性</th>
            <th field="buttonFunction" halign="center" width="24%">方法名</th>
          </tr>
        </thead>
      </table>
      <div id="tb_buttons" style="padding:0px 30px;height: 35px">
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="formAttrAdd('buttons')">新增</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="formAttrModify('buttons')">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="formAttrDelete('buttons')">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-up" onclick="formAttrUp('buttons')">上移</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-down" onclick="formAttrDown('buttons')">下移</a>
      </div>
    </div>
  </div>
  <div class="form-line">
    <label class="form-label">搜索栏</label>
    <div class="form-item">
      <table id="dg_searchs" style="width:100%;height:200px">
        <thead>
          <tr>
            <th field="searchName" halign="center" width="18%">输入名</th>
            <th field="searchId" halign="center" width="16%">输入id</th>
            <th field="searchField" halign="center" width="16%">关联字段</th>
            <th field="searchAttr" halign="center" width="18%">输入属性</th>
            <th field="searchType" halign="center" width="16%" formatter="formatterFieldType">输入类型</th>
            <th field="searchCondition" halign="center" width="16%" formatter="formatterCondition">搜索条件</th>
          </tr>
        </thead>
      </table>
      <div id="tb_searchs" style="padding:0px 30px;height: 35px">
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="formAttrAdd('searchs')">新增</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="formAttrModify('searchs')">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="formAttrDelete('searchs')">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-up" onclick="formAttrUp('searchs')">上移</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-down" onclick="formAttrDown('searchs')">下移</a>
      </div>
    </div>
  </div>
  <div class="form-line">
    <label class="form-label">搜索按钮</label>
    <div class="form-item">
      <table id="dg_searchButtons" style="width:100%;height:200px">
        <thead>
          <tr>
            <th field="buttonName" halign="center" width="24%">按钮名称</th>
            <th field="buttonId" halign="center" width="24%">按钮id</th>
            <th field="buttonAttr" halign="center" width="24%">图标属性</th>
            <th field="buttonFunction" halign="center" width="24%">方法名</th>
          </tr>
        </thead>
      </table>
      <div id="tb_searchButtons" style="padding:0px 30px;height: 35px">
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="formAttrAdd('searchButtons')">新增</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="formAttrModify('searchButtons')">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="formAttrDelete('searchButtons')">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-up" onclick="formAttrUp('searchButtons')">上移</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-down" onclick="formAttrDown('searchButtons')">下移</a>
      </div>
    </div>
  </div>
  <div class="form-line">
    <label class="form-label">JS代码</label>
    <div class="form-item">
      <div style="border:1px solid #ddd;">
        <textarea id="jsCode"></textarea>
      </div>
      <div class="">表格ID为“dg”,代码中请勿使用单引号。</div>
    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/codemirror/codemirror.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var inputType = [
  {
    'id': 'textbox',
    'text': '文本框'
  },
  {
    'id': 'combobox',
    'text': '下拉框'
  },
  {
    'id': 'datebox',
    'text': '日期框'
  }
]
var searchCondition = [
  {
    'id': 'equal',
    'text': '等于'
  },
  {
    'id': 'notEqual',
    'text': '不等于'
  },
  {
    'id': 'contains',
    'text': '包含'
  },
  {
    'id': 'notContains',
    'text': '不包含'
  },
  {
    'id': 'isEmpty',
    'text': '是否为空'
  },
  {
    'id': 'greater',
    'text': '大于'
  },
  {
    'id': 'less',
    'text': '小于'
  }
]
var fields = []
var result
var resultJson = ''
var title = '<%=title%>'
var editor
var columns = []
var buttons = []
var searchs = []
var searchButtons = []
$(function(){
  $('#title').textbox('setValue', title)
  //js编辑器初始化
  editor=CodeMirror.fromTextArea(document.getElementById("jsCode"),{
    mode:"text/javascript",
    lineNumbers: true,
    lineSeparator: '\n'
  })
  // 初始化数据
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do?tableName="+title,
    type: "POST",
    dataType:'json',
    async:false,
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      fields = fieldsForCombobox(data.field)
    }
  })
  $.ajax({
    url: "<%=basePath%>/pages/button/framework/get.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'title': title
    },
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      if (data.obj != null) {
        result = data.obj
        init(true)
      } else {
        init(false)
      }
    }
  })
})
function init (flag) {
  if (flag) {
    columns = eval(result.columns)
    buttons = eval(result.buttons)
    searchs = eval(result.searchs)
    searchButtons = eval(result.search_buttons)
    if(result.js_code!='' && typeof(result.js_code)!='undefined'){
      editor.setValue(result.js_code.replace(/&quot;/g, '"').replace(/换行符/g, '\n'))
    }
  } else {
    for (var i = 0; i < fields.length; i++) {
      if (fields[i].text != 'pid') {
        columns.push({
          'field': fields[i].text,
          'title': fields[i].title
        })
      }
    }
    buttons = [
      {
        "buttonName":"新增",
        "buttonAttr":"fa fa-plus",
        "buttonId":"add",
        "buttonFunction":"functionAdd()"
      },
      {
        "buttonName":"修改",
        "buttonAttr":"fa fa-pencil",
        "buttonId":"edit",
        "buttonFunction":"functionModify()"
      },
      {
        "buttonName":"查看",
        "buttonAttr":"fa fa-info-circle",
        "buttonId":"view",
        "buttonFunction":"functionView()"
      },
      {
        "buttonName":"删除",
        "buttonAttr":"fa fa-trash",
        "buttonId":"delete",
        "buttonFunction":"functionDelete()"
      }
    ]
  }
  //表格表头初始化
  $('#dg_columns').datagrid({
    data:columns,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb_columns'
  })
  //表格按钮初始化
  $('#dg_buttons').datagrid({
    data:buttons,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb_buttons'
  })
  //搜索栏初始化
  $('#dg_searchs').datagrid({
    data:searchs,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb_searchs'
  })
  //搜索按钮初始化
  $('#dg_searchButtons').datagrid({
    data:searchButtons,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb_searchButtons'
  })
}
//新增一条表格表头
function formAttrAdd (id) {
	layer.open({
    type: 2,
    title: "添加数据",
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ["600px", "400px"],
    btn: "保存",
    content: "editFormAttr/newFormAttr"+id+".jsp",
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
}
//传输表单字段
function tranformFields () {
  return fields
}
//新增成功
function addDialogCallBackSuccess (id, item) {
  messageSaveOK()
  $('#dg_'+id).datagrid('appendRow', item)
}
//修改一条数据
function formAttrModify (id) {
  var row = $('#dg_'+id).datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: "添加数据",
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ["600px", "400px"],
    btn: "保存",
    content: "editFormAttr/editFormAttr"+id+".jsp",
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
//传输数据
function tranformRow (id) {
  var row = $('#dg_'+id).datagrid('getSelected')
  return row
}
//修改成功
function editDialogCallBackSuccess (id, item) {
  messageSaveOK()
  var row = $('#dg_'+id).datagrid('getSelected')
  var i = $('#dg_'+id).datagrid('getRowIndex', row)
  $('#dg_'+id).datagrid('updateRow', {index: i, row: item})
}
//删除一条表格表头
function formAttrDelete (id) {
  var row = $('#dg_'+id).datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
	var i = $('#dg_'+id).datagrid('getRowIndex', row)
	$('#dg_'+id).datagrid('deleteRow', i)
}
// 表格表头上移
function formAttrUp (id) {
  $('#dg_'+id)
  var row = $('#dg_'+id).datagrid('getSelected')
  var index = $('#dg_'+id).datagrid('getRowIndex', row)
  if (index > 0) {
    $('#dg_'+id).datagrid('insertRow', {
      index: index-1,
      row: row
    })
    $('#dg_'+id).datagrid('deleteRow', index+1)
    $('#dg_'+id).datagrid('selectRow', index-1)
  }
}
// 表格表头下移
function formAttrDown (id) {
  var row = $('#dg_'+id).datagrid('getSelected')
  var index = $('#dg_'+id).datagrid('getRowIndex', row)
  if (index < $('#dg_'+id).datagrid('getRows').length-1) {
    $('#dg_'+id).datagrid('deleteRow', index)
    $('#dg_'+id).datagrid('insertRow', {
      index: index+1,
      row: row
    })
    $('#dg_'+id).datagrid('selectRow', index+1)
  }
}
// 保存
function save () {
    var obj = new Object()
    obj.title = $.trim($('#title').textbox('getValue'))
    obj.columns = removeIsNewRecord($("#dg_columns").datagrid("getRows"))
    obj.buttons = removeIsNewRecord($("#dg_buttons").datagrid("getRows"))
    obj.searchs = removeIsNewRecord($("#dg_searchs").datagrid("getRows"))
    obj.search_buttons = removeIsNewRecord($("#dg_searchButtons").datagrid("getRows"))
    var jsCode = editor.getValue().replace(/"/g, '&quot;').replace(/\n/g, '换行符')
    //console.log(obj.jsCode)
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    //alert(jsCode.indexOf('\n'))
    console.log(jsCode)
    $.ajax({
      url: "<%=basePath%>/pages/button/framework/create.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str,
        "jsCode": jsCode
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
       messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        var flag = data ==="true" ? true : false
        if(flag){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.editDialogCallBackSuccess()
        	parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })

}
function formatterCondition (value, row, index) {
  if (value == 'equal') {
    return '等于'
  } else if (value == 'notEqual') {
    return '不等于'
  } else if (value == 'contains') {
    return '包含'
  } else if (value == 'notContains') {
    return '不包含'
  } else if (value == 'isEmpty') {
    return '是否为空'
  } else if (value == 'greater') {
    return '大于'
  } else if (value == 'less') {
    return '小于'
  }
}
</script>
