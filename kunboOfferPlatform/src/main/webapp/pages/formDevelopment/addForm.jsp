<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>创建表单</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="field-con">
    <div class="field-form-line field-form-line-one">
      <label>数据库表名</label>
      <input type="text" id="title" class="easyui-textbox" data-options="width:742" />
    </div>
    <div class="field-form-line">
      <label>表单标题</label>
      <input type="text" id="name" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>表类型</label>
      <select class="easyui-combobox" id="type" data-options="width:278,panelHeight:105,editable:false">
        <option value="0">主表</option>
        <option value="1">子表</option>
        <option value="2">树形表</option>
      </select>
    </div>
    <div class="field-form-line">
      <label>表单列数</label>
      <select class="easyui-combobox" id="columnNumber" data-options="width:278,editable:false">
        <option value="1">每行1列</option>
        <option value="2">每行2列</option>
        <option value="3">每行3列</option>
        <option value="4">每行4列</option>
      </select>
    </div>
    <div class="field-form-line" id="treeFieldCon" style="display:none;">
      <label>树节点字段</label>
      <input type="text" id="treeField" class="easyui-combobox" data-options="width:278,editable:false" />
    </div>
    <div id="treeCon">
      <div class="field-form-line">
        <label>是否需要树结构</label>
        <select class="easyui-combobox" id="needTree" data-options="width:278,panelHeight:70,editable:false">
          <option value="false">否</option>
          <option value="true">是</option>
        </select>
      </div>
      <div id="treeFormCon" style="display:none;">
        <div class="field-form-line">
          <label>树结构表单</label>
          <select class="easyui-combobox" id="treeForm" data-options="width:278,editable:false">
          </select>
        </div>
      </div>
    </div>
  </div>
  <table id="dg" style="width:100%;height:450px">
    <thead>
      <tr>
        <th field="id" hidden="true"></th>
        <th field="text" width="150">字段名</th>
        <th field="title" width="150">字段标题</th>
        <th field="fieldType" width="100" formatter="formatterFieldType">字段类型</th>
        <th field="width" width="70">宽度</th>
        <th field="height" width="70">高度</th>
        <th field="editable" width="70" formatter="formatterWhether">可编辑</th>
        <th field="disabled" width="70" formatter="formatterWhether">禁用</th>
        <th field="readonly" width="70" formatter="formatterWhether">只读</th>
        <th field="required" width="70" formatter="formatterWhether">必填</th>
        <th field="listDisplay" width="70" formatter="formatterWhether">列表显示</th>
      </tr>
    </thead>
  </table>
  <div id="tb" style="padding:0px 30px;height: 35px">
    <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="fieldAdd()">新增</a>
    <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="fieldModify()">修改</a>
    <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="fieldDelete()">删除</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-up" onclick="fieldUp()">上移</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-arrow-down" onclick="fieldDown()">下移</a>

  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var fields = []
var IsExistenceForTableName = false
var treeFields = []
$(function(){
  //表格初始化
  $('#dg').datagrid({
    data:fields,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb'
  })
  // 是否显示选择树节点字段
  $('#type').combobox({
    onSelect: function (record) {
      if (record.value == 2) {
        $('#treeFieldCon').show()
        $('#treeCon').hide()
      } else {
        $('#treeFieldCon').hide()
        $('#treeCon').show()
      }
    }
  })
  treeFields = removeFieldForTree(fields)
  $('#treeField').combobox({
    data: treeFields,
    valueField:'text',
    textField:'title'
  })
  // 是否需要树结构
  $('#needTree').combobox({
    onSelect: function (record) {
      if (record.value == 'true') {
        $('#treeFormCon').show()
      } else {
        $('#treeFormCon').hide()
      }
    }
  })
  // 初始化树结构表单数据
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getByType.do?type=2",
    type: "POST",
    dataType:'json',
    error: function() //失败
    {
      messageloadError()
    },
    success: function(data)//成功
    {
      $('#treeForm').combobox({
        data: data,
        valueField:'title',
        textField:'name',
        editable:false
      })
    }
  })
  $('#title').textbox('textbox').bind('blur', function(e){ // 触发检测表名重复
    var title = 'business_cloud_'+$.trim($('#title').textbox('getValue'))
    if (!checkNull(title)) {
      checkTableName(title)
    }
  })
})
function checkTableName (newValue) {
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/IsExistenceForTableName.do",
    type: "POST",
    dataType:'text',
    data:
    {
      "tableName": newValue
    },
    error: function(e) //失败
    {
     console.info("异常="+JSON.stringify(e));
     messageSaveError();
     IsExistenceForTableName = false
    },
    success: function(data)//成功
    {
      data = $.trim(data)
      var flag = data ==="true" ? true : false
      if(flag){
        messageCustom('数据库表单名已存在，请更改')
        IsExistenceForTableName = false
      }else{
        IsExistenceForTableName = true
      }
    }
  })
}
// 保存
function save () {
  if (checkInput()) {
    var obj = new Object()
    obj.title = 'business_cloud_'+$.trim($('#title').textbox('getValue'))
    obj.name = $.trim($('#name').textbox('getValue'))
    obj.type = $('#type').combobox('getValue')
    obj.columnNumber = $('#columnNumber').combobox('getValue')
    obj.treeField = $.trim($('#treeField').textbox('getValue'))
    obj.needTree = $('#needTree').combobox('getValue')
    obj.treeForm = $('#treeForm').combobox('getValue')
    var temp = fieldsToObject(fields)
    if (obj.type == '2') {
      temp.push({"fieldType":"textbox","text":"pid","title":"父ID","listDisplay":"false","type":"int"})
    }
    if (obj.needTree == 'true') {
      temp.push({"fieldType":"textbox","text":"pid","title":"父ID","listDisplay":"false","type":"int"})
      //temp.push({"fieldType":"textbox","text":"relate_id","title":"关联ID","listDisplay":"false","type":"varchar(255)"})
    }
    if (obj.type == '0') {
      // temp.push({"fieldType":"textbox","text":"create_user_id","title":"用户ID","listDisplay":"false","type":"int"})
      // temp.push({"fieldType":"textbox","text":"create_date","title":"添加日期","listDisplay":"false","type":"datetime"})
      // temp.push({"fieldType":"textbox","text":"uuid","title":"UUID","listDisplay":"false","type":"int"})
      // temp.push({"fieldType":"textbox","text":"taskid","title":"任务ID","listDisplay":"false","type":"int"})
    }
    obj.field = temp
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    document.write(_SavingHtml)
    $.ajax({
      url: "<%=basePath%>/pages/crminterface/creatTable.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
        messageSaveError();
        completeLoading()
      },
      success: function(data)//成功
      {
        completeLoading()
        data = $.trim(data)
        var flag = data ==="true" ? true : false
        if(flag){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.addDialogCallBackSuccess()
        	parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
//验证表单
function checkInput () {
  var title = $('#title').textbox('getValue')
	if (checkNull(title)) {
     layerMsgCustom('数据库表单名不能为空')
     return false
	}
  if (!IsExistenceForTableName) {
    messageCustom('数据库表单名已存在，请更改')
    return false
  }
  if (checkNull($('#name').textbox('getValue'))) {
     layerMsgCustom('表单标题不能为空')
     return false
  }
  return true
}
</script>
