<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建字段</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>数据来源</label>
    <select id="type" class="easyui-combobox" data-options="width:70,editable:false">
      <option value="0">新建</option>
      <%-- <option value="1">已存在</option> --%>
    </select>
  </div>
  <div class="clear"></div>
  <div id="type0" style="display:none">
    <div class="field-form-line field-form-line-one">
      <label>数据库名</label>
      <input type="text" id="tableName" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="clear"></div>
    <div class="field-form-line field-form-line-one">
      <label>标题</label>
      <input type="text" id="title" class="easyui-textbox" data-options="width:278" />
    </div>
  </div>
  <div id="type1" style="display:none">
    <div class="field-form-line field-form-line-one">
      <label>选择表</label>
      <select id="selectTableName" class="easyui-combobox" data-options="editable:false"></select>
    </div>
    <div class="field-form-line field-form-line-one">
      <label>选择字段</label>
      <select id="selectField" class="easyui-combobox" data-options="editable:false"></select>
    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
$(function(){
  $('#type').combobox({
    onSelect: function(rec){
      if (rec.value == 0) {
        $('#type0').show()
        $('#type1').hide()
      } else if (rec.value == 1) {
        $('#type1').show()
        $('#type0').hide()
      }
    }
  })
  // init()
})
function init () {
  $.ajax({
    url: "<%=basePath%>/crm/ActionFormUtil/getAllTable.do",
    type: "POST",
    dataType:'json',
    data:{},
    error: function(e) //失败
    {
      console.info("异常="+JSON.stringify(e));
      messageSaveError();
    },
    success: function(data)//成功
    {
      $('#selectTableName').combobox({
        valueField: 'title',
        textField: 'name',
        data: data.rows,
        onSelect: function(rec){
          $.ajax({
            url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
            type: "POST",
            dataType:'json',
            data:
            {
              'tableName': rec.title
            },
            error: function() //失败
            {
              messageloadError()
            },
            success: function(data)//成功
            {
              var fields = data.field
              for (var i = 0; i < fields.length; i++) {
                delete fields[i].disabled
              }
              $('#selectField').combobox({
                valueField: 'text',
                textField: 'title',
                data: data.field
              })
            }
          })
        }
      })
    }
  })

}
//保存
function insert () {
	if (checkInput()) {
    var obj = new Object()
    obj.type = $('#type').combobox('getValue')
    if (obj.type == 0) {
      obj.tableName = 'select_' + $.trim($('#tableName').textbox('getValue'))
      obj.title = $.trim($('#title').textbox('getValue'))
      obj.valueField = 'id'
      obj.textField = 'text'
    } else {
      obj.tableName = $('#selectTableName').combobox('getValue')
      obj.title = $('#selectTableName').combobox('getText')
      obj.valueField = 'id'
      obj.textField = $('#selectField').combobox('getValue')
    }
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/pages/crminterface/creatSelectTable.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str
      },
      error: function(e) //失败
      {
        console.info("异常="+JSON.stringify(e));
        messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        if(data == '创建成功'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.addSelectDialogCallBackSuccess()
        	parent.layer.close(index) //再执行关闭
        }else if(data == '表名已存在'){
        	 alert(data)
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
//验证表单
function checkInput () {
  if ($('#type').combobox('getValue') == 0) {
    if (checkNull($('#tableName').textbox('getValue'))) {
       layerMsgCustom('数据库名不能为空')
       return false
  	}
  	if (checkNull($('#title').textbox('getValue'))) {
       layerMsgCustom('标题不能为空')
       return false
  	}
  } else {
    if (checkNull($('#selectTableName').combobox('getValue'))) {
       layerMsgCustom('请选择表')
       return false
  	}
  	if (checkNull($('#selectField').combobox('getValue'))) {
       layerMsgCustom('请选择字段')
       return false
  	}
  }

	return true
}
</script>
