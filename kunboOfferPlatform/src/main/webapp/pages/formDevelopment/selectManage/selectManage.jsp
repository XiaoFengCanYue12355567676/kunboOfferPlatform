<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>下拉数据管理</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-panel" data-options="fit:true" style="position:relative">
  <div data-options="region:'west',title:'',border:false,split:true" style="text-align: center;overflow:hidden;width: 40%;height: 100%">
    <table id="dg_select" style="width:100%;height:100%">
      <thead>
       <tr>
         <th field="title" width="100%">下拉列表</th>
         <th field="id" width="0px" hidden="true">序列</th>
       </tr>
      </thead>
  	</table>
    <div id="tb_select" style="padding:0px 30px;height: 35px">
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="selectAdd()">新增</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="selectDelete()">删除</a>

    </div>
  </div>
  <div style="text-align: center;overflow:hidden;position:absolute;left:40%;top: 0;width: 60%;height: 100%">
    <table id="dg" style="width:100%;height:100%;">
      <thead>
       <tr>
         <th field="text" width="50%">选项名称</th>
         <th field="text_en" width="50%">英文名称</th>
         <th field="id" width="0px" hidden="true">序列</th>
       </tr>
      </thead>
  	</table>
  	<div id="tb" style="padding:0px 30px;height: 35px">
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="functionAdd()">新增</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionModify()">修改</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="functionDelete()">删除</a>

    </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript">
var formName = ''
$(function(){
    // 初始化数据
  $('#dg_select').datagrid({
    url: '<%=basePath%>/crm/ActionFormUtil/getByTableName.do?tableName=table_manage_select',
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb_select',
    onClickRow: function (rowIndex) {
      var row = $('#dg_select').datagrid('getSelected');
      if (row.type == 0) {
        initOptionTable(row.table_name)
      }else{
        formName = ''
        $('#dg').datagrid('loadData', { total: 0, rows: [] })
      }
    }
  })
  $('#dg').datagrid({
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb'
  })
})
function initOptionTable (title) {
  $('#dg').datagrid('loadData', { total: 0, rows: [] })
  $('#dg').datagrid({
    url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do?tableName="+title,
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:false,
    fitColumns:true,
    striped:true,
    toolbar:'#tb'
  })
  formName = title
  $('#dg').datagrid('reload')
}
function selectAdd () {
  layer.open({
    type: 2,
    title: '添加下拉数据表',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['520px', '400px'],
    btn: '保存',
    content: 'addSelect.jsp',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.insert()
    }
  })
}
//新增成功
function addSelectDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg_select').datagrid('reload')
}
function selectDelete () {
  var row = $('#dg_select').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条下拉表')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/pages/crminterface/deleteSelectTable.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': row.table_name,
            'type': row.type
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#dg_select').datagrid('reload')
            }
          }
        })
      }
    })
  }
}
//新增一条参数
function functionAdd () {
  if(formName==''){
    alert('无法新增选项')
  }else{
    layer.open({
      type: 2,
      title: '添加选项',
      shadeClose: true,
      shade: false,
      maxmin: true, //开启最大化最小化按钮
      area: ['520px', '300px'],
      btn: '保存',
      content: 'addOption.jsp?title=' + formName,
      yes:function(index){
        var ifname="layui-layer-iframe"+index//获得layer层的名字
        var Ifame=window.frames[ifname]//得到框架
        Ifame.insert()
      }
    })
  }
}
//新增成功
function addDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//修改一条参数
function functionModify () {
  var row = $('#dg').datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条选项')
    return false
  }
  layer.open({
    type: 2,
    title: '修改选项',
    shadeClose: true,
    shade: false,
    maxmin: true, //开启最大化最小化按钮
    area: ['520px', '300px'],
    btn: '保存',
    content: 'editOption.jsp?title=' + formName + '&id=' + row.id,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.update()
    }
  })
}
function transferRowData () {
  var row = $('#dg').datagrid('getSelected')
  return row
}
//修改成功
function editDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//删除一条参数
function functionDelete () {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条选项')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormSelectUtil/Select/delete.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': formName,
            'id': row.id
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#dg').datagrid('reload')
            }
          }
        })
      }
    })
  }
}

</script>
