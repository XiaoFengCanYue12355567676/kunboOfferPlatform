<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String title = request.getParameter("title");
    String id = request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>新建字段</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="field-con">
  <div class="field-form-line field-form-line-one">
    <label>选项名称</label>
    <input type="text" id="text" class="easyui-textbox" data-options="width:278" />
  </div>
  <div class="field-form-line field-form-line-one">
    <label>英文名称</label>
    <input type="text" id="text_en" class="easyui-textbox" data-options="width:278" />
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
var formName = '<%=title%>'
var id = '<%=id%>'
var obj = new Object()
$(function(){
  init()
})
function init () {
  obj = parent.transferRowData()
  $('#text').textbox('setValue', obj.text)
  $('#text_en').textbox('setValue', obj.text_en)
}
//保存
function update () {
	if (checkInput()) {
    obj.text = "'" + $.trim($('#text').textbox('getValue')) + "'"
    obj.title = formName
    obj.text_en = "'" + $.trim($('#text_en').textbox('getValue')) + "'"  
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormSelectUtil/Select/update.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str,
        'id': id
      },
      error: function(e) //失败
      {
        console.info("异常="+JSON.stringify(e));
        console.info("异常");
        messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        if(data == '1'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.editDialogCallBackSuccess()
        	parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
//验证表单
function checkInput () {
	if (checkNull($('#text').textbox('getValue'))) {
     layerMsgCustom('选项名称不能为空')
     return false
	}
	return true
}
</script>
