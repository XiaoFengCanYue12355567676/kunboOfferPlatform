<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>表单管理</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-panel" data-options="fit:true">

  	<table id="dg" style="width:100%;height:100%">
      <thead>
        <tr>
          <th field="id" hidden="true"></th>
          <th field="title">数据库表名</th>
          <th field="name">表单标题</th>
          <th field="type" formatter="formType">表类型</th>
          <th field="str_json" formatter="formFields">字段</th>
        </tr>
      </thead>
  	</table>
    <div id="tb" style="padding:0px 30px;height: 35px">
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="functionAdd()">新增</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-pencil" onclick="functionModify()">修改</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-cog" onclick="functionSet()">配置</a>
      <a href="#" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="functionDelete()">删除</a>

    </div>

</div>

</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
$(function(){
  //表格初始化
  $('#dg').datagrid({
    url: '<%=basePath%>/crm/ActionFormUtil/getAllTable.do',
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:true,
    fitColumns:true,
    striped:true,
    toolbar:'#tb',
    pageSize:20
  })
})
function formType (value, row, index) {
  if (value == 0) {
    return '主表'
  } else if (value == 1) {
    return '子表'
  } else if (value == 2) {
    return '树形表'
  }
}
function formFields (value, row, index) {
  var obj = JSON.parse(value)
  var fields = obj.field
  var temp = ''
  for (var i = 0; i < fields.length; i++) {
    var field = fields[i]
    if (field.text != 'create_user_id' && field.text != 'taskid') {
      temp += field.title + ','
    }
  }
  temp = removeComma(temp)
  return temp
}
//新增一条表单
function functionAdd () {
  layer.open({
    type: 2,
    title: '添加表单',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['980px', document.body.clientHeight-20+'px'],
    btn: '保存',
    content: 'addForm.jsp',
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.save()
    }
  })
}
//新增成功
function addDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//修改一条表单
function functionModify () {
  var row = $('#dg').datagrid('getSelected')
  if (row == null) {
    alert('请先选择一条数据')
    return false
  }
  layer.open({
    type: 2,
    title: '修改表单',
    shadeClose: false,
    shade: 0.3,
    maxmin: true, //开启最大化最小化按钮
    area: ['980px', document.body.clientHeight-20+'px'],
    btn: '保存',
    content: 'editForm.jsp?title=' + row.title,
    yes:function(index){
      var ifname="layui-layer-iframe"+index//获得layer层的名字
      var Ifame=window.frames[ifname]//得到框架
      Ifame.save()
    }
  })
}
//修改成功
function editDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//配置表单
function functionSet () {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    if (row.type!='0') {
      alert('只有主表可以配置')
    } else {
      layer.open({
      type: 2,
      title: '配置表单',
      shadeClose: false,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: ['980px', document.body.clientHeight-20+'px'],
      btn: ['保存','删除'],
      content: 'editFormAttr.jsp?title=' + row.title,
      yes:function(index){
        var ifname="layui-layer-iframe"+index//获得layer层的名字
        var Ifame=window.frames[ifname]//得到框架
        Ifame.save()
      },
      btn2:function(index){
        $.messager.confirm('确认','您确认想要删除吗？',function(r){
          if (r){
            $.ajax({
              url: "<%=basePath%>/pages/button/framework/delete.do",
              type: "POST",
              dataType:'text',
              data:
              {
                'title': row.title
              },
              error: function() //失败
              {
                messageDeleteError()
              },
              success: function(data)//成功
              {
                data = $.trim(data)
                var flag = data ==="true" ? true : false
                if(flag){
                  messageDeleteOK()
                }else{
                  messageDeleteError()
                }
              }
            })
          }
        })
      }
    })
    }
  }
}
//删除一条表单
function functionDelete () {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormUtil/deleteByTableName.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'tableName': row.title
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#dg').datagrid('reload')
            }
          }
        })
      }
    })
  }
}

</script>
