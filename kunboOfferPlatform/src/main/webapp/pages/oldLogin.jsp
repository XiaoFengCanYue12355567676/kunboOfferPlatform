<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html> 
<html lang="en">    
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>维保云平台</title>  
    <link href="css/login/login.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<%=basePath%>/pages/css/base.css" >
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/custom/uimaker/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/custom/uimaker/icon.css"> 
	<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

	<script type="text/javascript" src="<%=basePath%>/custom/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/custom/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/custom/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>


</head> 
<body class="white">
	<div class="login-hd">
		<div class="left-bg"></div>
		<div class="right-bg"></div>
		<div class="hd-inner">
<!-- 		<span> -->
		
<!-- 			<img alt="" src="./images/Image 2.png" style="margin-top: 20px"> -->
<!-- 			<span class="sys-name" style="font-size: 25px;margin-top: 30px">智能运维云平台</span> -->
<!-- 		</span>  -->
        <span id="sysSet">
		
			
		</span> 
		</div>
	</div>
	<div class="login-bd">
		<div class="bd-inner">
			<div class="inner-wrap">
				<div class="lg-zone">
					<div class="lg-box">
						<div class="lg-label"><h4>用户登录</h4></div> 
						<form>
							<div class="lg-username input-item clearfix">
								<i class='fa fa-user-o'></i>
								<input type="text" autocomplete="off" id="userName" placeholder="账号">
							</div>
							<div class="lg-password input-item clearfix">
								<i class='fa fa-key'></i>
								<input type="password" id="password1" style="display:none">
								<input type="password" autocomplete="off" id="password"  placeholder="请输入密码">
							</div>
							<div class="tips clearfix">
								<label><input id='isSave' type="checkbox">记住用户名</label>
							</div>
							<div class="enter">
								<a href="javascript:;" class="purchaser" onClick="login()" style="width: 300px;background-color:#1da02b">登录</a>
							</div>
						</form>
						<div class="line line-y"></div>
						<div class="line line-g"></div>
					</div>
				</div>
				<div class="lg-poster"></div>
			</div>
		</div>
	</div>
	<div class="login-ft">
		<div class="ft-inner">
<!-- 			<div class="about-us">
				<a href="javascript:;">关于我们</a>
				<a href="javascript:;">法律声明</a>
				<a href="javascript:;">服务条款</a>
				<a href="javascript:;">联系方式</a>
			</div> -->
			<div class="address">中国 山东 威海市 威海高技术产业开发区初村镇山海路80号&nbsp;邮编：264200 &nbsp;&nbsp;Copyright&nbsp;©&nbsp;2017&nbsp;-&nbsp;2020&nbsp;</div>
			<div class="other-info">建议使用IE8及以上版本浏览器&nbsp;</div>
		</div>
	</div>
</body> 
</html>
    
<script type="text/javascript">
$.ajax({
	url: "<%=basePath%>/crm/ActionFormUtil/getByTableName.do?tableName=system_set",
	type: "POST",
	dataType:'json',
	error: function() //失败
	{
		messageloadError()
	},
	success: function(data)//成功
	{
		if (data.rows.length > 0) {
			
			var row = data.rows[0]
			if (row.system_icon != '') {
				$('#sysSet').html('<img src="<%=basePath%>' + row.system_icon + '" style="margin-top: 20px;height:60px"><span class="sys-name" style="font-size: 25px;margin-top: 30px">'+row.system_name+'</span>')
			} else {
				$('#sysSet').html('<span>'+row.system_name+'</span>')
			}
			$(document).attr('title',row.system_name)
		} else {
			$('#sysSet').html('<img alt="" src="/pages/images/Image 2.png" style="margin-top: 20px"><span class="sys-name" style="font-size: 25px;margin-top: 30px">克莱特商务云平台</span>')
			$(document).attr('title','克莱特商务云平台')
		}
	}
})

    function ajaxLoading(){   
        $("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");   
        $("<div class=\"datagrid-mask-msg\"></div>").html("正在登录，请稍候。。。").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});   
     }   
     function ajaxLoadEnd(){   
         $(".datagrid-mask").remove();   
         $(".datagrid-mask-msg").remove();               
    } 

	function login()
	{
		console.info("login");

		if(!checkInput()) return;

		console.info('if(!checkInput()) return;');
		var userName = $('#userName').val();
		var password = $('#password').val();
		var isSave = $("input[type='checkbox']").is(':checked');
		if(isSave == true)
		{
			console.info("userName 111= "+userName);
			console.info("password 111= "+password);
			console.info('isSave 111 = '+isSave);
			setCookie('username',userName,365)
			setCookie('password',password,365)
			setCookie('saveFlag',isSave,365)
		}
		else
		{
			console.info("userName 222= "+userName);
			console.info("password 222= "+password);
			console.info('isSave 222 = '+isSave);
			setCookie('username','',365)
			setCookie('password','',365)
			setCookie('saveFlag','',365)
		}

		ajaxLoading();
		$.get
        (
             "<%=basePath%>/system/user/login.do",
            {	
                userName:userName,
                password:password
            },           
            function(data)
            {
				console.info(data.result);
				if(data.result==true)
				{					
					// ajaxLoadEnd();
					top.location='<%=basePath%>/pages/main.jsp';
				}
				if(data.result==false)
				{
					ajaxLoadEnd();
					$.messager.alert('提醒',"<font size='4px'>登录失败</font>",'warning');
				}
            },
			'json'
        );
	}
	$(function(){		
		checkCookie();
	})

</script>
<script type="text/javascript">
	function getCookie(c_name)
	{
		if (document.cookie.length>0)
		{
			c_start=document.cookie.indexOf(c_name + "=")
			if (c_start!=-1)
			{ 
				c_start=c_start + c_name.length+1 
				c_end=document.cookie.indexOf(";",c_start)
				if (c_end==-1) c_end=document.cookie.length
				return unescape(document.cookie.substring(c_start,c_end))
			} 
		}
		return ""
	}

	function setCookie(c_name,value,expiredays)
	{
		var exdate=new Date()
		exdate.setDate(exdate.getDate()+expiredays)
		document.cookie=c_name+ "=" +escape(value)+
		((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
	}

	function checkCookie()
	{
		var username=getCookie('username');
		var password=getCookie('password');
		var saveFlag = getCookie('saveFlag');

		console.info('username 222= '+username);
		console.info('password 222= '+password);
		console.info('saveFlag 222= '+saveFlag);

		if(username!=null&&username!='')
		{
			$('#userName').val(username);
		}
		if(password!=null&&password!='')
		{
			$('#password').val(password);
		}
		if(saveFlag == 'true')
		{
			console.info('xxxxxxxx');
			$("[type='checkbox']").attr("checked",true);//全选  
		}
		else
		{
			console.info('yyyyyyy');
			$("[type='checkbox']").attr("checked",false);//全选  
		}
	}

</script>
<!-- 回车键按键绑定 -->
<script type="text/javascript">
$('#password').bind('keypress',function(event){  
    if(event.keyCode == "13") 
    {
    	console.info('回车键按键绑定');
    	login();
    }
});  
$('#userName').bind('keypress',function(event){  
    if(event.keyCode == "13") 
    {
    	console.info('回车键按键绑定');
    	login();
    }
});  
var checkInput = function()
{
	if(checkNull($('#userName').val()))
	{
		messageCustom('请输入用户名');
		return false;
	}
	if(checkNull($('#password').val()))
	{
		messageCustom('请输入密码');
		return false;
	}
	return true;
}

</script>