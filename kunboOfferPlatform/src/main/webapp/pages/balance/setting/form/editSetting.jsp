<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>修改设置</title> 

    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/pages/css/process.css">    
    <!-- buttons 组件 -->
    <link type="text/css" href="/buttons/buttons.css" rel="stylesheet"/>    
    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    
<style type="text/css">
    html, body{ margin:0; height:100%;width: 100%;border-width:0px}
</style>


<body class="easyui-layout">
    <div data-options="region:'west',iconCls:'icon-reload',title:'',split:false,border:true" 
         style="height:60px;width: 100%;height: 45px;border-width:0px;overflow:hidden" >  
	    <table class="editTable" style="text-align: center; margin-top: 30px;margin-left:0px" >
	        <tr>
	            <td class="label">销售订单号</td>
	            <td >
                    <input id='salesOrder' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='销售订单号'>     
                </td>
	            <td class="label">生产订单号</td>
	            <td >
                    <input id='productionOrder' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='销售订单号'>     
                </td>                            
            </tr>	
	        <tr>
	            <td class="label">叶片部编码</td>
	            <td >
                    <input id='bladeCode' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='叶片部编码'>     
                </td>
	            <td class="label"></td>
	            <td >
                </td>                            
            </tr>	
	        <tr>
	            <td class="label">叶尖支撑点到旋转中心距离L1</td>
	            <td >
                    <input id='apexDistance' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='叶尖支撑点到旋转中心距离L1'>     
                </td>
	            <td class="label">叶根支撑点到旋转中心距离L2</td>
	            <td >
                    <input id='rootDistance' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='叶根支撑点到旋转中心距离L2'>     
                </td>                            
            </tr>	            	              
	        <tr>
	            <td class="label">配重点与叶尖距离</td>
	            <td >
                    <input id='bobDistance' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='配重点与叶尖距离'>     
                </td>
	            <td class="label"></td>
	            <td >
                </td>                            
            </tr>	  
	        <tr>
	            <td class="label">标准互差</td>
	            <td >
                    <input id='multiDeviation' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='标准互差'>     
                </td>
	            <td class="label">最大误差</td>
	            <td >
                    <input id='maxDeviation' class="easyui-textbox" data-options="required:true,width:150"
                    prompt='最大误差'>     
                </td>                            
            </tr>	             	                  
	        <tr>
                <td class="label"></td>
                <td >
                    <!-- <input id='pc_pid' class="easyui-textbox" data-options="required:false,width:150,editable:true" > -->
                </td>
                <td class="label"></td>
                <td style="text-align:right">
                    <a onclick='save()' class="button button-primary button-rounded button-small"                        
                    ><i class="fa fa-check"></i>保存</a>
                    <!-- <input id='partsName' class="easyui-textbox" data-options="required:false,width:150,editable:true"  >        -->
                </td>
            </tr>	                   
	    </table>
	</div>
	<!-- <div data-options="region:'south',iconCls:'icon-reload',title:'',split:false,border:true" style="height: 45px;border-width:0px">  
        <div style="text-align: right;margin-top: 5px">
            <a onclick='save()' class="button button-primary button-rounded button-small"><i class="fa fa-check"></i>保存</a>
        </div>        
    </div> -->
</body> 
</html>
<script type="text/javascript">

    var id = shuoheUtil.getUrlHeader().id;

    var BalanceOrder = new Object();

 	$(function() {
        var re = Easyui.Ajax.get('/balance/order/findById.do?id='+id);
        BalanceOrder = re.obj;
        console.info('BalanceOrder = '+JSON.stringify(BalanceOrder));
        if(re.result == false)
        {
            
        }
        else
        {
            $('#salesOrder').textbox('setValue',BalanceOrder.salesOrder);
            $('#productionOrder').textbox('setValue',BalanceOrder.productionOrder);
            $('#bladeCode').textbox('setValue',BalanceOrder.bladeCode);
            $('#apexDistance').textbox('setValue',BalanceOrder.apexDistance);
            $('#rootDistance').textbox('setValue',BalanceOrder.rootDistance);
            $('#bobDistance').textbox('setValue',BalanceOrder.bobDistance);
            $('#multiDeviation').textbox('setValue',BalanceOrder.multiDeviation);
            $('#maxDeviation').textbox('setValue',BalanceOrder.maxDeviation);
        }
	}); 
	
    function save() {
        console.info('save');
        if(shuoheUtil.checkRequired() == false) return;
        BalanceOrder.salesOrder = $('#salesOrder').textbox('getValue');  
        BalanceOrder.productionOrder = $('#productionOrder').textbox('getValue');  
        BalanceOrder.bladeCode = $('#bladeCode').textbox('getValue');  
        BalanceOrder.apexDistance = $('#apexDistance').textbox('getValue');  
        BalanceOrder.rootDistance = $('#rootDistance').textbox('getValue');  
        BalanceOrder.bobDistance = $('#bobDistance').textbox('getValue');  
        BalanceOrder.multiDeviation = $('#multiDeviation').textbox('getValue');  
        BalanceOrder.maxDeviation = $('#maxDeviation').textbox('getValue');  

        shuoheUtil.layerSaveMaskOn();
        $.ajax({
            url: "/balance/order/save.do", 
            type: "POST",
            dataType: 'json',
            data: {
                'data': JSON.stringify(BalanceOrder)
            },
            error: function() //失败
            {
                shuoheUtil.layerTopMsgError('通讯失败');
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data) //成功
            {
                shuoheUtil.layerSaveMaskOff();
                if (data.result == true) {
                    parent.closeLayer();       
                    shuoheUtil.layerTopMsgOK('保存成功');
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引          
                    parent.layer.close(index); //再执行关闭                    
                } else {
                    shuoheUtil.layerTopMsgError(data.describe);
                }
            }
        });
    }
</script>