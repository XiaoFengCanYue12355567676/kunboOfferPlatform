<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>平衡配置管理</title> 


    <link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
    <link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme"/>
    <!-- FontAwesome字体图标 -->
    <link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <link type="text/css" href="buttons/buttons.css" rel="stylesheet">

    <!-- jQuery相关引用 -->
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
    <!-- TopJUI框架配置 -->
    <script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
    <!-- TopJUI框架核心-->
    <script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
    <!-- TopJUI中文支持 -->
    <script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

    <script type="text/javascript" src="/pages/js/moment.min.js"></script>
    <script type="text/javascript" src="/pages/js/layer/layer.js"></script>
    <script type="text/javascript" src="/pages/js/util.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheLayer.js"></script>

    <script type="text/javascript" src="/pages/js/base-loading.js"></script>
    <script type="text/javascript" src="/pages/js/shuoheUtil.js"></script>
    <!-- <script type="text/javascript" src="/pages/js/product/productManage.js"></script> -->
    

    <style type="text/css">
        html, body{ margin:0; height:100%; }
        
    </style>
    
</head> 
<script>
var remoteHost = '';
</script>
<body style="overflow: hidden;" class="easyui-layout">
    <div data-options="region:'center',split:true" style="height:100%;width:40%;">

        <table id="dg" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                idField:'templateInfoId',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb'"
            >
            <thead>
                <tr>
                    <!-- <th field="check"  checkbox="true">uuid</th>                 -->
                    <th field="id"  hidden ="true">uuid</th>                
                    <th field="salesOrder">销售订单号</th>                
                    <th field="productionOrder">生产订单号</th>
                    <th field="bladeCode">叶片部编码</th>
                    <th field="apexDistance" formatter='Easyui.Datagraid.formatterDecimal2'>叶尖支撑点到旋转中心距离L1</th>  
                    <th field="rootDistance" formatter='Easyui.Datagraid.formatterDecimal2'>叶根支撑点到旋转中心距离L2</th>  
                    <th field="bobDistance" formatter='Easyui.Datagraid.formatterDecimal2'>配重点与叶尖距离</th>  
                    <th field="multiDeviation" formatter='Easyui.Datagraid.formatterDecimal2'>标准互差</th>                      
                    <th field="maxDeviation" formatter='Easyui.Datagraid.formatterDecimal2'>最大误差</th>  
                    <th field="bladeNum">叶片数量</th>            
                </tr>
            </thead>
        </table>    
        <div id="tb" style="height:35px">
            <!-- <a id='btnDgAdd' href="javascript:void(0)" class="easyui-linkbutton" style="margin-left: 35px" data-options="iconCls:'fa fa-plus',plain:true" onclick="funMgAdd()">新增</a> -->
            <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" 
                style="margin-left: 35px"
                data-options="iconCls:'fa fa-plus',plain:true" 
                onclick="funDgCreate()">新建</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-edit',plain:true" 
                onclick="funDgEdit()">修改</a>
            <!-- <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-trash',plain:true" 
                onclick="funDgDelete()">删除</a>
            <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-jpy',plain:true" 
                onclick="quote()">报价</a> -->
            <!-- <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                data-options="iconCls:'fa fa-hand-pointer-o',plain:true" 
                onclick="funDgEdit()">查看</a>             -->
        </div>
    </div>       
    <div data-options="region:'east',iconCls:'icon-reload',split:true"  style="height:100%;width:60%;">
        <table id="dg2" class='easyui-datagrid' style="width:100%;height:100%" title="" data-options="
                idField:'templateInfoId',
                treeField:'text',
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb2   '"
            >
            <thead>
                <tr>
                    <!-- <th field="check"  checkbox="true">uuid</th>                 -->
                    <th field="chakan" formatter='formatterView'>查看</th>
                    <th field="isChanged" formatter='formatterIsChanged'>变更</th>
                    <th field="discard" formatter='formatterDeal'>废弃</th>      
                    <th field="id" hidden>uuid</th>            
                    <th field="bladeCode">叶片编号</th>         
                    <th field="apexWight">叶尖重量</th>                
                    <th field="rootWight">页根重量</th>  
                    <th field="bladeTorque">叶片力矩</th>        
                    <th field="torqueDeviation" formatter='Easyui.Datagraid.formatterDecimal2'>力矩差</th>                
                    <th field="calcApexMaxBallast" formatter='Easyui.Datagraid.formatterDecimal2'>计算叶尖最大配重/g</th>
                    <th field="calcApexMinBallast">最小配重重量/g</th>
                    <th field="predictMaxBladeTorque" formatter='Easyui.Datagraid.formatterDecimal2'>预计最大配重后叶片力矩M/g.m</th>  
                    <th field="predictMinBladeTorque" formatter='Easyui.Datagraid.formatterDecimal2'>预计最大配重后叶片力矩M/g.m</th>  
                    <th field="quoteDate">报价时间</th>           
                </tr>
            </thead>
            <div id="tb2" style="height:35px">
                <a id='btnDgEdit' href="javascript:void(0)" class="easyui-linkbutton" 
                    style="margin-left: 35px"
                    data-options="iconCls:'fa fa-plus',plain:true" 
                    onclick="funDg2Create()">新建</a>
                <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                    data-options="iconCls:'fa fa-edit',plain:true" 
                    onclick="funDg2Edit()">修改</a>
                <a id='btnDgDelete' href="javascript:void(0)" class="easyui-linkbutton" 
                    data-options="iconCls:'fa fa-trash',plain:true" 
                    onclick="funDg2Delete()">删除</a>     
            </div>            
        </table>    
  
    </div>       
  <script type="text/javascript">
    var typeId;
    var user = new Object();

 	//表格初始化 
    $(function(){

        user = Easyui.User.getUser();
        console.info('user = '+JSON.stringify(user));

        $('#dg').datagrid({
            url:"/balance/order/list.do",
            queryParams:{

            },
            onClickRow:function(index, row)
            {
                console.info(JSON.stringify(row));
                $('#dg2').datagrid({
                    url:"/balance/blade/findByOrderId.do",
                    queryParams:{
                        'orderId':row.id
                    },
                    onLoadSuccess:function(data)
                    {
                        console.info('onLoadSuccess ='+JSON.stringify(data));
                        for(var i=0;i<data.rows.length;i++)
                        {
                            console.info('i = '+i+' id ='+data.rows[i].id);
                            $('#delete_'+data.rows[i].id).linkbutton({});
                            $('#change_'+data.rows[i].id).linkbutton({});
                            $('#view_'+data.rows[i].id).linkbutton({});
                        }
                    },

                });
            }
        });

        // $('#dg').treegrid('loadData',dgdata);
    }); 
     

  
	</script>
</body> 
</html>
<script type="text/javascript">
    function funDgCreate()
    {
        Easyui.Layer.openLayerWindow(
            '新建单据',
            'form/addSetting.jsp',
            ['700px','500px']);  
    }

    function closeLayer()
    {
        $('#dg').datagrid('reload');
        $('#dg2').datagrid('reload');
    }
    function funDgEdit()
    {
        var dg_row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(dg_row))
        {
            shuoheUtil.layerTopMsgError('修改前先选择一条数据');
            return;
        }
        Easyui.Layer.openLayerWindow(
            '编辑报价单',
            'form/editSetting.jsp?id='+dg_row.id,
            ['700px','500px']);  
    }
    function funDgDelete()
    {
        var row = Easyui.Datagraid.getSelectRow('dg');
        if(Easyui.Object.isEmptyObject(row))
        {
            shuoheUtil.layerMsgError('删除前请先选择一条数据');
        }
        else
        {
            $.messager.defaults = { ok: '确定',cancel: '取消'};
            $.messager.confirm('确认删除', "<font size='3px'>您确认将删除这个报价单么?</font>", 
                function(r){
                    console.info('r = '+r);
                    if (r){
                    console.info('doFunction');
                    deleteHistoryProductById(row);
                }
            });
        }
    }
    function deleteHistoryProductById(row)
    {
        var r = Easyui.Ajax.get('/shop/history/order/deleteOrderById.do?orderId='+row.id);
        if(r.result == true)
        {
            shuoheUtil.layerMsgOK('删除成功');
            $('#dg').datagrid('reload');
        }
        else
        {
            shuoheUtil.layerMsgError('删除失败');
            $('#dg').datagrid('reload');
        }
    }
    function formatterDeal(value,row,index)
    {
        if(row.discard == true)
        {
            var htmlstr =  '<a id="delete_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="plain:true" \
                            >--</a>';
        }
        else
        {
            var htmlstr =  '<a id="delete_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="iconCls:\'fa fa-trash\',plain:true" \
                            onclick="funDg2Delete('+'\''+row.id+'\')"></a>';
        }
        return htmlstr;
    }
    function formatterView(value,row,index)
    {

        var htmlstr =  '<a id="view_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton "'
                    +        'data-options="iconCls:\'fa fa-hand-pointer-o\',plain:true" '
                    +        'onclick="funDg2View('+'\''+row.id+'\')"></a>';
        return htmlstr;
    }    
    
    function formatterIsChanged(value,row,index)
    {
        var htmlstr;
        if(row.changed == false)
        {
             htmlstr =  '<a id="change_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton "'
                        +    'data-options="plain:true" '
                        +    'onclick="funDg2ViewNoChange()">--</a>';
        }
        else
        {
             htmlstr =  '<a id="change_'+row.id+'" href="javascript:void(0)" class="easyui-linkbutton " \
                            data-options="iconCls:\'fa fa-exclamation text-danger\',plain:true" \
                            onclick="funDg2ViewChange('+'\''+row.id+'\')"></a>';
        }

        return htmlstr;
    }
    function funDg2ViewNoChange()
    {
        shuoheUtil.layerTopMsgError('没有产生变更');
    }
    function funDg2ViewChange(id)
    {
        console.info('funDg2ViewChange = '+id);
    }
    function funDg2View(id)
    {
        console.info('funDg2View = '+id);
        Easyui.Layer.openLayerWindow(
            '查看报价单',
            'viewOffer.jsp?id='+id,
            ['750px','600px']);  
    }
    function funDg2Delete(id)
    {
        console.info('funDg2Delete = '+id);
        shuoheUtil.layerSaveMaskOn();
        $.ajax({
            url: "/shop/history/discardProductById.do",
            type: "POST",
            dataType:'json',
            async: false,
            data:
            {            
                id: id
            },
            error: function() {
                shuoheUtil.layerSaveMaskOff();
            },
            success: function(data1)//成功
            {
                console.log(data1)
                shuoheUtil.layerSaveMaskOff();
                shuoheUtil.layerTopMsgOK('保存成功');
                $('#dg2').datagrid('reload');
            }
        })        
    }
</script>