<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
      xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
<head lang="zh-CN">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, process-scalable=no"/>
    <title>用戶列表</title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
 <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" rel="stylesheet"/>
 <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="/lib/layer/theme/default/layer.css" rel="stylesheet"/>
 
 <script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="/lib/layer/layer.js"></script>
 <script src="/js/common.js"></script>
 <style>
 	table{
 		border:1px solid #000
 	}
 	table th{
 		border:1px solid #000;
 		background:gray;
 		color:white;
 	}
 	table td{
 		border:1px solid #000
 	}
 </style>
</head>
<body>
<div class="container">
  	<div id="tabletarget">
  	</div>
		
	

</div>
<script type="text/javascript">
$(document).ready(function(){
		$.ajax({
            url:"/process/myProcessList",
            type:"post",
            contentType:'application/json',
            success:function(list){
                if(list){
                	var op = '<table id="tablelist" class="table table-bordered"><tr><th>任务名称</th><th>任务id</th><th>任务执行人</th><th>任务描述</th><th>任务定义名称</th><th>任务申请时间</th><th>操作</th></tr><tr>';
                	$.each(list, function (i, group) { 
                		op+='<td>'+list[i].name+'</td><td>'+list[i].taskId+'</td><td>'+list[i].assignee+'</td><td>'+list[i].description+'</td><td>'+list[i].taskDefinitionKey+'</td><td>'+list[i].createTime+'</td><td><a href="#" onclick="claimTask('+list[i].taskId+');return false;">认领任务</a>&nbsp;&nbsp;<a href="#" onclick="claimTask('+list[i].taskId+');return false;">完成任务</a></td>';
                    });
                	op+='</tr></table>';
					$("#tabletarget").append(op);
                }else{
                	layer.msg("技术不行啊");
                }
            },
            error:function(e){
                alert("错误！！");
            }
        });   
})
	function claimTask(taskId){
		$.ajax({
            url:"/process/completeTask",
            type:"post",
            data:{
            	taskId:taskId
            },
            success:function(result){
            	if(result.status==0){
                	layer.msg("任务申领成功");
                }else{
                	layer.msg("技术不行啊");
                }
            },
            error:function(e){
                alert("错误！！");
                window.clearInterval(timer);
            }
        });        
	}
	
function completeTask(taskId){
	$.ajax({
        url:"/process/claimTask",
        type:"post",
        data:{
        	taskId:taskId
        },
        success:function(result){
        	if(result.status==0){
            	layer.msg("任务完成");
            }else{
            	layer.msg("技术不行啊");
            }
        },
        error:function(e){
            alert("错误！！");
            window.clearInterval(timer);
        }
    });        
}
</script>
</body>
</html>