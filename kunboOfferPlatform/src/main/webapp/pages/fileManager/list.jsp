<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>文件管理务列表</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/js/layui/css/layui.css" rel="stylesheet" media="all"/>
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layui/layui.js"></script>
<%-- <script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/pdfjs/build/pdf.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/pdfjs/build/pdf.worker.js"></script> --%>
<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">文件编号</th>
					<th field="fileName" align="center">文件名称</th>
					<th field="fileType" align="center">文件类型</th>
					<th data-options="field:'_showpath',align:'center',formatter:formatIt">文件路径</th>
					<th field="filePath" align="center" hidden="true" >文件路径</th>
					<th field="fileRealPath" align="center" hidden="true">文件真实路径</th>
					<th field="uploadTime" align="center">上传时间</th>
					<th field="uploadBy" align="center">上传者</th>
					<th data-options="field:'_operate',width:80,align:'center',formatter:showOrHidden">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<button id='addFile'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="uploadFile()">上传文档</button>
			<button id='addPic'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="uploadPic()">上传图片</button>
			<button id='addExcel'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="uploadExc()">导入数据表格</button>
			<button id='addExcelSimple'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="uploadExcSimple()">导入数据表格示范</button>
				<!-- <input type="file" name="file" class="layui-upload-file" > -->
		</div>
	</div>


<script type="text/javascript">
</script>
	<script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/upload/getFilelist',
        queryParams:
        {
        },
        method:'get'
      })
    });
    //对于超出60个字符的剩余部分，用。。。显示
    function formatIt(value, row, index){
    	return expHtmlTab(row.filePath);
    }
  </script>
	<script type="text/javascript">
	//控制修改按钮的显示
	function showOrHidden(value,row,index){
		var fileType = row.fileType
		if(fileType==".jpg"||fileType==".png"||fileType==".jpeg"||fileType==".bmp"){
			return "<a onclick=\"previewPic('"+row.filePath+"','"+row.fileName+"')\">预览</a>/<a onclick=\"deleteFile('"+row.id+"','"+row.fileRealPath+"')\">删除</a>";
		}else if(fileType==".pdf"){
			return "<a onclick=\"previewPdf('"+row.filePath+"')\">预览</a>/<a onclick=\"deleteFile('"+row.id+"','"+row.fileRealPath+"')\">删除</a>";
		}else if(fileType==".zip"){
			return "<a href='"+"<%=basePath%>"+row.filePath+"'>下载</a>";
		}else{
			return "<a onclick=\"deleteFile('"+row.id+"','"+row.fileRealPath+"')\">删除</a>";
		}
	};
	
  	function onClickRow(rowIndex,rowData){
  	};

  	function previewPdf(filePath){
  		perContent = layer.open({
  			type: 2,
            title: '预览pdf',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '550px'],
            content:'<%=basePath%>/TopJUI/topjui/plugins/pdfjs/web/viewer.html?file='+filePath
  		});
  		layer.full(perContent);
  	}
  	
  	
  	function previewPic(filePath,fileName){
  		layer.photos({
  		    photos: 
  		  	{
  	  		  "title": "图片预览", //相册标题
  	  		  "id": 1, //相册id
  	  		  "start": 0, //初始显示的图片序号，默认0
  	  		  "data": [   //相册包含的图片，数组格式
  	  		    {
  	  		      "alt": fileName,
  	  		      "pid": 1, //图片id
  	  		      "src": filePath, //原图地址
  	  		      //"thumb": "" //缩略图地址
  	  		    }
  	  		  ]
  	  		}
  		    ,shift: 5 //0-6的选择，指定弹出图片动画类型，默认随机
  		  });
  	}
  	
  	function uploadFile(){
  		layer.open({
  			type: 2,
            title: '上传文件',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '600px'],
            content:'<%=basePath%>/upload/add',
            btn:['关闭'],
            yes: function(index, layero){
                 //按钮【按钮一】的回调
            	//var res = window["layui-layer-iframe" + index].callbackdata();
            	//最后关闭弹出层
            	layer.close(index);
            	location.reload(); 
            },
  		});
  	}
  	function uploadPic(){
  		layer.open({
  			type: 2,
            title: '上传文件',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '600px'],
            content:'<%=basePath%>/upload/addPicture',
            btn:['关闭'],
            yes: function(index, layero){
                 //按钮【按钮一】的回调
            	//var res = window["layui-layer-iframe" + index].callbackdata();
            	//最后关闭弹出层
            	layer.close(index);
            	location.reload(); 
            },
  		});
  	}
  	function uploadExc(){
  		layer.open({
  			type: 2,
            title: '导入excel',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '600px'],
            content:'<%=basePath%>/upload/addExc',
            btn:['关闭'],
            yes: function(index, layero){
                //按钮【按钮一】的回调
            	//var res = window["layui-layer-iframe" + index].callbackdata();
            	//最后关闭弹出层
            	layer.close(index);
            	//location.reload(); 
            },
  		});
  	}
  	
  	function uploadExcSimple(){
  		layer.open({
  			type: 2,
            title: '导入excel',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '600px'],
            content:'<%=basePath%>/upload/addExcelSimple',
            btn:['关闭'],
            yes: function(index, layero){
                //按钮【按钮一】的回调
            	//var res = window["layui-layer-iframe" + index].callbackdata();
            	//最后关闭弹出层
            	layer.close(index);
            	//location.reload(); 
            },
  		});
  	}
  	function deleteFile(id,filePath){
  		$.ajax({
            url: "<%=basePath%>/upload/deleteFile",
    				type : "POST",
    				dataType : 'json',
    				data : {
    					id:id,
    					filePath:filePath,
    				},
    				success : function(result)//成功
    				{
    					layer.msg(result.msg);
    				}
    	});
    	location.reload();    
  	}
  </script>
</body>
</html>
