<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;

	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>上传文件</title>

<link rel="stylesheet" href="/TopJUI/topjui/plugins/uploadify/Huploadify.css">
<!-- jQuery相关引用 -->
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/jquery-2.2.0.min.js"></script>
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/jquery.dragsort-0.5.2.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
<!-- HHuploadify文件上传 -->
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/jquery.Huploadify.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>
</head>
<body >
	<div id="upload"></div>
	<script type="text/javascript">
	var path = ''
	$('#upload').Huploadify({
		fileTypeExts:'*.*',//允许上传的文件类型，格式'*.jpg;*.doc'
		uploader:'<%=basePath%>/upload/file',//文件提交的地址
        auto:false,//是否开启自动上传
        method:'post',//发送请求的方式，get或post
        multi:false,//是否允许选择多个文件
        formData:{fileType:"file"},//发送给服务端的参数，格式：{key1:value1,key2:value2}
        fileObjName:'file',//在后端接受文件的参数名称，如PHP中的$_FILES['file']
        fileSizeLimit:102400,//允许上传的文件大小，单位KB
        showUploadedPercent:true,//是否实时显示上传的百分比，如20%
        showUploadedSize:false,//是否实时显示已上传的文件大小，如1M/2M
        buttonText:'选择文件',//上传按钮上的文字
        removeTimeout: 1000,//上传完成后进度条的消失时间，单位毫秒
        //itemTemplate:itemTemp,//上传队列显示的模板
	    onUploadStart:function(file){
	        console.log(file.name+'开始上传');
	    },
	    onInit:function(obj){
	        console.log('初始化');
	        console.log(obj);
	    },
	    onUploadComplete:function(file,data){
	        console.log(data);
					path = JSON.parse(data).obj.filePath
	    },
	    onCancel:function(file){
	        console.log(file.name+'删除成功');
	    },
	    onClearQueue:function(queueItemCount){
	        console.log('有'+queueItemCount+'个文件被删除了');
	    },
	    onDestroy:function(){
	        console.log('destroyed!');
	    },
	    onSelect:function(file){
	        console.log(file.name+'加入上传队列');
	    },
	    onQueueComplete:function(queueData){
	        console.log('队列中的文件全部上传完成',queueData);
	    }
	});
	function close () {
		return path
	}
		//onUploadSuccess：在返回状态status为200时触发
		//onUploadError：返回状态status不为200时触发
		//onUploadComplete：在onUploadSuccess或onUploadError触发后触发
	</script>
</body>
</html>
