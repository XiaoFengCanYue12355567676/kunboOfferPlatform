<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>上传文件</title>

<!-- <link rel="stylesheet" href="/TopJUI/topjui/plugins/uploadify/Huploadify.css"> -->
<link type="text/css" href="/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css" href="/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<!-- jQuery相关引用 -->
<script src="/TopJUI/topjui/plugins/uploadify/jquery-2.2.0.min.js"></script>
<script src="/TopJUI/topjui/plugins/uploadify/jquery.dragsort-0.5.2.min.js"></script>
<script type="text/javascript" src="/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript" src="/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript" src="/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript" src="/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript" src="/pages/js/moment.min.js"></script>
<script type="text/javascript" src="/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="/pages/js/util.js"></script>

<script type="text/javascript" src="/pages/js/base-loading.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<!-- HHuploadify文件上传 -->
<script src="/TopJUI/topjui/plugins/uploadify/jquery.Huploadify.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>
</head>
<body class="easyui-layout" style="overflow: hidden;">
	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:false,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20">
			<thead>
				<tr href="#">
<!-- 					<th field="fileName" >文件名称</th>
					<th field="progess" formatter='formatterProgress' >上传进度</th> -->
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<button id="btnUpload" style="width: 100px;height: 35px"></button>
			<!-- <button id='btnUpload'  class="easyui-linkbutton uploadify-button" data-options="iconCls:'fa fa-plus',plain:true" ></button>		 -->
		</div>		
	</div>



	<!-- <div id="upload" hidden="true"></div> -->
<script type="text/javascript">
	var paths = []
	$('#btnUpload').Huploadify({
		fileTypeExts:'*.*',//允许上传的文件类型，格式'*.jpg;*.doc'
		uploader:'/upload/file',//文件提交的地址
        auto:false,//是否开启自动上传
        method:'post',//发送请求的方式，get或post
        multi:true,//是否允许选择多个文件
        formData:{fileType:"file"},//发送给服务端的参数，格式：{key1:value1,key2:value2}
        fileObjName:'file',//在后端接受文件的参数名称，如PHP中的$_FILES['file']
        fileSizeLimit:102400,//允许上传的文件大小，单位KB
        showUploadedPercent:true,//是否实时显示上传的百分比，如20%
        showUploadedSize:false,//是否实时显示已上传的文件大小，如1M/2M
        buttonText:'选择文件',//上传按钮上的文字
        removeTimeout: 1000,//上传完成后进度条的消失时间，单位毫秒
        //itemTemplate:itemTemp,//上传队列显示的模板
	    onUploadStart:function(file){
	        console.log(file.name+'开始上传');
	    },
	    onInit:function(obj){
	        console.log('初始化');
	        console.log(obj);
	    },
	    onUploadComplete:function(file,data){
	        console.log(file.name+'上传完成');
					paths.push(JSON.parse(data).obj.filePath)
	    },
	    onCancel:function(file){
	        console.log(file.name+'删除成功');
	    },
	    onClearQueue:function(queueItemCount){
	        console.log('有'+queueItemCount+'个文件被删除了');
	    },
	    onDestroy:function(){
	        console.log('destroyed!');
	    },
	    onSelect:function(file){
			console.log(file.name + '加入上传队列');
			console.log(file);
			var a = $('#dg').datagrid('appendRow', {
				fileName: file.name,
				progess:true
			});
			console.info('a = '+a);
			$('#p').progressbar({
				value: 60
			});
			$('#btn').linkbutton();
	    },
	    onQueueComplete:function(queueData){
	        console.log('队列中的文件全部上传完成',queueData);
	    },
		onProgress: function(file, loaded, total) {
			var percent = (loaded / total * 100).toFixed(2) + '%';
			console.log('onProgress = '+percent);
			// var eleProgress = _this.find('#fileupload_' + instanceNumber + '_' + file.index + ' .uploadify-progress');
			// var percent = (loaded / total * 100).toFixed(2) + '%';
			// if (option.showUploadedSize) {
			// 	eleProgress.nextAll('.progressnum .uploadedsize').text(F.formatFileSize(loaded));
			// 	eleProgress.nextAll('.progressnum .totalsize').text(F.formatFileSize(total));
			// }
			// if (option.showUploadedPercent) {
			// 	eleProgress.nextAll('.up_percent').text(percent);
			// }
			// eleProgress.children('.uploadify-progress-bar').css('width', percent);
		}
	});
	function close () {
		return paths
	}
		//onUploadSuccess：在返回状态status为200时触发
		//onUploadError：返回状态status不为200时触发
		//onUploadComplete：在onUploadSuccess或onUploadError触发后触发
	// function formatterProgress(value, row, index) {
	// 	// var htmlstr = '<div id="p" class="easyui-progressbar" data-options="value:60" style="width:400px;"></div>';
	// 	var htmlstr = '<input id=1  class="easyui-switchbutton" checked>';  
	// 	return htmlstr;
	// }
	$('#dg').datagrid({
		columns: [
			[{
				field: 'fileName',
				title: '文件名称'
			}, {
				field: 'progess',
				title: '进度',width:420,height:'50px',
				formatter: function(value, row, index) {
					console.info('formatter');
					// var htmlstr = '<input id="a1" class="easyui-switchbutton" checked>';
					var htmlstr = '<div id="p" class="easyui-progressbar" data-options="value:60" style="width:400;"></div>';
					return htmlstr;
				}
			},{
				field: 'btn',
				title: '按钮',width:300,
				formatter: function(value, row, index) {
					console.info('formatter');
					// var htmlstr = '<input id="a1" class="easyui-switchbutton" checked>';
					var htmlstr = '<a id="btn1" href="#" class="easyui-linkbutton fa fa-cloud-upload" onclick="a()">上传</a>'
					htmlstr += '<a id="btn2" href="#" class="easyui-linkbutton fa fa-times" onclick="a()">删除</a>'
					return htmlstr;
				}
			}]
		],
		onLoadSuccess:function(data) {
			console.log('onLoadSuccess ='+data);
		},
		onBeforeLoad:function(param) {
			console.log('onBeforeLoad ='+param);
		}
	})
	function a() {
		console.info('a');
	}
</script>
</body>
</html>
