<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>上传文件</title>

<link rel="stylesheet" href="/TopJUI/topjui/plugins/uploadify/HHuploadify.css">
<!-- jQuery相关引用 -->
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/jquery-2.2.0.min.js"></script>
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/jquery.dragsort-0.5.2.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/js/common.js"></script>
<!-- HHuploadify文件上传 -->
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/HHuploadify.js"></script>
<script src="<%=basePath%>/TopJUI/topjui/plugins/uploadify/HHuploadify.dragable.js"></script>

<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>
</head>
<body >
	<div id="upload"></div>
	<script type="text/javascript">
	var imgPath = ''
	var options = {
		  container: '#upload', // i.e. #upload

		  // upload options
		  url: '<%=basePath%>/upload/file', // upload to which server url
		  method: 'post', // http request type: post/put
		  field: 'file', // upload file name field, php $_FILES['file']
		  data: {fileType:'img'}, // append data in your request like: {key1:value1,key2:value2}

		  // view options
		  fileTypeExts: 'jpg,jpeg,png,gif,JPG,PNG,GIF,JPEG', // file can be uploaded exts like: 'jpg,png'
		  fileSizeLimit: 2048, // max upload file size: KB

		  multiple: false, // be or not be able to choose multi files
		  single: true, // force to upload only one item, even through multiple is true
		  auto: false, // auto begin to upload after select local files

		  chooseText: '选择图片', // words on choose button
		  uploadText: '上传', // words on upload button, if auto is true, upload button will not show


		  files: null, // array, if files is not empty, list will be rendered when plugin loaded, see demo

		  showUploadProcess: 'percent', // bar|percent|size, when uploading, which one to show the process of uploading status
		  showPreview: 1, // whether preview file before/during upload, 0: close; 1: only preview local origin file; 2: preview file on server by result 'url' fields after upload complated
		  //showPreviewField: 'url', // when showPreview is 2, which field will be used as image url from server side in response json
		  template: '<span id="uploadify-{queueId}-{fileId}" class="uploadify-item"><span class="uploadify-item-container"><span class="uploadify-item-progress"></span><a href="javascript:void(0);" class="uploadify-item-delete" data-fileid="{fileId}">&times;</a></span></span>',
		  // envents callback functions
		  onInit: null, // when plugin is ready
		  onSelect: null, // when select a file
		  onSelectError: null,
		  onUploadStart: null, // when a file upload start
			onUploadSuccess: function(file,data) {
        // console.log(data)
				imgPath = JSON.parse(data).obj.filePath
			},
		  onUploadError: null, // when a file upload fail
		  onUploadComplete: null, // when a file upload finished, success or failure
		  onUploadCancel: null, // when cancel a file to upload
		  onQueueComplete: null, // when all of the files in a queue complate (success or error), may you have more than one queue
		  onRemoved: null, // when remove a file in the list
		  onDestroy: null, // when all resource removed
		  onReset: null, // when after reset done
		}

	var uploader = new HHuploadify(options)
	function transferPath () { // 取图片路径
		return imgPath
	}
	</script>
</body>
</html>
