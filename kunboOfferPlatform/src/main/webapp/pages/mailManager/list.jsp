<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*"%>
<%@page import="java.io.PrintWriter"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	User user = null;
	String userJson = null;
	String permissionssJson = null;
	PrintWriter ss = response.getWriter();
	try {
		user = (User) session.getAttribute("user");
		if (user == null) {
			System.out.println("user = null");
			//response.sendRedirect(basePath+"/pages/login.jsp");
			String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
			ss.print(topLocation);
		} else {
			userJson = Json.toJsonByPretty(user);
		}
	} catch (Exception e) {
		String topLocation = "<script>top.location.href=\"" + basePath + "/pages/login.jsp\"</script>";
		ss.print(topLocation);
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>邮件管理列表</title>


<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css"
	rel="stylesheet">
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css"
	rel="stylesheet" id="dynamicTheme" />
<!-- FontAwesome字体图标 -->
<link type="text/css"
	href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />
<!-- jQuery相关引用 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<!-- TopJUI框架配置 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<!-- TopJUI框架核心-->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<!-- TopJUI中文支持 -->
<script type="text/javascript"
	src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/pages/js/base-loading.js"></script>

<script type="text/javascript"
	src="<%=basePath%>/js/common.js"></script>
<style type="text/css">
html, body {
	margin: 0;
	height: 100%;
}
</style>

</head>
<body class="easyui-layout" style="overflow: hidden;">
	<script type="text/javascript">
      var user = <%=userJson%>;
      var static_permissionssJson = <%=permissionssJson%>;
    </script>

	<div data-options="region:'center',title:'',split:true"
		style="width: 100%;">
		<table id="dg" class='easyui-datagrid'
			style="width: 100%; height: 100%" title=""
			data-options="
                rownumbers:true,
                singleSelect:true,
                autoRowHeight:false,
                pagination:true,
                fitColumns:false,
                striped:true,
                checkOnSelect:true,
                selectOnCheck:true,
                collapsible:true,
                toolbar:'#tb',
                pageSize:20,
                onClickRow:onClickRow">
			<thead>
				<tr href="#">
					<th field="id" align="center">文件编号</th>
					<th field="subject" align="center">文件名称</th>
					<th field="content" align="center">文件类型</th>
					<th field="to" align="center" hidden="true" >文件路径</th>
					<th field="carbonCopy" align="center" hidden="true">文件真实路径</th>
					<th field="bCarbonCopy" align="center">上传时间</th>
					<th field="attachment" align="center">上传者</th>
					<th field="sendTime" align="center">上传者</th>
					<th field="sendBy" align="center">上传者</th>
					<th data-options="field:'_operate',width:80,align:'center',formatter:showOrHidden">操作</th>
				</tr>
			</thead>
		</table>
		<div id="tb" style="height: 35px">
			<button id='createNewMail'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="createNewMail()">新建邮件</button>
			<button id='addPic'  class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="uploadPic()">上传图片</button>
				<!-- <input type="file" name="file" class="layui-upload-file" > -->
		</div>
	</div>


<script type="text/javascript">
</script>
	<script type="text/javascript">
    $(function(){
      $('#dg').datagrid({
        url:'<%=basePath%>/mail/getMailList',
        queryParams:
        {
        },
        method:'get'
      })
    });
    //对于超出60个字符的剩余部分，用。。。显示
    function formatIt(value, row, index){
    	return expHtmlTab(row.filePath);
    }
  </script>
	<script type="text/javascript">
	
	//控制修改按钮的显示
	function showOrHidden(value,row,index){
	};
	
  	function onClickRow(rowIndex,rowData){
  	};

  	function previewPdf(filePath){
  	}
  	
  	
  	function previewPic(filePath,fileName){
  	}
  	
  	function createNewMail(){
  		layer.open({
  			type: 2,
            title: '发送邮件',
            shadeClose: false,
            shade: 0.3,
            zIndex: 1,
            maxmin: true, //开启最大化最小化按钮
            area: ['940px', '600px'],
            content:'<%=basePath%>/mail/add',
            btn:['关闭'],
            yes: function(index, layero){
                 //按钮【按钮一】的回调
            	//var res = window["layui-layer-iframe" + index].callbackdata();
            	//最后关闭弹出层
            	layer.close(index);
            	location.reload(); 
            },
  		});
  	}
  </script>
</body>
</html>
