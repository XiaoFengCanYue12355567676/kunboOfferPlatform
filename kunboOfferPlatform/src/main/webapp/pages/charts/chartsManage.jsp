<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>图表管理</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-panel" data-options="fit:true">
  <div class="form-order-table-con" style="width:30%;;height: 100%;">
    <table id="tt" style="height:100%;width: 100%;">
      <thead>
        <tr>
          <th field="text" width="100%">表单</th>
          <th field="id" width="0px" hidden="true">序列</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="form-order-table-con" style="width:70%;height: 100%">
    <table id="dg" class='easyui-datagrid' style="width:100%;height:100%">
        <thead>
          <tr>
            <th field="id" align="center" hidden ="true">id</th>
            <th field="form_name" align="center">表单</th>
            <th field="name" align="center">名称</th>
            <th field="descr" align="center">描述</th>
            <th field="type" align="center">图表类型</th>
            <th field="x_field" align="center">X轴字段</th>
            <th field="y_field" align="center">Y轴字段</th>
            <th field="creater" align="center">创建人</th>
            <th field="create_date" align="center">创建时间</th>
          </tr>
        </thead>
      </table>
      <div id="tb" style="height:35px">
        <%-- <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="view()">查看</a> --%>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-plus',plain:true" onclick="add()">新增</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-pencil',plain:true" onclick="edit()">修改</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'fa fa-trash',plain:true" onclick="dele()">删除</a>
      </div>
  </div>
</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript">
var basePath = '<%=basePath%>'
var user = <%=userJson%>
$(function(){
  $('#tt').treegrid({
    url: '<%=basePath%>/topJUI/index/getMenuList.do',
    idField:'id',
    treeField:'text',
    onClickRow: function (rowIndex) {
     var rr_row = $('#tt').treegrid('getSelected');
     $('#dg').datagrid({
       url:'<%=basePath%>/crm/ActionFormSelectUtil/Select/getChartsByFormName.do',
       queryParams: {
         formName:rr_row.url.split('=')[1]
       }
     })
    }
  })
  $('#dg').datagrid({
    rownumbers:true,
    singleSelect:true,
    autoRowHeight:false,
    pagination:true,
    fitColumns:true,
    striped:true,
    toolbar:'#tb',
    pageSize:20
  })
})
//新增一条数据
function add () {
  var row = $('#tt').treegrid('getSelected')
  if(row == null){
    layerMsgCustom('请选择一个目录')
    return;
  }
  layer.open({
      type: 2,
      title: '新建图表',
      shadeClose: false,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: ['900px', document.body.clientHeight-20+'px'],
      btn: '保存',
      content: 'addChart.jsp?formName='+row.url.split('=')[1],
      yes:function(index){
        var ifname="layui-layer-iframe"+index//获得layer层的名字
        var Ifame=window.frames[ifname]//得到框架
        Ifame.save();
      }
  })
}
//新增成功
function addDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//修改一条数据
function edit () {
  var row = $('#dg').treegrid('getSelected')
  if(row == null){
    layerMsgCustom('请选择一条数据')
    return;
  }
  layer.open({
      type: 2,
      title: '修改图表',
      shadeClose: false,
      shade: 0.3,
      maxmin: true, //开启最大化最小化按钮
      area: ['900px', document.body.clientHeight-20+'px'],
      btn: '保存',
      content: 'editChart.jsp?id='+row.id,
      yes:function(index){
        var ifname="layui-layer-iframe"+index//获得layer层的名字
        var Ifame=window.frames[ifname]//得到框架
        Ifame.save();
      }
  })
}
//传输选中数据
function transferRowData () {
  var row = $('#dg').datagrid('getSelected')
  return row
}
//修改成功
function editDialogCallBackSuccess () {
  messageSaveOK()
  $('#dg').datagrid('reload')
}
//删除一条数据
function dele () {
  var row = $('#dg').datagrid('getSelected')
  if(row == null) {
    alert('请先选择一条数据')
    return false
  } else {
    $.messager.confirm('确认','您确认想要删除吗？',function(r){
      if (r){
        $.ajax({
          url: "<%=basePath%>/crm/ActionFormSelectUtil/Select/deleteCharts.do",
          type: "POST",
          dataType:'text',
          data:
          {
            'id': row.id
          },
          error: function() //失败
          {
            messageDeleteError()
          },
          success: function(data)//成功
          {
            data = $.trim(data)
            if(data == '0'){
              messageDeleteError()
            }else{
              messageDeleteOK()
             $('#dg').datagrid('reload')
            }
          }
        })
      }
    })
  }
}
</script>
