/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : oa

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-02-09 09:21:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for charts_manage
-- ----------------------------
DROP TABLE IF EXISTS `charts_manage`;
CREATE TABLE `charts_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `form_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `creater` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name_x` varchar(255) DEFAULT NULL,
  `name_y` varchar(255) DEFAULT NULL,
  `x_field` varchar(255) DEFAULT NULL,
  `y_field` varchar(255) DEFAULT NULL,
  `searchs` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
