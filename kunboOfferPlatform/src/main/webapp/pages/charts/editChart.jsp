<%@page import="com.shuohe.entity.system.user.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page language="java" import="java.util.*"%>
<%@ page import="com.shuohe.util.json.*" %>
<%@page import="java.io.PrintWriter"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
    String id = request.getParameter("id");
    User user = null;
    String userJson = null;
    String permissionssJson = null;
    PrintWriter ss = response.getWriter();
    try
    {
        user = (User)session.getAttribute("user");
        if(user==null)
        {
          System.out.println("user = null");
          //response.sendRedirect(basePath+"/pages/login.jsp");
          String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
          ss.print(topLocation);
        }
        else
        {
            userJson = Json.toJsonByPretty(user);
        }
    }
    catch(Exception e)
    {
        String topLocation = "<script>top.location.href=\""+basePath+"/pages/login.jsp\"</script>";
        ss.print(topLocation);
        e.printStackTrace();
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="renderer" content="webkit">
<title>创建图表</title>
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/css/topjui.core.min.css" rel="stylesheet">
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/themes/default/topjui.green.css" rel="stylesheet" id="dynamicTheme" />
<link type="text/css" href="<%=basePath%>/TopJUI/topjui/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=basePath%>/pages/css/add-form.css" rel="stylesheet" />
</head>
<body>
<div class="easyui-layout">
  <div class="field-con">
    <div class="field-form-line">
      <label>名称</label>
      <input type="text" id="name" class="easyui-textbox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>图表类型</label>
      <select class="easyui-combobox" id="type" data-options="width:278,panelHeight:105,editable:false">
        <option value="line">折线图</option>
        <option value="bar">柱状图</option>
        <option value="pie">饼图</option>
      </select>
    </div>
    <div class="field-form-line field-form-line-one">
      <label>描述</label>
      <input type="text" id="descr" class="easyui-textbox" data-options="width:708" />
    </div>
    <div class="field-form-line">
      <label>X轴字段</label>
      <input type="text" id="x_field" class="easyui-combobox" data-options="width:278" />
    </div>
    <div class="field-form-line">
      <label>Y轴字段(数值)</label>
      <input type="text" id="y_field" class="easyui-combobox" data-options="width:278" />
    </div>
    <div class="field-form-line field-form-line-one">
      <label>搜索条件</label>
      <div class="field-form-table-con">
        <table id="dg_searchs" style="width:100%;height:200px">
          <thead>
            <tr>
              <th field="manner" halign="center" width="30%" editor="{type:'combobox',options:{data:mannerData,editable:false}}" formatter="formatterManner">方式</th>
              <th field="field" halign="center" width="40%" editor="{type:'combobox',options:{valueField:'text',textField:'title',editable:false,data:xFidlds}}">字段</th>
              <th field="condition" halign="center" width="30%" editor="{type:'combobox',options:{editable:false,data:conditionData}}" formatter="formatterCondition">条件</th>
            </tr>
          </thead>
        </table>
        <div id="tb_searchs" style="padding:0px 30px;height: 35px">
          <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-plus" onclick="compentAdd()">新增</a>
          <a href="javascript:void(0)" class="easyui-linkbutton" btnCls="topjui-btn-normal" plain="true" iconCls="fa fa-trash" onclick="compentDelete()">删除</a>
        </div>

      </div>
    </div>
  </div>

</div>
</body>
</html>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/plugins/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/static/public/js/topjui.config.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/topjui.core.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/TopJUI/topjui/js/locale/topjui.lang.zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/moment.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/layer/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/base-loading.js"></script>
<script type="text/javascript" src="<%=basePath%>/pages/js/add-form.js"></script>
<script type="text/javascript">
var basePath = '<%=basePath%>'
var user = <%=userJson%>
var formName = ''
var fields = []
var xFidlds = []
var yFidlds = []
var searchs = []
var id = '<%=id%>'
var result
$(function(){
  init()
})
function init () {
  result = parent.transferRowData()
  console.log(result)
  $.ajax({
    url: "<%=basePath%>/pages/crminterface/getDatagridForJson.do",
    type: "POST",
    dataType:'json',
    data:
    {
      'tableName': result.form_name
    },
    error: function() // 失败
    {
      messageloadError()
    },
    success: function(data) // 成功
    {
      fields = data.field
      xFidlds = removeFieldDisable(fields)
      yFidlds = fieldsForChart(fields)
      $('#x_field').combobox({
        data: xFidlds,
        valueField: 'text',
        textField: 'title',
        editable: false
      })
      $('#y_field').combobox({
        data: yFidlds,
        valueField: 'text',
        textField: 'title',
        editable: false
      })

      $('#name').textbox('setValue', result.name)
      $('#type').combobox('setValue', result.type)
      $('#descr').textbox('setValue', result.descr)
      $('#x_field').combobox('setValue', result.x_field)
      $('#y_field').combobox('setValue', result.y_field)
      if (result.searchs!='') {
        searchs = JSON.parse(result.searchs)
      }
      //搜索初始化
      $('#dg_searchs').edatagrid({
        data:searchs,
        rownumbers:true,
        singleSelect:true,
        autoRowHeight:false,
        pagination:false,
        fitColumns:true,
        striped:true,
        toolbar:'#tb_searchs',
        autoSave:true,
    		destroyMsg:{
    			norecord:{
    				title:'',
    				msg:'请先选择一条数据'
    			},
    			confirm:{
    				title:'',
    				msg:'确定要删除吗?'
    			}
    		}
      })
    }
  })
}
//新增一条搜索条件
function compentAdd () {
	$('#dg_searchs').edatagrid('addRow')
}
//删除一条搜索条件
function compentDelete () {
	var row = $('#dg_searchs').edatagrid('getSelected')
	var i = $('#dg_searchs').edatagrid('getRowIndex', row)
	$('#dg_searchs').edatagrid('editRow', i)
	$('#dg_searchs').edatagrid('destroyRow', i)
}
// 保存
function save () {
  if (checkInput()) {
    var obj = new Object()
    obj.name = $.trim($('#name').textbox('getValue'))
    obj.type = $('#type').combobox('getValue')
    obj.descr = $.trim($('#descr').textbox('getValue'))
    obj.x_field = $('#x_field').combobox('getValue')
    obj.y_field = $('#y_field').combobox('getValue')
    obj.name_x = $('#x_field').combobox('getText')
    obj.name_y = $('#y_field').combobox('getText')
    obj.searchs = JSON.stringify(searchs)
    var info_str = JSON.stringify(obj)
    console.log(info_str)
    $.ajax({
      url: "<%=basePath%>/crm/ActionFormSelectUtil/Select/updateCharts.do",
      type: "POST",
      dataType:'text',
      data:
      {
        "jsonStr": info_str,
        "id": id
      },
      error: function(e) //失败
      {
       console.info("异常="+JSON.stringify(e));
        messageSaveError();
      },
      success: function(data)//成功
      {
        data = $.trim(data)
        if(data == '1'){
        	var index = parent.layer.getFrameIndex(window.name) //先得到当前iframe层的索引
          parent.editDialogCallBackSuccess()
        	parent.layer.close(index) //再执行关闭
        }else{
        	 messageSaveError();
        }
      }
    })
  }
}
//验证表单
function checkInput () {
  if (checkNull($('#name').textbox('getValue'))) {
     layerMsgCustom('名称不能为空')
     return false
  }if (checkNull($('#type').combobox('getValue'))) {
     layerMsgCustom('请选择类型')
     return false
  }
  return true
}
</script>
